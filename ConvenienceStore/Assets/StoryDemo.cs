﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryDemo : MonoBehaviour
{
    public string path;
    public TextAsset Drama;

    private void Start ()
    {
        string drama = Drama.text;
        string [] StoryText = drama.Split ( '\n' );
        StartCoroutine ( PlayDemo ( StoryText ) );
    }
    IEnumerator PlayDemo ( string [] StoryText )
    {
        StoryManager.self.gameObject.SetActive ( true );
        DramaPlayerManager dramaPlayer = new DramaPlayerManager ( "" , path , false );
        yield return dramaPlayer.Play ( StoryText );
    }
}
