﻿Shader "GGDogShader/Memory Overlay"
{
    Properties
    {
        _DarkTex ("Dark Tex", 2D) = "white" {}
		_DarkTex_Alpha("DarkTex_Alpha",Range(0,1)) = 0.85
        _GlowTex ("Glow Tex", 2D) = "white" {}
		_GlowTex_Alpha("GlowTex_Alpha",Range(0,1)) = 0.6
        _FadeTex ("Fade Tex", 2D) = "white" {}
		_FadeTex_Alpha("FadeTex_Alpha",Range(0,1)) = 0.7
		_Overlay_Color("Overlay_Color",COLOR) = (1,1,1,1)
    }
    SubShader
    {
        Pass
        {
			
			Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float4 scrPos : TEXCOORD1;
				float4 color : COLOR;
            };
			
            sampler2D _DarkTex;
			fixed _DarkTex_Alpha;
			
            sampler2D _GlowTex;
			fixed _GlowTex_Alpha;

            sampler2D _FadeTex;
            float4 _FadeTex_ST;
			fixed _FadeTex_Alpha;

			uniform sampler2D _RenderTexture;

			float4 _Overlay_Color; //覆蓋顏色

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _FadeTex);
				o.scrPos = ComputeGrabScreenPos(o.vertex);
				o.color = v.color;
                return o;
            }
			
            fixed4 frag (v2f i) : SV_Target
            {
				fixed3 grabpass_color =tex2D(_RenderTexture,i.scrPos.xy/i.scrPos.w).rgb;

				fixed Gray = (grabpass_color.r+grabpass_color.g+grabpass_color.b)/3;

				fixed4 GrabPass_Gray = fixed4(Gray,Gray,Gray,1);


				//覆蓋色
				fixed4 Overlay = float4( lerp(2*_Overlay_Color.rgb*GrabPass_Gray , 1-2*(1-_Overlay_Color.rgb)*(1-GrabPass_Gray),step(0.5,GrabPass_Gray) ) ,1);
				Overlay = lerp( GrabPass_Gray , Overlay , _Overlay_Color.a );



				//正常色
				fixed4 Fade = tex2D(_FadeTex,i.uv);
			     Fade = lerp( Overlay , fixed4(Fade.rgb,1) , Fade.a*_FadeTex_Alpha);



				 //發光色
				fixed4 Glow = tex2D(_GlowTex,i.uv);
				Glow = Fade+Glow*Glow.a*_GlowTex_Alpha;

				
				 //暗色邊框

				fixed Noise  = tex2D(_GlowTex,i.uv + sin(_Time.y*1)* _Time.y*0.5*float2(1.5,1.5) ).r;
				fixed Noise1 = tex2D(_GlowTex,i.uv + sin(_Time.y*1)* _Time.y*0.5*float2(-2,-2.5) ).r;

				fixed Noise2 = tex2D(_FadeTex,i.uv +  _Time.y*1.5*float2(-2,-0.5)).r;
				fixed Noise3 = tex2D(_FadeTex,i.uv +  _Time.y*1.5*float2(1.5,1.5)).r;

				fixed4 Dark = tex2D(_DarkTex,i.uv + Noise*0.025 + Noise1*0.025  + Noise2*0.5+Noise3*0.5 +1.135);
				 Dark = lerp( Glow , fixed4(Dark.rgb,1) , Dark.a *_DarkTex_Alpha*1.5);


                return fixed4(Dark.rgb,1);
            }
            ENDCG
        }
    }
}
