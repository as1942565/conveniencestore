﻿using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
[AddComponentMenu("PostProcessing/Memory Overlay")]

public class MemoryOverlay : MonoBehaviour {

    public Material OverlayMaterial = null;
    
    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        RenderTexture rt = RenderTexture.GetTemporary(source.width, source.height);
        //rt.DiscardContents();
        
        OverlayMaterial.SetTexture("_RenderTexture", source);

        Graphics.Blit(source, rt, OverlayMaterial, 0);

        Graphics.Blit(rt, destination);

        RenderTexture.ReleaseTemporary(rt);
    }

}
