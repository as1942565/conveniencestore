﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emitter_obj : MonoBehaviour {


    public GameObject obj;
    public GameObject obj_final_pos;

    int amount = 10;

     float interval = 0.1f;

     Vector2 RandomAngelRange = new Vector2(45,-180);

    GameObject[] Obj_array = new GameObject[30];

    GameObject Coins_ObjPool;

    private void Awake()
    {
        //創造物件池
        Coins_ObjPool = new GameObject();
        Coins_ObjPool.name = "Coins_ObjPool";
        Coins_ObjPool.transform.parent = transform;
        Coins_ObjPool.transform.localPosition = new Vector3(0, 0, 0);


        //創造重複使用的物件，並置入物件池
        for (int i = 0; i < amount; i++)
        {
            GameObject GO = GameObject.Instantiate(obj) as GameObject;  //創造重複使用的物件
            GO.transform.parent = Coins_ObjPool.transform;              //置入物件池
            
            GO.GetComponent<SpriteRenderer> ().sortingOrder = 5;

            Obj_array[i] = GO;
        }

    }
    
    private void OnEnable()
    {
        StartCoroutine(Emitter());
    }
    
    IEnumerator Emitter()
    {
        for (int i = 0; i < amount; i++)
        {
            Obj_array[i].transform.localPosition = new Vector3(0, 0, 0);

            Obj_array[i].transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(RandomAngelRange.x, RandomAngelRange.y)));

            float Randomscale = Random.Range(0.25f, 0.3f);
            Obj_array[i].transform.localScale = new Vector3(Randomscale, Randomscale, 0);

            Obj_array[i].SetActive(true);

            yield return new WaitForSeconds(interval);
        }

        yield return new WaitForSeconds(amount* interval);
        obj_final_pos.SetActive(false);

        StopCoroutine(Emitter());
        
        gameObject.SetActive(false);
    }

}
