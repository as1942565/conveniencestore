﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Value_Anime : MonoBehaviour {

    public static Value_Anime self;
    private void Awake ()
    {
        self = this;
        gameObject.SetActive ( false );
        init = true;
    }

    public GameObject Coin_Emitter;

    public Text Value_Text;


    public int Original_Value;
    public int Final_Value;

    private bool init;

    private void OnEnable()
    {
        if ( init == false )
            return;

        i = Original_Value;
        Coin_Emitter.SetActive(true);
    }


    int i=0;
    void Update()
    {
        if (i < Final_Value && Final_Value - Original_Value >= 100)
        {
            i+=(int) (0.02f *(Final_Value - Original_Value));
            Value_Text.text = i.ToString("N0");
        }
        else if(i < Final_Value && Final_Value - Original_Value < 100)
        {
            i += (int)Mathf.Ceil(0.2f *(Final_Value - Original_Value));
            Value_Text.text = i.ToString("N0");
        }
        else
        {
            i = Final_Value;
            Value_Text.text = Final_Value.ToString("N0");
            gameObject.SetActive(false);
            Observer.PlayerStatusPanel.self.Fade ();
        }
    }

    public bool AddMoney ()
    {
        return Original_Value < Final_Value;
    }
    
}
