﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chase_obj : MonoBehaviour {


    public GameObject final_pos;
    public float Forward_Speed;

    float Rotate_Speed;
    float Rotate_Speed_av;

    void Start () {
		
	}
	
	void Update ()
    {
        //角度瞄準目標
        Rotate_Speed = 4 * Forward_Speed;

        Rotate_Speed_av = Rotate_Speed / Vector3.Distance(transform.position, final_pos.transform.position);

        float angle = TwoObjAngel(transform.position, final_pos.transform.position);

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, 0, angle)), Rotate_Speed_av);


        //以y軸前行位移  
        
        transform.Translate(Vector3.up * Forward_Speed);  



        if(Vector3.Distance(transform.position, final_pos.transform.position)<0.2f)
        {
            final_pos.SetActive(true);
            gameObject.SetActive(false);
        }

    }




    //計算兩物件的夾角
    float TwoObjAngel(Vector2 my, Vector2 Target)
    {
        float angle = -1 * Mathf.Atan2(Target.x - my.x, Target.y - my.y) * Mathf.Rad2Deg;

        return angle;
    }

}
