﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Card_Price : MonoBehaviour
{

    [Tooltip ( "是否啟用縮放跳動的效果" )]
    public bool ScaleEffectAnime;

    [Tooltip ( "當前價格" )]
    public int PriceOriginalValue;

    [Tooltip ( "目標價格" )]
    public int PriceTopValue;

    public string Cost;

    [Tooltip ( "建議售價-文字物件" )]
    public Text Price_Text;
    public Text New_Price_Text;

    [Tooltip ( "特效" )]
    public GameObject Price_Text_Effect;

    [Tooltip ( "速度: 如果為零表示每幀+1的速度" )]
    [Range ( 0 , 5f )]
    public float RunSpeed;

    int Speed;
    float OriginalScale;

    public void Click ()
    {
        //速度計算: 依照和目標數值的差異而有所不同
        Speed = (int)( ( PriceTopValue - int.Parse ( Price_Text.text ) ) * RunSpeed / 100 ) + 1;

        //記住文字原本的Scale大小
        OriginalScale = Price_Text.transform.localScale.x;

        //開始數值增加的動畫
        StartCoroutine ( PriceAnime () );
    }


    bool Scale = true;

    [HideInInspector] public bool Finished = false;

    IEnumerator PriceAnime ()
    {
        New_Price_Text.text = PriceOriginalValue.ToString () + " / " + Cost;
        //持續增加數值的動畫開始，Speed是數值的增加速度
        while ( int.Parse ( Price_Text.text ) + Speed < PriceTopValue )
        {
            yield return new WaitForFixedUpdate ();

            Price_Text.text = ( int.Parse ( Price_Text.text ) + Speed ).ToString ();
            New_Price_Text.text = ( int.Parse ( Price_Text.text ) + Speed ).ToString () + " / " + Cost;
            if ( ScaleEffectAnime )
            {
                //文字縮放動畫效果
                if ( Scale )
                {
                    Price_Text.transform.localScale += new Vector3 ( 0.1f , 0.1f , 0 );
                    New_Price_Text.transform.localScale += new Vector3 ( 0.1f , 0.1f , 0 );
                }
                if ( !Scale )
                {
                    Price_Text.transform.localScale -= new Vector3 ( 0.017f , 0.017f , 0 );
                    New_Price_Text.transform.localScale -= new Vector3 ( 0.017f , 0.017f , 0 );
                }
                if ( Price_Text.transform.localScale.x > OriginalScale + 0.1 ) { Scale = false; }
                if ( Price_Text.transform.localScale.x < OriginalScale ) { Scale = true; }
            }
        }
        PlayButtonAudio.self.PlayUpgrade ();

        //增加數值的動畫結束
        if ( int.Parse ( Price_Text.text ) + Speed >= PriceTopValue )
        {
            //數值固定在最大值
            Price_Text.text = PriceTopValue.ToString ();
            New_Price_Text.text = PriceTopValue.ToString () + " / " + Cost;
            Price_Text_Effect.SetActive ( false );
            Price_Text_Effect.SetActive ( true );

        }

        if ( ScaleEffectAnime )
        {
            //文字縮放動畫效果結尾，讓Scale還原到原本大小的動畫
            while ( Price_Text.transform.localScale.x > OriginalScale )
            {
                yield return new WaitForFixedUpdate ();

                Price_Text.transform.localScale -= new Vector3 ( 0.01f , 0.01f , 0 );
                New_Price_Text.transform.localScale -= new Vector3 ( 0.01f , 0.01f , 0 );
            }
        }

        //動畫結束
        Finished = true;
    }
}
