﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card_Star : MonoBehaviour {

    [Tooltip("星星-物件")]
    public GameObject[] Stars;

    [Tooltip("升星特效-物件")]
    public GameObject StarEffect;

    [Tooltip("升級後最高星級(當前星數為此數值-1)")]
    public int LevelTop;

    [Tooltip("動畫速度")]
    public float RunSpeed=1;

    [Tooltip("連續間隔(秒)")]
    public float Interval_second;
    
    float[] OriginalScale = new float[100];

    public void Click ()
    {
        int LevelNow = LevelTop - 1;//當前星數

        for (int i = 0; i < LevelNow; i++)
        {
           Stars[i].SetActive(true);
        }

        //記住星星原本的Scale大小
        for ( int i = LevelNow; i < LevelTop; i++ )
        {
            OriginalScale [i] = Stars [i].transform.localScale.x;
        }
        //開始星星縮放的動畫
        StartCoroutine ( StarAnime () );
    }

    bool Scale = true;

    [HideInInspector] public bool Finished=false;

    IEnumerator StarAnime()
    {
        int LevelNow = LevelTop - 1;//當前星數

        for (int i = LevelNow; i < LevelTop; i++)
        {
            //下一顆星星開啟前的延遲
            yield return new WaitForSeconds(Interval_second);

            //升星特效
            StarEffect.SetActive(false);
            StarEffect.transform.position = Stars[i].transform.position;
            StarEffect.SetActive(true);
            //星星開啟
            Stars[i].SetActive(true);

            while (Stars[i].transform.localScale.x >= OriginalScale[i])
            {
                yield return new WaitForFixedUpdate();

                //星星縮放動畫效果
                if (Scale) { Stars[i].transform.localScale += new Vector3(1f, 1f, 0)* RunSpeed; }

                if (!Scale) { Stars[i].transform.localScale -= new Vector3(0.1f, 0.1f, 0) * RunSpeed; }

                if (Stars[i].transform.localScale.x > OriginalScale[i] + 0.5) { Scale = false; }
            }

            Scale = true;
        }
        
        //動畫結束
        Finished = true;
    }
 }
