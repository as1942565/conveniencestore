﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Card_Familier : MonoBehaviour
{

    [Tooltip ( "Mask-物件" )]
    public GameObject [] Mask;
    [Tooltip ( "原本的圖案:" )]
    public Vector3 Original;
    [Tooltip ( "結果的圖案:" )]
    public Vector3 result;
    [Tooltip ( "速度曲線" )]
    public AnimationCurve waveform;

    [Tooltip ( "動態模糊的圖" )]
    public Sprite WhenSpeed_Sprite;

    float S1;
    float S2;
    float S3;

    void Start ()
    {
        Original.x = CorrespondValue ( Original.x );
        Original.y = CorrespondValue ( Original.y );
        Original.z = CorrespondValue ( Original.z );

        Original.x = FixedToValue ( Original.x );
        Original.y = FixedToValue ( Original.y );
        Original.z = FixedToValue ( Original.z );


        for ( int i = 0; i < Mask.Length; i++ )
        {
            float ResultValue = i;

            switch ( i )
            {
                case 0: ResultValue = Original.x; break;
                case 1: ResultValue = Original.y; break;
                case 2: ResultValue = Original.z; break;
            }

            Mask [i].transform.GetChild ( 0 ).transform.localPosition = new Vector3 ( 0 , ResultValue , 0 );
            Mask [i].transform.GetChild ( 1 ).transform.localPosition = new Vector3 ( 0 , Mask [i].transform.GetChild ( 0 ).transform.localPosition.y + 350 , 0 );
        }
    }


    //輸入等級數值對應到腳本內的設定值1~5
    int CorrespondValue ( float i )
    {
        switch ( (int)i )
        {
            case 0: i = 4; break;
            case 1: i = 5; break;
            case 2: i = 1; break;
            case 3: i = 3; break;
            case 4: i = 2; break;
        }
        return (int)i;
    }


    //數值對應到座標
    int FixedToValue ( float i )
    {
        switch ( (int)i )
        {
            case 1: i = -140; break;
            case 2: i = -70; break;
            case 3: i = 0; break;
            case 4: i = 70; break;
            case 5: i = 140; break;
        }
        return (int)i;
    }


    public void Click ()
    {
        result.x = CorrespondValue ( result.x );
        result.y = CorrespondValue ( result.y );
        result.z = CorrespondValue ( result.z );

        result.x = FixedToValue ( result.x );
        result.y = FixedToValue ( result.y );
        result.z = FixedToValue ( result.z );

        StartCoroutine ( InOrderPlay () );
    }



    IEnumerator InOrderPlay ()
    {
        StartCoroutine ( FamilierAnime_0 () );

        yield return new WaitForSeconds ( 0.3f );

        StartCoroutine ( FamilierAnime_1 () );

        yield return new WaitForSeconds ( 0.3f );

        StartCoroutine ( FamilierAnime_2 () );
    }




    //bool Day_Mask_Finished;
    //bool Night_Mask_Finished;

    [HideInInspector] public bool Finished;


    IEnumerator FamilierAnime_0 ()
    {
        GameObject A = Mask [0].transform.GetChild ( 0 ).gameObject;
        GameObject B = Mask [0].transform.GetChild ( 1 ).gameObject;

        Sprite A_OriginalSprite = A.GetComponent<Image> ().sprite;
        Sprite B_OriginalSprite = B.GetComponent<Image> ().sprite;

        Image A_Sprite = A.GetComponent<Image> ();
        Image B_Sprite = B.GetComponent<Image> ();

        bool SlowDown = false;
        float Speed = 0;
        float time = 0;

        //相對的結果值
        float ResultValue = result.x;

        Mask [0].transform.GetChild ( 3 ).gameObject.SetActive ( true );
        //開始轉拉霸動畫
        while ( !SlowDown )
        {
            Run ( Speed , A , B , ResultValue );

            yield return new WaitForFixedUpdate ();

            time++;

            Speed = waveform.Evaluate ( time++ );

            //換成動態模糊的圖
            if ( time > 20 )
            {
                A_Sprite.sprite = WhenSpeed_Sprite;
                B_Sprite.sprite = WhenSpeed_Sprite;
            }

            //一段時間後固定慢速度
            if ( time >= 210 )
            {
                Speed = 1; //穩定速度
                           //  Speed = _Speed(A, B, ResultValue);
                           //  S1 = Speed;
                           //歸位成以5為一個單位，最後以5為單位去對齊結果
                int AposY = (int)( A.transform.localPosition.y );
                int BposY = (int)( A.transform.localPosition.y );
                A.transform.localPosition = new Vector3 ( 0 , AposY - AposY % ( Speed * 10 ) + Speed * 10 , 0 );
                B.transform.localPosition = new Vector3 ( 0 , BposY - BposY % ( Speed * 10 ) + Speed * 10 , 0 );

                if ( A.transform.localPosition.y == B.transform.localPosition.y && A.transform.localPosition.y > 0 )
                {
                    A.transform.localPosition = new Vector3 ( 0 , B.transform.localPosition.y - 350 , 0 );
                }
                if ( A.transform.localPosition.y == B.transform.localPosition.y && A.transform.localPosition.y < 0 )
                {
                    A.transform.localPosition = new Vector3 ( 0 , B.transform.localPosition.y + 350 , 0 );
                }

                //進入下面的while迴圈，讓速度慢下來並穩定
                SlowDown = true;
            }
        }

        //速度慢下來並穩定
        while ( SlowDown )
        {
            time++;
            yield return new WaitForFixedUpdate ();

            //換成原本的圖
            if ( time > 60 )
            {
                A_Sprite.sprite = A_OriginalSprite;
                B_Sprite.sprite = B_OriginalSprite;
            }

            if ( Run ( Speed , A , B , ResultValue ) || time > 400 ) //用time來防止Bug發生
            {
                SlowDown = false;
            }
        }

        //將圖案做定位，防止ㄧ些Bug
        A.transform.localPosition = new Vector3 ( 0 , ResultValue , 0 );
        B.transform.localPosition = new Vector3 ( 0 , A.transform.localPosition.y - 350 , 0 );

        //發光特效開啟
        Mask [0].transform.GetChild ( 2 ).gameObject.SetActive ( true );
        //速度線特效關閉
        Mask [0].transform.GetChild ( 3 ).gameObject.SetActive ( false );

        //Day_Mask_Finished = true;
    }



    IEnumerator FamilierAnime_1 ()
    {
        GameObject A = Mask [1].transform.GetChild ( 0 ).gameObject;
        GameObject B = Mask [1].transform.GetChild ( 1 ).gameObject;

        Sprite A_OriginalSprite = A.GetComponent<Image> ().sprite;
        Sprite B_OriginalSprite = B.GetComponent<Image> ().sprite;

        Image A_Sprite = A.GetComponent<Image> ();
        Image B_Sprite = B.GetComponent<Image> ();

        bool SlowDown = false;
        float Speed = 0;
        float time = 0;

        //相對的結果值
        float ResultValue = result.y;

        Mask [1].transform.GetChild ( 3 ).gameObject.SetActive ( true );
        //開始轉拉霸動畫
        while ( !SlowDown )
        {
            Run ( Speed , A , B , ResultValue );

            yield return new WaitForFixedUpdate ();

            time++;

            Speed = waveform.Evaluate ( time++ );

            //換成動態模糊的圖
            if ( time > 20 )
            {
                A_Sprite.sprite = WhenSpeed_Sprite;
                B_Sprite.sprite = WhenSpeed_Sprite;
            }
            //一段時間後固定慢速度
            if ( time >= 210 )
            {
                Speed = 1; //穩定速度
                           // Speed = _Speed(A, B, ResultValue);
                           // S2 = Speed;
                           //歸位成以5為一個單位，最後以5為單位去對齊結果
                int AposY = (int)( A.transform.localPosition.y );
                int BposY = (int)( A.transform.localPosition.y );
                A.transform.localPosition = new Vector3 ( 0 , AposY - AposY % ( Speed * 10 ) + Speed * 10 , 0 );
                B.transform.localPosition = new Vector3 ( 0 , BposY - BposY % ( Speed * 10 ) + Speed * 10 , 0 );

                if ( A.transform.localPosition.y == B.transform.localPosition.y && A.transform.localPosition.y > 0 )
                {
                    A.transform.localPosition = new Vector3 ( 0 , B.transform.localPosition.y - 350 , 0 );
                }
                if ( A.transform.localPosition.y == B.transform.localPosition.y && A.transform.localPosition.y < 0 )
                {
                    A.transform.localPosition = new Vector3 ( 0 , B.transform.localPosition.y + 350 , 0 );
                }

                //進入下面的while迴圈，讓速度慢下來並穩定
                SlowDown = true;
            }
        }

        //速度慢下來並穩定
        while ( SlowDown )
        {
            time++;
            yield return new WaitForFixedUpdate ();

            //換成原本的圖
            if ( time > 60 )
            {
                A_Sprite.sprite = A_OriginalSprite;
                B_Sprite.sprite = B_OriginalSprite;
            }
            if ( Run ( Speed , A , B , ResultValue ) || time > 400 ) //用time來防止Bug發生
            {
                SlowDown = false;
            }
        }

        //將圖案做定位，防止ㄧ些Bug
        A.transform.localPosition = new Vector3 ( 0 , ResultValue , 0 );
        B.transform.localPosition = new Vector3 ( 0 , A.transform.localPosition.y - 350 , 0 );

        //發光特效開啟
        Mask [1].transform.GetChild ( 2 ).gameObject.SetActive ( true );
        //速度線特效關閉
        Mask [1].transform.GetChild ( 3 ).gameObject.SetActive ( false );

        //Night_Mask_Finished = true;
    }



    IEnumerator FamilierAnime_2 ()
    {
        GameObject A = Mask [2].transform.GetChild ( 0 ).gameObject;
        GameObject B = Mask [2].transform.GetChild ( 1 ).gameObject;

        Sprite A_OriginalSprite = A.GetComponent<Image> ().sprite;
        Sprite B_OriginalSprite = B.GetComponent<Image> ().sprite;

        Image A_Sprite = A.GetComponent<Image> ();
        Image B_Sprite = B.GetComponent<Image> ();

        bool SlowDown = false;
        float Speed = 0;
        float time = 0;

        //相對的結果值
        float ResultValue = result.z;

        Mask [2].transform.GetChild ( 3 ).gameObject.SetActive ( true );
        //開始轉拉霸動畫
        while ( !SlowDown )
        {
            Run ( Speed , A , B , ResultValue );

            yield return new WaitForFixedUpdate ();

            time++;

            Speed = waveform.Evaluate ( time++ );

            //換成動態模糊的圖
            if ( time > 20 )
            {
                A_Sprite.sprite = WhenSpeed_Sprite;
                B_Sprite.sprite = WhenSpeed_Sprite;
            }
            //一段時間後固定慢速度
            if ( time >= 210 )
            {
                Speed = 1f; //穩定速度
                            // Speed = _Speed(A, B, ResultValue);
                            // S3 = Speed;

                //歸位成以5為一個單位，最後以5為單位去對齊結果
                int AposY = (int)( A.transform.localPosition.y );
                int BposY = (int)( A.transform.localPosition.y );
                A.transform.localPosition = new Vector3 ( 0 , AposY - AposY % ( Speed * 10 ) + Speed * 10 , 0 );
                B.transform.localPosition = new Vector3 ( 0 , BposY - BposY % ( Speed * 10 ) + Speed * 10 , 0 );

                if ( A.transform.localPosition.y == B.transform.localPosition.y && A.transform.localPosition.y > 0 )
                {
                    A.transform.localPosition = new Vector3 ( 0 , B.transform.localPosition.y - 350 , 0 );
                }
                if ( A.transform.localPosition.y == B.transform.localPosition.y && A.transform.localPosition.y < 0 )
                {
                    A.transform.localPosition = new Vector3 ( 0 , B.transform.localPosition.y + 350 , 0 );
                }

                //進入下面的while迴圈，讓速度慢下來並穩定
                SlowDown = true;
            }
        }

        //速度慢下來並穩定
        while ( SlowDown )
        {
            time++;
            yield return new WaitForFixedUpdate ();

            //換成原本的圖
            if ( time > 60 )
            {
                A_Sprite.sprite = A_OriginalSprite;
                B_Sprite.sprite = B_OriginalSprite;
            }
            if ( Run ( Speed , A , B , ResultValue ) || time > 400 ) //用time來防止Bug發生
            {
                SlowDown = false;
            }
        }

        //將圖案做定位，防止ㄧ些Bug
        A.transform.localPosition = new Vector3 ( 0 , ResultValue , 0 );
        B.transform.localPosition = new Vector3 ( 0 , A.transform.localPosition.y - 350 , 0 );

        //發光特效開啟
        Mask [2].transform.GetChild ( 2 ).gameObject.SetActive ( true );
        //速度線特效關閉
        Mask [2].transform.GetChild ( 3 ).gameObject.SetActive ( false );

        Finished = true;
    }

    bool Run ( float Speed , GameObject A , GameObject B , float Value )
    {
        A.transform.localPosition -= new Vector3 ( 0 , 10 , 0 ) * Speed;
        B.transform.localPosition -= new Vector3 ( 0 , 10 , 0 ) * Speed;

        if ( A.transform.localPosition.y < -210 )
        {
            A.transform.localPosition = new Vector3 ( 0 , B.transform.localPosition.y + 350 , 0 );
        }
        if ( B.transform.localPosition.y < -210 )
        {
            B.transform.localPosition = new Vector3 ( 0 , A.transform.localPosition.y + 350 , 0 );
        }

        bool stop = false;

        //位置歸位接近結果位置時，本函式回傳true
        if ( Mathf.Abs ( A.transform.localPosition.y - Value ) <= Speed || Mathf.Abs ( B.transform.localPosition.y - Value ) <= Speed )
        {
            stop = true;
        }

        return stop;
    }

    /*
    float _Speed( GameObject A, GameObject B, float Value )
    {
        float speed=0.5f;
        
        if (A.transform.localPosition.y < B.transform.localPosition.y)
        {
            if (A.transform.localPosition.y + Value >= 0)
            {
                speed = (Value + A.transform.localPosition.y) / 1;
            }

            if (A.transform.localPosition.y + Value <= 0)
            {
                speed = (Value + B.transform.localPosition.y ) / 1;
            }
        }

        if (B.transform.localPosition.y < A.transform.localPosition.y)
        {
            if (B.transform.localPosition.y + Value >= 0)
            {
                speed = (Value + B.transform.localPosition.y) / 1;
            }

            if (B.transform.localPosition.y + Value <= 0)
            {
                speed = (Value + A.transform.localPosition.y ) / 1;
            }
        }

        speed = (Mathf.Round(speed * 100)) / 100 ;
        
        return speed;
    }*/

}