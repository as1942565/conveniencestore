﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button_Click : MonoBehaviour
{

    [Tooltip ( "間隔(秒)" )]
    public float Interval = 0.5f;

    Card_Star _Card_Star;
    Card_Price _Card_Price;
    Card_Familier _Card_Familier;

    void Start ()
    {
        _Card_Star = GetComponent<Card_Star> ();
        _Card_Familier = GetComponent<Card_Familier> ();
        _Card_Price = GetComponent<Card_Price> ();
    }

    public void Click ()
    {
        StartCoroutine ( InOrderClick () );
    }


    //依序執行每個部份的Click()
    IEnumerator InOrderClick ()
    {

        //進入Card_Star的動畫
        _Card_Star.Click ();

        //Card_Star尚未結束動畫，持續等待
        while ( !_Card_Star.Finished ) { yield return new WaitForFixedUpdate (); }

        //間隔延遲
        yield return new WaitForSeconds ( Interval );


        //Card_Star動畫結束，進入Card_Familier的動畫
        _Card_Familier.Click ();

        //Card_Familier尚未結束動畫，持續等待
        while ( !_Card_Familier.Finished ) { yield return new WaitForFixedUpdate (); }

        //間隔延遲
        yield return new WaitForSeconds ( Interval );

        //Card_Familier動畫結束，進入Card_Price的動畫
        _Card_Price.Click ();

    }
}
