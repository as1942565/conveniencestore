﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkipAllAnime : MonoBehaviour
{
    bool allowSkip = false;

    public GameObject Card_BezierFollow;

    public GameObject Card;
    public string Anime_name;
    Animation Card_Animation;

    Animation Mailer_Animation;

    private void Start ()
    {
        Card_Animation = Card.GetComponent<Animation> ();
        Mailer_Animation = GetComponent<Animation> ();

        GlowEffect = Card_BezierFollow.transform.GetChild ( 0 ).gameObject;
    }

    GameObject GlowEffect;
    public void Skip ()
    {
        if ( allowSkip == false )
            return;
        GlowEffect.transform.parent = Card.transform;
        GlowEffect.transform.localScale = new Vector3 ( 1 , 1 , 1 );
        GlowEffect.transform.localPosition = new Vector3 ( 0 , 0 , 0 );
        GlowEffect.transform.localRotation = Quaternion.Euler ( 0 , 0 , 0 );

        Card_BezierFollow.SetActive ( false );
        Card.GetComponent<Button> ().enabled = false;
        Card.SetActive ( true );
        Card_Animation.Play ( "CardFlip" );
        Card_Animation ["CardFlip"].time = 1;
        gameObject.SetActive ( false );
        allowSkip = false;
    }
    public void AllowSkip()
    {
        allowSkip = true;
    }
    public void NotAllowSkip()
    {
        allowSkip = false;
    }
}
