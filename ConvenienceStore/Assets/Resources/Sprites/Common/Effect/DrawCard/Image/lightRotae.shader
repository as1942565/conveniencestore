﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/UV rotation"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Speed("Speed", float) = 1
	}
	SubShader
	{
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Blend SrcAlpha One
 
	Pass
	{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#include "UnityCG.cginc"
 
        struct appdata
        {
            float4 vertex : POSITION;
            float2 uv : TEXCOORD0;
			fixed4 color : COLOR;
        };
		struct v2f 
		{
			float4 pos : SV_POSITION;
			float2 uv : TEXCOORD0;
			fixed4 color : COLOR;
		};
 
	float _Speed;

	v2f vert(appdata v)
	{
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
 
		float2 pivot = float2(0.5, 0.5);         //中心点
 
		float cosAngle = cos(_Time.y*_Speed);
		float sinAngle = sin(_Time.y*_Speed);
		float2x2 rot = float2x2(cosAngle, -sinAngle, sinAngle, cosAngle);  //确定以z轴旋转
		
		float2 uv = v.uv.xy - pivot;
		o.uv = mul(rot, uv);           //旋转矩阵相乘变换坐标
		o.uv += pivot;
		o.color = v.color;

		return o;
	}
 
	sampler2D _MainTex;
 
	fixed4 frag(v2f i) : SV_Target
	{
 
		return tex2D(_MainTex, i.uv)*i.color;
	}
 
		ENDCG
	}
	}
}
