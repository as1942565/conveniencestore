﻿Shader "Unlit/Card_Glow"
{
	Properties
	{
		[Header(_________The card face_________)] [Space] [Space]
		_MainTexBack ("Card Glow", 2D) = "white" {}
		
		[Header(__________Skew Effect__________)] [Space] [Space]
        _Skew("Skew Horizontal", Range(-1,1)) = 0
        _Skew2("Skew Vertical", Range(-1,1)) = 0
		
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" }
		ZWrite Off
		Blend SrcAlpha One
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
                float4 color    : COLOR;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
                float4 color    : COLOR;
			};

			sampler2D _MainTexBack;
			float4 _MainTexBack_ST;

			float _Skew;
			float _Skew2;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.uv = TRANSFORM_TEX(v.uv, _MainTexBack);
				o.vertex = UnityObjectToClipPos(float4( v.vertex.x, v.vertex.y,v.vertex.z ,v.vertex.w));
				o.color = v.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				
				fixed4 col = tex2D(_MainTexBack, float2(
				i.uv.x -  ((i.uv.y) - step(0,_Skew2)) *_Skew2*(i.uv.x-0.5) ,
				i.uv.y -  ((i.uv.x) - step(0,_Skew)) *_Skew*(i.uv.y-0.5) ));

				return col*i.color ;
			}
			ENDCG
		}
	}
}
