﻿Shader "Unlit/Card"
{
	Properties
	{
		[Header(_________The card face_________)] [Space] [Space]
		_MainTex ("Card Front", 2D) = "white" {}
		_MainTexBack ("Card Back", 2D) = "white" {}
		
		[Header(__________Skew Effect__________)] [Space] [Space]
        _Skew("Skew Horizontal", Range(-1,1)) = 0
        _Skew2("Skew Vertical", Range(-1,1)) = 0
		
		[Header(__________Slide Reflect__________)] [Space] [Space]
		_Reflect_Color("Reflect Color",Color)=(1,1,1,1)
		_Reflect_Pos("Reflect Pos",Range(-2.5,2.5))=0
		_Reflect_Width("Reflect Width",Range(1,2.5))=1.5
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" }
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Cull Off

		LOD 100

		Pass
		{
			//正面
			Cull Back
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
                float4 color    : COLOR;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
                float4 color    : COLOR;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			float _Skew;
			float _Skew2;

			v2f vert (appdata v)
			{
				v2f o;
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.vertex = UnityObjectToClipPos(float4( v.vertex.x, v.vertex.y,v.vertex.z ,v.vertex.w));
				o.color = v.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				
				fixed4 col = tex2D(_MainTex, float2(
				i.uv.x -  ((i.uv.y) - step(0,_Skew2)) *_Skew2*(i.uv.x-0.5) ,
				i.uv.y -  ((i.uv.x) - step(0,_Skew)) *_Skew*(i.uv.y-0.5) ));

				return col*i.color;
			}
			ENDCG
		}

		
		Pass
		{
			//背面
			Cull Front
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
                float4 color    : COLOR;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
                float4 color    : COLOR;
			};

			sampler2D _MainTexBack;
			float4 _MainTexBack_ST;

			float _Skew;
			float _Skew2;
			
			fixed4 _Reflect_Color;
			float _Reflect_Pos;
			float _Reflect_Width;


			v2f vert (appdata v)
			{
				v2f o;
				o.uv = TRANSFORM_TEX(v.uv, _MainTexBack);
				o.vertex = UnityObjectToClipPos(float4( v.vertex.x, v.vertex.y,v.vertex.z ,v.vertex.w));
				o.color = v.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				
				fixed4 col = tex2D(_MainTexBack, float2(
				i.uv.x -  ((i.uv.y) - step(0,_Skew2)) *_Skew2*(i.uv.x-0.5) ,
				i.uv.y -  ((i.uv.x) - step(0,_Skew)) *_Skew*(i.uv.y-0.5) ));

				float LightRange = i.uv.x -abs(_SinTime.y)*20+10 + i.uv.y;

				fixed4 FlashColor = _Reflect_Color.a * fixed4(1,1,1,1) *step( LightRange ,_Reflect_Width) *step(2- LightRange,_Reflect_Width)  
				* clamp(pow(LightRange,5*_Reflect_Width),0,1);

				return col*i.color + fixed4(FlashColor.rgb*col.a,0)*_Reflect_Color;
			}
			ENDCG
		}
	}
}
