﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]

public class CardSkew : MonoBehaviour
{
    [Range(-1, 1)]
    public float Skew_Horizontal;

    [Range(-1, 1)]
    public float Skew_Vertical;
    
    FollowBezier _FollowBezier;

    Material _Material;

    float OriScale;

    void Start ()
    {
        if (GetComponent<FollowBezier>())
        {
            _FollowBezier = GetComponent<FollowBezier>();
        }

        _Material = GetComponent<Image>().material;
        OriScale = transform.localScale.y;
    }
	
	void Update ()
    {
        if (_FollowBezier)
        {
            Skew_Horizontal = Mathf.Clamp(Vector3.Angle(Vector3.down, _FollowBezier.NextDir) / 270, 0, 1);


            Skew_Vertical = -Mathf.Clamp(Skew_Horizontal * 1.5f, 0, 1);

            transform.localScale = new Vector3(-OriScale * (1 + Skew_Vertical * 0.6f), OriScale, 1);

            Skew_Vertical = Mathf.Clamp(Skew_Horizontal * 0.5f, 0, 1);
        }

        _Material.SetFloat("_Skew", Skew_Horizontal);
        _Material.SetFloat("_Skew2", Skew_Vertical);
    }
    
}
