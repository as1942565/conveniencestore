﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Card_Content : MonoBehaviour
{
    #region
    /*[Header("卡片級別(左上角的)")]
    public Sprite Level;

    [Header("卡片商品名稱")]
    public string Name_Text;

    [Header("卡片商品的圖")]
    public Sprite Commodity;

    [Header("受歡迎程度")]
    public Vector3 Familier;
    
    [Header("卡片星等")]
    public int Star;

    [Header("建議售價")]
    public string Price_Text;

    [Header("成本")]
    public string Cost_Text;
    
    [Header("商品詳細內容")]
    [TextArea(4,4)]
    public string Content_Text;*/
    #endregion
    public GameObject Card;
    public Prefab.ResearchPrefab research;

    public Image RankImage;
    public Text NameText;
    public Image IconImage;
    public Image TypeImage;
    public Image [] PopularImage;
    public GameObject [] Stars;
    public Text PriceText;
    public Text Explanation;

    public Material m;

    void Awake ()
    {
        Texture texture = Resources.Load<Texture> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameUI/Commodity/Commodity/Background" );
        m.SetTexture ( "_MainTex" , texture );

        #region
        /*//卡片級別
        transform.GetChild ( 0 ).GetComponent<Image> ().sprite = Level;

        //卡片商品名稱
        transform.GetChild ( 1 ).GetComponent<Text> ().text = Name_Text;

        //卡片商品的圖
        transform.GetChild ( 2 ).GetComponent<Image> ().sprite = Commodity;

        //受歡迎程度
        FamilierSet ();

        //星等
        for ( int i = 0; i < Star; i++ )
        {
            transform.GetChild ( 4 ).GetChild ( i ).gameObject.SetActive ( true );
        }

        //建議售價、成本
        transform.GetChild ( 5 ).GetChild ( 0 ).GetComponent<Text> ().text = Price_Text;
        transform.GetChild ( 5 ).GetChild ( 1 ).GetComponent<Text> ().text = Cost_Text;

        //商品詳細內容
        transform.GetChild ( 6 ).GetComponent<Text> ().text = Content_Text;*/
        #endregion
        int id = research.commodityID;
        GameData.Commodity commodity = GameLoopManager.instance.gm.commodityList.Data [id];
        int type = (int)commodity.type;
        int rank = commodity.rank;
        //string name = commodity.name;
        string name = Localizer.Translate ( commodity.name );
        int star = commodity.star;
        string sale = commodity.price[star].ToString ();
        string cost = commodity.cost.ToString ();
        //string explanation = commodity.explanation;
        string explanation = Localizer.Translate ( commodity.explanation );

        RankImage.sprite = SpritePath.CommodityRank [rank];
        NameText.text = name;
        IconImage.sprite = SpritePath.Commodity [id];
        TypeImage.sprite = SpritePath.CommodityType [type];
        for ( int i = 0; i < PopularImage.Length; i++ )
        {
            int popular = (int)commodity.salesRate [i];
            PopularImage [i].sprite = SpritePath.CommodityPopular_W [popular];
        }
        for ( int i = 0; i < Stars.Length; i++ )
        {
            Stars [i].SetActive ( star > i );
        }
        PriceText.text = sale + " / " + cost;
        Explanation.text = explanation;
    }

    public void Click ()
    {
        LoadingScene.self.Transitions ( 0.5f , 1f , true );
        StartCoroutine ( "Special" );
    }

    IEnumerator Special ()
    {
        yield return new WaitForSeconds ( 0.5f );
        int id = research.commodityID;
        if ( id == 50 )
        {
            PlayerDataManager pm = GameLoopManager.instance.pm;
            GameDataManager gm = GameLoopManager.instance.gm;

            pm.businessUnlock.SetUnlock ( 64 , 0 , true );
            pm.businessUnlock.SetMode ( 64 , BusinessUnlock.state.watch );
            GameLoopManager.instance.om.unlockActorData.SetUnlock ( 25 , true );
            GameLoopManager.instance.om.unlockCGData.SetUnlock ( 3 , 0 , true );
            Action.HireEmployee.Run ( 21 , pm , gm );

            if ( StoryManager.self != null )
            {
                string language = GameLoopManager.instance.om.optionData.GetLanguageStr ();
                StoryManager.self.StartStory ( $"Data/Story/{language}H/H_Employee3/" , "H_Employee3_EX01" );
                Achievement.SetAchievement ( Achievement.achievementNames.Actor6.ToString () );
                StoryManager.self.ReadActivityDrama = true;
            }
            else
                StoryManager.self.ReadActivityDrama = false;
        }
        while ( StoryManager.self.ReadActivityDrama == true )
        {
            yield return null;
        }

        yield return new WaitForSeconds ( 0.5f );
        Close ();
    }

    void Close ()
    {
        BaseKeyBoard.Ban = false;
        int id = research.commodityID;
        Action.Research.Run ( id , GameLoopManager.instance.nextShift );
        Destroy ( Card );

        Calender.Shift shift = GameLoopManager.instance.pm.calender.GetShift ();
        if ( shift == Calender.Shift.morning )
        {
            UI.BuildLog.self.OnClosed += OnOpenResearch;
        }
        else
        {
            OnOpenResearch ();
        }
    }
    private const string path = "GameUICanvas/MainAction/BuildCommodity";
    void OnOpenResearch ()
    {
        UI.BuildLog.self.OnClosed -= OnOpenResearch;
        if ( StoryManager.self.EnableTeach == true )
            return;

        UI.MainPanel.self.SwitchPanel ( 0 );
    }

    //受歡迎程度設置
    /*
    void FamilierSet ()
    {
        Familier.x = CorrespondValue ( Familier.x );
        Familier.y = CorrespondValue ( Familier.y );
        Familier.z = CorrespondValue ( Familier.z );

        Familier.x = FixedToValue ( Familier.x );
        Familier.y = FixedToValue ( Familier.y );
        Familier.z = FixedToValue ( Familier.z );

        for ( int i = 0; i < 3; i++ )
        {
            float ResultValue = i;

            switch ( i )
            {
                case 0: ResultValue = Familier.x; break;
                case 1: ResultValue = Familier.y; break;
                case 2: ResultValue = Familier.z; break;
            }

            transform.GetChild ( 3 ).GetChild ( i ).GetChild ( 0 ).transform.localPosition = new Vector3 ( 0 , ResultValue , 0 );
            //transform.GetChild(3).GetChild(i).GetChild(1).transform.localPosition = new Vector3(0, transform.GetChild(i).transform.GetChild(0).transform.localPosition.y + 350, 0);
        }
    }
    */

    //輸入等級數值對應到腳本內的設定值1~5
    int CorrespondValue ( float i )
    {
        switch ( (int)i )
        {
            case 0: i = 4; break;
            case 1: i = 5; break;
            case 2: i = 1; break;
            case 3: i = 3; break;
            case 4: i = 2; break;
        }
        return (int)i;
    }

    //數值對應到座標
    int FixedToValue ( float i )
    {
        switch ( (int)i )
        {
            case 1: i = -140; break;
            case 2: i = -70; break;
            case 3: i = 0; break;
            case 4: i = 70; break;
            case 5: i = 140; break;
        }
        return (int)i;
    }
}
