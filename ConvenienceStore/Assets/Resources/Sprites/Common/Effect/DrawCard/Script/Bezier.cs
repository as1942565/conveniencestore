﻿using UnityEngine;
using System.Collections.Generic;
[RequireComponent ( typeof ( LineRenderer ) )]
[ExecuteInEditMode]

public class Bezier : MonoBehaviour
{
    //其他腳本呼叫使用，存放線上所有的點
    [HideInInspector] public Vector3 [] AllPoints = new Vector3 [130];

    //其他腳本呼叫使用，線上所有點的數量，越多線就越平滑，階數*Smooth個
    [HideInInspector] public int _segmentNum;


    [Range ( 2 , 13 )]
    public int N_num = 5; //階數

    public bool IsDrawPointConnect = true; //顯示控制點的連線，凸包範圍邊線

    public int Smooth = 10; //平滑度，表示控制點之間的線段內有多少個點

    [Range ( 0 , 13 )]
    public float Weight; //曲度

    public LineRenderer lineRenderer;


    //int layerOrder;

    Transform [] controlPoints = new Transform [13];

    void Awake ()
    {
        if ( !lineRenderer )
        {
            lineRenderer = GetComponent<LineRenderer> ();
        }
        //lineRenderer.sortingLayerID = layerOrder;
        lineRenderer.sortingLayerID = 0;

        //子物件放入陣列
        for ( int i = 0; i < transform.childCount; i++ )
        {
            controlPoints [i] = transform.GetChild ( i ).transform;

            //添加圖示
            if ( Application.platform == RuntimePlatform.WindowsEditor )
            {
                IconManager.SetIcon ( transform.GetChild ( i ).gameObject , IconManager.Icon.CircleRed );  //手機無法使用空物件圖示
            }
        }

        //曲線上的所有點數
        _segmentNum = N_num * Smooth;
    }

    [ContextMenu ( "重設" )]
    void _Reset ()
    {
        //先清空所有子物件
        while ( transform.childCount > 0 )
        {
            DestroyImmediate ( transform.GetChild ( 0 ).gameObject );
        }

        //產生新一批的子物件
        for ( int i = 0; i < N_num; i++ )
        {
            GameObject NewGo = new GameObject ();
            NewGo.name = "Point " + ( i + 1 ).ToString ();
            NewGo.transform.parent = transform;
        }

        //子物件放入陣列
        for ( int i = 0; i < N_num; i++ )
        {
            controlPoints [i] = transform.GetChild ( i ).transform;

            //添加圖示
            if ( Application.platform == RuntimePlatform.WindowsEditor )
            {
                IconManager.SetIcon ( transform.GetChild ( i ).gameObject , IconManager.Icon.CircleRed );  //手機無法使用空物件圖示
            }
        }

        //重置歸零
        _segmentNum = 0;
        //曲線上的所有點數
        _segmentNum = N_num * Smooth;
    }

    void Update ()
    {
        DrawCurve ();

        DrawPointConnect ();
    }

    //繪製控制點的連線
    void DrawPointConnect ()
    {
        if ( IsDrawPointConnect )
        {
            for ( int i = 1; i < N_num; i++ )
            {
                if ( controlPoints [i - 1] && controlPoints [i] )
                {
                    Debug.DrawLine ( controlPoints [i - 1].position , controlPoints [i].position , new Color32 ( 255 , 255 , 255 , 80 ) );
                }
            }
        }
    }

    //用LineRender繪製貝茲曲線
    void DrawCurve ()
    {
        for ( int i = 1; i <= _segmentNum; i++ )
        {
            float t = i / (float)_segmentNum;

            Vector3 pixel = BezierCalculate ( t , controlPoints );

            lineRenderer.positionCount = i;
            lineRenderer.SetPosition ( i - 1 , pixel );

            AllPoints [i - 1] = pixel; //存放線上所有的點，放到存放陣列裡給其他腳本呼叫使用
        }

    }

    //有理貝茲曲線-點
    Vector3 BezierCalculate ( float t , Transform [] P )
    {
        //暫存結果: 分子
        Vector3 p = new Vector3 ( 0 , 0 , 0 );


        //暫存結果: 權重分母
        float pw = 0;

        //階數
        int n = N_num - 1;

        for ( int i = 0; i < N_num; i++ )
        {
            if ( P [i] )
            {
                if ( i == 0 || i == N_num - 1 )
                {
                    p += Combination ( n , i ) * Mathf.Pow ( 1 - t , n - i ) * Mathf.Pow ( t , i ) * P [i].position * 1;
                    pw += Combination ( n , i ) * Mathf.Pow ( 1 - t , n - i ) * Mathf.Pow ( t , i ) * 1;
                }
                else
                {
                    p += Combination ( n , i ) * Mathf.Pow ( 1 - t , n - i ) * Mathf.Pow ( t , i ) * P [i].position * Mathf.Clamp ( Weight , 0 , n - 1 );
                    pw += Combination ( n , i ) * Mathf.Pow ( 1 - t , n - i ) * Mathf.Pow ( t , i ) * Mathf.Clamp ( Weight , 0 , n - 1 );
                }
            }
        }
        p /= pw;
        return p;
    }

    //組合
    int Combination ( int n , int k )
    {
        int Cnk = 0;

        Cnk = factorial ( n ) / ( factorial ( k ) * factorial ( n - k ) );

        return Cnk;
    }
    //階乘
    int factorial ( int I )
    {
        if ( I == 0 ) { I = 1; } //零的階乘為1

        for ( int i = I - 1; i > 0; i-- ) { I *= i; }

        return I;
    }
}