﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchAnime : MonoBehaviour
{
    Animation anim;

    public string Anime_name;
    public GameObject SkipObj;

    private void Awake ()
    {
        anim = GetComponent<Animation> ();
    }
    
    public void Click ()
    {
        anim.Play ( Anime_name );
        if ( Anime_name == "CardFlip" )
        {
            transform.localPosition = Vector3.zero;
            PlayButtonAudio.self.PlayReaserch ();
        }
        GetComponent<Button> ().enabled = false;
    }

    public Animation nextAnime;
    public string nextAnime_name;

    public void NextAnime ()
    {
        nextAnime.Play ( nextAnime_name );
        if ( SkipObj != null )
            SkipObj.SetActive ( true );
    }
}
