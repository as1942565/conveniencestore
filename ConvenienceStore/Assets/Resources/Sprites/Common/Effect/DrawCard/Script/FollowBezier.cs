﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBezier : MonoBehaviour {

    public Bezier _Bezier;

    [Range(0,1)]
    public float Speed;

    [Tooltip("速度曲線")]
    public AnimationCurve Speedwaveform;

    int i;

    [HideInInspector]
    public Vector3 NextDir;

    AimeObjActive _AimeObjActive;

    ParticleSystem myParticleSystem;
    ParticleSystem.EmissionModule emissionModule;

    GameObject GlowEffect;

    void OnEnable ()
    {
        i = 0;
        time = 0;
        transform.position = _Bezier.AllPoints[i];
        
        _AimeObjActive = GetComponent<AimeObjActive>();

        GlowEffect = gameObject.transform.GetChild(0).gameObject;
        myParticleSystem = GlowEffect.GetComponent<ParticleSystem>();
        emissionModule = myParticleSystem.emission;
    }


    float time;

	void FixedUpdate ()
    {
        if (i < _Bezier._segmentNum-1)
        {
            transform.position = Vector3.Lerp(transform.position, _Bezier.AllPoints[i], 0.1f * Speedwaveform.Evaluate(time));

            if (Vector3.Distance(transform.position, _Bezier.AllPoints[i]) < ((0.5f / Speedwaveform.Evaluate(time) + 0.1f)))
            {
                i++;
                time++;
                
            }
        }
        if (i <= _Bezier._segmentNum - 2)
        {
            NextDir = _Bezier.AllPoints[i + 1] - _Bezier.AllPoints[i];
            Quaternion targetRotate = Quaternion.Euler(0, 0, TwoVector_Angle(Vector3.down, NextDir));
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotate, 0.3f);
        }

        //最後的位置修正，在(0,0,0)
        if(i > _Bezier._segmentNum - 2)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, new Vector3(0, 0, 0), 0.1f * Speedwaveform.Evaluate(time));

            NextDir = new Vector3(0, -1, 0);
            Quaternion targetRotate = Quaternion.Euler(0, 0, TwoVector_Angle(Vector3.down, NextDir));
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotate, 0.3f);
            
            emissionModule.rateOverTime = 5;
        }

        //到達最後位置，關閉物件，換開啟另一個觸發物件
        if(Vector3.Distance(transform.localPosition, new Vector3(0,0,0))<1f)
        {
            _AimeObjActive.ActiveObj();
            GlowEffect.transform.parent = _AimeObjActive.Obj.transform;
            GlowEffect.transform.localScale = new Vector3(1, 1, 1);
            gameObject.SetActive(false);
        }
    }


    //求出兩向量之間的夾角
    float TwoVector_Angle(Vector3 Origin, Vector3 Target)
    {
        float angle = Vector3.Angle(Origin, Target);

        Vector3 normal = Vector3.Cross(Origin, Target);//叉乘求出法線向量
        angle *= Mathf.Sign(Vector3.Dot(normal, Vector3.forward));  //求法線向量與物體上方向向量點乘，結果為1或-1，修正旋轉方向
        
        return angle;
    }

}
