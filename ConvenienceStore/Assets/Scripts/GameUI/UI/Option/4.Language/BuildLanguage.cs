﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class BuildLanguage : BaseBuildPanel
    {
        public Button Arrow_L;
        public Button Arrow_R;
        public Image languageImg;
        private Localizer.Language tempLanguage;
        private int quantity = 5;
        private int languageNum = 0;

        private const string path = "GameUI/Option/Language";

        Prefab.LanguagePrefab languagePrefab;
        Sprite [] languageSpr;

        public override void Build ()
        {
            languageSpr = Resources.LoadAll<Sprite> ( "Sprites/Common/" + path );


            Arrow_L.onClick.AddListener ( GenerateArrow ( -1 ) );
            Arrow_R.onClick.AddListener ( GenerateArrow ( 1 ) );

            isBuild = true;
        }

        public override void Init ()
        {
            tempLanguage = (Localizer.Language)GameLoopManager.instance.om.optionData.GetLanguage ();
            languageNum = GameLoopManager.instance.om.optionData.GetLanguage ();
            languageImg.sprite = languageSpr [languageNum];
        }

        UnityAction GenerateArrow ( int id )
        {
            return () =>
            {
                OptionManager.updateBool = true;
                PlayButtonAudio.self.PlaySetting ();
                ChangeLanguage ( id );
            };
        }

        void ChangeLanguage ( int id )
        {
            languageNum += id;
            if ( languageNum >= quantity )
                languageNum = 0;
            if ( languageNum < 0 )
                languageNum = quantity - 1;
            languageImg.sprite = languageSpr [languageNum];
            tempLanguage = (Localizer.Language)languageNum;
        }

        public void SetLanguage ()
        {
            GameLoopManager.instance.om.optionData.SetLanguage ( tempLanguage );
        }
    }
}