﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class SoundLanguagePrefab : MonoBehaviour
    {
        public Button Button;
        public GameObject Frame;
        public Image TypeImage;
    }
}