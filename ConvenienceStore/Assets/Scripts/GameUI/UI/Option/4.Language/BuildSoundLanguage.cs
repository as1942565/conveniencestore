﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class BuildSoundLanguage : BaseBuildPanel
    {
        public Button Arrow_L;
        public Button Arrow_R;
        public Image languageImg;

        public enum SoundLanguage
        {
            Taiwanese = 0,
            Japanese = 1,
        }
        private SoundLanguage tempSoundLanguage;
        private int quantity = 2;
        private int languageNum = 0;

        private const string path = "GameUI/Option/Language";

        Prefab.SoundLanguagePrefab soundLanguage;
        Sprite [] soundSpr;

        public override void Build ()
        {
            soundSpr = Resources.LoadAll<Sprite> ( "Sprites/Common/" + path );
            Arrow_L.onClick.AddListener ( GenerateArrow ( -1 ) );
            Arrow_R.onClick.AddListener ( GenerateArrow ( 1 ) );

            isBuild = true;
        }

        public override void Init ()
        {
            tempSoundLanguage = (SoundLanguage)GameLoopManager.instance.om.optionData.GetSoundLanguage ();
            languageNum = GameLoopManager.instance.om.optionData.GetSoundLanguage ();
            languageImg.sprite = soundSpr [languageNum];
        }

        UnityAction GenerateArrow ( int id )
        {
            return () =>
            {
                OptionManager.updateBool = true;
                PlayButtonAudio.self.PlaySetting ();
                ChangeLanguage ( id );
            };
        }

        void ChangeLanguage ( int id )
        {
            languageNum += id;
            if ( languageNum >= quantity )
                languageNum = 0;
            if ( languageNum < 0 )
                languageNum = quantity - 1;
            languageImg.sprite = soundSpr [languageNum];
            tempSoundLanguage = (SoundLanguage)languageNum;
        }

        public void SetSoundLanguage ()
        {
            GameLoopManager.instance.om.optionData.SetSoundLanguage ( tempSoundLanguage );
        }
    }
}