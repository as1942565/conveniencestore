﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class OptionBar : MonoBehaviour ,IPointerUpHandler
{
    public Scrollbar scrollbar;
    public Text text;

    private int trueVaule;

    public Action<int> OnSetValue;

    public void SetValue ()
    {
        int value = Mathf.FloorToInt ( scrollbar.value * 100 ); //0.47 * 100 = 47   | 0.48 * 100 = 48
        float temp = value * 2f / 10f;                          //47 * 2 / 10 = 9.4 | 48 * 2 / 10 = 9.6
        float round = Mathf.Round ( temp );                     //9.4 -> 9          | 9.6 -> 10
        trueVaule = Mathf.FloorToInt ( round / 2 * 10 );        //9 / 2 * 10 = 45   | 10 / 2 * 10 = 50
        text.text = trueVaule.ToString ();
        if (OnSetValue != null )
        {
            OnSetValue ( trueVaule );
        }
    }
    
    public void SetValue ( int value )
    {
        text.text = value.ToString ();
        scrollbar.value = value / 100f;
    }

    public int GetValue ()
    {
        return trueVaule;
    }

    public void OnPointerUp ( PointerEventData eventData )
    {
        scrollbar.value = trueVaule / 100f;
        text.text = trueVaule.ToString ();
        UI.OptionManager.updateBool = true;
    }
}
