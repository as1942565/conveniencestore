﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI
{
    public class OptionManager : BaseBuildPanel
    {
        public static bool updateBool = false;

        public BuildResolution buildResolution;
        public BuildStory buildStory;
        public BuildStoryCheck buildStoryCheck;
        public BuildSoundLanguage buildSoundLanguage;
        public BuildLanguage buildLanguage;
        public BuildSetup buildSetup;

        public GameObject Shield;
        public GameObject Option;

        public Button OK;
        public Button Back;

        public Image Background;

        public static OptionManager self;
        private void Awake ()
        {
            self = this;
        }

        public override void Build ()
        {
            OK.onClick.AddListener ( GenerateOKBtn () );
            Back.onClick.AddListener ( GenerateResetBtn () );
            isBuild = true;
        }

        public override void Init ()
        {
            PlayButtonAudio.self.PlayMenuClick ();
            Shield.SetActive ( true );
            Option.SetActive ( true );
            OK.enabled = true;
            Back.enabled = true;

            buildResolution.Open ();
            buildStory.Open ();
            buildStoryCheck.Open ();
            buildSoundLanguage.Open ();
            buildLanguage.Open ();
            buildSetup.Open ();

            Sprite backgroundSpr = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameUI/Option/Background" );
            Background.sprite = backgroundSpr;

            OK.GetComponent<Image> ().sprite = Resources.LoadAll<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameUI/Option/Button" ) [1];
            Back.GetComponent<Image> ().sprite = Resources.LoadAll<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameUI/Option/Button" ) [0];
        }

        UnityAction GenerateOKBtn ()
        {
            return () =>
            {
                OKBtn ();
                PlayButtonAudio.self.PlaySetting ();
            };
        }

        void OKBtn ()
        {
            BuildResolution.Resolution resolution = buildResolution.GetResolution ();
            GameLoopManager.instance.om.optionData.SetResolution ( resolution );
            GameLoopManager.instance.SetResolution ();
            buildStory.SetStory ();
            buildStoryCheck.SetStoryBool ();
            buildSoundLanguage.SetSoundLanguage ();
            buildLanguage.SetLanguage ();
            buildSetup.SetVolume ();
            SaveLoadManager.OnlyDataSave ( GameLoopManager.instance.om );
            if ( BuildMainPanel.self != null )
                BuildMainPanel.self.Init ();

            Shield.SetActive ( false );
            Option.SetActive ( false );
            updateBool = false;
            StorySceneManager.log = false;
        }

        UnityAction GenerateResetBtn ()
        {
            return () =>
            {
                ResetBtn ();
                PlayButtonAudio.self.PlaySetting ();
            };
        }
        void ResetBtn ()
        {
            GameLoopManager.instance.om.optionData.Reset ();
            GameLoopManager.instance.om.audioData.Init ();

            buildResolution.Init ();
            buildStory.Init ();
            buildStoryCheck.Init ();
            buildSoundLanguage.Init ();
            buildLanguage.Init ();
            buildSetup.Init ();
            SaveLoadManager.OnlyDataSave ( GameLoopManager.instance.om );
        }
    
        public override void Close ()
        {
            Option.SetActive ( false );
            StorySceneManager.log = false;
        }
    }
}