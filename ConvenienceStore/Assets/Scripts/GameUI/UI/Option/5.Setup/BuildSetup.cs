﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class BuildSetup : BaseBuildPanel
    {
        private int quantity = 7;
        private float [] posY = new float [] { 260f , 195f , 133f , 67f , 2f , -63f , -127f };

        private const string path = "GameUI/Option/Setup";

        Prefab.SetupPrefab [] SetupPrefab;

        public override void Build ()
        {
            SetupPrefab = new Prefab.SetupPrefab [quantity];

            for ( int i = 0; i < SetupPrefab.Length; i++ )
            {
                Prefab.SetupPrefab setup = Instantiate ( Resources.Load<Prefab.SetupPrefab> ( "Prefabs/" + path ) , transform );
                setup.transform.localPosition = new Vector3 ( setup.transform.localPosition.x , posY[i] , 0 );
                SetupPrefab [i] = setup;
            }
            isBuild = true;
            SetRuntime ();
        }

        void SetRuntime ()
        {
            SetupPrefab [0].optionBar.OnSetValue += ( v ) =>
            {
                GameLoopManager.instance.om.audioData.SetMain ( v );
            };
            SetupPrefab [1].optionBar.OnSetValue += ( v ) =>
            {
                GameLoopManager.instance.om.audioData.SetBGM ( v );
            };
            SetupPrefab [3].optionBar.OnSetValue += ( v ) =>
            {
                GameLoopManager.instance.om.audioData.SetSystemEffect ( v );
            };
        }

        public override void Init ()
        {
            SetupPrefab [0].SetValue ( GameLoopManager.instance.om.audioData.GetMain () );
            SetupPrefab [1].SetValue ( GameLoopManager.instance.om.audioData.GetTrueBGM () );
            SetupPrefab [2].SetValue ( GameLoopManager.instance.om.audioData.GetTrueStoryEffect () );
            SetupPrefab [3].SetValue ( GameLoopManager.instance.om.audioData.GetTrueSystemEffect () );
            SetupPrefab [4].SetValue ( GameLoopManager.instance.om.audioData.GetTrueSoundEffect () );
            SetupPrefab [5].SetValue ( GameLoopManager.instance.om.audioData.GetTrueBreatheEffect () );
            SetupPrefab [6].SetValue ( GameLoopManager.instance.om.audioData.GetTrueHEffect () );
        }

        public void SetVolume ()
        {
            GameLoopManager.instance.om.audioData.SetMain ( SetupPrefab [0].GetValue () );
            GameLoopManager.instance.om.audioData.SetBGM ( SetupPrefab [1].GetValue () );
            GameLoopManager.instance.om.audioData.SetStoryEffect ( SetupPrefab [2].GetValue () );
            GameLoopManager.instance.om.audioData.SetSystemEffect ( SetupPrefab [3].GetValue () );
            GameLoopManager.instance.om.audioData.SetSoundEffect ( SetupPrefab [4].GetValue () );
            GameLoopManager.instance.om.audioData.SetBreatheEffect ( SetupPrefab [5].GetValue () );
            GameLoopManager.instance.om.audioData.SetHEffect ( SetupPrefab [6].GetValue () );
        }
    }
}