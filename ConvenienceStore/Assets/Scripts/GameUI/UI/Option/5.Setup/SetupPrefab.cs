﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Prefab
{
    public class SetupPrefab : MonoBehaviour
    {
        private const string path = "GameUI/Option/Scrollbar";

        public OptionBar optionBar;

        private void Awake ()
        {
            optionBar = Instantiate ( Resources.Load<OptionBar> ( "Prefabs/" + path ) , transform );

        }

        public void SetValue ( int value )
        {
            optionBar.SetValue ( value );
        }

        public int GetValue ()
        {
            return optionBar.GetValue ();
        }
    }
}