﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Prefab
{
    public class StoryPrefab : MonoBehaviour
    {
        private const string path = "GameUI/Option/Scrollbar";

        private OptionBar optionBar;

        private void Awake ()
        {
            optionBar = Instantiate ( Resources.Load<OptionBar> ( "Prefabs/" + path ) , transform );
        }

        public void SetValue ( int value )
        {
            optionBar.SetValue ( value );
        }

        public int GetValue ()
        {
            return optionBar.GetValue ();
        }
    }
}