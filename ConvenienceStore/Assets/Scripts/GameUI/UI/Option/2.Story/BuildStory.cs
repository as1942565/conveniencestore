﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class BuildStory : BaseBuildPanel
    {
        private int quantity = 3;
        private float [] posY = new float [] { 105f , 42f , -20f };

        private const string path = "GameUI/Option/Story";

        Prefab.StoryPrefab [] story;

        public override void Build ()
        {
            story = new Prefab.StoryPrefab [quantity];

            for ( int i = 0; i < story.Length; i++ )
            {
                Prefab.StoryPrefab s = Instantiate ( Resources.Load<Prefab.StoryPrefab> ( "Prefabs/" + path ) , transform );
                s.transform.localPosition = new Vector3 ( s.transform.localPosition.x , posY[i] , 0 );
                story [i] = s;
            }
            isBuild = true;
        }

        public override void Init ()
        {
            story [0].SetValue ( GameLoopManager.instance.om.optionData.GetKeyWordSpeed () );
            story [1].SetValue ( GameLoopManager.instance.om.optionData.GetAutoPlaySpeed () );
            int alpha = GameLoopManager.instance.om.optionData.GetTalkBoxAlpha ();
            story [2].SetValue ( alpha );
            if ( TalkBoxManager.self != null )
                TalkBoxManager.self.SetTalkBoxAlpha ( alpha );
        }

        public void SetStory ()
        {
            GameLoopManager.instance.om.optionData.SetKeyWordSpeed ( story [0].GetValue () );
            GameLoopManager.instance.om.optionData.SetAutoPlaySpeed ( story [1].GetValue () );
            int alpha = story [2].GetValue ();
            GameLoopManager.instance.om.optionData.SetTalkBoxAlpha ( alpha );
            if ( TalkBoxManager.self != null )
                TalkBoxManager.self.SetTalkBoxAlpha ( alpha );
        }
    }
}