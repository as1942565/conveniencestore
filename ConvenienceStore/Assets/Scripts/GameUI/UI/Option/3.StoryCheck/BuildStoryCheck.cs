﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace UI
{
    public class BuildStoryCheck : BaseBuildPanel
    {
        [SerializeField]
        private bool tempCheck1 = true;

        private const string path = "GameUI/Option/StoryCheck";

        Prefab.StoryCheckPrefab storyCheck;

        public override void Build ()
        {
            storyCheck = Instantiate ( Resources.Load<Prefab.StoryCheckPrefab> ( "Prefabs/" + path ) , transform );
            storyCheck.Button1.onClick.AddListener ( GenerateCheck1 () );
            isBuild = true;
        }

        public override void Init ()
        {
            tempCheck1 = GameLoopManager.instance.om.optionData.GetContinueSound ();
            storyCheck.CheckImage1.SetActive ( tempCheck1 );
        }

        UnityAction GenerateCheck1 ()
        {
            return () =>
            {
                OptionManager.updateBool = true;
                Check1 ();
            };
        }
        void Check1 ()
        {
            tempCheck1 = !tempCheck1;
            storyCheck.CheckImage1.SetActive ( tempCheck1 );
        }
        

        public void SetStoryBool ()
        {
            GameLoopManager.instance.om.optionData.SetContinueSound ( tempCheck1 );
        }
    }
}