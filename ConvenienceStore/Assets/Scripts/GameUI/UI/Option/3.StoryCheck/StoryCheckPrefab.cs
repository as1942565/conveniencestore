﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class StoryCheckPrefab : MonoBehaviour
    {
        public Button Button1;
        public GameObject CheckImage1;
        public Button Button2;
        public GameObject CheckImage2;
    }
}