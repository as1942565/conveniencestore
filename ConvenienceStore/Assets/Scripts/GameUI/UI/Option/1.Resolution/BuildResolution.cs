﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace UI
{
    public class BuildResolution : BaseBuildPanel
    {
        public enum Resolution
        {
            fullScreen = 0,
            w800 = 1,
            w1280 = 2,
            w1920 = 3,
        }
        private Resolution tempResolution;


        private int quantity = 4;
        private float distance = 180f;

        private const string path = "GameUI/Option/Resolution";

        Prefab.ResolutionPrefab [] resolution;

        public override void Build ()
        {
            resolution = new Prefab.ResolutionPrefab [quantity];

            Sprite [] resolutionSpr = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + path );
            for ( int i = 0; i < resolution.Length; i++ )
            {
                Prefab.ResolutionPrefab res = Instantiate ( Resources.Load<Prefab.ResolutionPrefab> ( "Prefabs/" + path ) , transform );
                res.transform.localPosition += new Vector3 ( distance * i , 0 , 0 );
                res.TypeImage.sprite = resolutionSpr [i];
                res.Button.onClick.AddListener ( GenerateType ( i ) );
                resolution [i] = res;
            }
            isBuild = true;
        }

        public override void Init ()
        {
            int type = GameLoopManager.instance.om.optionData.GetResolution ();
            SwitchType ( type );

            Sprite [] resolutionSpr = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + path );
            for ( int i = 0; i < resolution.Length; i++ )
            {
                resolution [i].TypeImage.sprite = resolutionSpr [i];
            }
        }

        UnityAction GenerateType ( int id )
        {
            return () =>
            {
                OptionManager.updateBool = true;
                PlayButtonAudio.self.PlaySetting ();
                SwitchType ( id );
            };
        }
        void SwitchType ( int id )
        {
            for ( int i = 0; i < resolution.Length; i++ )
            {
                resolution [i].Frame.SetActive ( i == id );
            }
            tempResolution = (Resolution)id;
        }

        public Resolution GetResolution ()
        {
            return tempResolution;
        }
    }
}