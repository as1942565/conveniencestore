﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Observer
{
    public class PlayerStatusPanel : BaseObserver
    {


        public Transform panel;

        private Text moneyText;                                             //金錢文字
        private Text popularText;                                           //人氣值文字
        private Text cleanText;                                             //清潔度文字
        private Text policeText;                                            //治安度文字

        private Transform dayIconSpace;
        private Image BeforeDayIcon;                                        
        private Image dayIcon;                                              //天數底圖
        private Image BeforeDayBox;                                        
        private Image dayIconBox;

        private Transform daySpace;
        private Image dayImage;                                             //天數圖片
        private Image beforeDayImage;

        private Transform shiftSpace;
        private Text shiftText;                                             //時段文字
        private Text beforeShiftText;                                       //前一個時段文字
        private Image shiftImage;                                           //時段圖片
        private Image beforeShiftImage;                                     //前一個時段圖片

        private Image [] actionPoint;                                       //行動點父物件

        private const string path = "Sprites/Common/GameUI/Background/";

        public static PlayerStatusPanel self;
        private void Awake ()
        {
            self = this;
            moneyText = panel.Find ( "MoneyText" ).GetComponent<Text> ();
            popularText = panel.Find ( "PopularText" ).GetComponent<Text> ();
            cleanText = panel.Find ( "CleanText" ).GetComponent<Text> ();
            policeText = panel.Find ( "PoliceText" ).GetComponent<Text> ();

            dayIconSpace = panel.Find ( "DayIconMask/DayIconSpace" );
            dayIcon = panel.Find ( "DayIconMask/DayIconSpace/DayIcon" ).GetComponent<Image> ();
            BeforeDayIcon = panel.Find ( "DayIconMask/DayIconSpace/BeforeDayIcon" ).GetComponent<Image> ();
            dayIconBox = panel.Find ( "DayIconMask/DayBox" ).GetComponent<Image> ();
            BeforeDayBox = panel.Find ( "DayIconMask/BeforeDayBox" ).GetComponent<Image> ();

            daySpace = panel.Find ( "DayTextMask/Day" );
            dayImage = panel.Find ( "DayTextMask/Day/DayImage" ).GetComponent<Image> ();
            beforeDayImage = panel.Find ( "DayTextMask/Day/BeforeDayImage" ).GetComponent<Image> ();

            shiftText = panel.Find ( "Shift/ShiftMask/Shift/ShiftText" ).GetComponent<Text> ();
            beforeShiftText = panel.Find ( "Shift/ShiftMask/Shift/BeforeShiftText" ).GetComponent<Text> ();
            shiftSpace = panel.Find ( "Shift/ShiftMask/Shift" );
            shiftImage = panel.Find ( "Shift/ShiftMask/Shift/ShiftImage" ).GetComponent<Image> ();
            beforeShiftImage = panel.Find ( "Shift/ShiftMask/Shift/BeforeShiftImage" ).GetComponent<Image> ();

            Transform action = panel.Find ( "Action" );
            actionPoint = action.GetComponentsInChildren<Image> ();
        }
        protected override void RefreshComponent ( PlayerDataManager pm )
        {
            OnMoneyUpdated ( pm.playerData.GetMoney () );
            OnPopularUpdated ( pm.playerData.GetPopular () );
            OnCleanUpdated ( pm.playerData.GetClean () );
            OnPoliceUpdated ( pm.playerData.GetPolice () );
            OnShiftUpdated ( pm.calender );
            OnActionPointUpdated ( pm.calender );
            Observer.PlayerStatusPanel.self.Fade ();
        }

        protected override void RegisterObserver ( PlayerDataManager pm )
        {
            pm.playerData.OnMoneyChanged += OnMoneyUpdated;
            pm.playerData.OnPopularChanged += OnPopularUpdated;
            pm.playerData.OnCleanChanged += OnCleanUpdated;
            pm.playerData.OnPoliceChanged += OnPoliceUpdated;
            pm.calender.AfterNextShift += OnShiftUpdated;
            pm.calender.OnEnergyChange += OnActionPointUpdated;
        }

        protected override void UnregisterObserver ( PlayerDataManager pm )
        {
            pm.playerData.OnMoneyChanged -= OnMoneyUpdated;
            pm.playerData.OnPopularChanged -= OnPopularUpdated;
            pm.playerData.OnCleanChanged -= OnCleanUpdated;
            pm.playerData.OnPoliceChanged -= OnPoliceUpdated;
            pm.calender.AfterNextShift -= OnShiftUpdated;
            pm.calender.OnEnergyChange -= OnActionPointUpdated;
        }
        private void OnMoneyUpdated ( int money )
        {
            moneyText.text = money.ToString ( "N0" );
        }
        private void OnPopularUpdated ( int popular )
        {
            popularText.text = popular.ToString ( "N0" );
        }
        private void OnCleanUpdated ( int clean )
        {
            cleanText.text = clean.ToString ( "N0" );
        }
        private void OnPoliceUpdated ( int police )
        {
            policeText.text = police.ToString ();
        }


        private int tempDay = 0;
        private void OnShiftUpdated ( Calender calender )
        {
            Sprite [] daySpr = Resources.LoadAll<Sprite> ( path + "DayNum" );
            int day = calender.GetDay ();
            
            if ( day >= Calender.MaximumDay )
            {
                return;
            }

            if ( StoryManager.self.EnableTeach )
                dayImage.sprite = daySpr [0];
            else
            {
                beforeDayImage.sprite = daySpr [day];
                dayImage.sprite = daySpr [day + 1];

                /*if ( tempDay != day )
                {
                    daySpace.localPosition = Vector3.zero;
                    daySpace.DOLocalMoveX ( 100 , 1f );
                }*/
            }

            Sprite [] dayIconSpr = Resources.LoadAll<Sprite> ( path + "Shift/DayIcon" );
            Sprite [] dayBoxSpr = Resources.LoadAll<Sprite> ( path + "Shift/DayBox" );
            Sprite [] ShiftSpr = Resources.LoadAll<Sprite> ( path + "Shift/ShiftImage" );
            int shift = (int)calender.GetShift ();
            int beforeShift = (int)calender.GetShift () - 1;
            if ( beforeShift == -1 )
                beforeShift = 2;
            string [] shiftName = new string [] { "早班" , "晚班" , "大夜班" };
            BeforeDayIcon.sprite = dayIconSpr [beforeShift];
            dayIcon.sprite = dayIconSpr [shift];
            BeforeDayBox.sprite = dayBoxSpr [beforeShift];
            dayIconBox.sprite = dayBoxSpr [shift];

            shiftImage.sprite = ShiftSpr [shift];
            beforeShiftImage.sprite = ShiftSpr [beforeShift];

            shiftText.text = Localizer.Translate ( shiftName [shift] );
            beforeShiftText.text = Localizer.Translate ( shiftName [beforeShift] );

            tempDay = day;

            shiftSpace.localPosition = Vector3.zero;
            dayIconSpace.localEulerAngles = Vector4.zero;
            beforeShiftText.DOFade ( 1 , 0.1f );
            beforeShiftImage.DOFade ( 1 , 0.1f );
            dayIconBox.DOFade ( 0 , 0.1f );
            //Invoke ( "Fade" , 0.5f );
        }

        public void Fade ()
        {
            beforeShiftImage.DOFade ( 0 , 1.5f );
            beforeShiftText.DOFade ( 0 , 1.5f );
            shiftSpace.DOLocalMoveY ( -100 , 1.5f );
            //dayIconSpace.DOLocalMoveY ( -115 , 1.5f );
            dayIconSpace.DOLocalRotate ( new Vector3 ( 0 , 0 , -90 ) , 1.5f );
            dayIconBox.DOFade ( 1 , 1.5f );
        }
     
        private void OnActionPointUpdated ( Calender calender )
        {
            int maxEnergy = calender.GetDayEneryLeft ();
            for ( int i = 0; i < actionPoint.Length; i++ )
            {
                if ( actionPoint [i] != null )
                {
                    actionPoint [i].gameObject.SetActive ( i < maxEnergy );
                }
            }
        }
    }
}