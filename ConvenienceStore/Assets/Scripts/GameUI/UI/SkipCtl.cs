﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkipCtl : MonoBehaviour
{
    public void Skip ()
    {
        PlayButtonAudio.self.PlayRest ();
        LoadingScene.self.Transitions ( 0.5f , 1f , true );
        Invoke ( "skip" , 0.5f );
    }
    void skip ()
    {
        GameLoopManager.instance.pm.calender.UseEnergy ();
    }
}
