﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace UI
{
    public class BuildDragIcon : BaseBuildPanel
    {
        public static BuildDragIcon self;
        private void Awake ()
        {
            self = this;
        }

        public GraphicRaycaster GameUICanvas;
        public Image IconImage;

        int commodityID = -1;
        Prefab.SetupCommodityItemPrefab item;
        Prefab.CabinetPrefab oldCabinet;

        SetupCommodityManager manager;
        BuildCabinet buildCabinet;
        public void GetManager ( SetupCommodityManager manager , BuildCabinet buildCabinet )
        {
            this.manager = manager;
            this.buildCabinet = buildCabinet;
        }

        public override void Build ()
        {
            IconImage.gameObject.SetActive ( false );
            isBuild = true;
        }

        public override void Init ()
        {
        }

        public override void Close ()
        {
            IconImage.gameObject.SetActive ( false );
        }

        public void DragIcon ( int id , Prefab.SetupCommodityItemPrefab item , Prefab.CabinetPrefab oldCabinet )
        {
            this.item = item;
            this.oldCabinet = oldCabinet;
            commodityID = id;
            if ( id == -1 )
                return;
            SetDragIconPos ();
            IconImage.gameObject.SetActive ( true );
            IconImage.sprite = SpritePath.Commodity [id];
        }
        void SetDragIconPos ()
        {
            Vector2 pos = Camera.main.ScreenToViewportPoint ( Input.mousePosition );
            var itempos = new Vector2 ( ( pos.x - 0.5f ) * 1920 , ( pos.y - 0.5f ) * 1080 );
            IconImage.transform.localPosition = itempos;
        }

        private void Update ()
        {
            if ( IconImage.gameObject.activeSelf )
            {
                SetDragIconPos ();
                if ( Input.GetMouseButtonUp ( 0 ) )
                {
                    if ( commodityID == -1 )
                        return;
                    PlayButtonAudio.self.PlaySetupCommodity ();
                    if ( item != null )
                    {
                        PutCommodityWithItem ();
                        return;
                    }
                    if ( oldCabinet != null )
                    {
                        PutCommodityWithCabinet ();
                    }
                }
            }
        }

        void PutCommodityWithItem ()
        {
            SetupCommodityManager.updateBool = true;
            item.TargetBool ( false );
            IconImage.gameObject.SetActive ( false );

            Prefab.CabinetPrefab cabinet = GetCabinet ();
            if ( cabinet == null )
            {
                manager.cabinet.ClearPutCabinet ();
                return;
            }
            else
            {
                buildCabinet.CheckItem ( commodityID );
            }
            //int type = (int)gm.commodityList.Data [commodityID].type;
            //if ( cabinet.GetCabinetType () == type || cabinet.GetCabinetType () == 4 )
            if ( cabinet.Frame.activeSelf )
            {
                if ( cabinet.GetCommodityID () != -1 )
                {
                    cabinet.CancelSold ();
                }
                //cabinet.GetItem ( item );
                cabinet.UpdateData ( commodityID );
                manager.cabinet.SetSoldID ( cabinet.GetCabinetType () , cabinet.GetCabinetID () , commodityID );
                item.SoldBool ( true );
            }
            manager.cabinet.ClearPutCabinet ();
        }
        void PutCommodityWithCabinet () //櫃子移到櫃子
        {
            UI.SetupCommodityManager.updateBool = true;
            IconImage.gameObject.SetActive ( false );


            Prefab.CabinetPrefab cabinet = GetCabinet ();
            if ( cabinet == null )
            {
                manager.cabinet.ClearPutCabinet ();
                return;
            }
            //int type = (int)gm.commodityList.Data [commodityID].type;
            //if ( cabinet.GetCabinetType () == type || cabinet.GetCabinetType () == 4 )
            if ( cabinet.Frame.activeSelf )
            {
                oldCabinet.ChangeCommodity ( oldCabinet , cabinet , gm , manager );
                cabinet.UpdateData ( commodityID );
                manager.cabinet.SetSoldID ( cabinet.GetCabinetType () , cabinet.GetCabinetID () , commodityID );
            }
            manager.cabinet.ClearPutCabinet ();
        }

        Prefab.CabinetPrefab GetCabinet () 
        {
            PointerEventData eventData = new PointerEventData ( StoryManager.self.eventSystem );
            eventData.position = Input.mousePosition;

            GraphicRaycaster raycaster = GameUICanvas;
            List<RaycastResult> result = new List<RaycastResult> ();
            raycaster.Raycast ( eventData , result );
            if ( result.Count == 0 )
                return null;
            Prefab.CabinetPrefab cabinetPrefab = null;
            for ( int i = 0; i < result.Count; i++ )
            {
                cabinetPrefab = result [i].gameObject.GetComponent<Prefab.CabinetPrefab> ();
                if ( cabinetPrefab != null )
                    return cabinetPrefab;
            }
            return null;
        }
    }
}