﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class SetupCommodityManager : BaseBuildPanel
    {
        public static bool updateBool;
        public Button BackBtn;

        public BuildSetupCommodity setupCommodity;
        public BuildCabinet cabinet;
        public BuildDragIcon dragIcon;

        public override void Build ()
        {
            cabinet.GetManager ( this );
            setupCommodity.GetManager ( this );
            dragIcon.GetManager ( this , cabinet );
            isBuild = true;
            BackBtn.onClick.AddListener ( GenerateBack () );
        }

        public override void Init ()
        {
            cabinet.Open ();
            setupCommodity.Open ();
            dragIcon.Open ();
        }
        
        public void OKBtn ()
        {
            Prefab.CabinetPrefab [] [] cabinets = cabinet.GetCabinets ();
            for ( int type = 0; type < cabinets.Length; type++ )
            {
                for ( int id = 0; id < pm.cabinet.GetTypeUnlockNum ( type ); id++ )
                {
                    int commoidtyID = cabinets [type] [id].GetCommodityID ();
                    if ( commoidtyID >= 0 )
                        Action.SetupCommodity.Run ( type , id , commoidtyID , pm );
                    else
                        pm.cabinet.SetCommodityID ( type , id , commoidtyID );
                }
            }
            CloseBtn ();
            PlayButtonAudio.self.PlayCommodityOK ();
        }
        public void ResetBtn ()
        {
            Prefab.CabinetPrefab [] [] cabinets = cabinet.GetCabinets ();
            for ( int t = 0; t < cabinets.Length; t++ )
            {
                for ( int i = 0; i < pm.cabinet.GetTypeUnlockNum ( t ); i++ )
                {
                    int commodityID = pm.cabinet.GetCommodityID ( t , i );
                    cabinets [t] [i].UpdateData ( commodityID );
                }
            }
            setupCommodity.ResetSold ();
        }
        UnityAction GenerateBack ()
        {
            return () =>
            {
                PlayButtonAudio.self.PlayCommodityCancel ();
                CloseBtn ();
            };
        }
        public void CloseBtn ()
        {
            SetupCommodityManager.updateBool = false;
            BuildRestock.updateBool = false;
            setupCommodity.Close ();
            cabinet.Close ();
            dragIcon.Close ();
            MainPanel.self.SwitchPanel ( 0 );
        }
    }
}
