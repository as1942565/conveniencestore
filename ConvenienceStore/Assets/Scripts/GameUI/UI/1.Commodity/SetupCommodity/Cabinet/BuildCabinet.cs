﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class BuildCabinet : BaseBuildPanel
    {
        [SerializeField]    
        private int cabinetType;
        [SerializeField]
        private int cabinetID;
        [SerializeField]
        private int commodityID;

        public Image SpecialCaninet;
        public Image Background;

        private Vector2 [] [] Pos = new Vector2 [] [] {
                                                        new Vector2 [] { new Vector2 ( -75f , 105f ) , new Vector2 ( 73f , 105f ) , new Vector2 ( -223f , 105f ) , new Vector2 ( 221f , 105f ) } ,
                                                        new Vector2 [] { new Vector2 ( -75f , -110f ) , new Vector2 ( 73f , -110f ) , new Vector2 ( -223f , -110f ) , new Vector2 ( 221f , -110f ) } ,
                                                        new Vector2 [] { new Vector2 ( -75f , -333f ) , new Vector2 ( 73f , -333f ) , new Vector2 ( -223f , -333f ) , new Vector2 ( 221f , -333f ) } ,
                                                        new Vector2 []{ new Vector2 ( 453f , 105f ) , new Vector2 ( 453f , -50f ) } ,
                                                        new Vector2 []{ new Vector2 ( 0 , 302f ) } };
        private const string CabinetPath = "GameUI/Commodity/SetupCommodity/";
        
        Prefab.CabinetPrefab [] [] Item;
        int [] [] soldingID;

        SetupCommodityManager manager;
        public void GetManager ( SetupCommodityManager manager )
        {
            this.manager = manager;
        }

        public override void Build ()
        {
            Item = new Prefab.CabinetPrefab [Pos.Length][];
            soldingID = new int [Pos.Length] [];
            for ( int t = 0; t < Pos.Length; t++ )
            {
                Item [t] = new Prefab.CabinetPrefab [Pos [t].Length];
                soldingID [t] = new int [Pos [t].Length];
                for ( int i = 0; i < Pos [t].Length; i++ )
                {
                    Prefab.CabinetPrefab cabinet = Instantiate ( Resources.Load<Prefab.CabinetPrefab> ( "Prefabs/" + CabinetPath + "Cabinet" ) , transform );
                    cabinet.transform.localPosition = Pos [t] [i];
                    cabinet.Icon.sprite = SpritePath.None;
                    cabinet.GetData ( t , i , this , manager.setupCommodity );
                    Item [t] [i] = cabinet;
                    soldingID [t] [i] = -1;
                }
            }
            isBuild = true;
        }

        public override void Init ()
        {
            for ( int y = 0; y < Item.Length; y++ )
            {
                for ( int x = 0; x < Item [y].Length; x++ )
                {
                    Item [y] [x].gameObject.SetActive ( false );                    //將所有商品櫃關閉
                    if ( x >= pm.cabinet.GetTypeUnlockNum ( y ) )
                        continue;
                    SetSoldID ( y , x , pm.cabinet.GetCommodityID ( y , x ) );
                }

                int unlockQuantity = pm.cabinet.GetTypeUnlockNum ( y );             //取得該種類商品櫃的解鎖數量
                for ( int x = 0; x < unlockQuantity; x++ )
                {
                    Item [y] [x].gameObject.SetActive ( true );                     //依據解鎖數量來顯示商品櫃
                    int id = pm.cabinet.GetCommodityID ( y , x );
                    Item [y] [x].SetCommoidtyID ( id );
                    if ( id < 0 )
                    {
                        Item [y] [x].Icon.sprite = SpritePath.None;
                        continue;
                    }
                    Item [y] [x].Icon.sprite = SpritePath.Commodity [id];
                    //Item [y] [x].GetItem ( manager.setupCommodity.GetItem ( id ) );
                }
            }
            bool unlockSpecial = pm.cabinet.GetTypeUnlockNum ( 3 ) >= 2;
            SpecialCaninet.gameObject.SetActive ( unlockSpecial );

            SpecialCaninet.sprite = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}{CabinetPath}Cabinet/SpecialCabinet" );
            Background.sprite = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}{CabinetPath}Cabinet/NormalCabinet" );
        }

        public void CanPutCabinet ( int cabinetType )
        {
            if ( StoryManager.self != null && StoryManager.self.TeachBool )
            {
                int type = StoryManager.self.teachSceneManager.setupCommodityType;
                int id = StoryManager.self.teachSceneManager.setupCommodityID;
                Item [type] [id].Frame.SetActive ( true );
                return;
            }
            for ( int i = 0; i < Item[cabinetType].Length; i++ )
            {
                Item [cabinetType] [i].Frame.SetActive ( true );
            }

            if ( cabinetType != 3 )
                Item [4] [0].Frame.SetActive ( true );
        }
        public void ClearPutCabinet ()
        {
            for ( int t = 0; t < Item.Length; t++ )
            {
                for ( int i = 0; i < Item [t].Length; i++ )
                {
                    Item [t] [i].Frame.SetActive ( false );
                }
            }
        }

        public Prefab.CabinetPrefab [] [] GetCabinets ()
        {
            return Item;
        }

        public void SetSoldID ( int type , int id , int commodityID )
        {
            if ( type >= soldingID.Length )
            {
                Debug.LogError ( type + "超過陣列長度" );
                return;
            }
            if ( id > pm.cabinet.GetTypeUnlockNum ( type ) )
            {
                Debug.LogError ( id + "超已解所長度" );
                return;
            }
            soldingID [type] [id] = commodityID;
        }
        public int GetSoldID ( int type , int id )
        {
            if ( type >= soldingID.Length )
            {
                Debug.LogError ( type + "超過陣列長度" );
                return -1;
            }
            if ( id > pm.cabinet.GetTypeUnlockNum ( type ) )
            {
                Debug.LogError ( id + "超已解所長度" );
                return -1;
            }
            return soldingID [type] [id];
        }
        
        public bool GetSoldBool ( int id )
        {
            for ( int t = 0; t < soldingID.Length; t++ )
            {
                for ( int i = 0; i < soldingID [t].Length; i++ )
                {
                    if ( soldingID [t] [i] == id )
                        return true;
                }
            }
            return false;
        }

        public void CheckItem ( int id )
        {
            for ( int t = 0; t < Item.Length; t++ )
            {
                for ( int i = 0; i < Item [t].Length; i++ )
                {
                    if ( Item [t] [i].GetCommodityID () == id )
                    {
                        Item [t] [i].ClearData ();
                    }
                }
            }
        }
    }
}