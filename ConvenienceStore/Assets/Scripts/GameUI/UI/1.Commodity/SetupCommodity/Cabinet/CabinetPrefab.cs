﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Prefab
{
    public class CabinetPrefab : MonoBehaviour , IPointerClickHandler , IPointerDownHandler
    {
        public GameObject Frame;
        public Image Icon;

        private int cabinetType;
        private int cabinetID;
        [SerializeField]
        private int commodityID;
        UI.BuildCabinet buildCabinet;
        UI.BuildSetupCommodity buildSetupCommodity;
        public void GetData ( int type , int id , UI.BuildCabinet buildCabinet , UI.BuildSetupCommodity buildSetupCommodity )
        {
            cabinetType = type;
            cabinetID = id;
            commodityID = -1;
            this.buildCabinet = buildCabinet;
            this.buildSetupCommodity = buildSetupCommodity;
            gameObject.name = $"cabinet{type.ToString ()}-{id.ToString ()}";
        }

        /*[SerializeField]
        SetupCommodityItemPrefab item;
        public void GetItem ( SetupCommodityItemPrefab item )
        {
            this.item = item;
        }
        public void ClearItem ()
        {
            item = null;
        }*/

        public void UpdateData ( int commodityID )
        {
            this.commodityID = commodityID;
            if ( commodityID < 0 )
            {
                ClearData ();
                return;
            }
            Icon.sprite = SpritePath.Commodity [commodityID];
        }
        public void ClearData ()
        {
            Icon.sprite = SpritePath.None;
            commodityID = -1;
            buildCabinet.SetSoldID ( cabinetType , cabinetID , -1 );
        }

        public int GetCabinetID ()
        {
            return cabinetID;
        }
        public int GetCabinetType ()
        {
            return cabinetType;
        }

        public int GetCommodityID ()
        {
            return commodityID;
        }
        public void SetCommoidtyID ( int id )
        {
            commodityID = id;
        }

        public void OnPointerClick ( PointerEventData eventData )
        {
            if ( eventData.button == PointerEventData.InputButton.Right )
            {
                if ( StoryManager.self.TeachBool )
                    return;
                CancelSold ();
                ClearData ();
                UI.SetupCommodityManager.updateBool = true;
                PlayButtonAudio.self.PlayCommodityCancel ();
            }
        }
        public void CancelSold ()
        {
            SetupCommodityItemPrefab [] items = buildSetupCommodity.GetItem ();
            for ( int i = 0; i < items.Length; i++ )
            {
                if ( items [i].GetCommodityID () == commodityID )
                {
                    items [i].SoldBool ( false );
                    break;
                }
            }
        }

        public void OnPointerDown ( PointerEventData eventData )
        {
            if ( eventData.button != PointerEventData.InputButton.Left )
                return;
            if ( commodityID == -1 )
                return;
            
            buildSetupCommodity.DragCommodityWithCabinet ( commodityID , this );
        }

        public void ChangeCommodity ( CabinetPrefab oldCabinet , CabinetPrefab cabinet , GameDataManager gm , UI.SetupCommodityManager manager )
        {
            int id = cabinet.GetCommodityID ();
            if ( id == -1 )
            {
                UpdateData ( -1 );
                //cabinet.item = oldCabinet.item;
                //oldCabinet.ClearItem ();
                return;
            }
            int type = (int)gm.commodityList.Data [id].type;
            if ( cabinetType == 4 || cabinetType == type )
            {
                UpdateData ( id );
                manager.cabinet.SetSoldID ( oldCabinet.GetCabinetType () , oldCabinet.GetCabinetID () , id );
            }
            else
            {
                UpdateData ( -1 );
                CancelSold ();
                buildSetupCommodity.CancelSold ( id );
            }
        }
    }
}
