﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Prefab
{
    public class SetupCommodityItemPrefab : MonoBehaviour , IPointerDownHandler
    {
        public Image ItemImage;
        public Sprite NormalSpr;
        public Sprite SpecialSpr;
        public Image DataImage;
        public GameObject Sprite;
        public Image Icon;
        public GameObject Target;
        public Text Name;
        public Text Quantity;
        public Text Price;
        public Image Popular;
        public GameObject Lock;
        public GameObject Sold;
        public Image SoldOut;
        public Image DragIcon;

        private int commodityID;

        UI.BuildSetupCommodity buildSetupCommodity;
        public void GetBuildSetupCommodity ( UI.BuildSetupCommodity buildSetupCommodity )
        {
            this.buildSetupCommodity = buildSetupCommodity;
        }

        public void UpdateData ( int id , GameData.Commodity.CommodityType commodityType , GameDataManager gm , PlayerDataManager pm , bool sold )
        {
            if ( id < 0 )
            {
                ClearData ();
                return;
            }

            SoldOut.sprite = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameUI/Commodity/SetupCommodity/Commodity/SoldOut" );

            DataImage.sprite = Resources.LoadAll<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameUI/Commodity/SetupCommodity/Commodity/Item" ) [2];

            if ( commodityType == GameData.Commodity.CommodityType.special )
                ItemImage.sprite = SpecialSpr;
            else
                ItemImage.sprite = NormalSpr;
            Sprite.SetActive ( true );
            commodityID = id;
            //string name = gm.commodityList.Data [id].name;
            string name = Localizer.Translate ( gm.commodityList.Data [id].name );
            int quantity = pm.storage.GetQuantity ( id );
            int price = pm.commodityItem.GetData ( id ).sale;
            int shift = (int)pm.calender.GetShift ();
            int popular = (int)pm.commodityItem.GetData ( id ).popular [shift];
            Icon.sprite = SpritePath.Commodity [id];
            DragIcon.sprite = SpritePath.Commodity [id];
            Name.text = name;
            Quantity.text = quantity.ToString ();
            Price.text = price.ToString ( "N0" );
            Popular.sprite = SpritePath.CommodityPopular_W [popular];
            Lock.SetActive ( false );
            gameObject.name = "commodity" + commodityID.ToString ();
            SoldOut.gameObject.SetActive ( quantity == 0 );
            Sold.SetActive ( sold );
        }

        public void ClearData ()
        {
            commodityID = -1;
            ItemImage.sprite = NormalSpr;
            Sprite.SetActive ( false );
            Icon.sprite = SpritePath.None;
            Name.text = "";
            Quantity.text = "";
            Price.text = "";
            Lock.SetActive ( true );
            Popular.sprite = SpritePath.None;
            gameObject.name = "None";
            SoldOut.gameObject.SetActive ( false );
            Sold.SetActive ( false );
        }

        public void SoldBool ( bool sold )
        {
            Sold.SetActive ( sold );
        }

        public void TargetBool ( bool target )
        {
             Target.SetActive ( target );
        }

        public void OnPointerDown ( PointerEventData eventData )
        {
            if ( SoldOut.gameObject.activeSelf )
                return;
            if ( commodityID == -1 )
                return;
            if ( StoryManager.self != null && StoryManager.self.tsm.UseSetupCommodity == false )
                return;

            buildSetupCommodity.DragCommodityWithItem ( commodityID , this );
            TargetBool ( true );
        }

        public int GetCommodityID ()
        {
            return commodityID;
        }
    }
}
