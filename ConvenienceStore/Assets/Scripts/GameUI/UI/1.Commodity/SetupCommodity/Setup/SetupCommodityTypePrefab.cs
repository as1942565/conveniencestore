﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class SetupCommodityTypePrefab : MonoBehaviour
    {
        public GameObject Frame;
        public Image TypeImage;
        public Button Button;
    }
}
