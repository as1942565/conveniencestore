﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;

namespace UI
{
    public class BuildSetupCommodity : BaseBuildPanel
    {
        public Image OKImg;
        public Image BackImg;

        public Transform ItemSpace;
        public Scrollbar scrollbar;
        
        [SerializeField]
        private int commodityID = -1;

        private int quantity = 21;
        private float distance_L = 95f;
        private int typeLength = 3;
        private float distance_T = 235f;

        private const string SetupCommodityPath = "GameUI/Commodity/SetupCommodity/";
        private const string TypePath = "GameUI/Commodity/Type";

        Prefab.SetupCommodityTypePrefab [] Type;
        Prefab.SetupCommodityItemPrefab [] Item;

        SetupCommodityManager manager;
        public void GetManager ( SetupCommodityManager manager )
        {
            this.manager = manager;
        }

        public override void Build ()
        {
            Type = new Prefab.SetupCommodityTypePrefab [typeLength];
            Item = new Prefab.SetupCommodityItemPrefab [quantity];

            for ( int i = 0; i < quantity; i++ )
            {
                Prefab.SetupCommodityItemPrefab item = Instantiate ( Resources.Load<Prefab.SetupCommodityItemPrefab> ( "Prefabs/" + SetupCommodityPath + "Item" ) , ItemSpace );
                item.transform.localPosition += new Vector3 ( 0 , -distance_L * i , 0 );
                item.GetBuildSetupCommodity ( this );
                Item [i] = item;
            }
            Sprite [] typeSprite = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + TypePath );
            for ( int i = 0; i < typeLength; i++ )
            {
                Prefab.SetupCommodityTypePrefab type = Instantiate ( Resources.Load<Prefab.SetupCommodityTypePrefab> ( "Prefabs/" + SetupCommodityPath + "TypeButton" ) , transform );
                type.transform.localPosition += new Vector3 ( distance_T * i , 0 , 0 );
                type.TypeImage.sprite = typeSprite [i];
                type.Button.onClick.AddListener ( GenerateType ( i ) );
                type.name = "Type" + i.ToString ();
                Type [i] = type;
            }
            isBuild = true;
        }

        public override void Init ()
        {
            SwitchType ( GameData.Commodity.CommodityType.food );

            Sprite [] typeSprite = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + TypePath );
            for ( int i = 0; i < Type.Length; i++ )
            {
                Type [i].TypeImage.sprite = typeSprite [i];
            }

            OKImg.sprite = SpritePath.OKBtn2 ();
            BackImg.sprite = SpritePath.BackBtn2 ();
        }

        UnityAction GenerateType ( int id )
        {
            return () =>
            {
                SwitchType ( (GameData.Commodity.CommodityType)id );
                PlayButtonAudio.self.PlayCommoditySwitch ();
            };
        }
        void SwitchType ( GameData.Commodity.CommodityType commodityType )
        {
            scrollbar.value = 1;
            for ( int t = 0; t < Type.Length; t++ )
            {
                Type [t].Frame.SetActive ( t == (int)commodityType );
            }
            ShowCommodity ( commodityType );
        }
        void ShowCommodity ( GameData.Commodity.CommodityType commodityType )
        {
            List<int> unlockID = new List<int> ();
            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )
            {
                if ( !pm.commodityItem.GetData ( i ).unlock && gm.commodityList.Data [i].type != GameData.Commodity.CommodityType.special )
                    continue;
                if ( gm.commodityList.Data [i].type == commodityType && gm.commodityList.Data [i].type != GameData.Commodity.CommodityType.special )
                    unlockID.Add ( i );
                if ( (int)commodityType + 1 == (int)gm.commodityList.Data [i].specialType && pm.storage.GetQuantity ( i ) > 0 )
                    unlockID.Add ( i );
            }
            for ( int i = 0; i < Item.Length; i++ )
            {
                if ( i < unlockID.Count )
                {
                    int id = unlockID [i];
                    Item [i].UpdateData ( id , gm.commodityList.Data[id].type , gm , pm , manager.cabinet.GetSoldBool ( id ) );
                }
                else
                    Item [i].ClearData ();
            }
        }

        public void DragCommodityWithItem ( int commodityID , Prefab.SetupCommodityItemPrefab item )
        {
            this.commodityID = commodityID;
            manager.dragIcon.DragIcon ( commodityID , item , null );

            int type = (int)gm.commodityList.Data [commodityID].type;
            manager.cabinet.CanPutCabinet ( type );
        }
        public void DragCommodityWithCabinet ( int commodityID , Prefab.CabinetPrefab cabinet )
        {
            this.commodityID = commodityID;
            manager.dragIcon.DragIcon ( commodityID , null , cabinet );

            int type = (int)gm.commodityList.Data [commodityID].type;
            manager.cabinet.CanPutCabinet ( type );
        }

        public void ResetSold ()
        {
            for ( int i = 0; i < Item.Length; i++ )
            {
                int id = Item [i].GetCommodityID ();
                if ( id == -1 )
                    continue;
                Item [i].SoldBool ( manager.cabinet.GetSoldBool ( id ) );
            }
        }
        public Prefab.SetupCommodityItemPrefab [] GetItem ()
        {
            return Item;
        }

        public void CancelSold ( int id )
        {
            for ( int i = 0; i < Item.Length; i++ )
            {
                if ( Item [i].GetCommodityID () == id )
                {
                    Item [i].SoldBool ( false );
                    break;
                }
            }
        }
    }
}