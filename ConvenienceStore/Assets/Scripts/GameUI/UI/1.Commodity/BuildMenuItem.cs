﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class BuildMenuItem : BaseBuildPanel
    {
        public static BuildMenuItem self;
        private void Awake ()
        {
            self = this;
        }

        public GameObject Shield;
        public Transform MenuSpace;
        public Button CloseBtn;

        private IBuild [] Panel;

        private float distance = 180f;
        private string [] itemName = new string [] { "Research" , "Upgrade" , "SetupCommodity" , "Restock" };
        private bool [] showAP = new bool [] { true , false , false , false };

        private const string MenuItemPath = "GameUI/Commodity/MenuItem";

        Prefab.MenuItemPrefab [] Item;

        public override void Init ()
        {
            SwitchType ( -1 );
            Item [3].Mask.SetActive ( pm.calender.GetShift () != Calender.Shift.morning );
            Item [3].Button.enabled = pm.calender.GetShift () == Calender.Shift.morning;

            Sprite [] sprites = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + MenuItemPath );
            for ( int i = 0; i < Item.Length; i++ )
            {
                Item [i].UpdateData ( sprites[i] , showAP [i] );
            }
        }

        public void OpenRestock ()
        {
            Item [3].Mask.SetActive ( false );
            Item [3].Button.enabled = true;
        }

        public override void Build ()
        {
            Panel = new IBuild [itemName.Length];
            this.Item = new Prefab.MenuItemPrefab [itemName.Length];
            Transform panel = transform.Find ( "Panel" );
            for ( int i = 0; i < itemName.Length; i++ )
            {
                Transform tmp = panel.Find ( itemName [i] );
                if ( tmp != null )
                {
                    IBuild b = tmp.GetComponent<IBuild> ();
                    Panel [i] = b;
                }
            }
            Prefab.MenuItemPrefab [] Item = new Prefab.MenuItemPrefab [itemName.Length];
            Sprite [] sprites = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + MenuItemPath );
            for ( int i = 0; i < itemName.Length; i++ )
            {
                Prefab.MenuItemPrefab item = Instantiate ( Resources.Load<Prefab.MenuItemPrefab> ( "Prefabs/" + MenuItemPath ) , MenuSpace );
                item.name = itemName [i];
                item.transform.localPosition += new Vector3 ( 0 , -distance * i , 0 );
                item.UpdateData ( sprites[i] , showAP [i] );
                item.Button.onClick.AddListener ( GenerateType ( i ) );
                this.Item[i] = item;
            }
            CloseBtn.onClick.AddListener ( CloseWindow );
            isBuild = true;
        }

        UnityAction GenerateType ( int id )
        {
            return () =>
            {
                SwitchType ( id );
                PlayButtonAudio.self.PlayClick ();
            };
        }
        public void SwitchType ( int id )
        {
            switchType ( id );
            if ( BuildRestock.self != null )
                BuildRestock.self.Reset ();
        }
        void CloseWindow ()
        {
            PlayButtonAudio.self.PlayCommodityX ();
            MainPanel.self.Close ();
            SetupCommodityManager.updateBool = false;
            BuildRestock.updateBool = false;
        }
        public IEnumerator CkeckUpdate ( int id )
        {
            while ( MessageBox.self.wait )
            {
                yield return null;
            }
            if ( !MessageBox.self.yes )
                yield break;

            SetupCommodityManager.updateBool = false;
            BuildRestock.updateBool = false;
            switchType ( id );
        }
        public void switchType ( int id )
        {
            for ( int i = 0; i < Panel.Length; i++ )
            {
                Item [i].Frame.SetActive ( i == id );
                if ( i != id )
                {
                    Panel [i].Close ();
                    continue;
                }
                Panel [i].Open ();
            }
            MenuSpace.gameObject.SetActive ( id != 2 );
        }

        public override void Close ()
        {
            base.Close (); 
            MenuSpace.gameObject.SetActive ( true );
        }
    }
}
