﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class RestockItemPrefab : MonoBehaviour
    {
        public int commodityID;
        public Image Icon;
        public Text QuantityText;
        public Text CostText;
        public Text RestockText;
        public Text WaitRestockText;
        public Text NameText;
        public Text TotalText;
        public GameObject Frame;
        public Button Button;

        public void UpdateWaitRestockText ( int num , PlayerDataManager pm , GameDataManager gm )
        {
            WaitRestockText.text = "+" + num.ToString ();
            WaitRestockText.gameObject.SetActive ( num > 0 );
            int restockInt = pm.purchase.GetDefinePurchase ( commodityID );
            int waitRestockInt = pm.purchase.GetWaitPurchase ( commodityID );
            int costInt = gm.commodityList.Data [commodityID].cost;
            string total = ( ( restockInt + waitRestockInt ) * costInt ).ToString ( "N0" ) + Localizer.Translate ( "元" );
            TotalText.text = total;
        }

        public void UpdateData ( int id , PlayerDataManager pm , GameDataManager gm )
        {
            if ( id < 0 )
            {
                ClearData ( id );
                return;
            }
            commodityID = id;
            gameObject.name = "commodity" + commodityID.ToString ();
            string quantity = pm.storage.GetQuantity ( id ).ToString () + Localizer.Translate ( "個" );
            int costInt = gm.commodityList.Data [id].cost;
            string cost = costInt.ToString () + Localizer.Translate ( "元" );
            int restockInt = pm.purchase.GetDefinePurchase ( id );
            string restock = restockInt.ToString () + Localizer.Translate ( "個" );
            int waitRestockInt = pm.purchase.GetWaitPurchase ( id );
            string waitRestock = waitRestockInt.ToString ();
            //string name = gm.commodityList.Data [id].name;
            string name = Localizer.Translate ( gm.commodityList.Data [id].name );
            

            Icon.sprite = SpritePath.Commodity[id];
            QuantityText.text = quantity;
            CostText.text = cost;
            RestockText.text = restock;
            int num;
            if ( int.TryParse ( waitRestock , out num ) )
            {
                UpdateWaitRestockText ( num , pm , gm );
            }
            else
                WaitRestockText.gameObject.SetActive ( false );
            NameText.text = name;
            
            Button.enabled = true;
        }

        public void ClearData ( int id )
        {
            gameObject.name = "None";
            Icon.sprite = SpritePath.None;
            QuantityText.text = "";
            CostText.text = "";
            RestockText.text = "";
            WaitRestockText.gameObject.SetActive ( false );
            NameText.text = "";
            TotalText.text = "";
            Button.enabled = false;
        }
    }
}
