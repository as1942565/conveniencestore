﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class BuildQuantity : BaseBuildPanel
    {
        private float [] pos = new float [] { 620f , 400f , 705f , 315f };
        private float [] width = new float [] { 66.5f , 66.5f , 77.5f , 77.5f };
        private float [] height = new float [] { 54f , 54f , 47.5f , 47.5f };

        private const string path = "GameUI/Commodity/Restock/Quantity/";

        Prefab.QuantityPrefab [] Item;
        Sprite [] quantitySpr;
        Sprite [] quantityBackSpr;

        private int itemID;
        public void GetItemID ( int itemID )
        {
            this.itemID = itemID;
        }

        private BuildRestock buildRestock;
        public void GetBuildRestock ( BuildRestock buildRestock )
        {
            this.buildRestock = buildRestock;
        }

        public override void Build ()
        {
            quantitySpr = Resources.LoadAll<Sprite> ( "Sprites/Common/" + path + "Quantity" );
            quantityBackSpr = Resources.LoadAll<Sprite> ( "Sprites/Common/" + path + "QuantityBack" );
            Item = new Prefab.QuantityPrefab [pos.Length];
            for ( int i = 0; i < Item.Length; i++ )
            {
                Prefab.QuantityPrefab item = Instantiate ( Resources.Load<Prefab.QuantityPrefab> ( "Prefabs/" + path + "Quantity" ) , transform );
                item.buildQuantity = this;
                item.FrameImage.sprite = quantitySpr [i];
                item.FrameImage.rectTransform.sizeDelta = new Vector2 ( width [i] , height [i] );
                item.BackImage.sprite = quantityBackSpr [i];
                item.BackImage.rectTransform.sizeDelta = new Vector2 ( width [i] , height [i] );
                item.transform.localPosition = new Vector2 ( pos[i] , -300f );
                item.name = "Btn" + i.ToString ();
                item.buildRestock = buildRestock;
                Item [i] = item;
            }
            buildRestock.OnPurchaseChanged += OnPurchaseUpdated;
            isBuild = true;

            Item [0].purchase = 1;
            Item [1].purchase = -1;
            Item [2].purchase = 25;
            Item [3].purchase = -25;
        }
        public override void Destroy ()
        {
            base.Destroy ();
            buildRestock.OnPurchaseChanged -= OnPurchaseUpdated;
        }

        public event System.Action<int> OnStockChanged;
        public void ChangeStock ( int num )
        {
            OnStockChanged?.Invoke ( num );
        }

        public override void Init ()
        {
            int max = pm.purchase.GetMaxPurchaseNumByOne ();
            //Item [2].purchase = max;
            //Item [3].purchase = -max;
        }

        void OnPurchaseUpdated ( int purchase )
        {
            if ( purchase == -1 )
            {
                for ( int i = 0; i < Item.Length; i++ )
                {
                    Item [i].Frame.SetActive ( false );
                }
                return;
            }

            int before = ( pm.storage.GetTotalItem () + pm.purchase.GetAllDefiningNum () );
            int maxStock = pm.storage.GetMaxStorage ();
            if ( before > maxStock )
            {
                for ( int i = 0; i < Item.Length; i++ )
                {
                    Item [i].Frame.SetActive ( false );
                }
                return;
            }

            int max = pm.purchase.GetMaxPurchaseNumByOne ();
            Item [0].Frame.SetActive ( purchase < max );
            Item [1].Frame.SetActive ( purchase > 0 );
            if ( StoryManager.self.EnableTeach )
                return;
            Item [2].Frame.SetActive ( purchase < max );
            Item [3].Frame.SetActive ( purchase > 0 );
        }
    }
}