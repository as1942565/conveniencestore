﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

namespace Prefab
{
    public class QuantityPrefab : MonoBehaviour , IPointerDownHandler , IPointerUpHandler
    {
        public UI.BuildRestock buildRestock;
        public UI.BuildQuantity buildQuantity;
        public int id;
        public int purchase;
        public Image BackImage;
        public Image FrameImage;
        public GameObject Frame;

        private bool ClickBool = false;
        private float CD = 0.2f;
        private float MaxCD;

        AudioSource audioPlayer;

        void Update ()
        {
            if ( Input.GetMouseButton ( 1 ) )
            {
                if ( audioPlayer != null )
                    Destroy ( audioPlayer.gameObject );
                AudioSource [] audios = EffectManager.self.transform.GetComponentsInChildren<AudioSource> ();
                for ( int i = 0; i < audios.Length; i++ )
                {
                    Destroy ( audios[i].gameObject );
                }
            }

            if ( StoryManager.self.TeachBool && Mathf.Abs ( purchase ) > 1 )
                return;
            if ( StoryManager.self.tsm.LockRestockBtn )
                Init ();
            if ( Time.time > MaxCD && ClickBool )
            {
                buildQuantity.ChangeStock ( purchase );
                int id = buildRestock.GetCommoidtyID ();
                if ( Action.Restock.CheckMaxPurchase ( id , GameLoopManager.instance.gm.commodityList.Data [id].cost , purchase , GameLoopManager.instance.pm , GameLoopManager.instance.gm ) == 0 )
                {
                    Init ();
                }
                MaxCD = CD + Time.time;
                CD = 0.05f;
            }
        }

        public void OnPointerDown ( PointerEventData eventData )
        {
            if ( StoryManager.self.tsm.LockRestockBtn )
                return;
            if ( buildRestock.GetCommoidtyID () == -1 )
                return;
            if ( FrameImage.gameObject.activeSelf == false )
                return;
            PlayerDataManager pm = GameLoopManager.instance.pm;
            int before = ( pm.storage.GetTotalItem () + pm.purchase.GetAllDefiningNum () );
            int maxStock = pm.storage.GetMaxStorage ();
            if ( before > maxStock )
            {
                UI.MessageBox.Notice ( "庫存已滿。" );
                return;
            }
            PlayAudio ();
            Frame.SetActive ( false );
            ClickBool = true;
            UI.BuildRestock.updateBool = true;
        }

        void PlayAudio ()
        {
            if ( purchase == 1 )
                audioPlayer = PlayButtonAudio.self.PlayCustom ( "Audios/Effect/GameUI/" , "AddCommodity" , true );
            else if ( purchase == -1 )
                audioPlayer = PlayButtonAudio.self.PlayCustom ( "Audios/Effect/GameUI/" , "RemoveCommodity" , true );

            if ( audioPlayer == null )
                return;
            audioPlayer.pitch = 1;
            audioPlayer.DOPitch ( 2 , 1 ).SetEase ( Ease.Linear );
        }

        void Init ()
        {
            if ( audioPlayer != null ) 
                Destroy ( audioPlayer.gameObject );
            ClickBool = false; 
            CD = 0.2f;
            MaxCD = Time.time;
        }

        public void OnPointerUp ( PointerEventData eventData )
        {
            if ( buildRestock.GetCommoidtyID () == -1 )
                return;
            Init ();
        }
    }
}