﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class RestockTypePrefab : MonoBehaviour
    {
        public Image TypeImage;
        public GameObject Frame;
        public Button Button;
    }
}
