﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class BuildRestock : BaseBuildPanel
    {
        public static BuildRestock self;
        private void Awake ()
        {
            self = this;
        }

        public static bool updateBool;

        public Image StockBackground;
        public Image OKImg;
        public Image background;
        public BuildQuantity buildQuantity;
        public BuildReview buildReview;
        public Scrollbar scrollbar;
        public Transform LatticeSpace;
        public Transform TypeSpace;
        public Text BeforeStockText;
        public Text MaxStockText;
        public Text AfterStockText;
        public Text QuantityText;
        public Button BackBtn;
        
        [SerializeField]
        private int itemID = -1;
        [SerializeField]
        private int commodityID = -1;

        private int quantity = 17;
        private float distance = 72f;
        private float [] typePosX = new float [] { -280.6f , -8.4f , 269.1f };

        private const string ResotckPath = "GameUI/Commodity/Restock/";
        private const string TypePath = "GameUI/Commodity/Type";

        Prefab.RestockTypePrefab [] Type;
        Prefab.RestockItemPrefab [] Item;

        public override void Init ()
        {
            SwitchType ( GameData.Commodity.CommodityType.food );

            string before = ( pm.storage.GetTotalItem () + pm.purchase.GetAllDefiningNum () ).ToString ();
            string maxStock = pm.storage.GetMaxStorage ().ToString ();
            BeforeStockText.text = before;
            MaxStockText.text = maxStock;
            AfterStockText.text = "";
            QuantityText.text = "0";
            updateBool = false;

            Sprite [] typeSprite = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + TypePath );
            for ( int i = 0; i < Type.Length; i++ )
            {
                Type [i].TypeImage.sprite = typeSprite [i];
            }

            background.sprite = Resources.Load<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + ResotckPath + "Background" );

            OKImg.sprite = SpritePath.OKBtn ();
            StockBackground.sprite = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}{ResotckPath}StockBackground" );
        }

        public override void Build ()
        {
            Type = new Prefab.RestockTypePrefab [typePosX.Length];
            Item = new Prefab.RestockItemPrefab [quantity];

            Sprite [] typeSprite = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + TypePath );
            for ( int i = 0; i < typePosX.Length; i++ )
            {
                Prefab.RestockTypePrefab type = Instantiate ( Resources.Load<Prefab.RestockTypePrefab> ( "Prefabs/" + ResotckPath + "TypeButton" ) , TypeSpace );
                type.transform.localPosition = new Vector2 ( typePosX [i] , 410f );
                type.TypeImage.sprite = typeSprite [i];
                type.Button.onClick.AddListener ( GenerateType ( i ) );
                type.gameObject.name = "Type" + i.ToString ();
                Type [i] = type;
            }

            for ( int i = 0; i < quantity; i++ )
            {
                Prefab.RestockItemPrefab item = Instantiate ( Resources.Load<Prefab.RestockItemPrefab> ( "Prefabs/" + ResotckPath + "Item" ) , LatticeSpace );
                item.transform.localPosition += new Vector3 ( 0 , -distance * i , 0 );
                item.Button.onClick.AddListener ( GenerateCommodity ( item , i ) );
                Item [i] = item;
            }

            buildQuantity.OnStockChanged += OnStockUpdated;
            buildQuantity.GetBuildRestock ( this );
            buildQuantity.Open ();
            isBuild = true;
            BackBtn.onClick.AddListener ( GenerateBack () );
        }
        public override void Destroy ()
        {
            base.Destroy ();
            buildQuantity.OnStockChanged -= OnStockUpdated;
        }

        UnityAction GenerateType ( int id )
        {
            return () =>
            {
                SwitchType ( (GameData.Commodity.CommodityType)id );
                PlayButtonAudio.self.PlayCommoditySwitch ();
            };
        }

        GameData.Commodity.CommodityType commodityType;
        void SwitchType ( GameData.Commodity.CommodityType commodityType )
        {
            this.commodityType = commodityType;
            scrollbar.value = 1;
            SwitchCommodity ( -1 , -1 );
            QuantityText.text = "0";
            for ( int i = 0; i < Type.Length; i++ )
            {
                Type [i].Frame.SetActive ( i == (int)commodityType );
            }
            ShowCommodity ( commodityType );
        }
        public void SwitchType ()
        {
            SwitchType ( commodityType );
        }
        void ShowCommodity ( GameData.Commodity.CommodityType commoityType )
        {
            List<int> unlockID = new List<int> ();
            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )
            {
                if ( pm.commodityItem.GetData ( i ).unlock && gm.commodityList.Data [i].type == commoityType )
                    unlockID.Add ( i );
            }
            for ( int i = 0; i < Item.Length; i++ )
            {
                if ( i < unlockID.Count )
                {
                    int id = unlockID [i];
                    Item [i].UpdateData ( id , pm , gm );
                }
                else
                    Item [i].ClearData ( -1 );
            }
        }

        UnityAction GenerateCommodity ( Prefab.RestockItemPrefab item , int itemID )
        {
            return () =>
            {
                SwitchCommodity ( item.commodityID , itemID );
                PlayButtonAudio.self.PlayCommodityClick ();
            };
        }
        void SwitchCommodity ( int commodityID , int itemID )
        {
            this.commodityID = commodityID;
            this.itemID = itemID;
            for ( int i = 0; i < Item.Length; i++ )
            {
                Item [i].Frame.SetActive ( i == itemID );
            }
            UpdatePurchase ();
        }

        void UpdatePurchase ()
        {
            int waitPurchase = -1;
            if ( itemID >= 0 )
            {
                waitPurchase = pm.purchase.GetWaitPurchase ( commodityID );
                QuantityText.text = waitPurchase.ToString ();
            }
            OnPurchaseChanged?.Invoke ( waitPurchase );
        }

        public event System.Action<PlayerDataManager , GameDataManager> OnStockChanged;
        public event System.Action<int> OnPurchaseChanged;
        void OnStockUpdated ( int num )
        {
            if ( commodityID == -1 )
                return;
            num = Action.Restock.CheckMaxPurchase ( commodityID , gm.commodityList.Data[commodityID].cost , num , pm , gm );
            pm.purchase.AddWaitPurchase ( commodityID , num );
            UpdatePurchase ();
            int waitQuantity = pm.purchase.GetWaitPurchase ( commodityID );
            Item [itemID].UpdateWaitRestockText ( waitQuantity , pm , gm );

            OnStockChanged?.Invoke ( pm , gm );
            OnPurchaseChanged?.Invoke ( waitQuantity );
        }

        public void TeachOnStockUpdated ( int commodityID , int quantity )
        {
            if ( commodityID == -1 )
                return;
            pm.purchase.ClearWaitPurchase ( commodityID );
            pm.purchase.AddWaitPurchase ( commodityID , quantity );
            UpdatePurchase ();
            Item [itemID].UpdateWaitRestockText ( quantity , pm , gm );

            OnStockChanged?.Invoke ( pm , gm );
            OnPurchaseChanged?.Invoke ( quantity );
        }

        public void OKBtn ()
        {
            BuildRestock.updateBool = false;
            List<int> commodityIDs = new List<int> ();
            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )
            {
                int waitQuantity = pm.purchase.GetWaitPurchase ( i );
                if ( pm.purchase.GetWaitPurchase ( i ) > 0 )
                {
                    commodityIDs.Add ( i );
                }
            }
            if ( commodityIDs.Count > 0 || StoryManager.self.EnableTeach == true )
            {
                buildReview.SetIDs ( commodityIDs.ToArray () );
                buildReview.Open ();
                return;
            }
            Back ();
        }
        UnityAction GenerateBack ()
        {
            return () =>
            {
                PlayButtonAudio.self.PlayCommodityCancel ();
                Back ();
            };
        }
        public void Back ()
        {
            MainPanel.self.SwitchPanel ( 0 );
            Reset ();
        }

        public void Reset ()
        {
            updateBool = false;
            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )
            {
                pm.purchase.ClearWaitPurchase ( i );
            }
        }

        public int GetCommoidtyID ()
        {
            return commodityID;
        }
    }
}