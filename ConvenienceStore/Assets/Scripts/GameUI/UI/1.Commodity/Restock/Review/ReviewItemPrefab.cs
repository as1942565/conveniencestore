﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace Prefab
{
    public class ReviewItemPrefab : MonoBehaviour
    {
        public Text commodityNameText;
        public Text quantityText;
        public Text totalText;
        public Button AddButton;
        public Button RemoveButton;

        public int id;

        public void UpdateData ( int id , GameDataManager gm , PlayerDataManager pm )
        {
            if ( id < 0 || id >= gm.commodityList.Data.Length )
            {
                NoData ();
                return;
            }
            ShowName ( id , gm );
            ShowTotal ( id , gm , pm );
            AddButton.gameObject.SetActive ( true );
            RemoveButton.gameObject.SetActive ( true );

            AddButton.onClick.RemoveAllListeners ();
            RemoveButton.onClick.RemoveAllListeners ();
            AddButton.onClick.AddListener ( GenerateAdd ( id ) );
            RemoveButton.onClick.AddListener ( GenerateRemove ( id ) );
            name = "Item" + id.ToString ();
        }

        void ShowName ( int id , GameDataManager gm )
        {
            //string name = gm.commodityList.Data [id].name;
            string name = Localizer.Translate ( gm.commodityList.Data [id].name );
            commodityNameText.text = name;
        }
        void ShowTotal ( int id , GameDataManager gm , PlayerDataManager pm )
        {
            int quantity = pm.purchase.GetWaitPurchase ( id );
            if ( quantityText != null )
                quantityText.text = quantity.ToString ();
            int total = gm.commodityList.Data [id].cost * quantity;
            if ( totalText != null )
            totalText.text = total.ToString ( "N0" ) + Localizer.Translate ( "元" );
        }

        void NoData ()
        {
            commodityNameText.text = "";
            quantityText.text = "";
            totalText.text = "";
            AddButton.gameObject.SetActive ( false );
            RemoveButton.gameObject.SetActive ( false );
        }
        UnityAction GenerateAdd ( int index )
        {
            return () =>
            {
                AddBtn ( index );
            };
        }

        public void AddBtn ( int index )
        {
            GameDataManager gm = GameLoopManager.instance.gm;
            PlayerDataManager pm = GameLoopManager.instance.pm;

            if ( pm.purchase.GetWaitPurchase ( index ) >= pm.purchase.GetMaxPurchaseNumByOne () )
                return;
            int allWaittingNum = pm.purchase.GetAllWaittingNum ();
            int allDefiningNum = pm.purchase.GetAllDefiningNum ();
            int storageRemain = pm.storage.GetStorageRemain () - allDefiningNum - allWaittingNum;
            if ( storageRemain <= 0 )
                return;
            pm.purchase.AddWaitPurchase ( index , 1 );
            ShowTotal ( index , gm , pm );
            UI.BuildReview.self.StockChanged ( pm , gm );
        }
        UnityAction GenerateRemove ( int index )
        {
            return () =>
            {
                RemoveBtn ( index );
            };
        }
        public void RemoveBtn ( int index )
        {
            GameDataManager gm = GameLoopManager.instance.gm;
            PlayerDataManager pm = GameLoopManager.instance.pm;
            
            if ( pm.purchase.GetWaitPurchase ( index ) <= 0 )
                return;
            pm.purchase.AddWaitPurchase ( index , -1 );
            ShowTotal ( index , gm , pm );
            UI.BuildReview.self.StockChanged ( pm , gm );
        }
    }
}