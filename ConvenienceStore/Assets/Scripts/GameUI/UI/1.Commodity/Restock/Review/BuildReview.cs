﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class BuildReview : BaseBuildPanel
    {
        public static BuildReview self;
        void Awake ()
        {
            self = this;
        }

        public Image Background;
        public Scrollbar scrollbar;
        public Transform ItemSpace;
        public Button OKBtn;
        public Button BackBtn;
        
        private float distance = 41f;

        Prefab.ReviewItemPrefab [] items;

        private const string path = "GameUI/Commodity/Restock/Review/";

        private int [] commodityIDs;
        public void SetIDs ( int [] ids )
        {
            commodityIDs = new int [ids.Length];
            commodityIDs = ids;
        }

        public int GetID ( int id )
        {
            return commodityIDs [id];
        }

        public override void Build ()
        {
            OKBtn.onClick.AddListener ( GenerateOK () );
            BackBtn.onClick.AddListener ( GenerateBack () );
            isBuild = true;
        }
        
        UnityAction GenerateOK ()
        {
            return () =>
            {
                OK ();
            };
        }
        void OK ()
        {
            BuildRestock.updateBool = false;
            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )
            {
                int waitQuantity = pm.purchase.GetWaitPurchase ( i );
                if ( pm.purchase.GetWaitPurchase ( i ) > 0 )
                {
                    Action.Restock.Run ( i , waitQuantity , gm , pm );
                }
            }
            NextShift.Restock.Run ( pm , gm );
            PlayButtonAudio.self.PlayCommodityOK ();
            PlayButtonAudio.self.PlayCustom ( "Audios/Effect/" , "Coin" , false );
            MainPanel.self.SwitchPanel ( 0 );
            Close ();
        }
        UnityAction GenerateBack ()
        {
            return () =>
            {
                Close ();
            };
        }

        public override void Close ()
        {
            base.Close ();
            BuildRestock.self.SwitchType ();
        }

        public SG.InitOnStart initOnStart;
        public override void Init ()
        {
            scrollbar.value = 1;
            BuildRestock.updateBool = true;
            items = new Prefab.ReviewItemPrefab [commodityIDs.Length];

            initOnStart.UpdateCount ( items.Length );

            Background.sprite = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameUI/Commodity/Restock/Review/Background" );
            OKBtn.GetComponent<Image> ().sprite = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameUI/Commodity/Restock/Review/OKBtn" );
        }

        public event System.Action<PlayerDataManager , GameDataManager> OnStockChanged;

        public void StockChanged ( PlayerDataManager pm , GameDataManager gm )
        {
            OnStockChanged?.Invoke ( pm , gm );
        }
    }
}