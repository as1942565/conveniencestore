﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Observer
{
    public class Restock : BaseObserver
    {
        public UI.BuildRestock restock;
        public UI.BuildReview review;
        public Text MoneyText;
        public Text LessText;
        public Text ResultText;
        public Text IncreaseText;
        public Text AfterStockText;
        public GameObject Arrow;

        protected override void RefreshComponent ( PlayerDataManager pm )
        {
            string money = pm.playerData.GetMoney ().ToString ( "N0" ) + Localizer.Translate ( "元" );

            MoneyText.text = money;
            LessText.text = "";
            ResultText.text = money;
            IncreaseText.text = "";
            AfterStockText.text = "";
            Arrow.SetActive ( false );
        }

        protected override void RegisterObserver ( PlayerDataManager pm )
        {
            restock.OnStockChanged += OnStockUpdated;
            review.OnStockChanged += OnStockUpdated;
        }

        protected override void UnregisterObserver ( PlayerDataManager pm )
        {
            restock.OnStockChanged -= OnStockUpdated;
            review.OnStockChanged -= OnStockUpdated;
        }

        void OnStockUpdated ( PlayerDataManager pm , GameDataManager gm )
        {
            int money = pm.playerData.GetMoney ();
            int less = pm.purchase.GetAllWaittingCost ( gm );
            int increaseInt = pm.purchase.GetAllWaittingNum ();
            string increase = "+" + increaseInt.ToString ();
            string afterStock = ( pm.storage.GetTotalItem () + pm.purchase.GetAllDefiningNum () + increaseInt ).ToString ();

            MoneyText.text = money.ToString ( "N0" ) + Localizer.Translate ( "元" );
            if ( less == 0 )
                LessText.text = "";
            else
                LessText.text = "-" + less.ToString ( "N0" );
            ResultText.text = ( money - less ).ToString ( "N0" ) + Localizer.Translate ( "元" );
            IncreaseText.text = increase;
            IncreaseText.gameObject.SetActive ( increase != "+0" );
            AfterStockText.text = afterStock;
            Arrow.SetActive ( true );
        }
    }
}