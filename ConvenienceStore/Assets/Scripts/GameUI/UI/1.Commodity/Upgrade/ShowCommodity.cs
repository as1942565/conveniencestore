﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class ShowCommodity : BaseBuildPanel
    {
        public int id;

        private const string path = "Prefabs/GameUI/Commodity/Upgrade/Improve/Improve";

        Prefab.ImprovePrefab improve;
       
        public override void Build ()
        {
            improve = Instantiate ( Resources.Load<Prefab.ImprovePrefab> ( path ) , transform );
            improve.transform.localPosition = new Vector3 ( -500 , 0 , 0 );
            improve.transform.localScale = new Vector3 ( 1 , 1 , 1 );
            isBuild = true;
        }

        public override void Init ()
        {
            improve.BeforeData ( id , pm , gm );
        }
        public override void Open ()
        {
            base.Open ();
            closeCalled = false;
        }
        private bool closeCalled =false;
        private int closedid = -1;
        public void CloseAfter ( float second )
        {
            closedid = id;
            closeCalled = true;
            Invoke ( "DelayClose" , second );
        } 

        private void DelayClose ()
        {
            if(closeCalled && closedid == id)
            {
                Close ();
            }
        }
    }
}