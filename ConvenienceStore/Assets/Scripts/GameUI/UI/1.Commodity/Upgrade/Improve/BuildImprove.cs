﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class BuildImprove : BaseBuildPanel
    {
        public Transform EffectCanvas;
        public Button OKBtn;
        public Button BackBtn;
        [SerializeField]
        private int commodityID = -1;

        public const string improvePath = "GameUI/Commodity/Upgrade/Improve/";

        Prefab.ImprovePrefab Before;

        public void GetData ( int commodityID )
        {
            this.commodityID = commodityID;
            if ( commodityID == -1 )
            {
                Close ();
                return;
            }
            Before.BeforeData ( commodityID , pm , gm );

            Prefab.ImprovePrefab After = Instantiate ( Before.gameObject , transform ).GetComponent<Prefab.ImprovePrefab> ();
            After.name = "After";
            After.transform.localPosition = new Vector3 ( 550 , 60 , 0 );
            After.AfterData ( commodityID , pm , gm );
        }

        public override void Init ()
        {

        }

        public override void Build ()
        {
            Before = Instantiate ( Resources.Load<Prefab.ImprovePrefab> ( "Prefabs/" + improvePath + "Improve" ) , transform );
            Before.name = "Before";
            OKBtn.onClick.AddListener ( OK );
            BackBtn.onClick.AddListener ( Back );
            isBuild = true;
        }
        void OK ()
        {
            BaseKeyBoard.Ban = true;
            Invoke ( "Improve" , 0.5f );
            LoadingScene.self.Transitions ( 0.5f , 1f , false );
            PlayButtonAudio.self.PlayCommodityOK ();
        }

        void Improve ()
        {
            MainPanel.self.Close ();
            Prefab.UpgradePrefab upgrade = Instantiate ( Resources.Load<Prefab.UpgradePrefab> ( "Prefabs/" + improvePath + "Upgrade" ) , EffectCanvas );
            int star = pm.commodityItem.GetData ( commodityID ).star;
            GameData.Commodity.PopularAddition [] popular;
            popular = pm.commodityItem.GetData ( commodityID ).popular;
            Vector3 BeforePopular = new Vector3 ( (int)popular [0] , (int)popular [1] , (int)popular [2] );
            for ( int i = 0; i < 3; i++ )                //搜尋所有時段
            {
                GameData.Commodity.UpgradeChance [] chances = gm.commodityList.Data [commodityID].MorningChance;
                switch ( i )
                {
                    case 1:
                        chances = gm.commodityList.Data [commodityID].EveningChance;
                        break;
                    case 2:
                        chances = gm.commodityList.Data [commodityID].NightChance;
                        break;
                }
                pm.commodityItem.RanPopular ( commodityID , i , chances [star].UpgradeRate , chances [star].DowngradeRate );//該商品該星級的3個時段隨機變化
            }
            popular = pm.commodityItem.GetData ( commodityID ).popular;
            Vector3 AfterPopular = new Vector3 ( (int)popular [0] , (int)popular [1] , (int)popular [2] );
            upgrade.UpdateData ( commodityID , star , BeforePopular , AfterPopular );
        }

        void Back ()
        {
            Close ();
        }
    }
}
