﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class ImprovePrefab : MonoBehaviour
    {
        public Image Background;
        public Image RankImage;
        public GameObject [] Stars;
        public Text NameText;
        public Image IconImage;
        public Image TypeImage;
        public Image [] PopularImage;
        public Text PriceText;
        public Text Explanation;

        public void BeforeData ( int id , PlayerDataManager pm , GameDataManager gm )
        {
            Background.sprite = Resources.Load<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + "GameUI/Commodity/Commodity/Background" );
            int rank = gm.commodityList.Data [id].rank;
            int star = pm.commodityItem.GetData ( id ).star;
            //string name = gm.commodityList.Data [id].name;
            string name = Localizer.Translate ( gm.commodityList.Data [id].name );
            int type = (int)gm.commodityList.Data [id].type;
            GameData.Commodity.PopularAddition [] popular = pm.commodityItem.GetData ( id ).popular;
            int [] sale = gm.commodityList.Data [id].price;
            string cost = " / " + gm.commodityList.Data [id].cost;
            //string explanation = gm.commodityList.Data [id].explanation;
            string explanation = Localizer.Translate ( gm.commodityList.Data [id].explanation );

            RankImage.sprite = SpritePath.CommodityRank [rank];
            for ( int i = 0; i < Stars.Length; i++ )
            {
                Stars [i].SetActive ( star > i );
            }
            NameText.text = name;
            IconImage.sprite = SpritePath.Commodity [id];
            TypeImage.sprite = SpritePath.CommodityType [type];
            for ( int i = 0; i < PopularImage.Length; i++ )
            {
                int rate = (int)popular [i];
                PopularImage [i].sprite = SpritePath.CommodityPopular_W [rate];
            }
            PriceText.text = sale[star].ToString () + cost;
            Explanation.text = explanation;
        }
        public void AfterData ( int id , PlayerDataManager pm , GameDataManager gm )
        {
            Background.sprite = Resources.Load<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + "GameUI/Commodity/Commodity/Background" );
            int star = pm.commodityItem.GetData ( id ).star;
            GameData.Commodity commodity = gm.commodityList.Data [id];
            string cost = " / " + gm.commodityList.Data [id].cost;
            int [] sale = commodity.price;

            int [] upChance = new int [3];
            upChance [0] = commodity.MorningChance [star].UpgradeRate;
            upChance [1] = commodity.EveningChance [star].UpgradeRate;
            upChance [2] = commodity.NightChance [star].UpgradeRate;
            GameData.Commodity.PopularAddition [] popular = pm.commodityItem.GetData ( id ).popular;
            for ( int i = 0; i < PopularImage.Length; i++ )
            {
                PopularImage [i].raycastTarget = true;
                PopularImage [i].GetComponent<ShowChance> ().chance = upChance [i];
                int rate = (int)popular [i];
                if ( upChance[i] == 100 )
                {
                    PopularImage [i].sprite = SpritePath.CommodityPopular_R [rate];
                }
                else if ( upChance[i] > 0 )
                {
                    PopularImage [i].sprite = SpritePath.CommodityPopular_B [rate];
                }
            }
            star++;
            for ( int i = 0; i < Stars.Length; i++ )
            {
                Stars [i].SetActive ( star > i );
            }
            string price = $"<color=#ff0000ff>{sale [star].ToString ()}</color>" + cost;
            PriceText.text = price;
        }
    }
}
