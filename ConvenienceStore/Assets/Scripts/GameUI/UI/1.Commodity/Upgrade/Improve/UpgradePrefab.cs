﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class UpgradePrefab : MonoBehaviour
    {
        public Image Background;
        public float Interval;
        public Image RankImage;
        public Text NameText;
        public Image IconImage;
        public Image TypeImage;
        public Image [] PopularImage;
        public GameObject [] Stars;
        public Text PriceText;
        public Text Explanation;
        public Card_Star _Card_Star;
        public Card_Familier _Card_Familier;
        public Card_Price _Card_Price;

        private int id;
        private bool complete = false;

        public void UpdateData ( int id , int star , Vector3 BeforePopular , Vector3 AfterPopular )
        {
            Background.sprite = Resources.Load<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + "GameUI/Commodity/Commodity/Background" );

            this.id = id;
            GameData.Commodity commodity = GameLoopManager.instance.gm.commodityList.Data [id];
            int type = (int)commodity.type;
            //string name = commodity.name;
            string name = Localizer.Translate ( commodity.name );
            int rank = commodity.rank;
            
            string cost = commodity.cost.ToString ();
            //string explanation = commodity.explanation;
            string explanation = Localizer.Translate ( commodity.explanation );

            RankImage.sprite = SpritePath.CommodityRank [rank];
            NameText.text = name;
            IconImage.sprite = SpritePath.Commodity [id];
            TypeImage.sprite = SpritePath.CommodityType [type];
            for ( int i = 0; i < PopularImage.Length; i++ )
            {
                int popular = (int)commodity.salesRate [i];
                PopularImage [i].sprite = SpritePath.CommodityPopular_W [popular];
            }
            PriceText.text = "/ " + cost;
            Explanation.text = explanation;
            if ( _Card_Star == null || _Card_Familier == null || _Card_Price == null )
            {
                Debug.LogWarning ( "缺少改良動畫腳本" );
                return;
            }
            _Card_Star.LevelTop = star + 1;
            _Card_Familier.Original = BeforePopular;
            _Card_Familier.result = AfterPopular;
            _Card_Price.Price_Text.text = commodity.price [star].ToString ();
            _Card_Price.PriceOriginalValue = commodity.price [star];
            _Card_Price.Cost = cost;
            _Card_Price.PriceTopValue = commodity.price [star + 1];
            _Card_Price.New_Price_Text.text = commodity.price [star] + " / " + cost;

            StartCoroutine ( InOrderClick () );
        }

        //依序執行每個部份的Click()
        IEnumerator InOrderClick ()
        {
            yield return new WaitForSeconds ( Interval );
            //進入Card_Star的動畫
            _Card_Star.Click ();

            //Card_Star尚未結束動畫，持續等待
            while ( !_Card_Star.Finished ) { yield return new WaitForFixedUpdate (); }

            //間隔延遲
            yield return new WaitForSeconds ( Interval );

            //Card_Star動畫結束，進入Card_Familier的動畫
            _Card_Familier.Click ();

            //Card_Familier尚未結束動畫，持續等待
            while ( !_Card_Familier.Finished ) { yield return new WaitForFixedUpdate (); }

            //間隔延遲
            yield return new WaitForSeconds ( Interval );

            //Card_Familier動畫結束，進入Card_Price的動畫
            _Card_Price.Click ();
            //間隔延遲
            yield return new WaitForSeconds ( Interval );
            complete = true;
        }
        public void Click ()
        {
            if ( complete == false )
                return;
            LoadingScene.self.Transitions ( 0.5f , 1f , true );
            Invoke ( "UseEnergy" , 0.5f );
        }

        private const string path = "GameUICanvas/MainAction/BuildCommodity";
        void UseEnergy ()
        {
            BaseKeyBoard.Ban = false;
            Destroy ( gameObject );
            Action.Upgrade.Run ( id , GameLoopManager.instance.pm , GameLoopManager.instance.gm , GameLoopManager.instance.nextShift );

            Calender.Shift shift = GameLoopManager.instance.pm.calender.GetShift ();
            if ( shift == Calender.Shift.morning  )
            {
                if ( StoryManager.self != null && !StoryManager.self.EnableTeach )
                    UI.BuildLog.self.OnClosed += OnOpenImprove;
            }
            else
            {
                OnOpenImprove ();
            }
        }
        void OnOpenImprove ()
        {
            UI.MainPanel.self.SwitchPanel ( 0 );
            UI.BuildMenuItem menuItem = GameObject.Find ( path ).GetComponent<UI.BuildMenuItem> ();
            menuItem.SwitchType ( 1 );
            UI.BuildLog.self.OnClosed -= OnOpenImprove;
        }
    }
}
