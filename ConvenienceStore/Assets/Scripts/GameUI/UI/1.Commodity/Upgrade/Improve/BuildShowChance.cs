﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class BuildShowChance : MonoBehaviour
    {
        public static BuildShowChance self;
        private void Awake ()
        {
            self = this;
            gameObject.SetActive ( false );
        }

        public Text text;

        public void UpdateChance ( int chance )
        {
            Vector2 pos = Camera.main.ScreenToViewportPoint ( Input.mousePosition );
            var itempos = new Vector2 ( ( pos.x - 0.5f ) * 1920 + 80 , ( pos.y - 0.5f ) * 1080 + 45 );
            transform.localPosition = itempos;

            this.gameObject.SetActive ( true );
            text.text = chance.ToString () + "%";
        }

        public void Hide ()
        {
            this.gameObject.SetActive ( false );
        }

        private void FixedUpdate ()
        {
            if ( gameObject.activeSelf )
            {
                Vector2 pos = Camera.main.ScreenToViewportPoint ( Input.mousePosition );
                var itempos = new Vector2 ( ( pos.x - 0.5f ) * 1920 + 80 , ( pos.y - 0.5f ) * 1080 + 45 );
                transform.localPosition = itempos;
            }
        }
    }
}