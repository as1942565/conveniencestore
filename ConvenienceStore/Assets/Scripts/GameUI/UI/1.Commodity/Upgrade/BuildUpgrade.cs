﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UI
{
    public class BuildUpgrade : BaseBuildPanel
    {
        public Scrollbar scrollbar;
        public Transform Backgorund;
        public Transform LatticeSpace;
        public BuildImprove BuildImprove;
        public ShowCommodity showCommodity;

        [SerializeField]
        private int type = 0;
        [SerializeField]
        private int commodityID = -1;

        private const string UpgragePath = "GameUI/Commodity/Upgrade/";
        private const string TypePath = "GameUI/Commodity/Type";
        private const string starPath = "Prefabs/GameUI/Commodity/Upgrade/Star";

        private float [] typePosX = new float [] { -265f , 0f , 265f };

        private float distance = 220f;
        private int quantityX = 4;
        private int quantityY = 5;
        private float [] [] starPos = new float [5] [] {new float [] { 0 } ,
                                                        new float [] { 0 } ,
                                                        new float [] { -30 , 30 } ,
                                                        new float [] { -50 , 0 , 50 } ,
                                                        new float [] { -60 , -20 , 20 , 60 } };
        Prefab.UpgradeTypePrefab [] Type;
        Prefab.UpgradeItemPrefab [] Item;


        public override void Init ()
        {
            SwitchType ( GameData.Commodity.CommodityType.food );

            Sprite [] typeSprite = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + TypePath );
            for ( int i = 0; i < typePosX.Length; i++ )
            {
                Type [i].TypeImage.sprite = typeSprite [i];
            }
        }

        public override void Build ()
        {
            Sprite [] typeSprite = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + TypePath );
            Type = new Prefab.UpgradeTypePrefab [typePosX.Length];
            Item = new Prefab.UpgradeItemPrefab [quantityX * quantityY];

            for ( int i = 0; i < typePosX.Length; i++ )
            {
                Prefab.UpgradeTypePrefab type = Instantiate ( Resources.Load<Prefab.UpgradeTypePrefab> ( "Prefabs/" + UpgragePath + "TypeButton" ) , Backgorund );
                type.TypeImage.sprite = typeSprite [i];
                type.transform.localPosition = new Vector2 ( typePosX [i] , 350 );
                type.GetComponent<Button> ().onClick.AddListener ( GenerateType ( i ) );
                type.name = "Type" + i;
                Type [i] = type;
            }

            int id = 0;
            for ( int y = 0; y < quantityY; y++ )
            {
                for ( int x = 0; x < quantityX; x++ )
                {
                    Prefab.UpgradeItemPrefab item = Instantiate ( Resources.Load<Prefab.UpgradeItemPrefab> ( "Prefabs/" + UpgragePath + "Item" ) , LatticeSpace );
                    item.transform.localPosition = new Vector2 ( distance * x  , -distance * y );
                    item.Button.onClick.AddListener ( GenrateCommodity ( item ) );
                    item.showCommodity = showCommodity;
                    Item [id] = item;
                    id++;
                }
            }
            isBuild = true;
        }

        UnityAction GenerateType ( int id )
        {
            return () =>
            {
                SwitchType ( (GameData.Commodity.CommodityType)id );
                PlayButtonAudio.self.PlayCommoditySwitch ();
            };
        }
        void SwitchType ( GameData.Commodity.CommodityType commodityType )
        {
            scrollbar.value = 1;
            type = (int)commodityType;
            SwitchCommodity ( -1 );
            for ( int t = 0; t < Type.Length; t++ )
            {
                Type [t].TypeFrame.SetActive ( t == (int)commodityType );
            }
            ShowCommodity ( commodityType );
        }
        void ShowCommodity ( GameData.Commodity.CommodityType commodityType )
        {
            List<int> unlockID = new List<int> ();
            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )
            {
                if ( pm.commodityItem.GetData ( i ).unlock && gm.commodityList.Data [i].type == commodityType && pm.commodityItem.GetImproveUnlock ( i , gm ) == true )
                    unlockID.Add ( i );
            }
            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )
            {
                if ( pm.commodityItem.GetData ( i ).unlock && gm.commodityList.Data [i].type == commodityType && pm.commodityItem.GetImproveUnlock ( i , gm ) == false )
                    unlockID.Add ( i );
            }
            for ( int i = 0; i < unlockID.Count; i++ )
            {
                int id = unlockID [i];
                Item [i].UpdateData ( id , pm , gm , starPos );
            }
            for ( int i = unlockID.Count; i < Item.Length; i++ )
            {
                Item [i].ClearData ( -1 );
            }
        }

        UnityAction GenrateCommodity ( Prefab.UpgradeItemPrefab item )
        {
            return () =>
            {
                SwitchCommodity ( item.id );
                PlayButtonAudio.self.PlayCommodityClick ();
            };
        }
        void SwitchCommodity ( int commodityID )
        {
            this.commodityID = commodityID;
            BuildImprove.Open ();
            BuildImprove.GetData ( commodityID );
        }
    }
}