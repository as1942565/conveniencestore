﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class UpgradeTypePrefab : MonoBehaviour
    {
        public Image TypeImage;
        public GameObject TypeFrame;
    }
}
