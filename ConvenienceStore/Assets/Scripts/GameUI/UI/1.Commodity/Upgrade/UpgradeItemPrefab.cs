﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Prefab
{
    public class UpgradeItemPrefab : MonoBehaviour , IPointerEnterHandler , IPointerExitHandler 
    {
        public UI.ShowCommodity showCommodity;
        public int id;
        public Image Icon;
        public Image Complete;
        public Image ConditionBox;
        public Text QuantityText;
        public GameObject Condition;
        public GameObject [] Stars;
        public Button Button;

        private const string path = "GameUI/Commodity/Upgrade/";

        public void UpdateData ( int id , PlayerDataManager pm , GameDataManager gm , float [] [] starPos )
        {
            OptionData optionData = GameLoopManager.instance.om.optionData;
            Sprite completeSpr = Resources.Load<Sprite> ( $"Sprites/{optionData.GetLanguageStr ()}{path}Complete" );
            Complete.sprite = completeSpr;
            Sprite conditionBoxSpr = Resources.Load<Sprite> ( $"Sprites/{optionData.GetLanguageStr ()}{path}ConditionQuantity" );
            ConditionBox.sprite = conditionBoxSpr;

            name = "commodity" + id.ToString ();
            if ( id < 0 )
            {
                ClearData ( id );
                return;
            }
            this.id = id;
            int star = pm.commodityItem.GetData ( id ).star;
            bool complete = pm.commodityItem.GetImproveComplete ( id );
            int saleQuantity = pm.commodityItem.GetData ( id ).saleQuantity;
            int saleQuantityAfterUpdate = pm.commodityItem.GetData ( id ).saleQuantityAfterUpdate;
            int targetQuantity = saleQuantity - saleQuantityAfterUpdate;
            int condition = 0;
            if ( star < 3 )
                condition = pm.commodityItem.GetData ( id ).improveCondition [star];
            bool unlock = pm.commodityItem.GetImproveUnlock ( id , gm );
            Sprite icon = gm.commodityList.Data [id].icon;

            for ( int i = 0; i < starPos.Length - 1; i++ )
            {
                Stars [i].SetActive ( star > i );
            }
            for ( int i = 0; i < starPos [star].Length; i++ )
            {
                Stars [i].transform.localPosition = new Vector2 ( starPos [star] [i] , -70 );
            }
            Icon.sprite = SpritePath.Commodity [id];
            Complete.gameObject.SetActive ( complete );
            QuantityText.text = $"{targetQuantity.ToString ()}/{condition}";
            Condition.SetActive ( !unlock );
            Button.enabled = !complete && unlock;
        }

        public void ClearData ( int id )
        {
            name = "None";
            this.id = id;
            Icon.sprite = SpritePath.None;
            Complete.gameObject.SetActive ( false );
            QuantityText.text = "";
            Condition.SetActive ( false );
            for ( int i = 0; i < Stars.Length; i++ )
            {
                Stars [i].SetActive ( false );
            }
            Button.enabled = false;
        }

        
        public void OnPointerEnter ( PointerEventData eventData )
        {
            if ( id == -1 )
                return;
            showCommodity.id = id;
            showCommodity.Open ();
        }

        public void OnPointerExit ( PointerEventData eventData )
        {
            showCommodity.CloseAfter ( 0.5f );
        }

    }
}
