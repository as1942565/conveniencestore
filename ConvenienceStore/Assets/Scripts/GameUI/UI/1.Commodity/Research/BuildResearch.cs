﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class BuildResearch : BaseBuildPanel
    {
        public Image Background;
        public Transform EffectCanvas;
        public Button OK;
        public Button BackBtn;

        [SerializeField]
        private GameData.Commodity.CommodityType commodityType = GameData.Commodity.CommodityType.nothing;
        [SerializeField]
        private int rank = -1;

        private const string ResearchPath = "GameUI/Commodity/Research/";

        private float [] typePosX = new float [] { -263f , -48f , 167f };
        private float [] costPosY = new float [] { -45f , -155f , -265f };

        Sprite [] costSprite;
        Prefab.ResearchTypePrefab [] Type;
        Prefab.ResearchCostPrefab [] Cost;

        public override void Init ()
        {
            SetupCommodityManager.updateBool = false;
            BuildRestock.updateBool = false;
            SwitchType ( GameData.Commodity.CommodityType.nothing );
            SwitchCost ( -1 );

            Sprite [] typeSprite = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + ResearchPath + "TypeIcon" );
            for ( int i = 0; i < Type.Length; i++ )
            {
                Type [i].TypeImage.sprite = typeSprite [i];
            }
            Sprite [] costFrameSprite = Resources.LoadAll<Sprite> ( "Sprites/Common/" + ResearchPath + "MoneyTarget" );
            for ( int i = 0; i < costPosY.Length; i++ )
            {
                Cost [i].FrameImage.sprite = costFrameSprite [i];
            }

            Sprite backgroundSpr = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr()}{ResearchPath}ResearchBackground"  );
            Background.sprite = backgroundSpr;

            costSprite = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + ResearchPath + "Money" );
            
            OK.GetComponent<Image> ().sprite = SpritePath.OKBtn ();
            BackBtn.GetComponent<Image> ().sprite = SpritePath.BackBtn ();
        }

        public override void Build ()
        {
            Type = new Prefab.ResearchTypePrefab [typePosX.Length];
            Cost = new Prefab.ResearchCostPrefab [costPosY.Length];

            for ( int i = 0; i < typePosX.Length; i++ )
            {
                Prefab.ResearchTypePrefab type = Instantiate ( Resources.Load<Prefab.ResearchTypePrefab> ( "Prefabs/" + ResearchPath + "TypeButton" ) , transform );
                type.transform.localPosition = new Vector2 ( typePosX [i] , 250 );
                type.Button.onClick.AddListener ( GenerateType ( i ) );
                type.name = "Type" + i.ToString ();
                Type [i] = type;
            }

            for ( int i = 0; i < costPosY.Length; i++ )
            {
                Prefab.ResearchCostPrefab cost = Instantiate ( Resources.Load<Prefab.ResearchCostPrefab> ( "Prefabs/" + ResearchPath + "CostButton" ) , transform );
                cost.transform.localPosition = new Vector2 ( -45f , costPosY [i] );
                cost.gameObject.SetActive ( false );
                cost.Button.onClick.AddListener ( GenerateCost ( i ) );
                cost.name = "Cost" + i.ToString ();
                Cost [i] = cost;
            }
            isBuild = true;
            BackBtn.onClick.AddListener ( GenerateBack () );
            OK.onClick.AddListener ( GenerateOK () );
        }

        UnityAction GenerateType ( int id )
        {
            return () =>
            {
                SwitchType ( (GameData.Commodity.CommodityType)id );
                PlayButtonAudio.self.PlayCommoditySwitch ();
            };
        }
        void SwitchType ( GameData.Commodity.CommodityType commodityType )
        {
            this.commodityType = commodityType;
            for ( int t = 0; t < Type.Length; t++ )
                Type [t].Frame.SetActive ( t == (int)commodityType );

            bool nothing = commodityType == GameData.Commodity.CommodityType.nothing;
            for ( int rank = 0; rank < Cost.Length; rank++ )
            {
                Cost [rank].gameObject.SetActive ( !nothing );
                if ( nothing )
                    continue;
                bool canResearch = Action.Research.CheckResearch ( rank , (int)commodityType , gm , pm );
                Cost [rank].Button.enabled = canResearch;
                if ( canResearch )
                {
                    Cost[rank].CostImage.sprite = costSprite [rank + 1];
                }
                else
                {
                    Cost [rank].FrameImage.gameObject.SetActive ( false );
                    Cost[rank].CostImage.sprite = costSprite [0];
                    this.rank = -1;
                }
            }
        }

        UnityAction GenerateCost ( int id )
        {
            return () =>
            {
                if ( SwitchCost ( id ) )
                {
                    PlayButtonAudio.self.PlayCommoditySwitch ();
                }
            };
        }
        bool SwitchCost ( int rank )
        {
            bool canPay = false;
            if ( rank >= 0 )
            {
                int ResearchCost = gm.customData.Data.DevelopMoney [rank];
                canPay = Action.Research.CheckMoney ( ResearchCost , pm );
                if ( !canPay )
                {
                    return false ;
                }
            }
            this.rank = rank;
            for ( int r = 0; r < Cost.Length; r++ )
            {
                Cost [r].FrameImage.gameObject.SetActive ( r == rank && canPay );
            }
            return true;
        }

        UnityAction GenerateOK ()
        {
            return () =>
            {
                OKBtn ();
                PlayButtonAudio.self.PlayCommodityOK ();
            };
        }

        public void OKBtn ()
        {
            if ( commodityType == GameData.Commodity.CommodityType.nothing )
            {
                MessageBox.Warning ( "請選擇種類" );
                //MainPanel.self.Close ();
                return;
            }
            if ( rank == -1 )
            {
                MessageBox.Warning ( "請選擇價格" );
                //MainPanel.self.Close ();
                return;
            }
            BaseKeyBoard.Ban = true;
            Invoke ( "Research" , 0.5f );
            LoadingScene.self.Transitions ( 0.5f , 1f , false );
            PlayButtonAudio.self.PlayCustom ( "Audios/Effect/" , "Coin" , false );
        }

        void Research ()
        {
            MainPanel.self.Close ();
            int id = Action.Research.ResearchID ( rank , (int)commodityType , gm , pm );
            if ( id == -1 )
            {
                Debug.LogError ( "無法研發" );
                return;
            }
            Prefab.ResearchPrefab research = Instantiate ( Resources.Load<Prefab.ResearchPrefab> ( "Prefabs/" + ResearchPath + "ResearchAnim" ) , EffectCanvas );
            research.commodityID = id;
            if ( StoryManager.self != null && StoryManager.self.EnableTeach )
            {
                TeachSceneManager.researchID = id;
            }
        }

        UnityAction GenerateBack ()
        {
            return () =>
            {
                PlayButtonAudio.self.PlayCommodityCancel ();
                MainPanel.self.Close ();
            };
        }
    }
}