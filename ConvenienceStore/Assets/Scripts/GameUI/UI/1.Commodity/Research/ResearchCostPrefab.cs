﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class ResearchCostPrefab : MonoBehaviour
    {
        public Image CostImage;
        public Image FrameImage;
        public Button Button;
    }
}
