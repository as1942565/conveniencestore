﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class MenuItemPrefab : MonoBehaviour
    {
        public Image MenuImage;
        public GameObject Frame;
        public GameObject AP;
        public Button Button;
        public GameObject Mask;

        private bool showAP;

        public void UpdateData ( Sprite menuSprite ,  bool showAP )
        {
            this.showAP = showAP;

            MenuImage.sprite = menuSprite;
            AP.SetActive ( showAP );
        }
    }
}
