﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class HireWorkerPrefab : MonoBehaviour
    {
        public int workerID;
        public Image Background;
        public GameObject Target;
        public Button Button;
    }
}
