﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Tools;

namespace UI
{
    public class BuildHireWorker : BaseBuildPanel
    {
        public GameObject Shield;
        public Transform Space;

        public Button OKBtn;
        [SerializeField]
        private List<int> workerIDs = new List<int> ();
        public const string hireWorkerPath = "GameUI/Activity/Business/HireWorker/";

        private string [] Pos = new string [] { "0" , "-400^400" , "-600^0^600" , "-600^-200^200^600" };
        private float [] Size = new float [] { 0.75f , 0.75f , 0.75f , 0.6f };
        Prefab.HireWorkerPrefab [] hireWorker;
        private int num = 3;
        [SerializeField]
        bool [] isClickID;
        private int canHireLength;

        private void Start ()
        {
            GameLoopManager.instance.nextShift.OnHireWorker += Open;
            Close ();
        }
        private void OnDestroy ()
        {
            if ( GameLoopManager.instance == null )
            {
                return;
            }
            GameLoopManager.instance.nextShift.OnHireWorker -= Open;
        }

        public override void Build ()
        {
            OKBtn.onClick.AddListener ( GenerateOK () );
            isBuild = true;
        }
        public override void Init ()
        {
            GameLoopManager.instance.IsHire = true;
            isClickID = new bool [gm.workerList.Data.Length];
            Shield.SetActive ( true );
            Sprite [] WorkerSpr = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr ()  + hireWorkerPath + "HireWorker" );
            List<int> canHireID = pm.hireWorker.GetCanHireID ();
            canHireLength = canHireID.Count;
            canHireID.Shuffle ();
            if ( pm.hireWorker.GetSpecial () == true )
                canHireID [1] = 20;
            num = Mathf.Min ( canHireID.Count , num );
            hireWorker = new Prefab.HireWorkerPrefab [num];
            for ( int i = 0; i < num; i++ )
            {
                string [] pos = Pos [num - 1].Split ( '^' );
                Prefab.HireWorkerPrefab worker = Instantiate ( Resources.Load<Prefab.HireWorkerPrefab> ( "Prefabs/" + hireWorkerPath + "HireWorker" ) , Space );
                worker.transform.localPosition = new Vector3 ( int.Parse ( pos [i] ) , 0 , 0 );
                float scale = Size [num - 1];
                worker.transform.localScale = new Vector3 ( scale , scale , scale );
                int id = canHireID [i];
                worker.workerID = id;
                worker.Background.sprite = WorkerSpr [id];
                worker.Button.onClick.AddListener ( ChooseWorker ( i ) );
                hireWorker [i] = worker;
                worker.gameObject.name = "Worker" + id.ToString ();
            }
            workerIDs.Clear ();
            OKBtn.gameObject.SetActive ( false );
        }

        public void Teach ( int [] workerIDs )
        {
            isClickID = new bool [gm.workerList.Data.Length];
            Shield.SetActive ( true );
            Sprite [] WorkerSpr = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + hireWorkerPath + "HireWorker" );
            List<int> canHireID = new List<int> ();
            for ( int i = 0; i < workerIDs.Length; i++ )
                canHireID.Add ( workerIDs [i] );
            num = workerIDs.Length;
            hireWorker = new Prefab.HireWorkerPrefab [num];
            for ( int i = 0; i < num; i++ )
            {
                string [] pos = Pos [num - 1].Split ( '^' );
                Prefab.HireWorkerPrefab worker = Instantiate ( Resources.Load<Prefab.HireWorkerPrefab> ( "Prefabs/" + hireWorkerPath + "HireWorker" ) , transform );
                worker.transform.localPosition = new Vector3 ( int.Parse ( pos [i] ) , 0 , 0 );
                float scale = Size [num - 1];
                worker.transform.localScale = new Vector3 ( scale , scale , scale );
                int id = canHireID [i];
                worker.workerID = id;
                worker.Background.sprite = WorkerSpr [id];
                worker.Button.onClick.AddListener ( ChooseWorker ( i ) );
                hireWorker [i] = worker;
                worker.gameObject.name = "Worker" + id.ToString ();
            }
        }

        UnityAction ChooseWorker ( int id )
        {
            return () =>
            {
                PlayButtonAudio.self.PlayWorkerClick ();
                SwitchWorker ( id );
            };
        }
        private int workerID;
        void SwitchWorker ( int id )
        {
            int quantity = GameLoopManager.instance.pm.hireWorker.GetQuantity ();
            workerID = hireWorker [id].workerID;
            if ( !isClickID [workerID] )
            {
                if ( workerIDs.Count >= quantity )
                {
                    isClickID [workerIDs [0]] = false;
                    workerIDs.RemoveAt ( 0 );
                }
                workerIDs.Add ( workerID );
                isClickID [workerID] = true;
            }
            else
            {
                workerIDs.Remove ( workerID );
                isClickID [workerID] = false;
            }
            OKBtn.gameObject.SetActive ( workerIDs.Count == quantity || workerIDs.Count >= canHireLength );
            for ( int i = 0; i < hireWorker.Length; i++ )
            {
                int wid = hireWorker [i].workerID;
                hireWorker [i].Target.SetActive ( isClickID[wid] );
            }
        }

        UnityAction GenerateOK ()
        {
            return () =>
            {
                PlayButtonAudio.self.PlayWorkerOK ();
                OK ();
            };
        }
        public void OK ()
        {
            int quantity = GameLoopManager.instance.pm.hireWorker.GetQuantity ();
            if ( canHireLength - quantity == 0 )
                Achievement.SetAchievement ( Achievement.achievementNames.Worker.ToString () );
            if ( workerID == 20 )
                Achievement.SetAchievement ( Achievement.achievementNames.Actor7.ToString () );

            GameLoopManager.instance.IsHire = false;
            LoadingScene.self.Transitions ( 0.5f , 1f , false );
            Invoke ( "ReadDrama" , 0.5f );
        }

        public void ReadDrama ()
        {
            if ( StoryManager.self != null )
            {
                string language = GameLoopManager.instance.om.optionData.GetLanguageStr ();
                TextAsset temp = Resources.Load<TextAsset> ( $"Data/Story/{language}Worker/Work" + ( workerID - 2 ).ToString () );
                if ( temp == null )
                {
                    HireWorker ();
                    return;
                }
                StoryManager.self.ReadWorkerDrama = true;
                StartCoroutine ( "WaitDramaEnd" );
                StoryManager.self.StartStory ( $"Data/Story/{language}Worker/Work" , ( workerID - 2 ).ToString () );
            }
        }
        IEnumerator WaitDramaEnd ()
        {
            while ( StoryManager.self.ReadWorkerDrama == true )
            {
                yield return null;
            }
            gameObject.SetActive ( false );
            Invoke ( "HireWorker" , 0.5f );
        }

        public void HireWorker ()
        {
            for ( int i = 0; i < workerIDs.Count; i++ )
            {
                GameLoopManager.instance.om.unlockActorData.SetUnlock ( workerIDs [i] + 4 , true );

                if ( workerIDs [i] == 20 )
                {
                    pm.businessUnlock.SetUnlock ( 65 , 0 , true );
                    pm.businessUnlock.SetMode ( 65 , BusinessUnlock.state.watch );
                    GameLoopManager.instance.om.unlockActorData.SetUnlock ( 26 , true );
                    GameLoopManager.instance.om.unlockCGData.SetUnlock ( 3 , 1 , true );
                }
                Action.HireEmployee.Run ( workerIDs [i] , pm , gm );
            }
            SaveLoadManager.OnlyDataSave ( GameLoopManager.instance.om );
            for ( int i = 0; i < hireWorker.Length; i++ )
            {
                Destroy ( hireWorker[i].gameObject );
            }
            Close ();
            Shield.SetActive ( false );
            pm.calender.UseEnergy ();                                       //消耗行動點
        }
    }
}
