﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class BusinessItemPrefab : MonoBehaviour
    {
        public int id;
        public Text NameText;
        public Text CostText;
        public Image ItemImage;
        public GameObject AP;
        public GameObject WatchIcon;
        public GameObject Target;
        public Button Button;
        public GameObject Lock;

        public void UpdateData ( int id , PlayerDataManager pm , GameDataManager gm , Sprite [] itemSprite )
        {
            if ( id < 0 )
            {
                ClearData ( id , itemSprite );
                return;
            }
            this.id = id;
            gameObject.name = "Item" + id.ToString ();
            string name = gm.businessList.Data [id].name;
            string cost = gm.businessList.Data [id].cost.ToString ( "N0" ) + Localizer.Translate ( "元" );
            int type = (int)gm.businessList.Data [id].eventType;
            bool watch = pm.businessUnlock.GetMode ( id ) == BusinessUnlock.state.watch;

            NameText.text = Localizer.Translate ( name );
            CostText.text = cost;
            ItemImage.sprite = itemSprite [type];
            AP.SetActive ( type == 0 );
            Button.enabled = type != 4;
            WatchIcon.SetActive ( watch );

            Lock.SetActive ( !pm.businessUnlock.GetUnlock ( id ) );
        }

        public void ClearData ( int id , Sprite [] itemSprite )
        {
            gameObject.name = "None";
            this.id = id;
            NameText.text = "";
            CostText.text = "";
            ItemImage.sprite = itemSprite [4];
            AP.SetActive ( false );
            Button.enabled = false;
            WatchIcon.SetActive ( false );
            Lock.SetActive ( false );
        }
    }
}
