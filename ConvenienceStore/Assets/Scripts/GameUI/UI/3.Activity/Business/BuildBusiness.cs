﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class BuildBusiness : BaseBuildPanel
    {
        public Transform PageSpace;
        public Transform ItemSpace;

        public Text AdditionText;
        public Text ConditionText;
        public Text CompleteText;
        public Button OKBtn;
        public Button BackBtn;

        [SerializeField]
        private int page = 0;
        [SerializeField]
        private int businessID = -1;

        private const string businessPath = "GameUI/Activity/Business/";
        private const string pagePath = "Sprites/Common/Button/Page/";

        private int pageLength = 9;
        private float distance_P = 70f;
        private int itemLength = 6;
        private float distance_I = 90f;

        Sprite [] itemSprite;
        Prefab.BusinessPagePrefab [] Page;
        Prefab.BusinessItemPrefab [] Item;

        private int [] demoLockID = new int [] { 13 , 14 , 15 , 18 , 19 , 22 , 23 , 24 , 25 , 28 , 29 , 32 , 33 , 34 , 37 , 38 , 41 , 42 , 43 , 44 , 45 , 46 , 47 , 50 , 51 , 52 , 53 , 54 , 55 , 59 , 60 , 61 , 62 };
        private int [] steamLockID = new int [] { 57 , 58 , 59 , 60 , 61 , 62 , 63 };
        private List<int> endingID = new List<int> ();

        public override void Init ()
        {
            SwitchPage ( 0 );
            OKBtn.GetComponent<Image> ().sprite = SpritePath.OKBtn ();
            BackBtn.GetComponent<Image> ().sprite = SpritePath.BackBtn ();
        }

        public override void Build ()
        {
            for ( int i = 0; i < gm.businessList.Data.Length; i++ )
            {
                if ( gm.businessList.Data [i].eventType == GameData.Business.EventType.ending )
                    endingID.Add ( i );
            }

            itemSprite = Resources.LoadAll<Sprite> ( "Sprites/Common/" + businessPath + "ItemSprite" );
            Page = new Prefab.BusinessPagePrefab [pageLength];
            Item = new Prefab.BusinessItemPrefab [itemLength];

            Sprite [] pageSprite = Resources.LoadAll<Sprite> ( pagePath + "Page" );
            Sprite [] targetPageSprite = Resources.LoadAll<Sprite> ( pagePath + "TargetPage" );
            for ( int i = 0; i < pageLength; i++ )
            {
                Prefab.BusinessPagePrefab page = Instantiate ( Resources.Load<Prefab.BusinessPagePrefab> ( "Prefabs/" + businessPath + "Page" ) , PageSpace );
                page.PageImage.sprite = pageSprite [i];
                page.TargetPageImage.sprite = targetPageSprite [i];
                page.transform.localPosition += new Vector3 ( distance_P * i , 0 , 0 );
                page.Button.onClick.AddListener ( GeneratePage ( i ) );
                Page [i] = page;
            }

            for ( int i = 0; i < itemLength; i++ )
            {
                Prefab.BusinessItemPrefab item = Instantiate ( Resources.Load<Prefab.BusinessItemPrefab> ( "Prefabs/" + businessPath + "Item" ) , ItemSpace );
                item.transform.localPosition += new Vector3 ( 0 , -distance_I * i , 0 );
                item.Button.onClick.AddListener ( GenerateItem ( item , i ) );
                Item [i] = item;
            }
            OKBtn.onClick.AddListener ( GenerateOK () );
            BackBtn.onClick.AddListener ( GenerateBack () );
            isBuild = true;
        }

        UnityAction GeneratePage ( int id )
        {
            return () =>
            {
                PlayButtonAudio.self.PlayPage ();
                SwitchPage ( id );
            };
        }
        void SwitchPage ( int page )
        {
            ClearText ();
            SwitchItem ( -1 , -1 );
            for ( int i = 0; i < Page.Length; i++ )
            {
                Page [i].TargetPageImage.gameObject.SetActive ( i == page );
            }
            ShowItem ( page );
            this.page = page;
        }
        private void ShowItem ( int page )
        {
            List<int> unlockID = GetID ();

            int itemQuantity = 0;
            for ( int i = 0; i < Item.Length; i++ )
            {
                if ( i + page * Item.Length >= unlockID.Count )
                    continue;
                int id = unlockID [i + page * Item.Length];
                
                Item [i].UpdateData ( id , pm , gm , itemSprite );
                itemQuantity++;
            }
            for ( int i = itemQuantity; i < Item.Length; i++ )
            {
                Item [i].ClearData ( -1 , itemSprite );
            }
        }

        List<int> GetID ()
        {
            List<int> unlockID = new List<int> ();
            BusinessUnlock unlock = pm.businessUnlock;
            if ( GameLoopManager.instance.GetDemo () == false )
            {
                int id = unlock.GetEndingID ();
                if ( id != -1 )
                {
                    unlockID.Add ( endingID [id] );
                }
            }
            for ( int i = 0; i < gm.businessList.Data.Length; i++ )
            {
                if ( unlock.GetMode ( i ) == BusinessUnlock.state.hide )
                    continue;
                if ( unlock.GetMode ( i ) == BusinessUnlock.state.groupLock )
                    continue;
                if ( gm.businessList.Data [i].eventType == GameData.Business.EventType.ending )
                    continue;
                if ( gm.businessList.Data [i].eventType == GameData.Business.EventType.h && GameLoopManager.instance.AllAge == true )
                    continue;
                if ( GameLoopManager.instance.GetDemo () == true )
                {
                    bool skip = false;
                    for ( int j = 0; j < demoLockID.Length; j++ )
                    {
                        if ( i == demoLockID [j] )
                        {
                            skip = true;
                            break;
                        }
                    }
                    if ( skip == true )
                        continue;
                }
                if ( GameLoopManager.instance.GetSteam () == true )
                {
                    bool skip = false;
                    for ( int j = 0; j < steamLockID.Length; j++ )
                    {
                        if ( i == steamLockID[j] )
                        {
                            skip = true;
                            break;
                        }
                    }
                    if ( skip == true )
                        continue;
                }
                if ( unlock.GetUnlock ( i ) )
                    unlockID.Add ( i );
            }
            for ( int i = 0; i < gm.businessList.Data.Length; i++ )
            {
                if ( gm.businessList.Data [i].eventType == GameData.Business.EventType.normal && unlock.GetUnlock ( i ) == false )
                {
                    unlockID.Add ( i );
                    continue;
                }
            }
            if ( GameLoopManager.instance.GetSteam () )
            {
                int [] ids = new [] { 30 , 32 , 37 , 39 };
                for ( int i = 0; i < ids.Length; i++ )
                {
                    int id = ids [i];
                    if ( pm.businessUnlock.GetUnlock ( id ) == false )
                        unlockID.Add ( id );
                }
            }
            return unlockID;
        }
        private UnityAction GenerateItem ( Prefab.BusinessItemPrefab item , int id )
        {
            return () =>
            {
                PlayButtonAudio.self.PlayBusinessClick ();
                SwitchItem ( item.id , id );
            };
        }
        private void SwitchItem ( int businessID , int itemID )
        {
            bool canPay = false;
            if ( businessID >= 0 )
            {
                int cost = gm.businessList.Data [businessID].cost;
                canPay = Action.Business.CheckMoney ( cost , pm );
                if ( !canPay )
                {
                    MessageBox.Warning ( "店長業務餘額不足" );
                }
                gm.businessList.Data [businessID].effectText = Localizer.Translate ( gm.businessList.Data [businessID].effectText );
                string addition = gm.businessList.Data [businessID].effectText.Replace ( '-' , '\n' );
                gm.businessList.Data [businessID].unlockRequirement = Localizer.Translate ( gm.businessList.Data [businessID].unlockRequirement );
                string condition = gm.businessList.Data [businessID].unlockRequirement.Replace ( '-' , '\n' ).Replace ( ':' , '-' );
                int num = gm.businessList.Data [businessID].unlock.Length;
                string complete = "";
                for ( int c = 0; c < num; c++ )
                {
                    bool completeBool = pm.businessUnlock.GetCondition ( businessID , c );
                    if ( condition == "無" )
                    {
                        complete += "\n";
                        continue;
                    }
                    complete += completeBool ? Localizer.Translate ( "已達成" ) + "\n" : Localizer.Translate ( "未達成" ) + "\n";
                }
                UpdateText ( addition , condition , complete );
            }
            this.businessID = businessID;
            for ( int i = 0; i < Item.Length; i++ )
                Item [i].Target.SetActive ( i == itemID );
        }
        private void ClearText ()
        {
            AdditionText.text = "";
            ConditionText.text = "";
            CompleteText.text = "";
        }

        private void UpdateText ( string addition , string condition , string complete )
        {
            AdditionText.text = addition;
            ConditionText.text = condition;
            int length = Mathf.Clamp ( complete.Length - 1 , 0 , int.MaxValue );
            CompleteText.text = complete.Substring ( 0 , length );
        }

        UnityAction GenerateOK ()
        {
            return () =>
            {
                PlayButtonAudio.self.PlayBusinessOK ();
                OK ();
            };
        }
        private void OK ()
        {
            if ( businessID == -1 || !pm.businessUnlock.GetUnlock ( businessID ) )
            {
                Back ();
                return;
            }
            Invoke ( "Business" , 0.5f );
            LoadingScene.self.Transitions ( 0.5f , 1f , gm.businessList.Data[businessID].eventType == 0 );
            if ( gm.businessList.Data[businessID].cost != 0 )
                PlayButtonAudio.self.PlayCustom ( "Audios/Effect/" , "Coin" , false );
        }

        UnityAction GenerateBack ()
        {
            return () =>
            {
                PlayButtonAudio.self.PlayBusinessCancel ();
                Back ();
            };
        }
        void Back ()
        {
            MainPanel.self.Close ();
        }

        private void Business ()
        {
            Back ();
            Action.Business.Run ( businessID , pm , gm , GameLoopManager.instance.nextShift );
        }
    }
}
