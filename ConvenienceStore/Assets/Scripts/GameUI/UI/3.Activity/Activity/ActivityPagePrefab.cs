﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class ActivityPagePrefab : MonoBehaviour
    {
        public Image PageImage;
        public Image TargetPageImage;
        public Button Button;
    }
}