﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class BuildWorkerItem : BaseBuildPanel
    {
        public Image Background;
        public CustomScrollRect rect;

        List<int> employeeIDs = new List<int> ();

        public Transform WorkerSpace;

        private int quantity = 15;
        private float distance = 119f;

        private const string WorkerPath = "GameUI/Activity/Activity/Mission/";

        Prefab.WorkerItemPrefab [] Item;

        SchedualeManager manager;
        #region SetManager : 由外部設定主要管理器
        public void SetManager ( SchedualeManager manager )
        {
            this.manager = manager;
        }
        #endregion

        #region Build : 初次建立
        public override void Build ()
        {
            Item = new Prefab.WorkerItemPrefab [quantity];

            for ( int i = 0; i < quantity; i++ )
            {
                Prefab.WorkerItemPrefab item = Instantiate ( Resources.Load<Prefab.WorkerItemPrefab> ( "Prefabs/" + WorkerPath + "Item" ) , WorkerSpace );
                item.transform.localPosition += new Vector3 ( 0 , -distance * i , 0 );
                item.Button.onClick.AddListener ( ChooseWorker ( i ) );
                item.SetManager ( manager );
                Item [i] = item;
            }
            isBuild = true;
        }
        #endregion

        #region Init : 每次打開都初始化
        public override void Init ()
        {
            ShowWorkerItem ();
            CancelFrame ();
            rect.enabled = !StoryManager.self.EnableTeach;

            Background.sprite = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameUI/Activity/Worker/Background" );
        }
        #endregion

        public void CopySchedule ( int [] workerID )
        {
            for ( int i = 0; i < workerID.Length; i++ )
            {
                int id = workerID [i];
                SwitchWorker ( id );
            }
        }

        #region ShowWorkerItem : 顯示所有員工
        void ShowWorkerItem ()
        {
            List<int> unlockID = new List<int> ();
            for ( int i = 0; i < gm.workerList.Data.Length; i++ )
            {
                if ( pm.employeeList.GetUnlockID ( i ) )
                    unlockID.Add ( i );
            }
            for ( int i = 0; i < unlockID.Count; i++ )
            {
                int id = unlockID [i];
                Item [i].UpdateData ( id , pm , gm );
            }
            for ( int i = unlockID.Count; i < Item.Length; i++ )
            {
                Item [i].NoneData ();
            }
        }
        #endregion

        #region ChooseWorker : 選擇員工 ( AddListener )
        UnityAction ChooseWorker ( int id )
        {
            return () =>
            {
                PlayButtonAudio.self.PlayEmployeeClick ();
                SwitchWorker ( id );
            };
        }
        #endregion

        #region SwitchWorker : 選擇員工
        void SwitchWorker ( int id )
        {
            if ( employeeIDs.Count >= 2 )
            {
                MessageBox.Warning ( "最多只能委託兩名員工" );
                return;
            }
            Item [id].Target.SetActive ( true );
            Item [id].Button.enabled = false;
            Item [id].Mask.SetActive ( false );
            int employeeID = Item [id].employeeID;
            employeeIDs.Add ( employeeID );
            manager.UpdateEmployeeID ();
        }
        #endregion

        #region GetEmployeeIDs : 由外部取得目前派遣的員工編號
        public List<int> GetEmployeeIDs ()
        {
            return employeeIDs;
        }
        #endregion

        #region CancelWorker : 由外部取消目前派遣的員工編號
        public void CancelWorker ( int id )
        {
            int itemID = -1;
            for ( int i = 0; i < Item.Length; i++ )
            {
                if ( Item [i].employeeID == id )
                    itemID = i;
            }
            if ( itemID == -1 )
            {
                Debug.LogError ( id + "再員工欄位中找不到該編號" );
                itemID = 0;
            }
            Item [itemID].Target.SetActive ( false );
            Item [itemID].Button.enabled = true;    
            employeeIDs.Remove ( id );
            manager.UpdateEmployeeID ();
        }
        #endregion

        #region ClearEmployeeIDs : 清除目前派遣的員工編號列表
        public void ClearEmployeeIDs ()
        {
            employeeIDs.Clear ();
        }
        #endregion

        #region CancelFrame : 取消員工的選擇框
        void CancelFrame ()
        {
            for ( int i = 0; i < Item.Length; i++ )
            {
                Item [i].Target.SetActive ( false );
            }
        }
        #endregion
    }
}
