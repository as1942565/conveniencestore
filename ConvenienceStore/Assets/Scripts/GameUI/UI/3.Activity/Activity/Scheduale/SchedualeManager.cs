﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class SchedualeManager : BaseBuildPanel
    {
        public BuildMission buildMission;
        public BuildWorkerItem buildWorkerItem;
        public BuildActivity buildActivity;

        public Button OKBtn;
        public Button BackBtn;
        
        public List<ActivityScheduale.Scheduale> tempScheduales = new List<ActivityScheduale.Scheduale> ();

        [SerializeField]
        public bool tempGroup = false;
        private const string path = "GameUI/Activity/Activity/";

        int activityID = -1;
        #region SetActivityID : 由外部設定員工活動編號
        public void SetActivityID ( int id )
        {
            activityID = id;
        }
        #endregion

        #region GetActivityID : 由外部取得員工活動編號
        public int GetActivityID ()
        {
            return activityID;
        }
        #endregion

        #region Build : 初次建立
        public override void Build ()
        {
            buildMission.SetManager ( this );
            buildWorkerItem.SetManager ( this );
            isBuild = true;

            OKBtn.onClick.AddListener ( GenerateOK () );
            BackBtn.onClick.AddListener ( GenerateBack () );
        }
        #endregion

        #region Init : 每次打開都初始化
        public override void Init ()
        {
            buildMission.SetManager ( this );
            buildMission.Open ();
            buildWorkerItem.Open ();

            for ( int i = 0; i < tempScheduales.Count; i++ )
            {
                int activityID = tempScheduales [i].ActivityID;
                if ( this.activityID == activityID )
                {
                    buildMission.CopySchedule ( tempScheduales [i] );
                    buildWorkerItem.CopySchedule ( tempScheduales[i].EmployeesID );
                    break;
                }
            }
        }
        #endregion

        public void Reset ()
        {
            buildMission.Close ();
            buildWorkerItem.Close ();
        }

        #region Close : 每次關閉
        public override void Close ()
        {
            buildMission.Close ();
            buildWorkerItem.Close ();
        }
        #endregion

        UnityAction GenerateOK ()
        {
            return () =>
            {
                PlayButtonAudio.self.PlayActivityClick ();
                OK ();
            };
        }

        bool useAction = false;
        #region OK : 確認鍵 ( Button )
        public void OK ()
        {
            for ( int i = 0; i < tempScheduales.Count; i++ )
            {
                if ( tempScheduales[i].GetMethod () == ActivityScheduale.Scheduale.SchedualeMethod.money )
                {
                    PlayButtonAudio.self.PlayCustom ( "Audios/Effect/" , "Coin" , false );
                    break;
                }
            }
            useAction = false;
            for ( int i = 0; i < tempScheduales.Count; i++ )
            {
                if ( tempScheduales[i].GetMethod () == ActivityScheduale.Scheduale.SchedualeMethod.boss )
                {
                    useAction = true;
                    break;
                }
            }

            if ( useAction )
            {
                LoadingScene.self.Transitions ( 0.5f , 1f , true );
            }
            StartCoroutine ( "Scheduale" );
        }
        #endregion
        
        #region Scheduale : 將預計要委託的活動全部委託出去
        IEnumerator Scheduale ()
        {
            if ( useAction )
                yield return new WaitForSeconds ( 0.5f );
            
            for ( int i = 0; i < tempScheduales.Count; i++ )
            {
                PlayActivityStory.self.DoEvent ( tempScheduales [i].ActivityID , useAction );

                yield return null;
                
                while ( StoryManager.self.ReadActivityDrama == true )
                {
                    StoryManager.self.WaitForDramaEnd = true;
                    yield return null;
                }
                StoryManager.self.WaitForDramaEnd = false;
            }

            for ( int i = 0; i < tempScheduales.Count; i++ )
            {
                Action.Activity.Run ( tempScheduales [i] , pm , gm );
                pm.activityScheduleLog.OnGetSchedule ( pm.calender , tempScheduales [i] );
            }

            if ( useAction )
                pm.calender.UseEnergy ();
            Back ();
        }
        #endregion

        UnityAction GenerateBack ()
        {
            return () =>
            {
                PlayButtonAudio.self.PlayActivityCancel ();
                Back ();
            };
        }
        #region Back : 返回鍵 ( Button )
        public void Back ()
        {
            buildWorkerItem.ClearEmployeeIDs ();
            tempScheduales.Clear ();
            MainPanel.self.Close ();
            tempGroup = false;
        }
        #endregion

        #region ChangeState : 依照預計委託的狀況來更換每個活動的狀態 (可執行 or 執行中 or 同種類不可執行)
        public void ChangeState ()
        {
            Prefab.ActivityItemPrefab [] prefabs = buildActivity.GetItem ();
            LockGroup ( prefabs );
            for ( int i = 0; i < prefabs.Length; i++ )
            {
                int id = prefabs [i].id;
                for ( int j = 0; j < tempScheduales.Count; j++ )
                {
                    if ( tempScheduales [j].ActivityID == id )
                    {
                        prefabs [i].StateImage.sprite = ActivitySprite.State [1];
                        break;
                    }
                }
            }
        }
        #endregion
        
        #region LockGroup : 鎖住同種類的活動
        void LockGroup ( Prefab.ActivityItemPrefab [] prefabs )
        {
            for ( int i = 0; i < prefabs.Length; i++ )
            {
                if ( tempGroup == true && prefabs [i].group > 0 )
                {
                    prefabs [i].StateImage.sprite = ActivitySprite.State [3];
                    prefabs [i].EntrustBtn.enabled = false;
                }
            }
        }
        #endregion

        #region UpdateEmployeeID : 更新目前派前的員工編號
        public void UpdateEmployeeID ()
        {
            List<int> employeeIDs = buildWorkerItem.GetEmployeeIDs ();
            buildMission.ShowEmployeeIcon ( employeeIDs );
            int baseChance = CalculateBaseChance ( employeeIDs );
            buildMission.ShowChance ( baseChance );
        }
        #endregion

        #region CancelWorker : 取消第N個員工
        public void CancelWorker ( int index )
        {
            if ( index >= buildWorkerItem.GetEmployeeIDs ().Count )
                return;
            int id = buildWorkerItem.GetEmployeeIDs () [index];
            buildWorkerItem.CancelWorker ( id );
        }
        #endregion

        #region CalculateBaseChance : 計算基礎成功率
        int CalculateBaseChance ( List<int> employeeIDs )
        {
            int baseChance = 0;
            int difficult = (int)gm.activityList.Data [activityID].difficult;
            int type = (int)gm.activityList.Data [activityID].type;
            int [] allChance = gm.customData.Data.ActivityChance [difficult].ToArray ();
            for ( int i = 0; i < employeeIDs.Count; i++ )
            {
                int id = employeeIDs [i];
                if ( type == 3 )
                {
                    int power = pm.employeeList.GetEmployee ( id ).GetAbility ( 0 );
                    int luck = pm.employeeList.GetEmployee ( id ).GetAbility ( 2 );
                    baseChance += ( allChance [power] + allChance [luck] ) / 2;
                }
                else
                {
                    int ability = pm.employeeList.GetEmployee ( id ).GetAbility ( type );
                    baseChance += allChance [ability];
                }
            }
            return baseChance;
        }
        #endregion
    }
}
