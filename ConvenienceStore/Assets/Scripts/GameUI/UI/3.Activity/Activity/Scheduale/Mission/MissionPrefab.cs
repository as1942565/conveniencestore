﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Prefab
{
    public class MissionPrefab : MonoBehaviour
    {
        public Image Background;
        public Image [] TypeImg;
        public Text NameText;
        public Image TypeImage;
        public Image DifficultImage;
        public Text DayText;
        public Text Explanation;
        public Text Chance;
        public Button [] RewardButton;
        public GameObject [] RewardFrame;
        public Image MoneyImage;
        public Button [] IconBtn;
        [HideInInspector]
        public int [] EmployeeIDs;
        public Button OKBtn;
        public Button BackBtn;
        
        #region UpdateData : 更新資料
        public void UpdateData ( int activityID , GameDataManager gm )
        {
            Background.sprite = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr()}GameUI/Activity/Activity/Mission/Background" );


            Sprite [] TpyeSpr = Resources.LoadAll<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr()}GameUI/Activity/Activity/Mission/Reward/RewardType" );
            for ( int i = 0; i < TpyeSpr.Length; i++ )
            {
                TypeImg [i].sprite = TpyeSpr [i];
            }

            if ( activityID < 0 || activityID >= gm.activityList.Data.Length )
            {
                Debug.LogError ( activityID + "不是員工活動編號" );
                activityID = 0;
            }
            string day = ( gm.activityList.Data [activityID].shift / 3 ).ToString () + "天";
            //string explanation = gm.activityList.Data [activityID].explanation;
            string explanation = Localizer.Translate ( gm.activityList.Data [activityID].explanation );

            int type = (int)gm.activityList.Data [activityID].type;
            int difficult = (int)gm.activityList.Data [activityID].difficult;

            //NameText.text = gm.activityList.Data [activityID].name;
            string name = Localizer.Translate ( gm.activityList.Data [activityID].name );
            NameText.text = name;
            TypeImage.sprite = ActivitySprite.Type [type];
            DifficultImage.sprite = ActivitySprite.Difficult [difficult];
            Sprite [] moneySpr = Resources.LoadAll<Sprite> ( "Sprites/Common/GameUI/Activity/Activity/Mission/Reward/Money" );
            MoneyImage.sprite = moneySpr [difficult];
            DayText.text = day;
            Explanation.text = explanation;
        }
        #endregion

        #region LockButton : 鎖住按鈕 ( 昨日銷售用 )
        public void LockButton ()
        {
            for ( int i = 0; i < RewardButton.Length; i++ )
            {
                RewardButton [i].enabled = false;
            }
            OKBtn.gameObject.SetActive ( false );
            BackBtn.gameObject.SetActive ( false );
        }
        #endregion

        #region ChangeReward : 變更委託方式 ( 昨日銷售用 )
        public void ChangeReward ( int mode )
        {
            for ( int i = 0; i < RewardFrame.Length; i++ )
            {
                RewardFrame [i].gameObject.SetActive ( i == mode );
            }
        }
        #endregion
    }
}
