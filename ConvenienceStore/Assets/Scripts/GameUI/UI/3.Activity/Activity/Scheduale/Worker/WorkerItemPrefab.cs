﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class WorkerItemPrefab : MonoBehaviour
    {
        public int employeeID;
        public Image Icon;
        public Text NameText;
        public Text EXPText;
        public GameObject EXPBox;
        public RectTransform EXPRect;
        public GameObject LevelBox;
        public Text LevelText;
        public GameObject RelationBox;
        public Text RelationText;
        public RectTransform RelationRect;
        public Text [] AbilityText;
        public GameObject Mask;
        public GameObject Target;
        public Button Button;

        private float width = 1180f;
        private float height = 150f;
        private string [] abilityRank = new string [] { "D" , "C" , "B" , "A" , "S" };

        Sprite NoneWorker;
        private const string path = "Sprites/Common/GameUI/Activity/Activity/Mission/Worker/NoneWorker";

        UI.SchedualeManager manager;
        #region SetManager : 從外部設定主要管理器
        public void SetManager ( UI.SchedualeManager manager )
        {
            this.manager = manager;
        }
        #endregion

        #region UpdateData : 更新資料
        public void UpdateData ( int id , PlayerDataManager pm , GameDataManager gm )
        {
            if ( id < 0 )
            {
                NoneData ();
                return;
            }
            employeeID = id;
            gameObject.name = "Employee" + id.ToString ();
            string name = gm.workerList.Data [id].name;
            float exp = pm.employeeList.GetEmployee ( id ).GetExp ();
            int [] EmployeeEXP = gm.customData.Data.EmployeeLevel;
            int level = pm.employeeList.GetEmployee ( id ).GetLevel ();
            float maxExp;
            string expStr;
            if ( level + 1 >= EmployeeEXP.Length )
            {
                maxExp = 0;
                EXPRect.SetSizeWithCurrentAnchors ( RectTransform.Axis.Horizontal , 0 );
                expStr = exp.ToString () + " /∞ ";
            }
            else
            {

                maxExp = EmployeeEXP [level + 1];
                float scale_E = exp / maxExp;
                EXPRect.SetSizeWithCurrentAnchors ( RectTransform.Axis.Horizontal , width * scale_E );
                expStr = exp.ToString () + " / " + maxExp.ToString ();
            }
            
            string levelStr = "Lv " + level.ToString ();
            int relation = pm.employeeList.GetEmployee ( id ).GetRelationRank ();
            float r = pm.employeeList.GetEmployee ( id ).GetRelationEXP ();
            int [] EmployeeRelation = gm.customData.Data.EmployeeRelation;
            if ( relation + 1 < EmployeeRelation.Length )
            {
                float scale_R = r / (float)EmployeeRelation [relation + 1];
                RelationRect.SetSizeWithCurrentAnchors ( RectTransform.Axis.Vertical , height * scale_R );
            }
            else
                RelationRect.SetSizeWithCurrentAnchors ( RectTransform.Axis.Vertical , 0 );
            string [] ability = new string [3];
            for ( int a = 0; a < ability.Length; a++ )
            {
                int rank = pm.employeeList.GetEmployee ( id ).GetAbility ( a );
                ability [a] = abilityRank [rank];
            }
            bool isFree = pm.activityScheduale.IsEmployeeFree ( id );
            bool tempEntrust = false;
            List<ActivityScheduale.Scheduale> IDs = manager.tempScheduales;
            for ( int i = 0; i < IDs.Count; i++ )
            {
                for ( int j = 0; j < IDs[i].EmployeesID.Length; j++ )
                {
                    if ( IDs[i].EmployeesID [j] == employeeID )
                    {
                        tempEntrust = true;
                        break;
                    }
                }
            }
            Icon.sprite = SpritePath.ActorIcon[id];
            NameText.text = Localizer.Translate ( name );
            EXPText.text = expStr;
            EXPBox.SetActive ( true );
            
            LevelBox.SetActive ( true );
            LevelText.text = levelStr;
            RelationBox.SetActive ( true );
            RelationText.text = relation.ToString ();
            for ( int i = 0; i < ability.Length; i++ )
            {
                AbilityText [i].text = ability [i];
            }
            Mask.SetActive ( !isFree || tempEntrust );
            Button.enabled = isFree && !tempEntrust;
            gameObject.SetActive ( true );
        }
        #endregion

        #region NoneData : 未解鎖的資料
        public void NoneData ()
        {
            gameObject.name = "None";
            employeeID = -1;
            if ( NoneWorker == null )
                GetNoneWorker ();
            Icon.sprite = NoneWorker;
            NameText.text = "";
            EXPText.text = "";
            EXPBox.SetActive ( false );
            LevelBox.SetActive ( false );
            RelationBox.SetActive ( false );
            for ( int i = 0; i < AbilityText.Length; i++ )
            {
                AbilityText [i].text = "";
            }
            Target.SetActive ( false );
            Mask.SetActive ( false );
            Button.enabled = false;
        }
        #endregion

        #region GetNoneWorker : 取得未解鎖的縮圖
        void GetNoneWorker ()
        {
            NoneWorker = Resources.Load<Sprite> ( path );
            if ( NoneWorker == null )
                Debug.LogError ( path + "路徑不正確" );
        }
        #endregion
    }
}
