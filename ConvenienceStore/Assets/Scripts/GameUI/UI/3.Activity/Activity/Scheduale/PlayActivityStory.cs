﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayActivityStory : MonoBehaviour
{
    public static PlayActivityStory self;
    private void Awake ()
    {
        self = this;
    }

    private string storyID = "0";

    public void DoEvent ( int activityID , bool useAction )
    {
        ActivityUnlock activityUnlock = GameLoopManager.instance.pm.activityUnlock;
        storyID = "0";
        int [] AID = new int [] { 0 , 1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10 , 11 , 12 , 14 , 15 , 17 , 18 };
        string [] SID = new string [] { "Activity9" , "Activity9" , "Activity9" ,
                                        "Activity69" , "Activity69" , "Activity69" ,
                                        "Activity5" , "Activity5" , "Activity5" ,
                                        "Special" ,
                                        "Popular1" , "Popular2" , "Popular3" ,
                                        "Clear2" , "Clear3" ,
                                        "Police2" , "Police3" };
        for ( int i = 0; i < AID.Length; i++ )
        {
            if ( AID [i] == activityID && activityUnlock.GetWatch ( activityID ) == false )
            {
                storyID = SID [i];
                break;
            }
        }
        if ( storyID == "0" )
            return;
        activityUnlock.SetWatch ( activityID , true );
        StoryManager.self.ReadActivityDrama = true;
        if ( useAction == false )
        {
            LoadingScene.self.Transitions ( 0.5f , 1f , false );
            Invoke ( "Play" , 0.5f );
        }
        else
            Play ();
    }

    void Play ()
    {
        if ( StoryManager.self != null )
        {
            string language = GameLoopManager.instance.om.optionData.GetLanguageStr ();
            StoryManager.self.StartStory ( $"Data/Story/{language}Activity/" , storyID );
            StoryManager.self.ReadActivityDrama = true;
        }
        else
            StoryManager.self.ReadActivityDrama = false;
    }
}
