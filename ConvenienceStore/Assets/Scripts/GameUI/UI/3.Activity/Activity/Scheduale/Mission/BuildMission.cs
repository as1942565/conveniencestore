﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class BuildMission : BaseBuildPanel
    {
        public Transform MissionSpace;
        public Button OKBtn;
        public Button BackBtn;

        private string path = "Prefabs/GameUI/Activity/Activity/Mission/Mission";
        private int [] extraChances = new int [] { 0 , 10 , 20 };

        ActivityScheduale.Scheduale.SchedualeMethod method = ActivityScheduale.Scheduale.SchedualeMethod.normal;

        Prefab.MissionPrefab missionPrefab;

        SchedualeManager manager;
        #region SetManager : 設定主要Manager
        public void SetManager ( SchedualeManager manager )
        {
            this.manager = manager;
        }
        #endregion

        #region Build : 初次生產
        public override void Build ()
        {
            missionPrefab = Instantiate ( Resources.Load<Prefab.MissionPrefab> ( path ) , MissionSpace );
            for ( int i = 0; i < missionPrefab.RewardButton.Length; i++ )
            {
                missionPrefab.RewardButton [i].onClick.AddListener ( ChooseReward ( i ) );
            }
            for ( int i = 0; i < missionPrefab.IconBtn.Length; i++ )
            {
                missionPrefab.IconBtn [i].onClick.AddListener ( GreateCancelWorkerBtn ( i ) );
            }
            missionPrefab.OKBtn.onClick.AddListener ( GenerateOK () );
            missionPrefab.BackBtn.onClick.AddListener ( GenerateBack () );
            isBuild = true;
        }
        #endregion

        #region Init : 每次開啟都初始化
        public override void Init ()
        {
            int activityID = manager.GetActivityID ();
            missionPrefab.UpdateData ( activityID , gm );
            SwitchType ( ActivityScheduale.Scheduale.SchedualeMethod.normal );
            ClearEmployeeIcon ();
        }
        #endregion

        public void CopySchedule ( ActivityScheduale.Scheduale scheduale )
        {
            SwitchType ( scheduale.GetMethod () );
        }

        #region ChooseReward : 選擇委託方式 ( AddListener )
        UnityAction ChooseReward ( int id )
        {
            return () =>
            {
                SwitchType ( (ActivityScheduale.Scheduale.SchedualeMethod)id );
                PlayButtonAudio.self.PlayEmployeeButtom ();
            };
        }
        #endregion
        
        #region SwitchType : 選擇委託方式 
        void SwitchType ( ActivityScheduale.Scheduale.SchedualeMethod method )
        {
            for ( int i = 0; i < missionPrefab.RewardFrame.Length; i++ )
            {
                missionPrefab.RewardFrame [i].SetActive ( i == (int)method );
            }
            this.method = method;
            manager.UpdateEmployeeID ();
        }
        #endregion

        #region GreateCancelWorkerBtn : 取消員工 ( AddListener )
        UnityAction GreateCancelWorkerBtn ( int index )
        {
            return () =>
            {
                PlayButtonAudio.self.PlayEmployeeCancel ();
                CancelWorker ( index );
            };
        }
        #endregion

        #region CancelWorker : 取消員工
        void CancelWorker ( int index )
        {
            manager.CancelWorker ( index );
        }
        #endregion

        #region ShowEmployeeIcon : 顯示目前派遣的員工縮圖
        public void ShowEmployeeIcon ( List<int> employeeIDs )
        {
            ClearEmployeeIcon ();
            for ( int i = 0; i < employeeIDs.Count; i++ )
            {
                int id = employeeIDs [i];
                missionPrefab.IconBtn [i].image.sprite = SpritePath.ActorIcon [id];
            }
        }
        #endregion

        private int totalChance = 0;
        #region ShowChance : 顯示成功率
        public void ShowChance ( int baseChance )
        {
            int EXC = extraChances [(int)method];
            totalChance = Mathf.Clamp ( EXC + baseChance , 0 , 100 );
            missionPrefab.Chance.text = totalChance.ToString () + "%";
        }
        #endregion

        #region ClearEmployeeIcon : 清除所有員工縮圖
        void ClearEmployeeIcon ()
        {
            for ( int i = 0; i < missionPrefab.IconBtn.Length; i++ )
            {
                missionPrefab.IconBtn [i].image.sprite = SpritePath.None;
            }
        }
        #endregion

        UnityAction GenerateOK ()
        {
            return () =>
            {
                PlayButtonAudio.self.PlayActivityClick ();
                OK ();
            };
        }
        #region OK : 確認鍵 ( Button )
        public void OK ()
        {
            int [] employeeIDs = manager.buildWorkerItem.GetEmployeeIDs ().ToArray ();
            if ( employeeIDs.Length == 0 )
            {
                MessageBox.Warning ( "必須至少委託一名員工" );
                return;
            }

            int activityID = manager.GetActivityID ();
            int difficult = (int)gm.activityList.Data [activityID].difficult;
            int executeShift = pm.calender.TotalShift + gm.activityList.Data [activityID].shift;
            int bonus = gm.customData.Data.RewardMoney [difficult];

            ActivityScheduale.Scheduale temp = new ActivityScheduale.Scheduale ( gm.activityList.GenerateEvent ( activityID ) , employeeIDs , activityID );
            bool success = Action.Activity.Success ( totalChance );
            temp.SetMethod ( method );
            temp.SetExcuteShift ( executeShift );
            temp.SetSuccessed ( success );
            temp.SetBonus ( bonus );
            if ( gm.activityList.Data [activityID].group > 0 )
                manager.tempGroup = true;

            for ( int i = 0; i < manager.tempScheduales.Count; i++ )
            {
                int id = manager.tempScheduales [i].ActivityID;

                if ( id == activityID )
                    manager.tempScheduales.RemoveAt ( i );
            }

            manager.tempScheduales.Add ( temp );
            Back ();
        }
        #endregion

        UnityAction GenerateBack ()
        {
            return () =>
            {
                PlayButtonAudio.self.PlayActivityCancel ();
                Back ();
            };
        }
        #region Back : 返回鍵 ( Button )
        public void Back ()
        {
            manager.ChangeState (); 
            manager.Close ();
            manager.buildWorkerItem.ClearEmployeeIDs ();
        }
        #endregion
    }
}
