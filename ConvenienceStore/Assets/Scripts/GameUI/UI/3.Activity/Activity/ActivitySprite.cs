﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ActivitySprite
{
    public static Sprite [] Type = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + "GameUI/Activity/Activity/Type" );
    public static Sprite [] Difficult = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + "GameUI/Activity/Activity/Difficult" );
    public static Sprite [] State = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + "GameUI/Activity/Activity/Mode" );
}
