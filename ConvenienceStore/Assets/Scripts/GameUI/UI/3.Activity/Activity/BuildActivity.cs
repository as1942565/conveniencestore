﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class BuildActivity : BaseBuildPanel
    {
        public Image OKImg;
        public Image BackImg;
        public SchedualeManager schedualeManager;
        public Transform Space;

        [SerializeField]
        private int page = 0;

        private const string activityPath = "GameUI/Activity/Activity/";

        private int pageLength = 4;
        private float distance_P = 70f;
        private int itemLength = 6;
        private float distance_I = 93f;

        Prefab.ActivityPagePrefab [] Page;
        Prefab.ActivityItemPrefab [] Item;

        public override void Init ()
        {
            for ( int i = 0; i < gm.activityList.Data.Length; i++ )
            {
                if ( pm.activityUnlock.GetMode ( i ) == ActivityUnlock.state.Processing )
                    pm.activityUnlock.SetMode ( i , ActivityUnlock.state.NotAvailable );
            }
            SwitchPage ( 0 );

            OKImg.sprite = SpritePath.OKBtn ();
            BackImg.sprite = SpritePath.BackBtn ();
        }

        public override void Build ()
        {
            Page = new Prefab.ActivityPagePrefab [pageLength];
            Item = new Prefab.ActivityItemPrefab [itemLength];

            for ( int i = 0; i < pageLength; i++ )
            {
                Prefab.ActivityPagePrefab page = Instantiate ( Resources.Load<Prefab.ActivityPagePrefab> ( "Prefabs/" + activityPath + "Page" ) , Space );
                page.PageImage.sprite = SpritePath.Page [i];
                page.TargetPageImage.sprite = SpritePath.TargetPage [i];
                page.transform.localPosition += new Vector3 ( distance_P * i , 0 , 0 );
                page.Button.onClick.AddListener ( GeneratePage ( i ) );
                Page [i] = page;
            }

            for ( int i = 0; i < itemLength; i++ )
            {
                Prefab.ActivityItemPrefab item = Instantiate ( Resources.Load<Prefab.ActivityItemPrefab> ( "Prefabs/" + activityPath + "Item" ) , Space );
                item.transform.localPosition += new Vector3 ( 0 , -distance_I * i , 0 );
                item.EntrustBtn.onClick.AddListener ( GenerateItem ( i ) );
                item.LockBtn.onClick.AddListener ( GenerateLockText ( i ) );
                Item [i] = item;
            }
            isBuild = true;
            schedualeManager.Build ();
        }

        UnityAction GeneratePage ( int id )
        {
            return () =>
            {
                PlayButtonAudio.self.PlayPage ();
                SwitchPage ( id );
            };
        }
        void SwitchPage ( int page )
        {
            for ( int p = 0; p < Page.Length; p++ )
            {
                Page [p].TargetPageImage.gameObject.SetActive ( p == page );
            }
            ShowItem ( page );
            this.page = page;
        }
        void ShowItem ( int page )
        {
            List<int> itemID = new List<int> ();
            for ( int i = 0; i < gm.activityList.Data.Length - 2; i++ )
            {
                if ( pm.activityUnlock.GetUnlock ( i ) )
                    itemID.Add ( i );
            }

            for ( int i = 0; i < gm.activityList.Data.Length - 2; i++ )
            {
                if ( !pm.activityUnlock.GetUnlock ( i ) )
                    itemID.Add ( i );
            }

            int itemQuantity = 0;
            for ( int i = 0; i < Item.Length; i++ )
            {
                if ( i + page * Item.Length >= itemID.Count )
                    continue;
                int id = itemID [i + page * Item.Length];  
                Item [i].UpdateData ( id , pm , gm );
                itemQuantity++;
            }
            for ( int i = itemQuantity; i < Item.Length; i++ )
            {
                Item [i].ClearData ( -1 );
            }
            schedualeManager.ChangeState ();
        }
        UnityAction GenerateItem ( int index )
        {
            return () =>
            {
                PlayButtonAudio.self.PlayActivityClick ();
                int activityID = Item [index].id;
                schedualeManager.SetActivityID ( activityID );
                schedualeManager.Open ();
            };
        }
        public Prefab.ActivityItemPrefab [] GetItem ()
        {
            return Item;
        }
        UnityAction GenerateLockText ( int index )
        {
            return () =>
            {
                PlayButtonAudio.self.PlayActivityClick ();
                for ( int i = 0; i < Item.Length; i++ )
                {
                    Item [i].LockTextObj.SetActive ( i == index && !Item [i].LockTextObj.activeSelf );
                }
            };
        }
    }
}