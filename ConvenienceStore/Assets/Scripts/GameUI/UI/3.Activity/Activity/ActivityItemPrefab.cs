﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class ActivityItemPrefab : MonoBehaviour
    {
        public int id;
        public Text NameText;
        public Image TypeImage;
        public Image DifficultImage;
        public Image StateImage;
        public GameObject Mask;
        public Button EntrustBtn;
        public Button LockBtn;
        public int type;
        public int difficult;
        public int group;
        public GameObject Lock;
        public GameObject LockTextObj;
        public Text LockText;

        public void UpdateData ( int id , PlayerDataManager pm , GameDataManager gm )
        {
            this.id = id;
            gameObject.name = "Item" + id.ToString ();
            GameData.Activity activity = gm.activityList.Data [id];
            //string name = activity.name;
            string activityName = Localizer.Translate ( activity.name );
            type = (int)activity.type;
            difficult = (int)activity.difficult;
            group = activity.group;
            int m = (int)pm.activityUnlock.GetMode ( id );
            Sprite state = ActivitySprite.State [m];

            NameText.text = activityName;
            TypeImage.sprite = ActivitySprite.Type [type];
            DifficultImage.sprite = ActivitySprite.Difficult [difficult];
            StateImage.sprite = state;
            EntrustBtn.gameObject.SetActive ( pm.activityUnlock.GetUnlock ( id ) );
            EntrustBtn.enabled = m == 0;
            Mask.SetActive ( false );
            Lock.SetActive ( !pm.activityUnlock.GetUnlock ( id ) );
            //LockText.text = gm.activityList.Data [id].conditionString;
            LockText.text = Localizer.Translate ( gm.activityList.Data [id].conditionString );
            LockTextObj.SetActive ( false );
        }

        public void ClearData ( int id )
        {
            gameObject.name = "None";
            Sprite None = SpritePath.None;
            NameText.text = "";
            TypeImage.sprite = None;
            DifficultImage.sprite = None;
            StateImage.sprite = None;
            EntrustBtn.gameObject.SetActive ( false );
            Mask.SetActive ( true );
            group = 0;
            Lock.SetActive ( false );
        }
    }
}
