﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class ActivityManager : BaseBuildPanel
    {
        public static ActivityManager self;
        private void Awake ()
        {
            self = this;
        }
        public SchedualeManager manager;
        public Button [] Type;
        public Image TypeImage;

        Sprite [] typeSpr;
        private const string TypePath = "GameUI/Activity/Type";

        public Builder [] Panel;
        public override void Init ()
        {
            SwitchPanel ( 0 );
            manager.Reset ();

            typeSpr = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + TypePath );

        }

        public override void Build ()
        {
            typeSpr = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + TypePath );
            for ( int i = 0; i < Type.Length; i++ )
            {
                Type [i].onClick.AddListener ( GenerateType ( i ) );
            }
            isBuild = true;
        }

        UnityAction GenerateType ( int id )
        {
            return () =>
            {
                PlayButtonAudio.self.PlayActivityType ();
                SwitchPanel ( id );
            };
        }
        public void SwitchPanel ( int id )
        {
            manager.tempScheduales.Clear ();
            for ( int i = 0; i < Panel.Length; i++ )
            {
                if ( i == id )
                    Panel [i].Open ();
                else
                    Panel [i].Close ();
            }
            TypeImage.sprite = typeSpr [id];
        }
    }
}
