﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class MainPanel : MonoBehaviour
    {
        public static MainPanel self;
        private void Awake ()
        {
            self = this;
        }

        private void Start ()
        {
            Panel [4].Open ();
            Panel [4].Close ();
            Button = GetComponentsInChildren<Button> ();
            for ( int i = 0; i < Button.Length; i++ )
            {
                Button[i].onClick.AddListener ( GeneratePanel ( i ) );
            }
        }

        public GameObject Shield;
        public GameObject Background;
        public GameObject MainAction;

        private Button [] Button;
        public BaseBuildPanel [] Panel;

        UnityAction GeneratePanel ( int id )
        {
            return () =>
            {
                PlayButtonAudio.self.PlayClick ();
                SwitchPanel ( id );
            };
        }

        public void SwitchPanel ( int id )
        {
            for ( int i = 0; i < Panel.Length; i++ )
            {
                if ( i == id )
                {
                    Panel [i].Open ();
                }
                else
                {
                    Panel [i].Close ();
                }
            }
            Shield.SetActive ( true );
        }

        public void Close ()
        {
            for ( int i = 0; i < Panel.Length; i++ )
            {
                Panel [i].Close ();
            }
            Shield.SetActive ( false );
        }

        bool maskBool;
        public void ShowWindow ( bool show )
        {
            Background.SetActive ( show );
            MainAction.SetActive ( show );

            if ( show == false )
            {
                maskBool = Shield.activeSelf;
                Shield.SetActive ( false );
            }
            if ( show == true )
            {
                Shield.SetActive ( maskBool );
            }
        }
    }
}
