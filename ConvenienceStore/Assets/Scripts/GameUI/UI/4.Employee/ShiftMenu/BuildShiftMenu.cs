﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace UI
{
    public class BuildShiftMenu : BaseBuildPanel
    {
        public Transform ShiftSapce;

        private int shiftQuantity = 4;
        private float shiftDistance = 60f;

        private const string path = "GameUI/Employee/ShiftMenu/";

        Prefab.ShiftMenuPrefab [] ShiftMenu;

        public override void Build ()
        {
            Sprite [] shiftSpr = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + path + "Shift" );
            Sprite [] shiftBackSpr = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + path + "ShiftBack" );
            ShiftMenu = new Prefab.ShiftMenuPrefab [shiftQuantity];
            for ( int i = 0; i < ShiftMenu.Length; i++ )
            {
                Prefab.ShiftMenuPrefab shift = Instantiate ( Resources.Load<Prefab.ShiftMenuPrefab> ( "Prefabs/" + path + "ShiftMenu" ) , ShiftSapce );
                shift.transform.localPosition += new Vector3 ( 0 , -shiftDistance * i , 0 );
                shift.ShiftMenu.sprite = shiftBackSpr [i];
                shift.Shift.sprite = shiftSpr [i];
                shift.Button.onClick.AddListener ( GenerateShift ( i ) );
                shift.name = i.ToString ();
                ShiftMenu [i] = shift;
            }

            isBuild = true;
        }

        UnityAction GenerateShift ( int id )
        {
            return () =>
            {
                Switchshift ( id );
                PlayButtonAudio.self.PlayShift ();
            };
        }
        public event System.Action<int> OnEmployeeShiftUpdated;
        void Switchshift ( int id )
        {
            Close ();
            if ( OnEmployeeShiftUpdated != null )
            {
                OnEmployeeShiftUpdated ( id );
            }
        }

        public override void Init ()
        {
            Sprite [] shiftSpr = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + path + "Shift" );
            Sprite [] shiftBackSpr = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + path + "ShiftBack" );
            
            for ( int i = 0; i < ShiftMenu.Length; i++ )
            {
                ShiftMenu [i].ShiftMenu.sprite = shiftBackSpr [i];
                ShiftMenu [i].Shift.sprite = shiftSpr [i];
            }
        }
    }
}