﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class ShiftMenuPrefab : MonoBehaviour
    {
        public Image ShiftMenu;
        public Image Shift;
        public Button Button;
    }
}