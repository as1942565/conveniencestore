﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class EmployeeItemPrefab : MonoBehaviour
    {
        public int id;
        public int shift;
        public Image Actor;
        public Image Shift;
        public GameObject [] Powers;
        public Button ActorBtn;
        public Button ShiftBtn;

        private const string path = "GameUI/Employee/";
        Sprite [] shiftSpr;
        Sprite NoShiftSpr;

        private void Start ()
        {
            GetSprite ();
        }
        void GetSprite ()
        {
            shiftSpr = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + path + "Shift" );    
            if ( shiftSpr == null )
            {
                Debug.LogError ( path + "Shift路徑不正確" );
            }
        }

        public void UpdateShift ( int id , int shift, PlayerDataManager pm )
        {
            this.shift = shift;
            GetSprite ();
            Shift.sprite = shiftSpr [shift];
        }
        public void UpdateData ( int id , PlayerDataManager pm )
        {
            if ( id == -1 )
            {
                ClearData ();
                return;
            }
            this.name = id.ToString ();
            this.id = id;
            Sprite sprite = SpritePath.ActorIcon [id];
            Actor.sprite = sprite;

            ActorBtn.gameObject.SetActive ( true );
            int power = pm.employeeList.GetEmployee ( id ).GetPower ();
            for ( int i = 0; i < Powers.Length; i++ )
            {
                Powers [i].SetActive ( i < power );
            }
            ShiftBtn.enabled = power > 0;
        }
        public void ClearData ()
        {
            this.name = "None";
            id = -1;
            if ( NoShiftSpr == null )
                GetNoShiftSprite ();
            Shift.sprite = NoShiftSpr;
            ActorBtn.gameObject.SetActive ( false );
            for ( int i = 0; i < Powers.Length; i++ )
            {
                Powers [i].SetActive ( false );
            }
            ShiftBtn.enabled = false;
        }
        void GetNoShiftSprite ()
        {
            NoShiftSpr = Resources.LoadAll<Sprite> ( "Sprites/Common/" + path + "NoShift" ) [0];   
        }
    }
}