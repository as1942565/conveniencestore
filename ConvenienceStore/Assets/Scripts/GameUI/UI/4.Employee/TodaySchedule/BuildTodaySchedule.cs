﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Tools;

namespace UI
{
    public class BuildTodaySchedule : BaseBuildPanel
    {
        public Image Background;
        public Transform ItemSpace;
        public Button BackBtn;

        private int quantityX = 4;
        private float distanceX = 140f;
        private int quantityY = 3;
        private float distanceY = 145f;

        private const string path = "GameUI/Employee/TodaySchedule/";

        Prefab.TodaySchedulePrefab [] [] Item;

        BuildEmployee buildEmployee;
        public void GetBuildEmployee ( BuildEmployee buildEmployee )
        {
            this.buildEmployee = buildEmployee;
        }

        public override void Build ()
        {
            Item = new Prefab.TodaySchedulePrefab [quantityY] [];
            for ( int y = 0; y < quantityY; y++ )
            {
                Item [y] = new Prefab.TodaySchedulePrefab [quantityX];
                for ( int x = 0; x < quantityX; x++ )
                {
                    Prefab.TodaySchedulePrefab item = Instantiate ( Resources.Load<Prefab.TodaySchedulePrefab> ( "Prefabs/" + path + "Item" ) , ItemSpace );
                    item.transform.localPosition = new Vector3 ( distanceX * x , -distanceY * y , 0 );
                    Item [y] [x] = item;
                }
            }
            isBuild = true;

            BackBtn.onClick.AddListener ( GenerateBack () );
        }

        public bool IsOpen ()
        {
            return isBuild;
        }
        
        public override void Init ()
        {
            for ( int s = 0; s < Item.Length; s++ )
            {
                for ( int i = 0; i < Item [s].Length; i++ )
                {
                    int lockQuantity = pm.employeeScheduing.GetUnlock ( s );
                    Item [s] [i].Lock.SetActive ( i >= lockQuantity );
                    Item [s] [i].ClearData ();
                }
            }

            IntList [] employeeID = GetTempEmployeeID ();
            for ( int s = 0; s < employeeID.Length; s++ )
            {
                for ( int i = 0; i < employeeID [s].Count; i++ )
                {
                    int id = employeeID [s] [i];
                    Item [s] [i].UpdateData ( id );
                }
            }

            Background.sprite =  Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameUI/Employee/TodaySchedule" );


        }
        IntList [] GetTempEmployeeID ()
        {
            IntList [] employeeID = new IntList [3];
            for ( int s = 0; s < employeeID.Length; s++ )
            {
                employeeID [s] = new IntList ();
            }
            int [] temp = buildEmployee.GetTempEmployeeShift ();
            for ( int i = 0; i < temp.Length; i++ )
            {
                int shift = temp [i];
                if ( shift != 3 )
                {
                    employeeID [shift].Add ( i );
                }
            }
            return employeeID;
        }

        UnityAction GenerateBack ()
        {
            return () =>
            {
                PlayButtonAudio.self.PlayCancelShift ();
                Close ();
            };
        }
    }
}