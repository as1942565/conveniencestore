﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class TodaySchedulePrefab : MonoBehaviour
    {
        public GameObject Lock;
        public Image ActorIcon;

        public void UpdateData ( int id )
        {
            ActorIcon.sprite = SpritePath.ActorIcon[id];
        }

        public void ClearData ()
        {
            ActorIcon.sprite = SpritePath.None;
        }
    }
}