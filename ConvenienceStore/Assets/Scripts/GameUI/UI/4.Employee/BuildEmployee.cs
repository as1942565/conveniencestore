﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Tools;

namespace UI
{
    public class BuildEmployee : BaseBuildPanel
    {
        [SerializeField]
        private int calenderDay = 0;

        public Image Background;
        public Image CheckToday;
        public Transform ItemSpace;
        public Transform EmployeeSpace;
        public BuildShiftMenu buildShiftMenu;
        public BuildTodaySchedule buildTodaySchedule;
        public Text [] ScheduleQuantityText;
        public Button Back;
        public Button OK;

        private int quantityX = 5;
        private float distanceX = 170f;
        private int quantityY = 4;
        private float distanceY = 270f;

        private const string path = "GameUI/Employee/";

        Prefab.EmployeePrefab employee;
        Prefab.EmployeeItemPrefab [] Item;
        [SerializeField]
        IntList [] tempEmployeeShift;

        public override void Build ()
        {
            employee = Instantiate ( Resources.Load<Prefab.EmployeePrefab> ( "Prefabs/" + path + "Employee" ) , EmployeeSpace );

            Item = new Prefab.EmployeeItemPrefab [quantityX * quantityY];
            int id = 0;
            for ( int y = 0; y < quantityY; y++ )
            {
                for ( int x = 0; x < quantityX; x++ )
                {
                    Prefab.EmployeeItemPrefab item = Instantiate ( Resources.Load<Prefab.EmployeeItemPrefab> ( "Prefabs/" + path + "Item" ) , ItemSpace );
                    item.transform.localPosition = new Vector3 ( distanceX * x , -distanceY * y , 0 );
                    item.ActorBtn.onClick.AddListener ( GenerateActor ( id ) );
                    item.ShiftBtn.onClick.AddListener ( GenerateShift ( id ) );
                    Item [id] = item;
                    id++;
                }
            }

            buildShiftMenu.OnEmployeeShiftUpdated += OnEmployeeShiftChanged;
            buildTodaySchedule.GetBuildEmployee ( this );

            tempEmployeeShift = new IntList [Calender.MaximumDay];
            for ( int d = 0; d < tempEmployeeShift.Length; d++ )
            {
                tempEmployeeShift [d] = new IntList ();
                for ( int i = 0; i < gm.workerList.Data.Length; i++ )
                {
                    tempEmployeeShift [d].Add ( 3 );
                }
            }

            isBuild = true;
            Back.onClick.AddListener ( GenerateBack () );
            OK.onClick.AddListener ( GenerateOK () );
        }

        public override void Destroy ()
        {
            buildShiftMenu.OnEmployeeShiftUpdated -= OnEmployeeShiftChanged;
            base.Destroy ();
        }

        UnityAction GenerateActor ( int id )
        {
            return () =>
            {
                SwitchActor ( id );
                PlayButtonAudio.self.PlayEmployeeSwitch ();
            };
        }
        void SwitchActor ( int id )
        {
            int [] unlockIDs = pm.employeeList.GetAllUnlockID ();
            if ( id >= unlockIDs.Length )
            {
                Debug.LogError ( id + "超過解鎖數量" );
                return;
            }
            int employeeID = unlockIDs [id];
            employee.UpdateData ( employeeID , pm.employeeList.GetEmployee ( employeeID ) , gm.customData.Data.EmployeeRelation );
        }
        UnityAction GenerateShift ( int id )
        {
            return () =>
            {
                SwitchShift ( id );
                PlayButtonAudio.self.PlayShift ();
            };
        }
        private int itemID;
        void SwitchShift ( int itemID )
        {
            this.itemID = itemID;
            buildShiftMenu.transform.position = Item [itemID].transform.position;
            buildShiftMenu.Open ();
        }

        void OnEmployeeShiftChanged ( int shift )
        {
            if ( Item [itemID].shift == shift )
                return;
            if ( CheckScheduleQuantity ( shift ) )
            {
                int employeeID = Item [itemID].id;
                tempEmployeeShift [calenderDay][employeeID] = shift;
                Item [itemID].UpdateShift ( employeeID , shift , pm );
                UpdateScheduleQuantity ();

                if ( buildTodaySchedule.IsOpen () )
                {
                    buildTodaySchedule.Init ();
                }
            }
            else
            {
                MessageBox.Warning ( "排班欄位已滿" );
            }
        }

        public int [] GetTempEmployeeShift ()
        {
            return tempEmployeeShift [calenderDay].ToArray ();
        }

        bool CheckScheduleQuantity ( int shift )
        {
            if ( shift == 3 )
                return true;

            int [] quantity = GetQuantity ();
            return quantity [shift] != pm.employeeScheduing.GetUnlock ( shift );
        }

        int [] GetQuantity ()
        {
            int [] quantity = new int [tempEmployeeShift[calenderDay].Count];
            for ( int s = 0; s < quantity.Length; s++ )
            {
                int tempShift = tempEmployeeShift[calenderDay] [s];
                if ( tempShift == 3 )
                    continue;
                quantity [tempShift]++;
            }
            return quantity;
        }

        void UpdateScheduleQuantity ()
        {
            int [] quantity = GetQuantity ();
            for ( int i = 0; i < ScheduleQuantityText.Length; i++ )
            {
                int maxQuantity = pm.employeeScheduing.GetUnlock ( i );
                ScheduleQuantityText [i].text = quantity [i].ToString () + "/" + maxQuantity.ToString ();
            }
        }

        public override void Init ()
        {
            calenderDay = pm.calender.GetDay () + 1;
            SwitchActor ( 0 );

            int [] unlcokID = pm.employeeList.GetAllUnlockID ();

            for ( int i = 0; i < unlcokID.Length; i++ )
            {
                int id = unlcokID [i];
                Item [i].UpdateData ( id , pm );
                int shift = pm.employeeScheduleLog.GetEmployeeShift ( calenderDay , id , pm.employeeList );
                Item [i].UpdateShift ( id , shift , pm );
                if ( calenderDay < 30 )
                    tempEmployeeShift [calenderDay] [id] = shift;
            }
            for ( int i = unlcokID.Length; i < Item.Length; i++ )
            {
                Item [i].ClearData ();
            }
            UpdateScheduleQuantity ();
            buildShiftMenu.Close ();
            buildTodaySchedule.Close ();
            OK.GetComponent<Image> ().sprite = SpritePath.OKBtn4 () [0];
            Back.GetComponent<Image> ().sprite = SpritePath.OKBtn4 () [1];
            Background.sprite = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameUI/Employee/Background" );
            CheckToday.sprite = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameUI/Employee/CheckTodayBtn" );
        }

        public void CheckTodayScheduleBtn ()
        {
            buildTodaySchedule.Open ();
            PlayButtonAudio.self.PlayCheckShift ();
        }

        UnityAction GenerateOK ()
        {
            return () =>
            {
                OKBtn ();
                PlayButtonAudio.self.PlayConfirmShift ();
            };
        }

        public void OKBtn ()
        {
            int today = pm.calender.GetDay () + 1;
            for ( int d = today; d < Calender.MaximumDay; d++ )
            {
                for ( int i = 0; i < tempEmployeeShift [d].Count; i++ )
                {
                    int shift = tempEmployeeShift [d] [i];
                    if ( shift == 3 )
                    {
                        pm.employeeScheduleLog.ResetEmployeeID ( d , i );
                        continue;
                    }
                    pm.employeeScheduleLog.SetEmployeeID ( d , i , shift );
                }
            }
            BackBtn ();
        }

        UnityAction GenerateBack ()
        {
            return () =>
            {
                BackBtn ();
                PlayButtonAudio.self.PlayEmployeeCancel ();
            };
        }

        public void BackBtn ()
        {
            MainPanel.self.Close ();

            if ( BuildLog.self.OnClosed != null )
            {
                BuildLog.self.OnClosed ();
            }
        }
    }
}