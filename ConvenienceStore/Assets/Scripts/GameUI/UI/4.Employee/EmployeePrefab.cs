﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Prefab
{
    public class EmployeePrefab : MonoBehaviour
    {
        public Image ActorImage;
        public Image DataImage;
        public Text RelationRankText;
        public Text RelationText;
        public RectTransform rect;
        public GameObject RelationObj;

        private float width;
        private float height;

        private const string path = "GameUI/Employee/Actor/Actor";
        //Sprite [] actorSpr;
        Sprite [] dataSpr;

        private void Awake ()
        {
            width = rect.rect.width;
            height = rect.rect.height;
        }

        Sprite GetActorSprite ( int id )
        {
            Sprite actorSpr = Resources.Load<Sprite> ( "Sprites/Common/" + path + id.ToString () );
            if ( actorSpr == null )
            {
                //Debug.LogError ( "Sprites/Common/" + path + id.ToString () + "路徑不正確" );
                return SpritePath.None;
            }
            return actorSpr;
        }

        void GetDataSprite ()
        {
            dataSpr = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + path + "EmployeeData" );
            if ( dataSpr == null )
            {
                Debug.LogError ( path + "EmployeeData" + "路徑不正確" );
            }
        }

        public void UpdateData ( int id , Employee employee , int [] maxExp )
        {
            if ( dataSpr == null )
            {
                GetDataSprite ();
            }

            RelationObj.GetComponent<Image> ().sprite = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameUI/Employee/Actor/Relation" );
            Sprite actorSpr = GetActorSprite ( id );
            if ( GameLoopManager.instance.GetSteam () == true && id == 2 )
                actorSpr = SpritePath.None;
            ActorImage.sprite = actorSpr;
            if ( id >= dataSpr.Length )
            {
                DataImage.sprite = SpritePath.None;
            }
            else
                DataImage.sprite = dataSpr [id];
            if ( employee.IsMain () )
                Relation ( id , employee , maxExp );
            RelationObj.SetActive ( employee.IsMain () );

        }
        void Relation ( int id , Employee employee , int [] maxExp )
        {
            rect.DOSizeDelta ( new Vector2 ( width , 0 ) , 0 );
            int rank = employee.GetRelationRank ();
            int exp = employee.GetRelationEXP ();
            RelationRankText.text = rank.ToString ();
            RelationText.text = exp.ToString ();
            if ( rank + 1 >= maxExp.Length )
            {
                rect.DOSizeDelta ( new Vector2 ( width , 0 ) , 1 );
            }
            else
            {
                int max = maxExp [rank + 1];
                float scale = (float)exp / (float)max;
                rect.DOSizeDelta ( new Vector2 ( width , height * scale ) , 1 );
            }
        }
    }
}