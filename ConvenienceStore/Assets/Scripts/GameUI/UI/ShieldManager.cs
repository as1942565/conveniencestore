﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI
{
    public class ShieldManager : MonoBehaviour, IPointerClickHandler
    {
        void Notice ()
        {
            if ( GameLoopManager.instance.IsHire == true )
                return;
            if ( StoryManager.self != null && StoryManager.self.gameObject.activeSelf == true )
                return;
            if ( SetupCommodityManager.updateBool )
            {
                MessageBox.MainShow ( "上架尚未完成，確定要離開嗎？" );
                StartCoroutine ( Waitting () );
                //StartCoroutine ( UI.BuildMenuItem.self.CkeckUpdate ( 0 ) );
            }
            else if ( BuildRestock.updateBool )
            {
                MessageBox.MainShow ( "進貨尚未完成，確定要離開嗎？" );
                StartCoroutine ( Waitting () );
                //StartCoroutine ( UI.BuildMenuItem.self.CkeckUpdate ( 0 ) );
            }
            else if ( OptionManager.updateBool )
            {
                MessageBox.MainShow ( "設定尚未存檔，確定要離開嗎？" );
                StartCoroutine ( Waitting () );
                //StartCoroutine ( UI.BuildMenuItem.self.CkeckUpdate ( 0 ) );
            }
            else
                Close ();
        }

        void Update ()
        {
            if ( Input.GetMouseButtonDown ( 1 ) )
            {
                Notice ();
            }
        }

        public void OnPointerClick ( PointerEventData eventData )
        {
            Notice ();
        }
        IEnumerator Waitting ()
        {
            while ( MessageBox.self.wait )
            {
                yield return null;
            }
            if ( MessageBox.self.yes )
            {
                Close ();
            }
        }

        void Close ()
        {
            SetupCommodityManager.updateBool = false;
            BuildRestock.updateBool = false;
            if ( BuildRestock.self != null )
            BuildRestock.self.Back ();
            MainPanel.self.Close ();
            OptionManager.self.Close ();
            if ( BuildDragIcon.self != null )
                BuildDragIcon.self.Close ();
        }
    }
}