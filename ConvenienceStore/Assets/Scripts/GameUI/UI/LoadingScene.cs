﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LoadingScene : MonoBehaviour
{
    public static LoadingScene self;
    private void Awake ()
    {
        self = this;
    }

    public Image Shield;

    private void Start ()
    {   
        FadeIn ( 0.5f );
    }

    public void Transitions ( float fadeTime , float blackTime , bool screenshot = true )
    {
        OpenImage ();
        Invoke ( "CloseImage" , fadeTime + blackTime );
        Shield.DOFade ( 1 , fadeTime );
        Shield.DOFade ( 0 , fadeTime ).SetDelay ( blackTime );
        if ( screenshot )
        {
            if ( ScreenShot.self != null )
                ScreenShot.self.CreateScreenshot ( fadeTime + blackTime );
        }
    }

    public void FadeIn ( float fadeTime )
    {
        OpenImage ();
        Color color = Shield.color;
        color.a = 1;
        Shield.color = color;
        Shield.DOFade ( 0 , fadeTime );
        Invoke ( "CloseImage" , fadeTime );
    }

    public void FadeOut ( float fadeTime )
    {
        OpenImage ();
        Color color = Shield.color;
        color.a = 0;
        Shield.color = color;
        Shield.DOFade ( 1 , fadeTime );
    }

    void OpenImage ()
    {
        Shield.enabled = true;
    }

    void CloseImage ()
    {
        Shield.enabled = false;
    }
}
