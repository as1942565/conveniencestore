﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace UI
{
    public class MessageBox : MonoBehaviour
    {
        [HideInInspector]
        public bool yes;
        [HideInInspector]
        public bool wait;

        public static MessageBox self;
        private void Awake ()
        {
            self = this;
            SceneManager.activeSceneChanged += ChangedActiveScene;
        }
        private MessageBox () { }

        private void ChangedActiveScene ( Scene current , Scene next )
        {
            canvas.worldCamera = Camera.main;
        }

        public Canvas canvas;
        public GameObject Shield;
        public Text msgText;
        public Animation WarningBox;
        public Text warningText;
        public Animation NoticeBox;
        public Text noticeText;

        public static void MainShow ( string msg )
        {
            self.Show ( Localizer.Translate ( msg ) );
        }
        void Show ( string msg )
        {
            wait = true;
            Shield.SetActive ( true );
            msgText.text = msg;
        }

        public static void Warning ( string msg )
        {
            self.warning ( Localizer.Translate ( msg ) );
        }
        void warning ( string msg )
        {
            WarningBox.gameObject.SetActive ( true );
            warningText.text = msg;
            WarningBox.Stop ();
            WarningBox.Play ();
        }
        public static void Notice ( string msg )
        {
            self.notice ( Localizer.Translate ( msg ) );
        }
        void notice ( string msg )
        {
            NoticeBox.gameObject.SetActive ( true );
            noticeText.text = msg;
            NoticeBox.Stop ();
            NoticeBox.Play ();
            EffectManager.self.Play ( "Audios/Effect/" , "Notice" , false , EffectPlayer.AudioType.system );
        }

        public void OKBtn ()
        {
            yes = true;
            wait = false;
            Shield.SetActive ( false );
        }

        public void CancelBtn ()
        {
            yes = false;
            wait = false;
            Shield.SetActive ( false );
        }
    }
}