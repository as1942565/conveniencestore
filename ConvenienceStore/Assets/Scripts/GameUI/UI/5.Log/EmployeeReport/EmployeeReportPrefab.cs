﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class EmployeeReportPrefab : MonoBehaviour
    {
        public ActivityScheduale.Scheduale scheduale;
        public GameObject Frame;
        public Image [] ActorIcon;
        public Text ActivityName;
        public Text BonusText;
        public Text ResultText;
        public Button Button;

        public void UpdateData ( ActivityScheduale.Scheduale scheduale , GameData.ActivityList activityList , int yesterday )
        {
            this.scheduale = scheduale;
            Actor ( scheduale );
            Activity ( scheduale , activityList , yesterday );
            Button.enabled = true;
            Frame.SetActive ( false );
        }
        public void ClearData ()
        {
            for ( int i = 0; i < ActorIcon.Length; i++ )
            {
                ActorIcon [i].sprite = SpritePath.None;
            }
            ActivityName.text = "";
            BonusText.text = "";
            ResultText.text = "";
            Button.enabled = false;
        }

        void Actor ( ActivityScheduale.Scheduale scheduale )
        {
            int [] employeeIDs = scheduale.EmployeesID;
            for ( int i = 0; i < ActorIcon.Length; i++ )
            {
                if ( i < employeeIDs.Length )
                {
                    int id = employeeIDs [i];
                    ActorIcon [i].sprite = SpritePath.ActorIcon [id];
                }
                else
                    ActorIcon [i].sprite = SpritePath.None;
            }
        }
        void Activity ( ActivityScheduale.Scheduale scheduale , GameData.ActivityList activityList , int yesterday )
        {
            int activityID = scheduale.ActivityID;
            //ActivityName.text = activityList.Data [activityID].name;
            string name = Localizer.Translate ( activityList.Data [activityID].name );
            ActivityName.text = name;

            if ( scheduale.GetMethod () == ActivityScheduale.Scheduale.SchedualeMethod.money )
                BonusText.text = scheduale.GetBonus ().ToString ( "N0" ) + Localizer.Translate ( "元" );
            else
                BonusText.text = "0" + Localizer.Translate ( "元" );

            if ( yesterday < scheduale.GetExecuteShift () / 3 )
            {
                ResultText.text = Localizer.Translate ( "進行中" );
                return;
            }
            if ( scheduale.GetSuccessed () )
            {
                ResultText.text = Localizer.Translate ( "圓滿成功" );
                return;
            }
            ResultText.text = Localizer.Translate ( "失敗" );
        }
    }
}