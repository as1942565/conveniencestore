﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class ScheduleReportPrefab : MonoBehaviour
    {
        public Text TotalText;
        public Text RunText;
        public Text SuccessText;
        public Text FailText;

        public Image CommodityIcon;
        public Text CommdoityName;

        public void UpdateData ( Calender calender , GameLog log , ActivityScheduleLog activityScheduleLog , GameData.CommodityList commodity )
        {
            int day = calender.GetDay ();
            Number ( day , activityScheduleLog );
            Commodity ( day , log , commodity );
        }
        void Number ( int day , ActivityScheduleLog activityScheduleLog )
        {
            int run = activityScheduleLog.GetRun ( day );
            int success = activityScheduleLog.GetSuccessed ( day );
            int fail = activityScheduleLog.GetFail ( day );
            int total = run + success + fail;

            TotalText.text = total.ToString ();
            RunText.text = run.ToString ();
            SuccessText.text = success.ToString ();
            FailText.text = fail.ToString ();
        }
        void Commodity ( int day ,  GameLog log , GameData.CommodityList commodity )
        {
            int commodityID = log.GetSpecialCommodityID ( day , commodity );
            if ( commodityID == -1 )
            {
                CommodityIcon.sprite = SpritePath.None;
                CommdoityName.text = "";
                return;
            }
            //string name = commodity.Data [commodityID].name;
            string name = Localizer.Translate ( commodity.Data [commodityID].name );
            int quantity = log.GetSpecialCommodityQuantity ( day , commodity );

            CommodityIcon.sprite = SpritePath.Commodity [commodityID];
            CommdoityName.text = name + " x " + quantity.ToString ();
        }
    }
}