﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class BuildEmployeeReport : BaseBuildPanel
    {
        [SerializeField]
        private int itemID = -1;

        public Image Background;
        public Image OKImg;
        public BuildShowActivity buildShowActivity;
        public Transform ItemSpace;
        public Transform ReportSpace;

        private int itemQuantity = 10;
        private int itemDistance = 115;

        private const string path = "GameUI/Log/EmployeeReport/";

        Prefab.EmployeeReportPrefab [] Item;
        Prefab.ScheduleReportPrefab ScheduleReport;

        public override void Build ()
        {
            Item = new Prefab.EmployeeReportPrefab [itemQuantity];
            for ( int i = 0; i < itemQuantity; i++ )
            {
                Prefab.EmployeeReportPrefab item = Instantiate ( Resources.Load<Prefab.EmployeeReportPrefab> ( "Prefabs/" + path + "Item" ) , ItemSpace );
                item.transform.localPosition += new Vector3 ( 0 , -i * itemDistance , 0 );
                item.Button.onClick.AddListener ( GenerateItem ( i ) );
                Item [i] = item;
            }
            ScheduleReport = Instantiate ( Resources.Load<Prefab.ScheduleReportPrefab> ( "Prefabs/" + path + "ScheduleReport" ) , ReportSpace );
            isBuild = true;
        }

        UnityAction GenerateItem ( int id )
        {
            return () =>
            {
                PlayButtonAudio.self.PlayReport ();
                SwitchItem ( id );
            };
        }
        void SwitchItem ( int id )
        {
            if ( itemID == id )
            {
                buildShowActivity.Close ();
                Item [itemID].Frame.SetActive ( false );
                itemID = -1;
                return;
            }
            for ( int i = 0; i < Item.Length; i++ )
            {
                Item [i].Frame.SetActive ( i == id );
            }

            ActivityScheduale.Scheduale scheduale = Item [id].scheduale;
            buildShowActivity.SetScheduale ( scheduale );
            buildShowActivity.Open ();
            itemID = id;
        }

        public void OKBtn ()
        {
            PlayButtonAudio.self.PlayReport ();
            Close ();
        }

        public override void Init ()
        {
            buildShowActivity.Close ();
            for ( int i = 0; i < Item.Length; i++ )
                Item [i].Frame.SetActive ( false );
            itemID = -1;

            int day = pm.calender.GetDay ();
            ScheduleReport.UpdateData ( pm.calender , pm.gameLog , pm.activityScheduleLog , gm.commodityList );
            ActivityScheduale.Scheduale [] scheduales = pm.activityScheduleLog.GetScheduales ( day );
            for ( int i = 0; i < scheduales.Length; i++ )
            {
                Item [i].UpdateData ( scheduales [i] , gm.activityList , day );
            }
            for ( int i = scheduales.Length; i < Item.Length; i++ )
            {
                Item [i].ClearData ();
            }
            OKImg.sprite = SpritePath.OKBtn4 () [0];
            Background.sprite = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameUI/Log/EmployeeReport/Background" );
        }
    }
}