﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{

    public class BuildShowActivity : BaseBuildPanel
    {
        Prefab.MissionPrefab mission;

        private const string path = "GameUI/Activity/Activity/Mission/";

        private ActivityScheduale.Scheduale scheduale;
        public void SetScheduale ( ActivityScheduale.Scheduale scheduale )
        {
            this.scheduale = scheduale;
        }

        public override void Build ()
        {
            mission = Instantiate ( Resources.Load<Prefab.MissionPrefab> ( "Prefabs/" + path + "Mission" ) , transform );
            mission.transform.localPosition = new Vector3 ( -550 , 0 , 0 );
            mission.LockButton ();
            isBuild = true;
        }

        public override void Init ()
        {
            int id = scheduale.ActivityID;
            GameData.Activity activity = gm.activityList.Data [id];
            //string name = activity.name;
            string name = Localizer.Translate ( activity.name );
            int type = (int)activity.type;
            int difficult = (int)activity.difficult;
            mission.UpdateData ( id , gm );
            mission.ChangeReward ( (int)scheduale.GetMethod () );
            ChangeChance ();
            ChangeEmployee ();
        }
        void ChangeChance ()
        {
            int id = scheduale.ActivityID;
            GameData.Activity activity = gm.activityList.Data [id];
            GameData.Activity.ActivityType type = activity.type;
            GameData.Activity.ActivityDifficult difficult = activity.difficult;
            int chance = Action.Activity.Chance ( scheduale.EmployeesID, type , difficult , gm , pm );//取得基本成功率
            int [] EC = new int [] { 0 , 10 , 20 };
            int extraChance = EC [(int)scheduale.GetMethod ()];
            int totalChance = chance + extraChance;
            totalChance = Mathf.Clamp ( totalChance , 0 , 100 );
            mission.Chance.text = totalChance.ToString () + "%";
        }
        void ChangeEmployee ()
        { 
            for ( int i = 0; i < mission.IconBtn.Length; i++ )
            {
                if ( i >= scheduale.EmployeesID.Length )
                {
                    mission.IconBtn [i].image.sprite = SpritePath.None;
                    continue;
                }
                int id = scheduale.EmployeesID [i];
                Sprite actorIcon = SpritePath.ActorIcon [id];
                mission.IconBtn [i].image.sprite = actorIcon;
            }
        }
    }

}