﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class IncomePrefab : MonoBehaviour
    {
        [HideInInspector]
        public bool finish;

        public Text TotalText;
        private int total;
        public Text [] CommodityText;
        private int [] commodity;
        public Text MVPText;
        private string MVP;
        public Text PopularText;
        private string popular;
        public Text EXText;
        private int ex;

        private void Awake ()
        {
            commodity = new int [CommodityText.Length];
        }

        public void UpdateData ( PlayerDataManager pm , GameDataManager gm )
        {
            int day = pm.calender.GetDay ();
            if ( day < 0 )
            {
                ClearData ();
                return;
            }
            total = 0;
            for ( int i = 0; i < CommodityText.Length; i++ )
            {
                commodity [i] = pm.gameLog.GetAllCommodityIncomByTypeWithDay ( day , i , gm );
                CommodityText [i].text = commodity [i].ToString ( "N0" ) + Localizer.Translate ( "元" );
                total += commodity [i];
            }
            MVPIncome ( pm , gm , day );
            MVPPopular ( pm , gm , day );
            
            EXText.text = "0" + Localizer.Translate ( "元" );
            TotalText.text = total.ToString ( "N0" ) + Localizer.Translate ( "元" );
            pm.gameLog.SetTotalIncome ( day , total );
        }
        void MVPIncome ( PlayerDataManager pm , GameDataManager gm , int day )
        {
            int mvpID = pm.gameLog.GetMVPIncome ( day );
            if ( mvpID == -1 )
            {
                MVP = Localizer.Translate ( "無" );
                MVPText.text = MVP;
                return;
            }
            //MVP = gm.commodityList.Data [mvpID].name;
            MVP = Localizer.Translate ( gm.commodityList.Data[mvpID].name );
            MVPText.text = MVP;
        }
        void MVPPopular ( PlayerDataManager pm , GameDataManager gm , int day )
        {
            int popularID = pm.gameLog.GetMVPPopular ( day );
            if ( popularID == -1 )
            {
                popular = Localizer.Translate ( "無" );
                PopularText.text = popular;
                return;
            }
            //popular = gm.commodityList.Data [popularID].name;
            popular = Localizer.Translate ( gm.commodityList.Data [popularID].name );
            PopularText.text = popular;
        }

        public void ClearData ()
        {
            for ( int i = 0; i < CommodityText.Length; i++ )
            {
                CommodityText [i].text = "0" + Localizer.Translate ( "元" );
            }
            MVPText.text = Localizer.Translate ( "無" );
            PopularText.text = Localizer.Translate ( "無" );
            EXText.text = "0" + Localizer.Translate ( "元" );
            TotalText.text = "0" + Localizer.Translate ( "元" );
        }

        

        public void Init ()
        {
            finish = false;
            TotalText.text = "";
            for ( int i = 0; i < CommodityText.Length; i++ )
            {
                CommodityText [i].text = "";
            }
            MVPText.text = "";
            PopularText.text = "";
            EXText.text = "";
        }

        private int part = 20;

        public IEnumerator Play ()
        {
            int n = 0;
            int run = 0;

            float tempTotal = total / part;
            if ( tempTotal > 0 )
            {
                while ( n < part )
                {
                    run += Mathf.FloorToInt ( tempTotal );
                    TotalText.text = run.ToString ( "N0" ) + Localizer.Translate ( "元" );
                    yield return new WaitForSeconds ( 1 / part );
                    n++;
                }
            }

            TotalText.text = total.ToString ( "N0" ) + Localizer.Translate ( "元" );
            yield return new WaitForSeconds ( 0.3f );

            n = 0;
            run = 0;

            for ( int i = 0; i < commodity.Length; i++ )
            {
                float tempCommodity = commodity [i] / part;
                if ( tempCommodity > 0 )
                {
                    while ( n < part )
                    {
                        run += Mathf.FloorToInt ( tempCommodity );
                        CommodityText [i].text = run.ToString ( "N0" ) + Localizer.Translate ( "元" );
                        yield return new WaitForSeconds ( 1 / part );
                        n++;
                    }
                }
                CommodityText [i].text = commodity [i].ToString ( "N0" ) + Localizer.Translate ( "元" );
                yield return new WaitForSeconds ( 0.3f );
                n = 0;
                run = 0;
            }
            
            MVPText.text = MVP;
            yield return new WaitForSeconds ( 0.3f );
            PopularText.text = popular;
            yield return new WaitForSeconds ( 0.3f );
            EXText.text = "0" + Localizer.Translate ( "元" );
            yield return new WaitForSeconds ( 0.3f );
            finish = true;
        }
    }
}
