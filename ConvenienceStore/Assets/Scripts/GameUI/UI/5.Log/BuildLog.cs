﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class BuildLog : BaseBuildPanel
    {
        public static BuildLog self;
        private void Awake ()
        {
            self = this;
        }

        public Image Background;
        public Image IncomeBtn;
        public Image ReportBtn;
        public Prefab.IncomePrefab income;
        public Prefab.ReportPrefab report;
        public Prefab.ExpendPrefab expend;
        public Prefab.ExplanationPrefab explanation;
        public Prefab.TotalPrefab totalPrefab;
        public BuildIncome buildIncome;
        public BuildEmployeeReport buildEmployeeReport;
        public Button OKBtn;

        public override void Build ()
        {
            //GameLoopManager.instance.pm.calender.AfterNextShiftDone += OnMorningChanged;
            isBuild = true;
            OKBtn.onClick.AddListener ( GenerateOK () );
        }

        public override void Destroy ()
        {
            base.Destroy ();
            //GameLoopManager.instance.pm.calender.AfterNextShiftDone -= OnMorningChanged;
        }

        private string path = "GameUI/Log/";
        public override void Init ()
        {
            income.UpdateData ( pm , gm );
            report.UpdateData ( pm );
            expend.UpdateData ( pm );
            explanation.UpdateData ( pm );
            totalPrefab.UpdateData ( pm );

            OKBtn.GetComponent<Image> ().sprite = SpritePath.OKBtn ();

            Background.sprite = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}{path}YesterdaySaleBackground" );
            IncomeBtn.sprite = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}{path}SaleDetailBtn" );
            ReportBtn.sprite = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}{path}DetailReportBtn" );
        }

        public void CheckIncome ()
        {
            buildIncome.Open ();
            PlayButtonAudio.self.PlayCheckLog ();
        }

        public void CheckEmployeeReport ()
        {
            buildEmployeeReport.Open ();
            PlayButtonAudio.self.PlayCheckLog ();
        }

        public System.Action OnClosed;
        public System.Action ShowEmployeeSchedlue;
        UnityAction GenerateOK ()
        {
            return () =>
            {
                PlayButtonAudio.self.PlayCommodityOK ();
                if ( GameLoopManager.WaitStoryLog == true )
                {
                    LoadingScene.self.Transitions ( 0.5f , 1f , false );
                    Invoke ( "OK" , 0.5f );
                }
                else
                    OK ();
            };
        }
        public void OK ()
        {
            GameLoopManager.WaitStoryLog = false;
            MainPanel.self.Close ();
            if ( ShowEmployeeSchedlue != null )
            {
                ShowEmployeeSchedlue ();
            }
            else if ( OnClosed != null )
            {
                OnClosed ();
            }
        }

        public void OnMorningChanged ( Calender c )
        {
            if ( c.GetShift () == Calender.Shift.morning )
            {
                Open ();
            }
        }

        public void Show ()
        {
            ShowEmployeeSchedlue -= Show;
            Invoke ( "EmployeeSchedlue" , 0.1f );
        }

        void EmployeeSchedlue ()
        {
            MainPanel.self.SwitchPanel ( 3 );
        }

        public IEnumerator Play ()
        {
            while ( ChangeDay.self.playAnim == true )
            {
                yield return null;
            }

            income.Init ();
            expend.Init ();
            report.Init ();
            explanation.Init ();

            StartCoroutine ( income.Play () );
            while ( income.finish == false )
            {
                yield return null;
            }

            StartCoroutine ( expend.Play () );
            while ( expend.finish == false )
            {
                yield return null;
            }

            StartCoroutine ( report.Play () );
            while ( report.finish == false )
            {
                yield return null;
            }

            StartCoroutine ( explanation.Play () );
            yield return null;
        }
    }
}
