﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class TotalPrefab : MonoBehaviour
    {
        public Text YesterdayMoneyText;
        public Text ResultText;
        public Text TodayMoneyText;

        public void UpdateData ( PlayerDataManager pm )
        {
            int day = pm.calender.GetDay ();

            string yesterdayMoney = pm.gameLog.GetTotalMoney ( day ).ToString ( "N0" ) + Localizer.Translate ( "元" );
            int income = pm.gameLog.GetTotalIncome ( day );
            int expenditure = pm.gameLog.GetTotalExpenditure ( day );
            string result = ( income - expenditure ).ToString ( "N0" ) + Localizer.Translate ( "元" );
            string todayMoney = pm.playerData.GetMoney ().ToString ( "N0" ) + Localizer.Translate ( "元" );

            YesterdayMoneyText.text = yesterdayMoney;
            ResultText.text = result;
            TodayMoneyText.text = todayMoney;
        }
    }
}