﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class BuildIncome : BaseBuildPanel
    {
        [SerializeField]
        private int ItemID = -1;

        public Image Background;
        public Image OKImg;
        public Transform TypeSpace;
        public Transform ItemSpace;
        public BuildSaleQuantity buildSaleQuantity;

        private float [] posX = new float [] { 20f , 215f , 410f , 605f , -130f };
        private float [] widths = new float [] { 190f , 190f , 190f , 190f , 100f };
        private float height = 65f;
        private float posY = 360f;
        private int itemQuantity = 10;
        private float itemDistance = 65f;

        private const string path = "GameUI/Log/Income/";

        Prefab.IncomeTypePrefab [] Type;
        Prefab.IncomeItemPrefab [] Item;
        Prefab.IncomeAdditionPrefab addition;

        public override void Build ()
        {
            Type = new Prefab.IncomeTypePrefab [posX.Length];
            Sprite [] typeSpr = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + path + "Type" );
            for ( int i = 0; i < posX.Length; i++ )
            {
                Prefab.IncomeTypePrefab type = Instantiate ( Resources.Load<Prefab.IncomeTypePrefab> ( "Prefabs/" + path + "Type" ) , TypeSpace );
                type.transform.localPosition = new Vector3 ( posX[i] , posY , 0 );
                type.Frame.GetComponent<RectTransform> ().sizeDelta = new Vector2 ( widths [i] , height );
                type.Rect.sizeDelta = new Vector2 ( widths [i] , height );
                type.TypeImage.sprite = typeSpr [i];
                type.Frame.sprite = typeSpr [i];
                type.Button.onClick.AddListener ( GenerateType ( i ) );
                Type [i] = type;
            }

            Item = new Prefab.IncomeItemPrefab [itemQuantity];
            Sprite [] itemSpr = Resources.LoadAll<Sprite> ( "Sprites/Common/" + path + "Item" );
            for ( int i = 0; i < itemQuantity; i++ )
            {
                Prefab.IncomeItemPrefab item = Instantiate ( Resources.Load<Prefab.IncomeItemPrefab> ( "Prefabs/" + path + "Item" ) , ItemSpace );
                item.transform.localPosition += new Vector3 ( 0 , -itemDistance * i , 0 );
                item.Button.onClick.AddListener ( GenerateItem ( i ) );
                Item [i] = item;
            }

            addition = Instantiate ( Resources.Load<Prefab.IncomeAdditionPrefab> ( "Prefabs/" + path + "Addition" ) , transform );
            isBuild = true;
        }

        UnityAction GenerateType ( int id )
        {
            return () =>
            {
                SwitchType ( id );
                PlayButtonAudio.self.PlayList ();
            };
        }
        void SwitchType ( int id )
        {
            for ( int i = 0; i < Item.Length; i++ )
                ClearSaleQuantity ( i );

            for ( int i = 0; i < Type.Length; i++ )
            {
                Type [i].Frame.gameObject.SetActive ( id == i );
            }

            List<int> soldID = GetSoldID ( id );

            for ( int i = 0; i < soldID.Count; i++ )
            {
                int commodityID = soldID [i];
                Item [i].UpdateData ( commodityID , gm , pm );
            }
            for ( int i = soldID.Count; i < Item.Length; i++ )
            {
                Item [i].ClearData ();
            }
        }
        List<int> GetSoldID ( int type )
        {
            int day = pm.calender.GetDay ();
            List<int> soldID = new List<int> ();
            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )
            {
                int sold = 0;
                for ( int s = 0; s < 3; s++ )
                {
                    sold += pm.gameLog.GetCommoditySaleQuantity ( day * 3 + s , i );
                }
                if ( type == 4 )
                {
                    if ( sold > 0 )
                    {
                        soldID.Add ( i );
                        continue;
                    }
                }
                if ( sold > 0 && (int)gm.commodityList.Data [i].type == type )
                    soldID.Add ( i );
            }
            return soldID;
        }

        UnityAction GenerateItem ( int itemID )
        {
            return () =>
            {
                PlayButtonAudio.self.PlayList ();
                SwitchItem ( itemID );
            };
        }
        void SwitchItem ( int itemID )
        {
            int id = Item [itemID].commodityID;
            if ( ItemID == itemID )
            {
                ClearSaleQuantity ( itemID );
                return;
            }
            for ( int i = 0; i < Item.Length; i++ )
            {
                Item [i].Frame.SetActive ( i == itemID );
            }
            buildSaleQuantity.GetID ( id );
            buildSaleQuantity.Open ();
            ItemID = itemID;
        }
        void ClearSaleQuantity ( int itemID )
        {
            buildSaleQuantity.Close ();
            Item [itemID].Frame.SetActive ( false );
            ItemID = -1;
        }

        public void OKBtn ()
        {
            PlayButtonAudio.self.PlayList ();
            Close ();
        }

        public override void Init ()
        {
            SwitchType ( 4 );
            addition.UpdateData ( pm , gm );

            Sprite [] typeSpr = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + path + "Type" );
            for ( int i = 0; i < Type.Length; i++ )
            {
                Type [i].TypeImage.sprite = typeSpr [i];
            }
            Background.sprite = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameUI/Log/Income/Background" );
            OKImg.sprite = SpritePath.OKBtn4 () [0];
            buildSaleQuantity.GetComponent<Image>().sprite = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameUI/Log/Income/Shift" );
        }
    }
}