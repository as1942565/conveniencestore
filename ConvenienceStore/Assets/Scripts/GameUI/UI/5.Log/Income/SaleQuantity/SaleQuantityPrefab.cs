﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class SaleQuantityPrefab : MonoBehaviour
    {
        public Text QuantityText;

        public void UpdateData ( int id , int shift , GameLog log )
        {
            int quantity = log.GetCommoditySaleQuantity ( shift , id );
            QuantityText.text = quantity.ToString ();
        }
    }
}