﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class BuildSaleQuantity : BaseBuildPanel
    {
        private float [] posX = new float [] { -95f , 1f , 95f };
        private float posY = -45f;

        private const string path = "GameUI/Log/Income/SaleQuantity/";

        Prefab.SaleQuantityPrefab [] SaleQuantity;

        public override void Build ()
        {
            SaleQuantity = new Prefab.SaleQuantityPrefab [posX.Length];
            for ( int i = 0; i < posX.Length; i++ )
            {
                Prefab.SaleQuantityPrefab sale = Instantiate ( Resources.Load<Prefab.SaleQuantityPrefab> ( "Prefabs/" + path + "SaleQuantity" ) , transform );
                sale.transform.localPosition = new Vector3 ( posX[i] , posY , 0 );
                SaleQuantity [i] = sale;
            }
            isBuild = true;
        }

        private int commodityID;
        public void GetID ( int commodityID )
        {
            this.commodityID = commodityID;
        }

        public override void Init ()
        {
            int day = pm.calender.GetDay ();
            for ( int i = 0; i < SaleQuantity.Length; i++ )
            {
                SaleQuantity [i].UpdateData ( commodityID , day * 3 + i , pm.gameLog );
            }
        }
    }
}