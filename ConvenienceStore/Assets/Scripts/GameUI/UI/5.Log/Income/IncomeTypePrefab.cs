﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{   
    public class IncomeTypePrefab : MonoBehaviour
    {
        public RectTransform Rect;
        public Image TypeImage;
        public Image Frame;
        public Button Button;
    }
}