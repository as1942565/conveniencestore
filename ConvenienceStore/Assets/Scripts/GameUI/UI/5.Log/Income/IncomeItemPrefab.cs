﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class IncomeItemPrefab : MonoBehaviour
    {
        public int commodityID;
        public Image Icon;
        public Text NameText;
        public Text PriceText;
        public GameObject Frame;
        public Text SaleQuantityText;
        public Text TotalText;
        public Button Button;

        public void UpdateData ( int id , GameDataManager gm , PlayerDataManager pm )
        {
            commodityID = id;
            if ( id == -1 )
            {
                ClearData ();
                return;
            }
            int yesterday = pm.calender.GetYesterday ();
            GameData.Commodity commodity = gm.commodityList.Data [id];
            Icon.sprite = SpritePath.Commodity [id];
            //NameText.text = commodity.name;
            NameText.text = Localizer.Translate ( commodity.name );

            CommodityItem.Data data = pm.commodityItem.GetData ( id );
            PriceText.text = data.sale.ToString ( "N0" ) + Localizer.Translate ( "元" );
            int saleQuantity = 0;
            int total = 0;
            for ( int i = 0; i < 3; i++ )
            {
                saleQuantity += pm.gameLog.GetCommoditySaleQuantity ( yesterday * 3 + i , id );
                total += pm.gameLog.GetCommodityIncome ( yesterday * 3 + i , id );
            }
            SaleQuantityText.text = saleQuantity.ToString ( "N0" ) + "個";
            TotalText.text = total.ToString ( "N0" ) + Localizer.Translate ( "元" );
            Button.enabled = true;
        }
        public void ClearData ()
        {
            Icon.sprite = SpritePath.None;
            NameText.text = "";
            PriceText.text = "";
            SaleQuantityText.text = "";
            TotalText.text = "";
            Button.enabled = false;
        }
    }
}