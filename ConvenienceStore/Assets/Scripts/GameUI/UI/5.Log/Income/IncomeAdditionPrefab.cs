﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class IncomeAdditionPrefab : MonoBehaviour
    {
        public Text popularText;
        public Text popularAdditionText;
        public Text [] shiftAdditionText;
        public Text incomeAdditionText;
        public Text hightestIncomeText;
        public Text hightestIncomeMoney;
        public Image hightIncomeIcon;
        public Text hightestSaleText;
        public Text hightestSaleQuantity;
        public Image hightSaleIcon;

        public void UpdateData ( PlayerDataManager pm , GameDataManager gm )
        {
            int day = pm.calender.GetYesterday ();
            Popular ( pm , gm );
            Shift ( pm , day );
            Income ( pm );
            HightestIncome ( gm , pm , day );
            HightestSale ( gm , pm , day );
        }
        void Popular ( PlayerDataManager pm , GameDataManager gm )
        {
            int popular = pm.playerData.GetPopular ();
            float popularAddition = Formula.Popular.StoreAddition ( pm );
            popularText.text = popular.ToString ( "N0" );
            popularAdditionText.text = popularAddition * 100 + "%";
        }
        void Shift ( PlayerDataManager pm , int day )
        {
            float [] shiftAddition = new float [shiftAdditionText.Length];
            for ( int i = 0; i < shiftAddition.Length; i++ )
            {
                shiftAddition[i] = Formula.CommodityIncome.ScheduleAdditionWithShift ( pm , i );
                shiftAdditionText [i].text = shiftAddition [i] * 100 + "%";
            }
        }
        void Income ( PlayerDataManager pm )
        {
            float incomeAddition = Formula.CommodityIncome.StoreAddition ( pm );
            incomeAdditionText.text = incomeAddition * 100 + "%";
        }
        void HightestIncome ( GameDataManager gm , PlayerDataManager pm , int day )
        {
            int commodityID = pm.gameLog.GetMVPIncome ( day );
            if ( commodityID == -1 )
            {
                hightestIncomeText.text = "";
                hightestIncomeMoney.text = "";
                hightIncomeIcon.sprite = SpritePath.None;
                return;
            }
            //string commodityName = gm.commodityList.Data [commodityID].name;
            string commodityName = Localizer.Translate ( gm.commodityList.Data [commodityID].name );
            string commodityInomce = pm.gameLog.GetMVPIncomeMoney ( day ).ToString ( "N0" );
            hightestIncomeText.text = commodityName;
            hightestIncomeMoney.text = commodityInomce + Localizer.Translate ( "元" );
            hightIncomeIcon.sprite = SpritePath.Commodity [commodityID];
        }
        void HightestSale ( GameDataManager gm , PlayerDataManager pm , int day )
        {
            int commodityID = pm.gameLog.GetMVPPopular ( day );
            if ( commodityID == -1 )
            {
                hightestSaleText.text = "";
                hightestSaleQuantity.text = "";
                hightSaleIcon.sprite = SpritePath.None;
                return;
            }
            //string commodityName = gm.commodityList.Data [commodityID].name;
            string commodityName = Localizer.Translate ( gm.commodityList.Data [commodityID].name );
            string commodityQuantity = pm.gameLog.GetMVPPopularQuantity ( day ).ToString ( "N0" );
            hightestSaleText.text = commodityName;
            hightestSaleQuantity.text = commodityQuantity + "個";
            hightSaleIcon.sprite = SpritePath.Commodity [commodityID];
        }
    }
}