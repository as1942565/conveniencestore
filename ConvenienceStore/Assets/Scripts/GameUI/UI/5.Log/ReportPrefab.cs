﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class ReportPrefab : MonoBehaviour
    {
        [HideInInspector]
        public bool finish;

        public Text ReportText;
        private string report;
        public Text RunningText;
        private string running;
        public Text SuccessedText;
        private string successed;
        public Text FailedText;
        private string failed;
        public TextScrollView textScrollView;

        public void UpdateData ( PlayerDataManager pm )
        {
            int day = pm.calender.GetDay ();

            report = pm.gameLog.GetReport ( day );
            running = pm.activityScheduleLog.GetRun ( day ).ToString ();
            successed = pm.activityScheduleLog.GetSuccessed ( day ).ToString ();
            failed = pm.activityScheduleLog.GetFail ( day ).ToString ();

            ReportText.text = report;
            RunningText.text = running;
            SuccessedText.text = successed;
            FailedText.text = failed;

            Canvas.ForceUpdateCanvases();

            float height = ReportText.rectTransform.rect.height;
            RectTransform rect = textScrollView.GetComponent<RectTransform> ();
            textScrollView.ChangeWindowHeight ( rect , ReportText , height );
            textScrollView.ChangeScrollSensitivity ( ReportText , height );
        }

        public void Init ()
        {
            finish = false;
            ReportText.text = "";
            RunningText.text = "";
            SuccessedText.text = "";
            FailedText.text = "";
        }

        public IEnumerator Play ()
        {
            ReportText.text = report;
            yield return new WaitForSeconds ( 0.5f );
            ReportText.text = report;
            yield return new WaitForSeconds ( 0.5f );
            RunningText.text = running;
            yield return new WaitForSeconds ( 0.5f );
            SuccessedText.text = successed;
            yield return new WaitForSeconds ( 0.5f );
            FailedText.text = failed;
            yield return new WaitForSeconds ( 0.3f );
            finish = true;
        }
    }
}
