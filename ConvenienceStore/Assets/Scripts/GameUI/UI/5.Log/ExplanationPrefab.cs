﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class ExplanationPrefab : MonoBehaviour
    {
        public Text WxplanationText;
        private string planation;
        public TextScrollView textScrollView;

        public void UpdateData ( PlayerDataManager pm )
        {
            int day = pm.calender.GetDay ();
            planation = pm.gameLog.GetInformation ( day );
            WxplanationText.text = planation;

            Canvas.ForceUpdateCanvases();

            float height = WxplanationText.rectTransform.rect.height;
            RectTransform rect = textScrollView.GetComponent<RectTransform> ();
            textScrollView.ChangeWindowHeight ( rect , WxplanationText , height );
            textScrollView.ChangeScrollSensitivity ( WxplanationText , height );
        }

        public void Init ()
        {
            WxplanationText.text = "";
        }

        public IEnumerator Play ()
        {
            WxplanationText.text = planation;
            yield return null;
        }
    }
}
