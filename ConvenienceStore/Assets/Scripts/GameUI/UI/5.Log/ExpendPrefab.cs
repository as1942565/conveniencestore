﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class ExpendPrefab : MonoBehaviour
    {
        [HideInInspector]
        public bool finish;

        public Text TotalText;
        private int total;
        public Text RestockText;
        private int restock;
        public Text ActivityText;
        private int activity;
        public Text ExpandAndBusiness;
        private int EAB;
        public Text OtherText;
        private int other;

        public void UpdateData ( PlayerDataManager pm )
        {
            int day = pm.calender.GetDay ();
            if ( day < 0 )
            {
                ClearData ();
                return;
            }
            restock = pm.gameLog.GetRestockWithDay ( day );
            activity = pm.gameLog.GetActivityWithDay ( day );
            int expand = pm.gameLog.GetExpandWithDay ( day );
            int business = pm.gameLog.GetBusinessWithDay ( day );
            EAB = expand + business;
            other = pm.gameLog.GetOtherWithDay ( day );
            total = restock + activity + EAB + other;
            pm.gameLog.SetTotalExpenditure ( day , total );

            TotalText.text = total.ToString ( "N0" ) + Localizer.Translate ( "元" );
            RestockText.text = restock.ToString ( "N0" ) + Localizer.Translate ( "元" );
            ActivityText.text = activity.ToString ( "N0" ) + Localizer.Translate ( "元" );
            ExpandAndBusiness.text = EAB.ToString ( "N0" ) + Localizer.Translate ( "元" );
            OtherText.text = other.ToString ( "N0" ) + Localizer.Translate ( "元" );
        }
        public void ClearData ()
        {
            TotalText.text = "0" + Localizer.Translate ( "元" );
            RestockText.text = "0" + Localizer.Translate ( "元" );
            ActivityText.text = "0" + Localizer.Translate ( "元" );
            ExpandAndBusiness.text = "0" + Localizer.Translate ( "元" );
            OtherText.text = "0" + Localizer.Translate ( "元" );
        }

        public void Init ()
        {
            finish = false;
            TotalText.text = "";
            RestockText.text = "";
            ActivityText.text = "";
            ExpandAndBusiness.text = "";
            OtherText.text = "";
        }

        private int part = 20;

        public IEnumerator Play ()
        {
            int n = 0;
            int run = 0;

            float tempTotal = total / part;
            if ( tempTotal > 0 )
            {
                while ( n < part )
                {
                    run += Mathf.FloorToInt ( tempTotal );
                    TotalText.text = run.ToString ( "N0" ) + Localizer.Translate ( "元" );
                    yield return new WaitForSeconds ( 1 / part );
                    n++;
                }
            }

            TotalText.text = total.ToString ( "N0" ) + Localizer.Translate ( "元" );
            yield return new WaitForSeconds ( 0.3f );

            n = 0;
            run = 0;
                
            float tempRestock = restock / part;
            if ( tempRestock > 0 )
            {
                while ( n < part )
                {
                    run += Mathf.FloorToInt ( tempRestock );
                    RestockText.text = run.ToString ( "N0" ) + Localizer.Translate ( "元" );
                    yield return new WaitForSeconds ( 1 / part );
                    n++;
                }
            }

            RestockText.text = restock.ToString ( "N0" ) + Localizer.Translate ( "元" );
            yield return new WaitForSeconds ( 0.3f );

            n = 0;
            run = 0;

            float tempActivity = activity / part;
            if ( tempActivity > 0 )
            {
                while ( n < part )
                {
                    run += Mathf.FloorToInt ( tempActivity );
                    ActivityText.text = run.ToString ( "N0" ) + Localizer.Translate ( "元" );
                    yield return new WaitForSeconds ( 1 / part );
                    n++;
                }
            }

            ActivityText.text = activity.ToString ( "N0" ) + Localizer.Translate ( "元" );
            yield return new WaitForSeconds ( 0.3f );

            n = 0;
            run = 0;

            float tempEAB = EAB / part;
            if ( tempEAB > 0 )
            {
                while ( n < part )
                {
                    run += Mathf.FloorToInt ( tempEAB );
                    ExpandAndBusiness.text = run.ToString ( "N0" ) + Localizer.Translate ( "元" );
                    yield return new WaitForSeconds ( 1 / part );
                    n++;
                }
            }

            ExpandAndBusiness.text = EAB.ToString ( "N0" ) + Localizer.Translate ( "元" );
            yield return new WaitForSeconds ( 0.3f );

            n = 0;
            run = 0;

            float tempOther = other / part;
            if ( tempOther > 0 )
            {
                while ( n < part )
                {
                    run += Mathf.FloorToInt ( tempOther );
                    OtherText.text = run.ToString ( "N0" ) + Localizer.Translate ( "元" );
                    yield return new WaitForSeconds ( 1 / part );
                    n++;
                }
            }

            OtherText.text = other.ToString ( "N0" ) + Localizer.Translate ( "元" );
            yield return new WaitForSeconds ( 0.3f );
            finish = true;
        }
    }
}
