﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class BuildMainPanel : BaseBuildPanel
    {
        public static BuildMainPanel self;
        private void Awake ()
        {
            self = this;
        }

        public Image BaseUI;
        public Button [] panels;

        private const string path = "GameUI/Background/MainButton/";

        public override void Init ()
        {
            OptionData optionData = GameLoopManager.instance.om.optionData;

            Sprite [] panelSprite = Resources.LoadAll<Sprite> ( "Sprites/" + optionData.GetLanguageStr () + path  );
            BaseUI.sprite = Resources.Load<Sprite> ( $"Sprites/{optionData.GetLanguageStr ()}GameUI/Background/BaseUI" );
            if ( panels [0] == null )
                return;
            for ( int i = 0; i < panels.Length; i++ )
            {
                panels [i].image.sprite = panelSprite [i * 3];
                SpriteState ss = new SpriteState ();
                ss.highlightedSprite = panelSprite [i * 3 + 1];
                ss.pressedSprite = panelSprite [i * 3 + 2];
                panels [i].spriteState = ss;

            }
        }

        public override void Build ()
        {
        }
    }
}