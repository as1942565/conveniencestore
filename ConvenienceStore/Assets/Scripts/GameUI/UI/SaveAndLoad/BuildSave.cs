﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System.IO;

namespace UI
{
    public class BuildSave : BaseBuildPanel
    {
        public GameObject Background;
        public Transform PageSpace;
        public Transform ItemSpace;
        public Image Screenshot;

        public Image BackBtnImage;
        public Image DeleteBtnImage;
        public Image SaveBtnImage;
        public Image LoadBtnImage;

        private int pageLength = 9;
        private float pageDistance = 90f;
        private int itemLength = 6;
        private float itemDistance = 140f;

        private const string path = "GameUI/SaveAndLoad/";

        Prefab.SavePagePrefab [] savePages;
        Prefab.SaveItemPrefab [] saveItems;
        Prefab.SaveItemPrefab autoItem;
        
        Sprite [] itemSpr;
        
        private int tempItemID = -1;
        private int page;

        public override void Open ()
        {
            base.Open ();
        }

        public override void Build () 
        {
            savePages = new Prefab.SavePagePrefab [pageLength];
            for ( int i = 0; i < savePages.Length; i++ )
            {
                Prefab.SavePagePrefab page = Instantiate ( Resources.Load<Prefab.SavePagePrefab> ( "Prefabs/" + path + "Page" ) , PageSpace );
                page.transform.localPosition += new Vector3 ( pageDistance * i , 0 , 0 );
                page.PageImg.sprite = SpritePath.Page [i];
                page.Frame.sprite = SpritePath.TargetPage [i];
                page.Button.onClick.AddListener ( GeneratePage ( i ) );
                page.name = "Page" + i.ToString ();
                savePages [i] = page;
            }
            isBuild = true;

            autoItem = Instantiate ( Resources.Load<Prefab.SaveItemPrefab> ( "Prefabs/" + path + "Item" ) , ItemSpace );
            autoItem.name = "AutoItem";
            autoItem.transform.localPosition = new Vector3 ( 725 , 460 , 0 );
            autoItem.Button.onClick.RemoveAllListeners ();
            if ( gameObject.name == "Load" )
                autoItem.Button.onClick.AddListener ( AutoItem );

            saveItems = new Prefab.SaveItemPrefab [itemLength];
            for ( int i = 0; i < saveItems.Length; i++ )
            {
                Prefab.SaveItemPrefab item = Instantiate ( Resources.Load<Prefab.SaveItemPrefab> ( "Prefabs/" + path + "Item" ) , ItemSpace );
                item.transform.localPosition += new Vector3 ( 0 , -itemDistance * i , 0 );
                item.Button.onClick.AddListener ( GenerateItem ( i ) );
                saveItems [i] = item;
            }
        }

        UnityAction GeneratePage ( int page )
        {
            return () =>
            {
                SwitchPage ( page );
                PlayButtonAudio.self.PlaySaveLoad ();
            };
        }
        void SwitchPage ( int page )
        {
            this.page = page;
            tempItemID = -1;
            SwitchItem ( tempItemID );

            for ( int i = 0; i < savePages.Length; i++ )
            {
                savePages [i].Frame.gameObject.SetActive ( i == page );
            }
            for ( int i = 0; i < saveItems.Length; i++ )
            {
                saveItems [i].Frame.SetActive ( false );
            }
            ShowItem ( page );

        }
        void ShowItem ( int page )
        {
            for ( int i = 0; i < saveItems.Length; i++ )
            {
                saveItems [i].UpdateData ( new SaveData () , ( page * 6 + i ).ToString () );
            }
        }

        UnityAction GenerateItem ( int id )
        {
            return () =>
            {
                SwitchItem ( id );
                PlayButtonAudio.self.PlaySaveLoad ();
            };
        }
        void SwitchItem ( int id )
        {
            if ( id == -1 )
            {
                Screenshot.sprite = SpritePath.None;
                return;
            }
            if ( tempItemID == id && id != -1 )
            {
                saveItems [id].Frame.SetActive ( false );
                tempItemID = -1;
                Screenshot.sprite = SpritePath.None;
                return;
            }
            int index = page * itemLength + id;
            StartCoroutine ( ScreenShot.self.FindScreenShot ( Screenshot , index.ToString () ) );
            for ( int i = 0; i < saveItems.Length; i++ )
            {
                saveItems [i].Frame.SetActive ( id == i );
                
            }
            tempItemID = id;
            autoItem.Frame.SetActive ( false );
        }

        void AutoItem ()
        {
            if ( tempItemID != -2 )
            {
                tempItemID = -2;
                autoItem.Frame.SetActive ( true );
                for ( int i = 0; i < saveItems.Length; i++ )
                {
                    saveItems [i].Frame.SetActive ( false );
                }
                StartCoroutine ( ScreenShot.self.FindScreenShot ( Screenshot , "Auto" ) );
            }
            else
            {
                tempItemID = -1;
                autoItem.Frame.SetActive ( false );
                Screenshot.sprite = SpritePath.None;
            }
        }

        public override void Init ()
        {
            tempItemID = -1;
            SwitchItem ( tempItemID );
            Background.SetActive ( true );
            SwitchPage ( GameLoopManager.SaveAndLoadPage );
            Screenshot.sprite = SpritePath.None;

            autoItem.UpdateAutoData ( new SaveData () );

            Sprite [] loadSprite = Resources.LoadAll<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameUI/SaveAndLoad/LoadBtn" );
            Sprite [] saveSprite = Resources.LoadAll<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameUI/SaveAndLoad/SaveBtn" );

            BackBtnImage.sprite = loadSprite [0];
            DeleteBtnImage.sprite = loadSprite [1];
            if ( SaveBtnImage != null )
                SaveBtnImage.sprite = saveSprite [2];
            if ( LoadBtnImage != null )
                LoadBtnImage.sprite = loadSprite [2];
        }

        public void BackBtn ()
        {
            Background.SetActive ( false );
            PlayButtonAudio.self.PlaySaveLoad ();
        }
        public void DeleteBtn ()
        {
            if ( tempItemID == -1 )
                return;
            if ( tempItemID == -2 )
            {
                MessageBox.Warning ( "自動存檔不能刪除" );
                return;
            }
            PlayButtonAudio.self.PlaySaveLoad ();

            string id = tempItemID.ToString ();
            if ( id == "-2" )
            {
                id = "Auto";
            }
            else
            {
                id = ( tempItemID + page * 6 ).ToString ();
            }
            string subFolderPath2 = SaveLoadManager.GetPath ();
            File.Delete ( Path.Combine ( subFolderPath2 , $"SaveFile_{id}.xml" ) );
            File.Delete ( Path.Combine ( subFolderPath2 , $"SaveData_{id}.xml" ) );
            File.Delete ( Path.Combine ( subFolderPath2 , $"ScreenShot{id}.png" ) );
            Screenshot.sprite = SpritePath.None;
            if ( id == "Auto" )
            {
                autoItem.UpdateData ( new SaveData () , id );
            }
            else
                saveItems [tempItemID].UpdateData ( new SaveData () , id );
            
        }
        public void SaveBtn ()
        {
            if ( tempItemID == -1 )
            {
                MessageBox.Warning ( "請選擇檔案" );
                return;
            }
            PlayButtonAudio.self.PlaySaveLoad ();

            int index = page * itemLength + tempItemID;
            SaveLoadManager.Save ( pm , index.ToString () );
            SaveData ( index );
            GameLoopManager.SaveAndLoadPage = page;
        }

        void SaveData ( int index )
        {
            SaveData saveData = new SaveData ();
            saveData.SetData ( pm.calender.GetDay () , pm.playerData.GetMoney () , (int)pm.calender.GetShift () );

            SaveLoadManager.SaveDataSave ( saveData , index.ToString () );
            ScreenShot.self.SaveScreenShot ( Screenshot , index.ToString () );

            saveItems [tempItemID].UpdateData ( saveData , index.ToString () );
        }
        public void LoadBtn ()
        {
            if ( tempItemID == -1 )
            {
                MessageBox.Warning ( "請選擇檔案" );
                return;
            }
            bool res;
            if ( tempItemID == -2 )
                res = SaveLoadManager.Load ( out pm , "Auto" );
            else
            {
                int index = page * itemLength + tempItemID;
                res = SaveLoadManager.Load ( out pm , index.ToString () );
            }
            if ( res )
            {
                GameLoopManager.instance.pm = pm;
                GameLoopManager.instance.OnLoaded ( GameLoopManager.instance.pm );
                BGMManager.self.PlayShiftBGM ();
                Scene scene = SceneManager.GetActiveScene();
                GameLoopManager.instance.Load = false;
                if ( scene.name != "Demo" )
                {
                    LoadingScene.self.Transitions ( 0.5f , 1f , true );
                    Invoke ( "GoToScene" , 0.5f );
                }
                else
                {
                    GoToScene ();
                }
                BackBtn ();
                return;
            }
            MessageBox.Warning ( "此記錄檔為空白" );
        }
        void GoToScene ()
        {
            GameLoopManager.SaveAndLoadPage = page;
            SceneManager.LoadScene ( "Demo" );
        }
    }
}