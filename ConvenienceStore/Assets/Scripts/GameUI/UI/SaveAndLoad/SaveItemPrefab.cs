﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class SaveItemPrefab : MonoBehaviour
    {
        public Image TypeImg;
        public Text DayText;
        public Text MoneyText;
        public GameObject Data;
        public Button Button;
        public GameObject Frame;
        public GameObject Empty;

        private const string path = "Sprites/Common/GameUI/Background/Shift/ShiftImage";
        private Vector2 [] pos = new Vector2 [] { new Vector2 ( 30f , 22f ) , new Vector2 ( 97f , 28f ) , new Vector2 ( 157f , 24f ) };

        Sprite [] shiftIconSpr;

        void GetSprite ()
        {
            shiftIconSpr = Resources.LoadAll<Sprite> ( path );
            if ( shiftIconSpr == null )
            {
                Debug.LogError ( path + "路徑不正確" );
            }
        }

        public void UpdateData ( SaveData saveData , string id )
        {
            Data.SetActive ( true );
            bool res = SaveLoadManager.SaveDataLoad ( out saveData , id );
            if ( res )
            {
                SetShiftIcon ( (int)saveData.GetData ().shift );
                SetDay ( saveData.GetData ().day );
                SetMoney ( saveData.GetData ().money );
                Empty.SetActive ( false );
                return;
            }
            NoData ();
        }
        public void UpdateAutoData ( SaveData saveData )
        {
            Data.SetActive ( true );
            bool res = SaveLoadManager.SaveDataLoad ( out saveData , "Auto" );
            if ( res )
            {
                SetShiftIcon ( (int)saveData.GetData ().shift );
                SetDay ( saveData.GetData ().day );
                SetMoney ( saveData.GetData ().money );
                Empty.SetActive ( false );
                return;
            }
            NoData ();
        }
        void SetShiftIcon ( int shift )
        {
            if ( shiftIconSpr == null )
                GetSprite ();
            TypeImg.sprite = shiftIconSpr [shift];
            TypeImg.transform.localPosition = pos [shift];
        }
        void SetDay ( int day )
        {
            DayText.text = day.ToString ();
        }
        void SetMoney ( int money )
        {
            MoneyText.text = money.ToString ( "N0" );
        }

        public void NoData ()
        {
            Data.SetActive ( false );
            Empty.SetActive ( true );
        }
    }
}