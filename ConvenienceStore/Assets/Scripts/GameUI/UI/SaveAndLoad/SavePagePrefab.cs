﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class SavePagePrefab : MonoBehaviour
    {
        public Image PageImg;
        public Image Frame;
        public Button Button;
    }
}