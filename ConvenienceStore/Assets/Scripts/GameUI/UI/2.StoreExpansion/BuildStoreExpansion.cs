﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;

namespace UI
{
    public struct StoreExpansionItem
    {
        public float height;
        public int id;
        public string name;
        public int cost;
        public bool complete;
        public bool nextLevel;
        public int level;
        public Sprite [] typeIcon;
    }

    public class BuildStoreExpansion : BaseBuildPanel
    {
        public static BuildStoreExpansion self;
        private void Awake ()
        {
            self = this;
        }

        public Image Background;
        public SG.InitOnStart initOnStart;
        public Scrollbar scrollbar;
        public Text CostText;
        public Text MoneyText;
        public Text AdditionText;
        public Text Expanation;
        public Image IconImage;
        public Button OKBtn;
        public Button BackBtn;
        public Button SkipBtn;
        public Transform SkipBar;

        [SerializeField]
        private GameData.StoreExpansion.ExpansionType type = GameData.StoreExpansion.ExpansionType.income;
        [SerializeField]
        private List<int> storeIDs = new List<int> ();
        [SerializeField]
        private bool [] TargetBool;
        
        private float distance_I = 79f;
        private int typeLength = 5;
        private float distance_T = 170f;
        private bool skipBool = true;
        private int totalCost;

        public const string StoreExpansionPath = "GameUI/StoreExpansion/";

        List<StoreExpansionItem> Data;
        Prefab.StoreExpansionTypePrefab [] Type;
        Sprite [] typeSpr;
        Sprite [] iconSpr;

        public override void Init ()
        {
            ClearTarget ();
            SwitchType ( GameData.StoreExpansion.ExpansionType.income );
            totalCost = 0;
            CostText.text = "";
            storeIDs.Clear ();
            CostText.gameObject.SetActive ( false );
            initOnStart.GetComponent<LoopVerticalScrollRect>().enabled = !StoryManager.self.EnableTeach;

            Sprite [] typeImage = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + StoreExpansionPath + "Type" );
            Sprite [] targetTypeImage = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + StoreExpansionPath + "TargetType" );
            for ( int i = 0; i < typeLength; i++ )
            {
                Type [i].TypeImage.sprite = typeImage [i];
                Type [i].FrameImage.sprite = targetTypeImage [i];
            }

            OKBtn.GetComponent<Image> ().sprite = SpritePath.OKBtn ();
            BackBtn.GetComponent<Image> ().sprite = SpritePath.BackBtn ();
            Background.sprite = Resources.Load<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + StoreExpansionPath + "StoreExpansionBackground" );
            SkipBtn.GetComponent<Image> ().sprite = Resources.Load<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + StoreExpansionPath + "Skip" );
        }

        void ClearTarget ()
        {
            for ( int i = 0; i < TargetBool.Length; i++ )
            {
                TargetBool [i] = false;
            }
        }

        void CreateItem ( int [] ids )
        {
            Data.Clear ();

            for ( int i = 0; i < ids.Length; i++ )
            {
                int id = ids [i];
                StoreExpansionItem item = CreateData ( id );
                Data.Add ( item );
            }
            initOnStart.UpdateCount ( Data.Count );
        }

        StoreExpansionItem CreateData ( int id )
        {
            StoreExpansionItem data = new StoreExpansionItem ();
            data.height = distance_I;
            data.id = id;
            data.name = gm.storeExpansionList.Data [id].name;
            data.cost = gm.storeExpansionList.Data [id].cost;
            data.complete = pm.storeExpansionUnlock.GetComplete ( id );
            int level = gm.storeExpansionList.Data [id].storeLevel;
            data.nextLevel = level == pm.businessUnlock.GetLevel () + 1;
            data.typeIcon = new Sprite [2];
            for ( int i = 0; i < 2; i++ )
            {
                if ( i >= gm.storeExpansionList.Data [id].type.Count )
                {
                    data.typeIcon [i] = SpritePath.None;
                    continue;
                }
                int type = (int)gm.storeExpansionList.Data [id].type [i];
                data.typeIcon [i] = typeSpr [i];
            }
            data.level = level;

            return data;
        }

        public override void Build ()
        {
            TargetBool = new bool [gm.storeExpansionList.Data.Length];
            Data = new List<StoreExpansionItem> ();
            Type = new Prefab.StoreExpansionTypePrefab [typeLength];
            
            typeSpr = Resources.LoadAll<Sprite> ( "Sprites/Common/" + StoreExpansionPath + "TypeIcon" );
            iconSpr = Resources.LoadAll<Sprite> ( "Sprites/Common/" + StoreExpansionPath + "Icon" );
            
            Sprite [] typeImage = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + StoreExpansionPath + "Type" );
            Sprite [] targetTypeImage = Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + StoreExpansionPath + "TargetType" );
            for ( int i = 0; i < typeLength; i++ )
            {
                Prefab.StoreExpansionTypePrefab type = Instantiate ( Resources.Load<Prefab.StoreExpansionTypePrefab> ( "Prefabs/" + StoreExpansionPath + "Type" ) , transform );
                type.name = "Type" + i.ToString ();
                type.transform.localPosition += new Vector3 ( distance_T * i , 0 , 0 );
                type.Button.onClick.AddListener ( GenerateType ( i ) );
                type.TypeImage.sprite = typeImage [i];
                type.FrameImage.sprite = targetTypeImage [i];

                Type [i] = type;
            }
            OKBtn.onClick.AddListener ( OK );
            BackBtn.onClick.AddListener ( GenerateBack () );
            SkipBtn.onClick.AddListener ( GenerateSkipMode () );
            isBuild = true;
        }
        UnityAction GenerateType ( int id )
        {
            return () =>
            {
                SwitchType ( (GameData.StoreExpansion.ExpansionType)id );
                PlayButtonAudio.self.PlayExpansionSwitch ();
            };
        }
        public void SwitchType ( GameData.StoreExpansion.ExpansionType type )
        {
            scrollbar.value = 1;
            SwitchItem ( -1 );
            for ( int t = 0; t < Type.Length; t++ )
            {
                Type [t].FrameImage.gameObject.SetActive ( t == (int)type );
            }
            this.type = type;
            ShowItem ( type );
        }
        void ShowItem ( GameData.StoreExpansion.ExpansionType type )
        {
            List<int> unlockID = new List<int> ();
            List<int> nextID = new List<int> ();
            List<int> completeID = new List<int> ();
            for ( int i = 0; i < gm.storeExpansionList.Data.Length; i++ )
            {
                if ( !pm.storeExpansionUnlock.GetUnock ( i ) )
                    continue;

                int storeLevel = pm.businessUnlock.GetLevel ();
                bool conform = false;

                for ( int j = 0; j < gm.storeExpansionList.Data [i].type.Count; j++ )
                {
                    if ( gm.storeExpansionList.Data [i].type [j] == type )
                    {
                        conform = true;
                        break;
                    }
                }
                if ( conform )
                {
                    if ( pm.storeExpansionUnlock.GetComplete ( i ) )
                    {
                        completeID.Add ( i );
                        continue;
                    }
                    if ( storeLevel + 1 == gm.storeExpansionList.Data [i].storeLevel )
                    {
                        nextID.Add ( i );
                        continue;
                    }
                    if ( storeLevel >= gm.storeExpansionList.Data [i].storeLevel )
                        unlockID.Add ( i );
                }
            }

            for ( int i = 0; i < nextID.Count; i++ )
            {
                unlockID.Add ( nextID [i] );
            }

            for ( int i = 0; i < completeID.Count; i++ )
            {
                unlockID.Add ( completeID [i] );
            }

            CreateItem ( unlockID.ToArray () );
        }
        
        public void SwitchItem ( int storeID )
        {
            string addition = "";
            string expanation = "";

            string money = pm.playerData.GetMoney ().ToString ( "N0" ) + Localizer.Translate ( "元" );
            UpdateText ( totalCost , money , "" , "" , SpritePath.None );
            
            if ( storeID >= 0 )
            {
                int cost = gm.storeExpansionList.Data [storeID].cost;
                if ( skipBool == true )
                {
                    if ( TargetBool [storeID] )
                    {
                        storeIDs.Remove ( storeID );
                        totalCost -= cost;
                        TargetBool [storeID] = false;
                    }
                    else
                    {
                        bool unlock = gm.storeExpansionList.Data [storeID].storeLevel <= pm.businessUnlock.GetLevel ();
                        if ( unlock == false )
                        {
                            MessageBox.Warning ( "還債後解鎖" );
                            addition = gm.storeExpansionList.Data [storeID].skill;
                            expanation = gm.storeExpansionList.Data [storeID].explanation;
                            UpdateText ( totalCost , money , addition , expanation , iconSpr [storeID] );
                            return;
                        }
                        if ( pm.storeExpansionUnlock.GetComplete ( storeID ) == false )
                        {

                            bool canPay = Action.StoreExpansion.CheckMoney ( totalCost + cost , GameLoopManager.instance.pm );
                            if ( !canPay )
                            {
                                MessageBox.Warning ( "擴建餘額不足" );
                            }
                            else if ( gm.storeExpansionList.Data [storeID].storeLevel <= pm.businessUnlock.GetLevel () && !pm.storeExpansionUnlock.GetComplete ( storeID ) )
                            {
                                totalCost += cost;
                                TargetBool [storeID] = true;
                                storeIDs.Add ( storeID );
                            }
                        }
                    }
                }
                addition = gm.storeExpansionList.Data [storeID].skill;
                expanation = gm.storeExpansionList.Data [storeID].explanation;
                UpdateText ( totalCost , money , addition , expanation , iconSpr [storeID] );
            }
        }

        void UpdateText ( int cost , string money , string addition , string expanation , Sprite icon )
        {
            bool targetBool = false;
            for ( int i = 0; i < TargetBool.Length; i++ )
            {
                if ( TargetBool[i] )
                {
                    targetBool = true;
                    break;
                }
            }
            CostText.gameObject.SetActive ( targetBool );
            CostText.text = "-" + cost.ToString ( "N0" );
            MoneyText.text = money;
            AdditionText.text = Localizer.Translate ( addition );
            Expanation.text = Localizer.Translate ( expanation );
            IconImage.sprite = icon;
        }

        void OK ()
        {
            if ( storeIDs.Count == 0 )
            {
                //MessageBox.MainShow ( "尚未選擇" );
                PlayButtonAudio.self.PlayExpansionOK ();
                Back ();
                return;
            }
            Invoke ( "StoreExpansion" , 1f );
            LoadingScene.self.Transitions ( 0.5f , 1f , true );
            PlayButtonAudio.self.PlayExpansionOK ();
            PlayButtonAudio.self.PlayCustom ( "Audios/Effect/" , "Coin" , false );
        }

        UnityAction GenerateBack ()
        {
            return () =>
            {
                PlayButtonAudio.self.PlayExpansionCancel ();
                Back ();
            };
        }
        void Back ()
        {
            MainPanel.self.Close ();
        }

        void StoreExpansion ()
        {
            MainPanel.self.Close ();
            Action.StoreExpansion.Run ( storeIDs.ToArray () , pm , gm , GameLoopManager.instance.nextShift );
        }

        UnityAction GenerateSkipMode ()
        {
            return () =>
            {
                PlayButtonAudio.self.PlayExpansionType ();
                ChangeSkipMode ();
            };
        }

        public void ChangeSkipMode ()
        {
            skipBool = !skipBool;
            float pos = skipBool ? 45 : -45;
            SkipBar.DOLocalMoveX ( pos , 0.5f ).SetEase( Ease.OutExpo );
        }

        public StoreExpansionItem GetItem ( int id )
        {
            if ( id >= Data.Count )
            {
                Debug.LogError ( id + "超過項目長度" );
                StoreExpansionItem temp = new StoreExpansionItem ();
                temp.height = -1;
                return temp;
            }
            return Data [id];
        }

        public bool GetTarget ( int id )
        {
            if ( id >= TargetBool.Length )
            {
                Debug.LogError ( id + "超過項目長度" );
                return false;
            }
            return TargetBool [id];
        }

        public int GetTotal ()
        {
            return totalCost;
        }
    }
}
