﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace Prefab
{
    public class StoreExpansionItemPrefab : MonoBehaviour
    {
        public int id;
        public GameObject Target;
        public GameObject Click;
        public Text NameText;
        public Text CostText;
        public GameObject Lock;
        public Button Button;
        public bool clickBool;
        public Image [] typeIcon;
        public GameObject Complete;
        public GameObject NextLevel;

        private int cost;

        public void UpdateData ( UI.StoreExpansionItem data )
        {
            if ( data.id < 0 )
            {
                Debug.LogError ( data.id + "不該小於0" );
                return;
            }
            id = data.id;
            gameObject.name = "Item" + id.ToString ();

            NameText.text = Localizer.Translate ( data.name );
            cost = data.cost;
            CostText.text = cost.ToString ( "N0" ) + Localizer.Translate ( "元" );
            Target.SetActive ( UI.BuildStoreExpansion.self.GetTarget ( id ) );
            Lock.SetActive ( false );
            Button.enabled = true;
            for ( int i = 0; i < typeIcon.Length; i++ )
                typeIcon [i].sprite = data.typeIcon [i];
            Complete.GetComponent<Image> ().sprite = Resources.Load<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + "GameUI/StoreExpansion/Complete" );
            Complete.SetActive ( data.complete );
            NextLevel.SetActive ( data.nextLevel );
            Button.onClick.RemoveAllListeners ();
            Button.onClick.AddListener ( GenerateItem ( id ) );
        }

        UnityAction GenerateItem ( int id )
        {
            return () =>
            {
                int totalCost = UI.BuildStoreExpansion.self.GetTotal ();
                UI.BuildStoreExpansion.self.SwitchItem ( id );
                bool targetBool = UI.BuildStoreExpansion.self.GetTarget ( id );
                if ( targetBool )
                {
                    PlayButtonAudio.self.PlayExpansionClick ();
                }
                else
                {
                    PlayButtonAudio.self.PlayEmployeeCancel ();
                }
                Target.SetActive ( targetBool );
            };
        }
    }
}
