﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class StoreExpansionTypePrefab : MonoBehaviour
    {
        public Image FrameImage;
        public Image TypeImage;
        public Button Button;
    }
}
