﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MyScrollRect
{
    public class Review : MonoBehaviour
    {
        LayoutElement element;
        private void Awake ()
        {
            element = GetComponent<LayoutElement> ();
        }

        void ScrollCellIndex ( int idx )
        {
            int commodityID = UI.BuildReview.self.GetID ( idx );

            Prefab.ReviewItemPrefab item = GetComponent<Prefab.ReviewItemPrefab> ();
            item.UpdateData ( commodityID , GameLoopManager.instance.gm , GameLoopManager.instance.pm );
        }
    }
}