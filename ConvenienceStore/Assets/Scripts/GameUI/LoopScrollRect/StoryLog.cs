﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MyScrollRect
{
    public class StoryLog : MonoBehaviour
    {
        LayoutElement element;
        private void Awake ()
        {
            element = GetComponent<LayoutElement> ();
        }

        void ScrollCellIndex ( int idx )
        {
            StoryLogData Data = StoryLogManager.self.GetStoryLogData ( idx );
            if ( Data.story == "" )
                return;

            Prefab.StoryLogPrefab log = GetComponent<Prefab.StoryLogPrefab> ();
            float height = log.UpdateData ( Data );

            element.preferredHeight = height;
        }
    }
}