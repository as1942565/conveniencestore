﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MyScrollRect
{
    public class StoreExpansion : MonoBehaviour
    {
        LayoutElement element;
        private void Awake ()
        {
            element = GetComponent<LayoutElement> ();
        }

        void ScrollCellIndex ( int idx )
        {
            UI.StoreExpansionItem Data = UI.BuildStoreExpansion.self.GetItem ( idx );
            if ( Data.height == -1 )
                return;

            element.preferredHeight = Data.height;
            Prefab.StoreExpansionItemPrefab item = GetComponent<Prefab.StoreExpansionItemPrefab> ();
            item.UpdateData ( Data );
        }
    }
}