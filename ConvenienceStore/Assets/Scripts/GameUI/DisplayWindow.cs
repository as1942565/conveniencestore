﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayWindow : MonoBehaviour
{
    public GameObject Window;
    private bool showBool;

    public void HideWindow ()
    {
        hide ( false );
    }
    void hide ( bool show )
    {
        showBool = show;
        Window.SetActive ( showBool );
    }
    private void Update ()
    {
        if ( showBool == false && Input.GetMouseButton ( 0 ) )
        {
            hide ( true );
        }
    }
}
