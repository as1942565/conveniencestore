﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Observer
{
    public abstract class BaseObserver : MonoBehaviour
    {
        protected GameLoopManager manager;

        protected virtual void Start ()
        {
            //在Start的時候聽取在manager 加上OnInit
            manager = GameLoopManager.instance;
            OnManagerInit ( manager );
            GameLoopManager.instance.OnInit += OnManagerInit;
            RefreshComponent ( manager.pm );
        }

        protected virtual void OnEnable ()
        {
            //這裡確定不會重複register
            if ( manager != null )
            {
                UnregisterObserver ( manager.pm );
                RegisterObserver ( manager.pm );
                RefreshComponent ( manager.pm );
            }
        }
        protected virtual void OnDisable ()
        {
            if ( manager != null )
            {
                UnregisterObserver ( manager.pm );
            }
        }

        protected virtual void OnDestroy ()
        {
            if ( GameLoopManager.instance == null )
                return;
            GameLoopManager.instance.OnInit -= OnManagerInit;
            UnregisterObserver ( manager.pm );
        }

        /// <summary>
        /// 需要實作誰要聽取什麼東西
        /// </summary>
        /// <param name="pm"></param>
        protected abstract void RegisterObserver ( PlayerDataManager pm );
        /// <summary>
        /// 需要實作解除聽取
        /// </summary>
        /// <param name="pm"></param>
        protected abstract void UnregisterObserver ( PlayerDataManager pm );

        protected abstract void RefreshComponent (PlayerDataManager pm);

        protected virtual void OnManagerInit ( GameLoopManager manager )
        {
            this.manager = manager;
            UnregisterObserver ( manager.pm );
            RegisterObserver ( manager.pm );
            RefreshComponent ( manager.pm );
        }

    }
}
