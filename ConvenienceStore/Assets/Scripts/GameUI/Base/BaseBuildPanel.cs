﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public abstract class BaseBuildPanel : Builder
    {
        protected PlayerDataManager pm;
        protected GameDataManager gm;

        protected bool isBuild;
        public abstract void Init ();
        public abstract void Build ();
        public override void Open ()
        {
            if (this.gameObject == null )
            {
                return;
            }
            gameObject.SetActive ( true );
            pm = GameLoopManager.instance.pm;
            gm = GameLoopManager.instance.gm;
            if ( !isBuild )
            {
                Build ();
            }
            Init ();
        }

        public override void Close ()
        {
            gameObject.SetActive ( false );
        }

        public override void Destroy ()
        {
            isBuild = false;
        }
    }
}
