﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class CGPagePrefab : MonoBehaviour
    {
        public Image Page;
        public Image TargetPage;
        public Button Button;
    }
}