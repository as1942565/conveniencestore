﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace UI
{
    public class BuildCG : BaseBuildPanel
    {
        public BuildFocusCG buildFocusCG;
        public BuildActor buildActor;
        public Transform CGSpace;
        public Transform PageSpace;
        public Image Background;
        public Text ScoreText;
        public Button TypeBtn;

        private int quantityX = 3;
        private int quantityY = 3;
        private float posX = 530f;
        private float posY = 285f;
        private int pageQuantity = 2;
        private float distance = 90f;
        private int page;

        private const string path = "GameStart/CG/";

        Prefab.CGPrefab [] CGs;
        Prefab.CGPagePrefab [] pages;
        Sprite [] [] sprites;

        string [] WuJing = new string [] { "A1" , "A1" , "A3" , "A4" , "A5" , "A6" };
        string [] LianShin = new string [] { "A1" , "A2" , "A3" , "A4" , "A5" , "A6" };
        string [] YiShan = new string [] { "A1" , "A2" , "A3" , "A4" , "A5" , "A6" };

        private void Update ()
        {
            if ( Input.GetKeyDown ( KeyCode.Escape ) )
            {
                Close ();
            }
        }

        private void Awake ()
        {
            Sprite [] [] WuJingSpr = new Sprite [WuJing.Length] [];
            Sprite [] [] LianShinSpr = new Sprite [LianShin.Length] [];
            Sprite [] [] YiShanSpr = new Sprite [YiShan.Length] [];
            Sprite [] [] Custom = new Sprite [2] [];
            for ( int w = 0; w < WuJing.Length; w++ )
            {
                WuJingSpr [w] = Resources.LoadAll<Sprite> ( "Sprites/Common/Drama/Picture/0/" + WuJing [w] );
            }
            for ( int l = 0; l < LianShin.Length; l++ )
            {
                LianShinSpr [l] = Resources.LoadAll<Sprite> ( "Sprites/Common/Drama/Picture/1/" + LianShin [l] );
            }
            if ( GameLoopManager.instance.GetSteam () == false )
            {
                for ( int y = 0; y < YiShan.Length; y++ )
                {
                    YiShanSpr [y] = Resources.LoadAll<Sprite> ( "Sprites/Common/Drama/Picture/2/" + YiShan [y] );
                }
            }
            else
            {
                YiShanSpr = new Sprite [0] [];
            }
            Custom [0] = Resources.LoadAll<Sprite> ( "Sprites/Common/Drama/Picture/3/A1" );
            Custom [1] = Resources.LoadAll<Sprite> ( "Sprites/Common/Drama/Picture/4/A1" );
            int length = WuJingSpr.Length + LianShinSpr.Length + YiShanSpr.Length + Custom.Length;
            if ( GameLoopManager.instance.GetSteam () == true )
                length = WuJingSpr.Length + LianShinSpr.Length + Custom.Length;
            sprites = new Sprite [length] [];
            for ( int w = 0; w < WuJingSpr.Length; w++ )
            {
                sprites [w] = WuJingSpr [w];
            }
            for ( int l = 0; l < LianShinSpr.Length; l++ )
            {
                sprites [l + WuJingSpr.Length] = LianShinSpr [l];
            }
            if ( GameLoopManager.instance.GetSteam () == false )
            {
                for ( int y = 0; y < YiShanSpr.Length; y++ )
                {
                    sprites [y + WuJingSpr.Length + LianShinSpr.Length] = YiShanSpr [y];
                }
            }
            for ( int c = 0; c < Custom.Length; c++ )
            {
                sprites [c + WuJingSpr.Length + LianShinSpr.Length + YiShanSpr.Length] = Custom [c];
            }

            gameObject.SetActive ( false );

            if ( GameLoopManager.instance.WatchStory == true )
            {
                GameLoopManager.instance.WatchStory = false;
                Open ();
            }
        }

        public override void Build ()
        {
            int id = 0;
            CGs = new Prefab.CGPrefab [quantityX * quantityY];
            for ( int y = 0; y < quantityY; y++ )
            {
                for ( int x = 0; x < quantityX; x++ )
                {
                    Prefab.CGPrefab cg = Instantiate ( Resources.Load<Prefab.CGPrefab> ( "Prefabs/" + path + "Item" ) , CGSpace );
                    cg.transform.localPosition += new Vector3 ( x * posX , -y * posY , 0 );
                    if ( id >= sprites.Length )
                        cg.NoData ();
                    else
                    {
                        cg.UpdateData ( id , sprites [id] [0] , pm );
                        cg.Button.onClick.AddListener ( GenerateCG ( id ) );
                        cg.Watch.onClick.AddListener ( GenerateWatch ( id ) );
                    }
                    CGs [id] = cg;
                    id++;
                }
            }
            Sprite [] pageSpr = SpritePath.Page2;
            Sprite [] targetPageSpr = SpritePath.TargetPage2;
            pages = new Prefab.CGPagePrefab [pageQuantity];
            for ( int i = 0; i < pages.Length; i++ )
            {
                Prefab.CGPagePrefab page = Instantiate ( Resources.Load<Prefab.CGPagePrefab> ( "Prefabs/" + path + "Page" ) , PageSpace );
                page.transform.localPosition += new Vector3 ( distance * i , 0 , 0 );
                page.Page.sprite = pageSpr [i];
                page.TargetPage.sprite = targetPageSpr [i];
                page.Button.onClick.AddListener ( GeneratePage ( i ) );
                pages [i] = page;
            }
            isBuild = true;

            TypeBtn.onClick.AddListener ( SwitchType );
        }

        void SwitchType ()
        {
            Close ();
            buildActor.Open ();
        }

        UnityAction GenerateCG ( int id )
        {
            return () =>
            {
                SwitchCG ( page * 9 + id );
                PlayButtonAudio.self.PlayGallery ();
            };
        }
        void SwitchCG ( int id )
        {
            int [] ids = GameLoopManager.instance.om.unlockCGData.GetCurrectID ( id );
            int employeeID = ids [0];
            int storyID = ids [1];
            buildFocusCG.SetPicture ( employeeID , storyID , id , this );
            buildFocusCG.Open ();
        }

        UnityAction GenerateWatch ( int id )
        {
            return () =>
            {
                Watch ( id );
                PlayButtonAudio.self.PlayGallery ();
            };
        }

        void Watch ( int id )
        {
            int [] ids = GameLoopManager.instance.om.unlockCGData.GetCurrectID ( id + page * CGs.Length );
            GameLoopManager.instance.WatchStory = true;
            GameLoopManager.instance.ids = ids;
            LoadingScene.self.Transitions ( 0.5f , 1f , false );
            Invoke ( "GoToScene" , 0.5f );
        }

        void GoToScene ()
        {
            SceneManager.LoadScene ( "Demo" );
        }

        UnityAction GeneratePage ( int page )
        {
            return () =>
            {
                SwitchPage ( page );
                PlayButtonAudio.self.PlayGallery ();
            };
        }
        void SwitchPage ( int page )
        {
            this.page = page;
            for ( int i = 0; i < pages.Length; i++ )
            {
                pages [i].TargetPage.gameObject.SetActive ( i == page );
            }
            ShowItem ( page );
        }
        void ShowItem ( int page )
        {
            int length = CGs.Length;
            for ( int i = 0; i < length; i++ )
            {
                int id = length * page + i;
                if ( id >= sprites.Length)
                    CGs [i].NoData ();
                else
                {
                    CGs [i].UpdateData ( id , sprites [id] [0] , pm );
                }
            }
        }

        public override void Close ()
        {
            base.Close ();
            PlayButtonAudio.self.PlayGallery ();
        }

        public override void Init ()
        {
            Background.sprite = Resources.Load<Sprite> ( "Sprites/" +  GameLoopManager.instance.om.optionData.GetLanguageStr () + path + "Background" );
            SwitchPage ( 0 );
            if ( GameLoopManager.instance.GetSteamCheck () )
                ScoreText.text = "100%";
            else
                ScoreText.text = GameLoopManager.instance.om.unlockCGData.GetUnlockScale ().ToString () + "%";

            TypeBtn.gameObject.SetActive ( GameLoopManager.instance.GetSteam () == false );
            TypeBtn.GetComponent<Image> ().sprite = Resources.LoadAll<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameStart/CG/Type" ) [0];
        }

        public Sprite [] [] GetSprites ()
        {
            return sprites;
        }
    }
}