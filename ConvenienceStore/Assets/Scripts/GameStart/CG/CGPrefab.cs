﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class CGPrefab : MonoBehaviour
    {
        public int employeeID;
        public int id;
        public Image Photo;
        public GameObject Mask;
        public Button Button;
        public Button Watch;

        public void UpdateData ( int id , Sprite spr , PlayerDataManager pm )
        {
            bool hide = false;

            this.id = id;
            Photo.sprite = spr;
            int [] ids = GameLoopManager.instance.om.unlockCGData.GetCurrectID ( id );
            employeeID = ids [0];
            if ( GameLoopManager.instance.GetSteam () )
            {
                if ( ids [0] == 2 )
                    hide = true;
                else
                    hide = false;
                if ( employeeID == 3 && id == 13 ) 
                    hide = true;
            }
            else
                hide = !GameLoopManager.instance.om.unlockCGData.GetUnlock ( ids [0] , ids [1] );

            Mask.SetActive ( hide );
            Button.enabled = !hide;
        }

        public void NoData ()
        {
            employeeID = -1;
            Mask.SetActive ( true );
        }
    }
}