﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class BuildFocusCG : BaseBuildPanel
    {
        private int employeeID;
        private int storyID;
        private int spriteID;
        [SerializeField]
        private int currectID;

        public Button BackBtn;
        public Button NextBtn;
        public Button LeftBtn;
        public Button RightBtn;
        public Image Picture; 

        Sprite [] [] sprites;
        UnlockCGData unlockCGData;

        public void SetPicture ( int employeeID , int storyID , int currectID , BuildCG buildCG )
        {
            this.currectID = currectID;
            unlockCGData = GameLoopManager.instance.om.unlockCGData;
            this.employeeID = employeeID;
            this.storyID = storyID;
            sprites = buildCG.GetSprites ();
            Picture.sprite = sprites [currectID] [0];
        }

        public override void Build ()
        {
            BackBtn.onClick.AddListener ( Back );
            NextBtn.onClick.AddListener ( Next );
            LeftBtn.onClick.AddListener ( Left );
            RightBtn.onClick.AddListener ( Right );
            isBuild = true;
        }

        public override void Init ()
        {
            spriteID = 0;
            LeftBtn.gameObject.SetActive ( sprites [storyID].Length > 1 );
            RightBtn.gameObject.SetActive ( sprites [storyID].Length > 1 );
        }

        void Back ()
        {
            Close ();
        }

        void Next ()
        {
            currectID++;
            if ( currectID >= unlockCGData.GetAllUnlockLength () )
                currectID = 0;

            while ( unlockCGData.GetUnlock ( currectID ) == false )
            {
                currectID++;
                if ( currectID >= unlockCGData.GetAllUnlockLength () )
                    currectID = 0;
            }
            spriteID = 0;
            Picture.sprite = sprites [currectID] [spriteID];
        }

        void Left ()
        {
            spriteID--;
            if ( spriteID < 0 )
                spriteID = sprites [currectID].Length - 1;
            Picture.sprite = sprites [currectID] [spriteID];
        }

        void Right ()
        {
            spriteID++;
            if ( spriteID >= sprites [currectID].Length )
                spriteID = 0;
            Picture.sprite = sprites [currectID] [spriteID];
        }
    }
}
