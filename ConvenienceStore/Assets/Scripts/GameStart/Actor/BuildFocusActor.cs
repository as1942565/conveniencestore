﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class BuildFocusActor : BaseBuildPanel
    {
        private int actorID;
        private int spriteID;

        public Button BackBtn;
        public Button NextBtn;
        public Button LeftBtn;
        public Button RightBtn;
        public Image Actor;

        Sprite [] [] sprites;

        public void SetActor ( int id , BuildActor buildActor )
        {
            actorID = id;
            sprites = buildActor.GetSprites ();
            Actor.sprite = sprites [actorID] [0];
        }

        public override void Build ()
        {
            BackBtn.onClick.AddListener ( Back );
            NextBtn.onClick.AddListener ( Next );
            LeftBtn.onClick.AddListener ( Left );
            RightBtn.onClick.AddListener ( Right );
            isBuild = true;
        }

        public override void Init ()
        {
            spriteID = 0;
            LeftBtn.gameObject.SetActive ( sprites [actorID].Length > 1 );
            RightBtn.gameObject.SetActive ( sprites [actorID].Length > 1 );
        }

        void Back ()
        {
            Close ();
        }

        void Next ()
        {
            actorID++;
            while ( actorID < sprites.Length && GameLoopManager.instance.om.unlockActorData.GetUnlock ( actorID ) == false )
                actorID++;
            if ( actorID >= sprites.Length )
                actorID = 0;
            spriteID = 0;
            Actor.sprite = sprites [actorID] [spriteID];
            LeftBtn.gameObject.SetActive ( sprites [actorID].Length > 1 );
            RightBtn.gameObject.SetActive ( sprites [actorID].Length > 1 );
        }

        void Left ()
        {
            spriteID--;
            if ( spriteID < 0 )
                spriteID = sprites [actorID].Length - 1;
            Actor.sprite = sprites [actorID] [spriteID];
        }

        void Right ()
        {
            spriteID++;
            if ( spriteID >= sprites [actorID].Length )
                spriteID = 0;
            Actor.sprite = sprites [actorID] [spriteID];
        }
    }
}