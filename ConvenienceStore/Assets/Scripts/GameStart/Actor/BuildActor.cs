﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UI
{
    public class BuildActor : BaseBuildPanel
    {
        public BuildCG buildCG;
        public BuildFocusActor buildFocusActor;
        public Transform ActorSpace;
        public Transform PageSpace;
        public Image Background;
        public Text ScoreText;
        public Button TypeBtn;

        private int page;
        private int quantityX = 3;
        private int quantityY = 3;
        private float posX = 530f;
        private float posY = 285f;
        private int pageQuantity = 3;
        private float distance = 90f;

        private const string path = "GameStart/CG/";
        private const string actorPath = "Sprites/Common/Drama/Actor/";

        Prefab.ActorPrefab [] actors;
        Prefab.CGPagePrefab [] pages;
        Sprite [] [] sprites;

        private void Update ()
        {
            if ( Input.GetKeyDown ( KeyCode.Escape ) )
            {
                Close ();
            }
        }

        private void Awake ()
        { 
            string [] index = new string [] { "0" , "1" , "2" , "3" , "4" , "5" , "6" };
            if ( GameLoopManager.instance.GetSteam () == true )
                index = new string [] { "0" , "1" , "3" , "5" , "6" };
            Sprite [] [] actor = new Sprite [index.Length] [];
            for ( int i = 0; i < actor.Length; i++ )
            {
                actor [i] = Resources.LoadAll<Sprite> ( actorPath + index [i] );
            }
            Sprite [] ex = Resources.LoadAll<Sprite> ( actorPath + "EX" );
            List<Sprite> worker = new List<Sprite> ();
            for ( int i = 0; i < ex.Length; i++ )
            {
                if ( ex[i].name.Contains ("WK") )
                {
                    worker.Add ( ex [i] );
                }
            }

            int length = actor.Length + worker.Count;
            sprites = new Sprite [length] [];
            for ( int i = 0; i < actor.Length; i++ )
            {
                sprites [i] = actor [i];
            }
            for ( int i = 0; i < worker.Count; i++ )
            {
                sprites [i + actor.Length] = new Sprite [1];
                sprites [i + actor.Length] [0] = worker [i];
            }

            gameObject.SetActive ( false );
        }

        public override void Build ()
        {
            int id = 0;
            actors = new Prefab.ActorPrefab [quantityX * quantityY];
            for ( int y = 0; y < quantityY; y++ )
            {
                for ( int x = 0; x < quantityX; x++ )
                {
                    Prefab.ActorPrefab actor = Instantiate ( Resources.Load<Prefab.ActorPrefab> ( "Prefabs/" + path + "Actor" ) , ActorSpace );
                    actor.transform.localPosition += new Vector3 ( x * posX , -y * posY , 0 );
                    if ( id >= sprites.Length )
                        actor.NoData ();
                    else
                    {
                        actor.UpdateData ( id , sprites [id] [0] , pm );
                        actor.Button.onClick.AddListener ( GenerateActor ( id ) );
                    }
                    actors [id] = actor;
                    id++;
                }
            }
            Sprite [] pageSpr = SpritePath.Page2;
            Sprite [] targetPageSpr = SpritePath.TargetPage2;
            pages = new Prefab.CGPagePrefab [pageQuantity];
            for ( int i = 0; i < pages.Length; i++ )
            {
                Prefab.CGPagePrefab page = Instantiate ( Resources.Load<Prefab.CGPagePrefab> ( "Prefabs/" + path + "Page" ) , PageSpace );
                page.transform.localPosition += new Vector3 ( distance * i , 0 , 0 );
                page.Page.sprite = pageSpr [i];
                page.TargetPage.sprite = targetPageSpr [i];
                page.Button.onClick.AddListener ( GeneratePage ( i ) );
                pages [i] = page;
            }
            isBuild = true;

            TypeBtn.onClick.AddListener ( SwitchType );
        }

        void SwitchType ()
        {
            Close ();
            buildCG.Open ();
        }

        public override void Init ()
        {
            Background.sprite = Resources.Load<Sprite> ( "Sprites/" +  GameLoopManager.instance.om.optionData.GetLanguageStr () + path + "Background" );
            SwitchPage ( 0 );
            if ( GameLoopManager.instance.GetSteamCheck () )
                ScoreText.text = "100%";
            else
                ScoreText.text = GameLoopManager.instance.om.unlockActorData.GetUnlockScale ().ToString () + "%";

            TypeBtn.GetComponent<Image>().sprite = Resources.LoadAll<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}GameStart/CG/Type" ) [1];
        }

        UnityAction GenerateActor ( int id )
        {
            return () =>
            {
                SwitchActor ( id );
                PlayButtonAudio.self.PlayGallery ();
            };
        }
        void SwitchActor ( int id )
        {
            buildFocusActor.SetActor ( id + page * actors.Length , this );
            buildFocusActor.Open ();
        }
        UnityAction GeneratePage ( int page )
        {
            return () =>
            {
                SwitchPage ( page );
                PlayButtonAudio.self.PlayGallery ();
            };
        }
        
        void SwitchPage ( int page )
        {
            this.page = page;
            for ( int i = 0; i < pages.Length; i++ )
            {
                pages [i].TargetPage.gameObject.SetActive ( i == page );
            }
            ShowItem ( page );
        }
        void ShowItem ( int page )
        {
            int length = actors.Length;
            for ( int i = 0; i < length; i++ )
            {
                int id = length * page + i;
                if ( id >= sprites.Length )
                    actors [i].NoData ();
                else
                {
                    actors [i].UpdateData ( id , sprites [id] [0] , pm );
                }
            }
        }
        public override void Close ()
        {
            base.Close ();
            PlayButtonAudio.self.PlayGallery ();
        }

        public Sprite [] [] GetSprites ()
        {
            return sprites;
        }
    }

}
