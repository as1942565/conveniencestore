﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class ActorPrefab : MonoBehaviour
    {
        public int id;
        public Image Actor;
        public GameObject Mask;
        public Button Button;

        public void UpdateData ( int id , Sprite spr , PlayerDataManager pm )
        {
            bool show = false;

            Button.gameObject.SetActive ( true );
            this.id = id;
            Actor.sprite = spr;
            show = !GameLoopManager.instance.om.unlockActorData.GetUnlock ( id );
            Mask.SetActive ( show );
            Button.enabled = !show;
        }

        public void NoData ()
        {
            id = -1;
            Button.gameObject.SetActive ( false );
            Mask.SetActive ( true );
        }
    }
}