﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

//遊戲開始
public class StartCtl : MonoBehaviour
{
    public UI.BuildSave BuildLoad;

    public string SceneName;
    public UI.BuildCG buildCG;

    public bool skip;

    public Image BG;
    public Sprite sprite1;
    public Sprite sprite2;

    private void Awake ()
    {
        if ( GameLoopManager.instance == null )
        {
            SceneManager.LoadScene ( "CreateObj" );
            return;
        }
        if ( skip )
        {
            GameLoopManager.instance.Load = true;
            GoToScene ();
        }
        if ( GameLoopManager.instance.GetSteam () )
        {
            BG.sprite = sprite2;
        }
        else
            BG.sprite = sprite1;
    }

    public void PrologueBtn ()
    {
        GameLoopManager.SkipPrologue = false;
        start ();
    }

    public void StartBtn ()
    {
        GameLoopManager.SkipPrologue = true;
        start ();
    }

    void start ()
    {
        EventSystem.current.currentSelectedGameObject.GetComponent<Button> ().enabled = false;
        PlayButtonAudio.self.PlayMenuClick ();
        ReadStory ( true );
    }

    void ReadStory ( bool read )
    {
        TeachPlayerManager.self.ClearIndex ();
        GameLoopManager.instance.Load = true;
        GameLoopManager.instance.ReadFirstStory = read;
        LoadingScene.self.Transitions ( 0.5f , 1f , true );
        Invoke ( "GoToScene" , 0.5f );          //0.5秒後切換場景
    }

    public void LoadBtn ()
    {
        PlayButtonAudio.self.PlayMenuClick ();
        BuildLoad.Open ();
    }

    public void ExtraBtn ()
    {
        PlayButtonAudio.self.PlayMenuClick ();
        buildCG.Open ();
    }

    public void EndBtn ()
    {
        PlayButtonAudio.self.PlayMenuClick ();
        EventSystem.current.currentSelectedGameObject.GetComponent<Button> ().enabled = false;
        Application.Quit ();
    }

    public void GoToScene ()                    //切換場景
    {
        GameLoopManager.instance.OnInit = null;

        SceneManager.LoadScene ( SceneName );   //切換場景

        GameLoopManager.instance.OnLoaded ( GameLoopManager.instance.pm );
        //GameLoopManager.instance.Init ();
    }
}
