﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PeopleAction : MonoBehaviour
{
    public float speed;

    Animator anim;
    SpriteRenderer spriteRenderer;

    public void Hide ()
    {
        if ( this == null )
            return;
        spriteRenderer = GetComponent<SpriteRenderer> ();
        Color c = spriteRenderer.color;
        c.a = 0;
        spriteRenderer.color = c;
    }
    public void Show ( float duration )
    {
        if ( this == null )
            return;
        spriteRenderer.DOFade ( 1 , duration );
    }
    public void SetAction ( string animName )
    {
        if ( this == null )
            return;
        anim = GetComponent<Animator> ();
        anim.speed = speed;
        anim.Play ( animName );
    }
    public void SetRight ( bool right )
    {
        if ( this == null )
            return;
        if ( right )
            transform.localScale = new Vector3 ( -1 , 1 , 1 );
    }
    public void SetStartPos ( Vector2 pos , float order )
    {
        if ( this == null )
            return;
        transform.localPosition = new Vector3 ( pos.x , pos.y , -order );
    }
    public void Move ( Vector2 pos , float order , float time )
    {
        if ( this == null )
            return;
        transform.DOLocalMove ( new Vector3 ( pos.x , pos.y , -order ) , time ).SetEase ( Ease.Linear );
    }
    public void Delete ( float duration )
    {
        if ( this == null )
            return;
        spriteRenderer.DOFade ( 0 , duration );
        Destroy ( this.gameObject , duration + 0.2f );
    }
 
}
