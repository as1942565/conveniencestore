﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Tools;

public class PeopleManager : MonoBehaviour
{
    public static PeopleManager self;
    private void Awake ()
    {
        self = this;
        if ( GameLoopManager.instance == null )
        {
            SceneManager.LoadScene ( "CreateObj" );
            return;
        }
        Init ( GameLoopManager.instance.gm , GameLoopManager.instance.pm );
    }
    
    public float duration = 1f;
    private float time;
    private float mainTime;

    private const string path = "Prefabs/People/";
    private Coroutine customerCor;
    private Coroutine mainPeopleCor;

    public void Init ( GameDataManager gm , PlayerDataManager pm )
    {
        int storeLevel = pm.businessUnlock.GetLevel ();
        if ( storeLevel >= 3 )
            storeLevel = 2;
        Path [] paths = new Path [gm.peopleLists[storeLevel].Data.path.Length];
        Path [] mainPaths = new Path [gm.mainPeopleLists[storeLevel].Data.path.Length];
        for ( int i = 0; i < paths.Length; i++ )
        {
            Path p = gm.peopleLists [storeLevel].Data.path [i];
            Path temp;
            temp.main = p.main;
            temp.startPos = p.startPos;
            temp.endPos = p.endPos;
            temp.right = p.right;
            temp.animName = p.animName;
            temp.order = p.order;
            paths [i] = temp;
        }
        for ( int i = 0; i < mainPaths.Length; i++ )
        {
            Path p = gm.mainPeopleLists [storeLevel].Data.path [i];
            Path temp;
            temp.main = p.main;
            temp.startPos = p.startPos;
            temp.endPos = p.endPos;
            temp.right = p.right;
            temp.animName = p.animName;
            temp.order = p.order;
            mainPaths [i] = temp;
        }
        GameObject [] customers = Resources.LoadAll<GameObject> ( path + "Customer/" );
        GameObject [] specials = Resources.LoadAll<GameObject> ( path + "Special/" );
        customerCor = StartCoroutine ( StartCustomerRoutine ( gm , pm , customers , specials , paths ) );

        GameObject [] mainPeople = Resources.LoadAll<GameObject> ( path + "Main/" );
        mainPeopleCor = StartCoroutine ( StartMainPeopleRoutine ( gm , pm , mainPeople , mainPaths ) );

    }

    IEnumerator StartCustomerRoutine ( GameDataManager gm , PlayerDataManager pm , GameObject [] customers , GameObject [] specials , Path [] paths )
    {
        time = Random.Range ( 3f , 4f );
        StartCoroutine ( GenerateCustomer ( gm , pm , customers , paths ) );
        yield return new WaitForSeconds ( time );

        int length = pm.specialPeople.GetAllUnlock ().Length;
        int ran = Random.Range ( 0 , 14 );
        if ( length > ran )
            customerCor = StartCoroutine ( StartSpecialRoutine ( gm , pm , customers , specials , paths ) );
        else
            customerCor = StartCoroutine ( StartCustomerRoutine ( gm , pm , customers , specials , paths ) );
    }
    
    IEnumerator StartSpecialRoutine ( GameDataManager gm , PlayerDataManager pm , GameObject [] customers , GameObject [] specials , Path [] paths )
    {
        time = Random.Range ( 3f , 4f );
        StartCoroutine ( GenerateSpecial ( gm , pm , specials , paths ) );
        yield return new WaitForSeconds ( time );

        int length = pm.specialPeople.GetAllUnlock ().Length;
        int ran = Random.Range ( 0 , 14 );
        if ( length > ran )
            customerCor = StartCoroutine ( StartSpecialRoutine ( gm , pm , customers , specials , paths ) );
        else
            customerCor = StartCoroutine ( StartCustomerRoutine ( gm , pm , customers , specials , paths ) );
    }

    IEnumerator StartMainPeopleRoutine ( GameDataManager gm , PlayerDataManager pm , GameObject [] mainPeople , Path [] paths )
    {
        mainTime = Random.Range ( 4f , 6f );
        StartCoroutine ( GenerateMain ( gm , pm , mainPeople , paths ) );
        yield return new WaitForSeconds ( mainTime );

        mainPeopleCor = StartCoroutine ( StartMainPeopleRoutine ( gm , pm , mainPeople , paths ) );
    }

    public void StopCreatePoeple ()
    {
        PeopleAction [] people = GetComponentsInChildren<PeopleAction> ();
        for ( int i = 0; i < people.Length; i++ )
        {
            Destroy ( people[i].gameObject );
        }
        StopCoroutine ( customerCor );
        StopCoroutine ( mainPeopleCor );
    }
    IEnumerator GenerateSpecial ( GameDataManager gm , PlayerDataManager pm , GameObject [] specials , Path [] paths )
    {
        //check posible customer
        int [] unlockID = pm.specialPeople.GetAllUnlock ();
        if ( unlockID.Length == 0 )
            yield break;
        unlockID.Shuffle ();
        //instantiate customer
        PeopleAction customer = Instantiate ( specials [unlockID[0]] , transform ).GetComponent<PeopleAction> ();
        customer.Hide ();
        //random path
        int ranPath = Random.Range ( 0 , paths.Length );
        while ( paths [ranPath].startPos == paths [ranPath].endPos )
        {
            ranPath = Random.Range ( 0 , paths.Length );
            yield return null;
        }
        yield return Walk ( customer , paths [ranPath] , time );
    }

    int tempMainPath = -1;
    IEnumerator GenerateMain ( GameDataManager gm , PlayerDataManager pm , GameObject [] mainPeople , Path [] paths )
    {
        //check posible main
        int ranID = 3;
        int id = 3;
        if ( pm.calender.GetDay () != 0 )
        {
            int day = pm.calender.GetDay ();
            int shift = (int)pm.calender.GetShift ();
            int [] workEmployeeID = pm.employeeScheduleLog.GetMainIDWithShift ( day , shift , pm.employeeList );

            if ( workEmployeeID.Length != 0 )
            {
                ranID = Random.Range ( 0 , workEmployeeID.Length );
            }
            if ( ranID != 3 )
                id = workEmployeeID [ranID];
        }
        PeopleAction people = Instantiate ( mainPeople [id] , transform ).GetComponent<PeopleAction> ();
        people.Hide ();
        //select path
        int ranPath = 0;
        if ( id != 3 )
        {
            ranPath = Random.Range ( 0 , paths.Length );
            while ( tempMainPath == ranPath )
            {
                ranPath = Random.Range ( 0 , paths.Length );
                yield return null;
            }
            while ( ranPath == 3 && !pm.storeExpansionUnlock.GetComplete ( 13 ) )
            {
                ranPath = Random.Range ( 0 , paths.Length );
                yield return null;
            }
            tempMainPath = ranPath;
        }

        //Walk(main,path)
        yield return Walk ( people , paths [ranPath] , mainTime );
    }

    int tempPath;
    IEnumerator GenerateCustomer ( GameDataManager gm , PlayerDataManager pm , GameObject [] customers , Path [] paths )
    {
        //check posible customer
        int ranID = Random.Range ( 0 , customers.Length );

        //instantiate customer
        PeopleAction customer = Instantiate ( customers [ranID] , transform ).GetComponent<PeopleAction> ();
        customer.Hide ();
        //random path
        int ranPath = Random.Range ( 0 , paths.Length );
        while ( tempPath == ranPath )
        {
            ranPath = Random.Range ( 0 , paths.Length );
            yield return null;
        }
        tempPath = ranPath;
        yield return Walk ( customer , paths [ranPath] , time );
    }

    private IEnumerator Walk ( PeopleAction people , Path path , float Time )
    {
        people.Show ( duration );
        people.SetAction ( path.animName );
        people.SetRight ( path.right );
        people.SetStartPos ( path.startPos , path.order );
        people.Move ( path.endPos , path.order , Time );
        yield return new WaitForSeconds ( Time - duration );
        people.Delete ( duration );
        yield return null;
    }

    [System.Serializable]
    public struct Path
    {
        public bool main;
        public Vector2 startPos;
        public Vector2 endPos;
        public bool right;
        public string animName;
        public float order;
    }
}
