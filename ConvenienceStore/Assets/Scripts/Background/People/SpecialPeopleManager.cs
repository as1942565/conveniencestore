﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialPeopleManager : MonoBehaviour
{
    public GameObject Mask;
    public GameObject ShowPeople;

    public static SpecialPeopleManager self;
    private void Awake ()
    {
        self = this;

    }

    public void Create ( string name )
    {
        Mask.SetActive ( true );
        ShowPeople.SetActive ( true );
        PeopleAction people = Instantiate ( Resources.Load<PeopleAction> ( "Prefabs/People/Special/" + name ) , transform );
        ShowPeople.transform.parent = people.transform;
        Vector2 pos = people.transform.GetChild ( 0 ).localPosition;
        ShowPeople.transform.localPosition = pos;
        StartCoroutine ( Walk ( people ) );
    }

    IEnumerator Walk ( PeopleAction people )
    {
        yield return new WaitForSeconds ( 0.1f );
        bool mask = UI.MainPanel.self.Shield.gameObject.activeSelf;
        UI.MainPanel.self.Shield.gameObject.SetActive ( false );
        UI.MainPanel.self.ShowWindow ( false );
        people.Show ( 1 );
        people.SetAction ( "Walk_F" );
        people.SetRight ( false );
        people.SetStartPos ( new Vector2 ( 5.04f , 0.7f ) , 10.1f );
        people.Move ( new Vector2 ( 1.35f , -1.46f ) , 10.1f , 3 );
        yield return new WaitForSeconds ( 3f );

        people.SetAction ( "Event" );
        people.SetRight ( true );
        while ( people.GetComponent<Animator>().GetCurrentAnimatorStateInfo( 0 ).IsName ( "Finish" ) == false )
        {
            yield return null;
        }

        people.Delete ( 0.8f );
        LoadingScene.self.Transitions ( 0.5f , 1f , true );
        yield return new WaitForSeconds ( 0.5f );
        GameLoopManager.CanAfterNextShift = true;
        Mask.SetActive ( false );
        ShowPeople.transform.parent = transform;
        ShowPeople.transform.localScale = new Vector3 ( 1 , 1 , 1 );
        ShowPeople.SetActive ( false );
        UI.MainPanel.self.ShowWindow ( true );
        UI.MainPanel.self.Shield.gameObject.SetActive ( mask );
    }
}
