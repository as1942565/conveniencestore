﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class ExpansionManager : MonoBehaviour
{
    public static ExpansionManager self;
    private void Awake ()
    {
        self = this;
        render = Resources.LoadAll<Sprite> ( "Sprites/Common/" + path + "StoreExpland" );
        Renderer = new Dictionary<int , SpriteRenderer> ();
        SceneManager.activeSceneChanged += ChangedActiveScene;
    }

    private const string path = "Background/";
    private Dictionary<int,SpriteRenderer> Renderer;
    
    #region pos
    private Vector2 [] pos = new Vector2 [] {/* 0 1 */new Vector2 (  0.65f ,  3.15f ) , new Vector2 (  0.65f ,  3.15f ) ,
                                             /* 2 3 */new Vector2 (  1.85f ,  2.65f ) , new Vector2 (  2.07f ,  2.52f ) ,
                                             /* 4 5 */new Vector2 (  1.81f ,  2.65f ) , new Vector2 (  5.66f , -1.89f ) ,
                                             /* 6 7 */new Vector2 (  5.66f , -1.89f ) , new Vector2 (  5.66f , -1.89f ) ,
                                             /* 8 9 */new Vector2 (  0.65f ,  3.15f ) , new Vector2 (  0.65f ,  3.15f ) ,
                                             /*10 11*/new Vector2 ( -1.20f , -2.53f ) , new Vector2 (  1.58f , -1.04f ) ,
                                             /*12 13*/new Vector2 (  4.04f ,  3.25f ) , new Vector2 (  0.47f , -3.67f ) ,
                                             /*14 15*/new Vector2 (  0.47f , -3.71f ) , new Vector2 (  0.47f , -3.74f ) ,
                                             /*16 17*/new Vector2 ( -9.36f , -0.26f ) , new Vector2 ( -5.01f , -4.34f ) ,
                                             /*18 19*/new Vector2 ( -8.16f , -0.04f ) , new Vector2 ( -6.67f , -4.83f ) ,
                                             /*20 21*/new Vector2 ( -7.34f , -4.5f  ) , new Vector2 ( -5.15f ,  0.67f ) ,
                                             /*22 23*/new Vector2 ( -8.83f ,  1.13f ) , new Vector2 (  9.08f ,  1.32f ) ,
                                             /*24 25*/new Vector2 ( -1.91f ,  4.64f ) , new Vector2 ( -3.60f , -3.68f ) ,
                                             /*26 27*/new Vector2 (  5.03f ,  4.5f  ) , new Vector2 (  5.43f ,  3.86f ) ,
                                             /*28 29*/new Vector2 ( -5.20f ,  3.49f ) , new Vector2 ( -5.20f ,  3.49f ) ,
                                             /*30 33*/new Vector2 ( -5.20f ,  3.49f ) , new Vector2 ( -5.20f ,  3.47f ) ,
                                             /*32 33*/new Vector2 ( -5.20f ,  3.49f ) , new Vector2 ( -8.63f , -2.10f ) };
    #endregion

    Sprite [] render;

    private void OnDestroy ()
    {
        SceneManager.activeSceneChanged -= ChangedActiveScene;
    }
    private void ChangedActiveScene ( Scene current , Scene next )
    {
        Invoke ( "CreateExpansion" , 0.2f );
    }

    void CreateExpansion ()
    {
        int [] completeID = GameLoopManager.instance.pm.storeExpansionUnlock.GetAllCompleteID ();
        GameLoopManager.instance.nextShift.CreatExpansion ( completeID );
    }

    public void CreateItem ( int id , GameDataManager gm )
    {
        int spriteGroup = gm.storeExpansionList.Data [id].spriteLevel;
        SpriteRenderer obj;
        if ( spriteGroup != 0 )
        {
            if ( Renderer.ContainsKey ( spriteGroup ) )
            {
                obj = Renderer [spriteGroup];
            }
            else
            {
                obj = Instantiate ( Resources.Load<SpriteRenderer> ( "Prefabs/" + path + "StoreExpland" ) , transform );
                Renderer.Add ( spriteGroup , obj );
            }
        }
        else
        {
            obj = Instantiate ( Resources.Load<SpriteRenderer> ( "Prefabs/" + path + "StoreExpland" ) , transform );
        }
        obj.sprite = render [id];
        Color tempColor = obj.color;
        tempColor.a = 0;
        obj.color = tempColor;
        obj.DOFade ( 1 , 1f );
        Vector2 newPos = gm.storeExpansionList.Data [id].spritePosition;
        int order = gm.storeExpansionList.Data [id].spriteOrder;
        obj.transform.localPosition = new Vector3 ( newPos.x , newPos.y , -order );
        obj.name = "Expansion_" + id.ToString ();
    }
}
