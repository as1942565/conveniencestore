﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Prefab
{
    public class StoreEventIcon : MonoBehaviour
    {
        private void OnMouseDown ()
        {
            if ( UI.MainPanel.self.Shield.activeSelf )
                return;

            StoreEventManager.self.PlayStory ();
            Destroy ( this.gameObject , 0.5f );
            StoreEventManager.self.Icon = null;
            StoreEventManager.self.isEvent = false;
        }
    }
}