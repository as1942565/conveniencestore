﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Observer;

public class LevelManager : BaseObserver
{
    private const string path = "Background/";
    private SpriteRenderer [] Render;

    List<SpriteRenderer> renders = new List<SpriteRenderer> ();

    public void ChangeShift ( int level , int shift , GameDataManager gm )
    {
        if ( level >= 3 )
            level = 2;
        Vector2 [] pos = gm.levelList.Data [level].pos;
        int [] orders = gm.levelList.Data [level].order;
        Sprite [] sprite = Resources.LoadAll<Sprite> ( $"Sprites/Common/{path}Shift{shift}/Level{level}" );
        if ( sprite == null )
        {
            Debug.LogError ( $"Sprites/{path}{shift}/Level{level}" + "路徑不存在" );
            return;
        }
        for ( int i = 0; i < sprite.Length; i++ )
        {
            if ( i >= renders.Count )
            {
                SpriteRenderer spriteRender = Instantiate ( Resources.Load<SpriteRenderer> ( "Prefabs/" + path + "StoreExpland" ) , transform );
                renders.Add ( spriteRender );
            }
            if ( renders [i] == null )
                return;
            renders [i].sprite = sprite [i];
            renders [i].name = $"{level}-{shift}";

            Vector3 newPos = new Vector3 ( pos [i].x , pos [i].y , -orders [i] );
            renders [i].transform.localPosition = newPos;
        }
        for ( int i = renders.Count - 1; i > sprite.Length; i-- )
        {
            Destroy ( renders[i].gameObject );
            renders.RemoveAt ( i );
        }
    }

    protected override void RefreshComponent ( PlayerDataManager pm )
    {
        ChangeShift ( pm.businessUnlock.GetLevel () , (int)pm.calender.GetShift () , manager.gm );
    }

    protected override void RegisterObserver ( PlayerDataManager pm )
    {
        
    }

    protected override void UnregisterObserver ( PlayerDataManager pm )
    {
    }
}
