﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Tools;

public class ChangeDay : MonoBehaviour
{
    public static ChangeDay self;
    private void Awake ()
    {
        self = this;
    }

    public Image image;
    public Image num;
    public Image background;

    [HideInInspector]
    public bool playAnim;

    private string path = "Sprites/Common/Background/ChangeDay/";
    private int id;
    private List<int> ids = new List<int> ();
    private float [] CD = new float [] { 0.1f , 0.05f , 0.1f };
    int employeeID;
    Sprite [] sprs;
    Sprite [] numSprs;

    private void Start ()
    {
        numSprs = Resources.LoadAll<Sprite> ( path + "DayNum" );

        ids = new List<int> ();
        ids.Add ( 0 );
        ids.Add ( 1 );
        ids.Add ( 2 );
        ids.Shuffle ();
    }

    public void Loading ()
    {
        playAnim = true;
        employeeID = ids [id];
        id++;
        if ( id >= ids.Count )
        {
            id = 0;
            ids.Shuffle ();
        }
        sprs = Resources.LoadAll<Sprite> ( path + employeeID );
        
        StartCoroutine ( Create () );
    }

    float duration = 1f;
    IEnumerator Create ()
    {
        int day = GameLoopManager.instance.pm.calender.GetDay ();
        num.sprite = numSprs [day];

        image.DOFade ( 1 , 0.01f );
        background.transform.DOLocalMoveX ( 450 , 0.01f );
        num.transform.DOLocalMoveY ( 335 , 0.01f );

        yield return new WaitForSeconds ( 0.01f );
        for ( int i = 0; i < sprs.Length; i++ )
        {
            image.sprite = sprs [i];
            yield return new WaitForSeconds ( CD [employeeID] );
            if ( i + 5 ==sprs.Length )
            {
                background.DOFade ( 1 , duration );
                background.transform.DOLocalMoveX ( 490 , duration );
            }
        }
        num.DOFade ( 1 , duration );
        num.transform.DOLocalMoveY ( 210 , duration );
        yield return new WaitForSeconds ( duration + 0.5f );

        image.DOFade ( 0 , 0.5f );
        background.DOFade ( 0 , 0.5f );
        num.DOFade ( 0 , 0.5f );

        yield return new WaitForSeconds ( 0.5f );
        playAnim = false;
    }
}
