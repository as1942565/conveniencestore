﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoreEventManager : MonoBehaviour
{
    [HideInInspector]
    public bool isEvent;
    [HideInInspector]
    public SpriteRenderer Icon;

    public static StoreEventManager self;
    private void Awake ()
    {
        self = this;
        NextShift.StoreEvent.OnStoreEventRun += ShowEvent;
    }
    private void OnDestroy ()
    {
        NextShift.StoreEvent.OnStoreEventRun -= ShowEvent;
    }

    private const string path = "Prefabs/Background/StoreEventIcon";

    private int employeeID;
    private int expansionID;
    private bool niceEvent;
    public void ShowEvent ( int employeeID , int expansionID , bool niceEvent )
    {
        if ( isEvent )
            return;
        this.employeeID = employeeID;
        this.expansionID = expansionID;
        this.niceEvent = niceEvent;

        SpriteRenderer [] expansion = GetComponentsInChildren<SpriteRenderer> ();
        for ( int i = 0; i < expansion.Length; i++ )
        {
            if ( expansion [i].name.Split ( '_' ) [1] == expansionID.ToString () )
            {
                SpriteRenderer button = Instantiate ( Resources.Load<SpriteRenderer> ( path ) , expansion [i].transform );
                button.transform.localPosition = Vector3.zero;
                Icon = button;
                isEvent = true;
                break;
            }
        }
    }
    string dramaPath;
    public void PlayStory ()
    {
        if ( StoryManager.self != null )
        {
            TextAsset temp = null;
            string eventType = niceEvent ? "Good" : "Bad";
            string language = GameLoopManager.instance.om.optionData.GetLanguageStr ();
            dramaPath = $"Data/Story/{language}Expansion/{eventType}/Employee{employeeID}_Expansion{expansionID}";
            temp = Resources.Load<TextAsset> ( dramaPath );
            if ( temp == null )
            {
                Invoke ( "DoEvent" , 1 );
                LoadingScene.self.Transitions ( 0.5f , 1 , false );
                return;
            }
            StoryManager.self.ReadExpantionDrama = true;
            StartCoroutine ( "WaitDramaEnd" );
            LoadingScene.self.Transitions ( 0.5f , 1f , false );
        }
    }

    IEnumerator WaitDramaEnd ()
    {
        yield return new WaitForSeconds ( 0.5f );
        StoryManager.self.StartStory ( "" , dramaPath );
        while ( StoryManager.self.ReadExpantionDrama == true )
        {
            yield return null;
        }
        Invoke ( "DoEvent" , 0.5f );
    }

    void DoEvent ()
    {
        string name = GameLoopManager.instance.gm.storeExpansionList.Data [expansionID].name;
        if ( niceEvent == false )
        {
            UI.MessageBox.Notice ( "似乎造成客人的困擾。" );
            return;
        }
        else
            UI.MessageBox.Notice ( "深受客人的喜愛。" );

        StoreEventBaseEvent [] e = GameLoopManager.instance.gm.storeEventList.Data.CreateEvents ( expansionID );
        if ( e == null )
        {
            Debug.LogError ( expansionID + "超過事件長度" );
            return;
        }
        for ( int i = 0; i < e.Length; i++ )
        {
            string [] param = e [i].GetParam ();
            if ( e [i].NeedAdditionalParam () )
            {
                param = new string [] { employeeID.ToString () , e [i].GetParam () [0] };
            }
            e [i].DoEvent ( GameLoopManager.instance.pm , GameLoopManager.instance.gm , param );
        }
    }
}
