﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

public class ShowScript : Editor
{
    [MenuItem ( "ShowScript/People" , priority = 0 )]
    public static void People ()
    {
        string path = "Assets/Scripts/GameUI/People/0";
        Object obj = AssetDatabase.LoadAssetAtPath ( path , typeof ( Object ) );
        EditorUtility.FocusProjectWindow ();
        Selection.activeObject = obj;
    }

    [MenuItem ( "ShowScript/Drama" , priority = 1 )]
    public static void Drama ()
    {
        string path = "Assets/Scripts/Drama/Event/Create";
        Object obj = AssetDatabase.LoadAssetAtPath ( path , typeof ( Object ) );
        EditorUtility.FocusProjectWindow ();
        Selection.activeObject = obj;
    }
}
#endif
