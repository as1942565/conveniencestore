﻿using GameData;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

public class AssetHandler
{
    [OnOpenAsset()]
    public static bool OpenActivityList(int instanceId, int line)
    {
        ActivityList obj = EditorUtility.InstanceIDToObject(instanceId) as ActivityList;
        if (obj != null)
        {
            ActivityListEditorWindow.Open(obj);
            return true;
        }
        return false;
    }
    [OnOpenAsset()]
    public static bool OpenBusinessList(int instanceId, int line)
    {
        BusinessList obj = EditorUtility.InstanceIDToObject(instanceId) as BusinessList;
        if (obj != null)
        {
            BusinessListEditorWindow.Open(obj);
            return true;
        }
        return false;
    }
    [OnOpenAsset()]
    public static bool OpenCommodityList(int instanceId, int line)
    {
        CommodityList obj = EditorUtility.InstanceIDToObject(instanceId) as CommodityList;
        if (obj != null)
        {
            CommodityListEditorWindow.Open(obj);
            return true;
        }
        return false;
    }

    [OnOpenAsset()]
    public static bool OpenWorkerList(int instanceId, int line)
    {
        WorkerList obj = EditorUtility.InstanceIDToObject(instanceId) as WorkerList;
        if (obj != null)
        {
            WorkerListEditorWindow.Open(obj);
            return true;
        }
        return false;
    }
    [OnOpenAsset()]
    public static bool OpenStoreExpansionList(int instanceId, int line)
    {
        StoreExpansionList obj = EditorUtility.InstanceIDToObject(instanceId) as StoreExpansionList;
        if (obj != null)
        {
            StoreExpansionListEditorWindow.Open(obj);
            return true;
        }
        return false;
    }
}