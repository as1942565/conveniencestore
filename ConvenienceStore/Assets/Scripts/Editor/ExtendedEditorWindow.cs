﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Threading;
using UnityEditorInternal;

public class ExtendedEditorWindow : EditorWindow
{
    protected SerializedObject serializedObject;
    protected SerializedProperty currentProperty;
    private string selectedPropertyPath;
    protected SerializedProperty selectedProperty;

    protected void DrawProperties(SerializedProperty prop, bool drawChildren, bool autoExtend =false)
    {
        string lastPropPath = string.Empty;
        foreach (SerializedProperty p in prop)
        {
            if ((p.isArray) && p.propertyType == SerializedPropertyType.Generic)
            {
                //Debug.Log(p.propertyPath);
                if (!string.IsNullOrEmpty(lastPropPath) && p.propertyPath.Contains(lastPropPath)) { continue; }
                EditorGUILayout.BeginHorizontal();
                p.isExpanded = EditorGUILayout.Foldout(p.isExpanded, p.displayName);
                lastPropPath = p.propertyPath;
                EditorGUILayout.EndHorizontal();
                if (p.isExpanded || autoExtend)
                {
                    EditorGUI.indentLevel++;
                    DrawArray(p, drawChildren);
                    EditorGUI.indentLevel--;
                }
            }
            else if (p.propertyType == SerializedPropertyType.ArraySize)
            {
                continue;
            }
            else
            {
                if (!string.IsNullOrEmpty(lastPropPath) && p.propertyPath.Contains(lastPropPath)) { continue; }
                lastPropPath = p.propertyPath;
                EditorGUILayout.PropertyField(p, true);
            }

        }
    }

    protected void DrawArray(SerializedProperty prop, bool drawChildren)
    {
        EditorGUILayout.LabelField(prop.displayName);
        EditorGUI.indentLevel++;
        //EditorGUILayout.PropertyField(prop, false);
        SerializedProperty arr = prop.FindPropertyRelative("Array");
        SerializedProperty size = arr.FindPropertyRelative("size");
        float labelwidth = EditorGUIUtility.labelWidth;
        EditorGUIUtility.labelWidth = 50;
        EditorGUILayout.PropertyField(size, true,GUILayout.Width(100));
        EditorGUIUtility.labelWidth = labelwidth;
        DrawProperties(arr, drawChildren, true);
        EditorGUI.indentLevel--;
    }
    protected void DrawBoolArray(SerializedProperty prop, bool drawChildren)
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(prop.displayName,GUILayout.Width(150));
        SerializedProperty size = prop.FindPropertyRelative("Array.size");
        EditorGUILayout.PropertyField(size, GUIContent.none, GUILayout.Width(30));
        int length = size.intValue;
        for(int i = 0; i < length; i++)
        {
            EditorGUILayout.PropertyField(prop.FindPropertyRelative($"Array.data[{i}]"), GUIContent.none,GUILayout.Width(15));
        }
        EditorGUILayout.EndHorizontal();
    }

    protected void DrawInlineArray(SerializedProperty prop, bool drawChildren)
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(prop.displayName, GUILayout.Width(150));
        SerializedProperty size = prop.FindPropertyRelative("Array.size");
        EditorGUILayout.LabelField("Size", GUILayout.Width(50));
        EditorGUILayout.PropertyField(size, GUIContent.none, GUILayout.Width(30));
        int length = size.intValue;
        EditorGUILayout.LabelField("Value:", GUILayout.Width(50));
        for (int i = 0; i < length; i++)
        {
            EditorGUILayout.PropertyField(prop.FindPropertyRelative($"Array.data[{i}]"), GUIContent.none, GUILayout.Width(40));
        }
        EditorGUILayout.EndHorizontal();
    }


    protected virtual void OnSelectChanged() { }


    Vector2 sidebarScroll = Vector2.zero;
    protected virtual void DrawSideBar(SerializedProperty prop)
    {
        EditorGUILayout.BeginVertical("box", GUILayout.MaxWidth(300), GUILayout.ExpandHeight(true));
        sidebarScroll = EditorGUILayout.BeginScrollView(sidebarScroll, GUILayout.MaxWidth(310), GUILayout.ExpandHeight(true));
        int i = 0;
        int removeIndex = -1;
        foreach (SerializedProperty p in prop)
        {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button($"{i}:{p.displayName}"))
            {
                selectedPropertyPath = p.propertyPath;
                selectedProperty = serializedObject.FindProperty(selectedPropertyPath);
                OnSelectChanged();
            }
            var oldColor = GUI.backgroundColor;
            GUI.backgroundColor = Color.green;
            if ( GUILayout.Button ( $"+" , GUILayout.Width ( 18 ) ) )
            {
                prop.InsertArrayElementAtIndex ( i );
            }
            GUI.backgroundColor = Color.red;
            if (GUILayout.Button($"x",GUILayout.Width(18)))
            {
                removeIndex = i;
            }
            GUI.backgroundColor = oldColor;
            EditorGUILayout.EndHorizontal();
            i++;
        }
        if(removeIndex != -1)
        {
            prop.DeleteArrayElementAtIndex(removeIndex);
        }
        if (!string.IsNullOrEmpty(selectedPropertyPath))
        {
            selectedProperty = serializedObject.FindProperty(selectedPropertyPath);
        }
        if ( GUILayout.Button ( "Add" ) )
            prop.FindPropertyRelative ( "Array" ).arraySize += 1;
        if ( GUILayout.Button ( "Remove" ) )
            prop.FindPropertyRelative ( "Array" ).arraySize -= 1;
        EditorGUILayout.EndScrollView();
        EditorGUILayout.EndVertical();
    }

    protected void DrawField(string propName , bool relative, bool inline = false)
    {
        SerializedProperty prop = null;
        if(relative && currentProperty!= null)
        {
            prop = currentProperty.FindPropertyRelative(propName);
        }
        else if (serializedObject != null)
        {
            prop = serializedObject.FindProperty(propName);
        }
        if(prop == null)
        {
            return; 
        }
        if (prop.isArray && prop.propertyType == SerializedPropertyType.Generic)
        {
            if (inline)
            {
                DrawInlineArray(prop, true);
            }
            else
            {
                if (prop.arrayElementType == "bool")
                {
                    DrawBoolArray(prop, true);
                }
                else
                {
                    DrawArray(prop, true);
                }
            }
        }
        else
        {
            EditorGUILayout.PropertyField(prop, true);
        }
    }
    protected void DrawSelectedField(string propName, bool relative)
    {
        if (relative && currentProperty != null)
        {
            Debug.Log(currentProperty.propertyPath);
            Debug.Log(currentProperty.FindPropertyRelative(propName));
            EditorGUILayout.PropertyField(currentProperty.FindPropertyRelative(propName), true);
        }
        else if (serializedObject != null)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty(propName), true);
        }
    }

    protected void Apply()
    {
        serializedObject.ApplyModifiedProperties();
    }

    protected static ReorderableList.HeaderCallbackDelegate DrawHeaderCallback(string header)
    {
        return (Rect r) =>
        {
            GUI.Label(r, header);
        };
    }

    protected static ReorderableList.ElementCallbackDelegate DrawElementCallback(SerializedProperty p)
    {
        return (Rect rect, int index, bool active, bool focused) =>
        {
            EditorGUI.PropertyField(rect, p.GetArrayElementAtIndex(index));
        };
    }
}
