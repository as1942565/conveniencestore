﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

public class ShowFolder : Editor
{
    [MenuItem ( "ShowWindow/Animation" , priority = 0 )]
    public static void Animation ()
    {
        string path = "Assets/Animations/People/0";
        Object obj = AssetDatabase.LoadAssetAtPath ( path , typeof ( Object ) );
        EditorUtility.FocusProjectWindow ();
        Selection.activeObject = obj;
    }

    [MenuItem ( "ShowWindow/PeoplePrefab" , priority = 1 )]
    public static void PeoplePrefab ()
    {
        string path = "Assets/Resources/Prefabs/People/0";
        Object obj = AssetDatabase.LoadAssetAtPath ( path , typeof ( Object ) );
        EditorUtility.FocusProjectWindow ();
        Selection.activeObject = obj;
    }

    [MenuItem ( "ShowWindow/ScriptableObject" , priority = 2 )]
    public static void ScriptableObject ()
    {
        string path = "Assets/Resources/Data/ScriptableObject/0";
        Object obj = AssetDatabase.LoadAssetAtPath ( path , typeof ( Object ) );
        EditorUtility.FocusProjectWindow ();
        Selection.activeObject = obj;
    }
}
#endif
