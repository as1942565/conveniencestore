﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using GameData;
using Tools;
using System;
using UnityEditorInternal;
//使用Editor的功能
[CustomEditor(typeof(BusinessList))]
public class BusinessListEditor : ScriptableObjectCustomEditor
{                                                                       //
    public override void OpenWindow()
    {
        BusinessListEditorWindow.Open((BusinessList)target);
    }

    #region ReadFromCSV
    private TextAsset csvFile = null;

    [MenuItem("Assets/Create/Data/BusinessList", priority = 4)]     //建立專屬選項
    private static void CreateMyAsset()                                //建立ScriptableObject
    {
        GameData.BusinessList asset = ScriptableObject.CreateInstance<GameData.BusinessList>();//生產ScriptableObject
        AssetDatabase.CreateAsset(asset, "Assets/Resources/" + GameData.BusinessList.resourcesPath + ".asset");//將生產的檔案放到指定路徑
        AssetDatabase.SaveAssets();                                    //將記憶體資料存置硬碟中
        EditorUtility.FocusProjectWindow();                            //顯示Project視窗
        Selection.activeObject = asset;                                 //選擇建立好的檔案
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GameData.BusinessList dataList = (GameData.BusinessList)target;                   //取得選擇的物件的BusinessList
        csvFile = (TextAsset)EditorGUILayout.ObjectField("CSV File", csvFile, typeof(TextAsset), false);
        if (csvFile != null)
        {
            if (GUILayout.Button("Read From CSV"))                 //如果點擊Read From CSV按鈕
            {
                ReadFromCSV(dataList, csvFile);                     //從CSV讀檔
            }
            EditorUtility.SetDirty(dataList);                        //將記憶體資料存置硬碟中
        }
    }

    private static void ReadFromCSV(GameData.BusinessList dataList, TextAsset csvFile)//從CSV讀檔
    {
        string[] businessData = csvFile.text.Split('\n');           //匯入CSV 空格分割
        dataList.Data = new Business[businessData.Length - 1];         //開啟適當陣列大小 (第一排不算所以-1)
        for (int i = 1; i < businessData.Length; i++)
        {
            string[] csv = businessData[i].Split(',');             //逗號分割
            dataList.Data[i - 1].name = csv[0];                       //傳送業務名稱
            dataList.Data[i - 1].eventType = (Business.EventType)int.Parse(csv[1]);//傳送業務種類
            dataList.Data[i - 1].cost = int.Parse(csv[2]);         //傳送業務費用
            dataList.Data[i - 1].effectText = csv[3];                 //傳送業務效果
            dataList.Data[i - 1].unlockRequirement = csv[4].Replace('-', '\n');//傳送解鎖條件文字

            string[] allUnlock = csv[5].Split('-');
            bool[] unlock = new bool[allUnlock.Length];
            for (int j = 0; j < unlock.Length; j++)
            {
                unlock[j] = bool.Parse(allUnlock[j]);
            }
            dataList.Data[i - 1].unlock = unlock;                      //傳送解鎖狀態
            dataList.Data[i - 1].groupID = int.Parse(csv[10]);

            if (csv[6] != "")
            {
                string[] eventClass = csv[6].Split('^');
                string[] eParam = csv[7].Split('^');
                dataList.Data[i - 1].eventData = new List<BusinessEventData>();
                if (eventClass.Length != eParam.Length)
                {
                    Debug.LogError("Event數量和變數數量不一樣");
                    continue;
                }
                for (int j = 0; j < eventClass.Length; j++)
                {
                    BusinessEventData.EventType eventType = BusinessEventData.EventType.AddIncomeB;
                    try
                    {
                        eventType = (BusinessEventData.EventType)System.Enum.Parse(typeof(BusinessEventData.EventType), eventClass[j]);
                    }
                    catch
                    {
                        Debug.LogError($"{eventClass[j]} 不是店長業務事件Enum");
                    }
                    //Debug.Log(eventType.ToString());
                    Type t = Type.GetType($"BusinessEvent.{eventType},Assembly-CSharp");// + eventType.ToString());   //將得到的字串變成腳本型態
                    if (t == null)
                    {
                        Debug.LogError($"{eventClass[j]} 不是店長業務事件");
                        continue;
                    }
                    BusinessBaseEvent e = (BusinessBaseEvent)Activator.CreateInstance(t);//強制將腳本型態轉成員工活動事件並且記錄下來
                    int plength = e.ParamLength();
                    string[] param = new string[0];
                    if (plength != 0)
                    {
                        param = eParam[j].Split('-');
                        if (plength != param.Length)
                        {
                            Debug.LogError($"事件 : {eventClass[j]} 的參數應該是 {plength} 但是只取得 {param.Length} 個 ， {eParam[j]}");
                        }
                    }
                    BusinessEventData eventdata = new BusinessEventData() { Event = eventType, Param = param };
                    dataList.Data[i - 1].eventData.Add(eventdata);
                }
            }
            else
            {
                dataList.Data[i - 1].eventData = new List<BusinessEventData>();
            }
            if (csv[8] != "")
            {
                string[] conditionClass = csv[8].Split('^');
                string[] conditionParam = csv[9].Split('^');
                dataList.Data[i - 1].unlockCondition = new List<UnlockConditionData>();
                if (conditionClass.Length != conditionParam.Length)
                {
                    Debug.LogError("condition數量和變數數量不一樣");
                    continue;
                }
                for (int j = 0; j < conditionClass.Length; j++)
                {
                    UnlockConditionData.ConditionType conditionType = UnlockConditionData.ConditionType.AllMainRelationB;
                    try
                    {
                        conditionType = (UnlockConditionData.ConditionType)System.Enum.Parse(typeof(UnlockConditionData.ConditionType), conditionClass[j]);
                    }
                    catch
                    {
                        Debug.LogError($"{conditionClass[j]} 不是解鎖條件Enum");
                    }
                    Type t = Type.GetType($"Condition.{conditionType},Assembly-CSharp");// + eventType.ToString());   //將得到的字串變成腳本型態
                    if (t == null)
                    {
                        Debug.LogError($"{conditionClass[j]} 不是解鎖條件");
                        continue;
                    }
                    string[] param = conditionParam[j].Split('-');
                    BaseUnlockCondition e = (BaseUnlockCondition)Activator.CreateInstance(t, param, 0);//強制將腳本型態轉成員工活動事件並且記錄下來
                    int plength = e.ParamLength();
                    if (plength != param.Length)
                    {
                        Debug.LogError($"解鎖條件 : {conditionClass[j]} 的參數應該是 {plength} 但是只取得 {param.Length} 個 ， {conditionParam[j]}");
                    }
                    UnlockConditionData eventdata = new UnlockConditionData() { Condition = conditionType, Param = param };
                    dataList.Data[i - 1].unlockCondition.Add(eventdata);
                }
            }
            else
            {
                dataList.Data[i - 1].unlockCondition = new List<UnlockConditionData>();
            }
        }
    }
    #endregion

}


public class BusinessListEditorWindow : ScriptableObjectCustomEditorWindow
{
    public static void Open(BusinessList d)
    {
        BusinessListEditorWindow window = GetWindow<BusinessListEditorWindow>();
        window.serializedObject = new SerializedObject(d);
        window.Show();
    }
    private ReorderableList unlockList;
    private ReorderableList eventList;

    protected override void OnSelectChanged()
    {
        base.OnSelectChanged();
        var eventListProperty = selectedProperty.FindPropertyRelative("eventData");
        eventList = new ReorderableList(serializedObject, eventListProperty, true, true, true, true);
        eventList.drawElementCallback = DrawElementCallback(eventListProperty);
        eventList.drawHeaderCallback = DrawHeaderCallback("Events");
        var unlockListProperty = selectedProperty.FindPropertyRelative("unlockCondition");
        unlockList = new ReorderableList(serializedObject, unlockListProperty, true, true, true, true);
        unlockList.drawElementCallback = DrawElementCallback(unlockListProperty);
        unlockList.drawHeaderCallback = DrawHeaderCallback("Unlock Conditions");
    }

    protected override void DrawSelectedPropertiesPanel()
    {
        currentProperty = selectedProperty;
        EditorGUILayout.BeginVertical("box");
        DrawField("name", true);
        DrawField("eventType", true);
        DrawField("cost", true);
        DrawField("effectText", true);
        DrawField("unlockRequirement", true);
        DrawField("groupID", true);
        DrawField("unlock", true);
        if (eventList != null) { eventList.DoLayoutList(); }
        if (unlockList != null) { unlockList.DoLayoutList(); }
        EditorGUILayout.EndVertical();
        //base.DrawSelectedPropertiesPanel();
    }

}