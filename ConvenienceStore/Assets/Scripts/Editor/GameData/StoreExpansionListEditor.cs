﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;
using GameData;
using Tools;
using System;
using UnityEditorInternal;

//使用Editor的功能
[CustomEditor ( typeof ( StoreExpansionList ) )]
public class StoreExpansionListEditor : ScriptableObjectCustomEditor
{                                                                       //
    private TextAsset csvFile;
    [MenuItem ( "Assets/Create/Data/StoreExpansionList" , priority = 2 )]//建立專屬選項    
    public static void CreateMyAsset ()                                 //建立ScriptableObject
    {
        StoreExpansionList asset = ScriptableObject.CreateInstance<StoreExpansionList> ();//生產ScriptableObject
        AssetDatabase.CreateAsset ( asset , "Assets/Resources/" + StoreExpansionList.resourcesPath + ".asset" );//將生產的檔案放到指定路徑
        AssetDatabase.SaveAssets ();                                    //將記憶體資料存置硬碟中
        EditorUtility.FocusProjectWindow ();                            //顯示Project視窗
        Selection.activeObject = asset;                                 //選擇建立好的檔案
    }

    public override void OnInspectorGUI ()
    {
        base.OnInspectorGUI ();
        StoreExpansionList dataList = (StoreExpansionList)target;       //取得選擇的物件的StoreDataList
        //如果點擊Read From CSV按鈕
        csvFile = (TextAsset)EditorGUILayout.ObjectField ( "CSV File" , csvFile , typeof ( TextAsset ) , false );
        if ( csvFile != null )
        {
            if ( GUILayout.Button ( "Read From CSV" ) )                 //如果點擊Read From CSV按鈕
            {
                ReadFromCSV ( dataList , csvFile );                     //從CSV讀檔
            }
            EditorUtility.SetDirty ( dataList );                        //將記憶體資料存置硬碟中
        }
    }

    private static void ReadFromCSV ( StoreExpansionList dataList , TextAsset csvFile )
    {
        string [] storeData = csvFile.text.Split ( '\n' );              //匯入CSV
        dataList.Data = new StoreExpansion [storeData.Length - 1];      //開啟適當陣列大小 (第一排不算所以-1)
        for ( int i = 1; i < storeData.Length; i++ )
        {
            string [] csv = storeData [i].Split ( ',' );                //逗號分割
            dataList.Data [i - 1].name = csv [0];                       //傳送擴建名稱

            string [] allType = csv [1].Split ( '-' );
            dataList.Data [i - 1].type = new List<StoreExpansion.ExpansionType> ();
            for ( int j = 0; j < allType.Length; j++ )
            {
                dataList.Data [i - 1].type.Add ( (StoreExpansion.ExpansionType)int.Parse ( allType [j] ) );
            }

            dataList.Data [i - 1].skill = csv [2].Replace ( '*' , ',' ).Replace ( '^' , '\n' );//傳送擴建功能
            dataList.Data [i - 1].cost = int.Parse ( csv [3] );        //傳送擴建費用
            dataList.Data [i - 1].explanation = csv [4].Replace ( '*' , ',' );//傳送擴建描述
            dataList.Data [i - 1].unlock = bool.Parse ( csv [5] );      //傳送解所狀態
            dataList.Data [i - 1].storeLevel = int.Parse ( csv [6] );    //傳送還債前置



            if ( csv [7] != "" )
            {
                string [] eventClass = csv [7].Split ( '^' );
                string [] eParam = csv [8].Split ( '^' );
                dataList.Data [i - 1].eventData = new List<StoreExpansionEventData> ();
                if ( eventClass.Length != eParam.Length )
                {
                    Debug.LogError ( "Event數量和變數數量不一樣" );
                    continue;
                }
                for ( int j = 0; j < eventClass.Length; j++ )
                {
                    StoreExpansionEventData.EventType eventType = StoreExpansionEventData.EventType.ActivityCleanSE;
                    try
                    {
                        eventType = (StoreExpansionEventData.EventType)System.Enum.Parse ( typeof ( StoreExpansionEventData.EventType ) , eventClass [j] );
                    }
                    catch
                    {
                        Debug.LogError ( $"{eventClass [j]} 不是店面擴張事件Enum" );
                    }
                    //Debug.Log(eventType.ToString());
                    Type t = Type.GetType ( $"StoreExpansionEvent.{eventType},Assembly-CSharp" );// + eventType.ToString());   //將得到的字串變成腳本型態
                    if ( t == null )
                    {
                        Debug.LogError ( $"{eventClass [j]} 不是店面擴張事件" );
                        continue;
                    }
                    StoreExpansionBaseEvent e = (StoreExpansionBaseEvent)Activator.CreateInstance ( t );//強制將腳本型態轉成員工活動事件並且記錄下來
                    int plength = e.ParamLength ();
                    string [] param = new string [0];
                    if ( plength != 0 )
                    {
                        param = eParam [j].Split ( '-' );
                        if ( plength != param.Length )
                        {
                            Debug.LogError ( $"事件 : {eventClass [j]} 的參數應該是 {plength} 但是只取得 {param.Length} 個 ， {eParam [j]}" );
                        }
                    }
                    StoreExpansionEventData eventdata = new StoreExpansionEventData () { Event = eventType , Param = param };
                    dataList.Data [i - 1].eventData.Add ( eventdata );
                }
            }
            else
            {
                dataList.Data [i - 1].eventData = new List<StoreExpansionEventData> ();
            }


            /*if (csv[9] != "")
            {
                string[] conditionClass = csv[9].Split('^');
                string[] conditionParam = csv[10].Split('^');
                dataList.Data[i - 1].unlockCondition = new List<UnlockConditionData>();
                if (conditionClass.Length != conditionParam.Length)
                {
                    Debug.LogError("condition數量和變數數量不一樣");
                    continue;
                }
                for (int j = 0; j < conditionClass.Length; j++)
                {
                    UnlockConditionData.ConditionType conditionType = UnlockConditionData.ConditionType.AllMainRelationB;
                    try
                    {
                        conditionType = (UnlockConditionData.ConditionType)System.Enum.Parse(typeof(UnlockConditionData.ConditionType), conditionClass[j]);
                    }
                    catch
                    {
                        Debug.LogError($"{conditionClass[j]} 不是解鎖條件Enum");
                    }
                    Type t = Type.GetType($"Condition.{conditionType},Assembly-CSharp");// + eventType.ToString());   //將得到的字串變成腳本型態
                    if (t == null)
                    {
                        Debug.LogError($"{conditionClass[j]} 不是解鎖條件");
                        continue;
                    }
                    string[] param = conditionParam[j].Split('-');
                    BaseUnlockCondition e = (BaseUnlockCondition)Activator.CreateInstance(t, param, 0);//強制將腳本型態轉成員工活動事件並且記錄下來
                    int plength = e.ParamLength();
                    if (plength != param.Length)
                    {
                        Debug.LogError($"解鎖條件 : {conditionClass[j]} 的參數應該是 {plength} 但是只取得 {param.Length} 個 ， {conditionParam[j]}");
                    }
                    UnlockConditionData eventdata = new UnlockConditionData() { Condition = conditionType, Param = param };
                    dataList.Data[i - 1].unlockCondition.Add(eventdata);
                }
            }
            else
            {
                dataList.Data[i - 1].unlockCondition = new List<UnlockConditionData>();
            }*/
        }
    }

    public override void OpenWindow ()
    {
        StoreExpansionListEditorWindow.Open ( (StoreExpansionList)target );
    }
}



public class StoreExpansionListEditorWindow : ScriptableObjectCustomEditorWindow
{
    public static void Open ( StoreExpansionList d )
    {
        StoreExpansionListEditorWindow window = GetWindow<StoreExpansionListEditorWindow> ();
        window.serializedObject = new SerializedObject ( d );
        window.Show ();
    }
    private ReorderableList unlockList;
    private ReorderableList eventList;
    private ReorderableList typeList;


    protected override void OnSelectChanged ()
    {
        base.OnSelectChanged ();
        var eventListProperty = selectedProperty.FindPropertyRelative ( "eventData" );
        eventList = new ReorderableList ( serializedObject , eventListProperty , true , true , true , true );
        eventList.drawElementCallback = DrawElementCallback ( eventListProperty );
        eventList.drawHeaderCallback = DrawHeaderCallback ( "Events" );
        var unlockListProperty = selectedProperty.FindPropertyRelative ( "unlockCondition" );
        unlockList = new ReorderableList ( serializedObject , unlockListProperty , true , true , true , true );
        unlockList.drawElementCallback = DrawElementCallback ( unlockListProperty );
        unlockList.drawHeaderCallback = DrawHeaderCallback ( "Unlock Conditions" );
        var typeListProperty = selectedProperty.FindPropertyRelative ( "type" );
        typeList = new ReorderableList ( serializedObject , typeListProperty , true , true , true , true );
        typeList.drawElementCallback = DrawElementCallback ( typeListProperty );
        typeList.drawHeaderCallback = DrawHeaderCallback ( "Store ExpansionType" );
    }
    protected override void DrawSelectedPropertiesPanel ()
    {
        int width = 1000;
        currentProperty = selectedProperty;
        EditorGUILayout.BeginVertical ( "box" );
        DrawField ( "name" , true );
        EditorGUILayout.LabelField ( "種類 : 增加收入Income , 增加客源Customer , 提升治安Police , 恢復清潔Clean , 增建設施Construct" , GUILayout.Width ( width ) );
        if ( typeList != null ) { typeList.DoLayoutList (); }
        DrawField ( "skill" , true );
        DrawField ( "cost" , true );
        DrawField ( "explanation" , true );
        DrawField ( "storeLevel" , true );
        DrawField ( "unlock" , true );
        if ( eventList != null ) { eventList.DoLayoutList (); }
        if ( unlockList != null ) { unlockList.DoLayoutList (); }
        DrawField ( "spritePosition" , true );
        DrawField ( "spriteLevel" , true );
        DrawField ( "spriteOrder" , true );
        DrawField ( "sprite" , true );

        EditorGUILayout.EndVertical ();
    }

}