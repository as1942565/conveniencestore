﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using GameData;


[CustomEditor(typeof(WorkerList))]
public class WorkerListEditor : ScriptableObjectCustomEditor
{                                                                       //
    private TextAsset csvFile;

    [MenuItem("Assets/Create/Data/EmployeeDataList", priority = 5)] //建立專屬選項  
    private static void CreateMyAsset()                                //建立ScriptableObject
    {
        WorkerList asset = ScriptableObject.CreateInstance<WorkerList>();//生產ScriptableObject
        AssetDatabase.CreateAsset(asset, "Assets/Resources/" + WorkerList.resourcesPath + ".asset");//將建立的檔案放到指定路徑
        AssetDatabase.SaveAssets();                                    //將記憶體資料存置硬碟
        EditorUtility.FocusProjectWindow();                            //顯示Project視窗
        Selection.activeObject = asset;                                 //選擇建立好的檔案
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        WorkerList dataList = (WorkerList)target;                       //取得的物件的EmployeeDataList
        csvFile = (TextAsset)EditorGUILayout.ObjectField("CSV File", csvFile, typeof(TextAsset), false);
        if (csvFile != null)
        {
            if (GUILayout.Button("Read From CSV"))                 //如果點擊Read From CSV按鈕
            {
                ReadFromCSV(dataList, csvFile);                     //讀取CSV
            }
            EditorUtility.SetDirty(dataList);                        //將記憶體資料存置硬碟
        }
    }

    private static void ReadFromCSV(WorkerList dataList, TextAsset csvFile)
    {                                                                   //
        string[] employee = csvFile.text.Split('\n');               //匯入CSV 空格分割
        dataList.Data = new Worker[employee.Length - 1];               //開啟適當陣列大小(第一排不用所以-1)
        for (int i = 1; i < employee.Length; i++)
        {
            string[] csv = employee[i].Split(',');                 //逗號分割
            dataList.Data[i - 1].id = int.Parse(csv[0]);           //傳送員工編號
            dataList.Data[i - 1].name = csv[1];                       //傳送員工姓名

            string[] allAbility = csv[2].Split('-');
            dataList.Data[i - 1].ability.abilities = new int[allAbility.Length];
            for (int j = 0; j < allAbility.Length; j++)
            {
                dataList.Data[i - 1].ability[j] = int.Parse(allAbility[j]);
            }

            dataList.Data[i - 1].level = int.Parse(csv[3]);        //傳送員工等級
            dataList.Data[i - 1].exp = int.Parse(csv[4]);          //傳送員工經驗值
            bool isMain = bool.Parse(csv[5]);
            dataList.Data[i - 1].isMain = isMain;                      //傳送是否為女主角
            dataList.Data[i - 1].unlock = bool.Parse(csv[6]);       //傳送員工是否解鎖
            if (isMain)
            {
                dataList.Data[i - 1].relationRank = int.Parse(csv[7]);  //傳送員工好感度
                dataList.Data[i - 1].relationEXP = int.Parse(csv[8]);  //傳送員工好感值
            }
            dataList.Data[i - 1].power = int.Parse(csv[9]);         //傳送員工體力值
            dataList.Data[i - 1].initWith = bool.Parse(csv[10]);    //傳送員工是否初次登場
        }
    }

    public override void OpenWindow()
    {
        WorkerListEditorWindow.Open((WorkerList)target);
    }
}


public class WorkerListEditorWindow : ScriptableObjectCustomEditorWindow
{
    public static void Open(WorkerList d)
    {
        WorkerListEditorWindow window = GetWindow<WorkerListEditorWindow>();
        window.serializedObject = new SerializedObject(d);
        window.Show();
    }

    protected override void OnSelectChanged()
    {
        base.OnSelectChanged();
        //reset ID;
      

    }

    protected override void DrawSideBar(SerializedProperty prop)
    {
        base.DrawSideBar(prop);
        int size = prop.FindPropertyRelative("Array").arraySize;
        for (int i = 0; i < size; i++)
        {
            prop.FindPropertyRelative($"Array.data[{i}].id").intValue = i;
        }
        if ( GUILayout.Button ( "Add" ) )
        {
            prop.FindPropertyRelative ( "Array" ).arraySize += 1;
        }
    }

    protected override void DrawSelectedPropertiesPanel()
    {
        currentProperty = selectedProperty;
        EditorGUILayout.BeginVertical("box");
        DrawField("name", true);
        DrawField("level", true);
        DrawField("exp", true);
        DrawField("isMain", true);
        DrawField("unlock", true);
        DrawField("relationRank", true);
        DrawField("relationEXP", true);
        DrawField("power", true);
        DrawField("initWith", true);
        //DrawProperties(currentProperty.FindPropertyRelative("abilitys"), false);
        DrawField("ability", true);
        EditorGUILayout.EndVertical();
        //base.DrawSelectedPropertiesPanel();
    }
}
