﻿using GameData;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

//使用Editor的功能
[CustomEditor(typeof(CommodityList))]
public class CommodityListEditor : ScriptableObjectCustomEditor
{                                                                           //
    private TextAsset csvFile = null;

    [MenuItem("Assets/Create/Data/CommodityList", priority = 0)]        //建立專屬選項
    public static void CreateMyAsset()                                     //建立ScriptableObject
    {
        CommodityList asset = ScriptableObject.CreateInstance<CommodityList>();//生產ScriptableObject
        AssetDatabase.CreateAsset(asset, "Assets/Resources/" + CommodityList.resourcesPath + ".asset");//將生產的檔案放到指定路徑
        AssetDatabase.SaveAssets();                                        //將記憶體資料存置硬碟中
        EditorUtility.FocusProjectWindow();                                //跳出Project視窗
        Selection.activeObject = asset;                                     //選擇建立好的檔案
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        CommodityList dataList = (CommodityList)target;
        csvFile = (TextAsset)EditorGUILayout.ObjectField("Csv File", csvFile, typeof(TextAsset), false);
        if (csvFile != null)
        {
            if (GUILayout.Button("Read From CSV"))                     //如果點擊了"Read From CSV"按鈕
            {
                ReadFromCSV(dataList, csvFile);                         //讀取CSV資料
            }
        }
        EditorUtility.SetDirty(dataList);                                //將記憶體資料存置硬碟中
    }

    private static void ReadFromCSV(CommodityList dataList, TextAsset csvfile)
    {
        string data = csvfile.text;                                         //匯入CSV
        string[] commodity = data.Split('\n');                          //換行分割
        dataList.Data = new Commodity[commodity.Length - 1];               //開啟適當陣列大小 (第一排不算所以-1)
        var sprites = Resources.LoadAll<Sprite> ( "Sprites/Common/GameUI/Commodity/Commodity" );
        for ( int i = 1; i < commodity.Length; i++ )
        {
            dataList.Data [i - 1].icon = sprites [i - 1];
            string [] csv = commodity [i].Split ( ',' );                    //逗號分割
            dataList.Data [i - 1].name = csv [0];                           //傳送商品名稱
            dataList.Data [i - 1].type = (Commodity.CommodityType)int.Parse ( csv [1] );//傳送商品種類  
            dataList.Data [i - 1].rank = int.Parse ( csv [2] );             //傳送商品階級
            dataList.Data [i - 1].cost = int.Parse ( csv [3] );             //傳送商品初始成本
            int star = int.Parse ( csv [4] );                               //取得商品星級
            dataList.Data [i - 1].star = star;                              //傳送商品星級
            string [] allSale = csv [5].Split ( '-' );
            int [] sale = new int [allSale.Length];
            for ( int j = 0; j < sale.Length; j++ )
            {
                sale [j] = int.Parse ( allSale [j] );
            }
            dataList.Data [i - 1].price = sale;                              //傳送所有星級的售價

            string [] allTime = csv [6].Split ( '-' );
            Commodity.PopularAddition [] time = new Commodity.PopularAddition [allTime.Length];
            for ( int j = 0; j < time.Length; j++ )
            {
                time [j] = (Commodity.PopularAddition)int.Parse ( allTime [j] );
            }
            dataList.Data [i - 1].salesRate = time;                              //傳送時段熱賣
            dataList.Data [i - 1].explanation = csv [7].Replace ( "*" , "，" );//傳送商品描述
            dataList.Data [i - 1].baseSaleValue = float.Parse ( csv [8] );  //傳送商品基本銷售數值
            dataList.Data [i - 1].quantity = int.Parse ( csv [9] );         //傳送商品研發初始數量
            dataList.Data [i - 1].unlock = bool.Parse ( csv [10] );          //商品是否被研發

            if ( int.Parse ( csv [1] ) >= 3 )
                continue;

            //dataList.Data [i - 1].ImproveCondition = int.Parse ( csv [11] );//改良前置(販售數量門檻)

            //早班
            string [] M_chance = csv [12].Split ( '-' );                    //new string [] { "100^0^0" , "50^50^0" , "40^30^30" };
            string [] [] M_allChance = new string [M_chance.Length] [];       //M_chance.Length : 3
            for ( int c = 0; c < M_chance.Length; c++ )
            {
                M_allChance [c] = M_chance [c].Split ( '^' );               //new string [][] { new string []{ "100" , "0" , "0" }  , new string []{ "50" , "50" , "0" } , new string []{ "40" , "30" , "30" } };
            }

            dataList.Data [i - 1].MorningChance = new Commodity.UpgradeChance [dataList.Data [i - 1].price.Length - 1];// new IntList [M_allChance.Length]; //建立陣列長度  M_allChance.Length : 3
            for ( int stars = 0; stars < dataList.Data [i - 1].MorningChance.Length; stars++ )
            {
                dataList.Data [i - 1].MorningChance [stars].UpgradeRate = int.Parse ( M_allChance [stars] [0] );
                dataList.Data [i - 1].MorningChance [stars].RemainRate = int.Parse ( M_allChance [stars] [1] );
                dataList.Data [i - 1].MorningChance [stars].DowngradeRate = int.Parse ( M_allChance [stars] [2] );
            }

            //晚班
            string [] E_chance = csv [13].Split ( '-' );
            string [] [] E_allChance = new string [E_chance.Length] [];
            for ( int c = 0; c < E_chance.Length; c++ )
            {
                E_allChance [c] = E_chance [c].Split ( '^' );
            }

            dataList.Data [i - 1].EveningChance = new Commodity.UpgradeChance [dataList.Data [i - 1].price.Length - 1];// new IntList [M_allChance.Length]; //建立陣列長度  M_allChance.Length : 3
            for ( int stars = 0; stars < dataList.Data [i - 1].EveningChance.Length; stars++ )
            {
                dataList.Data [i - 1].EveningChance [stars].UpgradeRate = int.Parse ( E_allChance [stars] [0] );
                dataList.Data [i - 1].EveningChance [stars].RemainRate = int.Parse ( E_allChance [stars] [1] );
                dataList.Data [i - 1].EveningChance [stars].DowngradeRate = int.Parse ( E_allChance [stars] [2] );
            }
            //大夜班
            string [] N_chance = csv [14].Split ( '-' );
            string [] [] N_allChance = new string [N_chance.Length] [];
            for ( int c = 0; c < N_chance.Length; c++ )
            {
                N_allChance [c] = N_chance [c].Split ( '^' );
            }
            dataList.Data [i - 1].NightChance = new Commodity.UpgradeChance [dataList.Data [i - 1].price.Length - 1];// new IntList [M_allChance.Length]; //建立陣列長度  M_allChance.Length : 3
            for ( int stars = 0; stars < dataList.Data [i - 1].NightChance.Length; stars++ )
            {
                dataList.Data [i - 1].NightChance [stars].UpgradeRate = int.Parse ( N_allChance [stars] [0] );
                dataList.Data [i - 1].NightChance [stars].RemainRate = int.Parse ( N_allChance [stars] [1] );
                dataList.Data [i - 1].NightChance [stars].DowngradeRate = int.Parse ( N_allChance [stars] [2] );
            }
        }
    }

    public override void OpenWindow()
    {
        CommodityListEditorWindow.Open((CommodityList)target);
    }
}


public class CommodityListEditorWindow : ScriptableObjectCustomEditorWindow
{
    public static void Open(CommodityList d)
    {
        CommodityListEditorWindow window = GetWindow<CommodityListEditorWindow>();
        window.serializedObject = new SerializedObject(d);
        window.Show();
    }
 
    protected override void OnSelectChanged()
    {
        base.OnSelectChanged();
    }

    protected override void DrawSelectedPropertiesPanel()
    {
        currentProperty = selectedProperty;
        EditorGUILayout.BeginVertical("box");
        DrawField("icon", true);
        DrawField("name", true);
        DrawField("type", true);
        DrawField("specialType", true);
        DrawField("rank", true);
        DrawField("cost", true);
        DrawField("star", true);
        DrawField("explanation", true);
        DrawField("baseSaleValue", true);
        DrawField("quantity", true);
        DrawField("unlock", true);
        DrawField("ImproveCondition", true);
        DrawField("price", true,true);
        DrawTimeAttributeTable();
        EditorGUILayout.EndVertical();
    }


    protected void DrawTimeAttributeTable()
    {
        int width = 220;
        EditorGUILayout.BeginVertical("Box");
        //Header
        EditorGUILayout.BeginHorizontal("Box");
        EditorGUILayout.LabelField("",GUILayout.Width(width));
        EditorGUILayout.LabelField("morning", GUILayout.Width(width));
        EditorGUILayout.LabelField("evening",GUILayout.Width(width));
        EditorGUILayout.LabelField("night",GUILayout.Width(width));
        EditorGUILayout.EndHorizontal();
        //salesRate
        EditorGUILayout.BeginHorizontal("Box");
        EditorGUILayout.LabelField("SalesRate", GUILayout.Width(width));
        EditorGUILayout.PropertyField(selectedProperty.FindPropertyRelative("salesRate.Array.data[0]"),GUIContent.none, GUILayout.Width(width));
        EditorGUILayout.PropertyField(selectedProperty.FindPropertyRelative("salesRate.Array.data[1]"),GUIContent.none, GUILayout.Width(width));
        EditorGUILayout.PropertyField(selectedProperty.FindPropertyRelative("salesRate.Array.data[2]"),GUIContent.none, GUILayout.Width(width));
        EditorGUILayout.EndHorizontal();

        
        //star price
        int length = selectedProperty.FindPropertyRelative("price").arraySize -1;
        selectedProperty.FindPropertyRelative($"MorningChance").arraySize = length;
        selectedProperty.FindPropertyRelative($"EveningChance").arraySize = length;
        selectedProperty.FindPropertyRelative($"NightChance").arraySize = length;
        //if ( selectedProperty.FindPropertyRelative($"MorningChance.Array.data[0].UpgradeRate") == null )

        for (int i = 0; i < length; i++)
        {
            string[] shift = new string[] { "MorningChance", "EveningChance", "NightChance" };
            EditorGUILayout.BeginHorizontal("Box");
            EditorGUILayout.LabelField($"Star{i}LevelUpChance", GUILayout.Width(width));
            foreach (string s in shift)
            {
                EditorGUILayout.BeginHorizontal("Box",GUILayout.Width(width));
                EditorGUILayout.PropertyField(selectedProperty.FindPropertyRelative($"{s}.Array.data[{i}]"), GUIContent.none, GUILayout.Width((width / 3) - 20));
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndHorizontal();

        }
        EditorGUILayout.EndVertical();

    }

}
