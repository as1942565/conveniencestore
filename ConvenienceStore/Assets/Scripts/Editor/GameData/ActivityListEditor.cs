﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;
using UnityEditor;
using Tools;
using UnityEditor.Callbacks;
using System;
using UnityEditorInternal;

[CustomEditor(typeof(ActivityList))]
public class ActivityListEditor : ScriptableObjectCustomEditor
{
    public override void OpenWindow()
    {
        ActivityListEditorWindow.Open((ActivityList)target);
    }
    [MenuItem("Assets/Create/Data/ActivityDataList", priority = 3)] //建立專屬選項
    public static void CreateMyAsset()                                 //建立ScriptableObject
    {
        ActivityList asset = ScriptableObject.CreateInstance<ActivityList>();//生產建立ScriptableObject
        AssetDatabase.CreateAsset(asset, "Assets/Resources/" + ActivityList.resourcesPath + ".asset");//將生產的檔案放在指定路徑
        AssetDatabase.SaveAssets();                                    //將記憶體資料存置硬碟中
        EditorUtility.FocusProjectWindow();                            //顯示Project視窗
        Selection.activeObject = asset;                                 //選擇建立好的檔案
    }
    #region ReadFromCSV
    private TextAsset csvFile = null;
   
 
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        ActivityList dataList = (ActivityList)target;
        csvFile = (TextAsset)EditorGUILayout.ObjectField("CSV File", csvFile, typeof(TextAsset), false);
        if (csvFile != null)
        {
            if (GUILayout.Button("Read From CSV"))
            {
                ReadFromCSV(dataList, csvFile);
            }
        }
        EditorUtility.SetDirty(dataList);
    }
    private static void ReadFromCSV ( ActivityList dataList , TextAsset csvFile )
    {
        string [] activity = csvFile.text.Split ( '\n' );               //匯入CSV 空格分割
        dataList.Data = new Activity [activity.Length - 1];             //開啟適當陣列大小 (第一排不算所以-1)
        for ( int i = 1; i < activity.Length; i++ )
        {
            string [] csv = activity [i].Split ( ',' );                 //逗號分割
            dataList.Data [i - 1].name = csv [0];                       //傳送員工活動名稱
            dataList.Data [i - 1].type = (Activity.ActivityType)int.Parse ( csv [1] );         //傳送員工活動種類
            dataList.Data [i - 1].difficult = (Activity.ActivityDifficult)int.Parse ( csv [2] );    //傳送員工活動難易度
            dataList.Data [i - 1].exp = int.Parse ( csv [3] );           //傳送員工活動經驗值
            dataList.Data [i - 1].shift = int.Parse ( csv [4] );          //傳送員工活動天數
            dataList.Data [i - 1].explanation = csv [5].Replace ( "*" , "，" );//傳送員工活動描述
            dataList.Data [i - 1].unlock = bool.Parse ( csv [6] );      //傳送員工活動解鎖狀態
            dataList.Data [i - 1].group = int.Parse ( csv [11] );       //傳送員工活動群組代號

            if ( csv [7] != "" )
            {
                string [] eventClass = csv [7].Split ( '^' );
                string [] eParam = csv [8].Split ( '^' );
                dataList.Data [i - 1].eventData = new List<ActivityEventData> ();
                if ( eventClass.Length != eParam.Length )
                {
                    Debug.LogError ( "Event數量和變數數量不一樣" );
                    continue;
                }
                for ( int j = 0; j < eventClass.Length; j++ )
                {
                    ActivityEventData.EventType eventType = ActivityEventData.EventType.AddCleanA;
                    try
                    {
                        eventType = (ActivityEventData.EventType)System.Enum.Parse ( typeof ( ActivityEventData.EventType ) , eventClass [j] );
                    }
                    catch
                    {
                        Debug.LogError ( $"{eventClass [j]} 不是員工活動事件Enum" );
                    }
                    //Debug.Log(eventType.ToString());
                    Type t = Type.GetType ( $"ActivityEvent.{eventType},Assembly-CSharp" );// + eventType.ToString());   //將得到的字串變成腳本型態
                    if ( t == null )
                    {
                        Debug.LogError ( $"{eventClass [j]} 不是員工活動事件" );
                        continue;
                    }
                    ActivityBaseEvent e = (ActivityBaseEvent)Activator.CreateInstance ( t );//強制將腳本型態轉成員工活動事件並且記錄下來
                    int plength = e.ParamLength ();
                    string [] param = eParam [j].Split ( '-' );
                    if ( plength != param.Length )
                    {
                        Debug.LogError ( $"事件 : {eventClass [j]} 的參數應該是 {plength} 但是只取得 {param.Length} 個 ， {eParam [j]}" );
                    }
                    ActivityEventData eventdata = new ActivityEventData () { Event = eventType , Param = param };
                    dataList.Data [i - 1].eventData.Add ( eventdata );
                }
            }
            else
            {
                dataList.Data [i - 1].eventData = new List<ActivityEventData> ();
            }
            if ( csv [9] != "" )
            {
                string [] conditionClass = csv [9].Split ( '^' );
                string [] conditionParam = csv [10].Split ( '^' );
                dataList.Data [i - 1].unlockCondition = new List<UnlockConditionData> ();
                if ( conditionClass.Length != conditionParam.Length )
                {
                    Debug.LogError ( "condition數量和變數數量不一樣" );
                    continue;
                }
                for ( int j = 0; j < conditionClass.Length; j++ )
                {
                    UnlockConditionData.ConditionType conditionType = UnlockConditionData.ConditionType.AllMainRelationB;
                    try
                    {
                        conditionType = (UnlockConditionData.ConditionType)System.Enum.Parse ( typeof ( UnlockConditionData.ConditionType ) , conditionClass [j] );
                    }
                    catch
                    {
                        Debug.LogError ( $"{conditionClass [j]} 不是解鎖條件Enum" );
                    }
                    Type t = Type.GetType ( $"Condition.{conditionType},Assembly-CSharp" );// + eventType.ToString());   //將得到的字串變成腳本型態
                    if ( t == null )
                    {
                        Debug.LogError ( $"{conditionClass [j]} 不是解鎖條件" );
                        continue;
                    }
                    BaseUnlockCondition e = (BaseUnlockCondition)Activator.CreateInstance ( t );//強制將腳本型態轉成員工活動事件並且記錄下來
                    int plength = e.ParamLength ();
                    string [] param = new string [0];
                    if ( plength != 0 )
                    {
                        param = conditionParam [j].Split ( '-' );
                        if ( plength != param.Length )
                        {
                            Debug.LogError ( $"解鎖條件 : {conditionClass [j]} 的參數應該是 {plength} 但是只取得 {param.Length} 個 ， {conditionParam [j]}" );
                        }
                    }
                    UnlockConditionData eventdata = new UnlockConditionData () { Condition = conditionType , Param = param };
                    dataList.Data [i - 1].unlockCondition.Add ( eventdata );
                }
            }
            else
            {
                dataList.Data [i - 1].unlockCondition = new List<UnlockConditionData> ();
            }
        }
    }
    #endregion
}

public class ActivityListEditorWindow : ScriptableObjectCustomEditorWindow
{
    public static void Open(ActivityList d)
    {
        ActivityListEditorWindow window = GetWindow<ActivityListEditorWindow>();
        window.serializedObject = new SerializedObject(d);
        window.Show();
    }
    private ReorderableList unlockList;
    private ReorderableList eventList;

    protected override void OnSelectChanged()
    {
        base.OnSelectChanged();
        var eventListProperty = selectedProperty.FindPropertyRelative("eventData");
        eventList = new ReorderableList(serializedObject, eventListProperty, true, true, true, true);
        eventList.drawElementCallback = DrawElementCallback(eventListProperty);
        eventList.drawHeaderCallback = DrawHeaderCallback("Events");
        var unlockListProperty = selectedProperty.FindPropertyRelative("unlockCondition");
        unlockList = new ReorderableList(serializedObject, unlockListProperty, true, true, true, true);
        unlockList.drawElementCallback = DrawElementCallback(unlockListProperty);
        unlockList.drawHeaderCallback = DrawHeaderCallback("Unlock Conditions");
    }

  

    protected override void DrawSelectedPropertiesPanel()
    {
        int width = 500;
        //base.DrawSelectedPropertiesPanel();
        currentProperty = selectedProperty;
        EditorGUILayout.BeginVertical("box");
        DrawField ("name", true);
        EditorGUILayout.LabelField("種類 : 體力Stamina , 魅力Charm , 運氣Luck",GUILayout.Width(width));
        DrawField("type", true);
        DrawField("difficult", true);
        DrawField("conditionString", true);
        DrawField("exp", true);
        DrawField("shift", true);
        DrawField("explanation", true);
        DrawField("unlock", true);
        DrawField("group", true);
        if (eventList != null) { eventList.DoLayoutList(); }
        if (unlockList != null) { unlockList.DoLayoutList(); }
        EditorGUILayout.EndVertical();
    }

}