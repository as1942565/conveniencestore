﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using GameData;
[CustomPropertyDrawer(typeof(Worker.Abillity))]
public class WorkerAbilityDrawer : PropertyDrawer
{
    public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
    {
        EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
        float labelwidth = EditorGUIUtility.labelWidth;
        var arr = property.FindPropertyRelative("abilities.Array");
        EditorGUI.LabelField(new Rect(rect.x , rect.y + 2, rect.width, rect.height),label);
        EditorGUIUtility.labelWidth = 70;
        for (int i =0; i< arr.arraySize; i++)
        {
            EditorGUI.PropertyField(new Rect(rect.x+(i*110)+ labelwidth, rect.y + 2, 100, rect.height), arr.GetArrayElementAtIndex(i), new GUIContent($"{(Worker.Abillity.type)i}"));
        }
        EditorGUILayout.EndHorizontal();
        EditorGUIUtility.labelWidth = labelwidth;
    }
}
