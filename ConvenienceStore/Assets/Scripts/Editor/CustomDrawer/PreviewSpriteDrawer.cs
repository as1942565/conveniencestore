﻿using UnityEngine;

namespace UnityEditor
{
    [CustomPropertyDrawer(typeof(PreviewSpriteAttribute))]
    public class PreviewSpriteDrawer : PropertyDrawer
    {
        const float imageHeight = 100;
        public override float GetPropertyHeight(SerializedProperty property,
                                                GUIContent label)
        {
            if (property.propertyType == SerializedPropertyType.ObjectReference &&
                (property.objectReferenceValue as Sprite) != null)
            {
                return EditorGUI.GetPropertyHeight(property, label, true) + imageHeight + 10;
            }
            return EditorGUI.GetPropertyHeight(property, label, true);
        }

        public override void OnGUI(Rect position,
                                    SerializedProperty property,
                                    GUIContent label)
        {
            //Draw the normal property field
            EditorGUI.PropertyField(position, property, label, true);
            if (property.propertyType == SerializedPropertyType.ObjectReference)
            {
                Sprite sprite = property.objectReferenceValue as Sprite;
                if (sprite != null)
                {
                    Rect c = sprite.rect;
                    Rect rect = new Rect(new Vector2(position.position.x, position.position.y +30),new Vector2( 100 , 100 ));
                    if ( Event.current.type == EventType.Repaint)
                    {
                        var tex = sprite.texture;
                        c.xMin /= tex.width;
                        c.xMax /= tex.width;
                        c.yMin /= tex.height;
                        c.yMax /= tex.height;
                        GUI.DrawTextureWithTexCoords ( rect , tex , c);
                    }
                }
            }
        }
    }
}