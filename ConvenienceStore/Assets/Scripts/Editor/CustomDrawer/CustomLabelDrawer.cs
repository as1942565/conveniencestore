﻿using UnityEngine;

namespace UnityEditor
{
    [CustomPropertyDrawer(typeof(CustomLabelAttribute))]
    public class CustomLabelDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property,
                                                GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }

        public override void OnGUI(Rect position,
                                    SerializedProperty property,
                                    GUIContent label)
        {
            //Draw the normal property field
            CustomLabelAttribute customLabelAttribute = attribute as CustomLabelAttribute;
            if (customLabelAttribute != null)
            {
                EditorGUI.PropertyField(position, property, new GUIContent(customLabelAttribute.Label), true);
            }
            else
            {
                EditorGUI.PropertyField(position, property, label, true);
            }
        }

    }
}