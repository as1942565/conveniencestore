﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using GameData;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using System.Runtime.InteropServices;

[CustomPropertyDrawer(typeof(UnlockConditionData))]
public class UnlockConditionDataDrawer : PropertyDrawer
{
    public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
    {
        EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
        var condition = property.FindPropertyRelative("Condition");
        EditorGUI.PropertyField(new Rect(rect.x, rect.y+2, 160, rect.height-4), condition, GUIContent.none);
        UnlockConditionData conObj = new UnlockConditionData();
        conObj.Condition = (UnlockConditionData.ConditionType)condition.enumValueIndex;
        conObj.Param = new string[0];
        var obj = conObj.ToObject();
        int pLength = obj.ParamLength();
        var param = property.FindPropertyRelative("Param.Array");
        var size = param.FindPropertyRelative("size");
        size.intValue = pLength;
        float labelwidth = EditorGUIUtility.labelWidth;
        EditorGUIUtility.labelWidth = 70;
        for (int i = 0; i < pLength; i++)
        {
            EditorGUI.PropertyField(new Rect(rect.x+(180+i*110), rect.y+2, 100, rect.height-4), param.FindPropertyRelative($"data[{i}]"), new GUIContent($"Param{i}"));
        }
        EditorGUILayout.EndHorizontal();
        EditorGUIUtility.labelWidth = labelwidth;
    }
}


[CustomPropertyDrawer(typeof(ActivityEventData))]
public class ActivityEventDataDrawer : PropertyDrawer
{
    public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
    {
        EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
        var condition = property.FindPropertyRelative("Event");
        EditorGUI.PropertyField(new Rect(rect.x, rect.y + 2, 160, rect.height - 4), condition, GUIContent.none);
        ActivityEventData conObj = new ActivityEventData();
        conObj.Event = (ActivityEventData.EventType)condition.enumValueIndex;
        conObj.Param = new string[0];
        var obj = conObj.ToObject();
        int pLength = obj.ParamLength();
        var param = property.FindPropertyRelative("Param.Array");
        var size = param.FindPropertyRelative("size");
        size.intValue = pLength;
        float labelwidth = EditorGUIUtility.labelWidth;
        EditorGUIUtility.labelWidth =70;
        string[] labels = obj.ParamName();
        for (int i = 0; i < pLength; i++)
        {
            EditorGUI.PropertyField(new Rect(rect.x + (180 + i * 160), rect.y + 2, 150, rect.height - 4), param.FindPropertyRelative($"data[{i}]"), new GUIContent(labels[i]));
        }
        EditorGUILayout.EndHorizontal();
        EditorGUIUtility.labelWidth = labelwidth;
    }
}



[CustomPropertyDrawer(typeof(BusinessEventData))]
public class BusinessEventDataDrawer : PropertyDrawer
{
    public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
    {
        EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
        var condition = property.FindPropertyRelative("Event");
        EditorGUI.PropertyField(new Rect(rect.x, rect.y + 2, 160, rect.height - 4), condition, GUIContent.none);
        BusinessEventData conObj = new BusinessEventData();
        conObj.Event = (BusinessEventData.EventType)condition.enumValueIndex;
        conObj.Param = new string[0];
        var obj = conObj.ToObject();
        int pLength = obj.ParamLength();
        var param = property.FindPropertyRelative("Param.Array");
        var size = param.FindPropertyRelative("size");
        size.intValue = pLength;
        float labelwidth = EditorGUIUtility.labelWidth;
        EditorGUIUtility.labelWidth = 70;
        string[] labels = obj.ParamName();
        for (int i = 0; i < pLength; i++)
        {
            EditorGUI.PropertyField(new Rect(rect.x + (180 + i * 160), rect.y + 2, 150, rect.height - 4), param.FindPropertyRelative($"data[{i}]"), new GUIContent(labels[i]));
        }
        EditorGUILayout.EndHorizontal();
        EditorGUIUtility.labelWidth = labelwidth;
    }
}


[CustomPropertyDrawer(typeof(StoreExpansionEventData))]
public class StoreExpansionEventDataDrawer : PropertyDrawer
{
    public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
    {
        EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
        var condition = property.FindPropertyRelative("Event");
        EditorGUI.PropertyField(new Rect(rect.x, rect.y + 2, 160, rect.height - 4), condition, GUIContent.none);
        StoreExpansionEventData conObj = new StoreExpansionEventData();
        conObj.Event = (StoreExpansionEventData.EventType)condition.enumValueIndex;
        conObj.Param = new string[0];
        var obj = conObj.ToObject();
        int pLength = obj.ParamLength();
        var param = property.FindPropertyRelative("Param.Array");
        var size = param.FindPropertyRelative("size");
        size.intValue = pLength;
        float labelwidth = EditorGUIUtility.labelWidth;
        EditorGUIUtility.labelWidth = 70;
        string[] labels = obj.ParamName();
        for (int i = 0; i < pLength; i++)
        {
            EditorGUI.PropertyField(new Rect(rect.x + (180 + i * 160), rect.y + 2, 150, rect.height - 4), param.FindPropertyRelative($"data[{i}]"), new GUIContent(labels[i]));
        }
        EditorGUILayout.EndHorizontal();
        EditorGUIUtility.labelWidth = labelwidth;
    }
}