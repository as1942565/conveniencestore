﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using GameData;

[CustomPropertyDrawer(typeof(Commodity.UpgradeChance))]
public class LevelUpChanceDrawer : PropertyDrawer
{
    public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
    {
        EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
        var upgrade = property.FindPropertyRelative("UpgradeRate");
        var remain = property.FindPropertyRelative("RemainRate");
        var down = property.FindPropertyRelative("DowngradeRate");
        float labelwidth = EditorGUIUtility.labelWidth;
        EditorGUIUtility.labelWidth = 15;
        EditorGUI.PropertyField(new Rect(rect.x, rect.y, 60, rect.height), upgrade, new GUIContent("▲"));
        EditorGUI.PropertyField(new Rect(rect.x+70, rect.y, 60, rect.height), remain, new GUIContent("●"));
        EditorGUI.PropertyField(new Rect(rect.x+140, rect.y, 60, rect.height), down, new GUIContent("▼"));
        EditorGUILayout.EndHorizontal();
        EditorGUIUtility.labelWidth = labelwidth;
    }
}
