﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Runtime.InteropServices;
using System;

public class ScriptableObjectCustomEditorWindow :ExtendedEditorWindow
{
    Vector2 selectedPanelScrollPos = Vector2.zero;
    public virtual void OnGUI()
    {
        if(serializedObject == null)
        {
            Close();
        }
        currentProperty = serializedObject.FindProperty("Data");
        EditorGUILayout.BeginHorizontal();
        
        DrawSideBar(currentProperty);

        EditorGUILayout.BeginVertical("box", GUILayout.ExpandHeight(true));

        if(selectedProperty != null)
        {
            selectedPanelScrollPos=EditorGUILayout.BeginScrollView(selectedPanelScrollPos, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
            DrawSelectedPropertiesPanel();
            EditorGUILayout.EndScrollView();
        }
        else
        {
            EditorGUILayout.LabelField("Select an Item from the list");
        }

        EditorGUILayout.EndVertical();

        EditorGUILayout.EndHorizontal();

        Apply();
    }
    protected virtual void DrawSelectedPropertiesPanel()
    {
        DrawProperties(selectedProperty, true);
    }

}
