﻿using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;


public abstract class ScriptableObjectCustomEditor : Editor 
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if(GUILayout.Button("Open Editor"))
        {
            OpenWindow();
        }
    }
    public abstract void OpenWindow();
}

