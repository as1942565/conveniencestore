﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayButtonAudio : MonoBehaviour
{
    public static PlayButtonAudio self;
    private void Awake ()
    {
        self = this;
    }

    public EffectManager manager;

    private const string path = "Audios/Effect/GameUI/";

    public void PlayActivityCancel ()
    {
        manager.Play ( path , "ActivityCancel" , false , EffectPlayer.AudioType.system );
    }
    public void PlayActivityClick ()
    {
        manager.Play ( path , "ActivityClick" , false , EffectPlayer.AudioType.system );
    }
    public void PlayActivitySwitch ()
    {
        manager.Play ( path , "ActivitySwitch" , false , EffectPlayer.AudioType.system );
    }
    public void PlayActivityType ()
    {
        manager.Play ( path , "ActivityType" , false , EffectPlayer.AudioType.system );
    }

    public void PlayBusinessCancel  ()
    {
        manager.Play ( path , "BusinessCancel " , false , EffectPlayer.AudioType.system );
    }
    public void PlayBusinessClick ()
    {
        manager.Play ( path , "BusinessClick" , false , EffectPlayer.AudioType.system );
    }
    public void PlayBusinessOK ()
    {
        manager.Play ( path , "BusinessOK" , false , EffectPlayer.AudioType.system );
    }

    public void PlayCheckShift ()
    {
        manager.Play ( path , "CheckShift" , false , EffectPlayer.AudioType.system );
    }
    public void PlayCancelShift ()
    {
        manager.Play ( path , "CancelShift" , false , EffectPlayer.AudioType.system );
    }

    public void PlayCheckLog ()
    {
        manager.Play ( path , "CheckLog" , false , EffectPlayer.AudioType.system );
    }
    
    public void PlayClick ()
    {
        manager.Play ( path , "Click" , false , EffectPlayer.AudioType.system );
    }
    public void PlayDramaClick ()
    {
        manager.Play ( path , "DramaClick" , false , EffectPlayer.AudioType.system );
    }
    public void PlayMenuClick ()
    {
        manager.Play ( path , "MenuClick" , false , EffectPlayer.AudioType.system );
    }
    public void PlayMenuMouseEnter ()
    {
        manager.Play ( path , "MenuMouseOver" , false , EffectPlayer.AudioType.system );
    }

    public void PlayCommodity ()
    {
        manager.Play ( path , "Commodity" , false , EffectPlayer.AudioType.system );
    }
    public void PlayCommodityClick ()
    {
        manager.Play ( path , "CommodityClick" , false , EffectPlayer.AudioType.system );
    }
    public void PlayCommodityOK ()
    {
        manager.Play ( path , "CommodityOK" , false , EffectPlayer.AudioType.system );
    }
    public void PlayCommoditySwitch ()
    {
        manager.Play ( path , "CommoditySwitch" , false , EffectPlayer.AudioType.system );
    }
    public void PlayCommodityCancel ()
    {
        manager.Play ( path , "CommodityCancel" , false , EffectPlayer.AudioType.system );
    }

    public void PlayEmployeeSwitch ()
    {
        manager.Play ( path , "EmployeeSwitch" , false , EffectPlayer.AudioType.system );
    }
    public void PlayEmployeeCancel ()
    {
        manager.Play ( path , "EmployeeCancel" , false , EffectPlayer.AudioType.system );
    }
    public void PlayEmployeeClick ()
    {
        manager.Play ( path , "EmployeeClick" , false , EffectPlayer.AudioType.system );
    }

    public void PlayExpansionClick ()
    {
        manager.Play ( path , "ExpansionClick" , false , EffectPlayer.AudioType.system );
    }
    public void PlayExpansionCancel ()
    {
        manager.Play ( path , "ExpansionCancel" , false , EffectPlayer.AudioType.system );
    }
    public void PlayExpansionOK ()
    {
        manager.Play ( path , "ExpansionOK" , false , EffectPlayer.AudioType.system );
    }
    public void PlayExpansionSwitch ()
    {
        manager.Play ( path , "ExpansionSwitch" , false , EffectPlayer.AudioType.system );
    }
    public void PlayExpansionType ()
    {
        manager.Play ( path , "ExpansionType" , false , EffectPlayer.AudioType.system );
    }

    public void PlayPage ()
    {
        manager.Play ( path , "Page" , false , EffectPlayer.AudioType.system );
    }

    public void PlaySetupCommodity ()
    {
        manager.Play ( path , "SetupCommodity" , false , EffectPlayer.AudioType.system );
    }
    public void PlayShift ()
    {
        manager.Play ( path , "Shift" , false , EffectPlayer.AudioType.system );
    }

    public void PlayWorkerClick ()
    {
        manager.Play ( path , "WorkerClick" , false , EffectPlayer.AudioType.system );
    }
    public void PlayWorkerOK ()
    {
        manager.Play ( path , "WorkerOK" , false , EffectPlayer.AudioType.system );
    }
    public void PlaySetting ()
    {
        manager.Play ( path , "Setting" , false , EffectPlayer.AudioType.system );
    }
    public void PlayGallery ()
    {
        manager.Play ( path , "Gallery" , false , EffectPlayer.AudioType.system );
    }
    public void PlaySaveLoad ()
    {
        manager.Play ( path , "SaveLoad" , false , EffectPlayer.AudioType.system );
    }
    public void PlayCommodityX ()
    {
        manager.Play ( path , "CommodityX" , false , EffectPlayer.AudioType.system );
    }
    public void PlayEmployeeButtom ()
    {
        manager.Play ( path , "EmployeeButtom" , false , EffectPlayer.AudioType.system );
    }
    public void PlayConfirmShift ()
    {
        manager.Play ( path , "ConfirmShift" , false , EffectPlayer.AudioType.system );
    }
    public void PlayList ()
    {
        manager.Play ( path , "List" , false , EffectPlayer.AudioType.system );
    }
    public void PlayReport ()
    {
        manager.Play ( path , "Report" , false , EffectPlayer.AudioType.system );
    }
    public void PlayRest ()
    {
        manager.Play ( path , "Rest" , false , EffectPlayer.AudioType.system );
    }
    public void PlaySpeedUp ()
    {
        manager.Play ( path , "SpeedUp" , false , EffectPlayer.AudioType.system );
    }
    public void PlayExpend ()
    {
        manager.Play ( path , "Expend" , false , EffectPlayer.AudioType.system );
    }
    public void PlayReaserch ()
    {
        manager.Play ( path , "Reaserch" , false , EffectPlayer.AudioType.system );
    }
    public void PlayUpgrade ()
    {
        manager.Play ( path , "Upgrade" , false , EffectPlayer.AudioType.system );
    }

    public AudioSource PlayCustom ( string path , string name , bool loop )
    {
        return manager.Play ( path , name , loop , EffectPlayer.AudioType.system );
    }
}
