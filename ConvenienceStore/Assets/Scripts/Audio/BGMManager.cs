﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BGMManager : MonoBehaviour
{
    public static BGMManager self;
    private void Awake ()
    {
        self = this;
        GameLoopManager.instance.om.audioData.OnVolumeChanged += OnVolumeChanged;
    }

    public AudioSource audioSource;

    [HideInInspector]
    public string tempPath = "";

    public bool Mute;

    public void Stop ()
    {
        audioSource.Stop ();
    }
    public void Fade ( float vol , float duration )
    {
        audioSource.DOKill ();
        audioSource.DOFade ( vol , duration ).SetEase ( Ease.InQuart );
    }

    public void OnVolumeChanged ( float vol , float duration )
    {
        audioSource.DOFade ( vol , duration ).SetEase ( Ease.InQuart );
    }

    public void SetClip ( string path )
    {
        AudioClip clip = Resources.Load<AudioClip> ( path );
        if ( clip == null )
        {
            Debug.LogError ( path + "路徑不正確" );
            return;
        }
        tempPath = path;
        audioSource.clip = clip;
        audioSource.Play ();
    }

    public void PlayBGM ( string path , AudioData audioData )
    {
        if ( Mute )
            return;
        if ( audioData == null )
        {
            GameLoopManager.instance.CreateOM ();
        }
        float duration = audioData.GetDuration ();
        AudioClip clip = Resources.Load<AudioClip> ( path );
        if ( clip == null )
        {
            Debug.LogError ( path + "路徑不正確" );
            return;
        }
        if ( path != tempPath )
        {
            tempPath = path;
            audioSource.clip = clip;
            Fade ( 0 , duration );
        }
        
        float vol = audioData.GetBGM () / 100f;
        if ( vol < 0.1f )
            Play ();
        else
            Invoke ( "Play" , duration );
    }
    void Play ()
    {
        AudioData audioData = GameLoopManager.instance.om.audioData;
        if ( Mute )
            return;
        audioSource.Play ();
        audioData.PlayBGM ();
    }

    private const string shiftPath = "Audios/BGM/Main/Shift";
    
    public void PlayShiftBGM ()
    {
        int shift = (int)GameLoopManager.instance.pm.calender.GetShift ();
        if ( tempPath == shift.ToString () )
            return;
        PlayBGM ( shiftPath + shift.ToString () , GameLoopManager.instance.om.audioData );
        tempPath = shift.ToString ();
    }
}
