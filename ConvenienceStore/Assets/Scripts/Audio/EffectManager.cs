﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectManager : MonoBehaviour
{
    public Transform Sound;

    public static EffectManager self;
    private void Awake ()
    {
        self = this;
    }

    private const string playerPath = "Prefabs/Drama/EffectPlayer";
    private const string soundPath = "Prefabs/Drama/SoundPlayer";
    public bool Mute;

    public AudioSource Play ( string path , string name , bool loop , EffectPlayer.AudioType audioType )
    {
        if ( Mute )
            return null;
        Transform parent = transform;
        if ( audioType == EffectPlayer.AudioType.sound )
            parent = Sound;
        EffectPlayer player = Instantiate ( Resources.Load<EffectPlayer> ( playerPath ) , parent );
        player.gameObject.name = name;
        AudioClip clip = Resources.Load<AudioClip> ( path + name );
        if ( clip == null )
        {
            Debug.LogError ( path + name + "路徑不正確" );
            return null;
        }
        player.PlayEffect ( clip , loop , audioType );
        return player.audio;
    }
    public AudioSource PlaySound ( string path , string name , bool loop , EffectPlayer.AudioType audioType )
    {
        if ( Mute )
            return null;
        Transform parent = transform;
        if ( audioType == EffectPlayer.AudioType.sound )
            parent = Sound;
        EffectPlayer player = Instantiate ( Resources.Load<EffectPlayer> ( soundPath ) , parent );
        player.gameObject.name = name;
        AudioClip clip = Resources.Load<AudioClip> ( path + name );
        if ( clip == null )
        {
            //Debug.LogError ( path + name + "路徑不正確" );
            return null;
        }
        player.PlayEffect ( clip , loop , audioType );
        return player.audio;
    }

    public void DeleteSound ()
    {
        AudioSource [] audios = Sound.GetComponentsInChildren<AudioSource> ();
        for ( int i = 0; i < audios.Length; i++ )
        {
            Destroy ( audios[i].gameObject );
        }
    }
}
