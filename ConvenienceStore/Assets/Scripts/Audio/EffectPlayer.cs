﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectPlayer : MonoBehaviour
{
    public new AudioSource audio;
    private bool isPlay = false;
    private AudioType audioType;

    public enum AudioType
    {
        story = 0,
        system = 1,
        sound = 2,
        breathe = 3,
        H = 4,
    }


    public void PlayEffect ( AudioClip clip , bool loop , AudioType audioType = AudioType.system )
    {
        if ( EffectManager.self.Mute )
            return;
        audio.clip = clip;
        audio.loop = loop;
        if ( audioType == AudioType.sound )
            StorySceneManager.isPlayingSound = true;
        audio.Play ();
        this.audioType = audioType;
        UpdateVolume ();
        isPlay = true;
    }

    private void Update ()
    {
        UpdateVolume ();
        if ( isPlay && audio.isPlaying == false )
        {
            StorySceneManager.isPlayingSound = false;
            GameObject.Destroy ( gameObject );
        }
    }

    private void UpdateVolume ()
    {
        AudioData audioData = GameLoopManager.instance.om.audioData;
        switch ( audioType )
        {
            case AudioType.story:
                audio.volume = audioData.GetStoryEffect () / 100f;
                break;
            case AudioType.system:
                audio.volume = audioData.GetSystemEffect () / 100f;
                break;
            case AudioType.sound:
                audio.volume = audioData.GetSoundEffect () / 100f;
                break;
            case AudioType.breathe:
                audio.volume = audioData.GetBreatheEffect () / 100f;
                break;
            case AudioType.H:
                audio.volume = audioData.GetHEffect () / 100f;
                break;
        }
    }
}
