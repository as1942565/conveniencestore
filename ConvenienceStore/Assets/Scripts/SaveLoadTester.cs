﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Tester
{

    public class SaveLoadTester : MonoBehaviour
    {
        public PlayerDataManager playerData;
        public GameDataManager gameData;

        // Start is called before the first frame update
        void Start ()
        {
            gameData = new GameDataManager ();
            gameData.Init ();
            playerData = new PlayerDataManager ();
            playerData.Init ( gameData );
        }

        private void OnGUI ()
        {
            if ( GUILayout.Button ( "Save" ) )
            {
                SaveLoadManager.Save ( GameLoopManager.instance.pm , "0" );

            }
            if ( GUILayout.Button ( "Load" ) )
            {
                bool res = SaveLoadManager.Load ( out playerData , "0" );
                if ( res == false )
                {
                    playerData.Init ( gameData );
                }
            }
        }
    }
}
