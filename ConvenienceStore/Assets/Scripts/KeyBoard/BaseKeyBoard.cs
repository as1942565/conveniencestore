﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseKeyBoard : MonoBehaviour
{
    public GoToMenu menu;

    public static bool Ban;

    private void Update ()
    {
        if ( Ban == true )
            return;
        if ( Input.GetKeyDown ( KeyCode.Escape ) && StoryManager.self.EnableTeach == false && StoryManager.self.gameObject.activeSelf == false )
        {
            menu.gameObject.SetActive ( true );
        }
    }
}
