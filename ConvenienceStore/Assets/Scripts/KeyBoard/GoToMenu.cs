﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GoToMenu : MonoBehaviour
{
    public Text Warning;
    public Image YesBtn;
    public Image NoBtn;

    private void OnEnable ()
    {
        Warning.text = Localizer.Translate ( "是否要回到主選單？" );
        YesBtn.sprite = SpritePath.OKBtn5 () [0];
        NoBtn.sprite = SpritePath.OKBtn5 () [1];
    }

    public void Yes ()
    {
        UI.SetupCommodityManager.updateBool = false;
        UI.BuildRestock.updateBool = false;
        UI.OptionManager.updateBool = false;

        LoadingScene.self.Transitions ( 0.5f , 1f , false );
        Invoke ( "Menu" , 0.8f );
    }
    void Menu ()
    {
        SceneManager.LoadScene ( "GameStart" );
    }
    public void No ()
    {
        gameObject.SetActive ( false );
    }
}
