﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmployeeTest : MonoBehaviour
{
    private void Update ()
    {
        if ( Input.GetKeyDown ( KeyCode.R ) )
        {
            for ( int i = 0; i < 3; i++ )
                GameLoopManager.instance.pm.employeeList.AddRelationEXP ( i , 3 , GameLoopManager.instance.gm , GameLoopManager.instance.pm );
        }
        if ( Input.GetKeyDown ( KeyCode.T ) )
        {
            GameLoopManager.instance.pm.employeeList.GetEmployee ( 0 ).AddExp ( 3 , GameLoopManager.instance.gm , GameLoopManager.instance.pm );
        }
        if ( Input.GetKeyDown ( KeyCode.Y ) )
        {
            GameLoopManager.instance.pm.employeeList.GetEmployee ( 0 ).ResetRank ();
        }
    }
}
