﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpansionText : MonoBehaviour
{
    /*0 , 0 , 9 , 9 , 9 , 
    7 , 7 , 7 , 0 , 9 , 
    9 , 7 , 8 , 8 , 8 , 
    0 , 13 , 0 , 0 , 0 , 
    2 , 0 , 0 , 0 , 11 ,
    0 , 0 , 0 , 0 , 0 ,
    0 , 0 , 11*/
    public int itemID;

    private void Update ()
    {
        if ( Input.GetKeyDown ( KeyCode.E ) )
        {
            for ( int i = 0; i < GameLoopManager.instance.gm.storeExpansionList.Data.Length; i++ )
            {
                GameLoopManager.instance.pm.storeExpansionUnlock.GetUnock ( i );
            }
            GameLoopManager.instance.pm.businessUnlock.SetLevel ( 2 );
        }
        if ( Input.GetKeyDown ( KeyCode.A ) )
        {
            ExpansionManager.self.CreateItem ( itemID , GameLoopManager.instance.gm );
            GameLoopManager.instance.pm.storeExpansionUnlock.SetComplete ( itemID , true );
            itemID++;
        }
    }
}
