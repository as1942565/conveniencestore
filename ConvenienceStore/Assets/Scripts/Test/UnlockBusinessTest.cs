﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockBusinessTest : MonoBehaviour
{
    void Update ()
    {
        if ( Input.GetKeyDown ( KeyCode.Z ) )
        {
            GameData.Business [] business = GameLoopManager.instance.gm.businessList.Data;
            for ( int i = 0; i < business.Length; i++ )
            {
                if ( i == 3 )
                    continue;
                for ( int c = 0; c < business [i].unlock.Length; c++ )
                {
                    GameLoopManager.instance.pm.businessUnlock.SetUnlock ( i , c , true );
                }
            }
        }
    }
}
