﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundTest : MonoBehaviour
{
    public LevelManager levelManager;

    public void Level1 ()
    {
        levelManager.ChangeShift ( 0 , (int)GameLoopManager.instance.pm.calender.GetShift () , GameLoopManager.instance.gm );
    }
    public void Level2 ()
    {
        levelManager.ChangeShift ( 1 , (int)GameLoopManager.instance.pm.calender.GetShift () , GameLoopManager.instance.gm );
    }
    public void Level3 ()
    {
        levelManager.ChangeShift ( 2 , (int)GameLoopManager.instance.pm.calender.GetShift () , GameLoopManager.instance.gm );
    }
}
