﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommodityTest : MonoBehaviour
{
    private void Update ()
    {
        if ( Input.GetKeyDown ( KeyCode.Q ) )
        {
            for ( int i = 0; i < GameLoopManager.instance.gm.commodityList.Data.Length; i++ )
            {
                GameLoopManager.instance.pm.commodityItem.SetUnlock ( i );
                GameLoopManager.instance.pm.storage.AddQuantity ( i , 30 , 0 );
            }
        }
        if ( Input.GetKeyDown ( KeyCode.W ) )
        {
            for ( int i = 0; i < GameLoopManager.instance.gm.commodityList.Data.Length; i++ )
            {
                GameLoopManager.instance.pm.commodityItem.AddSaleQuantity ( i , 100 );
            }
        }
    }
}
