﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HireWorkerTest : MonoBehaviour
{
    public UI.BuildHireWorker buildHireWorker;
    private void Update ()
    {
        if ( Input.GetKeyDown ( KeyCode.D ) )
        {
            PlayerDataManager pm = GameLoopManager.instance.pm;
            pm.hireWorker.SetQuantity ( 1 );
            pm.hireWorker.SetCanHireID ( GameLoopManager.instance.gm , pm );
            buildHireWorker.Open ();
        }
    }
}
