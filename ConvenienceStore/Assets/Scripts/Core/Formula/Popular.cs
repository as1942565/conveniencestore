﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Formula
{
    public static class Popular
    {
        public static int Calculate ( PlayerDataManager pm , GameDataManager gm )
        {
            float result = Formula ( pm , gm );
            return Mathf.FloorToInt ( result );
        }

        private static float Formula ( PlayerDataManager pm , GameDataManager gm )
        {
            float addPopular = AddPopular () + AddPopularWithSaleQuantity ( pm , gm );
            return popular ( pm ) + addPopular * StoreAddition ( pm );
        }

        //目前人氣值
        public static int popular ( PlayerDataManager pm )
        {
            return pm.playerData.GetPopular ();
        }

        //固定增加人氣值
        public static int AddPopular ()
        {
            return 2;
        }

        //銷售量轉人氣值
        public static float AddPopularWithSaleQuantity ( PlayerDataManager pm , GameDataManager gm )
        {
            int day = pm.calender.GetDay ();
            float sum = 0f;
            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )
            {
                int type = (int)gm.commodityList.Data [i].type;
                sum += pm.gameLog.GetCommoditySaleQuantity ( day , i );
            }
            return 1 + sum * 0.1f;
        }

        //擴建設施加成
        public static float StoreAddition ( PlayerDataManager pm )
        {
            return pm.storeExpansionAddition.GetPopular ();
        }
    }
}
