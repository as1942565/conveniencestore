﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;

namespace Formula
{
    //商品銷售數量公式
    public static class CommoditySaleQuantity
    {
        public static int Calculate ( PlayerDataManager pm , GameDataManager gm , int id , int cabinetType , Commodity.CommodityType type )
        {
            float result = Formula ( pm , gm ,  id , cabinetType , type );
            return Mathf.FloorToInt ( result );
        }
        //( 當前行動點人氣值 × 商品預計銷售量 × 時段加成 x 商品櫃加成 ) + 員工活動 + 營業活動
        private static float Formula ( PlayerDataManager pm , GameDataManager gm , int id , int cabinetType , Commodity.CommodityType type )
        {
            return Popular ( pm ) * BaseSaleValue ( gm , id ) * ShiftAddition ( pm , gm , id ) * CabinetAddition ( gm , cabinetType ) + ActivityAddition ( pm , type ) + BusinessAddition ( pm , type );
        }

        //當前人氣值
        public static int Popular ( PlayerDataManager pm )
        {
            return pm.playerData.GetPopular ();
        }

        //商品預計銷售量
        public static float BaseSaleValue ( GameDataManager gm , int id )
        {
            return gm.commodityList.Data [id].baseSaleValue;
        }

        //時段加成
        public static float ShiftAddition ( PlayerDataManager pm , GameDataManager gm , int id )
        {                                                                       //
            int shift = (int)pm.calender.GetShift ();                           //取得目前時段
            float [] populatAddition = gm.customData.Data.PopolarAddition;      //取得商品受歡迎度加成
            int commodityPopular = (int)pm.commodityItem.GetData ( id ).popular [shift];//取得該商品該時段的受歡迎程度
            return populatAddition [commodityPopular];                           //回傳該商品該時段的受歡迎程度加成
        }

        //商品櫃加成
        public static float CabinetAddition ( GameDataManager gm , int cabinetType )
        {
            return gm.cabinetList.Data [cabinetType].addition;
        }

        //員工活動加成
        public static float ActivityAddition ( PlayerDataManager pm , Commodity.CommodityType type )
        {
            return pm.activityAddition.GetSaleQuantity ( type );
        }

        //營業活動加成
        public static float BusinessAddition ( PlayerDataManager pm , Commodity.CommodityType type )
        {
            return pm.businessAddition.GetSaleQuantity ( type , pm.calender.TotalShift );
        }
    }
}
