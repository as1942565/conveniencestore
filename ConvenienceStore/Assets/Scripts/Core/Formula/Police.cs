﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Formula
{
    public static class Police
    {
        public static int Calculate ( PlayerDataManager pm )
        {
            float result = Formula ( pm );
            if ( result < 0 )
                result = 0f;
            return (int)result;
        }

        private static float Formula ( PlayerDataManager pm )
        {
            return police ( pm ) - ReducePoliceAmount ( pm ) + AddPolice ( pm );
        }

        public static int ReducePoliceAmount ( PlayerDataManager pm )
        {
            return (int)( LessPolice () + StoreExpandNum ( pm ) / 2 );
        }

        //目前治安度
        public static int police ( PlayerDataManager pm )
        {
            return pm.playerData.GetPolice ();
        }

        //固定損失的治安度
        public static int LessPolice ()
        {
            return 2;
        }

        //目前擴建設施數量
        public static int StoreExpandNum ( PlayerDataManager pm )
        {
            return pm.storeExpansionUnlock.GetExpansionQuantity ();
        }

        //自動回復治安度
        public static int AddPolice ( PlayerDataManager pm )
        {
            return pm.storeExpansionAddition.GetAutoAddPolice ();
        }
    }
}
