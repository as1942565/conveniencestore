﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;

namespace Formula
{
    //商品收入公式
    public static class CommodityIncome
    {
        //( 每個時段員工加成多25% 基礎值是100% 最多175% ) x (設施加成)x (營業活動加成)
        public static float Calculate ( Commodity.CommodityType type , PlayerDataManager pm )
        {
            float result = Formula ( type , pm );
            return result;
        }
        private static float Formula ( Commodity.CommodityType type , PlayerDataManager pm )
        {
            return ScheduleAddition ( pm ) * StoreAddition ( pm ) * ActivityAddition ( type , pm ) * BusinessAddition ( type , pm );
        }

        //員工排班加成
        public static float ScheduleAddition ( PlayerDataManager pm )
        {                                                                       //
            //每個時段員工加成多25% 基礎值是100% 最多175%
            int shift = (int)pm.calender.GetShift ();                           //取得現在的時段
            int day = pm.calender.GetDay ();
            int workEmployeeID = pm.employeeScheduleLog.GetScheduleQuantity ( day , shift , pm.employeeList );    //取得該時段排班人數
            float result = 1 + ( workEmployeeID - 1 ) * 0.25f;                  //每多1人多25%收入 (沒排人會扣25%)
            return result;
        }
        public static float ScheduleAdditionWithShift ( PlayerDataManager pm , int shift )
        {                                                                       //
            //每個時段員工加成多25% 基礎值是100% 最多175%
            int day = pm.calender.GetDay ();
            int workEmployeeID = pm.employeeScheduleLog.GetScheduleQuantity ( day , shift , pm.employeeList );    //取得該時段排班人數
            float result = 1 + ( workEmployeeID - 1 ) * 0.25f;                  //每多1人多25%收入 (沒排人會扣25%)
            return result;
        }

        //擴建設施加成
        public static float StoreAddition ( PlayerDataManager pm )
        {
            return pm.storeExpansionAddition.GetIncome ();
        }

        //員工活動加成
        public static float ActivityAddition ( Commodity.CommodityType type ,  PlayerDataManager pm )
        {
            return pm.activityAddition.GetIncome ( type );
        }

        //店長業務加成
        public static float BusinessAddition ( Commodity.CommodityType type ,  PlayerDataManager pm )
        {
            return pm.businessAddition.GetIncome ( type , pm.calender.TotalShift );
        }
    }
}
