﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Formula
{
    //清潔度公式
    public static class Clean
    {
        //當前行動點的清潔度-( 5 + 當前擴建設施數量*2 ) = 下回合清潔度 (數值最低為0，不會變成負數。)
        public static int Calculate ( PlayerDataManager pm  )
        {
            float result = Formula ( pm );
            if ( result < 0 )
                result = 0f;
            return (int)result;
        }

        private static float Formula ( PlayerDataManager pm  )
        {
            //( $"{clean ( pm )} - {ReduceCleanAmount(pm)} + {AddClean ( pm )}" );
            return clean ( pm ) - ReduceCleanAmount(pm) + AddClean ( pm );
        }
            
        public static int ReduceCleanAmount(PlayerDataManager pm )
        {
            return (int)( LessClean () + StoreExpandNum ( pm ) / 2 );
        }

        //目前清潔度
        public static int clean ( PlayerDataManager pm  )
        {
            return pm.playerData.GetClean ();
        }

        //固定損失的清潔度
        public static int LessClean ()
        {
            return 2;
        }

        //目前擴建設施數量
        public static int StoreExpandNum ( PlayerDataManager pm  )
        {
            return pm.storeExpansionUnlock.GetExpansionQuantity ();
        }

        //自動回復清潔度
        public static int AddClean ( PlayerDataManager pm )
        {
            return pm.storeExpansionAddition.GetAutoAddClean ();
        }
    }
}
