﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Action
{
    public static class HireEmployee
    {
        public static bool CheckUnlock ( int ID , PlayerDataManager pm , GameDataManager gm )
        {
            if ( ID >= gm.workerList.Data.Length )
            {
                Debug.LogError ( ID + "超過所有員工數量" );
                return false;
            }
            return !pm.employeeList.GetUnlockID ( ID );
        }
        public static bool Run ( int ID , PlayerDataManager pm , GameDataManager gm )
        {
            if ( !CheckUnlock ( ID , pm , gm ) )
            {
                Debug.LogError ( ID + "已經解鎖 不能再招募" );
                return false;
            }
            pm.employeeList.AddNewEmployee ( ID , gm.workerList.Data [ID] );
            string message = $"「員工招募」招募到{gm.workerList.Data [ID].name}\n";
            int shift = pm.calender.TotalShift;
            pm.gameLog.AddActionLog ( shift , message );
            return true;
        }
    }
}
