﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Action
{
    public static class StoreExpansion
    {
        public static bool CheckUnlock ( int ID , PlayerDataManager pm )        //檢查店面擴建解鎖狀態
        {                                                                       //
            return pm.storeExpansionUnlock.GetUnock ( ID );
        }
        public static bool CheckComplete ( int ID , PlayerDataManager pm )      //檢查店面擴建完成狀態
        {
            return pm.storeExpansionUnlock.GetComplete ( ID );
        }
        public static bool CheckMoney ( int cost , PlayerDataManager pm )       //檢查玩家能不能付錢
        {
            int money = pm.playerData.GetMoney ();
            return money >= cost;
        }

        public static bool Run ( int [] IDs , PlayerDataManager pm , GameDataManager gm , NextShiftProcessor nextShift )
        {                                                                       //
            for ( int i = 0; i < IDs.Length; i++ )
            {
                int ID = IDs [i];

                if ( !CheckUnlock ( ID , pm ) )
                {
                    Debug.LogWarning ( ID + "擴建尚未解鎖" );
                    return false;
                }
                if ( CheckComplete ( ID , pm ) )
                {
                    Debug.LogWarning ( ID + "擴建已完成" );
                    return false;
                }
                int cost = gm.storeExpansionList.Data [ID].cost;
                if ( !CheckMoney ( cost , pm ) )
                {
                    Debug.LogWarning ( "玩家金錢不足" );
                    return false;
                }
                int day = pm.calender.GetDay ();
                int spend = gm.storeExpansionList.Data [ID].cost;
                string name = gm.storeExpansionList.Data [ID].name;
                int shift = pm.calender.TotalShift;
                if ( !pm.storeLog.Getexpansion ( day , ID ) )
                {
                    string msg = name + "已擴建。";
                    pm.gameLog.AddInformation ( day , Localizer.Translate ( msg ) );
                    pm.storeLog.SetExpansion ( shift , ID , true );
                    pm.storeLog.AddNotice ( msg );
                }
                pm.playerData.AddMoney ( -spend );                                  //玩家扣錢
                int totalShift = pm.calender.TotalShift;
                pm.gameLog.AddExpand ( totalShift , spend );
                if ( ExpansionManager.self != null )
                    ExpansionManager.self.CreateItem ( ID , gm );
                string message = $"「店面擴建」擴建{gm.storeExpansionList.Data [ID].name}\n";
                pm.gameLog.AddActionLog ( shift , message );
            }
            nextShift.StoreExpansion ( IDs );                                    //擴建
            return true;
        }
    }
}
