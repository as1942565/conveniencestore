﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Action
{
    public static class SetupEmployee
    {
        public static bool CheckPower ( int id , PlayerDataManager pm)          //檢查員工體力值
        {
            return pm.employeeList.GetEmployee ( id ).GetPower () > 0;
        }
        public static bool CheckEmployeeUnlock ( int id , PlayerDataManager pm )//檢查員工解鎖狀態
        {
            if ( !pm.employeeList.GetUnlockID ( id ) )
            {
                Debug.LogError ( id + "超過目前已解鎖的員工數量" );
                return false;
            }
            return pm.employeeList.GetEmployee ( id ) != null;
        }
        public static bool CheckCabinetUnlock ( int shift , PlayerDataManager pm )//檢查該時段的排班欄位解鎖狀態
        {
            if ( shift > 3 )
            {
                Debug.LogError ( shift + "超過班次種類" );
                return false;
            }
            int day = pm.calender.GetDay ();
            return pm.employeeScheduing.GetUnlock ( shift ) > pm.employeeScheduleLog.GetScheduleQuantity ( day , shift , pm.employeeList );
        }

        public static bool Run ( int day , int employeeID , int shift , PlayerDataManager pm )
        {                                                                       //
            if ( !CheckEmployeeUnlock ( employeeID , pm ) )
            {
                Debug.LogError ( employeeID + "員工尚未解鎖" );
                return false;
            }
            if ( !CheckPower ( employeeID , pm ) )
            {
                Debug.LogError ( employeeID + "員工的體力不足" );
                return false;
            }
            if ( !CheckCabinetUnlock ( shift , pm ) )
            {
                Debug.LogError ( shift + "班次欄位數不足" );
                return false;
            }
            pm.employeeScheduleLog.SetEmployeeID ( day , employeeID , shift );
            string name = GameLoopManager.instance.gm.workerList.Data [employeeID].name;
            string [] shiftName = new string [] { "早班" , "晚班" , "大夜" };
            if ( shift <= 2 )
            {
                string message = $"(員工排班){name}排在{shiftName[shift]}\n";
                int totalShift = pm.calender.TotalShift;
                pm.gameLog.AddActionLog ( totalShift , message );
            }
            return true;
        }
    }
}
