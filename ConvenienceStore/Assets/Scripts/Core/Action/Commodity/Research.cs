﻿using System.Collections;
using System.Collections.Generic;
using Tools;
using UnityEngine;

namespace Action
{
    public static class Research
    {
        public static bool CheckMoney ( int spend , PlayerDataManager pm )      //檢查是否能付錢
        {
            int money = pm.playerData.GetMoney ();                              //取得玩家金錢
            return money > spend;                                               //回傳結果
        }
        public static List<int> FillResearch ( int rank , int type , GameDataManager gm , PlayerDataManager pm )//檢查是否研發完畢
        {
            List<int> ids = new List<int> ();                                   //可以研發的商品編號
            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )            //搜尋全部的商品
            {
                if ( rank < 2 )
                {
                    if ( !pm.commodityItem.GetData ( i ).unlock && (int)gm.commodityList.Data [i].type == type && gm.commodityList.Data [i].rank == rank )//如果該商品未解鎖 而且 符合商品種類 而且 該商品的階級符合研發階級
                        ids.Add ( i );                                          //就加入陣列
                }
                else
                {
                    if ( !pm.commodityItem.GetData ( i ).unlock && (int)gm.commodityList.Data [i].type == type && gm.commodityList.Data [i].rank >= rank )//如果該商品未解鎖 而且 符合商品種類 而且 該商品的階級符合研發階級
                        ids.Add ( i );                                          //就加入陣列
                }
            }
            return ids;
        }
        public static bool CheckResearch ( int rank , int type , GameDataManager gm , PlayerDataManager pm )
        {
            return FillResearch ( rank , type , gm , pm ).Count > 0;
        }

        public static int ResearchID ( int rank , int type , GameDataManager gm , PlayerDataManager pm )
        {
            int spend = gm.customData.Data.DevelopMoney [rank];                 //取得研發金額
            if ( !CheckMoney ( spend , pm ) )                                   //檢查是否能付錢
            {
                Debug.LogWarning ( "金額不足" );
                return -1;
            }
            List<int> researchID = new List<int> ();                            //可以研發的商品編號
            researchID = FillResearch ( rank , type , gm , pm );
            if ( !CheckResearch ( rank , type , gm , pm ) )                     //檢查是否研發完畢
            {
                Debug.LogWarning ( rank + "階段的" + type + "商品全部已解鎖" );
                return -1;
            }
            researchID.Shuffle ();                                              //將商品數打亂
            if ( gm.commodityList.Data [researchID [0]].rank > 2 )
            {
                researchID.Shuffle ();
            }
            int id = researchID [0];
            //string name = gm.commodityList.Data [id].name;
            string name = Localizer.Translate ( gm.commodityList.Data [id].name );
            string message = $"「商品研發」研發出{name}\n";
            int shift = pm.calender.TotalShift;
            pm.gameLog.AddActionLog ( shift , message );
            pm.playerData.AddMoney ( -spend );                                  //玩家扣錢
            return id;
        }

        public static bool Run ( int id , NextShiftProcessor nextShift )        //商品研發 ( 投資額等級 , 花費 )
        {        
            nextShift.Research ( id );                                          //研發該編號的商品
            return true;
        }
    }
}
