﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Action
{
    public static class Restock
    {
        public static int CheckMaxPurchase ( int commodityID , int cost , int stockNum , PlayerDataManager pm , GameDataManager gm )
        {
            //int maxPurchaseNumByOne = pm.purchase.GetMaxPurchaseNumByOne ();                        //單項商品進貨最大上限
            int maxPurchaseNumByOne = 10000;                        
            int allWaittingNum = pm.purchase.GetAllWaittingNum ();
            int allDefiningNum = pm.purchase.GetAllDefiningNum ();
            int waitPurchase = pm.purchase.GetWaitPurchase ( commodityID );
            int definingPurchase = pm.purchase.GetDefinePurchase ( commodityID );
            if ( stockNum > 0 )
            {
                //所有確定訂單
                int storageRemain = pm.storage.GetStorageRemain () - allDefiningNum - allWaittingNum;   //最大庫存量 減掉 所有商品持有數 減掉 所有訂單數量
                int quantity = Mathf.Min ( storageRemain , maxPurchaseNumByOne - waitPurchase - definingPurchase );
                int money = pm.playerData.GetMoney ();
                int tempCost = pm.purchase.GetAllWaittingCost ( gm );
                int canPayNum = ( money - tempCost ) / cost;
                int maxQuantity = Mathf.Clamp ( stockNum , 0 , quantity );
                return Mathf.Min ( canPayNum , maxQuantity );
            }
            else
            {
                return Mathf.Max ( stockNum , -waitPurchase );
            }
        }
        public static bool CheckMoney ( int id , int stockNum , GameDataManager gm , PlayerDataManager pm )
        {
            if ( stockNum <= 0 )
                return true;
            int spend = stockNum * gm.commodityList.Data [id].cost;
            return pm.playerData.GetMoney () - spend >= 0;
        }
        public static bool Run ( int id , int stockNum , GameDataManager gm , PlayerDataManager pm )
        {
            if ( !CheckMoney ( id , stockNum , gm , pm ) )
            {
                Debug.LogWarning ( "玩家的金錢不夠 因此無法進貨" + stockNum + "個" + id + "商品" );
                return false;
            }

            //string name = gm.commodityList.Data [id].name;
            string name = Localizer.Translate ( gm.commodityList.Data [id].name );
            string message = $"(商品進貨){name}進貨{stockNum}個\n";
            int shift = pm.calender.TotalShift;
            pm.gameLog.AddActionLog ( shift , message );

            int cost = gm.commodityList.Data [id].cost;                         //取得商品的成本
            pm.purchase.AddDefinePurchase ( id , stockNum );                    //確定進貨商品
            pm.purchase.ClearWaitPurchase ( id );                               //清除等待進貨商品
            int spend = cost * stockNum;                                        //紀錄已花費的錢
            pm.playerData.AddMoney ( -spend );                                  //更新玩家金錢
            int day = pm.calender.GetDay ();
            int totalShift = pm.calender.TotalShift;
            pm.gameLog.AddRestock ( totalShift , spend );
            return true;
        }
    }
}
