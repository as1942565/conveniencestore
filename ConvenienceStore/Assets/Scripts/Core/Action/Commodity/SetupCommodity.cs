﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Action
{
    public static class SetupCommodity
    {
        public static bool CheckQuantity ( int id , PlayerDataManager pm )      //檢查商品數量
        {                                                                       //
            return pm.storage.GetQuantity( id ) > 0;
        }
        public static bool CheckUnlock ( int type , int id , PlayerDataManager pm )//檢查欄位解鎖狀態
        {
            return id < pm.cabinet.GetTypeUnlockNum ( type );
        }

        public static bool Run ( int type , int cabinetID , int commodityID , PlayerDataManager pm )
        {
            if ( !CheckQuantity ( commodityID , pm ) )
            {
                Debug.LogWarning ( commodityID + "商品數量不足" );
                return false;
            }
            //string name = GameLoopManager.instance.gm.commodityList.Data [commodityID].name;
            string name = Localizer.Translate ( GameLoopManager.instance.gm.commodityList.Data [commodityID].name );
            string[] typeName = new string [] { "食品櫃" , "飲品櫃" , "雜貨櫃" , "限定櫃" , "主打櫃" };
            string message = $"(商品上架){name}放在{typeName[type]}的{cabinetID + 1}號欄位\n";
            int shift = pm.calender.TotalShift;
            pm.gameLog.AddActionLog ( shift , message );
            pm.cabinet.SetCommodityID ( type , cabinetID , commodityID );                //放置商品到指定商品櫃
            return true;
        }
    }
}
