﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Action
{
    public static class Upgrade
    {
        public static bool CheckUnlock ( int ID , PlayerDataManager pm , GameDataManager gm )//檢查改良解鎖狀態
        {                                                                       //
            return pm.commodityItem.GetImproveUnlock ( ID , gm );
        }
        public static bool CheckComplete ( int ID , PlayerDataManager pm )      //檢查改良完成狀態
        {
            return pm.commodityItem.GetImproveComplete ( ID );
        }

        public static bool Run ( int ID , PlayerDataManager pm , GameDataManager gm , NextShiftProcessor nextShift )
        {                                                                       //
            if ( !CheckUnlock ( ID , pm , gm ) )
            {
                Debug.LogWarning ( ID + "商品尚未完成改良條件" );
                return false;
            }
            if ( CheckComplete ( ID , pm ) )
            {
                Debug.LogWarning ( ID + "商品已經改良完畢" );
                return false;
            }
            int star = pm.commodityItem.GetData ( ID ).star;                    //紀錄CP值最高的商品星級
            //string name = gm.commodityList.Data [ID].name;
            string name = Localizer.Translate ( gm.commodityList.Data [ID].name );
            string message = $"「商品改良」將{name}改良到{star + 1}星\n";
            int shift = pm.calender.TotalShift;
            pm.gameLog.AddActionLog ( shift , message );
            nextShift.Improve ( ID , star );                                    //改良
            return true;
        }
    }
}
