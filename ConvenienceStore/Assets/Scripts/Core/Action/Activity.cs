﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Action
{
    public static class Activity
    {
        public static bool CheckUnlock ( int id , PlayerDataManager pm )                                      //檢查員工活動解鎖狀態
        {                                                                       //
            return pm.activityUnlock.GetUnlock ( id );
        }
        public static int CheckProhibit ( int id , PlayerDataManager pm )       //檢查員工活動執行狀態
        {
            return (int)pm.activityUnlock.GetMode ( id );
        }
        public static bool CheckPeople ( int length )                           //檢查員工活動人數狀態
        {
            return 0 < length && length <= 2;
        }
        

        public static bool Run ( ActivityScheduale.Scheduale tempScheduales , PlayerDataManager pm , GameDataManager gm )
        {
            return Run ( tempScheduales.ActivityID , tempScheduales.EmployeesID , tempScheduales.GetSuccessed () , tempScheduales.GetMethod () , pm , gm );
        }

        private static bool  CheckAll ( int activityID , int [] employeeID , bool totalChance , ActivityScheduale.Scheduale.SchedualeMethod method , PlayerDataManager pm , GameDataManager gm )
        {
             if ( !CheckUnlock ( activityID , pm ) )
            {
                Debug.LogError ( activityID + "員工活動尚未解鎖" );
                return false;
            }
            if ( CheckProhibit ( activityID , pm ) != 0 )
            {
                Debug.LogError ( activityID + "員工活動目前無法執行" );
                return false;
            }
            if ( !CheckPeople ( employeeID.Length ) )
            {
                Debug.LogError ( employeeID.Length + "人數不符合資格" );
                return false;
            }
            return true;
        }

        public static bool Run ( int activityID , int [] employeeID , bool totalChance , ActivityScheduale.Scheduale.SchedualeMethod method , PlayerDataManager pm , GameDataManager gm )
        {

            if (CheckAll ( activityID , employeeID , totalChance , method , pm , gm ) == false )
            {
                return false;
            }
            return Schedule ( activityID , employeeID , totalChance , method , pm , gm );
         
        }
        public static bool ScheduleWithoutChecks( ActivityScheduale.Scheduale tempScheduales , PlayerDataManager pm , GameDataManager gm )
        {
             return Schedule ( tempScheduales.ActivityID , tempScheduales.EmployeesID , tempScheduales.GetSuccessed () , tempScheduales.GetMethod () , pm , gm  );
        }

        static bool Schedule (int activityID , int [] employeeID , bool totalChance , ActivityScheduale.Scheduale.SchedualeMethod method , PlayerDataManager pm , GameDataManager gm )
        {
            ActivityScheduale.Scheduale scheduale = GetSchedule ( activityID , employeeID , totalChance , method , gm , pm );
           
            pm.activityScheduale.AddScheduales ( gm.activityList.GenerateEvent ( activityID ) , scheduale , gm , pm );//發佈委託
            pm.activityUnlock.SetMode ( activityID , ActivityUnlock.state.Processing );                       //該編號活動正在委託中
            LockGroup ( activityID , gm , pm );
            Result ( activityID , employeeID , gm , pm );

            return true;
        }

        static ActivityScheduale.Scheduale GetSchedule ( int activityID , int [] employeeIDs , bool successed , ActivityScheduale.Scheduale.SchedualeMethod method , GameDataManager gm , PlayerDataManager pm )
        {
            ActivityBaseEvent [] events = gm.activityList.GenerateEvent ( activityID );
            
            int executeShift = pm.calender.TotalShift + gm.activityList.Data [activityID].shift;
            
            GameData.Activity.ActivityDifficult difficult = gm.activityList.Data [activityID].difficult;        //取得活動困難度
            int bonus = gm.customData.Data.RewardMoney [(int)difficult];

            ActivityScheduale.Scheduale scheduale = new ActivityScheduale.Scheduale ( events , employeeIDs , activityID );
            scheduale.SetMethod ( method );
            scheduale.SetExcuteShift ( executeShift );
            scheduale.SetSuccessed ( successed );
            scheduale.SetBonus ( bonus );

            return scheduale;
        }

        static void Result ( int activityID , int [] employeeID , GameDataManager gm , PlayerDataManager pm )
        {
            string employeesName = "";
            for ( int i = 0; i < employeeID.Length; i++ )
            {
                int EID = employeeID[i];
                if ( EID < 0 )
                    continue;
                pm.activityScheduale.AddDeployedEmployee ( EID );
                employeesName += gm.workerList.Data [EID].name + ",";
            }
            employeesName = employeesName.Substring ( 0 , employeesName.Length - 1 );
            //string activityName = gm.activityList.Data [activityID].name;
            string activityName = Localizer.Translate ( gm.activityList.Data [activityID].name );
            string message = $"(員工活動){employeesName}委託{activityName}\n";
            int totalShift = pm.calender.TotalShift;
            pm.gameLog.AddActionLog ( totalShift , message );
        }

        public static int Chance ( int [] employeeID , GameData.Activity.ActivityType type , GameData.Activity.ActivityDifficult difficult , GameDataManager gm , PlayerDataManager pm )
        {
            int [] addition = gm.customData.Data.ActivityChance [(int)difficult].ToArray ();//取得該困難度該有的成功率
            int totalChance = 0;                                    //成功率加總
            for ( int i = 0; i < employeeID.Length; i++ )           //搜尋所有員工
            {
                int id = employeeID [i];                            //取得員工編號
                if ( id < 0 )
                    continue;
                int chance = 0;
                if ( type != GameData.Activity.ActivityType.StaminaAndLuck )           //如果不是特殊種類
                {
                    int ability = pm.employeeList.GetEmployee ( id ).GetAbility ( (int)type );//取得該種類的員工屬性階級
                    chance = addition [ability];                    //取得這個階級可以獲得多少成功率
                }
                else
                {
                    int ability1 = pm.employeeList.GetEmployee ( id ).GetAbility ( 0 );//取得員工的體力階級
                    int ability2 = pm.employeeList.GetEmployee ( id ).GetAbility ( 1 );//取得員工的運氣階級
                    chance = ( addition [ability1] + addition[ability2] ) / 2;     //相加除以二
                }
                totalChance += chance;                              //增加成功率
            }
            return totalChance;                                     //回傳成功率加總
        }
        public static bool Success ( int totalChance )
        {
            int ran = Random.Range ( 0 , 100 );
            return totalChance > ran;
        }
        static void LockGroup ( int id , GameDataManager gm , PlayerDataManager pm )
        {
            int group = gm.activityList.Data [id].group;
            if ( group <= 0 )
                return;
            List<int> lockID = new List<int> ();
            for ( int i = 0; i < gm.activityList.Data.Length; i++ )
            {
                if ( i != id && gm.activityList.Data [i].group == group )
                    lockID.Add ( i );
            }
            foreach ( int i in lockID )
            {
                pm.activityUnlock.SetMode ( i , ActivityUnlock.state.Group );
            }
        }
    }
}