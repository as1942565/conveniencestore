﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Action
{
    public static class Business
    {
        public static bool CheckMoney ( int spend , PlayerDataManager pm )      //檢查玩家能不能付錢
        {
            return pm.playerData.GetMoney () >= spend;
        }
        public static bool CheckUnlock ( int id , PlayerDataManager pm )        //檢查店長業務解鎖狀態
        {
            return pm.businessUnlock.GetUnlock ( id );
        }
        public static bool CheckAvailable ( int id , PlayerDataManager pm )      //檢查店長業務完成狀態
        {
            bool available = pm.businessUnlock.GetMode ( id ) == BusinessUnlock.state.Available;
            bool watch = pm.businessUnlock.GetMode ( id ) == BusinessUnlock.state.watch;
            return available || watch;
        }

        public static bool Run ( int id , PlayerDataManager pm , GameDataManager gm , NextShiftProcessor nextShift )
        {
            if ( !CheckUnlock ( id , pm ) )
            {
                Debug.LogError ( id + "店長業務尚未解鎖" );
                return false;
            }
            if ( !CheckAvailable ( id , pm ) )
            {
                Debug.LogError ( id + "店長業務目前無法執行" );
                return false;
            }
            int spend = gm.businessList.Data [id].cost;
            if ( !CheckMoney ( spend , pm ) )
            {
                Debug.LogError ( "玩家金錢低於" + spend );
                return false;
            }
            int day = pm.calender.GetDay ();
            pm.playerData.AddMoney ( -spend );                                  //玩家扣錢
            int totalShift = pm.calender.TotalShift;
            pm.gameLog.AddBusiness ( totalShift , spend );
            nextShift.Business ( id );                                          //執行店長業務                       
            return true;
        }
    }
}
