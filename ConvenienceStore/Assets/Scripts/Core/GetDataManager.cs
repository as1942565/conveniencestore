﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetDataManager : MonoBehaviour
{
    public static GetDataManager self;
    private void Awake ()
    {
        self = this;
    }

    private const string url = "http://ec2-18-218-8-65.us-east-2.compute.amazonaws.com:3000/test";

    public void SetData ( PlayerDataManager pm , GameDataManager gm )
    {
        StartCoroutine ( Run ( pm , gm ) );
    }

    public IEnumerator Run ( PlayerDataManager pm , GameDataManager gm )
    {
        WWWForm form = new WWWForm ();
        SpecialData data = GetData ( pm , gm );
        form.AddField ( "day" , data.day.ToString () );
        form.AddField ( "data1" , data.researchQuantity.ToString () );
        form.AddField ( "data2" , data.maxLevelCommodity );
        form.AddField ( "data3" , data.money );
        form.AddField ( "data4" , data.popular.ToString () );
        form.AddField ( "data5" , data.expandName );
        form.AddField ( "data6" , data.expandQuantity.ToString () );
        form.AddField ( "data7" , data.getPower );
        form.AddField ( "data8" , data.expandSpecialCabinet.ToString () );
        form.AddField ( "data9" , data.employeeName );
        form.AddField ( "guid" , data.guid );
        using ( WWW www = new WWW ( url , form ) )
        {
            yield return www;
        }
    }

    public SpecialData GetData ( PlayerDataManager pm , GameDataManager gm )
    {
        SpecialData data = new SpecialData ();
        //遊戲天數
        data.day = pm.calender.GetDay () - 2;
        data.researchQuantity = GetResearchQuantity ( pm );
        data.maxLevelCommodity = GetMaxLevelCommodity ( pm , gm );
        data.money = GetMoney ( pm );
        data.popular = GetPopular ( pm );
        data.expandName = GetExpand ( pm , gm );
        data.expandQuantity = GetExpandQuantity ( pm , gm );
        //data.getPower = GetPower ( pm );
        //第幾天擴建特殊商品櫃
        data.expandSpecialCabinet = pm.calender.GetExpandSpecialCabiinet ();
        data.employeeName = GetEmployeeNameOfHighestRelation ( pm , gm );
        //專屬ID
        data.guid = GameLoopManager.instance.om.guidData.GetGuid ();
        return data;
    }
    //研發商品數
    int GetResearchQuantity ( PlayerDataManager pm )
    {
        return pm.commodityItem.GetResearchQuantity ();
    }

    //最高等級商品(含數量)(-號分割)
    string GetMaxLevelCommodity ( PlayerDataManager pm , GameDataManager gm )
    {
        int length = gm.commodityList.Data.Length;
        int level = 0;
        List<string> commodityName = new List<string> ();
        List<int> quantity = new List<int> ();
        for ( int i = 0; i < length; i++ )
        {
            CommodityItem.Data data = pm.commodityItem.GetData ( i );
            if ( data.unlock && data.star > level )
            {
                commodityName.Clear ();
                quantity.Clear ();
                //commodityName.Add ( gm.commodityList.Data[i].name );
                commodityName.Add ( Localizer.Translate ( gm.commodityList.Data[i].name ) );
                quantity.Add ( pm.storage.GetQuantity ( i ) );
                level = data.star;
            }
            else if ( data.unlock && data.star == level )
            {
                //commodityName.Add ( gm.commodityList.Data[i].name );
                commodityName.Add ( Localizer.Translate ( gm.commodityList.Data[i].name ) );
                quantity.Add ( pm.storage.GetQuantity ( i ) );
            }
        }
        string result = "";
        for ( int i = 0; i < commodityName.Count; i++ )
        {
            result += $"{commodityName [i]} : {quantity [i]} 個-";
        }
        if ( result == "" )
        {
            Debug.LogError ( "沒有資料" );
            return "";
        }
        return result.Substring ( 0 , result.Length - 1 );
    }

    //最終金額
    string GetMoney ( PlayerDataManager pm )
    {
        return pm.playerData.GetMoney ().ToString ();
    }

    //最終人氣值
    int GetPopular ( PlayerDataManager pm )
    {
        return pm.playerData.GetPopular ();
    }

    //擴建設施(-號分割)
    string GetExpand ( PlayerDataManager pm , GameDataManager gm )
    {
        string name = "";
        for ( int i = 0; i < gm.storeExpansionList.Data.Length; i++ )
        {
            if ( pm.storeExpansionUnlock.GetComplete ( i ) )
            {
                name += gm.storeExpansionList.Data [i].name + "-";
            }
        }
        if ( name == "" )
        {
            Debug.LogError ( "無法讀取資料" );
            return "";
        }
        return name.Substring ( 0 , name.Length - 1 );
    }

    //設施數量
    int GetExpandQuantity ( PlayerDataManager pm , GameDataManager gm )
    {
        int quantity = 0;
        for ( int i = 0; i < gm.storeExpansionList.Data.Length; i++ )
        {
            if ( pm.storeExpansionUnlock.GetComplete ( i ) )
            {
                quantity++;
            }
        }
        return quantity;
    }

    //第幾天獲得新的行動點(-號分割)
    string GetPower ( PlayerDataManager pm )
    {
        int [] days = pm.calender.GetWhatDayToGetPower ();
        string daysStr = "";
        for ( int i = 0; i < daysStr.Length; i++ )
        {
            daysStr += days [i] + "-";
        }
        if ( daysStr == "" )
        {
            return "";
        }
        return daysStr.Substring ( 0 , daysStr.Length - 1 );
    }

    //哪位女主角好感度最高
    string GetEmployeeNameOfHighestRelation ( PlayerDataManager pm , GameDataManager gm )
    {
        int relationRank = -1;
        GameData.Worker [] workerList = gm.workerList.Data;
        List<string> EmployeeName = new List<string> ();
        for ( int i = 0; i < workerList.Length; i++ )
        {
            if ( workerList [i].isMain == false )
                continue;
            if ( pm.employeeList.GetEmployee ( i ).GetRelationRank () > relationRank )
            {
                EmployeeName.Clear ();
                EmployeeName.Add ( workerList [i].name );
                relationRank = pm.employeeList.GetEmployee ( i ).GetRelationRank ();
            }
            else if ( pm.employeeList.GetEmployee ( i ).GetRelationRank () == relationRank )
            {
                EmployeeName.Add ( workerList [i].name );
                relationRank = pm.employeeList.GetEmployee ( i ).GetRelationRank ();
            }
        }
        string result = "";
        for ( int i = 0; i < EmployeeName.Count; i++ )
        {
            result += EmployeeName [i] + "-";
        }
        if ( result == "" )
        {
            Debug.LogError ( "沒有資料" );
            return "";
        }
        return result.Substring ( 0 , result.Length - 1 );
    }
}
public struct SpecialData
{
    public int day;
    public int researchQuantity;
    public string maxLevelCommodity;
    public string money;
    public int popular;
    public string expandName;
    public int expandQuantity;
    public string getPower;
    public int expandSpecialCabinet;
    public string employeeName;
    public string guid;
}