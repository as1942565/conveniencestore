﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

[DataContract(Namespace= "")]
[System.Serializable]
public class SaveData
{
    [DataMember]
    [SerializeField]
    private SaveDataParam data;

    public void SetData ( int day , int money , int shift )
    {
        data.day = day + 1;
        data.money = money;
        data.shift = shift;
    }
    public SaveDataParam GetData ()
    {
        return data;
    }
}
[System.Serializable]
public struct SaveDataParam
{
    public int day;
    public int money;
    public int shift;
}
