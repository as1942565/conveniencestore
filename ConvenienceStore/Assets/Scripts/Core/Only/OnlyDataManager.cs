﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

[DataContract]
[KnownType(typeof(MainEmployee))]
[System.Serializable]
public class OnlyDataManager
{
    [DataMember]
    public UnlockCGData unlockCGData;
    [DataMember]
    public UnlockActorData unlockActorData;
    [DataMember]
    public GuidData guidData;
    [DataMember]
    public OptionData optionData;
    [DataMember]
    public AudioData audioData;

    public void Init ()
    {
        unlockCGData = new UnlockCGData ();
        unlockCGData.Init ();
        unlockActorData = new UnlockActorData ();
        unlockActorData.Init ();
        guidData = new GuidData ();
        guidData.Init ();
        optionData = new OptionData ();
        optionData.Init ();
        audioData = new AudioData ();
    }
}
