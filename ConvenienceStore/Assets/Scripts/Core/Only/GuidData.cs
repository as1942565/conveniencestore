﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization;

[DataContract(Namespace= "")]
[Serializable]
public class GuidData
{
    [DataMember]
    [SerializeField]
    private string guid;

    public void Init ()
    {
        guid = Guid.NewGuid().ToString ();
    }

    public string GetGuid ()
    {
        return guid;
    }
}
