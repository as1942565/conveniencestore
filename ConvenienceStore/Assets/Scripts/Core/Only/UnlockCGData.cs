﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;
using Tools;

[DataContract(Namespace= "")]
[System.Serializable]
public class UnlockCGData
{
    [DataMember]
    [SerializeField]
    private BoolList [] unlock;

    public void Init ()
    {
        unlock = new BoolList [4];
        for ( int e = 0; e < 3; e++ )
        {
            unlock [e] = new BoolList ();
            for ( int i = 0; i < 7; i++ )
            {
                unlock [e].Add ( false );
            }
        }
        unlock [3] = new BoolList ();
        unlock [3].Add ( false );
        unlock [3].Add ( false );
    } 

    public bool GetUnlock ( int employeeID , int id )
    {
        if ( GameLoopManager.instance.GetSteamCheck () == true )
            return true;
        if ( employeeID >= unlock.Length )
        {
            Debug.LogError ( employeeID + "超過女主角數量" );
            return false;
        }
        if ( id >= unlock[employeeID].Count )
        {
            Debug.LogError ( id + "超過CG數量" );
            return false;
        }
        return unlock [employeeID][id];
    }
    public bool GetUnlock ( int currectID )
    {
        int [] ids = GetCurrectID ( currectID );
        int employeeID = ids [0];
        int id = ids [1];
        if ( GameLoopManager.instance.GetSteam () == true )
            return true;
        if ( employeeID >= unlock.Length )
        {
            Debug.LogError ( employeeID + "超過女主角數量" );
            return false;
        }
        if ( id >= unlock[employeeID].Count )
        {
            Debug.LogError ( id + "超過CG數量" );
            return false;
        }
        return unlock [employeeID][id];
    }

    public void SetUnlock ( int employeeID , int id , bool u )
    {
        if ( employeeID >= unlock.Length )
        {
            Debug.LogError ( employeeID + "超過女主角數量" );
            return;
        }
        if ( id >= unlock[employeeID].Count )
        {
            Debug.LogError ( id + "超過CG數量" );
            return;
        }
        unlock [employeeID][id] = u;
    }
    public int GetUnlockScale ()
    {
        float quantity = 0;
        for ( int e = 0; e < unlock.Length; e++ )
        {
            for ( int i = 0; i < unlock[e].Count; i++ )
                if ( unlock [e][i] )
                quantity++;
        }
        if ( GameLoopManager.instance.GetSteam () == true )
            return Mathf.FloorToInt ( ( quantity / 12 )  * 100 );
        return Mathf.FloorToInt ( ( quantity / 18 )  * 100 );
    }
    public int [] GetCurrectID ( int id )
    {
        int employeeID = 0;
        while ( id > 0 || employeeID > unlock.Length )
        {
            //int tempID = id - unlock [employeeID].Count;
            int tempID = id - 6;
            if ( tempID < 0 )
            {
                break;
            }
            id = tempID;
            employeeID++;
        }
        if ( GameLoopManager.instance.GetSteam () == true && employeeID == 2 )
            employeeID = 3;
        return new int [] { employeeID , id };
    }

    public int GetUnlockLength ( int employeeID )
    {
        if ( employeeID >= unlock.Length )
        {
            Debug.LogError ( employeeID + "超過員工數量" );
            return 0;
        }
        int sum = 0;
        for ( int i = 0; i < unlock[employeeID].Count; i++ )
        {
            sum++;
        }
        return sum;
    }
    public int GetAllUnlockLength ()
    {
        if ( GameLoopManager.instance.GetSteam () == true )
            return 13;
        return 20;
        /*int sum = 0;
        for ( int t = 0; t < unlock.Length; t++ )
        {
            for ( int i = 0; i < unlock [t].Count; i++ )
            {
                if ( GameLoopManager.instance.GetSteam () == true && i == 2 )
                    continue;
                sum++;
            }
        }
        return sum;*/
    }
}
