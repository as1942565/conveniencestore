﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

[DataContract(Namespace= "")]
[System.Serializable]
public class UnlockActorData
{
    [DataMember]
    [SerializeField]
    private bool [] unlock;

    private int actorLength;

    private void ResetActorLength ()
    {
        actorLength = 25;
    }

    public void Init ()
    {
        unlock = new bool [30];
        for ( int i = 0; i < unlock.Length; i++ )
        {
            unlock [i] = false;
        }
    }

    public bool GetUnlock ( int id )
    {
        if ( GameLoopManager.instance.GetSteamCheck () == true )
            return true;
        if ( id >= unlock.Length )
        {
            Debug.LogError ( id + "超過角色數量" );
            return false;
        }
        if ( GameLoopManager.instance.GetSteam () == true )
        {
            if ( id >= 2 )
                id += 1;
            if ( id >= 4 )
                id += 1;
        }
        return unlock [id];
    }
    public void SetUnlock ( int id , bool u )
    {
        if ( id >= unlock.Length )
        {
            Debug.LogError ( id + "超過角色數量" );
            return;
        }
        unlock [id] = u;
    }
    public int GetUnlockScale ()
    {
        float quantity = 0;
        for ( int i = 0; i < unlock.Length; i++ )
        {
            if ( unlock [i] )
                quantity++;
        }
        //return Mathf.FloorToInt ( ( quantity / unlock.Length ) * 100 );
        if ( GameLoopManager.instance.GetSteam () == true )
            return Mathf.FloorToInt ( ( quantity / 22 ) * 100 );
        return Mathf.FloorToInt ( ( quantity / 24 ) * 100 );
    }
    public int GetActorLength ()
    {
        if ( actorLength == 0 )
            ResetActorLength ();
        return actorLength;
    }
}
