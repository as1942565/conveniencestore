﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NextShift
{
    public static class Trouble
    {
        public static void Run ( PlayerDataManager pm , Calender calender )
        {
            if ( calender.GetShift () == Calender.Shift.morning )
                return;

            if ( StoryManager.self.EnableTeach == true )
                return;

            int ran = Random.Range ( 0 , 100 );
            int chance = pm.troubleData.GetChance ();
            
            if ( ran > chance )
                return;
            
            GameLoopManager.Trouble = true;

            int storyID = pm.troubleData.GetStoryID ();
            if ( StoryManager.self != null )
            {
                string language = GameLoopManager.instance.om.optionData.GetLanguageStr ();
                StoryManager.self.StartStory ( $"Data/Story/{language}Trouble/Trouble" , ( storyID + 1 ).ToString () );
                pm.troubleData.SetWatchStory ( storyID , true );
            }
        }
    }
}
