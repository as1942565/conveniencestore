﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoticeMessage : MonoBehaviour
{
    public GameObject Mask;

    public static NoticeMessage self;
    private void Awake ()
    {
        self = this;
        Mask.SetActive ( false );
    }
    public void Run ( string [] msg )
    {
        StartCoroutine ( Show ( msg ) );
    }

    private float waitCD = 2f;
    private float MaxWaitCD = 0;
    IEnumerator Show ( string [] msg )
    {
        yield return new WaitForSeconds ( 0.5f );
        Mask.SetActive ( true );
        for ( int i = 0; i < msg.Length; i++ )
        {
            UI.MessageBox.Notice ( msg [i] );
            MaxWaitCD = Time.time + waitCD;
            while ( true )
            {
                if ( Time.time > MaxWaitCD )
                {
                    break;
                }
                if ( Input.GetMouseButtonDown ( 0 ) )
                {
                    break;
                }
                yield return null;
            }
            yield return null;
        }
        UI.MessageBox.self.NoticeBox.gameObject.SetActive ( false );
        Mask.SetActive ( false );
        GameLoopManager.instance.pm.storeLog.ClearNotice ();
        yield return null;
    }
}
