﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NextShift
{
    public static class StealCommodity
    {
        public static void Run ( PlayerDataManager pm , GameDataManager gm )
        {
            int [] soldCommodityIDs = pm.cabinet.GetAllSoldCommodity ();
            for ( int i = 0; i < soldCommodityIDs.Length; i++ )
            {
                int commodityID = soldCommodityIDs [i];

                int quantity = pm.storage.GetQuantity ( commodityID );
                if ( quantity == 0 )
                    continue;
                quantity -= pm.punishAddition.GetCommodityAddition ();
                if ( quantity < 0 )
                {
                    quantity = 0;
                    int cabinetType = (int)gm.commodityList.Data [commodityID].type;
                    int cabinetID = pm.cabinet.GetCabinetID ( commodityID );
                    pm.cabinet.SetCommodityID ( cabinetType , cabinetID , -1 );
                }
                pm.storage.SetQuantity ( commodityID , quantity );
            }
            pm.storeLog.AddNotice ( "已發生竊盜事件。" );
        }
    }
}
