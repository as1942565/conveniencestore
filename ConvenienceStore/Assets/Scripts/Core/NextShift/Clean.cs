﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NextShift
{
    public static class Clean
    {
        public static void Run ( PlayerDataManager pm , int day )
        {
            int clean = Formula.Clean.Calculate ( pm );
            if ( clean < 0 ) clean = 0;
            pm.playerData.SetClean ( clean );
            if ( clean <= 70 )
            {
                if ( !pm.storeLog.GetClean ( day ) )
                {
                    pm.gameLog.AddInformation ( day , Localizer.Translate ( "清潔度偏低，請盡速回復清潔度。" ) );
                    int shift = pm.calender.TotalShift;
                    pm.storeLog.SetClean ( shift , true );
                }
                int chance = 100 - clean;
                int ran = Random.Range ( 0 , 100 );
                if ( chance > ran )
                {
                    if ( StoryManager.self.EnableTeach )
                        return;
                    pm.punishAddition.SetLessSaleQuantityBool ( true );
                    pm.gameLog.AddInformation ( day , Localizer.Translate ( "已發生髒亂事件。" ) );
                    pm.storeLog.AddNotice ( "已發生髒亂事件。" );
                }
            }
        }
    }
}
