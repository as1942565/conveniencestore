﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NextShift
{
    public static class Police
    {
        public static void Run ( PlayerDataManager pm , int day )
        {
            int police = Formula.Police.Calculate ( pm );
            if ( police < 0 ) police = 0;
            pm.playerData.SetPolice ( police );
            if ( police < 50 )
            {
                if ( !pm.storeLog.GetPolice ( day ) )
                {
                    pm.gameLog.AddInformation ( day , Localizer.Translate ( "治安度偏低，請盡速回復治安度。" ) );
                    int shift = pm.calender.TotalShift;
                    pm.storeLog.SetPolice ( shift , true );
                }
                int chance = 100 - police;
                int ran = Random.Range ( 0 , 100 );
                if ( chance > ran )
                {
                    if ( StoryManager.self.EnableTeach )
                        return;
                    pm.punishAddition.SetLessCommodityBool ( true );
                    pm.gameLog.AddInformation ( day , Localizer.Translate ( "已發生竊盜事件。" ) );
                }
            }
        }
    }
}