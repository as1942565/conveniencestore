﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tools;
using System;
using Random = UnityEngine.Random;
namespace NextShift
{
    public static class StoreEvent
    {
        public static Action<int , int , bool> OnStoreEventRun;
        public static void Run ( GameDataManager gm , PlayerDataManager pm , int day )
        {
            int ran = Random.Range ( 0 , 100 );
            ran = 0;
            if ( ran >= gm.storeEventList.Data.Chance )
                return;

            List<int> employeeID = pm.employeeScheduleLog.GetWorkMainID ( day , pm.employeeList );
            if ( employeeID.Count == 0 )
                return;
            employeeID.Shuffle ();

            List<int> completeID = pm.storeExpansionUnlock.GetCompleteStoreEventID ( gm , employeeID [0] );
            if ( completeID.Count == 0 )
                return;

            int clean = pm.playerData.GetClean ();
            int police = pm.playerData.GetPolice ();
            bool niceEvent = clean + police >= gm.storeEventList.Data.Score;

            completeID.Shuffle ();
            pm.storeEventAddition.SetUnlock ( completeID [0] , true );
            OnStoreEventRun?.Invoke ( employeeID [0] , completeID [0] , niceEvent );
            //( $"第{yesterday + 1}天{employeeID [0]}號員工觸發{completeID [0]}號設施的突發事件 : {niceEvent}事件" );
        }
    }
}