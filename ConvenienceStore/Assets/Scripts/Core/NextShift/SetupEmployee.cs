﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tools;

namespace NextShift
{
    public static class SetupEmployee
    {
        public static void Run ( PlayerDataManager pm , GameDataManager gm )
        {
            EmployeeList employeeList = pm.employeeList;
            int day = pm.calender.GetDay ();

            Rest ( pm , day , employeeList );

            Work ( gm , pm , day , employeeList );
        }

        static void Rest ( PlayerDataManager pm , int day , EmployeeList employeeList )
        {
            int [] restID = pm.employeeScheduleLog.GetRestEmployeeIDs ( day , employeeList );
            for ( int i = 0; i < restID.Length; i++ )
            {
                int id = restID [i];
                employeeList.GetEmployee ( id ).ResetPower ();
            }
        }

        static void Work ( GameDataManager gm , PlayerDataManager pm , int day , EmployeeList employeeList )
        {
            int scheduleLength = gm.schedulingCabinetList.Data.Length;
            string [] log = new string [scheduleLength];
            if ( scheduleLength > 3 )
            {
                Debug.LogError ( scheduleLength + "超過班次種類長度" );
                scheduleLength = 3;
            }
            for ( int s = 0; s < scheduleLength; s++ )
            {
                int [] employeeIDs = pm.employeeScheduleLog.GetEmployeeIDWithShift ( day , s , employeeList );
                if ( employeeIDs.Length == 0 )
                {
                    NoSchedule ( pm , s );
                    continue;
                }
                for ( int i = 0; i < employeeIDs.Length; i++ )
                {
                    int id = employeeIDs [i];
                    Employee employee = employeeList.GetEmployee ( id );
                    employee.UsePower ();
                    log [s] += gm.workerList.Data [id].name + "\t";
                    if ( gm.workerList.Data[id].isMain )                    //如果該員工是女主角
                        employeeList.AddRelationEXP ( id , 2 , gm , pm );        //給該編號員工增加好感度
                    /*if ( employee.GetPower () == 0 )
                    {
                        pm.employeeScheduleLog.ResetEmployeeID ( day , id );
                    }*/
                }
            }
        }

        static void NoSchedule ( PlayerDataManager pm , int shift )
        {
            int day = pm.calender.GetDay ();
            string [] shiftName = new string [] { "早班" , "晚班" , "大夜班" };
            if ( !pm.storeLog.GetSchedule ( day , shift ) )
            {
                pm.gameLog.AddInformation ( day , Localizer.Translate ( "今日" + shiftName [shift] + "無人值班。" ) );
                pm.storeLog.SetSchedule ( day , shift , true );
            }
        }
    }
}
