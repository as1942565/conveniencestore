﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Formula;

//切換班次
public class NextShiftProcessor
{                                                                       //
    private PlayerDataManager pm;                                       //紀錄玩家資料管理器
    private GameDataManager gm;                                         //紀錄遊戲資料管理器
    public event System.Action OnResearch;
    public event System.Action OnHireWorker;

    public NextShiftProcessor ( PlayerDataManager pm , GameDataManager gm  )//建構式
    {
        this.gm = gm;
        UpdatePM ( pm );
    }

    public void UpdatePM(PlayerDataManager pm )
    {
        this.pm = pm;
        pm.calender.AfterNextShift -= AfterNextShift;
        pm.calender.AfterNextShift += AfterNextShift;
        pm.calender.BeforeNextShift -= BeforeNextShift;
        pm.calender.BeforeNextShift += BeforeNextShift;
    }

    private void BeforeNextShift ( Calender calender )
    {
        Value_Anime.self.Original_Value = pm.playerData.GetMoney ();

        NextShift.Commodity.Run ( pm , gm );
        int day = pm.calender.GetDay ();
        pm.gameLog.CalculateMVPIncome ( day , gm );
        pm.gameLog.CalculateMVPPopular ( day , gm );
        NextShift.CreateSpecialPeople.Run ( pm , gm );
    }
    private void AfterNextShift ( Calender calender )                   //主要邏輯區域 : 切換班次後該做甚麼事
    {
        pm.punishAddition.SetLessSaleQuantityBool ( false );
        pm.punishAddition.SetLessCommodityBool ( false );
        int day = pm.calender.GetDay ();
        int yesterday = pm.calender.GetYesterday ();

        int totalShift = pm.calender.TotalShift;
        pm.activityAddition.CheckIncome ( totalShift );
        pm.activityAddition.CheckSaleQuantity ( totalShift );
        pm.businessAddition.CheckIncome ( totalShift , gm , pm );
        pm.businessAddition.CheckSaleQuantity ( totalShift );

        int popular = Popular.Calculate ( pm , gm );
        if ( popular < 0 ) popular = 0;

        pm.playerData.SetPopular ( popular );                           //更新人氣值
        NextShift.Clean.Run ( pm , day );                               //更新清潔度
        NextShift.Police.Run ( pm , day );                              //更新治安度
        
        for ( int i = 0; i < gm.commodityList.Data.Length; i++ )        //搜尋所有商品
        {
            if ( pm.commodityItem.GetData ( i ).star >= 2 )
                continue;
            if ( pm.commodityItem.GetImproveUnlock ( i , gm ) )         //如果可以改良
            {
                string commodityName = gm.commodityList.Data [i].name;  //取得商品名稱
                string msg = commodityName + "可以開始改良。";
                if ( !pm.storeLog.GetImprove ( day , i ) )
                {
                    pm.gameLog.AddInformation ( day , Localizer.Translate ( msg ) );
                    pm.storeLog.SetImprove ( totalShift , i , true );
                }
                if ( pm.storeLog.GetCanUpdateNotice ( i ) == true )
                        continue;
                    pm.storeLog.AddNotice ( msg );
                    pm.storeLog.SetCanUpdateNotice ( i , true );
            }
        }
        int shift = (int)calender.GetShift ();                          //取得班次
        if ( calender.GetShift () == Calender.Shift.morning )           //如果是早班
        {
            if ( StoryManager.self != null && !StoryManager.self.EnableTeach )
                ChangeDay.self.Loading ();
            pm.gameLog.SetTotalMoney ( day , pm.playerData.GetMoney () );
            //排班的員工要增加好感度並且減少體力值 / 沒排班的員工全部回復體力值
            NextShift.SetupEmployee.Run ( pm , gm );
            if ( StoreEventManager.self.Icon != null )
            {
                GameObject.Destroy ( StoreEventManager.self.Icon , 0.5f );
                StoreEventManager.self.Icon = null;
            }
        }
        if ( pm.punishAddition.GetLessCommodityBool () )
        {
            NextShift.StealCommodity.Run ( pm , gm );
        }

        //pm.activityScheduleLog.AddSchedules ( day , pm.activityScheduale );
        //檢查有沒有完成的員工活動
        pm.activityScheduale.Run ( pm , gm , calender.TotalShift );
        if ( GameLoopManager.instance.levelManager != null )
            GameLoopManager.instance.levelManager.ChangeShift ( pm.businessUnlock.GetLevel () , (int)pm.calender.GetShift () , gm );
        NextShift.StoreEvent.Run ( gm , pm , day );
        if ( PeopleManager.self != null )
        PeopleManager.self.Init ( gm , pm );
        if ( StoryManager.self != null && !StoryManager.self.EnableTeach )
        {
            NoticeMessage.self.Run ( pm.storeLog.GetNotice () );
            GameLoopManager.instance.StartCoroutine ( PlayEffect () );
        }
        /*if ( StoryManager.self != null && !StoryManager.self.EnableTeach )
        {
            GetDataManager.self.SetData ( pm , gm );
        }*/

        AutoSave ();

        
    }
    IEnumerator PlayEffect ( )
    {
        while ( ChangeDay.self.playAnim == true )
        {
            yield return null;
        }

        Value_Anime.self.Final_Value = pm.playerData.GetMoney ();
        if ( Value_Anime.self.AddMoney () == true )
            Value_Anime.self.gameObject.SetActive ( true );
        else
            Observer.PlayerStatusPanel.self.Fade ();
        yield return null;
    }

    void AutoSave ()
    {
        if ( StoryManager.self.EnableTeach )
            return;
        SaveLoadManager.Save ( pm , "Auto" );
        SaveData saveData = new SaveData ();
        saveData.SetData ( pm.calender.GetDay () , pm.playerData.GetMoney () , (int)pm.calender.GetShift () );

        SaveLoadManager.SaveDataSave ( saveData , "Auto" );
        ScreenShot.self.SaveScreenShot ( null , "Auto" );
    }

    public void Research ( int ID )                                     //商品研發
    {
        pm.commodityItem.SetUnlock ( ID );                              //商品解鎖
        int defaultQuantity = gm.commodityList.Data [ID].quantity;      //取得預設數量
        int totalShift = pm.calender.TotalShift;
        pm.storage.AddQuantity ( ID , defaultQuantity , totalShift );                //該商品的存貨增加
        if ( OnResearch != null )
        {
            OnResearch ();
        }
        pm.calender.UseEnergy ();                                       //消耗行動點
    }

    public void Improve ( int ID , int star )                           //商品改良
    {
        int improveStar = star + 1;
        pm.commodityItem.SetStar ( ID , improveStar );                  //設定該商品的星級
        if ( improveStar > 3 )
        {
            Debug.LogError ( improveStar + "超過商品星級" );
            return;
        }
        int sale = gm.commodityList.Data [ID].price [improveStar];       //取得升級後的售價
        pm.commodityItem.SetSale ( ID , sale );                         //設定該星級的售價
        int saleQuantity = pm.commodityItem.GetData ( ID ).saleQuantity;
        pm.commodityItem.SetSaleQuantityAfterUpdate ( ID , saleQuantity );
        pm.storeLog.SetCanUpdateNotice ( ID , false );
        //pm.calender.UseEnergy ();                                       //消耗行動點
        if ( improveStar >= 3 )
            return;
        //int condition = gm.commodityList.Data [ID].ImproveCondition [improveStar];
        //pm.commodityItem.SetImproveCondition ( ID , improveStar , saleQuantity + condition );
    }

    public void Business ( int ID )                                     //店長業務
    {
        BusinessBaseEvent [] events = gm.businessList.GenerateEvent ( ID );//生成事件
        
        int groupID = gm.businessList.Data [ID].groupID;
        if ( groupID > 0 )
            pm.businessUnlock.GroupSetMode ( groupID , BusinessUnlock.state.groupLock );
        foreach ( BusinessBaseEvent e in events )                       //搜尋所有事件
        {
            if ( e != null )
            {
                e.DoEvent ( pm , gm );                                  //執行事件
            }
        }
        bool hireWorker = ID == 11;
        if ( hireWorker )
        {
            if ( OnHireWorker != null )
            {
                OnHireWorker ();
            }
            return;
        }
        if ( gm.businessList.Data[ID].eventType == 0 )
        {
            pm.calender.UseEnergy ();                                   //消耗行動點
        }
    }

    public void StoreExpansion ( int [] IDs )                               //店面擴建
    {
        PlayButtonAudio.self.PlayExpend ();
        for ( int i = 0; i < IDs.Length; i++ )
        {
            int ID = IDs [i];
            StoreExpansionBaseEvent [] events = gm.storeExpansionList.GerenateEvent ( ID );//生成事件
            foreach ( StoreExpansionBaseEvent e in events )                 //搜尋所有事件
            {
                if ( e != null )
                    e.DoEvent ( pm , gm );                                  //執行事件
            }
            pm.storeExpansionUnlock.SetComplete ( ID , true );              //該編號店面擴建已完成
        }
        pm.calender.UseEnergy ();                                       //消耗行動點
    }

    public void CreatExpansion ( int [] IDs )
    {
        for ( int i = 0; i < IDs.Length; i++ )
        {
            int ID = IDs [i];
            StoreExpansionBaseEvent [] events = GameLoopManager.instance.gm.storeExpansionList.GerenateEvent ( ID );//生成事件
            foreach ( StoreExpansionBaseEvent e in events )                 //搜尋所有事件
            {
                if ( e != null )
                    e.DoEvent ( GameLoopManager.instance.pm , GameLoopManager.instance.gm );                                  //執行事件
            }
            GameLoopManager.instance.pm.storeExpansionUnlock.SetComplete ( ID , true );              //該編號店面擴建已完成
            if ( ExpansionManager.self != null )
                ExpansionManager.self.CreateItem ( ID , GameLoopManager.instance.gm );
        }
    }

    public void SpecialEA ( int ID , int [] workIDs , bool successed )  //員工活動(店長指導)
    {
        ActivityBaseEvent [] events = gm.activityList.GenerateEvent ( ID );                           //生成並執行事件
        for ( int i = 0; i < events.Length; i++ )
        {
            events [i].DoEvent ( pm , gm );
        }
        pm.calender.UseEnergy ();                                       //消耗行動點
    }
}
