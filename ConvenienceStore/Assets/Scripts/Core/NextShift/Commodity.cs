﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NextShift
{
    public static class Commodity 
    {
        public static event System.Action<string> OnNextShiftChanged;

        public static void Run ( PlayerDataManager pm , GameDataManager gm )
        {                                                                       //
            
            //賣掉的商品換算成金錢
            for ( int t = 0; t < gm.cabinetList.Data.Length; t++ )              //搜尋所有商品櫃種類
            {
                for ( int i = 0; i < pm.cabinet.GetTypeUnlockNum ( t ); i++ )   //搜尋所有商品欄位
                {
                    if ( pm.cabinet.GetCommodityID ( t , i ) == -1 )                     //如果商品欄位有商品編號
                        continue;
                    int commodityID = pm.cabinet.GetCommodityID ( t , i );               //取得商品編號
                    GameData.Commodity.CommodityType commodityType = gm.commodityList.Data [commodityID].type;//取得商品櫃欄位
                    int cabinetID = i;                                          //取得商品櫃種類
                    int cabinetType = t;                                        //取得商品種類
                    int sold = SaleCommodity ( commodityID , commodityType , cabinetID , cabinetType , pm , gm );//賣出商品
                }
            }
        }

        private static int SaleCommodity ( int commodityID , GameData.Commodity.CommodityType commodityType , int cabinetID , int cabinetType , PlayerDataManager pm , GameDataManager gm )//賣出商品
        {
            int day = pm.calender.GetDay ();                                    //取得目前天數
            int saleQuantity = Formula.CommoditySaleQuantity.Calculate ( pm , gm , commodityID , cabinetType , commodityType );//取得商品銷售數量
            if ( pm.punishAddition.GetLessSaleQuantityBool () )
            {
                saleQuantity = Mathf.FloorToInt ( saleQuantity * pm.punishAddition.GetSaleQuantityAddition () );
            }
            int quantity = pm.storage.GetQuantity ( commodityID );              //取得該商品持有數
            string commodityName = gm.commodityList.Data [commodityID].name;    //取得商品名稱
            if ( saleQuantity >= quantity )                                     //如果銷售數量大於等於持有數
            {
                saleQuantity = quantity;
                pm.cabinet.SetCommodityID ( cabinetType , cabinetID , -1 );     //賣完後自動下架
                if ( !pm.storeLog.GetSold ( day , commodityID ) )
                {
                    pm.gameLog.AddInformation ( day , Localizer.Translate ( commodityName + "販售完畢。" ) );
                    int shift = pm.calender.TotalShift;
                    pm.storeLog.SetSold ( shift , commodityID , true );
                }
            }
            //紀錄賣掉的商品
            int totalShift = pm.calender.TotalShift;
            pm.gameLog.AddCommoditySaleQuantity ( totalShift , commodityID , saleQuantity );
            pm.commodityItem.AddSaleQuantity ( commodityID , saleQuantity );    //增加該商品的銷售數量(改良前置)

            //扣除賣掉的商品
            pm.storage.AddQuantity ( commodityID , -saleQuantity , totalShift );             //減少該商品的持有數

            //轉換成金錢
            int money = saleQuantity * pm.commodityItem.GetData ( commodityID ).sale;//取得每項商品的商品收入
            int getMoney = Mathf.RoundToInt ( money * Formula.CommodityIncome.Calculate ( commodityType , pm ) );    //加上加成效果的商品收益
            pm.gameLog.AddCommodityIncome ( totalShift , commodityID , getMoney );
            pm.playerData.AddMoney ( getMoney );                                //玩家賺錢

            string saleCommodtyLog = "";
            if ( OnNextShiftChanged != null )
            {
                if ( saleQuantity > 0 )
                {
                    string [] typeName = new string [] { "食" , "飲" , "雜" , "特" };
                    string message = $"({typeName [(int)commodityType]}){commodityName}\t\t賣掉 : {saleQuantity.ToString ()}個 \t 賺取{getMoney.ToString ( "N0" )}元";
                    saleCommodtyLog += message + "\n";
                }
                OnNextShiftChanged ( saleCommodtyLog );
            }
            return saleQuantity;
        }
    }
}
