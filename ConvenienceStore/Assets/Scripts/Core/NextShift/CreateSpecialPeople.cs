﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tools;

namespace NextShift
{
    public static class CreateSpecialPeople
    {
        public static void Run ( PlayerDataManager pm , GameDataManager gm )
        {
            if ( StoryManager.self.EnableTeach )
            {
                GameLoopManager.CanAfterNextShift = true;
                return;
            }
            int level = pm.businessUnlock.GetLevel ();
            int [] levelChance = gm.customData.Data.levelChance;
            int ran = Random.Range ( 0 , 100 );
            ran = 0;
            if ( levelChance [level] <= ran )
            {
                GameLoopManager.CanAfterNextShift = true;
                return;
            }

            int [] peopleID = pm.specialPeople.GetAllLock ();
            if ( peopleID.Length == 0 )
            {
                GameLoopManager.CanAfterNextShift = true;
                return;
            }
            peopleID.Shuffle ();
            int id = peopleID [0];
            pm.specialPeople.SetUnlock ( id , true );

            string name = gm.specialPeopleList.Data.peopleData [id].name_E;
            Achievement.SetAchievement ( name );
            SpecialPeopleManager.self.Create ( name );
        }
        public static void After ()
        {
            GameLoopManager.instance.pm.calender.After ();
        }
    }
}
