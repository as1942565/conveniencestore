﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NextShift
{
    public static class Restock
    {
        public static void Run ( PlayerDataManager pm , GameDataManager gm )
        {                                                                       //
            int totalShift = pm.calender.TotalShift;
            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )            //搜尋所有商品
            {
                int quantity = pm.purchase.GetDefinePurchase ( i );             //取得該商品的等待進貨數量
                if ( quantity != 0 )
                {
                    pm.storage.AddQuantity ( i , quantity , totalShift );                    //增加該商品的持有數
                }
                pm.purchase.ClearDefinePurchase ( i );                          //清除確定進貨數量
            }
        }
    }
}
