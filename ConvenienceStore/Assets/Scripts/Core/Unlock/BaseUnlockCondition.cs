﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseUnlockCondition
{

    public BaseUnlockCondition()
    {

    }

    public BaseUnlockCondition ( string [] param , int unlockItemID )
    {
        ParseParam ( param );
        this.unlockItemID = unlockItemID;
    }
    protected abstract void ParseParam ( string [] param );
    public abstract void Register ( GameDataManager gm , PlayerDataManager pm );
    public abstract void Unregister ( GameDataManager gm , PlayerDataManager pm );
    public abstract bool IsUnlocked ( GameDataManager gm , PlayerDataManager pm );
    protected int unlockItemID;
    public abstract int ParamLength();
}

//客製化的解鎖條件判定
public abstract class BaseUnlockCondition<T> : BaseUnlockCondition
{
    protected GameDataManager _gm;
    protected PlayerDataManager _pm;

    protected BaseUnlockCondition():base()
    {

    }
    public BaseUnlockCondition ( string [] param , int unlockItemID ) : base ( param , unlockItemID )
    {
    }

    protected void Run ( T list )                                                   //執行解鎖條件判定
    {
        if ( Check ( list ) )                                                       //如果達成解鎖條件
        {
            doEvent ( _gm , _pm );                                                  //執行解鎖動作
            //Unregister ( _gm , _pm );                                               //移除這個解鎖判定
        }
        else
            removeEvent ( _gm , _pm );
    }
    public override void Register ( GameDataManager gm , PlayerDataManager pm )     //新增檢查特殊解鎖條件
    {
        SetAction( gm , pm , Run);                                              //取得解鎖判定項目
        _gm = gm;
        _pm = pm;
    }
    public override void Unregister ( GameDataManager gm , PlayerDataManager pm )   //移除檢查特殊解鎖條件
    {
        UnsetAction( gm , pm ,Run);                                              //取得解鎖判定項目
        _gm = null;
        _pm = null;
    }

    protected abstract void SetAction ( GameDataManager gm , PlayerDataManager pm,System.Action<T> action );//取得解鎖判定項目
    protected abstract void UnsetAction (GameDataManager gm , PlayerDataManager pm,System.Action<T> action );//取得解鎖判定項目
    protected abstract void doEvent ( GameDataManager gm , PlayerDataManager pm );  //執行解鎖動作
    protected abstract void removeEvent ( GameDataManager gm , PlayerDataManager pm );
    protected abstract bool Check ( T list );                                       //檢查解鎖條件
}