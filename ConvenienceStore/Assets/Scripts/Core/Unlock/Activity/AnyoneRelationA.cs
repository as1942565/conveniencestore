﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Condition
{
    public class AnyoneRelationA : BaseUnlockCondition<EmployeeList>
    {
        public AnyoneRelationA() : base()
        {

        }
        public AnyoneRelationA ( string [] param , int unlockItemID ) : base ( param , unlockItemID )
        {
        }

        protected override void ParseParam ( string [] param )
        {
            relation = int.Parse ( param [0] );
        }

        private int relation;

        protected override bool Check ( EmployeeList list )
        {
            int [] id = list.GetAllUnlockID ();
            for ( int i = 0; i < id.Length; i++ )
            {
                Employee employee = list.GetEmployee ( id [i] );
                if ( employee.GetRelationRank() >= relation )
                {
                    return true;
                }
            }
            return false;
        }

        protected override void doEvent ( GameDataManager gm , PlayerDataManager pm )
        {
            pm.activityUnlock.SetUnlock ( unlockItemID , true );
        }

        protected override void SetAction(GameDataManager gm, PlayerDataManager pm, Action<EmployeeList> action)
        {
            pm.employeeList.OnAddRelation+= action;
        }

        public override bool IsUnlocked ( GameDataManager gm , PlayerDataManager pm )
        {
            return pm.activityUnlock.GetUnlock ( unlockItemID );
        }

        protected override void UnsetAction(GameDataManager gm, PlayerDataManager pm, Action<EmployeeList> action)
        {
            pm.employeeList.OnAddRelation -= action;
        }

        public override int ParamLength()
        {
            return 1;
        }

        protected override void removeEvent ( GameDataManager gm , PlayerDataManager pm )
        {
        }
    }

}
