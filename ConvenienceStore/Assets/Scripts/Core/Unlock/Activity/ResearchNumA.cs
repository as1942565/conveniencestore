﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Condition
{
    public class ResearchNumA : BaseUnlockCondition<CommodityItem>
    {
        public ResearchNumA() : base()
        {

        }
        public ResearchNumA ( string [] param , int unlockItemID ) : base ( param , unlockItemID )
        {
        }

        protected override void ParseParam ( string [] param )
        {
            researchNum = int.Parse ( param [0] );
            type = int.Parse ( param [1] );
        }

        private int researchNum;                                                //需要到達的研發數
        private int type;                                                       //被檢查的商品種類

        public override bool IsUnlocked ( GameDataManager gm , PlayerDataManager pm )
        {                                                                       //
            return pm.activityUnlock.GetUnlock ( unlockItemID );
        }
        protected override bool Check ( CommodityItem list )
        {
            int researchNum = list.GetResearchNumByType ( _gm , type );
            //Debug.Log($"{researchNum} >={ this.researchNum} ");
            return researchNum >= this.researchNum;
        }
        protected override void doEvent ( GameDataManager gm , PlayerDataManager pm )
        {
            //Debug.Log($"Unlock id: {unlockItemID}");
            pm.activityUnlock.SetUnlock ( unlockItemID , true );
        }
        protected override void SetAction ( GameDataManager gm , PlayerDataManager pm , Action<CommodityItem> action)
        {
            pm.commodityItem.OnItemUnlock += action;
        }

        protected override void UnsetAction(GameDataManager gm, PlayerDataManager pm, Action<CommodityItem> action)
        {
            pm.commodityItem.OnItemUnlock -= action;
        }

        public override int ParamLength()
        {
            return 2;
        }

        protected override void removeEvent ( GameDataManager gm , PlayerDataManager pm )
        {
        }
    }
}
