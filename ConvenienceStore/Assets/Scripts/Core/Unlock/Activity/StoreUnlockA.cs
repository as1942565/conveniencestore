﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Condition
{
    public class StoreUnlockA : BaseUnlockCondition<StoreExpansionUnlock>
    {
        public StoreUnlockA() : base()
        {

        }
        public StoreUnlockA ( string [] param , int unlockItemID ) : base ( param , unlockItemID )
        {
        }

        protected override void ParseParam ( string [] param )
        {
            storeID = int.Parse ( param [0] );
        }

        private int storeID;

        public override bool IsUnlocked ( GameDataManager gm , PlayerDataManager pm )
        {
            return pm.activityUnlock.GetUnlock ( unlockItemID );
        }

        protected override bool Check ( StoreExpansionUnlock list )
        {
            return list.GetComplete ( storeID );
        }

        protected override void doEvent ( GameDataManager gm , PlayerDataManager pm )
        {
            pm.activityUnlock.SetUnlock ( unlockItemID , true );
        }

    
        protected override void SetAction(GameDataManager gm, PlayerDataManager pm, Action<StoreExpansionUnlock> action)
        {
            pm.storeExpansionUnlock.OnItemComplete += action;
        }

        protected override void UnsetAction(GameDataManager gm, PlayerDataManager pm, Action<StoreExpansionUnlock> action)
        {
            pm.storeExpansionUnlock.OnItemComplete -= action;
        }

        public override int ParamLength()
        {
            return 1;
        }

        protected override void removeEvent ( GameDataManager gm , PlayerDataManager pm )
        {
        }
    }
}
