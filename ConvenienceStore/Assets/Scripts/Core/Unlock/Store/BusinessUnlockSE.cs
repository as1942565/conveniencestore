﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Condition
{
    public class BusinessUnlockSE : BaseUnlockCondition<BusinessUnlock>
    {
        public BusinessUnlockSE() : base()
        {

        }
        public BusinessUnlockSE ( string [] param , int unlockItemID ) : base ( param , unlockItemID )
        {
        }

        protected override void ParseParam ( string [] param )
        {
            businessID = int.Parse ( param [0] );
        }

        private int businessID;

        public override bool IsUnlocked ( GameDataManager gm , PlayerDataManager pm )
        {
            return pm.storeExpansionUnlock.GetUnock ( unlockItemID );
        }

        protected override bool Check ( BusinessUnlock list )
        {
            return list.GetMode ( businessID ) != BusinessUnlock.state.Available;
        }

        protected override void doEvent ( GameDataManager gm , PlayerDataManager pm )
        {
            pm.storeExpansionUnlock.SetUnlock ( unlockItemID , true );
        }

        protected override void SetAction ( GameDataManager gm , PlayerDataManager pm , Action<BusinessUnlock> action )
        {
            pm.businessUnlock.OnItemComplete += action;
        }

        protected override void UnsetAction ( GameDataManager gm , PlayerDataManager pm , Action<BusinessUnlock> action )
        {
            pm.businessUnlock.OnItemComplete -= action;
        }

        public override int ParamLength()
        {
            return 1;
        }

        protected override void removeEvent ( GameDataManager gm , PlayerDataManager pm )
        {
        }
    }
}
