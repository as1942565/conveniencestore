﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Condition
{
    public class EmployeeScheduleB : BaseUnlockCondition<Calender>
    {
        public EmployeeScheduleB () : base ()
        {

        }
        public EmployeeScheduleB ( string [] param , int unlockItemID ) : base ( param , unlockItemID )
        {

        }

        protected override void ParseParam ( string [] param )
        {
            employeeID = int.Parse ( param [0] );
            relation = int.Parse ( param [1] );
            conditionID = int.Parse ( param [2] );
        }

        private int employeeID;
        private int relation;
        private int conditionID;

        private int [] minDay = new int [] { 15 , 8 , 24 };
        private int [] maxDay = new int [] { 23 , 14 , 30 };

        public override bool IsUnlocked ( GameDataManager gm , PlayerDataManager pm )
        {
            return pm.businessUnlock.GetUnlock ( unlockItemID );
        }

        protected override bool Check ( Calender calender )
        {
            bool trueShift = calender.GetShift () != Calender.Shift.morning;
            int day = calender.GetDay ();
            bool trueDay = minDay [employeeID] <= day && day <= maxDay [employeeID];
            int shift = (int)calender.GetShift ();
            bool isWork = GameLoopManager.instance.pm.employeeScheduleLog.IsWorkAtNight ( day , employeeID , shift );
            /*Debug.Log ( $"trueShift : {trueShift} " +
                $"employeeID : {employeeID} minDay : {minDay [employeeID]} maxDay {maxDay[employeeID]} day {day} " +
                $"shift : " + shift );*/

            return trueShift && trueDay && isWork;
        }

        protected override void doEvent ( GameDataManager gm , PlayerDataManager pm )
        {
            pm.businessUnlock.SetUnlock ( unlockItemID , conditionID , true );

            if ( pm.businessUnlock.GetUnlock ( unlockItemID ) == false )
                return;

            string [] name = new string [] { "顏無菁" , "夜蓮心" , "笭伊姍" };
            pm.storeLog.AddNotice ( $"已發生{name [employeeID]}的生理期事件。" );
            GameLoopManager.SpecialDay = true;
            if ( StoryManager.self != null )
            {
                string language = GameLoopManager.instance.om.optionData.GetLanguageStr ();
                StoryManager.self.StartStory ( $"Data/Story/{language}SpecialDay/Employee" , employeeID.ToString () );
            }
        }

        protected override void SetAction ( GameDataManager gm , PlayerDataManager pm , Action<Calender> action )
        {
            pm.calender.CheckSpecialDay += action;
        }

        protected override void UnsetAction ( GameDataManager gm , PlayerDataManager pm , Action<Calender> action )
        {
            pm.calender.CheckSpecialDay -= action;
        }

        public override int ParamLength ()
        {
            return 3;
        }

        protected override void removeEvent ( GameDataManager gm , PlayerDataManager pm )
        {
            pm.businessUnlock.SetUnlock ( unlockItemID , conditionID , false );
        }
    }
}