﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Condition
{
    public class CommodityStarB : BaseUnlockCondition<CommodityItem>
    {
        public CommodityStarB() : base()
        {

        }
        public CommodityStarB ( string [] param , int unlockItemID ) : base ( param , unlockItemID )
        {
        }

        protected override void ParseParam ( string [] param )
        {
            conditionID = int.Parse ( param [0] );
            commodityID = int.Parse ( param [1] );
        }

        private int conditionID;
        private int commodityID;

        public override bool IsUnlocked ( GameDataManager gm , PlayerDataManager pm )
        {
            return pm.businessUnlock.GetUnlock ( unlockItemID );
        }

        protected override bool Check ( CommodityItem list )
        {
            return list.GetData ( commodityID ).star >= 3;
        }

        protected override void doEvent ( GameDataManager gm , PlayerDataManager pm )
        {
            pm.businessUnlock.SetUnlock ( unlockItemID , conditionID , true );
        }

        protected override void SetAction(GameDataManager gm, PlayerDataManager pm, Action<CommodityItem> action)
        {
            pm.commodityItem.OnMaxStar +=action;
        }

        protected override void UnsetAction(GameDataManager gm, PlayerDataManager pm, Action<CommodityItem> action)
        {
            pm.commodityItem.OnMaxStar -=action;
        }

        public override int ParamLength()
        {
            return 2;
        }

        protected override void removeEvent ( GameDataManager gm , PlayerDataManager pm )
        {
        }
    }
}
