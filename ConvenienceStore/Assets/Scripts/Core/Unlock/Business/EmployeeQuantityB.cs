﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Condition
{
    public class EmployeeQuantityB : BaseUnlockCondition<EmployeeList>
    {
        public EmployeeQuantityB() : base()
        {

        }
        public EmployeeQuantityB ( string [] param , int unlockItemID ) : base ( param , unlockItemID )
        {
        }

        protected override void ParseParam ( string [] param )
        {
            employeeQuantity = int.Parse ( param [0] );
            conditionID = int.Parse ( param [1] );
        }

        private int employeeQuantity;
        private int conditionID;


        public override bool IsUnlocked ( GameDataManager gm , PlayerDataManager pm )
        {
            return pm.businessUnlock.GetUnlock ( unlockItemID );
        }

        protected override bool Check ( EmployeeList list )
        {
            return list.GetAllUnlockID ().Length >= employeeQuantity;
        }

        protected override void doEvent ( GameDataManager gm , PlayerDataManager pm )
        {
            pm.businessUnlock.SetUnlock ( unlockItemID , conditionID , true );
        }

        protected override void SetAction(GameDataManager gm, PlayerDataManager pm, Action<EmployeeList> action)
        {
            pm.employeeList.OnAddEmployee += action;
        }

        protected override void UnsetAction(GameDataManager gm, PlayerDataManager pm, Action<EmployeeList> action)
        {
            pm.employeeList.OnAddEmployee -= action;
        }

        public override int ParamLength()
        {
            return 2;
        }

        protected override void removeEvent ( GameDataManager gm , PlayerDataManager pm )
        {
        }
    }
}
