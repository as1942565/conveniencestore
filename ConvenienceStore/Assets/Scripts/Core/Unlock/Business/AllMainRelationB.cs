﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Condition
{
    public class AllMainRelationB : BaseUnlockCondition<EmployeeList>
    {
        public AllMainRelationB() : base()
        {

        }
        public AllMainRelationB ( string [] param , int unlockItemID ) : base ( param , unlockItemID )
        {
        }

        protected override void ParseParam ( string [] param )
        {
            relation = int.Parse ( param [0] );
            conditionID = int.Parse ( param [1] );
        }
        private int relation;                                                   //所有員工需要到達的好感度等級
        private int conditionID;                                                //解鎖第幾個店長業務的條件

        protected override void SetAction ( GameDataManager gm , PlayerDataManager pm , Action<EmployeeList> action)//取得特殊條件判定
        {
            pm.employeeList.OnAddRelation +=action;
        }
        protected override void doEvent ( GameDataManager gm , PlayerDataManager pm )//主要解鎖區域
        {                                                                       //
            pm.businessUnlock.SetUnlock ( unlockItemID , conditionID , true );  //解鎖店長業務的第幾個條件
        }
        protected override bool Check ( EmployeeList list )                     //檢查是否達成特殊條件
        {
            int [] unlocked = list.GetAllUnlockID ();                           //取得所有解鎖的員工編號
            foreach ( int id in unlocked )                                      //搜尋所有員工
            {
                Employee e = list.GetEmployee ( id );                           //取得員工資料
                if ( e.IsMain () )                                              //如果是女主角
                {
                    if ( e.GetRelationRank () < relation )                      //如果女主角的好感度 小於 解鎖條件
                    {
                        return false;                                           //回傳尚未解鎖
                    }
                }
            }
            return true;
        }
        public override bool IsUnlocked ( GameDataManager gm , PlayerDataManager pm )//檢查該編號的業務是否已解鎖
        {
            return pm.businessUnlock.GetUnlock ( unlockItemID );
        }

        protected override void UnsetAction(GameDataManager gm, PlayerDataManager pm, Action<EmployeeList> action)
        {
            pm.employeeList.OnAddRelation -= action;
        }

        public override int ParamLength()
        {
            return 2;
        }

        protected override void removeEvent ( GameDataManager gm , PlayerDataManager pm )
        {
        }
    }
}
