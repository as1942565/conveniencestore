﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Condition
{
    public class ResearchNumB : BaseUnlockCondition<CommodityItem>
    {
        public ResearchNumB() : base()
        {

        }
        public ResearchNumB ( string [] param , int unlockItemID ) : base ( param , unlockItemID )
        {
        }

        protected override void ParseParam ( string [] param )
        {
            num = int.Parse ( param [0] );
            type = int.Parse ( param [1] );
            conditionID = int.Parse ( param [2] );
        }

        private int num;
        private int type;
        private int conditionID;

        public override bool IsUnlocked ( GameDataManager gm , PlayerDataManager pm )
        {
            return pm.businessUnlock.GetUnlock ( unlockItemID );
        }

        protected override bool Check ( CommodityItem list )
        {
            return list.GetResearchNumByType ( _gm , type ) >= num;
        }

        protected override void doEvent ( GameDataManager gm , PlayerDataManager pm )
        {
            pm.businessUnlock.SetUnlock ( unlockItemID , conditionID , true );
        }

        protected override void SetAction(GameDataManager gm, PlayerDataManager pm, Action<CommodityItem> action)
        {
            pm.commodityItem.OnItemUnlock +=action;
        }

        protected override void UnsetAction(GameDataManager gm, PlayerDataManager pm, Action<CommodityItem> action)
        {
            pm.commodityItem.OnItemUnlock -=action;
        }

        public override int ParamLength()
        {
            return 3;
        }

        protected override void removeEvent ( GameDataManager gm , PlayerDataManager pm )
        {
        }
    }
}
