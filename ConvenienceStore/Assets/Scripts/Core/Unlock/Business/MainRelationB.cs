﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Condition
{
    public class MainRelationB : BaseUnlockCondition<EmployeeList>
    {
        public MainRelationB() : base()
        {

        }
        public MainRelationB ( string [] param , int unlockItemID ) : base ( param , unlockItemID )
        {
        }

        protected override void ParseParam ( string [] param )
        {
            employeeID = int.Parse ( param [0] );
            relation = int.Parse ( param [1] );
            conditionID = int.Parse ( param [2] );
        }

        private int employeeID;
        private int relation;
        private int conditionID;

        public override bool IsUnlocked ( GameDataManager gm , PlayerDataManager pm )
        {
            return pm.businessUnlock.GetUnlock ( unlockItemID );
        }

        protected override bool Check ( EmployeeList list )
        {
            Employee employee = list.GetEmployee ( employeeID );
            if ( employee == null )
                return false;
            return employee.GetRelationRank () >= relation;
        }

        protected override void doEvent ( GameDataManager gm , PlayerDataManager pm )
        {
            pm.businessUnlock.SetUnlock ( unlockItemID , conditionID , true );
        }

        protected override void SetAction(GameDataManager gm, PlayerDataManager pm, Action<EmployeeList> action)
        {
            pm.employeeList.OnAddRelation += action;
        }

        protected override void UnsetAction(GameDataManager gm, PlayerDataManager pm, Action<EmployeeList> action)
        {
            pm.employeeList.OnAddRelation -= action;
        }

        public override int ParamLength()
        {
            return 3;
        }

        protected override void removeEvent ( GameDataManager gm , PlayerDataManager pm )
        {
            pm.businessUnlock.SetUnlock ( unlockItemID , conditionID , false );
        }
    }
}
