﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Condition
{
    public class StoreUnlockB : BaseUnlockCondition<StoreExpansionUnlock>
    {
        public StoreUnlockB() : base()
        {

        }
        public StoreUnlockB ( string [] param , int unlockItemID ) : base ( param , unlockItemID )
        {
        }

        protected override void ParseParam ( string [] param )
        {
            storeID = int.Parse ( param [0] );
            conditionID = int.Parse ( param [1] );
        }

        private int storeID;
        private int conditionID;

        public override bool IsUnlocked ( GameDataManager gm , PlayerDataManager pm )
        {
            return pm.businessUnlock.GetUnlock ( unlockItemID );
        }

        protected override bool Check ( StoreExpansionUnlock list )
        {
            return list.GetComplete ( storeID );
        }

        protected override void doEvent ( GameDataManager gm , PlayerDataManager pm )
        {
            pm.businessUnlock.SetUnlock ( unlockItemID , conditionID , true );
        }

        protected override void SetAction(GameDataManager gm, PlayerDataManager pm, Action<StoreExpansionUnlock> action)
        {
            pm.storeExpansionUnlock.OnItemComplete+=action;
        }

        protected override void UnsetAction(GameDataManager gm, PlayerDataManager pm, Action<StoreExpansionUnlock> action)
        {
            pm.storeExpansionUnlock.OnItemComplete-=action;
        }

        public override int ParamLength()
        {
            return 2;
        }

        protected override void removeEvent ( GameDataManager gm , PlayerDataManager pm )
        {
        }
    }
}
