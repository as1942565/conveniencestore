﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Condition;
using GameData;

public class UnlockManager
{
    public GameDataManager gm;
    public PlayerDataManager pm;

    public void Init ( GameDataManager gm , PlayerDataManager pm )
    {
        this.pm = pm;
        this.gm = gm;
        Activity [] activity = gm.activityList.Data;                             //取得員工活動資料
        for ( int i = 0; i < activity.Length; i++ )                             //搜尋所有員工活動資料
        {
            for ( int j = 0; j < activity [i].unlockCondition.Count; j++ )      //搜尋該員工活動的所有條件
            {
                BaseUnlockCondition condition = GenerateUC ( activity [i].unlockCondition [j] , i );//生產解鎖判定
                if ( condition.IsUnlocked ( gm , pm ) == false )              //如果該活動尚未解鎖
                {
                    condition.Register ( gm , pm );                         //新增解鎖判定項目
                }
            }
        }

        Business [] business = gm.businessList.Data;
        for ( int i = 0; i < business.Length; i++ )
        {
            for ( int j = 0; j < business [i].unlockCondition.Count; j++ )
            {
                BaseUnlockCondition condition = GenerateUC ( business [i].unlockCondition [j] , i );
                if ( condition.IsUnlocked ( gm , pm ) == false )
                {
                    condition.Register ( gm , pm );
                }
            }
        }

        StoreExpansion [] storeExpansions = gm.storeExpansionList.Data;
        for ( int i = 0; i < storeExpansions.Length; i++ )
        {
            for ( int j = 0; j < storeExpansions [i].unlockCondition.Count; j++ )
            {
                BaseUnlockCondition condition = GenerateUC ( storeExpansions [i].unlockCondition [j] , i );
                if ( condition == null )
                    continue;
                if ( condition.IsUnlocked ( gm , pm ) == false )
                {
                    condition.Register ( gm , pm );
                }
            }
        }

    }
    private BaseUnlockCondition GenerateUC (UnlockConditionData conditionData , int itemID )//生成解鎖判定
    {
        Type t = Type.GetType ( "Condition." + conditionData.Condition.ToString());                 //將得到的字串變成腳本型態
        if ( t == null )
        {
            Debug.LogWarning (conditionData.Condition + "不是特殊條件名稱" );
            return null;
        }
        BaseUnlockCondition c = (BaseUnlockCondition)Activator.CreateInstance ( t , conditionData.Param, itemID );
        return c;
    }
}
