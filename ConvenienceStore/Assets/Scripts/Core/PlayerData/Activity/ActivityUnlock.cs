﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

//員工活動解鎖狀態
[DataContract(Namespace= "")]
[System.Serializable]
public class ActivityUnlock
{                                                                       //
    [DataMember]
    [SerializeField]
    private bool[] Unlock;                                              //員工活動的解鎖狀態列表
    [DataMember]
    [SerializeField]
    private state [] Mode;                                              //員工活動的狀態
    [DataMember]
    [SerializeField]
    private bool [] WatchStory;

    public enum state
    {
        Available = 0,                                                  //可委託
        Processing = 1,                                                 //已委託
        NotAvailable = 2,                                               //委託中
        Group = 3,                                                      //群組關係
    }

    private ActivityUnlock ()
    {

    }
    public ActivityUnlock( GameData.ActivityList list )
    {
        Unlock = new bool [list.Data.Length];                           
        Mode = new state [list.Data.Length];
        WatchStory = new bool [list.Data.Length];
        for ( int i = 0; i < list.Data.Length; i++ )                    
        {
            Unlock [i] = list.Data [i].unlock;                          
            Mode [i] = 0;
        }
    }

    public void Init ( GameData.ActivityList list )
    {
        WatchStory = new bool [list.Data.Length];
    }

    public bool GetUnlock ( int ID )                                    //取得員工活動解鎖狀態
    {
        if ( ID >= Unlock.Length )
        {
            Debug.LogError ( ID + "超過員工活動陣列長度" );
            return false;
        }
        return Unlock [ID];
    }
    public void SetUnlock ( int ID , bool u )                           //設定員工活動解鎖狀態
    {
        if ( ID >= Unlock.Length )
        {
            Debug.LogError ( ID + "超過員工活動陣列長度" );
            return;
        }
        Unlock [ID] = u;
    }
    public List<int> GetAllCanRunID ()                                  //取得所有可以執行的活動編號
    {
        List<int> id = new List<int> ();
        for ( int i = 0; i < Unlock.Length; i++ )
        {
            if ( Unlock [i] && Mode [i] == 0 )
            {
                id.Add ( i );
            }
        }
        return id;
    }
    public state GetMode ( int ID )                                     //取得員工活動狀態
    {
        if ( ID >= Mode.Length )
        {
            Debug.LogError ( ID + "超過員工活動陣列長度" );
            return 0;
        }
        return Mode [ID];
    }
    public void SetMode ( int ID , state type )                           //設定員工活動狀態
    {
        if ( ID >= Mode.Length )
        {
            Debug.LogError ( ID + "超過員工活動陣列長度" );
            return;
        }
        Mode [ID] = type;
    }

    public bool [] GetWatchArray ()
    {
        return WatchStory;
    }

    public bool GetWatch ( int id )
    {
        if ( id >= WatchStory.Length )
        {
            Debug.LogError ( id + "大於員工活動數量" );
            return false;
        }
        return WatchStory [id];
    }
    public void SetWatch ( int id ,  bool watch )
    {
        if ( id >= WatchStory.Length )
        {
            Debug.LogError ( id + "大於員工活動數量" );
            return;
        }
        WatchStory [id] = watch;
    }
}
