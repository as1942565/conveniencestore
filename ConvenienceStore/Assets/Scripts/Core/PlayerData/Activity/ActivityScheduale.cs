﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

//員工活動委託行程
[DataContract ( Namespace = "" )]
[System.Serializable]
public class ActivityScheduale
{                                                                           //
    [DataMember]
    [SerializeField]
    private List<Scheduale> scheduales;                                     //委託行程的活動列表
    [DataMember]
    [SerializeField]
    private List<int> deployedEmployees;                                    //正在委託的員工列表
    public System.Action<Scheduale> OnRunScheduales;                        //模擬用
    public ActivityScheduale ()
    {
        scheduales = new List<Scheduale> ();
        deployedEmployees = new List<int> ();
    }

    public void AddDeployedEmployee ( int id )
    {
        deployedEmployees.Add ( id );                                   //加入委託名單中
    }
    //真正接取委託
    public void AddScheduales ( ActivityBaseEvent [] activity , Scheduale scheduale , GameDataManager gm , PlayerDataManager pm )
    {
        if ( scheduale.GetMethod () == Scheduale.SchedualeMethod.money )
        {
            int spend = scheduale.GetBonus ();
            if ( pm.playerData.GetMoney() < spend )
                Debug.LogError ( "玩家金錢不足" );
            else
                pm.playerData.AddMoney ( -spend );
            int totalShift = pm.calender.TotalShift;
            pm.gameLog.AddActivity ( totalShift , spend );
        }
        int day = pm.calender.GetDay ();
        scheduales.Add ( scheduale );                                       //加進List陣列
        string log = "委託 :";
        for ( int i = 0; i < scheduale.EmployeesID.Length; i++ )
        {
            int id = scheduale.EmployeesID [i];
            string name = gm.workerList.Data [id].name;
            log += name + " ";
        }
    }
    public bool [] IsEmployeesFree ( int [] employeesID )                   //檢查員工們是否有空
    {
        bool [] free = new bool [employeesID.Length];
        for ( int i = 0; i < employeesID.Length; i++ )                      //搜尋員工名單
        {
            if ( deployedEmployees.IndexOf ( employeesID [i] ) < 0 )        //如果員工不在委託名單內
            {
                free [i] = true;                                            //表示有空
            }
            else
            {
                free [i] = false;
            }
        }
        return free;                                                        //回傳結果
    }
    public bool IsEmployeeFree ( int id )                                   //檢查員工是否有空
    {
        if ( deployedEmployees.IndexOf ( id ) < 0 )                         //如果員工不在委託名單內
        {
            return true;                                                    //回傳有空
        }
        return false;                                                       //回傳沒空
    }
    public int GetSchedualesQuantity ()                                     //目前執行中的員工活動數量
    {
        return scheduales.Count;
    }
    public Scheduale [] GetSchedules ()
    {
        return scheduales.ToArray ();
    }
    //每次更換時段都來呼叫
    public void Run ( PlayerDataManager pm , GameDataManager gm , int shiftNow )
    {
        int day = pm.calender.GetDay ();
        Scheduale [] runables = GetAllRunableScheduales ( shiftNow );       //取得已達成的委託
        foreach ( Scheduale s in runables )                                 //搜尋所有已達成的委託
        {
            s.RunEvent ( pm , gm );                                         //執行委託
            if ( OnRunScheduales != null )
            {
                OnRunScheduales ( s );
            }
            
            RemoveSchedule ( s );                                           //移除委託
            pm.activityUnlock.SetMode ( s.ActivityID , 0 );                 //該活動沒有執行中
            foreach ( int id in s.EmployeesID )                             //搜尋本次達成的活動所派遣的員工們
            {
                deployedEmployees.Remove ( id );                            //從派遣名單中移除
            }
        }
    }
    
    private Scheduale [] GetAllRunableScheduales ( int shiftNow )           //取得已達成的委託
    {
        List<Scheduale> canRun = new List<Scheduale> ();
        foreach ( Scheduale s in scheduales )                               //搜尋所有委託的活動
        {
            if ( s.GetExecuteShift () <= shiftNow )                         //如果已超過活動的委託時段
            {
                canRun.Add ( s );                                           //加入已達成委託的陣列
            }
        }
        return canRun.ToArray ();                                           //回傳矩陣
    }
    private void RemoveSchedule ( Scheduale s )                             //移除委託
    {
        scheduales.Remove ( s );                                            //移除委託
    }

    [System.Serializable]
    public class Scheduale                                                  //委託行程
    {
        public enum SchedualeMethod                                         //委託形式
        {
            normal = 0,
            money = 1,
            boss = 2,
        }
        
        public int [] EmployeesID;                                          //委託執行的員工名單

        public int ActivityID;                                              //員工活動編號

        [SerializeField]
        private ActivityBaseEvent [] Events;                                //活動事件
        [SerializeField]
        private SchedualeMethod method;                                     //委託形式
        [SerializeField]
        private int ExecuteShift;                                            //現在班次 + 活動執行班次
        [SerializeField]
        private bool Successed;
        [SerializeField]
        private int Bonus;

        private Scheduale ()
        {

        }
        public Scheduale ( ActivityBaseEvent [] events , int [] employeesID , int activityID  )
        {
            Events = events;
            EmployeesID = employeesID;
            ActivityID = activityID;
        }

        public void RunEvent ( PlayerDataManager pm , GameDataManager gm )  //執行事件
        {
            foreach ( ActivityBaseEvent e in Events )                       //搜尋所有活動事件
            {
                e.DoEvent ( pm , gm );                                      //依序執行事件
            }
            for ( int i = 0; i < EmployeesID.Length; i++ )                  //搜尋所有員工
            {
                int id = EmployeesID [i];
                int Exp = gm.activityList.Data [ActivityID].exp;
                pm.employeeList.GetEmployee ( id ).AddExp ( Exp , gm , pm );     //增加員工們的經驗值
                //( gm.workerList.Data[id].name + " 增加經驗 : " + Exp );
                int relation = gm.customData.Data.relation [(int)method];
                if ( gm.workerList.Data [id].isMain )                       //如果該員工是女主角
                    pm.employeeList.AddRelationEXP ( id , relation , gm , pm );  //增加員工們的好感度
            }
            string name = gm.activityList.Data [ActivityID].name;
            if ( Successed )                                                //如果該員工活動結果成功
            {
                pm.storeLog.AddNotice ( name + "已完成。" );
                pm.activityScheduleLog.OnGetSuccessQuantity ( pm.calender );
                return;
            }
            pm.storeLog.AddNotice ( name + "已失敗。" );
            pm.activityScheduleLog.OnGetFailQuantity ( pm.calender );
        }

        public SchedualeMethod GetMethod ()                                 //取得委託形式
        {
            return method;
        }
        public void SetMethod ( SchedualeMethod method )                    //設定委託形式
        {
            this.method = method;
        }
        public int GetExecuteShift ()
        {
            return ExecuteShift;
        }
        public void SetExcuteShift ( int executeShift )
        {
            ExecuteShift = executeShift;
        }
        public bool GetSuccessed ()
        {
            return Successed;
        }
        public void SetSuccessed ( bool success )
        {
            Successed = success;
        }
        public int GetBonus ()
        {
            return Bonus;
        }
        public void SetBonus ( int bonus )
        {
            Bonus = bonus;
        }
    }
}