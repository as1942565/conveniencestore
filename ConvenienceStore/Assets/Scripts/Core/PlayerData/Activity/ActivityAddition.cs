﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;
using GameData;

//員工活動時效性加成
[DataContract(Namespace= "")]
[System.Serializable]
public class ActivityAddition
{
    [DataMember]
    [SerializeField]
    private List<ActivityIncomeAddition> income;                                    //商品收入加成資料
    [DataMember]
    [SerializeField]
    private List<ActivitySaleQuantityAddition> saleQuantity;                        //商品銷量加成資料

    public event System.Action<List<ActivityIncomeAddition>> OnActivityIncomeChanged;
    public event System.Action<List<ActivitySaleQuantityAddition>> OnActivitySaleQuantityChanged;

    public ActivityAddition ()
    {
        income = new List<ActivityIncomeAddition> ();
        saleQuantity = new List<ActivitySaleQuantityAddition> ();
    }

    public float GetIncome ( Commodity.CommodityType type )        //取得收入總加成
    {                                                                       //
        float sum = 0;
        for ( int i = 0; i < income.Count; i++ )
        {
            if ( income [i].type != type )
                continue;
            sum += income [i].addition;
        }
        if ( sum == 0 )
            return 1;
        if ( sum >= 1 )
        {
            Debug.LogError ( sum + "員工活動收入加成不該大於1" );
            return 1;
        }
        return sum;
    }
    public void AddIncome ( Commodity.CommodityType type , float addition , int time )//新增一個收入加成
    {
        ActivityIncomeAddition data;
        data.type = type;
        data.addition = addition;
        data.time = time;
        income.Add ( data );
        if ( OnActivityIncomeChanged != null )
        {
            OnActivityIncomeChanged ( income );
        }
    }
    public void CheckIncome ( int totalShift )                                      //檢查收入加成是否過期
    {
        for ( int i = income.Count - 1; i >= 0; i-- )
        {
            if ( income [i].time <= totalShift )
                income.Remove ( income[i] );
        }
        if ( OnActivityIncomeChanged != null )
        {
            OnActivityIncomeChanged ( income );
        }
    }
    public float GetSaleQuantity ( Commodity.CommodityType type )  //取得銷量總加成
    {
        float sum = 0;
        for ( int i = 0; i < saleQuantity.Count; i++ )
        {
            if ( saleQuantity [i].type != type )
                continue;
            sum += saleQuantity [i].addition;
        }
        return sum;
    }
    public void AddSaleQuantity ( Commodity.CommodityType type , float addition , int time )//新增一個銷量加成
    {
        ActivitySaleQuantityAddition data;
        data.type = type;
        data.addition = addition;
        data.time = time;
        saleQuantity.Add ( data );
        if ( OnActivitySaleQuantityChanged != null )
        {
            OnActivitySaleQuantityChanged ( saleQuantity );
        }
    }
    public void CheckSaleQuantity ( int totalShift )                                //檢查銷量加成是否過期
    {
        for ( int i = saleQuantity.Count - 1; i >= 0; i-- )
        {
            if ( saleQuantity [i].time <= totalShift )
                saleQuantity.Remove ( saleQuantity [i] );
        }
        if ( OnActivitySaleQuantityChanged != null )
        {
            OnActivitySaleQuantityChanged ( saleQuantity );
        }
    }
}

[System.Serializable]
public struct ActivityIncomeAddition
{
    public Commodity.CommodityType type;
    public float addition;
    public int time;
}
[System.Serializable]
public struct ActivitySaleQuantityAddition
{
    public Commodity.CommodityType type;
    public float addition;
    public int time;
}
