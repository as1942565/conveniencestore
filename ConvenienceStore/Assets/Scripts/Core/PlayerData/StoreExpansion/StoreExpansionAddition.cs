﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

public class StoreExpansionAddition
{
    private float income;

    private float saleQuantity;

    private float popular;

    private int autoAddClean;

    private int autoAddPolice;

    public event System.Action<float> OnIncomeChanged;
    public event System.Action<float> OnSaleQuantityChanged;
    public event System.Action<float> OnPopularChanged;
    public event System.Action<int> OnAutoCleanChanged;
    public event System.Action<int> OnAutoPoliceChanged;

    public StoreExpansionAddition ()
    {
        income = 1;
        saleQuantity = 1;
        popular = 1;
        autoAddClean = 0;
        autoAddPolice = 0;
    }

    public float GetIncome ()
    {
        return income;
    }
    public void AddIncome ( float addition )
    {
        if ( addition <= 0 )
        {
            Debug.LogError ( addition + "不該小於等於0" );
            return;
        }
        income += addition;
        if ( OnIncomeChanged != null )
        {
            OnIncomeChanged ( income );
        }
    }
    public float GetSaleQuantity ()
    {
        return saleQuantity;
    }
    public void AddSaleQuantity ( float addition )
    {
        if ( addition <= 0 )
        {
            Debug.LogError ( addition + "不該小於等於0" );
            return;
        }
        saleQuantity += addition;
        if ( OnSaleQuantityChanged != null )
        {
            OnSaleQuantityChanged ( saleQuantity );
        }
    }
    public float GetPopular ()
    {
        return popular;
    }
    public void AddPopular ( float addition )
    {
        if ( addition <= 0 )
        {
            Debug.LogError ( addition + "不該小於等於0" );
            return;
        }
        popular += addition;
        if ( OnPopularChanged != null )
        {
            OnPopularChanged ( popular );
        }
    }
    public int GetAutoAddClean ()
    {
        return autoAddClean;
    }
    public void AddAutoAddClean ( int addition )
    {
        if ( addition <= 0 )
        {
            Debug.LogError ( addition + "不該小於等於0" );
            return;
        }
        autoAddClean += addition;
        if ( OnAutoCleanChanged != null )
        {
            OnAutoCleanChanged ( autoAddClean );
        }
    }
    public int GetAutoAddPolice ()
    {
        return autoAddPolice;
    }
    public void AddAutoAddPolice ( int addition )
    {
        if ( addition <= 0 )
        {
            Debug.LogError ( addition + "不該小於等於0" );
            return;
        }
        autoAddPolice += addition;
        if ( OnAutoPoliceChanged != null )
        {
            OnAutoPoliceChanged ( autoAddPolice );
        }
    }
}

