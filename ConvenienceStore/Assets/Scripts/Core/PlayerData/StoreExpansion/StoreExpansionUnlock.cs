﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

//店面擴建解鎖狀態
[DataContract(Namespace= "")]
[System.Serializable]
public class StoreExpansionUnlock
{                                                                       //
    [DataMember]
    [SerializeField]
    private bool [] Unlock;                                             //店面擴建的解鎖狀態
    [DataMember]
    [SerializeField]
    private bool [] Complete;                                           //店面擴建的完成狀態
    //[DataMember]
    //[SerializeField]
    private int expansionQuantity;                                      //擴建數量(內建除外

    private StoreExpansionUnlock ()
    {
    }
    public StoreExpansionUnlock ( GameData.StoreExpansionList list )    //建構式
    {
        Unlock = new bool [list.Data.Length];
        Complete = new bool [list.Data.Length];
        for ( int i = 0; i < list.Data.Length; i++ )
        {
            Unlock [i] = list.Data [i].unlock;
            Complete [i] = false;
        }
        expansionQuantity = 0;
    }
    public System.Action<StoreExpansionUnlock> OnItemComplete;

    public bool GetUnock ( int ID )                                     //取得店面擴建解鎖狀態
    {
        if ( ID >= Unlock.Length )
        {
            Debug.LogError ( ID + "超過店面擴建資料長度" );
            return false;
        }
        return Unlock [ID];
    }
    public void SetUnlock ( int ID , bool u )                           //設定店面擴建解鎖狀態
    {
        if ( ID >= Unlock.Length )
        {
            Debug.LogError ( ID + "超過店面擴建資料長度" );
            return;
        }
        Unlock [ID] = u;
    }
    public void ExpantionComplete ()
    {
        for ( int i = 0; i < Unlock.Length; i++ )
        {
            if ( Unlock [i] == false )
                return;
        }
        Achievement.SetAchievement ( Achievement.achievementNames.Expantion.ToString () );
    }
    public bool GetComplete ( int ID )                                  //取得店面擴建完成狀態
    {
        if ( ID >= Complete.Length )
        {
            Debug.LogError ( ID + "超過店面擴建資料長度" );
            return false;
        }
        return Complete [ID];
    }
    public int [] GetAllCompleteID ()
    {
        List<int> completeID = new List<int> ();
        for ( int i = 0; i < Complete.Length; i++ )
        {
            if ( Complete[i] == true )
            {
                completeID.Add ( i );
            }
        }
        return completeID.ToArray ();
    }
    public void SetComplete ( int ID , bool c )                         //設定店面擴建完成狀態
    {
        if ( ID >= Complete.Length )
        {
            Debug.LogError ( ID + "超過店面擴建資料長度" );
            return;
        }
        Complete [ID] = c;
        expansionQuantity++;
        if ( OnItemComplete != null )
        {
            OnItemComplete ( this );
        }
    }
    public int GetExpansionQuantity ()
    {
        return expansionQuantity;
    }
    
    public List<int> GetCompleteStoreEventID ( GameDataManager gm , int employeeID )
    {
        List<int> completeID = new List<int> ();
        bool [] storeID = gm.storeEventList.Data.storeID [employeeID].ToArray ();
        for ( int i = 0; i < storeID.Length; i++ )
        {
            if ( Complete [i] && storeID [i] == true )
                completeID.Add ( i );
        }
        return completeID;
    }
}
