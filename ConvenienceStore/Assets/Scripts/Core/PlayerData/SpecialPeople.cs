﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

[DataContract(Namespace= "")]
[System.Serializable]
public class SpecialPeople
{
    [DataMember]
    [SerializeField]
    private bool [] unlock;

    public SpecialPeople ( int length )
    {
        unlock = new bool [length];
    }

    public void SetUnlock ( int id , bool un )
    {
        if ( id >= unlock.Length )
        {
            Debug.LogError ( id + "超過特殊員工數量" );
            return;
        }
        unlock [id] = un;
    }
    public int [] GetAllLock ()
    {
        List<int> temp = new List<int> ();
        for ( int i = 0; i < unlock.Length; i++ )
        {
            if ( unlock [i] == false )
                temp.Add ( i );
        }
        return temp.ToArray ();
    }
    public int [] GetAllUnlock ()
    {
        List<int> temp = new List<int> ();
        for ( int i = 0; i < unlock.Length; i++ )
        {
            if ( unlock [i] == true )
                temp.Add ( i );
        }
        return temp.ToArray ();
    }
}
