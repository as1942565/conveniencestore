﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;
using Tools;
using System.Runtime.Serialization;

//員工資料
[DataContract(Namespace= "")]
[System.Serializable]
[KnownType(typeof ( MainEmployee ))]
public class Employee
{
    [DataMember]
    [SerializeField]
    private Worker Worker;                                              //員工資料
    [DataMember]
    [SerializeField]
    private int [] ability;

    public virtual bool IsMain ()                                       //是否為女主角(預設回傳否)
    {
        return false;
    }
    private Employee ()
    {
    }

    public Employee ( Worker worker )
    {
        Worker = worker;
        ability = new int [worker.ability.Length];
        for ( int i = 0; i < ability.Length; i++ )
            ability [i] = worker.ability [(Worker.Abillity.type)i];
    }

    public int GetID ()                                                 //取得員工編號
    {
        return Worker.id;
    }
    public int GetExp ()                                                //取得員工經驗值
    {
        return Worker.exp;
    }
    public void AddExp ( int exp , GameDataManager gm , PlayerDataManager pm )               //增加員工經驗值
    {
        if ( exp <= 0 )
        {
            Debug.LogError ( exp + "員工增加的經驗值不應該小於等於0" );
            return;
        }
        Worker.exp += exp;
        LevelUp ( gm , pm );
    }
    public void ResetRank ()
    {
        Worker.relationRank = 0;
    }
    private void LevelUp ( GameDataManager gm , PlayerDataManager pm )                       //員工等級提升
    {
        int day = pm.calender.GetDay ();
        int [] Upgrade = gm.customData.Data.EmployeeLevel;              //取得每個等級升等所需經驗值
        if ( Worker.level >= 9 )                                        //如果員工等級達到10等就不再升等
            return;
        if ( Worker.exp >= Upgrade [Worker.level + 1] )                     //如果員工經驗值大於該等級所需經驗值
        {
            Worker.exp -= Upgrade [Worker.level + 1];                       //扣除員工經驗值
            Worker.level++;                                             //員工等級提升
            pm.gameLog.AddReport ( day , Worker.name + "的等級提升至" + Worker.level + "等。" );
            List<int> CanUpgradeability = new List<int> ();
            //Debug.Log(Worker.name + "等級提升至" + Worker.level);
            for (int i = 0; i < ability.Length; i++)                    //搜尋所有員工屬性
            {
                if ( ability [i] < 4 )                                  //如果員工屬性小於S級
                    CanUpgradeability.Add ( i );                        //代表可以再升級
            }
            if ( CanUpgradeability.Count == 0 )
            {
                return;
            }
            CanUpgradeability.Shuffle ();                               //亂數屬性
            AddAbility ( CanUpgradeability [0] , pm );                  //隨機提升1種員工屬性
        }
    }
    public int GetLevel ()                                              //取得員工等級
    {
        return Worker.level;
    }
    public int GetRelationEXP ()                                        //取得員工好感度
    {
        return Worker.relationEXP;
    }
    public float GetRelationScale ( GameDataManager gm )
    {
        int level = Worker.relationRank + 1;
        if ( level >= gm.customData.Data.EmployeeRelation.Length )
            return 1;
        float exp = Worker.relationEXP;
        float maxRelation = gm.customData.Data.EmployeeRelation [level];
        if ( maxRelation == 0 )
            return 0;
        return exp / maxRelation;
    }
    /// <summary>
    /// 禁止使用
    /// </summary>
    /// <param name="relation"></param>
    /// <param name="pm"></param>
    public void _addRelationEXP ( int relation , GameDataManager gm , PlayerDataManager pm )  //增加員工好感度
    {
        if ( relation <= 0 )
        {
            Debug.LogError ( relation + "員工增加的好感度不應該小於等於0" );
            return;
        }
        Worker.relationEXP += relation;
        RelationRankUp ( gm , pm );
    }
    private void RelationRankUp ( GameDataManager gm , PlayerDataManager pm )                //員工好感度提升
    {
        int day = pm.calender.GetDay ();
        int [] Upgrade = gm.customData.Data.EmployeeRelation;           //取得每個好感度等級升等所需好感度
        if ( Worker.relationRank >= 4 )                                 //如果員工好感度等級達到5等就不再升等
            return;
        if ( Worker.relationEXP >= Upgrade [Worker.relationRank + 1] )  //如果員工好感度大於該好感度等級所需好感度
        {
            Worker.relationEXP -= Upgrade [Worker.relationRank + 1];    //扣除員工好感度
            Worker.relationRank++;                                      //員工好感度等級提升
            if ( Worker.relationRank == 4 )
                pm.storeLog.AddNotice ( Worker.name + "的好感度等級已達上限。" );

            pm.gameLog.AddReport ( day , Worker.name + "的好感度等級提升至" + Worker.relationRank + "等。" );
            //( Worker.name + "好感度等級提升至" + Worker.relationRank );
        }
    }
    public int GetRelationRank ()                                       //取得員工好感度等級
    {
        return Worker.relationRank;
    }
    public int GetPower ()                                              //取得員工體力值
    {
        return Worker.power;
    }
    public void UsePower ()                                             //消耗員工體力值
    {
        Worker.power--;
    }
    public void ResetPower ()                                           //重置員工體力值
    {
        Worker.power = 3;
    }
    public int GetAbility ( int type )                                  //取得員工屬性
    {
        if ( type >= ability.Length )
        {
            Debug.LogError ( type + "超過員工屬性的陣列長度" );
            return 0;
        }
        return ability [type];
    }
    public void AddAbility ( int type , PlayerDataManager pm )          //增加員工屬性
    {
        int day = pm.calender.GetDay ();
        if ( type >= ability.Length )
        {
            Debug.LogError ( type + "超過員工屬性的陣列長度" );
            return;
        }
        ability [type]++;
        string [] abilityName = new string [] { "體力" , "魅力" , "運氣" };
        string [] rankName = new string [] { "D" , "C" , "B" , "A" , "S" };
        int rank = ability [type];
        pm.gameLog.AddReport ( day , Worker.name + "的" + abilityName [type] + "能力提升至" + rankName [rank] + "級。" );
    }
}
