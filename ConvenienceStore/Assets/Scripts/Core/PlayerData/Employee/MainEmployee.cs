﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;
using System.Runtime.Serialization;

//主要員工
[DataContract ( Namespace = "" )]
[System.Serializable]
public class MainEmployee : Employee
{                                                               //
    [DataMember]
    [SerializeField]
    private int RelationEXP;                                     //好感度
    [DataMember]
    [SerializeField]
    private int relationRank;                                    //好感度等級

    public MainEmployee ( Worker worker ) : base ( worker )
    {
        RelationEXP = worker.relationEXP;
        relationRank = worker.relationRank;
    }
    public override bool IsMain ()                              //是否為女主角(回傳是)
    {
        return true;
    }
}
