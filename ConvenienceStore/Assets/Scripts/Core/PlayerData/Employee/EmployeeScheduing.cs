﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;
using Tools;
using System;

//員工排班資料
[DataContract ( Namespace = "" )]
[System.Serializable]
public class EmployeeScheduing
{
    private int [] unlock;                                              //每個時段的解鎖數量

    public EmployeeScheduing ( GameData.SchedulingCabinetList list )
    {
        Init ( list );
    }

    void Init ( GameData.SchedulingCabinetList list )
    {
        int length = list.Data.Length;
        unlock = new int [length];
        for ( int i = 0; i < length; i++ )
        {
            unlock [i] = list.Data [i].unlockNum;
        }
    }

    public int GetUnlock ( int shift )
    {
        if ( shift >= 3 )
        {
            Debug.LogError ( shift + "超過時段數量" );
            return 0;
        }
        if ( unlock == null )
            Init ( GameLoopManager.instance.gm.schedulingCabinetList );
        return unlock [shift];
    }
    public void AddUnlock ( int shift , int num )
    {
        if ( shift >= 3 )
        {
            Debug.LogError ( shift + "超過時段數量" );
            return;
        }
        if ( unlock == null )
            Init ( GameLoopManager.instance.gm.schedulingCabinetList );
        unlock [shift] += num;
    }
}


