﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization;
using GameData;
using System.Linq;

//員工資訊列表
[DataContract ( Namespace = "" )]
[Serializable]
public class EmployeeList
{
    [DataMember]
    [SerializeField]
    private List<Employee> visEmplyee;
    private Dictionary<int , Employee> _e;
    private Dictionary<int , Employee> employees {
        get
        {
            if(_e == null )
            {
                _e = new Dictionary<int , Employee> ();
            }
            if (_e.Count != visEmplyee.Count )
            {
                if(_e.Count != 0)
                    _e = new Dictionary<int , Employee> ();
                
                foreach (Employee e in visEmplyee )
                {
                    _e.Add ( e.GetID () , e );
                }
            }
            return _e;
        }
    }

    [DataMember]
    [SerializeField]
    private bool [] unlock;                                             //員工是否已解鎖

    private EmployeeList ()
    {
    }
    public EmployeeList ( GameDataManager gm )
    {
        Worker [] works = gm.workerList.Data;
        unlock = new bool [works.Length];
        visEmplyee = new List<Employee> ();
        for ( int i = 0; i < works.Length; i++ )
        {
            unlock [i] = works [i].initWith;
            if ( works [i].initWith )                                   //如果是初次登場
            {
                Employee e = CreateEmployeeFromWorker ( works [i] );    //建立員工資料
                employees.Add ( e.GetID() , e );                                //將員工加入陣列中
                visEmplyee.Add ( e );
            }
        }
    }
    public int GetUnlockLength ()
    {
        return unlock.Length;
    }
    public void ResetUnlockArray ( int length )
    {
        bool [] temp = new bool [length];
        for ( int i = 0; i < unlock.Length; i++ )
        {
            temp [i] = unlock [i];
        }
        unlock = temp;
    }
    public System.Action<EmployeeList> OnAddRelation;
    public System.Action<EmployeeList> OnAddEmployee;
    private Employee CreateEmployeeFromWorker ( Worker worker )         //建立員工資料
    {
        Employee e;                                                     //宣告員工資訊
        if ( worker.isMain )                                            //如果是女主角
        {
            e = new MainEmployee ( worker );                            //傳遞員工資訊
        }
        else
        {
            e = new Employee ( worker );                                //傳遞員工資訊
        }
        return e;
    }

    public void AddNewEmployee ( int id , Worker worker )               //新增員工
    {
        if ( employees.TryGetValue ( id , out Employee e ) == false )   //如果此ID沒有員工資料 (代表沒重複)
        {
            Employee employee = CreateEmployeeFromWorker ( worker );    //建立員工資料
            visEmplyee.Add ( employee );

            SetUnlockID ( id , true );
            if ( OnAddEmployee != null )
            {
                OnAddEmployee ( this );
            }
        }
        else
        {
            Debug.LogError ( id + "重複新增員工" );
        }
    }
    public Employee GetEmployee ( int id )                              //取得員工資料
    {
        if ( employees.TryGetValue ( id , out Employee e ) )            //如果此ID有員工資料
        {
            return e;                                                   //回傳員工資料
        }
        Debug.LogWarning ( "Id not in dict" );
        return null;
    }
    public int [] GetAllUnlockID ()                                     //取得所有已解鎖的員工編號
    {
        return employees.Keys.ToArray ();
    }


    public bool GetUnlockID ( int ID )                                  //取得該編號員工的解鎖狀態
    {
        if ( ID >= unlock.Length )
        {
            Debug.LogError ( ID + "超過所有的員工數量" );
            return false;
        }
        return unlock [ID];
    }
    public void SetUnlockID ( int ID , bool u )                         //設定該編號員工的解鎖狀態
    {
        if ( ID >= unlock.Length )
        {
            Debug.LogError ( ID + "超過所有的員工數量" );
            return;
        }
        unlock [ID] = u;
    }
    public void AddRelationEXP ( int ID , int relation , GameDataManager gm , PlayerDataManager pm )//增加員工好感度
    {
        var e = GetEmployee ( ID );
        if ( e != null )
        {
            e._addRelationEXP ( relation , gm , pm );
            if ( OnAddRelation != null )
            {
                OnAddRelation ( this );
            }
        }
    }
}