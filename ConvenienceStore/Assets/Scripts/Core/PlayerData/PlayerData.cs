﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

[DataContract(Namespace= "")]
[System.Serializable]
public class PlayerData
{
    [DataMember]
    [SerializeField]
    private int Money;                                                      //玩家金錢
    [DataMember]
    [SerializeField]
    private int Popular;                                                    //人氣值
    [DataMember]
    [SerializeField]
    private int Clean;                                                      //清潔度
    [DataMember]
    [SerializeField]
    private int Police;                                                     //治安度

    public event System.Action<int> OnMoneyChanged;
    public event System.Action<int> OnPopularChanged;
    public event System.Action<int> OnCleanChanged;
    public event System.Action<int> OnPoliceChanged;

    public PlayerData ()                                                    //建構式
    {                                                                       //
        Money = 127000;
        Popular = 100;
        Clean = 100;
        Police = 100;
    }
    public int GetMoney ()                                                  //取得玩家金錢
    {
        return Money;
    }
    public void AddMoney ( int money )                                      //增加玩家金錢
    {
        Money += money;

        if ( Money >= 1000000 )
            Achievement.SetAchievement ( Achievement.achievementNames.Money.ToString () );

        if (OnMoneyChanged != null )
        {
            OnMoneyChanged ( Money );
        }
    }
    public void SetMoney ( int money )
    {
        Money = money;
        if (OnMoneyChanged != null )
        {
            OnMoneyChanged ( Money );
        }
    }
    public int GetPopular ()                                                //取得人氣值
    {
        return Popular;
    }
    public void SetPopular ( int popular )                                  //設定人氣值
    {
        Popular = popular;
        if ( OnPopularChanged != null )
        {
            OnPopularChanged ( Popular );
        }
    }
    public int GetClean ()                                                  //取得清潔度
    {
        return Clean;
    }
    public void SetClean ( int clean )                                      //設定清潔度
    {
        Clean = clean;
        if ( Clean > 100 )
            Clean = 100;
        if ( OnCleanChanged != null )
        {
            OnCleanChanged ( Clean );
        }
    }
    public int GetPolice ()                                                 //取得治安度
    {
        return Police;
    }
    public void SetPolice ( int police )                                    //設定治安度
    {
        Police = police;
        if ( Police > 100 )
            Police = 100;
        if ( OnPoliceChanged != null )
        {
            OnPoliceChanged ( Police );
        }
    }
}
