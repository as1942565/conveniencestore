﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

[DataContract(Namespace= "")]
[System.Serializable]
public class AudioData
{
    [DataMember]
    [SerializeField]
    private int main;
    [DataMember]
    [SerializeField]
    private int bgm;
    [DataMember]
    [SerializeField]
    private int storyEffect;
    [DataMember]
    [SerializeField]
    private int systemEffect;
    [DataMember]
    [SerializeField]
    private int soundEffect;
    [DataMember]
    [SerializeField]
    private int breatheEffect;
    [DataMember]
    [SerializeField]
    private int HEffect;

    [DataMember]
    [SerializeField]
    private float duration;

    [DataMember]
    [SerializeField]
    private float storyVol;

    public AudioData ()
    {
        Init ();
    }

    public void Init ()
    {
        main = 50;
        bgm = 10;
        storyEffect = 50;
        systemEffect = 50;
        soundEffect = 50;
        breatheEffect = 50;
        HEffect = 50;

        duration = 0.5f;
        storyVol = 1;
    }

    public event System.Action<float , float> OnVolumeChanged;
    public void SetMain ( int volume )
    {
        main = volume;
        if ( OnVolumeChanged != null )
            OnVolumeChanged ( main / 100f * bgm / 100f , duration );
    }
    public void SetStoryVol ( float vol , float dur = 0.5f )
    {
        if ( vol > 1 )
        {
            Debug.LogError ( vol + "音量不該大於100%" );
            vol = 1;
        }
        storyVol = vol;
        if ( OnVolumeChanged != null )
            OnVolumeChanged ( main / 100f * bgm / 100f * storyVol , dur );
    }
    public void PlayBGM ( float dur = 0.5f )
    {
        if ( OnVolumeChanged != null )
            OnVolumeChanged ( main / 100f * bgm / 100f * storyVol , dur );
    }
    public int GetMain ()
    {
        return main;
    }
    public void SetBGM ( int volume )
    {
        bgm = volume;
        if ( OnVolumeChanged != null )
            OnVolumeChanged ( main / 100f * bgm / 100f * storyVol , duration );
    }
    public int GetTrueBGM ()
    {
        return bgm;
    }
    public int GetBGM ()
    {
        return Mathf.RoundToInt( bgm * storyVol * main / 100 );
    }
    public int GetVideo ()
    {
        return Mathf.RoundToInt( bgm * main / 100 );
    }
    public void SetStoryEffect ( int volume )
    {
        storyEffect = volume;
    }
    public int GetTrueStoryEffect ()
    {
        return storyEffect;
    }
    public int GetStoryEffect ()
    {
        return Mathf.RoundToInt( storyEffect * main / 100 );
    }
    public void SetSystemEffect ( int volume )
    {
        systemEffect = volume;
    }
    public int GetTrueSystemEffect ()
    {
        return systemEffect;
    }
    public int GetSystemEffect ()
    {
        return Mathf.RoundToInt( systemEffect * main / 100 );
    }
    public void SetSoundEffect ( int volume )
    {
        soundEffect = volume;
    }
    public int GetTrueSoundEffect ()
    {
        return soundEffect;
    }
    public int GetSoundEffect ()
    {
        return Mathf.RoundToInt( soundEffect * main / 100 );
    }
    public void SetBreatheEffect ( int volume )
    {
        breatheEffect = volume;
    }
    public int GetTrueBreatheEffect ()
    {
        return breatheEffect;
    }
    public int GetBreatheEffect ()
    {
        return Mathf.RoundToInt( breatheEffect * main / 100 );
    }
    public void SetHEffect ( int volume )
    {
        HEffect = volume;
    }
    public int GetTrueHEffect ()
    {
        return HEffect;
    }
    public int GetHEffect ()
    {
        return Mathf.RoundToInt( HEffect * main / 100 );
    }

    public void SetDuration ( float duration )
    {
        this.duration = duration;
    }
    public float GetDuration ()
    {
        return duration;
    }
}
