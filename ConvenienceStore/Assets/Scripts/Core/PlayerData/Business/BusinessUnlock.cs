﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tools;
using System.Runtime.Serialization;

//店長業務解鎖狀態
[DataContract(Namespace= "")]
[System.Serializable]
public class BusinessUnlock
{                                                                       //
    [DataMember]
    [SerializeField]
    private BoolList [] unlockList;                                     //店長業務的解鎖狀態列表(每項活動解鎖條件不只1個)
    [DataMember]
    [SerializeField]
    private state [] mode;                                              //店長業務的狀態
    [DataMember]
    [SerializeField]
    private bool [] completeEnding;                                     //完成結局狀態

    [DataMember]
    [SerializeField]
    private IntList [] group;
    [DataMember]
    [SerializeField]
    private int level;
    [DataMember]
    [SerializeField]
    private int endingID;
    [DataMember]
    [SerializeField]
    private bool [] WatchStory;

    public enum state
    {
        Available = 0,                                                  //可執行
        hide = 1,                                                       //隱藏
        complete = 2,                                                   //完成
        watch = 3,                                                      //已觀看
        groupLock = 4,                                                  //群組關係
    }

    private BusinessUnlock ()
    {

    }
    public BusinessUnlock ( GameData.BusinessList list )
    {
        unlockList = new BoolList [list.Data.Length];                   //初始化陣列
        mode = new state [list.Data.Length];
        completeEnding = new bool [3];
        List<int> groupID = new List<int> ();
        for ( int i = 0; i < unlockList.Length; i++ )
        {
            unlockList [i] = new BoolList ( list.Data [i].unlock );     //新增並建構解鎖狀態列表
            mode [i] = 0;
            int GID = list.Data [i].groupID;
            bool GIDBool = false;
            foreach ( int id in groupID )
            {
                if ( GID == id )
                {
                    GIDBool = true;
                    break;
                }
            }
            if ( GIDBool == false )
                groupID.Add ( GID );
        }
        group = new IntList [groupID.Count];
        for ( int i = 0; i < group.Length; i++ )
        {
            group [i] = new IntList ();
            for ( int g = 0; g < list.Data.Length; g++ )
            {
                if ( list.Data[g].groupID == i )
                {
                    group [i].Add ( g );
                }
            }
        }
        level = 0;
        endingID = -1;

        /*int [] unlockID = new int [] { 15 , 21 , 27};
        for ( int i = 0; i < unlockID.Length; i++ )
        {
            int id = unlockID [i];
            for ( int y = 0; y < unlockList[id].Count; y++ )
            {
                unlockList [id] [y] = true;
            }
        }*/

        WatchStory = new bool [list.Data.Length];
    }

    public void Init ( GameData.BusinessList list )
    {
        WatchStory = new bool [list.Data.Length];
    }

    public bool GetCondition ( int ID , int conditionID )
    {
        if ( ID >= unlockList.Length )
        {
            Debug.LogError ( ID + "超過店長業務陣列長度" );
            return false;
        }
        if ( conditionID >= unlockList[ID].Count )
        {
            Debug.LogError ( ID + "超過店長業務陣列長度" );
            return false;
        }
        return unlockList [ID] [conditionID];
    }

    public bool GetUnlock ( int ID )                                    //取得該編號店長業務的解鎖狀態
    {
        if ( ID >= unlockList.Length )
        {
            Debug.LogError ( ID + "超過店長業務陣列長度" );
            return false;
        }
        bool unlock = true;
        for ( int i = 0; i < unlockList [ID].Count; i++ )               //檢查該店長業務的所有條件是否已解鎖
        {
            if ( !unlockList [ID] [i] )                                 //只要有其中一項還沒解鎖
            {
                unlock = false;                                         //以下不用檢查 直接回傳未解鎖
                break;
            }
        }
        return unlock;
    }
    public void SetUnlock ( int businessID , int conditionID , bool u ) //該店長業務其中一個條件已解鎖
    {
        if ( businessID >= unlockList.Length )
        {
            Debug.LogError ( businessID + "超過店長業務陣列長度" );
            return;
        }
        if ( conditionID >= unlockList [businessID].Count )
        {
            Debug.LogError ( businessID + "業務沒有" + conditionID + "條件" );
            return;
        }
        unlockList [businessID] [conditionID] = u;
    }
    public state GetMode ( int ID )                                     //取得該編號店長業務的狀態
    {
        if ( ID >= mode.Length )
        {
            Debug.LogError ( ID + "超過店長業務陣列長度" );
            return 0;
        }
        return mode [ID];
    }
    public System.Action<BusinessUnlock> OnItemComplete;
    public void SetMode ( int ID , state m )                            //設定該編號店長業務的狀態
    {
        if ( ID >= mode.Length )
        {
            Debug.LogError ( ID + "超過店長業務陣列長度" );
            return;
        }
        mode [ID] = m;
        if ( OnItemComplete != null )
        {
            OnItemComplete ( this );
        }
    }
    public int AnyEndingComplete ()
    {
        for ( int i = 0; i < completeEnding.Length; i++ )
        {
            if ( completeEnding [i] )
                return i;
        }
        return -1;
    }
    public void SetCompleteEnding ( int id , bool complete )
    {
        if ( id >= completeEnding.Length )
        {
            Debug.LogError ( id + "超過結局陣列長度" );
            return;
        }
        completeEnding [id] = complete;
    }
    public void GroupSetMode ( int groupID , state m )
    {
        for ( int i = 0; i < group[groupID].Count; i++ )
        {
            SetMode ( group[groupID][i] , m );
        }
    }
    public int GetLevel ()
    {
        return level;
    }
    public void SetLevel ( int l )
    {
        if ( l > 3 || l < 0 )
        {
            Debug.LogError ( l + "超過店面規模" );
            return;
        }
        level = l;
    }
    public int GetEndingID ()
    {
        return endingID;
    }
    public void SetEndingID ( int id )
    {
        if ( endingID != -1 )
            return;
        endingID = id;
    }

    public bool [] GetWatchArray ()
    {
        return WatchStory;
    }

    public bool GetWatch ( int id )
    {
        if ( id >= WatchStory.Length )
        {
            Debug.LogError ( id + "大於店長業務數量" );
            return false;
        }
        return WatchStory [id];
    }
    public void SetWatch ( int id ,  bool watch )
    {
        if ( id >= WatchStory.Length )
        {
            Debug.LogError ( id + "大於店長業務數量" );
            return;
        }
        WatchStory [id] = watch;
    }
}
