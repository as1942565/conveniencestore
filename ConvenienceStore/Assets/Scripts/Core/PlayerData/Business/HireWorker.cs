﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

//可招募的員工名單
[DataContract(Namespace= "")]
[System.Serializable]
public class HireWorker
{
    [DataMember]
    [SerializeField]
    private int quantity;
    [DataMember]
    [SerializeField]
    private List<int> canHireID;

    private bool special;

    public HireWorker ()
    {
        quantity = 0;
        canHireID = new List<int> ();
    }

    public int GetQuantity ()
    {
        if ( quantity <= 0 )
        {
            Debug.LogError ( quantity + "可招募的員工必須大於0" );
            return 1;
        }
        return quantity;
    }
    public void SetQuantity ( int num )
    {
        if ( num <= 0 )
        {
            Debug.LogError ( num + "可招募的員工必須大於0" );
            return;
        }
        quantity = num;
    }
    public List<int> GetCanHireID ()
    {
        return canHireID;
    }

    
    public void SetCanHireID ( GameDataManager gm , PlayerDataManager pm )
    {
        int [] steamHideID = new int [] { 7 , 10 , 11 , 13 , 17 , 20 };
        canHireID.Clear ();
        for ( int i = 0; i < gm.workerList.Data.Length; i++ )
        {
            bool hide = false;
            if ( GameLoopManager.instance.GetSteam () == true )
            {
                for ( int h = 0; h < steamHideID.Length; h++ )
                {
                    if ( steamHideID [h] == i )
                    {
                        hide = true;
                        break;
                    }
                }
            }
            if ( hide == true )
                continue;
            if ( pm.employeeList.GetUnlockID ( i ) )
                continue;
            if ( i >= 20 )
                continue;
            canHireID.Add ( i );
        }
        SpecailCondition ( pm );
    }

    bool SpecailCondition ( PlayerDataManager pm )
    {
        for ( int i = 0; i < 3; i++ )
        {
            if ( pm.employeeList.GetEmployee ( i ).GetRelationRank () < 2 )
                return false;
        }
        special = true;
        return true;
    }

    public bool GetSpecial ()
    {
        return special;
    }
}
