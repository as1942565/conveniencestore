﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;
using GameData;

//店長業務時效性加成
[DataContract(Namespace= "")]
[System.Serializable]
public class BusinessAddition
{
    [DataMember]
    [SerializeField]
    private List<BusinessIncomeAddition> income;                                    //商品收入加成資料
    [DataMember]
    [SerializeField]
    private List<BusinessSaleQuantityAddition> saleQuantity;                        //商品銷量加成資料

    public event System.Action<List<BusinessIncomeAddition>> BusinessIncomeChanged;
    public event System.Action<List<BusinessSaleQuantityAddition>> BusinessSaleQuantityChanged;

    public BusinessAddition ()
    {
        income = new List<BusinessIncomeAddition> ();
        saleQuantity = new List<BusinessSaleQuantityAddition> ();
    }

    public float GetIncome ( Commodity.CommodityType type , int totalShift )        //取得收入總加成
    {                                                                               //
        float sum = 0;
        for ( int i = 0; i < income.Count; i++ )
        {
            if ( income [i].type != type || income[i].RunShift < totalShift )
                continue;
            sum += income [i].addition;
        }
        if ( sum == 0 )
            return 1;
        return sum;
    }
    public void AddIncome ( int runShift , Commodity.CommodityType type , float addition , int time , int gruopID )//新增一個收入加成
    {
        BusinessIncomeAddition data;
        data.RunShift = runShift;
        data.type = type;
        data.addition = addition;
        data.time = time;
        data.GroupID = gruopID;
        income.Add ( data );
        if ( BusinessIncomeChanged != null )
        {
            BusinessIncomeChanged ( income );
        }
    }
    public void CheckIncome ( int totalShift , GameDataManager gm , PlayerDataManager pm )                 //檢查收入加成是否過期
    {
        for ( int i = income.Count - 1; i >= 0; i-- )
        {
            if ( income [i].time <= totalShift )
            {
                int id = income [i].GroupID;
                for ( int j = 0; j < gm.businessList.Data.Length; j++ )
                {
                    if ( gm.businessList.Data [j].groupID == id )
                    {
                        pm.businessUnlock.SetMode ( j , BusinessUnlock.state.Available );
                    }
                }
                income.Remove ( income [i] );
            }
        }
        if ( BusinessIncomeChanged != null )
        {
            BusinessIncomeChanged ( income );
        }
    }
    public float GetSaleQuantity ( Commodity.CommodityType type , int totalShift )  //取得銷量總加成
    {
        float sum = 0;
        for ( int i = 0; i < saleQuantity.Count; i++ )
        {
            if ( saleQuantity [i].type != type && saleQuantity[i].RunShift > totalShift )
                continue;
            sum += saleQuantity [i].addition;
        }
        return sum;

    }
    public void AddSaleQuantity ( int runShift , Commodity.CommodityType type , float addition , int time )//新增一個收入加成
    {
        BusinessSaleQuantityAddition data;
        data.RunShift = runShift;
        data.type = type;
        data.addition = addition;
        data.time = time;
        saleQuantity.Add ( data );
        if ( BusinessSaleQuantityChanged != null )
        {
            BusinessSaleQuantityChanged ( saleQuantity );
        }
    }
    public void CheckSaleQuantity ( int totalShift )                                //檢查銷量加成是否過期
    {
        for ( int i = saleQuantity.Count -1; i >= 0; i-- )
        {
            if ( saleQuantity [i].time <= totalShift )
                saleQuantity.Remove ( saleQuantity [i] );
        }
        if ( BusinessSaleQuantityChanged != null )
        {
            BusinessSaleQuantityChanged ( saleQuantity );
        }
    }
}

[System.Serializable]
public struct BusinessIncomeAddition
{
    public int RunShift;
    public Commodity.CommodityType type;
    public float addition;
    public int time;
    public int GroupID;
}

[System.Serializable]
public struct BusinessSaleQuantityAddition
{
    public int RunShift;
    public Commodity.CommodityType type;
    public float addition;
    public int time;
}
