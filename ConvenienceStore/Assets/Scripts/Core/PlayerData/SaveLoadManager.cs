﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public static class SaveLoadManager
{
    public static string GetPath ()
    {
        string path = Environment.GetFolderPath ( Environment.SpecialFolder.MyDocuments );
        string subFolderPath = Path.Combine ( path , "BlackMercuryStudio" );
        string subFolderPath2 = Path.Combine ( subFolderPath , "ConvenienceStore" );
        if ( Directory.Exists ( subFolderPath2 ) == true )
            return subFolderPath2;
        else
            return CreateSaveDirectory ( subFolderPath2 );
    }

    static string CreateSaveDirectory ( string subFolderPath2 )
    {
        Directory.CreateDirectory ( subFolderPath2 );

        using ( StreamWriter sw = File.CreateText ( Path.Combine ( subFolderPath2 , "dontopen.txt" ) ) )
        {
            sw.Write ( "https://reurl.cc/NRyqlk" );
        }	

        return subFolderPath2;
    }

    public static void Save ( PlayerDataManager playerdata , string index )
    {
        string path = GetPath ();
        XMLManager.Save<PlayerDataManager> ( playerdata , Path.Combine ( path , $"SaveFile_{index}.xml" ) );
    }
    public static bool Load ( out PlayerDataManager playerdata , string index )
    {
        string path = GetPath ();
        bool result = XMLManager.TryLoad<PlayerDataManager> ( Path.Combine ( path , $"SaveFile_{index}.xml" ) , out playerdata );
        if ( result == true )
            DLC ( playerdata );
        return result;
    }
    public static void SaveDataSave ( SaveData saveData , string index )   //顯示存檔的資料(金錢 天數 遊玩時段)
    {
        string path = GetPath ();
        XMLManager.Save<SaveData> ( saveData , Path.Combine ( path , $"SaveData_{index}.xml" ) );
    }
    public static bool SaveDataLoad ( out SaveData saveData , string index )
    {
        string path = GetPath ();
        bool result = XMLManager.TryLoad<SaveData> ( Path.Combine ( path , $"SaveData_{index}.xml" ) , out saveData );
        return result;
    }
    public static void OnlyDataSave ( OnlyDataManager onlyData )
    {
        string path = GetPath ();
        XMLManager.Save<OnlyDataManager> ( onlyData , Path.Combine ( path , "OnlyFile.xml" ) );
    }
    public static bool OnlyDataLoad ( out OnlyDataManager onlyData )
    {
        string path = GetPath ();
        bool result = XMLManager.TryLoad<OnlyDataManager> ( Path.Combine ( path , "OnlyFile.xml" ) , out onlyData );
        if ( result == true )
            OMDLC ( onlyData );

        return result; 
    }

    public static void DLC ( PlayerDataManager pm )
    {
        GameDataManager gm = GameLoopManager.instance.gm;

        if ( pm.specialPeople == null )
            pm.specialPeople = new SpecialPeople ( gm.specialPeopleList.Data.peopleData.Length );
        if ( pm.troubleData == null )
            pm.troubleData = new TroubleData ();

        int length = gm.workerList.Data.Length;
        if ( pm.employeeList.GetUnlockLength () != length )
            pm.employeeList.ResetUnlockArray ( length );
        if ( pm.employeeScheduleLog.GetEmployeeLength () != length )
            pm.employeeScheduleLog.ResetEmployeeList ( length );
        if ( pm.cabinet.GetUnlockArray () == null )
        {
            pm.cabinet.Init ( gm.cabinetList );
        }
        if ( pm.activityUnlock.GetWatchArray () == null )
            pm.activityUnlock.Init ( gm.activityList );
        if ( pm.businessUnlock.GetWatchArray () == null )
            pm.businessUnlock.Init ( gm.businessList );
        pm.calender.Init ();
        GameLoopManager.instance.pm = pm;
    }

    public static void OMDLC ( OnlyDataManager om )
    {
        if ( om.unlockActorData == null )
        {
            om.unlockActorData = new UnlockActorData ();
            om.unlockActorData.Init ();
        }
    }
}
