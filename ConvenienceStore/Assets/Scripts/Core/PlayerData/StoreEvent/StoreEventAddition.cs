﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

[DataContract(Namespace= "")]
[System.Serializable]
public class StoreEventAddition
{
    [DataMember]
    [SerializeField]
    private List<StoreEventIncomeAddition> income;
    [DataMember]
    [SerializeField]
    private List<StoreEventPopularAddition> popular;
    [DataMember]
    [SerializeField]
    private StoreEventSpecial special;
    [DataMember]
    [SerializeField]
    private bool [] unlock;

    public StoreEventAddition ()
    {
        income = new List<StoreEventIncomeAddition> ();
        popular = new List<StoreEventPopularAddition> ();
        unlock = new bool [34];
    }

    public float GetIncome ( GameData.Commodity.CommodityType type , int totalShift )
    {
        float sum = 1;
        for ( int i = 0; i < income.Count; i++ )
        {
            if ( income [i].type != type || income [i].totalShift < totalShift )
                continue;
            sum += income [i].addition;
        }
        return sum;
    }
    public void AddIncome ( int runShift , GameData.Commodity.CommodityType type , float addition )
    {
        StoreEventIncomeAddition data;
        data.totalShift = runShift;
        data.type = type;
        data.addition = addition;
        income.Add ( data );
    }
    public void CheckIncome ( int totalShift , GameDataManager gm , PlayerDataManager pm )
    {
        for ( int i = income.Count - 1; i >= 0; i-- )
        {
            income.Remove ( income [i] );
        }
    }
    public float GetPopular ( int totalShift )
    {
        float sum = 1;
        for ( int i = 0; i < popular.Count; i++ )
        {
            if ( popular [i].totalShift < totalShift )
                continue;
            sum += popular [i].addition;
        }
        return sum;
    }
    public void AddPopular ( int runShift , float addition )
    {
        StoreEventPopularAddition data;
        data.totalShift = runShift;
        data.addition = addition;
        popular.Add ( data );
    }
    public void CheckPopular ( int totalShift , GameDataManager gm , PlayerDataManager pm )
    {
        for ( int i = popular.Count -1; i >= 0; i-- )
        {
            popular.Remove ( popular[i] );
        }
    }
    public float GetSpecialAddition ()
    {
        if ( special.enable )
            return special.addition;
        return 1;
    }
    public void SetSpecial ( bool enable , float addition )
    {
        special.enable = enable;
        special.addition = addition;
    }
    public void CancelSpecial ()
    {
        special.enable = false;
    }

    public void SetUnlock ( int id , bool u )
    {
        unlock [id] = u;
    }
    public void StoreEventComplete ()
    {
        int add = 0;
        for ( int i = 0; i < unlock.Length; i++ )
        {
            if ( unlock [i] == true )
                add++;
        }
        if ( add >= 5 )
            Achievement.SetAchievement ( Achievement.achievementNames.Event.ToString () );
    }
}

[System.Serializable]
public struct StoreEventIncomeAddition
{
    public int totalShift;
    public GameData.Commodity.CommodityType type;
    public float addition;
}

[System.Serializable]
public struct StoreEventPopularAddition
{
    public int totalShift;
    public float addition;
}

[System.Serializable]
public struct StoreEventSpecial
{
    public bool enable;
    public float addition;
}
