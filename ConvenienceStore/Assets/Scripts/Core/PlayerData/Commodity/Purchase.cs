﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

//商品進貨資訊
[DataContract(Namespace= "")]
[System.Serializable]
public class Purchase
{                                                                       //
    [DataMember]
    [SerializeField]
    private int [] WaitPurchase;                                        //商品等待進貨的數量
    [DataMember]
    [SerializeField]
    private int [] DefinePurchase;                                      //商品確定進貨的數量
    [DataMember]
    [SerializeField]
    private int MaxPurchaseNum;                                         //每項商品一次進貨上限數

    private Purchase ()
    {

    }
    public Purchase ( int Length )
    {
        WaitPurchase = new int [Length];                                //初始化等待進貨的數量
        DefinePurchase = new int [Length];
        MaxPurchaseNum = 1000000;
    }

    public int GetWaitPurchase ( int ID )                               //取得該編號商品等待進貨的數量
    {
        if ( ID >= WaitPurchase.Length )
        {
            Debug.LogError ( ID + "超過商品資料長度" );
            return 0;
        }
        return WaitPurchase [ID];
    }
    public void AddWaitPurchase ( int ID , int num )                    //增加該編號商品等待進貨的數量
    {
        if ( ID >= WaitPurchase.Length )
        {
            Debug.LogError ( ID + "超過商品資料長度" );
            return;
        }
        WaitPurchase [ID] += num;
    }
    public void ClearWaitPurchase ( int ID )                            //清除該編號商品等待進貨的數量
    {
        if ( ID >= WaitPurchase.Length )
        {
            Debug.LogError ( ID + "超過商品資料長度" );
            return;
        }
        WaitPurchase [ID] = 0;
    }
    public int GetDefinePurchase ( int ID )
    {
        if ( ID >= DefinePurchase.Length )
        {
            Debug.LogError ( ID + "超過商品資料長度" );
            return 0;
        }
        return DefinePurchase [ID];
    }
    public void AddDefinePurchase ( int ID , int num )
    {
        if ( ID >= WaitPurchase.Length )
        {
            Debug.LogError ( ID + "超過商品資料長度" );
            return;
        }
        if ( num < 0 )
        {
            Debug.LogError ( num + "增加確定進貨數量不該小於0" );
            return;
        }
        DefinePurchase [ID] += num;
    }
    public void ClearDefinePurchase ( int ID )
    {
        if ( ID >= DefinePurchase.Length )
        {
            Debug.LogError ( ID + "超過商品資料長度" );
            return;
        }
        DefinePurchase [ID] = 0;
    }
    public int GetAllWaittingNum ()                                     //取得所有等待進貨的總數量
    {
        int num = 0;
        for ( int i = 0; i < WaitPurchase.Length; i++ )
        {
            num += WaitPurchase [i];
        }
        return num;
    }
    public int GetAllWaittingCost ( GameDataManager gm )
    {
        int total = 0;
        for ( int i = 0; i < WaitPurchase.Length; i++ )
        {
            int cost = gm.commodityList.Data [i].cost;
            total += WaitPurchase [i] * cost;
        }
        return total;
    }
    public int GetAllDefiningNum ()                                     //取得所有確定進貨的總數量
    {
        int num = 0;
        for ( int i = 0; i < DefinePurchase.Length; i++ )
        {
            num += DefinePurchase [i];
        }
        return num;
    }
    public int GetMaxPurchaseNumByOne ()                                //取得單一項目最大的進貨數量
    {
        return MaxPurchaseNum;
    }
    public void AddMaxPurchaseNumByOne ( int num )                      //增加單一項目最大的進貨數量
    {
        if ( num <= 0 )
        {
            Debug.LogError ( num + "無法增加進貨上限" );
            return;
        }
        MaxPurchaseNum += num;
    }
}
