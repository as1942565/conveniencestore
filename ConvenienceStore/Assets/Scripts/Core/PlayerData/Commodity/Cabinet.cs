﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;
using Tools;

//商品櫃資訊
[DataContract ( Namespace = "" )]
[System.Serializable]
public class Cabinet
{                                                                       //
    [SerializeField]
    private int [] unlock;                                              //商品櫃解鎖狀態
    [DataMember]
    [SerializeField]
    private IntList [] commodityID;                                     //擺放的商品編號

    private Cabinet ()
    {

    }
    public Cabinet ( GameData.CabinetList list )
    {
        int [] quantity = new int [] { 4 , 4 , 4 , 2 , 1 };
        commodityID = new IntList [quantity.Length];
        for ( int t = 0; t < commodityID.Length; t++ )
        {
            commodityID [t] = new IntList ();
            for ( int i = 0; i < quantity [t]; i++ )
            {
                commodityID [t].Add ( -1 );
            }
        }

        Init ( list );
    }

    public void Init ( GameData.CabinetList list )
    {
        int length = list.Data.Length;
        unlock = new int [length];
        for ( int i = 0; i < length; i++ )
        {
            unlock [i] = list.Data [i].unlockNum;
        }
    }

    public int GetTypeUnlockNum ( int type )                            //取得該種類商品櫃的解鎖欄位數
    {
        if ( type >= unlock.Length )
        {
            Debug.LogError ( $"type: {type} is larger than unlock.Length: {unlock.Length}" );
            return -1;
        }
        return unlock [type];
    }
    public void AddUnlock ( int type )                                  //增加該種類商品櫃的解鎖欄位數
    {
        if ( type >= unlock.Length )
        {
            Debug.LogError ( $"type: {type} is larger than unlock.Length: {unlock.Length}" );
            return;
        }
        if ( unlock [type] >= 4 )
        {
            Debug.LogError ( $"第{type}種商品櫃已達最大值" );
            return;
        }
        unlock [type]++;
    }
    public int GetCommodityID ( int cabinetType , int cabinetID )       //取得該種類商品櫃的那格欄位的商品編號
    {
        if ( cabinetType >= commodityID.Length )
        {
            Debug.LogError ( cabinetType + "超過商品櫃子種類數" );
            return -1;
        }
        if ( cabinetID >= commodityID [cabinetType].Count )
        {
            Debug.LogError ( $"{cabinetID} 超過商品櫃欄位數{commodityID [cabinetType].Count}" );
            return -1;
        }
        return commodityID [cabinetType] [cabinetID];
    }
    public void SetCommodityID ( int type , int cabinetID , int commodityID )    //設定該種類商品櫃的那格欄位的商品編號
    {
        if ( type >= this.commodityID.Length )
        {
            Debug.LogError ( type + "超過商品櫃子種類數" );
            return;
        }
        if ( cabinetID >= this.commodityID [type].Count )
        {
            Debug.LogError ( $"{cabinetID} 超過商品櫃欄位數{this.commodityID [type].Count}" );
            return;
        }
        this.commodityID [type] [cabinetID] = commodityID;
    }
    public int GetCabinetID ( int id )
    {
        for ( int t = 0; t < commodityID.Length; t++ )
        {
            for ( int i = 0; i < commodityID [t].Count; i++ )
            {
                if ( commodityID [t] [i] == id )
                    return i;
            }
        }
        Debug.LogError ( $"再商品櫃中找不到 { id }號商品" );
        return 0;
    }
    public void ClearID ()                                              //清除所有已解鎖的商品欄位
    {
        for ( int t = 0; t < commodityID.Length; t++ )
        {
            for ( int i = 0; i < commodityID [t].Count; i++ )
            {
                commodityID [t] [i] = -1;
            }
        }
    }
    public int [] GetUnlockArray ()
    {
        return unlock;
    }
    public int [] GetAllSoldCommodity ()
    {
        List<int> ids = new List<int> ();
        for ( int t = 0; t < commodityID.Length; t++ )
        {
            for ( int i = 0; i < commodityID[t].Count; i++ )
            {
                if ( commodityID [t] [i] == -1 )
                    continue;
                ids.Add ( commodityID [t] [i] );
            }
        }
        return ids.ToArray ();
    }
}
