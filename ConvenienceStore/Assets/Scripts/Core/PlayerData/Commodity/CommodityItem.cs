﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;
using Tools;
using System.Linq;

//商品資訊
[DataContract(Namespace= "")]
[System.Serializable]
public class CommodityItem
{                                                                       //   
    [DataMember]
    [SerializeField]
    private Data [] data;                                               //商品資料

    [System.Serializable]
    public struct Data                                                  //商品資料
    {
        public int star;                                                //商品星級
        public int sale;                                                //商品售價
        public bool unlock;                                             //商品解鎖狀態
        public int saleQuantityAfterUpdate;
        public int saleQuantity;                                        //商品銷售數量(累積)
        public GameData.Commodity.PopularAddition [] popular;                              //商品受歡迎程度
        public int [] improveCondition;
    }

    private CommodityItem ()
    {

    }
    public CommodityItem ( GameData.CommodityList list )
    {
        GameData.Commodity [] commoditys = list.Data;
        data = new Data [commoditys.Length];
        
        for ( int i = 0; i < commoditys.Length; i++ )
        {
            data [i].star = commoditys [i].star;
            data [i].sale = commoditys [i].price[commoditys [i].star];
            data [i].unlock = commoditys [i].unlock;                     //紀錄商品解鎖狀態
            data [i].saleQuantity = 0;
            data [i].popular = commoditys [i].salesRate.Cast<GameData.Commodity.PopularAddition>().ToArray();//建構並記錄受歡迎程度

            data [i].improveCondition = new int [list.Data [i].ImproveCondition.Length];
            if ( data [i].improveCondition.Length == 0 )
                continue;
            for ( int j = 0; j < data [i].improveCondition.Length; j++ )
            {
                int condition = list.Data [i].ImproveCondition [j];
                data [i].improveCondition [j] = condition;
            }
        }
    }
    public System.Action<CommodityItem> OnItemUnlock;
    public System.Action<CommodityItem> OnMaxStar;

    public Data GetData ( int ID )                                      //取得商品資料
    {
        if ( ID >= data.Length )
        {
            Debug.LogError ( ID + "超過商品資料長度" );
            return new Data();
        }
        return data [ID];
    }
    public void SetStar ( int ID , int star )                           //設定商品星級
    {
        if ( ID >= data.Length )
        {
            Debug.LogError ( ID + "超過商品資料長度" );
            return;
        }
        if ( star <= 0 )
        {
            Debug.LogError ( star + "無法增加星數" );
            return;
        }
        data[ID].star = star;
        if ( OnMaxStar != null )
        {
            OnMaxStar ( this );
        }
    }
    public void SetSale ( int ID , int sale )                           //設定商品售價
    {
        if ( ID >= data.Length )
        {
            Debug.LogError ( ID + "超過商品資料長度" );
            return;
        }
        if ( sale <= 0 )
        {
            Debug.LogError ( sale + "售價不該低於0" );
            return;
        }
        data [ID].sale = sale;
    }
    public void SetUnlock ( int ID )                                    //設定商品解鎖狀態
    {
        if ( ID >= data.Length )
        {
            Debug.LogError ( ID + "超過商品資料長度" );
            return;
        }
        data [ID].unlock = true;
        if(OnItemUnlock!= null )
        {
            OnItemUnlock ( this );
        }
        ResearchComplete ();
    }
    public void ResearchComplete ()
    {
        for ( int i = 0; i < data.Length; i++ )
        {
            if ( GameLoopManager.instance.gm.commodityList.Data [i].type == GameData.Commodity.CommodityType.special )
                continue;
            if ( data [i].unlock == false )
            {
                return;
            }
        }
        Achievement.SetAchievement ( Achievement.achievementNames.Commodity.ToString () );
    }
    public void SetImproveCondition ( int ID , int star , int condition )
    {
        if ( ID >= data.Length )
        {
            Debug.LogError ( ID + "超過商品資料長度" );
            return;
        }
        data [ID].improveCondition[star] = condition;
    }
    public void AddSaleQuantity ( int ID , int num )                    //增加商品銷售數量
    {
        if ( ID >= data.Length )
        {
            Debug.LogError ( ID + "超過商品資料長度" );
            return;
        }
        data [ID].saleQuantity += num;
    }
    public void SetSaleQuantityAfterUpdate ( int ID , int num )
    {
        if ( ID >= data.Length )
        {
            Debug.LogError ( ID + "超過商品資料長度" );
            return;
        }
        data [ID].saleQuantityAfterUpdate = num;
    }
    public int GetResearchNumByType ( GameDataManager gm , int type )   //取得該種類的研發數量
    {
        if ( type >= 3 )
        {
            Debug.LogError ( type + "超過可研發的種類數" );
            return 0;
        }
        int num = 0;
        for ( int i = 0; i < data.Length; i++ )
        {
            if ( (int)gm.commodityList.Data [i].type == type && data [i].unlock )
                num++;
        }
        return num;
    }
    public int GetResearchQuantity ()
    {
        int num = 0;
        for ( int i = 0; i < data.Length; i++ )
        {
            if ( data [i].unlock )
                num++;
        }
        return num;
    }
    public bool GetImproveUnlock ( int ID , GameDataManager gm )        //取得該商品的改良前置狀態
    {
        if ( gm.commodityList.Data [ID].type == GameData.Commodity.CommodityType.special )
            return false;
        int star = data [ID].star;
        if ( star >= 3 )
            return true;
        return data [ID].saleQuantity - data [ID].saleQuantityAfterUpdate >= data [ID].improveCondition [star];
    }   
    public bool GetImproveComplete ( int ID )                           //取得該商品的改良完成狀態
    {
        if ( ID >= data.Length )
        {
            Debug.LogError ( ID + "超過商品資料長度" );
            return false;
        }
        return data [ID].star > 2;
    }
    public void RanPopular ( int id , int shift , int up , int down )   //該時段的受歡迎程度隨機變化
    {
        int RanNum = Random.Range ( 0 , 100 );                          //0~100隨機抽一個數字
        if ( up > RanNum )                                              //如果隨機數介於0~上升數值
        {
            if ( (int)data [id].popular[shift] < 4 )
                data [id].popular [shift]++;                            //上升
            return;
        }
        if ( 100 - down < RanNum )                                      //如果隨機數介於下降數值~100
        {
            if ( data [id].popular [shift] <= 0 )
            {
                data [id].popular [shift] = 0;
                return;
            }
            data [id].popular [shift]--;                                //下降
            return;
        }
        //其餘的機率都是不變
    }

    public int RanGetSpecialID ( GameDataManager gm )                   //隨機取得一種特殊商品編號
    {
        List<int> specialID = new List<int> ();
        for ( int i = 0; i < data.Length; i++ )
        {
            if ( (int)gm.commodityList.Data [i].type >= 3 )
            {
                specialID.Add ( i );
            }
        }
        specialID.Shuffle ();
        return specialID [0];
    }
}
