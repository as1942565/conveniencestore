﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

//商品資訊
[DataContract ( Namespace = "" )]
[System.Serializable]
public class Storage
{
    [DataMember]
    [SerializeField]
    private int MaxStorage;                                             //商品最大存貨數
    [DataMember]
    [SerializeField]
    private int [] quantity;                                            //商品持有數

    private Storage ()
    {

    }
    public Storage ( GameData.CommodityList list )
    {
        MaxStorage = 100;
        quantity = new int [list.Data.Length];                          //初始化商品持有數
        for ( int i = 0; i < list.Data.Length; i++ )
        {
            quantity [i] = 0;                                           //紀錄商品持有數
        }
    }

    public int GetMaxStorage ()                                         //取得最大存貨數
    {
        return MaxStorage;
    }
    public void SetMaxStorage ( int storage )                           //設定最大存貨數
    {
        if ( storage < 100 )
        {
            Debug.LogError ( storage + "庫存不能低於100" );
            return;
        }
        MaxStorage = storage;
    }
    public int GetQuantity ( int ID )                                   //取得該編號商品持有數
    {
        if ( ID >= quantity.Length )
        {
            Debug.LogError ( ID + "超過商品資料長度" );
            return 0;
        }
        return quantity [ID];
    }

    public System.Action<int , int , int> OnAddQuantityChanged;

    public bool AddQuantity ( int ID , int num , int shift )                        //增加該編號商品持有數
    {
        if ( ID >= quantity.Length )
        {
            Debug.LogError ( ID + "超過商品資料長度" );
            return false;
        }
        quantity [ID] += num;

        if ( num > 0 )
            OnAddQuantityChanged?.Invoke ( ID , num , shift );
        return true;
    }
    public void SetQuantity ( int ID , int num )
    {
        if ( ID >= quantity.Length )
        {
            Debug.LogError ( ID + "超過商品資料長度" );
            return ;
        }
        quantity [ID] = num;
    }
    public int GetTotalItem ()                                          //取得所有商品的持有數(含特殊商品)
    {
        int sum = 0;
        foreach ( int i in quantity )
            sum += i;
        return sum;
    }                                       
    public int GetStorageRemain ()                                      //取得剩餘的空間數量
    {
        return MaxStorage - GetTotalItem ();
    }                                   
    public int GetSpecialQuantity ( GameDataManager gm )                //取得特殊商品持有數
    {
        int num = 0;
        for ( int i = 0; i < quantity.Length; i++ )
        {
            if ( (int)gm.commodityList.Data [i].type == 3 )
                num += quantity [i];
        }
        return num;
    }
}
