﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;
//玩家資料管理器
[DataContract]
[KnownType(typeof(MainEmployee))]
[System.Serializable]
public class PlayerDataManager
{
    [Header ( "商品" )]
    [DataMember]
    public CommodityItem commodityItem;                         //商品資訊
    [DataMember]
    public Cabinet cabinet;                                     //商品櫃
    [DataMember]
    public Purchase purchase;                                   //商品進貨
    [DataMember]
    public Storage storage;                                     //商品庫存

    [Header ( "店面擴建" )]
    [DataMember]
    public StoreExpansionUnlock storeExpansionUnlock;           //店面擴建解鎖狀態

    [Header ( "員工活動" )]
    [DataMember]
    public ActivityUnlock activityUnlock;                       //員工動解鎖狀態
    [DataMember]
    public ActivityScheduale activityScheduale;                 //員工委託行程
    [Header ( "店長業務" )]
    [DataMember]
    public BusinessUnlock businessUnlock;                       //店長業務解鎖狀態
    [Header ( "可招募的員工" )]
    [DataMember]
    public HireWorker hireWorker;                               //可招募的員工
    [Header ( "員工排班" )]
    [DataMember]
    public EmployeeScheduing employeeScheduing;                 //員工排班

    [Header ( "員工屬性" )]
    [DataMember]
    public EmployeeList employeeList;                           //員工屬性列表

    [Header ( "日曆" )]
    [DataMember]
    public Calender calender;                                   //日曆

    [Header ( "玩家資料" )]
    [DataMember]
    public PlayerData playerData;                               //玩家資料

    [Header ( "公式的加成資料" )]
    [DataMember]
    public ActivityAddition activityAddition;                   //員工業務加成
    [DataMember]
    public BusinessAddition businessAddition;                   //店長業務加成
    [DataMember]
    public StoreExpansionAddition storeExpansionAddition;       //店面擴建加成
    [DataMember]
    public SpecialAddition specialAddition;                     //活動效果加倍
    [DataMember]
    public PunishAddition punishAddition;                       //竊盜與髒亂事件
    [DataMember]
    public StoreEventAddition storeEventAddition;

    [Header ( "遊戲訊息" )]
    [DataMember]
    public GameLog gameLog;                                     //遊戲訊息
    [DataMember]
    public StoreLog storeLog;                                   //店鋪情報是否重複
    [DataMember]
    public EmployeeScheduleLog employeeScheduleLog;
    [DataMember]
    public ActivityScheduleLog activityScheduleLog;

    [Header ( "其他" )]
    [DataMember]
    public SpecialPeople specialPeople;
    public TroubleData troubleData;


    public void Init ( GameDataManager gm )
    {
        commodityItem = new CommodityItem ( gm.commodityList );
        cabinet = new Cabinet ( gm.cabinetList );
        purchase = new Purchase ( gm.commodityList.Data.Length );
        storage = new Storage ( gm.commodityList );
        storeExpansionUnlock = new StoreExpansionUnlock ( gm.storeExpansionList );
        activityUnlock = new ActivityUnlock ( gm.activityList );
        activityScheduale = new ActivityScheduale ();
        businessUnlock = new BusinessUnlock ( gm.businessList );
        hireWorker = new HireWorker ();
        employeeScheduing = new EmployeeScheduing ( gm.schedulingCabinetList );
        employeeList = new EmployeeList ( gm );
        calender = new Calender ();
        playerData = new PlayerData ();
        
        activityAddition = new ActivityAddition ();
        businessAddition = new BusinessAddition ();
        storeExpansionAddition = new StoreExpansionAddition ();
        specialAddition = new SpecialAddition ();
        punishAddition = new PunishAddition ();
        storeEventAddition = new StoreEventAddition ();

        gameLog = new GameLog ( gm.commodityList.Data.Length , storage , this );
        storeLog = new StoreLog ( gm.commodityList.Data.Length , gm.storeExpansionList.Data.Length , gm.commodityList );
        employeeScheduleLog = new EmployeeScheduleLog ( gm.workerList.Data.Length , gm.schedulingCabinetList.Data.Length );
        activityScheduleLog = new ActivityScheduleLog ();

        specialPeople = new SpecialPeople ( gm.specialPeopleList.Data.peopleData.Length );
        troubleData = new TroubleData ();
    }
}
