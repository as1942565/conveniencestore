﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization;
using UnityEngine.SceneManagement;
using Tools;
using System.IO;

[DataContract ( Namespace = "" )]
[Serializable]
public class Calender
{                                                                       //
    public enum Shift                                                   //三個班次
    {
        morning = 0,
        noon = 1,
        night = 2,
    }
    [DataMember]
    [SerializeField]
    private int totalShift;                                             //經過的總班數

    private int [] maxEnergy;                                           //每個時段的最大行動點(2 2 1)

    private int _energy;                                                //目前班次剩餘的行動點(最大為2)

    private List<int> getPower = new List<int> ();                      //第幾天增加行動點

    [DataMember]
    [SerializeField]
    private int expandSpecialCabiinet;                                  //第幾天擴建特殊商品櫃

    private int energy
    {
        set { _energy = value;
            if ( OnEnergyChange != null )
            {
                OnEnergyChange ( this );
            }
        }
        get
        {
            return _energy;
        }
    }
    public event Action<Calender> AfterNextShift;                          //下一個時段該做的事
    public event Action<Calender> AfterNextShiftDone;                          //下一個時段該做的事
    public event Action<Calender> BeforeNextShift;                          //下一個時段該做的事
    public event Action<Calender> OnEnergyChange;
    public const int MaximumDay = 30;                                   //遊戲運行總天數

    public Calender ()
    {
        totalShift = 0;
        Init ();
    }

    public void Init ()
    {
        maxEnergy = new int [] { 1 , 1 , 1 };
        energy = 1;
    }

    public int TotalShift { get { return totalShift; } }                //取得經過的班次

    private const Shift first = Shift.morning;                          //班次 預設為早班

    public int GetDay ()                                                //取得天數 -> 從0開始
    {
        return Mathf.FloorToInt ( ( totalShift + (int)first ) / 3 );
    }
    public int GetYesterday ()
    {
        int day = Mathf.FloorToInt ( ( totalShift + (int)first ) / 3 ) - 1;
        if ( day < 0 )
            return 0;
        return day;
    }
    public Shift GetShift ()                                            //取得時段
    {
        return (Shift)( ( totalShift + (int)first ) % 3 );
    }

    public int GetEnergy ()                                             //取得該班次剩餘的能量
    {
        return energy;
    }

    public void AddMaxEngry ()
    {
        //SetWhatDayToGetPower ();

        if ( maxEnergy [0] == 1 )
        {
            maxEnergy [0]++;
            if ( GetShift() == Shift.morning )
                energy++;
            return;
        }
        if ( maxEnergy [1] == 1 )
        {
            maxEnergy [1]++;
            if ( GetShift() == Shift.noon )
                energy++;
            return;
        }
        Debug.LogError ( "無法再增加行動點" );
    }
    public int GetMaxEngrgy ()                                          //取得目前的最大行動點
    {
        int sum = 0;
        for ( int i = 0; i < maxEnergy.Length; i++ )
        {
            sum += maxEnergy [i];
        }
        return sum;
    }

    public int GetDayEneryLeft ()                                       //取得這一天剩下的行動點
    {
        int s = (int)GetShift ();                                       //紀錄現在的班次
        int remainEnergy = GetEnergy ();                                //取得現在這個班次剩下的行動點
        for ( int i = s + 1; i < maxEnergy.Length; i++ )                //從下一次班次開始到最後一個班次
        {
            remainEnergy += maxEnergy [i];                              //計算剩下的班次還有多少行動點
        }
        return remainEnergy;                                            //回傳這一些剩下的行動點
    }
    
    IEnumerator WaitForStoryEnd ()
    {
        if ( GetShift () == Calender.Shift.night )
        {
            GameLoopManager.WaitStoryLog = true;
            UI.BuildLog.self.ShowEmployeeSchedlue += UI.BuildLog.self.Show;
            UI.BuildLog.self.Open ();
            GameLoopManager.instance.StartCoroutine ( UI.BuildLog.self.Play () );
        }

        while ( GameLoopManager.WaitStoryLog == true )
        {
            yield return null;
        }

        while ( StoryManager.self.ReadActivityDrama == true )
        {
            yield return null;
        }
        while ( GameLoopManager.ReadStory == true )
        {
            yield return null;
        }
        Before ();
    }

    void Before ()
    {
        GameLoopManager.CanAfterNextShift = false;
        if ( GetDay () < MaximumDay )
        {
            if ( BeforeNextShift != null )
            {
                BeforeNextShift ( this );
            }
        }

        totalShift += 1;
        if ( OnNextShift != null )
        {
            OnNextShift ( this );
        }

        if ( GetDay () >= 15 && GameLoopManager.instance.GetDemo () == true )
        {
            UI.MessageBox.Notice ( "試玩版到此結束，感謝你的遊玩。" );
            string subFolderPath2 = SaveLoadManager.GetPath ();
            File.Delete ( Path.Combine ( subFolderPath2 , $"SaveFile_Auto.xml" ) );
            File.Delete ( Path.Combine ( subFolderPath2 , $"SaveData_Auto.xml" ) );
            File.Delete ( Path.Combine ( subFolderPath2 , $"ScreenShotAuto.png" ) );
            SceneManager.LoadScene ( "GameStart" );
            return;
        }
        if ( GetDay () >= 29 )
        {
            string subFolderPath2 = SaveLoadManager.GetPath ();
            File.Delete ( Path.Combine ( subFolderPath2 , $"SaveFile_Auto.xml" ) );
            File.Delete ( Path.Combine ( subFolderPath2 , $"SaveData_Auto.xml" ) );
            File.Delete ( Path.Combine ( subFolderPath2 , $"ScreenShotAuto.png" ) );
            BadEnd ();
            totalShift--;
            return;
        }
        GameLoopManager.instance.StartCoroutine ( After () );
    }

    void BadEnd ()
    {
        StoryManager.self.ReadEndDrama = true;

        if ( StoryManager.self == null )
        {
            SceneManager.LoadScene ( "GameStart" );
            return;
        }
        string language = GameLoopManager.instance.om.optionData.GetLanguageStr ();
        if ( GameLoopManager.instance.pm.businessUnlock.GetMode ( 3 ) == BusinessUnlock.state.hide )
        {
            StoryManager.self.StartStory ( $"Data/Story/{language}" , "NoGirl" );
        }
        else
        {
            StoryManager.self.StartStory ( $"Data/Story/{language}" , "NoMoney" );
        }
    }

    public Action<Calender> OnNextShift;
    public void UseEnergy ()                                            //消耗行動點
    {
        energy -= 1;
        if ( energy == 0 )                                              //如果該班次的行動點消耗完畢
        {
            if ( PeopleManager.self != null )
                PeopleManager.self.StopCreatePoeple ();
            if ( energy < 0 )
            {
                Debug.LogError ( "行動點變成負數!" );
                energy = 0;
            }
            
            GameLoopManager.instance.StartCoroutine ( WaitForStoryEnd () );
        }

    }

    public Action<Calender> CheckSpecialDay;
    private const string BGMPath = "Audios/BGM/Main/Shift";
    public IEnumerator After ()
    {   
        while ( GameLoopManager.CanAfterNextShift == false )
        {
            yield return null;
        }
        NextShift.Trouble.Run ( GameLoopManager.instance.pm , this );
        yield return null;
        while ( GameLoopManager.Trouble == true )
        {
            yield return null;
        }

        while ( GameLoopManager.WaitStoryLog == true )
        {
            yield return null;
        }
        
        if ( CheckSpecialDay != null )
        {
            CheckSpecialDay ( this );
        }

        yield return null;
        
        while ( GameLoopManager.SpecialDay == true )
        {
            yield return null;
        }

        energy = maxEnergy [(int)GetShift ()];                      //行動點改為下個時段的最大行動點
        if ( GetDay () < MaximumDay )
        {
            if ( AfterNextShift != null )
            {
                AfterNextShift ( this );
            }
            if ( AfterNextShiftDone != null )
            {
                AfterNextShiftDone ( this );
            }
        }
        string shift = ( (int)GetShift () ).ToString ();
        BGMManager.self.PlayBGM ( BGMPath + shift , GameLoopManager.instance.om.audioData );
    }

    public void SetWhatDayToGetPower ()
    {
        if ( getPower.Count >= 2 )
        {
            Debug.LogError ( "不應該加超過2點" );
            return;
        }
        int day = GetDay ();
        getPower.Add ( day );
    }
    public int [] GetWhatDayToGetPower ()
    {
        return getPower.ToArray ();
    }
    public void SetExpandSpecialCabiinet ()
    {
        int day = GetDay ();
        expandSpecialCabiinet = day;
    }
    public int GetExpandSpecialCabiinet ()
    {
        return expandSpecialCabiinet;
    }
}
