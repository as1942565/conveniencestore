﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

[DataContract(Namespace= "")]
[System.Serializable]
public class TroubleData
{
    [DataMember]
    [SerializeField]
    private bool [] WatchStory;

    private int chance = 10;

    public TroubleData ()
    {
        WatchStory = new bool [4];
    }

    public bool GetWatchStory ( int id )
    {
        if ( id >= WatchStory.Length )
        {
            Debug.LogError ( id + "超過故事數量" );
            return false;
        }
        return WatchStory [id];
    }

    public void SetWatchStory ( int id , bool watch )
    {
        if ( id >= WatchStory.Length )
        {
            Debug.LogError ( id + "超過故事數量" );
            return;
        }
        WatchStory [id] = watch;
        CheckWatchStory ();
    }

    public int GetChance ()
    {
        return chance;
    }

    public int GetStoryID ()
    {
        List<int> id = new List<int> ();
        for ( int i = 0; i < WatchStory.Length; i++ )
        {
            if ( WatchStory [i] == false )
                id.Add ( i );
        }
        int ran = Random.Range ( 0 , id.Count );
        return id [ran];
    }

    private void CheckWatchStory ()
    {
        bool allWatch = true;
        for ( int i = 0; i < WatchStory.Length; i++ )
        {
            if ( WatchStory [i] == false )
            {
                allWatch = false;
                break;
            }
        }
        if ( allWatch == false )
            return;

        for ( int i = 0; i < WatchStory.Length; i++ )
        {
            WatchStory [i] = false;
        }
    }
}
