﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization;
using Tools;

//遊戲紀錄 (昨日銷售)
[DataContract ( Namespace = "" )]
[Serializable]
public class ActivityScheduleLog
{
    [DataMember]
    [SerializeField]
    private ActivityScheduleList [] Scheduales;
    [DataMember]
    [SerializeField]
    private int [] RunQuantity;
    [DataMember]
    [SerializeField]
    private int [] SuccessQuantity;
    [DataMember]
    [SerializeField]
    private int [] FailQuantity;

    public ActivityScheduleLog ()
    {
        int totalDay = Calender.MaximumDay;
        RunQuantity = new int [totalDay];
        SuccessQuantity = new int [totalDay];
        FailQuantity = new int [totalDay];

        Scheduales = new ActivityScheduleList [totalDay];
        for ( int i = 0; i < Scheduales.Length; i++ )
        {
            Scheduales [i] = new ActivityScheduleList ();
        }
        
        //UI.SchedualeManager.self.OnAddSchedule += OnGetSchedule;
    }
    public int GetSuccessed ( int day )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return 0;
        }
        return SuccessQuantity [day];
    }
    public int GetFail ( int day )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return 0;
        }
        return FailQuantity [day];
    }
    public int GetRun ( int day )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return 0;
        }
        return RunQuantity [day];
    }
    public int GetTotal ( int day )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return 0;
        }
        return SuccessQuantity [day] + FailQuantity [day] + RunQuantity [day];
    }
    public ActivityScheduale.Scheduale [] GetScheduales ( int day )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return null;
        }
        return Scheduales [day].ToArray ();
    }
    public void OnGetSchedule ( Calender calender , ActivityScheduale.Scheduale scheduale )
    {
        int day = calender.GetDay ();
        int totalShift = calender.TotalShift;
        int executeDay = ( scheduale.GetExecuteShift () - totalShift ) / 3;
        for ( int d = 0; d < executeDay + 1; d++ )
        {
            int totalDay = day + d;
            if ( totalDay >= Scheduales.Length )
                continue;
            Scheduales [totalDay].Add ( scheduale );
            if ( d < executeDay )
            {
                RunQuantity [totalDay]++;
            }
        }
    }
    public void OnGetSuccessQuantity ( Calender calender )
    {
        int day = calender.GetDay ();
        SuccessQuantity [day]++;
    }
    public void OnGetFailQuantity ( Calender calender )
    {
        int day = calender.GetDay ();
        FailQuantity [day]++;
    }
}
