﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization;
using Tools;

[DataContract ( Namespace = "" )]
[Serializable]
public class EmployeeScheduleLog
{
    [DataMember]
    [SerializeField]
    private IntList [] employeeSchedule;

    private EmployeeScheduleLog  ()
    {

    }
    public EmployeeScheduleLog ( int workerLength , int scheduleLength )
    {
        int totalDay = Calender.MaximumDay;
        employeeSchedule = new IntList [totalDay];
        for ( int d = 0; d < employeeSchedule.Length; d++ )
        {
            employeeSchedule [d] = new IntList ();
            for ( int i = 0; i < workerLength; i++ )
            {
                employeeSchedule [d].Add ( 3 );
            }
        }
    }

    public int GetEmployeeLength ()
    {
        return employeeSchedule.Length;
    }
    public void ResetEmployeeList ( int workerLength )
    {
        int totalDay = Calender.MaximumDay;
        IntList [] temp = new IntList [totalDay];
        for ( int d = 0; d < temp.Length; d++ )
        {
            temp [d] = new IntList ();
            for ( int i = 0; i < workerLength; i++ )
            {
                if ( i >= employeeSchedule [d].Count )
                    temp [d].Add ( 3 );
                else
                    temp [d] = employeeSchedule [d];
            }
        }
        employeeSchedule = temp;
    }
    
    public int GetEmployeeShift ( int day , int employeeID , EmployeeList employeeList )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return 3;
        }
        if ( employeeID >= employeeSchedule[day].Count )
        {
            Debug.LogError ( employeeID + "超過員工數量" );
            return 3;
        }
        if ( !employeeList.GetUnlockID ( employeeID ) )
        {
            Debug.LogError ( employeeID + "員工尚未解鎖" );
            return 3;
        }
        return employeeSchedule [day] [employeeID];
    }
    
    public int [] GetEmployeeIDWithShift ( int day , int shift , EmployeeList employeeList )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return new int [] { -1 };
        }
        if ( shift >= 3 )
        {
            Debug.LogError ( day + "超過班次種類數" );
            return new int [] { -1 };
        }

        List<int> employeeID = new List<int> ();
        for ( int i = 0; i < employeeSchedule[day].Count; i++ )
        {
            if ( employeeList.GetUnlockID ( i ) && employeeSchedule[day][i] == shift )
            {
                employeeID.Add ( i );
            }
        }
        return employeeID.ToArray ();
    }
    public int [] GetMainIDWithShift ( int day , int shift , EmployeeList employeeList )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return new int [] { -1 };
        }
        if ( shift >= 3 )
        {
            Debug.LogError ( day + "超過班次種類數" );
            return new int [] { -1 };
        }

        List<int> employeeID = new List<int> ();
        for ( int i = 0; i < employeeSchedule[day].Count; i++ )
        {
            if ( employeeList.GetUnlockID ( i ) && employeeList.GetEmployee ( i ).IsMain () == true && employeeSchedule[day][i] == shift )
            {
                employeeID.Add ( i );
            }
        }
        return employeeID.ToArray ();
    }
    public List<int> GetWorkMainID ( int yesterday , EmployeeList employeeList )
    {
        if ( yesterday >= Calender.MaximumDay )
        {
            Debug.LogError ( yesterday + "超過遊戲天數" );
            return null;
        }
        List<int> employeeID = new List<int> ();
        for ( int i = 0; i < 3; i++ )
        {
            if ( employeeList.GetUnlockID ( i ) && employeeSchedule[yesterday][i] != 3 )
            {
                employeeID.Add ( i );
            }
        }
        return employeeID;
    }
    public int [] GetRestEmployeeIDs ( int day , EmployeeList employeeList )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return new int [] { -1 };
        }
        List<int> employeeID = new List<int> ();
        for ( int i = 0; i < employeeSchedule[day].Count; i++ )
        {
            if ( employeeList.GetUnlockID ( i ) && employeeSchedule[day][i] == 3 )
            {
                employeeID.Add ( i );
            }
        }
        return employeeID.ToArray ();
    }
    public void SetEmployeeID ( int day , int employeeID , int shift )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return;
        }
        if ( employeeID >= employeeSchedule [day].Count )
        {
            Debug.LogError ( employeeID + "超過員工數量" );
            return;
        }
        if ( shift >= 3 )
        {
            Debug.LogError ( shift + "超過班次種類數" );
            return;
        }
        employeeSchedule [day] [employeeID] = shift;
    }

    public bool IsWorkAtNight ( int day , int employeeID , int shift )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return false;
        }
        if ( employeeID >= employeeSchedule [day].Count )
        {
            Debug.LogError ( employeeID + "超過員工數量" );
            return false;
        }
        if ( shift >= 3 )
        {
            Debug.LogError ( shift + "超過班次種類數" );
            return false;
        }
        for ( int d = 0; d < employeeSchedule.Length; d++ )
        {
            for ( int i = 0; i < employeeSchedule[d].Count; i++ )
            {
                if ( employeeSchedule[d][employeeID] == 1 || employeeSchedule[d][employeeID] == 2 )
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void ResetEmployeeID ( int day , int employeeID )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return;
        }
        if ( employeeID >= employeeSchedule [day].Count )
        {
            Debug.LogError ( employeeID + "超過員工數量" );
            return;
        }
        employeeSchedule [day] [employeeID] = 3;
    }
    public void ResetAllEmployeeID ( int day )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return;
        }
        for ( int i = 0; i < employeeSchedule[day].Count; i++ )
        {
            employeeSchedule [day] [i] = 3;
        }
    }

    public int GetScheduleQuantity ( int day , int shift , EmployeeList employeeList )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return 0;
        }
        if ( shift >= 3 )
        {
            Debug.LogError ( day + "超過班次種類數" );
            return 0;
        }
        return GetEmployeeIDWithShift ( day , shift , employeeList ).Length;
    }
}
