﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Tools;
using System.Runtime.Serialization;

[DataContract ( Namespace = "" )]
[Serializable]
public class StoreLog
{
    [DataMember]
    [SerializeField]
    private BoolList [] improve;
    [DataMember]
    [SerializeField]
    private BoolList [] sold;
    [DataMember]
    [SerializeField]
    private BoolList [] expansion;
    [DataMember]
    [SerializeField]
    private BoolList [] schedule;
    [DataMember]
    [SerializeField]
    private bool [] clean;
    [DataMember]
    [SerializeField]
    private bool [] police;
    [DataMember]
    [SerializeField]
    private bool [] CanUpdateNotice;
    [DataMember]
    [SerializeField]
    private List<string> notice;

    private StoreLog ()
    {

    }
    public StoreLog ( int commodityLength , int expansionLength , GameData.CommodityList commodityList )
    {
        int maxShift = Calender.MaximumDay * 3;
        improve = new BoolList [maxShift];
        sold = new BoolList [maxShift];
        expansion = new BoolList [maxShift];
        schedule = new BoolList [maxShift];

        for ( int s = 0; s < maxShift; s++ )
        {
            improve [s] = new BoolList ();
            sold [s] = new BoolList ();
            for ( int i = 0; i < commodityLength; i++ )
            {
                improve [s].Add ( false );
                sold [s].Add ( false );
            }

            expansion [s] = new BoolList ();
            for ( int i = 0; i < expansionLength; i++ )
            {
                expansion [s].Add ( false );
            }
            schedule [s] = new BoolList ();
            for ( int i = 0; i < 3; i++ )
            {
                schedule [s].Add ( false );
            }
        }

        clean = new bool [maxShift];
        police = new bool [maxShift];
        notice = new List<string> ();
        CanUpdateNotice = new bool [commodityList.Data.Length];
    }
    public bool GetImprove ( int day , int commodityID )
    {
        if ( day * 3 >= improve.Length )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return false;
        }
        if ( commodityID >= improve [day * 3].Count )
        {
            Debug.LogError ( commodityID + "超過商品數量" );
            return false;
        }
        bool repeat = false;
        for ( int i = 0; i < 3; i++ )
        {
            if ( improve [day * 3 + i] [commodityID] )
            {
                repeat = true;
                break;
            }
        }
        return repeat;
    }
    public void SetImprove ( int shift , int commodityID , bool repeat )
    {
        if ( shift >= improve.Length )
        {
            Debug.LogError ( shift + "超過遊戲總時段數" );
            return;
        }
        if ( commodityID >= improve [shift].Count )
        {
            Debug.LogError ( commodityID + "超過商品數量" );
            return;
        }
        improve [shift] [commodityID] = repeat;
    }
    public bool GetSold ( int day , int commodityID )
    {
        if ( day * 3 >= sold.Length )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return false;
        }
        if ( commodityID >= sold [day * 3].Count )
        {
            Debug.LogError ( commodityID + "超過商品數量" );
            return false;
        }
        bool repeat = false;
        for ( int i = 0; i < 3; i++ )
        {
            if ( sold [day * 3 + i] [commodityID] )
            {
                repeat = true;
                break;
            }
        }
        return repeat;
    }
    public void SetSold ( int shift , int commodityID , bool repeat )
    {
        if ( shift >= sold.Length )
        {
            Debug.LogError ( shift + "超過遊戲總時段數" );
            return;
        }
        if ( commodityID >= sold [shift].Count )
        {
            Debug.LogError ( commodityID + "超過商品數量" );
            return;
        }
        sold [shift] [commodityID] = repeat;
    }
    public bool Getexpansion ( int day , int storeID )
    {
        if ( day * 3 >= sold.Length )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return false;
        }
        if ( storeID >= expansion [day * 3].Count )
        {
            Debug.LogError ( storeID + "超過擴建數量" );
            return false;
        }
        bool repeat = false;
        for ( int i = 0; i < 3; i++ )
        {
            if ( expansion [day * 3 + i] [storeID] )
            {
                repeat = true;
                break;
            }
        }
        return repeat;
    }
    public void SetExpansion ( int shift , int storeID , bool repeat )
    {
        if ( shift >= expansion.Length )
        {
            Debug.LogError ( shift + "超過遊戲總時段數" );
            return;
        }
        if ( storeID >= expansion [shift].Count )
        {
            Debug.LogError ( storeID + "超過擴建數量" );
            return;
        }
        expansion [shift] [storeID] = repeat;
    }
    public bool GetClean ( int day )
    {
        if ( day * 3 >= clean.Length )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return false;
        }
        bool repeat = false;
        for ( int i = 0; i < 3; i++ )
        {
            if ( clean [day * 3 + i] )
            {
                repeat = true;
                break;
            }
        }
        return repeat;
    }
    public void SetClean ( int shift , bool repeat )
    {
        if ( shift >= clean.Length )
        {
            Debug.LogError ( shift + "超過遊戲總時段數" );
            return;
        }
        clean [shift] = repeat;
    }
    public bool GetPolice ( int day )
    {
        if ( day * 3 >= police.Length )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return false;
        }
        bool repeat = false;
        for ( int i = 0; i < 3; i++ )
        {
            if ( police [day * 3 + i] )
            {
                repeat = true;
                break;
            }
        }
        return repeat;
    }
    public void SetPolice ( int shift , bool repeat )
    {
        if ( shift >= police.Length )
        {
            Debug.LogError ( shift + "超過遊戲總時段數" );
            return;
        }
        police [shift] = repeat;
    }
    public bool GetSchedule ( int day , int time )
    {
        if ( day * 3 >= schedule.Length )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return false;
        }
        if ( time >= schedule[day*3].Count )
        {
            Debug.LogError ( time + "超過最大班數" );
            return false;
        }
        bool repeat = false;
        for ( int i = 0; i < 3; i++ )
        {
            if ( schedule [day * 3 + i][time] )
            {
                repeat = true;
                break;
            }
        }
        return repeat;
    }
    public void SetSchedule ( int shift , int time , bool repeat )
    {
        if ( shift >= schedule.Length )
        {
            Debug.LogError ( shift + "超過遊戲總時段數" );
            return;
        }
        if ( time >= schedule[shift].Count )
        {
            Debug.LogError ( time + "超過最大班數" );
            return;
        }
        schedule [shift] [time] = repeat;
    }
    public string [] GetNotice ()
    {
        return notice.ToArray ();
    }
    public void AddNotice ( string msg )
    {
        notice.Add ( msg );
    }
    public void ClearNotice ()
    {
        notice.Clear ();
    }
    public bool GetCanUpdateNotice ( int commodityID )
    {
        if ( commodityID >= CanUpdateNotice.Length )
        {
            Debug.LogError ( commodityID + "大於商品數量" );
            return CanUpdateNotice [0];
        }
        return CanUpdateNotice [commodityID];
    }
    public void SetCanUpdateNotice ( int commodityID , bool notice )
    {
        if ( commodityID >= CanUpdateNotice.Length )
        {
            Debug.LogError ( commodityID + "大於商品數量" );
            return;
        }
        CanUpdateNotice [commodityID] = notice;
    }
}
