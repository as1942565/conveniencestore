﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tools;
using System;
using System.Runtime.Serialization;

//遊戲紀錄 (昨日銷售)
[DataContract ( Namespace = "" )]
[Serializable]
public class GameLog
{                                                                       //
    [DataMember]
    [SerializeField]
    private IntList [] CommodityIncome;                                 //每種商品的收入
    [DataMember]
    [SerializeField]
    private IntList [] CommoditySaleQuantity;                           //每種商品的銷量
    [DataMember]
    [SerializeField]
    private int [] MVPIncome;                                           //銷售冠軍的商品編號
    [DataMember]
    [SerializeField]
    private int [] MVPIncomeMoney;                                      //銷售冠軍的收入
    [DataMember]
    [SerializeField]
    private int [] MVPPopular;                                          //人氣冠軍的商品編號
    [DataMember]
    [SerializeField]
    private int [] MVPPopularQuantity;                                  //人氣冠軍的銷量
    [DataMember]
    [SerializeField]
    private int [] OtherIncome;                                         //其他收入

    [DataMember]
    [SerializeField]
    private string [] Report;                                           //員工報告
    [DataMember]
    [SerializeField]
    private IntList [] CommodityAddQuantity;

    [DataMember]
    [SerializeField]
    private int [] Restock;                                             //進貨支出
    [DataMember]
    [SerializeField]
    private int [] Activity;                                            //員工活動支出
    [DataMember]
    [SerializeField]
    private int [] Expand;                                              //店面擴建支出
    [DataMember]
    [SerializeField]
    private int [] Business;                                            //店長業務支出
    [DataMember]
    [SerializeField]
    private int [] Other;                                               //其他支出

    [DataMember]
    [SerializeField]
    private string [] Information;                                      //店鋪情報
    [DataMember]
    [SerializeField]
    private string [] ActionLog;

    [DataMember]
    [SerializeField]
    private int [] TotalMoney;
    [DataMember]
    [SerializeField]
    private int [] TotalIncome;
    [DataMember]
    [SerializeField]
    private int [] TotalExpenditure;

    private GameLog ()
    {

    }
    public GameLog ( int length , Storage storage , PlayerDataManager pm )
    {
        storage.OnAddQuantityChanged += OnAddQuantityUpdated;

        int totalDay = Calender.MaximumDay;
        int totalShift = Calender.MaximumDay * 3;
        
        CommodityIncome = new IntList [totalShift];
        CommoditySaleQuantity = new IntList [totalShift];
        CommodityAddQuantity = new IntList [totalShift];
        for ( int s = 0; s < totalShift; s++ )
        {
            CommodityIncome [s] = new IntList ();
            CommoditySaleQuantity [s] = new IntList ();
            CommodityAddQuantity [s] = new IntList ();
            for ( int i = 0; i < length; i++ )
            {
                CommodityIncome [s].Add ( 0 );
                CommoditySaleQuantity [s].Add ( 0 );
                CommodityAddQuantity [s].Add ( 0 );
            }
        }

        MVPIncome = new int [totalDay];
        MVPIncomeMoney = new int [totalDay];
        for ( int i = 0; i < MVPIncome.Length; i++ )
        {
            MVPIncome [i] = -1;
        }
        MVPPopular = new int [totalDay];
        MVPPopularQuantity = new int [totalDay];
        for ( int i = 0; i < MVPPopular.Length; i++ )
        {
            MVPPopular [i] = -1;
        }
        OtherIncome = new int [totalShift];

        Report = new string [totalDay];

        Restock = new int [totalShift];
        Activity = new int [totalShift];
        Expand = new int [totalShift];
        Business = new int [totalShift];
        Other = new int [totalShift];

        Information = new string [totalDay];
        ActionLog = new string [totalShift];

        TotalMoney = new int [totalDay];
        TotalMoney [0] = pm.playerData.GetMoney ();
        TotalIncome = new int [totalDay];
        TotalExpenditure = new int [totalDay];
    }
    public int GetAllCommodityIncomByTypeWithDay ( int day , int type , GameDataManager gm )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲最大天數" );
            return 0;
        }
        if ( type >= gm.customData.Data.CommodityType )
        {
            Debug.LogError ( type + "超過商品種類數量" );
            return 0;
        }
        int [] IDs = gm.commodityList.GetCommodityByType ( type );
        int sum = 0;
        foreach ( int id in IDs )
        {
            for ( int i = day * 3; i < day * 3 + 3; i++ )
                sum += CommodityIncome [i] [id];
        }
        return sum;
    }
    public int GetAllCommodityIncomeByType ( int shift , int type , GameDataManager gm )//取得當天該種類的所有收入
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return 0;
        }
        if ( type >= gm.customData.Data.CommodityType )
        {
            Debug.LogError ( type + "超過商品種類數量" );
            return 0;
        }
        int [] IDs = gm.commodityList.GetCommodityByType ( type );
        int sum = 0;
        foreach ( int id in IDs )
        {
            sum += CommodityIncome [shift] [id];
        }
        return sum;
    }
    public int GetCommodityIncome ( int shift , int id )                  //取得當天該編號的收入
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
        }
        if ( id >= CommodityIncome [shift].Count )
        {
            Debug.LogError ( id + "超過商品長度" );
        }
        return CommodityIncome [shift] [id];
    }
    public void AddCommodityIncome ( int shift , int id , int income )  //增加當天該時段該編號的收入
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
        }
        if ( id >= CommodityIncome [shift].Count )
        {
            Debug.LogError ( id + "超過商品長度" );
        }
        CommodityIncome [shift] [id] += income;
    }
    public int GetMVPIncome ( int day )                              //取得當天收入最高的商品名稱
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return -1;
        }
        return MVPIncome [day];
    }
    public int GetMVPIncomeMoney ( int day )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return 0;
        }
        return MVPIncomeMoney [day];
    }
    public void CalculateMVPIncome ( int day , GameDataManager gm )     //計算當天收入最高的商品名稱
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return;
        }
        int id = -1;
        int maxMoney = 0;
        for ( int i = 0; i < gm.commodityList.Data.Length; i++ )
        {
            int income = 0;
            for ( int s = 0; s < 3; s++ )
            {
                income += CommodityIncome [day * 3 + s] [i];
            }
            if ( income > maxMoney )
            {
                maxMoney = income;
                id = i;
            }
        }
        MVPIncome [day] = id;
        MVPIncomeMoney [day] = maxMoney;
    }
    public int GetMVPPopular ( int day )                             //取得當天銷量最高的商品名稱
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return -1;
        }
        return MVPPopular [day];
    }
    public int GetMVPPopularQuantity ( int day )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return 0;
        }
        return MVPPopularQuantity [day];
    }
    public void CalculateMVPPopular ( int day , GameDataManager gm )//計算當天銷量最高的商品名稱
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return;
        }
        int id = -1;
        int maxQuantity = 0;
        for ( int i = 0; i < gm.commodityList.Data.Length; i++ )
        {
            int quantity = 0;
            for ( int s = 0; s < 3; s++ )
            {
                quantity += CommoditySaleQuantity [day * 3 + s] [i];
            }
            if ( quantity > maxQuantity )
            {
                maxQuantity = quantity;
                id = i;
            }
        }
        MVPPopular [day] = id;
        MVPPopularQuantity [day] = maxQuantity;
    }
    public int GetOtherIncome ( int shift )                               //取得當天的其他收入
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return 0;
        }
        return OtherIncome [shift];
    }
    public void AddOtherIncome ( int shift , int num )                  //增加當天該時段的其他收入
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return;
        }
        OtherIncome [shift] += num;
    }
    public string GetReport ( int day )                                 //取得當天的員工回報
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return "";
        }
        if ( Report [day] == "" )
            return Localizer.Translate ( "無" );
        return Report [day];
    }
    public void AddReport ( int day , string message )                  //增加當天的員工回報
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return;
        }
        Report [day] += Localizer.Translate ( message ) + "\n";
    }
    public int GetRestockWithDay ( int day )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲最大天數" );
            return 0;
        }
        int sum = 0;
        for ( int i = day * 3; i < day * 3 + 3; i++ )
        {
            sum += Restock [i];
        }
        return sum;
    }
    public int GetRestock ( int shift )                                   //取得當天的進貨支出
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return 0;
        }
        return Restock[shift];
    }
    public void AddRestock ( int shift , int money )                    //增加當天該時段的進貨支出
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return;
        }
        Restock [shift] += money;
    }
    public int GetActivityWithDay ( int day )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return 0;
        }
        int sum = 0;
        for ( int i = day * 3; i < day * 3 + 3; i++ )
        {
            sum += Activity [i];
        }
        return sum;
    }
    public int GetActivity ( int shift )                                  //取得當天的員工活動支出
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return 0;
        }
        return Activity [shift];
    }
    public void AddActivity ( int shift , int money )                   //增加當天該時段的員工活動支出
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return;
        }
        Activity [shift] += money;
    }
    public int GetExpandWithDay ( int day )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲最大天數" );
            return 0;
        }
        int sum = 0;
        for ( int i = day * 3; i < day * 3 + 3; i++ )
        {
            sum += Expand [i];
        }
        return sum;
    }
    public int GetExpand ( int shift )                                    //取得當天的擴建支出
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return 0;
        }
        return Expand [shift];
    }
    public void AddExpand ( int shift , int money )                     //增加當天該時段的擴建支出
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return;
        }
        Expand [shift] += money;
    }
    public int GetBusinessWithDay ( int day )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲最大時段數" );
            return 0;
        }
        int sum = 0; 
        for ( int i = day * 3; i < day * 3 + 3; i++ )
        {
            sum += Business [i];
        }
        return sum;
    }
    public int GetBusiness ( int shift )                                  //取得當天的店長業務支出
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return 0;
        }
        return Business [shift];
    }
    public void AddBusiness ( int shift , int money )                   //增加當天該時段的店長業務支出
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return;
        }
        Business [shift] += money;
    }
    public int GetOtherWithDay ( int day )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲最大天數" );
            return 0;
        }
        int sum = 0;
        for ( int i = day * 3; i < day * 3 + 3; i++ )
        {
            sum += Other [i];
        }
        return sum;
    }
    public int GetOther ( int shift )                                     //取得當天的其他支出
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return 0;
        }
        return Other [shift];
    }
    public void AddOther ( int shift , int num )                        //增加當天該時段的其他支出
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return;
        }
        Other [shift] += num;
    }
    public string GetInformation ( int day )                            //取得當天的店鋪情報
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return Localizer.Translate ( "無" );
        }
        if ( Information [day] == "" )
            return Localizer.Translate ( "無" );
        return Information [day];
    }
    public void AddInformation ( int day , string message )             //增加當天的店鋪情報
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲天數" );
            return;
        }
        Information [day] += message + "\n";
    }
    public string GetActionLog ( int shift )
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return "";
        }
        return ActionLog [shift];
    }
    public event Action<string> OnShiftMessage;
    public void AddActionLog ( int shift , string log )
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return;
        }
        ActionLog [shift] += log;
        if ( OnShiftMessage != null )
        {
            OnShiftMessage ( ActionLog [shift] );
        }
    }
    public int GetAllCommoditySaleQuantityByType ( int shift , int type , GameDataManager gm )//取得當天該種類的所有銷量
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return 0;
        }
        if ( type >= gm.customData.Data.CommodityType )
        {
            Debug.LogError ( type + "超過商品種類數量" );
            return 0;
        }
        int [] IDs = gm.commodityList.GetCommodityByType ( type );
        int sum = 0;
        foreach ( int id in IDs )
        {
            sum += CommoditySaleQuantity [shift] [id];
        }
        return sum;
    }
    public int GetCommoditySaleQuantity ( int shift , int id )            //取得當天該商品的銷量
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return 0;
        }
        if ( id >= CommoditySaleQuantity [shift].Count )
        {
            Debug.LogError ( id + "超過商品數量" );
            return 0;
        }
        return CommoditySaleQuantity[shift][id];
    }
    public void AddCommoditySaleQuantity ( int shift , int id , int quantity )//增加當天該時段該商品的銷量
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return;
        }
        if ( id >= CommoditySaleQuantity [shift].Count )
        {
            Debug.LogError ( id + "超過商品數量" );
            return;
        }
        CommoditySaleQuantity [shift] [id] += quantity;
    }
    public int GetTotalIncome ( int shift , GameDataManager gm )          //取得當天總收入
    {
        shift--;
        if ( shift < 0 )
            return -1;
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return 0;
        }
        int totalIncome = 0;
        for ( int i = 0; i < CommodityIncome[shift].Count; i++ )
        {
            totalIncome += CommodityIncome [shift] [i];
        }
        totalIncome += OtherIncome [shift];
        return totalIncome;
    }

    void OnAddQuantityUpdated ( int commodityID , int quantity , int shift )
    {
        if ( shift >= Calender.MaximumDay * 3 )
        {
            Debug.LogError ( shift + "超過遊戲最大時段數" );
            return;
        }
        if ( commodityID >= CommodityAddQuantity.Length )
        {
            Debug.LogError ( commodityID + "超過商品數量" );
            return;
        }
        if ( quantity <= 0 )
        {
            Debug.LogError ( quantity + "不該小於等於0" );
            return;
        }
        CommodityAddQuantity [shift] [commodityID] = quantity;
    }
    public int GetSpecialCommodityID ( int day , GameData.CommodityList commodity )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲最大天數" );
            return -1;
        }
        for ( int s = 0; s < 3; s++ )
        {
            for ( int i = 0; i < CommodityAddQuantity[day * 3 + s].Count; i++ )
            {
                if ( commodity.Data[i].type == GameData.Commodity.CommodityType.special && CommodityAddQuantity [day * 3 + s] [i] > 0 )
                {
                    return i;
                }
            }
        }
        return -1;
    }
    public int GetSpecialCommodityQuantity ( int day , GameData.CommodityList commodity )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲最大天數" );
            return 0;
        }
        for ( int s = 0; s < 3; s++ )
        {
            for ( int i = 0; i < CommodityAddQuantity[day * 3 + s].Count; i++ )
            {
                if ( commodity.Data[i].type == GameData.Commodity.CommodityType.special && CommodityAddQuantity [day * 3 + s] [i] > 0 )
                {
                    return CommodityAddQuantity [day * 3 + s] [i];
                }
            }
        }
        return 0;
    }

    public void SetTotalMoney ( int day , int money )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲最大天數" );
            return;
        }
        TotalMoney [day] = money;
    }
    public int GetTotalMoney ( int day )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲最大天數" );
            return 0;
        }
        return TotalMoney [day];
    }
    public void SetTotalIncome ( int day , int income )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲最大天數" );
            return;
        }
        TotalIncome [day] = income;
    }
    public int GetTotalIncome ( int day )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲最大天數" );
            return 0;
        }
        return TotalIncome [day];
    }
    public void SetTotalExpenditure ( int day , int expenditure )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲最大天數" );
            return;
        }
        TotalExpenditure [day] = expenditure;
    }
    public int GetTotalExpenditure ( int day )
    {
        if ( day >= Calender.MaximumDay )
        {
            Debug.LogError ( day + "超過遊戲最大天數" );
            return 0;
        }
        return TotalExpenditure [day];
    }
}
