﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

//店面擴建的特殊加成(活動效果加倍)
public class SpecialAddition
{
    private float cleanAddition;

    private float policeAddition;

    public SpecialAddition ()
    {
        cleanAddition = 1;
        policeAddition = 1;
    }

    public float GetCleanAddition ()
    {
        return cleanAddition;
    }
    public void SetCleanAddition ( float num )
    {
        cleanAddition = num;
    }
    public float GetPoliceAddition ()
    {
        return policeAddition;
    }
    public void SetPoliceAddition ( float num )
    {
        policeAddition = num;
    }
}
