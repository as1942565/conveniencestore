﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

//髒亂事件與竊盜事件
[DataContract(Namespace= "")]
[System.Serializable]
public class PunishAddition
{
    [DataMember]
    [SerializeField]
    private bool lessSaleQuantityBool;
    [DataMember]
    [SerializeField]
    private float saleQuantityAddition;
    [DataMember]
    [SerializeField]
    private bool lessCommodityBool;
    [DataMember]
    [SerializeField]
    private int commodityAddition;

    public PunishAddition ()
    {
        lessSaleQuantityBool = false;
        saleQuantityAddition = 0.5f;
        lessCommodityBool = false;
        commodityAddition = 10;
    }
    
    public bool GetLessSaleQuantityBool ()
    {
        return lessSaleQuantityBool;
    }
    public void SetLessSaleQuantityBool ( bool less )
    {
        lessSaleQuantityBool = less;
    }
    public float GetSaleQuantityAddition ()
    {
        return saleQuantityAddition;
    }

    public bool GetLessCommodityBool ()
    {
        return lessCommodityBool;
    }
    public void SetLessCommodityBool ( bool less )
    {
        lessCommodityBool = less;
    }
    public int GetCommodityAddition ()
    {
        return commodityAddition;
    }
}
