﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

[DataContract(Namespace= "")]
[System.Serializable]
public class OptionData
{
    [DataMember]
    [SerializeField]
    private UI.BuildResolution.Resolution resolution;
    [DataMember]
    [SerializeField]
    private int keyWordSpeed;
    [DataMember]
    [SerializeField]
    private int autoPlaySpeed;
    [DataMember]
    [SerializeField]
    private int talkBoxAlpha;
    [DataMember]
    [SerializeField]
    private bool continueSound;
    [DataMember]
    [SerializeField]
    private UI.BuildSoundLanguage.SoundLanguage soundLanguage;
    [DataMember]
    [SerializeField]
    private Localizer.Language language;

    public OptionData ()
    {
        Init ();
    }

    public void Init ()
    {
        resolution = UI.BuildResolution.Resolution.w1280;
        keyWordSpeed = 50;
        autoPlaySpeed = 50;
        talkBoxAlpha = 100;
        continueSound = true;
        soundLanguage = UI.BuildSoundLanguage.SoundLanguage.Taiwanese;
        language = Localizer.Language.English;
    }

    public void Reset ()
    {
        resolution = UI.BuildResolution.Resolution.w1280;
        keyWordSpeed = 50;
        autoPlaySpeed = 50;
        talkBoxAlpha = 100;
        continueSound = true;
    }

    public int GetResolution ()
    {
        return (int)resolution;
    }
    public void SetResolution ( UI.BuildResolution.Resolution resolution )
    {
        this.resolution = resolution;
    }
    public int GetKeyWordSpeed ()
    {
        return keyWordSpeed;
    }
    public void SetKeyWordSpeed ( int speed )
    {
        if ( speed == 0 )
            speed = 1;
        keyWordSpeed = speed;
    }
    public int GetAutoPlaySpeed ()
    {
        return autoPlaySpeed;
    }
    public void SetAutoPlaySpeed ( int speed )
    {
        if ( speed == 0 )
            speed = 1;
        autoPlaySpeed = speed;
    }
    public int GetTalkBoxAlpha ()
    {
        return talkBoxAlpha;
    }
    public void SetTalkBoxAlpha ( int alpha )
    {
        talkBoxAlpha = alpha;
    }
    public bool GetContinueSound ()
    {
        return continueSound;
    }
    public void SetContinueSound ( bool enable )
    {
        continueSound = enable;
    }
    public int GetSoundLanguage ()
    {
        return (int)soundLanguage;
    }
    public void SetSoundLanguage ( UI.BuildSoundLanguage.SoundLanguage soundLanguage )
    {
        this.soundLanguage = soundLanguage;
    }
    public int GetLanguage ()
    {
        return (int)language;
    }
    public string GetLanguageStr ()
    {
        if ( language == Localizer.Language.Taiwanese )
            return "Taiwanese/";
        if ( language == Localizer.Language.English )
            return "English/";
        if ( language == Localizer.Language.Japanese )
            return "Japan/";
        if ( language == Localizer.Language.Korean )
            return "Korean/";
        if ( language == Localizer.Language.Russian )
            return "Russian/";
        Debug.LogError ( language + "不符合現有語言" );
        return "Taiwanese/";
    }
    public void SetLanguage ( Localizer.Language language )
    {
        this.language = language;
    }
}
