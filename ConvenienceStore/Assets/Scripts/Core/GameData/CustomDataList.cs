﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;
using Tools;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor ( typeof ( CustomDataList ) )]
public class CustomDataListEditor : Editor
{
    [MenuItem("Assets/Create/Data/CustomList" , priority = 100 )]
    public static void CreateMyAsset ()
    {
        CustomDataList asset = ScriptableObject.CreateInstance<CustomDataList>();
        AssetDatabase.CreateAsset ( asset , "Assets/Resources/" + CustomDataList.resourcesPath + ".asset" );
        AssetDatabase.SaveAssets ();
        EditorUtility.FocusProjectWindow ();
        Selection.activeObject = asset;
    }

    public override void OnInspectorGUI ()
    {
        base.OnInspectorGUI ();
        CustomDataList listData = (CustomDataList)target;
        if ( GUILayout.Button ( "Read From Script" ) )
        {
            listData.Data.Init ();
        }
        EditorUtility.SetDirty ( target );
    }
}
#endif


public class CustomDataList : ScriptableObject
{
    public const string resourcesPath = "Data/ScriptableObject/CustomData";
    public CustomData Data;
}


namespace GameData
{
    [System.Serializable]
    public struct CustomData
    {                                                                       //
        [Header ( "商品種類數量" )]
        public int CommodityType;                                           //商品種類數量
        [Header ( "研發費用" )]
        public int [] DevelopMoney;                                         //研發費用
        [Header ( "發送獎金費用" )]
        public int [] RewardMoney;                                          //發送獎金費用
        [Header ( "依據委託形式增加好感度" )]
        public int [] relation;
        [Header ( "員工活動的成功率" )]
        public IntList [] ActivityChance;                                   //簡單活動的成功率
        [Header ( "員工每次升等需要多少經驗值" )]
        public int [] EmployeeLevel;                                        //員工每次升等需要多少經驗值
        [Header ( "員工每次升好感度需要多少好感值" )]
        public int [] EmployeeRelation;                                     //員工每次升好感度需要多少好感值
        [Header ( "熱賣程度加成" )]
        public float [] PopolarAddition;                                    //熱賣程度加成
        [Header ( "觸發特殊客人的機率" )]
        public int [] levelChance;
        
        public void Init ()
        {
            CommodityType = 4;
            DevelopMoney = new int [] { 500 , 1000 , 5000 };
            RewardMoney = new int [] { 500 , 1000 , 2000 };
            relation = new int [] { 4 , 4 , 8 };
            ActivityChance = new IntList [3];
            ActivityChance [0] = new IntList ( new int [] { 30 , 50 , 80 , 100 , 100 } );
            ActivityChance [1] = new IntList ( new int [] { 20 , 40 , 60 , 80 , 100 } );
            ActivityChance [2] = new IntList ( new int [] { 10 , 20 , 40 , 60 , 80 } );
            EmployeeLevel = new int [] { 0 , 5 , 10 , 10 , 15 , 15 , 20 , 20 , 30 , 30 };
            EmployeeRelation = new int [] { 0 , 10 , 20 , 30 , 40 };
            PopolarAddition = new float [] { 0.3f , 0.5f , 1f , 1.5f , 2.5f };
            levelChance = new int [] { 20 , 50 , 80 };
        }
    }
}
