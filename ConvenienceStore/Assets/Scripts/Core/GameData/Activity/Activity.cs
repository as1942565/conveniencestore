﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Tools;

namespace GameData
{
    [Serializable]
    public struct Activity
    {
        public enum ActivityType
        {
            Stamina = 0,
            Charm = 1,
            Luck = 2,
            StaminaAndLuck = 3,
        }

        public enum ActivityDifficult
        {
            Eazy = 0,
            Normal = 1,
            Hard = 2,
        }

        public string name;                                                                 //員工活動中文名稱
        public ActivityType type;                                                           //員工活動種類(體魅運)
        public ActivityDifficult difficult;                                                 //員工活動難度
        public int exp;                                                                     //員工活動經驗值
        public int shift;                                                                   //員工活動班次
        public string explanation;                                                          //員工活動描述
        public bool unlock;                                                                 //員工活動解鎖狀態
        public string conditionString;
        public List<ActivityEventData> eventData;
        public List<UnlockConditionData> unlockCondition;
        public int group;                                                                   //員工活動的群組代號

        public ActivityBaseEvent [] CreateEvents ()                                         //創立員工活動事件
        {
            ActivityBaseEvent [] events = new ActivityBaseEvent [eventData.Count];
            for ( int i = 0; i < eventData.Count; i++ )
            {
                events [i] = CreateEvent ( i );                                             //取得所有員工活動事件並放入陣列中
            }
            return events;                                                                  //回傳員工活動事件陣列
        }
        private ActivityBaseEvent CreateEvent ( int id )                                    //取得員工活動事件
        {
            if ( id >= eventData.Count )
            {
                Debug.LogWarning ( "超過員工活動事件長度" );
                return null;
            }
            Type t = Type.GetType ( "ActivityEvent." + eventData [id].Event.ToString () );  //將得到的字串變成腳本型態
            if ( t == null )
            {
                Debug.LogError ( eventData [id].Event + "不是員工活動事件" );
                return null;
            }
            ActivityBaseEvent e = (ActivityBaseEvent)Activator.CreateInstance ( t );        //強制將腳本型態轉成員工活動事件並且記錄下來
            e.SetParam ( eventData [id].Param );                                            //傳遞員工活動事件所需參數
            return e;
        }
    }
}
