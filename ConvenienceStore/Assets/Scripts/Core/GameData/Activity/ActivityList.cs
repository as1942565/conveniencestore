﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;
using Tools;



namespace GameData
{
    [System.Serializable]
    public class ActivityList : ScriptableObject
    {
        public const string resourcesPath = "Data/ScriptableObject/ActivityData";           //存放ScriptableObject路徑
        public Activity [] Data;                                                            //建立員工活動資料

        public ActivityBaseEvent [] GenerateEvent ( int id )                                //生成該編號活動的所有事件
        {
            if ( id >= Data.Length )
            {
                return null;
            }
            return Data [id].CreateEvents ();
        }
    }
}