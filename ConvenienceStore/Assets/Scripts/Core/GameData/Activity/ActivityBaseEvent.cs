﻿using System;
using UnityEngine;
using System.Runtime.Serialization;
using ActivityEvent;

//員工活動事件
[System.Serializable]
[DataContract]
[KnownType(typeof(AddCleanA))]
[KnownType(typeof(AddPoliceA))]
[KnownType(typeof(AddPopularA))]
[KnownType(typeof(AddSaleQuantityA))]
[KnownType(typeof(GetSpecialA))]
[KnownType(typeof(GroupUnlockA))]
[KnownType(typeof(ReduceIncomeA))]
[KnownType(typeof(UnlockA))]
[KnownType(typeof(CompleteA))]
public abstract class ActivityBaseEvent
{                                                                           //
    [DataMember]
    [SerializeField]
    private string [] Param;                                                //參數

    public void SetParam ( string [] param )
    {
        if(param.Length != ParamLength())
        {
            throw new Exception("Param Length Is Not Correct!");
        }
        Param = param;
    }
    public void DoEvent ( PlayerDataManager pm , GameDataManager gm )
    {
        DoEvent ( pm , gm , Param );                                        //觸發事件
    }
    public abstract void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param );
    public abstract int ParamLength();

    public virtual string[] ParamName()
    {
        string[] s = new string[ParamLength()];
        for(int i = 0; i< ParamLength(); i++)
        {
            s[i] = $"Param{i}";
        }
        return s;
    }
}
