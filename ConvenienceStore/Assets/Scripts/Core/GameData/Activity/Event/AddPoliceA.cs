﻿using DG.Tweening.Plugins;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ActivityEvent
{
    //增加治安度
    [System.Serializable]
    public class AddPoliceA : ActivityBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int police = int.Parse ( param [0] );
            int nowPolice = pm.playerData.GetPolice ();
            pm.playerData.SetPolice ( nowPolice + (int)( police * pm.specialAddition.GetPoliceAddition () ) );
            //( "(員工活動)增加" + police.ToString () + "治安度" );
        }

        public override int ParamLength()
        {
            return 1;
        }

        public override string[] ParamName()
        {
            return new string[] { "治安" };
        }
    }
}