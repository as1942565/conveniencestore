﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ActivityEvent
{
    //減少商品收入
    [System.Serializable]
    public class ReduceIncomeA : ActivityBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm,GameDataManager gm , string [] param )
        {
            float addition = float.Parse ( param [0] );
            GameData.Commodity.CommodityType type = (GameData.Commodity.CommodityType)int.Parse ( param [1] );
            int shift = int.Parse ( param [2] );
            int totalShift = pm.calender.TotalShift;
            pm.activityAddition.AddIncome ( type , addition , totalShift + shift );
            //( "(員工活動)商品收入變成原本的" + addition.ToString () + "倍" );
        }

        public override int ParamLength()
        {
            return 3;
        }

        public override string[] ParamName()
        {
            return new string[] {"打折","種類", "時效" };
        }
    }
}