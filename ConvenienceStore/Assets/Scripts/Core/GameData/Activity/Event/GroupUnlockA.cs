﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ActivityEvent
{
    [System.Serializable]
    public class GroupUnlockA : ActivityBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int group = int.Parse ( param [0] );
            for ( int i = 0; i < gm.activityList.Data.Length; i++ )
            {
                if ( gm.activityList.Data[i].group == group )
                {
                    pm.activityUnlock.SetMode ( i , 0 );
                }
            }
        }
        public override int ParamLength()
        {
            return 1;
        }

        public override string[] ParamName()
        {
            return new string[] {"群組編號" };
        }
    }
}
