﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ActivityEvent
{
    [System.Serializable]
    public class UnlockA : ActivityBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int unlockID = int.Parse ( param [0] );
            if ( !pm.activityUnlock.GetUnlock ( unlockID ) )
            {
                pm.activityUnlock.SetUnlock ( unlockID , true );
                //( "(員工活動)解鎖第" + unlockID.ToString () + "個員工活動" );
            }
        }
        public override int ParamLength()
        {
            return 1;
        }

        public override string[] ParamName()
        {
            return new string[] {"編號"};
        }
    }
}
