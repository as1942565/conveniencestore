﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ActivityEvent
{
    [System.Serializable]
    public class CompleteA : ActivityBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int id = int.Parse ( param [0] );
            pm.activityUnlock.SetMode ( id , ActivityUnlock.state.Available );
        }

        public override int ParamLength()
        {
            return 1;
        }

        public override string[] ParamName()
        {
            return new string[] {"活動編號" };
        }
    }
}