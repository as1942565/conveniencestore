﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ActivityEvent
{
    //增加商品銷售數量
    [System.Serializable]
    public class AddSaleQuantityA : ActivityBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            string [] quantity = param [0].Split ( '~' );
            int start = int.Parse ( quantity [0] );
            int end = int.Parse ( quantity [1] ) + 1;
            int RanQuantity = Random.Range ( start , end );
            GameData.Commodity.CommodityType type = (GameData.Commodity.CommodityType)int.Parse ( param [1] );
            int shift = int.Parse ( param [2] );
            int totalShift = pm.calender.TotalShift;
            pm.activityAddition.AddSaleQuantity ( type , RanQuantity , totalShift + shift );
            //( "(員工活動)" + type + "銷售數量增加" + RanQuantity.ToString () + "個" );
        }

        public override int ParamLength()
        {
            return 3;
        }

        public override string[] ParamName()
        {
            return new string[] {"銷量(0~19)","總類","時效" };
        }
    }
}