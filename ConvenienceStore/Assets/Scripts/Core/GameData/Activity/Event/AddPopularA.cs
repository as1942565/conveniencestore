﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ActivityEvent
{
    //增加人氣值
    [System.Serializable]
    public class AddPopularA : ActivityBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int popular = int.Parse ( param [0] );
            int nowPopular = pm.playerData.GetPopular ();
            pm.playerData.SetPopular ( nowPopular + popular );
            //( "(員工活動)增加" + popular.ToString () + "人氣值" );
        }

        public override int ParamLength()
        {
            return 1;
        }

        public override string[] ParamName()
        {
            return new string[] { "人氣值" };
        }
    }
}