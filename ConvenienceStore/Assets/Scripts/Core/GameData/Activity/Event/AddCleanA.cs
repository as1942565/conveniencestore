﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ActivityEvent
{
    //增加清潔度
    [System.Serializable]
    public class AddCleanA : ActivityBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int clean = int.Parse ( param [0] );
            int nowClean = pm.playerData.GetClean ();
            pm.playerData.SetClean ( nowClean + (int)(clean * pm.specialAddition.GetCleanAddition () ) );
            //( "(員工活動)增加" + clean.ToString () + "清潔度" );
        }

        public override int ParamLength()
        {
            return 1;
        }

        public override string[] ParamName()
        {
            return new string[] { "清潔度" };
        }
    }
}