﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;



namespace GameData
{
    [Serializable]
    public struct UnlockConditionData
    {
        public enum ConditionType
        {
            AnyoneRelationA,
            ResearchNumA,
            StoreUnlockA,
            AllMainRelationB,
            CommodityStarB,
            EmployeeQuantityB,
            MainRelationB,
            ResearchNumB,
            StoreUnlockB,
            EmployeeScheduleB,
            BusinessUnlockSE,
        }
        public ConditionType Condition;
        public string[] Param;
        public BaseUnlockCondition ToObject()
        {
            Type t = Type.GetType($"Condition.{Condition},Assembly-CSharp");// + eventType.ToString());   //將得到的字串變成腳本型態
            if (t == null)
            {
                Debug.LogError($"{Condition} 不是解鎖條件");
                return null;
            }
            BaseUnlockCondition e = (BaseUnlockCondition)Activator.CreateInstance(t);//強制將腳本型態轉成員工活動事件並且記錄下來
            return e;
        }
    }
    [Serializable]
    public struct ActivityEventData
    {
        public enum EventType
        {
            AddCleanA,
            AddPoliceA,
            AddPopularA,
            AddSaleQuantityA,
            GetSpecialA,
            GroupUnlockA,
            ReduceIncomeA,
            UnlockA,
            CompleteA,
        }
        public EventType Event;
        public string[] Param;
        public ActivityBaseEvent ToObject()
        {
            Type t = Type.GetType($"ActivityEvent.{Event},Assembly-CSharp");// + eventType.ToString());   //將得到的字串變成腳本型態
            if (t == null)
            {
                Debug.LogError($"{Event} 不是事件");
                return null;
            }
            ActivityBaseEvent e = (ActivityBaseEvent)Activator.CreateInstance(t);//強制將腳本型態轉成員工活動事件並且記錄下來
            return e;
        }
    }
    [Serializable]
    public struct BusinessEventData
    {
        public enum EventType
        {
            AddIncomeB,
            AddMaxPurchaseB,
            AddSaleQuantityB,
            CompleteB,
            DailyStoryB,
            EndingB,
            GetSpecialB,
            HideB,
            HStoryB,
            NiceStoryB,
            RecruitWorkerB,
            UnlockB,
            WatchB,
            PayMoneyB,
            CustomStoryB,
            ShowEndingB,
            OtherStoryB,
            AchievementB,
        }
        public EventType Event;
        public string[] Param;
        public BusinessBaseEvent ToObject()
        {
            Type t = Type.GetType($"BusinessEvent.{Event},Assembly-CSharp");// + eventType.ToString());   //將得到的字串變成腳本型態
            if (t == null)
            {
                Debug.LogError($"{Event} 不是事件");
                return null;
            }
            BusinessBaseEvent e = (BusinessBaseEvent)Activator.CreateInstance(t);//強制將腳本型態轉成員工活動事件並且記錄下來
            return e;
        }
    }

    [Serializable]
    public struct StoreExpansionEventData
    {
        public enum EventType
        {
            ActivityCleanSE,
            ActivityPoliceSE,
            AddActionSE,
            AddCleanSE,
            AddMaxCabinetSE,
            AddMaxSchedulingSE,
            AddPoliceSE,
            AddPopularSE,
            AddRevenueSE,
            AddSpecialCabinetSE,
            AddStorageSE,
            UnlockSE,
        }
        public EventType Event;
        public string[] Param;
        public StoreExpansionBaseEvent ToObject()
        {
            Type t = Type.GetType($"StoreExpansionEvent.{Event},Assembly-CSharp");// + eventType.ToString());   //將得到的字串變成腳本型態
            if (t == null)
            {
                Debug.LogError($"{Event} 不是事件");
                return null;
            }
            StoreExpansionBaseEvent e = (StoreExpansionBaseEvent)Activator.CreateInstance(t);//強制將腳本型態轉成員工活動事件並且記錄下來
            return e;
        }
    }

}