﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor ( typeof ( MainPeopleList ) )]
public class MainPeopleListEditor : Editor
{
    [MenuItem ( "Assets/Create/Data/MainPeopleList" , priority = 9 )]
    public static void CreateMyAsset ()
    {
        MainPeopleList asset = ScriptableObject.CreateInstance<MainPeopleList> ();
        AssetDatabase.CreateAsset ( asset , "Assets/Resources/" + MainPeopleList.resourcesPath + "3.asset" );
        AssetDatabase.SaveAssets ();
        EditorUtility.FocusProjectWindow ();
        Selection.activeObject = asset;
    }
    public override void OnInspectorGUI ()
    {
        base.OnInspectorGUI ();
        MainPeopleList mainPeopleList = (MainPeopleList)target;

        if ( GUILayout.Button ( "ReadMain1 From Script" ) )
        {
            mainPeopleList.Data.MainInit1 ();
        }
        if ( GUILayout.Button ( "ReadMain2 From Script" ) )
        {
            mainPeopleList.Data.MainInit2 ();
        }
        if ( GUILayout.Button ( "ReadMain3 From Script" ) )
        {
            mainPeopleList.Data.MainInit3 ();
        }
        EditorUtility.SetDirty ( target );
    }
}
#endif

namespace GameData
{
    public class MainPeopleList : ScriptableObject
    {
        public const string resourcesPath = "Data/ScriptableObject/MainPeopleData";
        public MainPeople Data;
    }
}