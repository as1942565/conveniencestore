﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor ( typeof ( PeopleList ) )]
public class PeopleListEditor : Editor
{
    [MenuItem ( "Assets/Create/Data/PeopleList" , priority = 9 )]
    public static void CreateMyAsset ()
    {
        PeopleList asset = ScriptableObject.CreateInstance<PeopleList> ();
        AssetDatabase.CreateAsset ( asset , "Assets/Resources/" + PeopleList.resourcesPath + "3.asset" );
        AssetDatabase.SaveAssets ();
        EditorUtility.FocusProjectWindow ();
        Selection.activeObject = asset;
    }
    public override void OnInspectorGUI ()
    {
        base.OnInspectorGUI ();
        PeopleList peopleList = (PeopleList)target;
        if ( GUILayout.Button ( "Read1 From Script" ) )
        {
            peopleList.Data.Init1 ();
        }
        if ( GUILayout.Button ( "Read2 From Script" ) )
        {
            peopleList.Data.Init2 ();
        }
        if ( GUILayout.Button ( "Read3 From Script" ) )
        {
            peopleList.Data.Init3 ();
        }
        
        EditorUtility.SetDirty ( target );
    }
}
#endif

namespace GameData
{
    public class PeopleList : ScriptableObject
    {
        public const string resourcesPath = "Data/ScriptableObject/PeopleData";
        public People Data;
    }
}