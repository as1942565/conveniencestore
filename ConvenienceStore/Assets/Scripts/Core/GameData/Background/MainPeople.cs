﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace GameData
{
    [Serializable]
    public class MainPeople
    {
        public PeopleManager.Path [] path;

        public void MainInit1 ()
        {
            bool [] main = new bool [] { true , true , true };

            Vector2 [] startPos = new Vector2 [] { new Vector2 ( 5f , 2f ) , new Vector2 ( -7.5f , -0.4f ) ,
                                                   new Vector2 ( 7.5f , 0 ) };

            Vector2 [] endPos = new Vector2 [] { new Vector2 ( 5f , 2f ) , new Vector2 ( -7.5f , -0.4f ) ,
                                                 new Vector2 ( 7.5f , 0 ) };

            bool []right = new bool [] { false , true , false };

            string [] animName = new string [] { "Salute" , "Work1" , "Work1" };

            float [] order = new float [] { 2 , 4 , 4 };

            path = new PeopleManager.Path [main.Length];
            for ( int i = 0; i < path.Length; i++ )
            {
                path [i].main = main [i];
                path [i].startPos = startPos [i];
                path [i].endPos = endPos [i];
                path [i].right = right [i];
                path [i].animName = animName [i];
                path [i].order = order [i];
            }
        }
        public void MainInit2 ()
        {
            bool [] main = new bool [] { true , true , true , true };

            Vector2 [] startPos = new Vector2 [] { new Vector2 ( 5f , 2f ) , new Vector2 ( -0.2f , 3.8f ) ,
                                                   new Vector2 ( 7.5f , 0 ) ,new Vector2 ( 2.89f , -2.24f ) };

            Vector2 [] endPos = new Vector2 [] { new Vector2 ( 5f , 2f ) , new Vector2 ( -0.2f , 3.8f ) ,
                                                 new Vector2 ( 7.5f , 0 ) ,new Vector2 ( 2.89f , -2.24f ) };

            bool []right = new bool [] { false , false , false , false };

            string [] animName = new string [] { "Salute" , "Work1" , "Work1" , "Work2" };

            float [] order = new float [] { 2 , 2 , 4 , 15 };

            path = new PeopleManager.Path [main.Length];
            for ( int i = 0; i < path.Length; i++ )
            {
                path [i].main = main [i];
                path [i].startPos = startPos [i];
                path [i].endPos = endPos [i];
                path [i].right = right [i];
                path [i].animName = animName [i];
                path [i].order = order [i];
            }
        }
        public void MainInit3 ()
        {
            bool [] main = new bool [] { true , true , true , true };

            Vector2 [] startPos = new Vector2 [] { new Vector2 ( 5f , 2f ) , new Vector2 ( -0.2f , 3.8f ) ,
                                                   new Vector2 ( -8.5f , -2 ) ,new Vector2 ( 2.89f , -2.24f ) };

            Vector2 [] endPos = new Vector2 [] { new Vector2 ( 5f , 2f ) , new Vector2 ( -0.2f , 3.8f ) ,
                                                 new Vector2 ( -8.5f , -2 ) ,new Vector2 ( 2.89f , -2.24f ) };

            bool []right = new bool [] { false , false , false , false };

            string [] animName = new string [] { "Salute" , "Work1" , "Work1" , "Work2" };

            float [] order = new float [] { 2 , 2 , 13 , 15 };

            path = new PeopleManager.Path [main.Length];
            for ( int i = 0; i < path.Length; i++ )
            {
                path [i].main = main [i];
                path [i].startPos = startPos [i];
                path [i].endPos = endPos [i];
                path [i].right = right [i];
                path [i].animName = animName [i];
                path [i].order = order [i];
            }
        }
    }
}