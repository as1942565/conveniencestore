﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace GameData
{
    [Serializable]
    public struct People
    {
        public PeopleManager.Path [] path;

        public void Init1 ()
        {
            bool [] main = new bool [] { false , false , false , false , false , false , false , false , false, false };

            Vector2 [] startPos = new Vector2 [] { new Vector2 (  4.50f ,  0.00f ) , new Vector2 ( -6.67f , -0.61f ) ,
                                        new Vector2 ( -5.40f ,  0.50f ) , new Vector2 ( -1.83f ,  2.16f ) ,
                                        new Vector2 ( -2.25f , -0.41f ) , new Vector2 ( -0.61f ,  0.74f ) ,
                                        new Vector2 ( -0.05f , -1.83f ) , new Vector2 (  2.08f , -0.41f ) ,
                                        new Vector2 ( -4.25f , -0.54f ) , new Vector2 ( -1.87f , -1.74f ) };

            Vector2 [] endPos = new Vector2 [] { new Vector2 ( -0.21f ,  2.72f ) , new Vector2 ( -2.68f , -2.91f ) ,
                                      new Vector2 ( -5.40f ,  0.50f ) , new Vector2 ( -1.83f , 2.16f ) , 
                                      new Vector2 ( -2.25f , -0.41f ) , new Vector2 ( -0.61f ,  0.74f ) ,
                                      new Vector2 ( -0.05f , -1.83f ) , new Vector2 (  2.08f , -0.41f ) ,
                                      new Vector2 ( -4.25f , -0.54f ) , new Vector2 ( -1.87f , -1.74f ) };

            bool []right = new bool [] { false , true , false , false , false , false , false , false , true , true };

            string [] animName = new string [] { "Walk_B" , "Walk_F" , "Idle_B" , "Idle_B" , "Idle_B" , "Idle_B" , "Idle_B" , "Idle_B" , "Idle_B" , "Idle_B" , "Idle_F" };

            float [] order = new float [] { 4.1f , 8.1f , 4.1f , 4.1f , 6.1f , 6.1f , 8.1f , 8.1f , 8.1f , 8.1f };

            path = new PeopleManager.Path [main.Length];
            for ( int i = 0; i < path.Length; i++ )
            {
                path [i].main = main [i];
                path [i].startPos = startPos [i];
                path [i].endPos = endPos [i];
                path [i].right = right [i];
                path [i].animName = animName [i];
                path [i].order = order [i];
            }
        }
        public void Init2 ()
        {
            bool [] main = new bool [] { false , false , false , false , false , false , false , false , false , false , false , false , false , false };

            Vector2 [] startPos = new Vector2 [] { new Vector2 ( -6.38f , -2.77f ) , new Vector2 ( -4.96f ,  1.19f ) ,
                                        new Vector2 ( -1.78f ,  2.55f ) , new Vector2 ( -2.68f ,  0.40f ) ,
                                        new Vector2 ( -0.38f ,  0.94f ) , new Vector2 ( 0 , 0 ) ,
                                        new Vector2 (  0.20f , -1.84f ) , new Vector2 ( -4.62f , -0.19f ) ,
                                        new Vector2 ( -3.02f , -1.02f ) , new Vector2 (  3.97f , -0.73f ) ,
                                        new Vector2 (  4.72f ,  0.77f ) , new Vector2 ( -3.98f , -0.66f ) ,
                                        new Vector2 ( -0.44f ,  3.03f ) , new Vector2 (  3.16f , -0.38f ) };

            Vector2 [] endPos = new Vector2 [] { new Vector2 ( -6.38f , -2.77f ) , new Vector2 ( -4.96f ,  1.19f ) ,
                                      new Vector2 ( -1.78f ,  2.55f ) , new Vector2 ( -2.68f ,  0.40f ) ,
                                      new Vector2 ( -0.38f ,  0.94f ) , new Vector2 (   0    ,   0    ) ,
                                      new Vector2 (  0.20f , -1.84f ) , new Vector2 ( -4.62f , -0.19f ) ,
                                      new Vector2 ( -3.02f , -1.02f ) , new Vector2 (  3.97f , -0.73f ) ,
                                      new Vector2 (  0.30f ,  2.97f ) , new Vector2 (  0.95f ,  1.91f ) ,
                                      new Vector2 ( -5.56f ,  0.36f ) , new Vector2 ( -1.04f , -2.57f ) };

            bool [] right = new bool [] { true , false , false , false , true , false , false , true , true , true , false , true , false , false };

            string [] animName = new string [] { "Idle_B" , "Idle_B" , "Idle_B" , "Idle_B" , "Idle_F" , "Idle_B" , "Idle_B" , "Idle_B" , "Idle_B" , "Idle_F" , "Walk_B" , "Walk_B" , "Walk_F" , "Walk_F" };

            float [] order = new float [] { 12.1f , 4.1f , 4.1f , 6.1f , 6.1f , 8.1f , 10.1f , 8.1f  , 8.1f , 10.1f , 6.5f , 6.1f , 4.1f , 10.1f };

            path = new PeopleManager.Path [main.Length];
            for ( int i = 0; i < path.Length; i++ )
            {
                path [i].main = main [i];
                path [i].startPos = startPos [i];
                path [i].endPos = endPos [i];
                path [i].right = right [i];
                path [i].animName = animName [i];
                path [i].order = order [i];
            }
        }
        public void Init3 ()
        {
            bool [] main = new bool [] { false , false , false , false , false , false , false , false , false , false , false };

            Vector2 [] startPos = new Vector2 [] { new Vector2 ( -7.49f , -3.44f ) , new Vector2 ( -3.74f ,  1.86f ) ,
                                        new Vector2 ( -2.10f ,  1.78f ) , new Vector2 ( -1.34f ,  0.89f ) ,
                                        new Vector2 ( -2.00f , -0.16f ) , new Vector2 (  0.98f ,  0.56f ) ,
                                        new Vector2 ( -0.72f , -1.01f ) , new Vector2 (  3.55f , -0.12f ) ,
                                        new Vector2 ( -4.21f , -3.51f ) , new Vector2 (  0.98f ,  2.01f ) ,
                                        new Vector2 ( -1.62f , -1.23f ) };

            Vector2 [] endPos = new Vector2 [] { new Vector2 ( -7.49f , -3.44f ) , new Vector2 ( -3.74f ,  1.86f ) ,
                                      new Vector2 ( -2.10f ,  1.78f ) , new Vector2 ( -1.34f ,  0.89f ) ,
                                      new Vector2 ( -2.00f , -0.16f ) , new Vector2 (  0.98f ,  0.56f ) ,
                                      new Vector2 ( -0.72f , -1.01f ) , new Vector2 (  3.55f , -0.12f ) ,
                                      new Vector2 ( -8.16f , -1.46f ) , new Vector2 ( -3.41f , -0.27f ) ,
                                      new Vector2 (  2.73f ,  1.03f ) };

            bool [] right = new bool [] { true , false , true , false , true , false , true , false , false , false , true };

            string [] animName = new string [] { "Idle_B" , "Idle_B" , "Idle_F" , "Idle_B" , "Idle_F" , "Idle_B" , "Idle_F" , "Idle_B" , "Walk_B" , "Walk_F" , "Walk_B" };

            float [] order = new float [] { 14.1f , 4.1f , 4.1f , 6.1f , 6.1f , 8.1f , 8.1f , 10.1f , 12.1f , 6.1f , 8.1f };

            path = new PeopleManager.Path [main.Length];
            for ( int i = 0; i < path.Length; i++ )
            {
                path [i].main = main [i];
                path [i].startPos = startPos [i];
                path [i].endPos = endPos [i];
                path [i].right = right [i];
                path [i].animName = animName [i];
                path [i].order = order [i];
            }
        } 
    }
}