﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace GameData
{
    [Serializable]
    public struct SpecialPeopleData
    {
        public SpecialPeopleStruct [] peopleData;

        public void Init ()
        {
            string [] name_C = new string [] { "Anna" , "Belos" , "Demon" , "Doroelle" , "Ellin" , "Kassandra" , "Lica" , "Lulu" , "MeiHong" , "Rezia" };
            string [] name_E = new string [] { "Anna" , "Belos" , "Demon" , "Doroelle" , "Ellin" , "Kassandra" , "Lica" , "Lulu" , "MeiHong" , "Rezia" };

            peopleData = new SpecialPeopleStruct [name_C.Length];
            for ( int i = 0; i < peopleData.Length; i++ )
            {
                peopleData [i].name_C = name_C [i];
                peopleData [i].name_E = name_E [i];
            }
        }
    }
}

[System.Serializable]
public struct SpecialPeopleStruct
{
    public string name_C;
    public string name_E;
}
