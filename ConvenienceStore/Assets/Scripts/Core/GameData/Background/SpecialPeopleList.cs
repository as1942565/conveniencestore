﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor ( typeof ( SpecialPeopleList ) )]
public class SpecialPeopleListEditor : Editor
{
    [MenuItem ( "Assets/Create/Data/SpecialPeopleList" , priority = 11 )]
    public static void CreateMyAsset ()
    {
        SpecialPeopleList asset = ScriptableObject.CreateInstance<SpecialPeopleList> ();
        AssetDatabase.CreateAsset ( asset , "Assets/Resources/" + SpecialPeopleList.resourcesPath + ".asset" );
        AssetDatabase.SaveAssets ();
        EditorUtility.FocusProjectWindow ();
        Selection.activeObject = asset;
    }

    public override void OnInspectorGUI ()
    {
        base.OnInspectorGUI ();
        SpecialPeopleList specialPeopleList = (SpecialPeopleList)target;
        if ( GUILayout.Button ( "Read From Script" ) )
        {
            specialPeopleList.Data.Init ();
        }
        
        EditorUtility.SetDirty ( target );
    }
}
#endif

namespace GameData
{
    public class SpecialPeopleList : ScriptableObject
    {
        public const string resourcesPath = "Data/ScriptableObject/SpecialPeopleData";
        public SpecialPeopleData Data;
    }
}