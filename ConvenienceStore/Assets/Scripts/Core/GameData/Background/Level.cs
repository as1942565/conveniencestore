﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace GameData
{
    [Serializable]
    public struct Level
    {
        public Vector2 [] pos;
        public int [] order;
    }
}