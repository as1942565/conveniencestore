﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor ( typeof ( LevelList ) )]
public class LevelListEditor : Editor
{
    [MenuItem ( "Assets/Create/Data/LevelList" , priority = 8 )]
    public static void CreateMyAsset ()
    {
        LevelList asset = ScriptableObject.CreateInstance<LevelList> ();
        AssetDatabase.CreateAsset ( asset , "Assets/Resources/" + LevelList.resourcesPath + ".asset" );
        AssetDatabase.SaveAssets ();
        EditorUtility.FocusProjectWindow ();
        Selection.activeObject = asset;
    }
    public override void OnInspectorGUI ()
    {
        base.OnInspectorGUI ();
        LevelList dataList = (LevelList)target;
    }
}
#endif

namespace GameData
{
    public class LevelList : ScriptableObject
    {
        public const string resourcesPath = "Data/ScriptableObject/LevelData";
        public Level [] Data;
    }
}