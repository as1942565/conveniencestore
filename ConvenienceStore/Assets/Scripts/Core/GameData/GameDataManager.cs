﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;

//遊戲資料管理器
[System.Serializable]
public class GameDataManager
{
    public CommodityList commodityList;                                     //商品資料列表
                                                                            
    public StoreExpansionList storeExpansionList;                           //店面擴建列表

    public ActivityList activityList;                                       //員工活動列表

    public BusinessList businessList;                                       //店長業務列表

    public WorkerList workerList;                                           //工人資料列表

    public CustomDataList customData;                                       //其他資料

    public SchedulingCabinetList schedulingCabinetList;                     //員工排班欄位列表

    public CabinetList cabinetList;                                         //商品擺放欄位列表

    public LevelList levelList;

    public StoreEventList storeEventList;

    public PeopleList [] peopleLists;

    public MainPeopleList [] mainPeopleLists;

    public TeachDataList teachDataList;

    public SpecialPeopleList specialPeopleList;

    public void Init ()
    {
        TextAsset s = Resources.Load<TextAsset> ( "Data/stringtable" );
        Localizer.Init (s.text);

        commodityList = Resources.Load<CommodityList> ( CommodityList.resourcesPath );
        storeExpansionList = Resources.Load<StoreExpansionList> ( StoreExpansionList.resourcesPath );
        activityList = Resources.Load<ActivityList> ( ActivityList.resourcesPath );
        /*
        string test = localizer.Translate ( Localizer.Language.English , activityList.Data [0].name );
        Debug.Log ( test );
        test = localizer.Translate ( Localizer.Language.Taiwanese , activityList.Data [0].name );
        Debug.Log ( test );
        */
        businessList = Resources.Load<BusinessList> ( BusinessList.resourcesPath );
        workerList = Resources.Load<WorkerList> ( WorkerList.resourcesPath );
        customData = Resources.Load<CustomDataList> ( CustomDataList.resourcesPath );
        schedulingCabinetList = Resources.Load<SchedulingCabinetList> ( SchedulingCabinetList.resourcesPath );
        cabinetList = Resources.Load<CabinetList> ( CabinetList.resourcesPath );
        levelList = Resources.Load<LevelList> ( LevelList.resourcesPath );
        storeEventList = Resources.Load<StoreEventList> ( StoreEventList.resourcesPath );
        peopleLists = new PeopleList [3];
        for ( int i = 0; i < peopleLists.Length; i++ )
        {
            peopleLists [i] = Resources.Load<PeopleList> ( PeopleList.resourcesPath + ( i + 1 ).ToString () );
        }
        mainPeopleLists = new MainPeopleList [3];
        for ( int i = 0; i < mainPeopleLists.Length; i++ )
        {
            mainPeopleLists [i] = Resources.Load<MainPeopleList> ( MainPeopleList.resourcesPath + ( i + 1 ).ToString () );
        }
        teachDataList = Resources.Load<TeachDataList> ( TeachDataList.resourcesPath );

        specialPeopleList = Resources.Load<SpecialPeopleList> ( SpecialPeopleList.resourcesPath );
    }
}
