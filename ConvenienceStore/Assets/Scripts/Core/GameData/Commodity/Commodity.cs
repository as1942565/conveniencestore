﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tools;
using System;

namespace GameData
{
    //序列化
    [Serializable]
    public struct Commodity
    {                                                                       //
        public string name;                                                 //商品名稱
        public enum CommodityType
        {
            food = 0,
            drink = 1,
            grocery = 2,
            special = 3,
            nothing = 4,
        }
        public enum SpecialType
        {
            nothing = 0,
            food = 1,
            drink = 2,
            grocery = 3,
        }
        public enum PopularAddition
        {
            p30 = 0,
            p50 = 1,
            p100 = 2,
            p150 = 3,
            p250 = 4,
        }
        /*public const int CommodityTypeCount = 4;
        private void Test ()
        {
            var myEnumMemberCount = Enum.GetNames(typeof(CommodityType)).Length;
        }*/
        [CustomLabel("種類")]
        public CommodityType type;                                          //商品種類
        public SpecialType specialType;
        public int rank;                                                    //商品階級
        public int cost;                                                    //商品成本
        public int star;                                                    //商品星級
        public int [] price;                                                 //商品價格
        public PopularAddition[] salesRate;                                                 //商品時段熱賣程度
        public string explanation;                                          //商品介紹文
        public float baseSaleValue;                                         //商品基本銷售數值
        public int quantity;                                                //商品研發初始數量
        public bool unlock;                                                 //商品是否被研發
        public int [] ImproveCondition;                                        //改良前置(販售數量門檻)
        [PreviewSprite()]
        [CustomLabel("圖片")]
        public Sprite icon;

        [Serializable]
        public struct UpgradeChance
        {
            public int UpgradeRate;
            public int RemainRate;
            public int DowngradeRate;
        }

        public UpgradeChance[] MorningChance;                                    //早上的成功率(1星)
        public UpgradeChance[] EveningChance;                                    //晚上的成功率(1星)
        public UpgradeChance[] NightChance;                                      //大夜的成功率(1星)
    }
}