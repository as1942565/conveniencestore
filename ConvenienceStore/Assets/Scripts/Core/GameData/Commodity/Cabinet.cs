﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameData
{                                                                       //
    [System.Serializable]
    public struct Cabinet
    {
        public int unlockNum;                                           //每種商品櫃的解鎖數量
        public float addition;                                          //商品櫃販售加成
    }
}
