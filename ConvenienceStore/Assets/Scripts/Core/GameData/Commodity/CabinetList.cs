﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor( typeof ( CabinetList ) )]
public class CabinetListEditor : Editor
{
    [MenuItem( "Assets/Create/Data/CabinetList" , priority = 6 )]
    public static void CreateMyAsset ()
    {
        CabinetList asset = ScriptableObject.CreateInstance<CabinetList> ();
        AssetDatabase.CreateAsset ( asset , "Assets/Resources/" + CabinetList.resourcesPath + ".asset" );
        AssetDatabase.SaveAssets ();
        EditorUtility.FocusProjectWindow ();
        Selection.activeObject = asset;
    }

    public override void OnInspectorGUI ()
    {
        base.OnInspectorGUI ();
        CabinetList dataList = (CabinetList)target;
    }
}
#endif

namespace GameData
{
    public class CabinetList : ScriptableObject
    {
        public const string resourcesPath = "Data/ScriptableObject/CabinetData";
        public Cabinet [] Data;
    }
}
