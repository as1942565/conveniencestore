﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameData
{
    public class CommodityList : ScriptableObject                           //從CSV讀取的商品資料
    {                                                                       //
        public const string resourcesPath = "Data/ScriptableObject/CommodityData";//存放ScriptableObject的路徑
        public Commodity [] Data;                                           //建立基礎商品資料

        public int [] GetCommodityByType ( int type )                       //取得該種類的所有商品編號
        {
            List<int> commodity = new List<int> ();
            for ( int i = 0; i < Data.Length; i++ )
            {
                if ( (int)Data [i].type == type )
                    commodity.Add ( i );
            }
            return commodity.ToArray ();
        }
    }
}