﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;
using Tools;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor ( typeof ( TeachDataList ) )]
public class TeachDataListEditor : Editor
{
    [MenuItem("Assets/Create/Data/TeachList" , priority = 200 )]
    public static void CreateMyAsset ()
    {
        TeachDataList asset = ScriptableObject.CreateInstance<TeachDataList>();
        AssetDatabase.CreateAsset ( asset , "Assets/Resources/" + TeachDataList.resourcesPath + ".asset" );
        AssetDatabase.SaveAssets ();
        EditorUtility.FocusProjectWindow ();
        Selection.activeObject = asset;
    }

    public override void OnInspectorGUI ()
    {
        base.OnInspectorGUI ();
        TeachDataList listData = (TeachDataList)target;
        if ( GUILayout.Button ( "Read From Folder" ) )
        {
            listData.Init ();
        }
        if ( GUILayout.Button ( "Skip" ) )
        {
            listData.Skip ();
        }
        EditorUtility.SetDirty ( target );
    }
}
#endif

public class TeachDataList : ScriptableObject
{
    public const string resourcesPath = "Data/ScriptableObject/TeachData";
    public TeachData [] Data;
    public int max;
    public void Skip ()
    {
        for ( int i = 0; i < Data.Length; i++ )
        {
            Data [i].skip = false;
        }
        for ( int i = 0; i < max + 1; i++ )
        {
            Data [i].skip = true;
        }
    }


    
    /*private string [] ChineseName = new string [] { "0.序章" , "1.開場故事" , "2.前導教學" , "3.研發飲品" , "4.飲品上架" , "5.研發雜貨" ,
                                         "6.雜貨上架" , "7.食品改良" , "8.商品進貨" , "9.設備採購" , "10.員工活動" ,
                                         "11.休息" , "12.昨日銷售" , "13.員工排班" , "14.商品進貨-2" , "15.工讀生招募" ,
                                         "16.結語" };*/
    public void Init ()
    {
        int [] quantity = new int [] { 9 , 10 , 9 , 10 };
        List<string> dayStr = new List<string> ();
        int day = 1;
        for ( int i = 0; i < quantity.Length; i++ )
        {
            for ( int d = 0; d < quantity[i]; d++ )
            {
                dayStr.Add ( "Day" + day + "/" );
            }
            day++;
        }

        string language = GameLoopManager.instance.om.optionData.GetLanguageStr ();
        TextAsset [] drama = Resources.LoadAll<TextAsset> ( $"Data/Story/{language}Teach" );
        List<string> teachName = new List<string> ();
        for ( int i = 0; i < drama.Length; i++ )
        {
            teachName.Add ( drama [i].name );
        }

        Data = new TeachData [drama.Length];
        for ( int i = 0; i < Data.Length; i++ )
        {
            Data [i].Name = teachName [i];
            Data [i].skip = false;
            Data [i].teachName = dayStr [i] + teachName [i];
        }
    }
}

