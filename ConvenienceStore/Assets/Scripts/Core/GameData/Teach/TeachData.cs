﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace GameData
{
    [System.Serializable]
    public struct TeachData
    {
        public string Name;
        public int day;
        public bool skip;
        public string teachName;
    }
}