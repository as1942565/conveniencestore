﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;

namespace GameData
{
    public class WorkerList : ScriptableObject
    {
        public const string resourcesPath = "Data/ScriptableObject/EmployeeData";//存放ScriptableObject的路徑
        [SerializeField]
        public Worker [] Data;                                          //員工資料
    }
}