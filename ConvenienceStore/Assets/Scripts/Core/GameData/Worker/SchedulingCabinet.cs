﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameData
{
    [System.Serializable]
    public struct SchedulingCabinet
    {                                                                       //
        public int unlockNum;                                               //每種排班欄位的解鎖數量
    }
}
