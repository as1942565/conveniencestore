﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor ( typeof ( SchedulingCabinetList ) )]
public class SchedulingCabinetListEditor : Editor
{
    [MenuItem ( "Assets/Create/Data/SchedulingCabinetList" , priority = 7 )]
    private static void CreateMyAsset ()
    {
        SchedulingCabinetList asset = ScriptableObject.CreateInstance<SchedulingCabinetList> ();
        AssetDatabase.CreateAsset ( asset , "Assets/Resources/" + SchedulingCabinetList.resourcesPath + ".asset" );
        AssetDatabase.SaveAssets ();
        EditorUtility.FocusProjectWindow ();
        Selection.activeObject = asset;
    }

    public override void OnInspectorGUI ()
    {
        base.OnInspectorGUI ();
        SchedulingCabinetList dataList = (SchedulingCabinetList)target;
    }
}
#endif


namespace GameData
{
    public class SchedulingCabinetList : ScriptableObject
    {
        public const string resourcesPath = "Data/ScriptableObject/SchedulingCabinetData";
        public SchedulingCabinet [] Data;
    }
}
