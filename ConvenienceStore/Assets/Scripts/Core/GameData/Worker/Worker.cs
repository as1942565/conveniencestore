﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameData
{
    //序列化
    [System.Serializable]
    public struct Worker
    {
        public string name;                                         //員工姓名
        public int id;
        [Header ( "體力-魅力-運氣" )]
        public int [] abilitys;                                      //員工屬性
        public Abillity ability;                                      //員工屬性
        public int level;                                           //員工等級
        public int exp;                                             //員工經驗值
        public bool isMain;                                         //是否為女主角
        public bool unlock;                                         //員工是否解鎖
        public int relationRank;                                    //員工好感度
        public int relationEXP;                                     //員工好感值
        public int power;                                           //員工體力值
        public bool initWith;                                       //是否初次登場

        [Serializable]
        public struct Abillity
        {
            [SerializeField]
            public int[] abilities;
            public int Stamina {
                get => abilities[(int)type.Stamina];
                set => abilities[(int)type.Stamina] = value;
            }
            public int Charm {
                get => abilities[(int)type.Charm];
                set => abilities[(int)type.Charm] = value;
            }
            public int Luck {
                get => abilities[(int)type.Luck];
                set => abilities[(int)type.Luck] = value;
            }
            public int this[type index] {
                get => abilities[(int)index];

                set => abilities[(int)index] = value;
            }
            public int this[int index] {
                get => abilities[index];

                set => abilities[index] = value;
            }
            public int Length => abilities.Length;
            public enum type
            {
                Stamina = 0,
                Charm =1,
                Luck =2,
            }
        }
    }
}