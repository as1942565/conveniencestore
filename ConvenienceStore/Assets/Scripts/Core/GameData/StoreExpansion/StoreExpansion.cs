﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Tools;

namespace GameData
{
    [Serializable]
    public struct StoreExpansion
    {                                                                       //
        public string name;                                                 //擴建名稱
        public List<ExpansionType> type;                                                 //擴建種類
        public string skill;                                                //擴建功能
        public int cost;                                                   //擴建費用
        public string explanation;                                          //擴建描述  
        public bool unlock;                                                 //解鎖狀態
        public int storeLevel;                                              //屬於商店第幾規模的設施
        public List<StoreExpansionEventData> eventData;
        public List<UnlockConditionData> unlockCondition;
        public Vector2 spritePosition;
        [PreviewSprite]
        public Sprite sprite;
        public int spriteLevel;
        public int spriteOrder;
        public enum ExpansionType
        {
            income = 0,
            customer =1,
            police =2,
            clean =3,
            construct =4,
        }

        public StoreExpansionBaseEvent [] CreateEvents ()                   //建立所有店面擴建事件
        {
            StoreExpansionBaseEvent [] events = new StoreExpansionBaseEvent [eventData.Count];
            for ( int i = 0; i < events.Length; i++ )                       //搜尋所有店面擴建  事件
            {
                events [i] = CreateEvent ( i );                             //建立店面擴建事件
            }
            return events;                                                  //回傳店面擴建事件
        }

        private StoreExpansionBaseEvent CreateEvent ( int i )               //建立店面擴建事件
        {
            if ( i >= eventData.Count )
            {
                Debug.LogWarning ( i + " 超過事件長度" );
                return null;
            }
          
            Type t = Type.GetType ( "StoreExpansionEvent." + eventData[i].Event.ToString() );//將字串轉成腳本類型
            if ( t == null )
            {
                Debug.LogWarning ( $"{eventData[i].Event} 不是事件名稱" );
                return null;
            }
            StoreExpansionBaseEvent e = (StoreExpansionBaseEvent)Activator.CreateInstance ( t );//強制將腳本類型轉成店面擴建事件
            e.SetParam (eventData[i].Param);                       //傳遞該店面擴建的參數
            return e;                                                       //回傳店面擴建事件
        }
    }
}