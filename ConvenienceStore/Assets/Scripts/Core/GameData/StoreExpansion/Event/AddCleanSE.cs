﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoreExpansionEvent
{
    //每回合增加清潔度
    public class AddCleanSE : StoreExpansionBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int clean = int.Parse ( param [0] );
            pm.storeExpansionAddition.AddAutoAddClean ( clean );
            //( "(店面擴建)每回合增加" + clean + "清潔度" );
        }
        public override int ParamLength()
        {
            return 1;
        }
        public override string[] ParamName()
        {
            return new string[] { "清潔度" };
        }
    }
}