﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoreExpansionEvent
{
    //每回合增加治安度
    public class AddPoliceSE : StoreExpansionBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int police = int.Parse ( param [0] );
            pm.storeExpansionAddition.AddAutoAddPolice ( police );
            //( "(店面擴建)每回合增加" + police.ToString () + "治安度" );
        }
        public override int ParamLength()
        {
            return 1;
        }
        public override string[] ParamName()
        {
            return new string[] { "治安度" };
        }
    }
}