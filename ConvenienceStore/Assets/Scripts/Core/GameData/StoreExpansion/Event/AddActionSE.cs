﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoreExpansionEvent
{
    public class AddActionSE : StoreExpansionBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            pm.calender.AddMaxEngry ();
        }
        public override int ParamLength()
        {
            return 0;
        }
        public override string[] ParamName()
        {
            return new string [0];
        }
    }
}
