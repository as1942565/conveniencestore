﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoreExpansionEvent
{
    //依據行動點增加人氣值
    public class AddPopularSE : StoreExpansionBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            float num = float.Parse ( param [0] );
            pm.storeExpansionAddition.AddPopular ( num );
        }
        public override int ParamLength()
        {
            return 1;
        }
        public override string[] ParamName()
        {
            return new string[] { "人氣加成" };
        }
    }
}