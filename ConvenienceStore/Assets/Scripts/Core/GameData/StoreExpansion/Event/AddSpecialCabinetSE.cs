﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoreExpansionEvent
{
    public class AddSpecialCabinetSE : StoreExpansionBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            pm.cabinet.AddUnlock ( 3 );
            pm.cabinet.AddUnlock ( 3 );
            pm.calender.SetExpandSpecialCabiinet ();
            //("解鎖特殊商品櫃");
        }
        public override int ParamLength()
        {
            return 0;
        }
        public override string[] ParamName()
        {
            return new string [0];
        }
    }
}
