﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoreExpansionEvent
{
    public class AddMaxCabinetSE : StoreExpansionBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int shift = int.Parse ( param [0] );
            string [] shiftName = new string [] { "食品" , "飲品" , "雜貨" , "特殊" , "主打" };
            pm.cabinet.AddUnlock ( shift );
            //( "(店面擴建)" + shiftName [shift] + "的商品櫃欄位 + 1" );
        }
        public override int ParamLength()
        {
            return 1;
        }
        public override string[] ParamName()
        {
            return new string[] { "時段" };
        }
    }
}
