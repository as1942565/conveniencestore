﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoreExpansionEvent
{
    //解鎖店面擴建項目
    public class UnlockSE : StoreExpansionBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int ID = int.Parse ( param [0] );
            pm.storeExpansionUnlock.SetUnlock ( ID , true );
            //( "(店面擴建)第" + ID + "店面擴建項目已解鎖" );
        }
        public override int ParamLength()
        {
            return 1;
        }
        public override string[] ParamName()
        {
            return new string[] { "編號" };
        }
    }
}
