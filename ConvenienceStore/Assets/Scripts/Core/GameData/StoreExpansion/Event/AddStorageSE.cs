﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoreExpansionEvent
{
    //增加商品最大庫存
    public class AddStorageSE : StoreExpansionBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int storage = int.Parse ( param [0]);
            pm.storage.SetMaxStorage ( storage );
            //( "(店面擴建)商品的庫存增加到" + storage + "個" );
        }
        public override int ParamLength()
        {
            return 1;
        }
        public override string[] ParamName()
        {
            return new string[] { "總庫存量" };
        }
    }
}
