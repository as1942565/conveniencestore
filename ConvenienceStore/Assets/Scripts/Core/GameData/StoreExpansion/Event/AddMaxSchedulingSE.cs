﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoreExpansionEvent
{
    //增加最大排班人數
    public class AddMaxSchedulingSE : StoreExpansionBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int shift = int.Parse ( param [0] );
            pm.employeeScheduing.AddUnlock ( shift , 1 );
        }
        public override int ParamLength()
        {
            return 1;
        }
        public override string[] ParamName()
        {
            return new string[] { "時段" };
        }
    }
}