﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoreExpansionEvent
{
    //增加整體收入
    public class AddRevenueSE : StoreExpansionBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            float num = float.Parse ( param [0] );
            pm.storeExpansionAddition.AddIncome ( num );
           //( "(店面擴建)整體營收增加" + num + "倍" );
        }
        public override int ParamLength()
        {
            return 1;
        }
        public override string[] ParamName()
        {
            return new string[] { "收入加成" };
        }
    }
}