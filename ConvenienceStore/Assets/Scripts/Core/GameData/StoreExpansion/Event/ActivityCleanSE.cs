﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoreExpansionEvent
{
    //增加員工活動的清潔度效果
    public class ActivityCleanSE : StoreExpansionBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            float addition = float.Parse ( param [0] );
            pm.specialAddition.SetCleanAddition ( addition );
        }

        public override int ParamLength()
        {
            return 1;
        }
        public override string[] ParamName()
        {
            return new string[] { "效果加成" };
        }
    }
}
