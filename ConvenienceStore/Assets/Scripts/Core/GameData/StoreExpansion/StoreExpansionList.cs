﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;
using Tools;
namespace GameData
{
    public class StoreExpansionList : ScriptableObject                  //店面擴建列表
    {
        //存放ScriptableObject路徑
        public const string resourcesPath = "Data/ScriptableObject/StoreExpansionData";//存放ScriptableObject路徑
        public StoreExpansion [] Data;                                  //店面擴建資料

        public StoreExpansionBaseEvent [] GerenateEvent ( int ID )      //生成該編號的所有事件
        {
            if ( ID >= Data.Length )
                return null;
            return Data [ID].CreateEvents ();                           //回傳該編號的所有事件
        }
    }
}