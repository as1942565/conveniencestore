﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StoreExpansionBaseEvent
{
    private string [] Param;

    public void SetParam ( string [] param )
    {
        Param = param;
    }

    public void DoEvent ( PlayerDataManager pm , GameDataManager gm )
    {
        DoEvent ( pm , gm , Param );
    }

    public abstract void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param );

    public abstract int ParamLength();

    public virtual string[] ParamName()
    {
        string[] s = new string[ParamLength()];
        for (int i = 0; i < ParamLength(); i++)
        {
            s[i] = $"Param{i}";
        }
        return s;
    }
}
