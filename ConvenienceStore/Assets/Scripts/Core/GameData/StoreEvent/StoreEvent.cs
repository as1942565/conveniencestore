﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Tools;

namespace GameData
{
    [Serializable]
    public struct StoreEvent
    {
        [Header ( "突發事件機率" )]
        public int Chance;
        [Header ( "好事件門檻" )]
        public int Score;
        [Header ( "會觸發突發事件" )]
        public BoolList [] storeID;
        [Header ( "事件名稱" )]
        public string [] eventName;
        [Header ( "事件參數" )]
        public string [] eventParam;

        public void Init ()
        {
            Chance = 30;
            Score = 120;

            int [] eventID = new int [] { 0 , 7 , 4 , 33 , 25 , 8 , 9 , 12 , 11 , 17 };
            string [] names = new string [] { "AddRelationE-AddPopularE" , "AddRelationE-AddIncomeE" , "AddRelationE-AddIncomeE" , "AddRelationE-GetSpecialE" , "AddRelationE-AddAllIncomeE" , "AddRelationE-AddAllIncomeE" , "AddRelationE-AddAllIncomeE" , "AddRelationE-AddIncomeE" , "AddRelationE-AddPopularE" , "AddRelationE-AddEXPE" };
            string [] param = new string [] { "3-3^0.15" , "3-3^0^0.25" , "3-3^1^0.25" , "3-1.5" , "6-9^0.3" , "6-3^0.15" , "6-3^0.15" , "6-3^1^0.45" , "6-3^0.3" , "15-20" };
            
            eventName = new string [34];
            eventParam = new string [34];
            for ( int i = 0; i < eventID.Length; i++ )
            {
                int id = eventID [i];
                eventName [id] = names [i];
                eventParam [id] = param [i];

            }
        }

        public StoreEventBaseEvent [] CreateEvents ( int id )
        {
            if ( eventName [id] == "" )
                return null;
            string [] eventNames = eventName [id].Split ( '-' );
            string [] param = eventParam [id].Split ( '-' );
            StoreEventBaseEvent [] events = new StoreEventBaseEvent [eventNames.Length];
            for ( int i = 0; i < events.Length; i++ )
            {
                events [i] = CreateEvent ( eventNames [i] , param [i] );
            }
            return events;
        }

        private StoreEventBaseEvent CreateEvent ( string name , string param )
        {
            Type t = Type.GetType ( "StoreEventEvent." + name );
            if ( t == null )
            {
                Debug.LogWarning ( $"{name} 不是是件" );
                return null;
            }
            StoreEventBaseEvent e = (StoreEventBaseEvent)Activator.CreateInstance ( t );
            e.SetParam ( param.Split ( '^' ) );
            return e;
        }
    }
}

