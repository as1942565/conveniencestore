﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor ( typeof ( StoreEventList ) )]
public class StoreEventListEditor : Editor
{
    [MenuItem("Assets/Create/Data/StoreEventList" , priority = 9 )]
    public static void CreateMyAsset ()
    {
        StoreEventList asset = ScriptableObject.CreateInstance<StoreEventList>();
        AssetDatabase.CreateAsset ( asset , "Assets/Resources/" + StoreEventList.resourcesPath + ".asset" );
        AssetDatabase.SaveAssets ();
        EditorUtility.FocusProjectWindow ();
        Selection.activeObject = asset;
    }

    public override void OnInspectorGUI ()
    {
        base.OnInspectorGUI ();
        StoreEventList listData = (StoreEventList)target;   
        if ( GUILayout.Button ( "Read From Script" ) )
        {
            listData.Data.Init ();
        }
        EditorUtility.SetDirty ( target );
    }
}
#endif

public class StoreEventList : ScriptableObject
{
    public const string resourcesPath = "Data/ScriptableObject/StoreEvent";
    public StoreEvent Data;
}