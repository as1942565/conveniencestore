﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StoreEventBaseEvent
{
    private string [] Param;
    
    public virtual bool NeedAdditionalParam ()
    {
       return false;
    }

    public string[] GetParam ()
    {
        return Param;
    }

    public void SetParam ( string [] param )
    {
        Param = param;
    }

    public void DoEvent ( PlayerDataManager pm , GameDataManager gm )
    {
        string t = "";
        for ( int i = 0; i < Param.Length; i++ )
        {
            t += Param [i] + "   ";
        }
        DoEvent ( pm , gm , Param );
    }
    public abstract void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] Param );
}
