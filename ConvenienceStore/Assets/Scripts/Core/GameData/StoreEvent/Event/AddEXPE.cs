﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoreEventEvent
{
    public class AddEXPE : StoreEventBaseEvent
    {
        public override bool NeedAdditionalParam ()
        {
            return true;
        }

        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] Param )
        {
            if ( Param.Length != 2 )
            {
                return;
            }
            //( $"增加{Param[0]}經驗值" );
            int employeeID = int.Parse ( Param[0] );
            int exp = int.Parse ( Param[1] );
            //Todo : 取得女主角編號
            pm.employeeList.GetEmployee ( employeeID ).AddExp ( exp , gm , pm );
        }
    }
}