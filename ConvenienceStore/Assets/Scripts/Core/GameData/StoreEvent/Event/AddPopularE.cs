﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoreEventEvent
{
    public class AddPopularE : StoreEventBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] Param )
        {
            //( $"{Param [0]}時段內 增加{Param [1]}倍的人氣加成" );
            int runShift = int.Parse ( Param [0] );
            float addition = float.Parse ( Param [1] );
            int totalShift = pm.calender.TotalShift;
            pm.storeEventAddition.AddPopular ( totalShift + runShift , addition );
        }
    }
}