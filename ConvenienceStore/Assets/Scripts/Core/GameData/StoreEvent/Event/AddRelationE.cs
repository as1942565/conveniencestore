﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoreEventEvent
{
    public class AddRelationE : StoreEventBaseEvent
    {
        public override bool NeedAdditionalParam ()
        {
            return true;
        }

        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] Param )
        {
            if ( Param.Length != 2 )
            {
                return;
            }
            int employeeID = int.Parse ( Param[0] );
            int relation = int.Parse ( Param [1] );
            pm.employeeList.AddRelationEXP ( employeeID , relation , gm , pm );
        }
    }
}