﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoreEventEvent
{
    public class AddIncomeE : StoreEventBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] Param )
        {
            //( $"{Param [0]}個時段內 {Param [1]}種類的營收增加{Param [2]}倍" );
            int runShift = int.Parse ( Param [0] );
            GameData.Commodity.CommodityType type = (GameData.Commodity.CommodityType)int.Parse ( Param [1] );
            float addition = float.Parse ( Param [2] );
            int totalShift = pm.calender.TotalShift;
            pm.storeEventAddition.AddIncome ( totalShift + runShift , type , addition );
        }
    }
}