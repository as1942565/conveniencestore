﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoreEventEvent
{
    public class GetSpecialE : StoreEventBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] Param )
        {
            //( $"下次獲得特殊商品 增加{Param[0]}倍" );
            float addition = float.Parse ( Param [0] );
            pm.storeEventAddition.SetSpecial ( true , addition );
        }
    }
}