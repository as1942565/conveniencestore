﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoreEventEvent
{
    public class AddAllIncomeE : StoreEventBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] Param )
        {
            //( $"{Param [0]}個時段內 所有營收增加{Param [1]}倍" );
            int shift = int.Parse ( Param [0] );
            float addition = float.Parse ( Param [1] );
            int totalShift = pm.calender.TotalShift;
            pm.storeEventAddition.AddIncome ( totalShift + shift , GameData.Commodity.CommodityType.food , addition );
            pm.storeEventAddition.AddIncome ( totalShift + shift , GameData.Commodity.CommodityType.drink , addition );
            pm.storeEventAddition.AddIncome ( totalShift + shift , GameData.Commodity.CommodityType.grocery , addition );
            pm.storeEventAddition.AddIncome ( totalShift + shift , GameData.Commodity.CommodityType.special , addition );
        }
    }
}
