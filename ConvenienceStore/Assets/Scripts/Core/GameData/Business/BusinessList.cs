﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;
using Tools;
//編輯專用
namespace GameData
{
    public class BusinessList : ScriptableObject                        //店長業務列表
    {
        public const string resourcesPath = "Data/ScriptableObject/BusinessData";//存放ScriptableObject路徑
        public Business [] Data;                                        //店長業務資料

        public BusinessBaseEvent [] GenerateEvent ( int ID )            //生成該編號的店長業務事件
        {
            if ( ID >= Data.Length )
            {
                Debug.LogError ( ID + "超過店長業務長度" );
                return null;
            }
            return Data[ID].CreateEvents();                             //回傳該編號的店長業務事件
        }
    }
}