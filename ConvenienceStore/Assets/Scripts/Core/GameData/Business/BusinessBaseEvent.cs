﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//abstract : 抽象型別  只能作為其他類別的基底類別(只可以被繼承不能被new)
public abstract class BusinessBaseEvent
{                                                               //
    private string [] Param;                                    //事件參數

    public void SetParam ( string [] param )
    {
        Param = param;
    }

    public void DoEvent ( PlayerDataManager pm , GameDataManager gm )
    {
        DoEvent ( pm , gm , Param );                            //執行事件
    }

    public virtual void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
    {
    }

    public abstract int ParamLength();

    public virtual string[] ParamName()
    {
        string[] s = new string[ParamLength()];
        for (int i = 0; i < ParamLength(); i++)
        {
            s[i] = $"Param{i}";
        }
        return s;
    }
}
