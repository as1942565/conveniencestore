﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BusinessEvent
{
    //還債
    public class PayMoneyB : BusinessBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int level = int.Parse ( param [0] );
            pm.businessUnlock.SetLevel ( level );
        }
        public override int ParamLength()
        {
            return 1;
        }
        public override string[] ParamName()
        {
            return new string[] { "店面規模" };
        }
    }
}