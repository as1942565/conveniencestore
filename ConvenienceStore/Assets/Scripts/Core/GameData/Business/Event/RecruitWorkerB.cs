﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tools;

namespace BusinessEvent
{
    //招募工讀生
    public class RecruitWorkerB : BusinessBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int quantity = int.Parse ( param [0] );
            pm.hireWorker.SetQuantity ( quantity );
            pm.hireWorker.SetCanHireID ( gm , pm );
        }
        public override int ParamLength()
        {
            return 1;
        }
        public override string[] ParamName()
        {
            return new string[] { "數量" };
        }
    }
}