﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BusinessEvent
{
    //隱藏店長業務項目
    public class HideB : BusinessBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int id = int.Parse ( param [0] );
            pm.businessUnlock.SetMode ( id , BusinessUnlock.state.hide );
            //(店長業務)隱藏第" + id.ToString () + "項店長業務項目;
        }
        public override int ParamLength()
        {
            return 1;
        }
        public override string[] ParamName()
        {
            return new string[] {  "編號" };
        }
    }
}