﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BusinessEvent
{
    //還債劇情
    public class CustomStoryB : BusinessBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            string businessID = param [1];
            int id = 0;
            if ( int.TryParse ( businessID , out id ) )
            {
                if ( pm.businessUnlock.GetWatch ( id ) == true )
                    return;
            }
            else
            {
                return;
            }
            string storyID = param [0];
            if ( StoryManager.self != null )
            {
                GameLoopManager.ReadStory = true;
                string language = GameLoopManager.instance.om.optionData.GetLanguageStr ();
                StoryManager.self.StartStory ( $"Data/Story/{language}Custom/" , storyID );
                pm.businessUnlock.SetWatch ( id , true );
            }
        }
        public override int ParamLength()
        {
            return 2;
        }
        public override string[] ParamName()
        {
            return new string[] { "觀看其他故事" , "店長業務編號" };
        }
    }
}
