﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BusinessEvent
{
    //增加商品收入
    public class AddIncomeB : BusinessBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            GameData.Commodity.CommodityType type = (GameData.Commodity.CommodityType)int.Parse ( param [0] );
            float addition = float.Parse ( param [1] );
            int shift = int.Parse ( param [2] );
            int groupID = int.Parse ( param [3] );
            int totalShift = pm.calender.TotalShift;
            int runShift = totalShift + 1;              //加成效果在下一個時段才開始觸發
            pm.businessAddition.AddIncome ( runShift , type , addition , runShift + shift , groupID );
            //( "(店長業務)所有商品收入增加" + addition.ToString () + "倍" );
        }

        public override int ParamLength()
        {
            return 4;
        }
        public override string[] ParamName()
        {
            return new string[] {"收入加成", "種類","時效","群組編號" };
        }
    }
}