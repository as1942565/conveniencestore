﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BusinessEvent
{
    //完成項目
    public class CompleteB : BusinessBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int completeID = int.Parse ( param [0] );
            pm.businessUnlock.SetMode ( completeID , BusinessUnlock.state.complete );
            //( "(店長業務)第" + completeID.ToString () + "項店長業務已完成" );
        }
        public override int ParamLength()
        {
            return 1;
        }
        public override string[] ParamName()
        {
            return new string[] { "編號" };
        }
    }
}