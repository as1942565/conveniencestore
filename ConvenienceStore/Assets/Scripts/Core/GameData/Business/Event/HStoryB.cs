﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BusinessEvent
{
    public class HStoryB : BusinessBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int employeeID = int.Parse ( param [0] );
            int storyID = int.Parse ( param [1] );
            string Path = $"H_Employee{employeeID}/H_Employee{employeeID}_0{storyID + 1}";
            if ( StoryManager.self != null )
            {
                string language = GameLoopManager.instance.om.optionData.GetLanguageStr ();
                StoryManager.self.StartStory ( $"Data/Story/{language}H/" , Path );
                GameLoopManager.instance.om.unlockCGData.SetUnlock ( employeeID , storyID , true );
            }
        }
        public override int ParamLength ()
        {
            return 2;
        }
        public override string [] ParamName ()
        {
            return new string [] { "女主編號" , "故事編號" };
        }
    }
}
