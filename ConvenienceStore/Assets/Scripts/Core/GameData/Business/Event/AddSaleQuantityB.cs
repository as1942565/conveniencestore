﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BusinessEvent
{
    //增加商品銷售數量
    public class AddSaleQuantityB : BusinessBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            GameData.Commodity.CommodityType type = (GameData.Commodity.CommodityType)int.Parse ( param [0] );
            int quantity = int.Parse ( param [1] );
            string [] typeName = new string [] { "食品" , "飲品" , "雜貨" };
            int shift = int.Parse ( param [2] );
            int totalShift = pm.calender.TotalShift;
            int runShift = totalShift + 1;              //加成效果在下一個時段才開始觸發
            pm.businessAddition.AddSaleQuantity ( runShift , type , quantity , runShift + shift );
            //( "(店長業務)" + typeName [(int)type] + "的銷售數量增加" + quantity.ToString () + "個" );
        }
        public override int ParamLength()
        {
            return 3;
        }
        public override string[] ParamName()
        {
            return new string[] { "銷量", "種類", "時效"};
        }
    }
}