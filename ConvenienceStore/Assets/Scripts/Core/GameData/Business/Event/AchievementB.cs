﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BusinessEvent
{
    public class AchievementB : BusinessBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            string name = param [0];
            Achievement.SetAchievement ( name );
        }

        public override int ParamLength ()
        {
            return 1;
        }
        public override string [] ParamName ()
        {
            return new string [] { "成就名稱" };
        }
    }
}