﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BusinessEvent
{
    public class WatchB : BusinessBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int id = int.Parse ( param [0] );
            pm.businessUnlock.SetMode ( id , BusinessUnlock.state.watch );
        }
        public override int ParamLength()
        {
            return 1;
        }
        public override string[] ParamName()
        {
            return new string[] { "編號" };
        }
    }
}
