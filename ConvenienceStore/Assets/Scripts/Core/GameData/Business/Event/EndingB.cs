﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BusinessEvent
{
    public class EndingB : BusinessBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int employeeID = int.Parse ( param [0] );
            int storyID = int.Parse ( param [1] );
            string Path = $"End_Employee{employeeID}/End_Employee{employeeID}_0{storyID + 1}";

            if ( StoryManager.self != null )
            {
                StoryManager.self.ReadEndDrama = true;
                string language = GameLoopManager.instance.om.optionData.GetLanguageStr ();
                StoryManager.self.StartStory ( $"Data/Story/{language}End/" , Path );
            }
            //( "(店長業務)觀看第" + employeeID.ToString () + "位員工的第" + storyID + "個結局故事" );
        }

        public override int ParamLength ()
        {
            return 2;
        }
        public override string [] ParamName ()
        {
            return new string [] { "女主編號" , "故事編號" };
        }
    }
}