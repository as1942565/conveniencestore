﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BusinessEvent
{
    //解鎖店長業務項目(只解一個))
    public class UnlockB : BusinessBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            pm.businessUnlock.SetUnlock ( int.Parse ( param [0] ) , int.Parse ( param [1] ) , true );
            //( "(店長業務)第" + param [0] + "項店長業務的第" + param [1] + "條件被解鎖" );
        }
        public override int ParamLength()
        {
            return 2;
        }
        public override string[] ParamName()
        {
            return new string[] { "編號", "條件編號" };
        }
    }
}