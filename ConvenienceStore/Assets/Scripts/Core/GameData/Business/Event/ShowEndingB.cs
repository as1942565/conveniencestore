﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BusinessEvent
{
    public class ShowEndingB : BusinessBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int id = int.Parse ( param [0] );
            pm.businessUnlock.SetEndingID ( id );



            if ( GameSimulation.instance == null )
                return;
            if ( GameSimulation.instance.employeeID != -1 )
                return;
            if ( id == 17 )
            {
                GameSimulation.instance.employeeID = 0;
                return;
            }
            if ( id == 23 )
            {
                GameSimulation.instance.employeeID = 1;
                return;
            }
            if ( id == 29 )
            {
                GameSimulation.instance.employeeID = 2;
                return;
            }
            Debug.LogError ( "(模擬) 找不到符合的員工" );
        }
        public override int ParamLength ()
        {
            return 1;
        }
        public override string[] ParamName()
        {
            return new string[] { "結局編號" };
        }
    }
}