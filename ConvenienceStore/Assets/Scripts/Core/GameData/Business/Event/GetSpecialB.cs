﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BusinessEvent
{
    //取得特殊商品
    public class GetSpecialB : BusinessBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            string [] num = param[0].Split ( '~' );
            int start = int.Parse ( num [0] );
            int end = int.Parse ( num [1] ) + 1;
            int RanQuantity = Random.Range ( start , end );
            int specialID = pm.commodityItem.RanGetSpecialID ( gm );
            int shift = pm.calender.TotalShift;
            pm.storage.AddQuantity ( specialID , RanQuantity , shift );
            //( "(店長業務)獲得" + RanQuantity.ToString () + "個特殊商品" );
        }
        public override int ParamLength()
        {
            return 1;
        }
        public override string[] ParamName()
        {
            return new string[] { "數量(0~8)"};
        }
    }
}