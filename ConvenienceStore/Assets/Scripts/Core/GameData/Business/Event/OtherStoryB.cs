﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BusinessEvent
{
    public class OtherStoryB : BusinessBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            string path = param [0];
            string name = param [1];
            string Path = $"{path}/{name}";
            if ( StoryManager.self != null )
            {
                string language = GameLoopManager.instance.om.optionData.GetLanguageStr ();
                StoryManager.self.StartStory ( $"Data/Story/{language}" , Path );
            }
        }
        public override int ParamLength ()
        {
            return 2;
        }
        public override string [] ParamName ()
        {
            return new string [] { "故事路徑" , "故事名稱" };
        }
    }
}