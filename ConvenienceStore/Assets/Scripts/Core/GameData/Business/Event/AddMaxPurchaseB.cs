﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BusinessEvent
{
    //增加商品進貨上限
    public class AddMaxPurchaseB : BusinessBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int num = int.Parse ( param [0] );
            pm.purchase.AddMaxPurchaseNumByOne ( num );
        }
        public override int ParamLength()
        {
            return 1;
        }
        public override string[] ParamName()
        {
            return new string[] { "進貨上限" };
        }
    }
}
