﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BusinessEvent
{
    //看主線故事
    public class NiceStoryB : BusinessBaseEvent
    {
        public override void DoEvent ( PlayerDataManager pm , GameDataManager gm , string [] param )
        {
            int employeeID = int.Parse ( param [0] );
            int storyID = int.Parse ( param [1] );
            string Path = $"Main_Employee{employeeID}/Main_Employee{employeeID}_0{storyID + 1}";

            if ( StoryManager.self != null )
            {
                string language = GameLoopManager.instance.om.optionData.GetLanguageStr ();
                StoryManager.self.StartStory ( $"Data/Story/{language}Main/" , Path );
            }

            //( "(店長業務)播放第" + employeeID.ToString () + "位女主角的第" + storyID.ToString () + "個主線故事" );
        }
        public override int ParamLength()
        {
            return 2;
        }
        public override string[] ParamName()
        {
            return new string[] { "女主編號", "故事編號" };
        }
    }
}