﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Tools;
using System.Linq;

namespace GameData
{
    //店長業務資料
    [Serializable]
    public struct Business
    {
        public enum EventType                                           //店長業務種類
        {                                                               //
            normal = 0,                                                 //0 : 一般事件
            story = 1,                                                  //1 : 故事事件
            h = 2,                                                      //2 : H事件
            ending = 3,                                                 //3 : 結局事件
            nothing = 4,
        }

        public string name;                                             //店長業務名稱
        public EventType eventType;                                     //店長業務種類
        public int cost;                                                //店長業務花費
        public string effectText;                                       //店長業務效果
        public string unlockRequirement;                                //店長業務解鎖條件文字
        public bool [] unlock;                                          //店長業務傑解鎖狀態
        public List<BusinessEventData> eventData;
        public List<UnlockConditionData> unlockCondition;
        public int groupID;                                             //店長業務群組關係
        public BusinessBaseEvent [] CreateEvents ()                     //建立事件們
        {
            BusinessBaseEvent [] events = new BusinessBaseEvent[eventData.Count];
            for ( int i = 0; i < eventData.Count; i++ )               //搜尋所有事件
            {
                events [i] = CreateEvent ( i );                         //建立店長業務事件
            }
            return events;                                              //回傳店長業務事件
        }
        private BusinessBaseEvent CreateEvent ( int id )                //建立店長業務事件
        {
            if ( id >= eventData.Count )
            {
                Debug.LogError ( id + "超過店長業務長度" );
                return null;
            }
            
            Type t = Type.GetType ( "BusinessEvent." + eventData[id].Event.ToString() );//將該名稱轉成腳本類型
            if ( t == null )                                            //如果該名稱無法轉成類型 (防呆)
            {
                Debug.LogWarning ($"type { eventData[id].Event} not found!");
                return null;
            }
            BusinessBaseEvent e = (BusinessBaseEvent)Activator.CreateInstance ( t );//強制轉成店長業務事件
            if ( id >= eventData.Count )
                e.SetParam ( new string [] { } );                       //將該事件的參數記錄下來
            else
                e.SetParam (eventData[id].Param.ToArray() );              //將該事件的參數記錄下來
            return e;                                        //回傳店長業務事件
        }
    }
}