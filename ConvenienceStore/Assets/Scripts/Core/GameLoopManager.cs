﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System.IO;
using System;

//遊戲管理器 (最大權限)
public class GameLoopManager : MonoBehaviour
{
    private GameLoopManager () { }
    public static GameLoopManager instance;
    public PlayerDataManager pm;                                        //玩家資料管理器(需要存檔)
    public GameDataManager gm;                                          //遊戲資料管理器(紀錄不用存檔的參數)
    public OnlyDataManager om;
    public NextShiftProcessor nextShift;                                //更換時段所要做的事
    public UnlockManager unlockManager;                                 //特殊解鎖條件管理器
    public System.Action<GameLoopManager> OnInit;
    public static bool CanAfterNextShift;
    public static bool ReadStory;
    public static bool WaitStoryLog;
    public static bool Trouble;
    public static bool SpecialDay;
    public static bool SkipPrologue;
    public static int SaveAndLoadPage = 0;

    //[HideInInspector]
    public bool ReadFirstStory;
    //[HideInInspector]
    public bool Load;

    public bool IsHire;
    
    [HideInInspector]
    public LevelManager levelManager;
    private ExpansionManager expansionManager;
    
    [HideInInspector]
    public bool AllAge = false;        //此功能暫時不會用到
    [SerializeField]
    private bool Demo;                  //15天試玩版
    [SerializeField]
    private bool SteamCheck;            //發行商檢查(開啟全CG)
    public bool WatchStory;
    public int [] ids;

    public void Test ( string key )
    {
        Debug.Log ( key );
    }

    public bool GetSteam ()                //Steam版
    {
        string documents = Environment.GetFolderPath ( Environment.SpecialFolder.MyDocuments );
        string path = Path.Combine ( documents , "BlackMercuryStudio/ConvenienceStore/dontopen.txt" );
        return File.Exists ( path ) ? true : false;
    }


    private void Awake ()
    {
        instance = this;
        Init ();                                                        //初始化
        bool res = SaveLoadManager.OnlyDataLoad ( out om );
        SceneManager.activeSceneChanged += ChangedActiveScene;
        if ( !res )
        {
            CreateOM ();
        }
        SetResolution ();
    }

    private Vector2 [] size = new Vector2 [] { new Vector2 ( 0f , 0f ) , new Vector2 ( 800f , 450f ) , new Vector2 ( 1280f , 720f ) , new Vector2 ( 1920f , 1080f ) };

    public void SetResolution ()
    {
        //GameLoopManager.instance.om.optionData.SetResolution ( tempResolution );
        int re = om.optionData.GetResolution ();
        if ( re == 0 )
        {
            FullScreen ();
            return;
        }

        Screen.SetResolution ( Mathf.FloorToInt ( size [re].x ) , Mathf.FloorToInt ( size [re].y ) , false );
    }

    void FullScreen ()
    {
        float width = Screen.width;
        float heigth = Screen.height;
        float scale = 16f / 9f;
        if ( width / heigth > scale )        //螢幕過寬 所以對照高度
        {
            float size = heigth / 9;
            Screen.SetResolution ( Mathf.FloorToInt ( size * 16 ) , Mathf.FloorToInt ( heigth ) , true );
        }

        if ( scale > width / heigth )        //螢幕過高 所以對照寬度
        {
            float size = width / 16;
            Screen.SetResolution ( Mathf.FloorToInt ( width ) , Mathf.FloorToInt ( size * 9 ) , true );
        }
    }

    private void Start ()
    {
        PlayerDataManager tmppm = null;
        bool teachData = SaveLoadManager.Load ( out tmppm , "Teach" );
      
        if ( !teachData )
        {
            BGMManager.self.Mute = true;
            EffectManager.self.Mute = true;
            SceneManager.LoadScene ( "Demo" );
            StartCoroutine ( CreateTeachData () );
        }
        else
        {
            OnLoaded ( tmppm );
        }
    }

    IEnumerator CreateTeachData ()
    {
        while ( StoryManager.self == null )
            yield return null;
        StoryManager.self.Mask.SetActive ( true );
        StoryManager.self.EnableTeach = true;
        for ( int i = 0; i < 38; i++ )
        {
            TeachPlayerManager.self.SkipDrama ( i );
            yield return null;
        }
        SaveLoadManager.Save ( pm , "Teach" );
        BGMManager.self.Mute = false;
        EffectManager.self.Mute = false;
        yield return null;
        SceneManager.LoadScene ( "GameStart" );
    }

    public void CreateOM ()
    {
        om = new OnlyDataManager ();
        om.Init ();
        SaveLoadManager.OnlyDataSave ( om );
    }

    private void ChangedActiveScene ( Scene current , Scene next )
    {
        levelManager = FindObjectOfType<LevelManager> ();
        expansionManager = FindObjectOfType<ExpansionManager> ();
        if ( expansionManager != null )
        {
            int [] storeEpansionID = new int [] { 2 , 5 , 13 , 28 };
            for ( int i = 0; i < storeEpansionID.Length; i++ )
                expansionManager.CreateItem ( storeEpansionID [i] , gm );
        }
        
        if ( next.name == "GameStart" )
        {
            BGMManager.self.SetClip ( "Audios/BGM/GameStart/GameStart1" );
            om.audioData.PlayBGM ( 0 );
        }
        if ( next.name == "Demo" )
            UI.BuildMainPanel.self.Init ();
    }

    public void Init ()                                                 //初始化
    {
        WaitStoryLog = false;
        gm = new GameDataManager ();
        gm.Init ();
        pm = new PlayerDataManager ();
        pm.Init ( gm );
        /*pm.commodityItem.SetUnlock ( 2 );
        pm.storage.AddQuantity ( 2 , 100 , 0 );
        pm.cabinet.SetCommodityID ( 0 , 0 , 2 );*/
        int [] storeEpansionID = new int [] { 2 , 5 , 13 , 28 };
        for ( int i = 0; i < storeEpansionID.Length; i++ )
        {
            int id = storeEpansionID [i];
            pm.storeExpansionUnlock.SetComplete ( id , true );
        }
        nextShift = new NextShiftProcessor ( pm , gm );
        unlockManager = new UnlockManager ();
        OnLoaded ( pm );
    }

    public void OnLoaded (  PlayerDataManager pm )
    {
        unlockManager.Init ( gm , pm );
        nextShift.UpdatePM ( pm );
        if (OnInit != null)
        {
            OnInit(this);
        }
    }

    public void ResearchCost ( int id )
    {
        
    }
    public UnityAction GenerateResearchCost ( int id )
    {
        return () => ResearchCost ( id );
    }

    public void ResearchType ( int id )
    {

    }
    public UnityAction GenerateResraechType ( int id )
    {
        return () => ResearchType ( id );
    }

    public void ImproveItem ( int id )
    {

    }
    public UnityAction GenerateImproveItem ( int id )
    {
        return () => ImproveItem ( id );
    }

    public void ImproveType ( int id )
    {

    }
    public UnityAction GenerateImproveType ( int id )
    {
        return () => ImproveType ( id );
    }

    public void SetupCommodityItem ( int id )
    {

    }
    public UnityAction GenerateSetupCommodityItem ( int id )
    {
        return () => SetupCommodityItem ( id );
    }

    public void SetCommodityType ( int id )
    {

    }
    public UnityAction GenerateSetupCommodityType ( int id )
    {
        return () => SetCommodityType ( id );
    }

    public void CabinetItem ( int id )
    {

    }
    public UnityAction GenertaeCabinetItem ( int id )
    {
        return () => CabinetItem ( id );
    }

    public void RestockItem ( int id )
    {

    }
    public UnityAction GenerateRestockItem ( int id )
    {
        return () => RestockItem ( id );
    }

    public void RestockType ( int id )
    {

    }
    public UnityAction GenerateRestockType ( int id )
    {
        return () => ResearchType ( id );
    }

    public bool GetDemo ()
    {
        return Demo;
    }
    public bool GetSteamCheck ()
    {
        return SteamCheck;
    }
}
