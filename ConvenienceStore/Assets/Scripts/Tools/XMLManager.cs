﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;              //編碼
using System.IO;       			//寫文件的Function
using System.Xml.Serialization; //序列化
using System.Xml;               //寫入
using System.Runtime.Serialization;

public class XMLManager 
{
	public static void Save<T> ( T obj, string filelocation ) 			// T : 一個模版 原本是使用PlayerData 但以後可能會儲存很多個 所以使用模版
	{
        SerializeObject<T> ( obj , filelocation );
		//CreateXML ( SerializeObject<T>(obj,fileLocation), filelocation );            //呼叫34行 
	}	

	public static bool TryLoad<T>( string filelocation, out T ret ) where T : class 	//嘗試讀檔
	{
		if ( File.Exists ( filelocation ) ) 
		{
			ret = DeserializeObject<T> ( LoadXML( filelocation ) );
			return true;
		} 
		else 
		{
			ret = null;
			return false;
		}
	}
	public static T Load<T> ( string filelocation ) where T : class 
	{
		if ( File.Exists ( filelocation ) ) 
		{
			return DeserializeObject<T> ( LoadXML( filelocation ) );
		} 
		else 
		{
			return null;
		}
	}

	private static string SerializeObject<T> ( T pObject,string path ) 			 //轉成字串   
	{                           
		DataContractSerializer  xs = new DataContractSerializer  ( typeof( T ) );
        using ( var writer = new XmlTextWriter ( File.CreateText (path) ) )
        {
            writer.Formatting = Formatting.Indented;
            xs.WriteObject ( writer , pObject );
        }//被序列化的腳本
        return "";
	}
	private static T DeserializeObject<T>(string pXmlizedString) 
	{
		DataContractSerializer xs = new DataContractSerializer ( typeof( T ) );										//被序列化的腳本
		MemoryStream memoryStream = new MemoryStream ( StringToUTF8ByteArray ( pXmlizedString ) );	//反序列化(讀取)的位置
		return ( T )xs.ReadObject ( memoryStream );												//反序列化( 位置 )
	}

	private static string UTF8ByteArrayToString ( byte[] characters ) 
	{
		UTF8Encoding encoding = new UTF8Encoding ();
		string constructedString = encoding.GetString ( characters );
		return ( constructedString );
	}
	private static byte[] StringToUTF8ByteArray ( string pXmlString )
	{
		UTF8Encoding encoding = new UTF8Encoding ();
		byte[] byteArray = encoding.GetBytes ( pXmlString );
		return byteArray;
	}
	private static void CreateXML( string data, string fileLocation ) 		// 輸出檔案  資料寫入文件
	{       
		StreamWriter writer;
		FileInfo t = new FileInfo ( fileLocation );
		if ( !t.Exists ) {
			writer = t.CreateText ();
		} else
		{
			t.Delete ();
			writer = t.CreateText ();
		}
		writer.Write ( data );
		writer.Close ();		//確保不會在存檔期間又再次存檔

		/*  using (BinaryWriter writer = new BinaryWriter(File.Open(fileLocation, FileMode.Create))) {
              writer.Write(encode(data));
          }*/
	}

	private static string LoadXML ( string fileLocation ) 
	{
		if ( File.Exists ( fileLocation ) ) 
		{
			StreamReader r = File.OpenText ( fileLocation );
			string _info = r.ReadToEnd ();
			r.Close ();		//確保不會在讀檔期間又再次讀檔

			return _info;
		}
		//  Debug.Log("file not found");
		/*  if (File.Exists(fileLocation)) {

            using (BinaryReader reader = new BinaryReader(File.Open(fileLocation, FileMode.Open))) {
                string _info = reader.ReadString();
                return decode(_info);
            }
        }*/
		return "";
	}
	public static string encode ( string strData )
	{
		return strData;
		// return Convert.ToBase64String(UTF8Encoding.UTF8.GetBytes(strData));
	}
	public static string decode ( string strData )
	{
		return strData;
		//  return UTF8Encoding.UTF8.GetString(Convert.FromBase64String(strData));
	}

}