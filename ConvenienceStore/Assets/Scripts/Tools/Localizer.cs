﻿using System;
using System.Collections.Generic;

public static class Localizer
{
    public enum Language
    {
        Taiwanese = 0,
        Japanese = 1,
        English = 2,
        Korean = 3,
        Russian = 4,
    }

    private static Dictionary<string , string> [] stringTable;

    public static void Init ( string data )
    {
        string [] line = data.Split ( '\n' );
        int languageCount = Enum.GetNames ( typeof ( Language ) ).Length;
        stringTable = new Dictionary<string , string> [languageCount];
        for ( int i = 0; i < languageCount; i++ )
        {
            stringTable [i] = new Dictionary<string , string> ();
        }

        for ( int i = 1; i < line.Length; i++ )
        {
            string [] cell = line [i].Split ( ',' );
            string key = cell [0];
            for ( int j = 0; j < languageCount; j++ )
            {
                stringTable [j].Add ( key , cell [j + 1] );
            }
            //GameLoopManager.instance.Test ( key );
        }
    }
    public static string Translate ( string key )
    {
        if ( key == "" )
            return "";

        string s = "";
        int language = GameLoopManager.instance.om.optionData.GetLanguage ();
        if ( stringTable [language].TryGetValue ( key , out s ) )
        {
            return s;
        }
        return "Key Not Found";
    }
}