﻿using System;
using System.Collections;
using System.Collections.Generic;
using Steamworks;

public static class Achievement
{
    public enum achievementNames
    {
        Money = 0,
        PayMoney = 1,
        Worker = 2,
        Commodity = 3,
        Expantion = 4,
        Event = 5,
        Actor7 = 6,
        Actor6 = 7,
        Actor1 = 8,
        Actor3 = 9,
        Actor2 = 10,
        Demon = 11,
        Belos = 12,
        Lulu = 13,
        Kassandra = 14,
        Anna = 15,
        MeiHong = 16,
        Ellin = 17,
        Lica = 18,
        Rezia = 19,
        Doroelle = 20,
        Complete = 21,
    }

    public static void SetAchievement ( string name )
    {
        bool check;
        /*if ( !SteamUserStats.GetAchievement ( name , out check ) )
        {
            SteamUserStats.SetAchievement ( name );
            GameLoopManager.instance.Test ( "解鎖成就 : " + name );
            Complete ();
            SteamUserStats.StoreStats ();
        }*/
    }

    public static void Complete ()
    {
        int length = Enum.GetNames ( typeof ( achievementNames ) ).Length;
        for ( int i = 0; i < length - 1; i++ )
        {
            string name = Enum.GetNames ( typeof ( achievementNames ) ).GetValue ( i ).ToString ();
            if ( !SteamUserStats.GetAchievement ( name , out bool check ) )
            {
                return;
            }
        }

        SteamUserStats.SetAchievement ( "Complete" );
        SteamUserStats.StoreStats ();
    }

    public static void ClearAchievement ( string name )
    {
        SteamUserStats.ClearAchievement ( name );
        SteamUserStats.StoreStats ();
    }
    
    public static void ClearAllAchievement ()
    {
        int length = Enum.GetNames ( typeof ( achievementNames ) ).Length;
        for ( int i = 0; i < length; i++ )
        {
            string name = Enum.GetNames ( typeof ( achievementNames ) ).GetValue ( i ).ToString ();
            ClearAchievement ( name );
        }
    }

}
