﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextScrollView : MonoBehaviour
{
    public ScrollRect scrollRect;

    public void ChangeWindowHeight ( RectTransform rect , Text text , float height )
    {
        float scale = text.transform.localScale.y;
        Vector2 size = rect.rect.size;
        rect.sizeDelta = new Vector2 ( size.x , height * scale );
    }

    public void ChangeScrollSensitivity ( Text text , float height )
    {
        int length = text.text.Split ('\n').Length;
        scrollRect.scrollSensitivity = height / length;
    }
}