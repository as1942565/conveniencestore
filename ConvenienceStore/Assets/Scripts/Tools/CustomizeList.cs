﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

namespace Tools
{
    [System.Serializable]
    public class BoolList : CustomizeList<bool>
    {
        public BoolList () : base ()
        {
        }
        public BoolList ( bool [] data ) : base ( data )
        {
        }
    }
    [System.Serializable]
    public class StringList : CustomizeList<string>
    {
        public StringList () : base ()
        {
        }
        public StringList ( string [] data ) : base ( data )
        {
        }
    }
    [System.Serializable]
    public class IntList : CustomizeList<int>
    {
        public IntList () : base ()
        {
        }
        public IntList ( int [] data ) : base ( data )
        {
        }
    }
    [System.Serializable]
    public class FloatList : CustomizeList<float>
    {
        public FloatList () : base ()
        {
        }
        public FloatList ( float [] data ) : base ( data )
        {
        }
    }
    [System.Serializable]
    public class ButtonList : CustomizeList<Button>
    {
        public ButtonList () : base ()
        {
        }
        public ButtonList ( Button [] data ) : base ( data )
        {
        }
    }
    [System.Serializable]
    public class ActivityScheduleList : CustomizeList<ActivityScheduale.Scheduale>
    {
        public ActivityScheduleList () : base ()
        {
        }
        public ActivityScheduleList ( ActivityScheduale.Scheduale [] data ) : base ( data )
        {
        }
    }
    [System.Serializable]
    public class CustomizeList<T>
    {
        [SerializeField]
        protected List<T> d;
        public virtual void Add ( T element )
        {
            d.Add ( element );
        }
        public virtual void Remove ( T element )
        {
            d.Remove ( element );
        }
        public virtual void Clear ()
        {
            d.Clear ();
        }

        public CustomizeList ()
        {
            d = new List<T> ();
        }

        public CustomizeList ( T [] data ) : this ()
        {
            foreach ( T u in data )
            {
                this.d.Add ( u );
            }
        }

        public int Count => d.Count;

        public virtual T this [int index]
        {
            get => d [index];

            set => d [index] = value;
        }

        public virtual List<T> ToList ()
        {
            return d;
        }
        public virtual T [] ToArray ()
        {
            return d.ToArray ();
        }
        public virtual int IndexOf ( T index )
        {
            return d.IndexOf ( index );
        }
    }

    public static class ListExtension
    {
        public static void Shuffle<T> ( this IList<T> list )
        {
            Random rng = new Random ( UnityEngine.Random.Range ( int.MinValue , int.MaxValue ) );
            int n = list.Count;
            while ( n > 1 )
            {
                n--;
                int k = rng.Next ( n + 1 );
                T value = list [k];
                list [k] = list [n];
                list [n] = value;
            }
        }
    }

}