﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class FindExtenstion
{
    public static List<Button> FindButtonInChild ( Transform root )             //找出子物件的所有按鈕
    {
        List<Button> b = new List<Button> ();
        FindButtonRecursive ( b , root );                                       //一層一層找出所有按鈕
        return b;
    }

    private static void FindButtonRecursive ( List<Button> b , Transform root ) //一層一層找出所有按鈕
    {
        Button tmp = root.GetComponent<Button> ();                              //找子物件之前先確認自己是不是Button
        if ( tmp != null )                                                      //如果是的話
        {
            b.Add ( tmp );                                                      //就加入陣列
        }
        for ( int i = 0; i < root.childCount; i++ )                             //搜尋所有子物件
        {
            FindButtonRecursive ( b , root.GetChild ( i ) );                    //找出子物件的按鈕
        }
    }
    public static UI.BuildHireWorker FindBuildHireWorkerInChild ( Transform root )           //找出子物件的所有按鈕
    {
        UI.BuildHireWorker b = null;
        FindButtonRecursive ( ref b , root );                                       //一層一層找出所有按鈕
        return b;
    }

    private static void FindButtonRecursive ( ref UI.BuildHireWorker b , Transform root ) //一層一層找出所有按鈕
    {
        UI.BuildHireWorker tmp = root.GetComponent<UI.BuildHireWorker> ();                              //找子物件之前先確認自己是不是Button
        if ( tmp != null )                                                      //如果是的話
        {
            b = tmp;
            return;
        }
        for ( int i = 0; i < root.childCount; i++ )                             //搜尋所有子物件
        {
            FindButtonRecursive ( ref b , root.GetChild ( i ) );                    //找出子物件的按鈕
        }
    }
}
