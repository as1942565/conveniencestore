﻿using UnityEngine;
using System.Collections.Generic;

public static class SpritePath
{
    public static Sprite None = Resources.Load<Sprite> ( "Sprites/Common/Null" );
    public static Sprite [] CommodityRank = Resources.LoadAll<Sprite> ( "Sprites/Common/GameUI/Commodity/Commodity/Rank" );
    public static Sprite [] Commodity = Resources.LoadAll<Sprite> ( "Sprites/Common/GameUI/Commodity/Commodity" );
    public static Sprite [] CommodityType = Resources.LoadAll<Sprite> ( "Sprites/Common/GameUI/Commodity/Commodity/TypeIcon" );
    public static Sprite [] CommodityPopular_W = Resources.LoadAll<Sprite> ( "Sprites/Common/GameUI/Commodity/Commodity/Popular_W" );
    public static Sprite [] CommodityPopular_R = Resources.LoadAll<Sprite> ( "Sprites/Common/GameUI/Commodity/Commodity/Popular_R" );
    public static Sprite [] CommodityPopular_B = Resources.LoadAll<Sprite> ( "Sprites/Common/GameUI/Commodity/Commodity/Popular_B" );
    public static Sprite [] Page = Resources.LoadAll<Sprite> ( "Sprites/Common/Button/Page/Page" );
    public static Sprite [] Page2 = Resources.LoadAll<Sprite> ( "Sprites/Common/Button/Page/Page2" );
    public static Sprite [] TargetPage = Resources.LoadAll<Sprite> ( "Sprites/Common/Button/Page/TargetPage" );
    public static Sprite [] TargetPage2 = Resources.LoadAll<Sprite> ( "Sprites/Common/Button/Page/TargetPage2" );
    public static Sprite [] ActorIcon = Resources.LoadAll<Sprite> ( "Sprites/Common/GameUI/Activity/Activity/Mission/Worker/Actor" );

    public static Sprite OKBtn ()
    {
        return Resources.Load<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + "Button/OKBtn" );
    }
    public static Sprite OKBtn2 ()
    {
        return Resources.Load<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + "Button/OKBtn2" );
    }
    public static Sprite OKBtn3 ()
    {
        return Resources.Load<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + "Button/OKBtn3" );
    }
    public static Sprite OKBtn3_Lock ()
    {
        return Resources.Load<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + "Button/OKBtn3_Lock" );
    }
    public static Sprite [] OKBtn4 ()
    {
        return Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + "Button/OKBtn4" );
    }
    public static Sprite [] OKBtn5 ()
    {
        return Resources.LoadAll<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + "Button/OKBtn5" );
    }
    public static Sprite BackBtn ()
    {
        return Resources.Load<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + "Button/BackBtn" );
    }
    public static Sprite BackBtn2 ()
    {
        return Resources.Load<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + "Button/BackBtn2" );
    }
    public static Sprite BackBtn3 ()
    {
        return Resources.Load<Sprite> ( "Sprites/" + GameLoopManager.instance.om.optionData.GetLanguageStr () + "Button/BackBtn3" );
    }
}