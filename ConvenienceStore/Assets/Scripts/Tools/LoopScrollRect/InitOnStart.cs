﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace SG
{
    [RequireComponent ( typeof ( UnityEngine.UI.LoopScrollRect ) )]
    [DisallowMultipleComponent]
    public class InitOnStart : MonoBehaviour
    {
        private void Awake ()
        {
            ls = GetComponent<LoopScrollRect> ();
        }

        public int totalCount = -1;
        LoopScrollRect ls;
        [SerializeField]
        bool RefillFromEnd = false;
        public void UpdateCount ( int count )
        {
            ls.totalCount = count;
            if ( RefillFromEnd )
            {
                ls.RefillCellsFromEnd ();
            }
            else
            {
                ls.RefillCells ();
            }
        }
    }
}