﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ScreenShot : MonoBehaviour
{
    public static ScreenShot self;
    void Awake ()
    {
        self = this;
        CreateScreenshot ( 0.5f );
    }

    [HideInInspector]
    public Texture2D texture2D;

    public void CreateScreenshot ( float delay )
    {
        StartCoroutine ( TakeScreenShot ( delay ) );
    }

    IEnumerator TakeScreenShot ( float delay )
    {
        float cd = Time.time + delay;
        //while ( Time.time < cd )
        while ( true )
        {
            if ( Input.GetMouseButton ( 0 ) )
            {
                break;
            }
            yield return null;
        }
        yield return new WaitForEndOfFrame ();

        texture2D = ScreenCapture.CaptureScreenshotAsTexture ();

    }

    public void SaveScreenShot ( Image Screenshot , string index )
    {
        if ( texture2D == null )
        {
            if ( Screenshot != null )
                Screenshot.sprite = SpritePath.None;
            return;
        }
        byte [] _byte = texture2D.EncodeToPNG ();
        string subFolderPath2 = SaveLoadManager.GetPath ();
        string path = Path.Combine ( subFolderPath2 , $"ScreenShot{index}.png" );

        File.WriteAllBytes ( path , _byte );
        if ( Screenshot != null )
            StartCoroutine ( FindScreenShot ( Screenshot , index ) );
    }

    public IEnumerator FindScreenShot ( Image Screenshot , string index )
    {
        string subFolderPath2 = SaveLoadManager.GetPath ();
        string path = Path.Combine ( subFolderPath2 , $"ScreenShot{index}.png" );
        if ( File.Exists ( path ) )
        {
            WWW www = new WWW ( "file://" + path );
            yield return www;
            Texture2D texture2D = new Texture2D ( 1 , 1 );
            www.LoadImageIntoTexture ( texture2D );
            Sprite spr = Sprite.Create ( texture2D , new Rect ( 0 , 0 , texture2D.width , texture2D.height ) , new Vector2 ( 0.5f , 0.5f ) , 100.0f );
            Screenshot.sprite = spr;
        }
        else
            Screenshot.sprite = SpritePath.None;
    }
}
