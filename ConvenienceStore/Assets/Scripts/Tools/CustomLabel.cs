﻿namespace UnityEngine
{
    public class CustomLabelAttribute : PropertyAttribute
    {
        public string Label;
        public CustomLabelAttribute(string Label) {
            this.Label = Label;
        }
    }
}