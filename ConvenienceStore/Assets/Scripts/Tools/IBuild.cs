﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface IBuild 
{
    void Open ();
    void Close ();
    void Destroy ();
}

public abstract class Builder :MonoBehaviour, IBuild
{
    public abstract void Close ();

    public abstract void Destroy ();

    public abstract void Open ();
}