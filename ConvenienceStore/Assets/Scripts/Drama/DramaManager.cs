﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DramaManager : MonoBehaviour
{
    public static DramaManager self;
    private void Awake ()
    {
        self = this;
        TalkBoxManager.self.TalkBox.gameObject.SetActive ( false );
    }

    public GameObject ClearMask;

    public Image Log;
    public Sprite Enabled_Log;
    public Sprite Disnabled_Log;

    public Image Auto;
    public Sprite Enabled_Auto;
    public Sprite Disnabled_Auto;

    public Image Skip;
    public Sprite Enabled_Skip;
    public Sprite Disnabled_Skip;

    private bool closeUI;

    private void Start ()
    {
        ClearMask.SetActive ( false );
    }

    private void Update ()
    {
        if ( Input.GetKeyDown ( KeyCode.LeftControl ) )
        {
            StorySceneManager.skip = true;
            Skip.sprite = Enabled_Skip;
        }

        if ( Input.GetKeyUp ( KeyCode.LeftControl ) )
        {
            StorySceneManager.skip = false;
            Skip.sprite = Disnabled_Skip;
        }
        if ( StorySceneManager.log == true )
            StorySceneManager.skip = false;
    }

    public void ChangeAuto ()
    {
        if ( StorySceneManager.log == true )
            return;
        StorySceneManager.skip = false;
        PlayButtonAudio.self.PlayDramaClick ();
        StorySceneManager.auto = !StorySceneManager.auto;
        ClearMask.SetActive ( StorySceneManager.auto );
        Skip.sprite = Disnabled_Skip;
        Auto.sprite = StorySceneManager.auto ? Enabled_Auto : Disnabled_Auto;
    }
    public void ChangeSkip ()
    {
        if ( StorySceneManager.log == true )
            return;
        StorySceneManager.auto = false;
        PlayButtonAudio.self.PlayDramaClick ();
        StorySceneManager.skip = !StorySceneManager.skip;
        ClearMask.SetActive ( StorySceneManager.skip );
        Auto.sprite = Disnabled_Auto;
        Skip.sprite = StorySceneManager.skip ? Enabled_Skip : Disnabled_Skip;
    }
    public void OpenLog ()
    {
        if ( StorySceneManager.log == true )
        {
            CloseLog ();
            return;
        }
        StorySceneManager.auto = false;
        StorySceneManager.skip = false;
        StorySceneManager.log = true;
        StoryLogManager.self.Open ();
        Log.sprite = Enabled_Log;
        Skip.sprite = Disnabled_Skip;
        Auto.sprite = Disnabled_Auto;
    }
    public void OpenMenu ()
    {
        StorySceneManager.log = true;
        UI.OptionManager.self.Open ();
    }
    public void CloseLog ()
    {
        StorySceneManager.log = false;
        StoryLogManager.self.Close ();
        Log.sprite = Disnabled_Log;
    }

    public void CancelSkip ()
    {
        StorySceneManager.skip = false;
        Skip.sprite = Disnabled_Skip;
    }

    public void ClearAutoAndSkip ()
    {
        StorySceneManager.auto = false;
        StorySceneManager.skip = false;
        ClearMask.SetActive ( false );
        Skip.sprite = Disnabled_Skip;
        Auto.sprite = Disnabled_Auto;
    }

    public void Close ()
    {
        TalkBoxManager.self.closeUI = true;
        TalkBoxManager.self.TalkBox.gameObject.SetActive ( false );
    }
}
