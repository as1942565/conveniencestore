﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace DramaEvent
{
    public class CheckEvent : BaseDramaEvent
    {
        public string [] events;
        public int line;

        public bool clearActor = false;

        public CheckEvent () : this ( new string [] { } , -1 )
        {

        }

        public CheckEvent ( string [] events , int line )
        {
            this.events = events;
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            string eventName = events [2].Replace ( "{" , "" ).Replace ( "}" , "" );

            Type t = null;
            if ( eventName == "Wait" )
            {
                if ( StorySceneManager.skip == true )
                    yield break;
                float w;
                if ( float.TryParse ( events[12] , out w ) )
                    yield return new WaitForSeconds ( w );
                else
                {
                    Debug.LogError ( eventName[12] + "不是浮點數" );
                    yield return new WaitForSeconds ( 0.5f );
                }
                yield break;
            }
            else
                t = Type.GetType ( "DramaEvent." + eventName + "Event" );
            if ( t == null )                                            
            {
                Debug.LogError ( line + ": " + eventName + "不是事件名稱" );
                yield break;
            }
            BaseDramaEvent e = (BaseDramaEvent)Activator.CreateInstance ( t );
            if ( line == -1 )
                Debug.LogError ( "沒有填入行數" );
            e.SetParam ( events , line );
            yield return e.Play ();
        }
    }
}