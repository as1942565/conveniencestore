﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseDramaEvent
{
    private string [] Param;

    public void SetParam ( string [] param )
    {
        Param = param;
    }

    public void DoEvent ()
    {
        DoEvent ( Param );
    }

    public virtual void DoEvent ( string [] param )
    {
    }
}
