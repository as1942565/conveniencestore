﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class PlayCustomEffectEvent : BaseDramaEvent
    {
        public string name;
        public bool loop;
        public string type;
        public int line;

        private const string playerPath = "Prefabs/Drama/EffectPlayer";
        private const string clipPath = "Audios/Effect/";

        public override void SetParam ( string [] param , int line )
        {
            this.line = line;
            name = param [12];

            if ( !bool.TryParse ( param [13] , out loop ) )
            {
                loop = false;
                Debug.LogError ( line + ": " + param [13] + "不是布林值" );
            }

            type = param [14];
        }

        public override IEnumerator Play ()
        {
            EffectPlayer player = GameObject.Instantiate ( Resources.Load<EffectPlayer> ( playerPath ) , StoryManager.self.audioSpace );
            player.gameObject.name = name;
            AudioClip clip = Resources.Load<AudioClip> ( clipPath + name );
            if ( clip == null )
                Debug.LogError ( line + ": " + clipPath + name + "   為錯誤路徑" );
            if ( type == "system" )
                player.PlayEffect ( clip , loop , EffectPlayer.AudioType.system );
            else
                player.PlayEffect ( clip , loop , EffectPlayer.AudioType.story );

            yield return null; 
        }
    }
}
