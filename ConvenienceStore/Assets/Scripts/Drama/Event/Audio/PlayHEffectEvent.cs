﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class PlayHEffectEvent : BaseDramaEvent
    {
        public string name;
        public bool loop;
        public int line;

        private const string playerPath = "Prefabs/Drama/EffectPlayer";
        private const string clipPath = "Audios/Effect/Drama/";

        public override void SetParam ( string [] param , int line )
        {
            name = param [12];
            if ( !bool.TryParse ( param [13] , out loop ) )
            {
                loop = false;
                Debug.LogError ( line + ": " + param [13] + "不是布林值" );
            }
        }

        public override IEnumerator Play ()
        {
            EffectPlayer player = GameObject.Instantiate ( Resources.Load<EffectPlayer> ( playerPath ) , StoryManager.self.ssm.AudioSpace );
            player.gameObject.name = name;
            AudioClip clip = Resources.Load<AudioClip> ( clipPath + name );
            if ( clip == null )
                Debug.LogError ( line + ": " + clipPath + name + "   為錯誤路徑" );
            player.PlayEffect ( clip , loop , EffectPlayer.AudioType.H );
            yield return null; 
        }
    }
}

