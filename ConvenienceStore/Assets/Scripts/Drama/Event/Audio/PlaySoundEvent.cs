﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class PlaySoundEvent : BaseDramaEvent
    {
        public string soundType;
        public string soundID;

        private const string path = "Audios/Sound/H/";

        public PlaySoundEvent () : this ( "" , "" )
        {

        }
        public PlaySoundEvent ( string soundName , string soundID )
        {
            this.soundType = soundName;
            this.soundID = soundID;
        }
        public override IEnumerator Play ()
        {
            EffectManager.self.PlaySound ( path + soundType + "/" , soundID , false , EffectPlayer.AudioType.sound );
            yield return null;
        }
    }
}