﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class FadeBGMEvent : BaseDramaEvent
    {
        public float vol;
        public float duration;
        public int line;

        public FadeBGMEvent () : this ( 0 , 0 )
        {

        }

        public FadeBGMEvent ( float vol , float duration )
        {
            this.vol = vol;
            this.duration = duration;
            line = -1;
        }

        public override void SetParam ( string [] param , int line )
        {
            this.line = line;
            if ( !float.TryParse ( param [12] , out vol ) )
            {
                vol = 1;
                Debug.LogError ( line + ": " + param [12] + "不是浮點數" );
            }

            if ( !float.TryParse ( param [13] , out duration ) )
            {
                duration = 0.5f;
                Debug.LogError ( line + ": " + param[13] + "不是浮點數" );
            }
        }

        public override IEnumerator Play ()
        {
            if ( StorySceneManager.skip == true )
                GameLoopManager.instance.om.audioData.SetStoryVol ( vol , 0 );
            else
                GameLoopManager.instance.om.audioData.SetStoryVol ( vol , duration );
            BGMManager.self.tempPath = "";
            
            yield return null;
        }
    }
}