﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class PlayCustomBGMEvent : BaseDramaEvent
    {
        public string path;
        public float vol;
        public int line;

        private const string BGMPath = "Audios/BGM/";

        public override void SetParam ( string [] param , int line )
        {
            this.line = line;
            path = param [12];
            if ( !float.TryParse ( param [13] , out vol ) )
            {
                vol = 1f;
                if ( param [13] != "1" )
                    Debug.LogError ( line + ": " + param [13] + "不是浮點數" );
            }
        }

        public override IEnumerator Play ()
        {
            float bgm = GameLoopManager.instance.om.audioData.GetBGM () / 100f;
            GameLoopManager.instance.om.audioData.SetStoryVol ( vol );
            BGMManager.self.PlayBGM ( BGMPath + path , GameLoopManager.instance.om.audioData );

            yield return null;
        }
    }
}