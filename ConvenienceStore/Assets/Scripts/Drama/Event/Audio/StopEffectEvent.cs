﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class StopEffectEvent : BaseDramaEvent
    {
        public string name;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            name = param [12];
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            EffectPlayer [] player = StoryManager.self.ssm.AudioSpace.GetComponentsInChildren<EffectPlayer> ();
            for ( int i = 0; i < player.Length; i++ )
            {
                if ( player[i].gameObject.name == name )
                {
                    GameObject.Destroy ( player[i].gameObject );
                }
            }
            yield return null;
        }
    }
}