﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class DisplayMemoryEvent : BaseDramaEvent
    {
        public bool enabled;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            if ( !bool.TryParse ( param [12] , out enabled ) )
            {
                Debug.LogError ( $"{line} : {param [12]}不是布林值" );
                enabled = false;
            }
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            MemoryOverlay memoryOverlay = Camera.main.GetComponent<MemoryOverlay> ();
            if ( memoryOverlay == null )
            {
                Debug.LogError ( "找不到回憶效果腳本" );
                yield break;
            }
            memoryOverlay.enabled = enabled;
            yield return null;
        }
    }
}