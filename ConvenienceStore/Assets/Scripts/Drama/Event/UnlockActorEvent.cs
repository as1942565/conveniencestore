﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class UnlockActorEvent : BaseDramaEvent
    {
        public int actorID;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            if ( !int.TryParse ( param [12] , out actorID ) )
            {
                Debug.LogError ( $"{line} : {param [12]} 不是員工編號" );
                actorID = -1;
            }
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            if ( actorID == -1 )
                yield break;
            GameLoopManager.instance.om.unlockActorData.SetUnlock ( actorID , true );
            SaveLoadManager.OnlyDataSave ( GameLoopManager.instance.om );
            yield return null;
        }
    }
}