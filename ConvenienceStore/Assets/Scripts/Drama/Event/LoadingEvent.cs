﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class LoadingEvent : BaseDramaEvent
    {
        //預設 1 , 2.5 , 0.5 
        //0秒後淡出 0.5(FadeTime)秒後全黑 
        //2.5(WaitTime)秒後執行下一段
        //0.5(FadeTime) + 1(BlackTime)秒後淡入 2(FadeTime)秒後全白  
        public float BlackTime;                     //黑畫面要黑幾秒
        public float WaitTime;                      //等待時間(經過該時間後跳下一段)
        public float FadeTime;                      //淡入淡出時間
        public int line;

        public LoadingEvent () : this ( 0 , 0 , 0 )
        {
        }

        public LoadingEvent ( float BlackTime , float WaitTime , float FadeTime )
        {
            this.BlackTime = BlackTime;
            this.WaitTime = WaitTime;
            this.FadeTime = FadeTime;
            line = -1;
        }

        public override void SetParam ( string [] param , int line )
        {
            this.line = line;
            if ( !float.TryParse ( param [12] , out BlackTime ) )
            {
                BlackTime = 1;
                Debug.LogError ( line + ": " + param [12] + "不是浮點數" );
            }
            if ( !float.TryParse ( param [13] , out WaitTime ) )
            {
                WaitTime = 2.5f;
                Debug.LogError ( line + ": " + param [13] + "不是浮點數" );
            }
            if ( !float.TryParse ( param [14] , out FadeTime  ) )
            {
                FadeTime = 0.5f;
                Debug.LogError ( line + ": " + param [14] + "不是浮點數" );
            }
        }

        public override IEnumerator Play ()
        {
            if ( StorySceneManager.skip == true )
                yield break;
            yield return new DeleteTextEvent ().Play ();
            yield return new DeleteAllActorEvent ().Play ();
            TalkBoxManager.self.ActorNameBox.SetActive ( false );

            LoadingScene.self.Transitions ( FadeTime , BlackTime , false );
            yield return new WaitForSeconds ( WaitTime );

            yield return null;
        }
    }
}
