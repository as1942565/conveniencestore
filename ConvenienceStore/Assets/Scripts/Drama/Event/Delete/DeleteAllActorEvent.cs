﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace DramaEvent
{
    public class DeleteAllActorEvent : BaseDramaEvent
    {
        public override IEnumerator Play ()
        {
            TalkBoxManager.self.ActorNameBox.SetActive ( false );

            Actor [] actor = StoryManager.self.ssm.Actor.GetComponentsInChildren<Actor> ();
            for ( int i = 0; i < actor.Length; i++ )
            {
                actor [i].EmoticoncBG.gameObject.SetActive ( false );
                actor [i].GetComponent<Image> ().DOFade ( 0 , 0.5f );
                GameObject.Destroy ( actor[i].gameObject , 0.5f );
            }
            StoryManager.self.ClearTemp ();
            yield return new DeleteTextEvent ().Play ();
            yield return new WaitForSeconds ( 0.5f );
            yield return null;
        }
    }
}