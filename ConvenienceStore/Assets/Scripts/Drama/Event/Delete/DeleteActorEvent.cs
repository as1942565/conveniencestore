﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace DramaEvent
{
    public class DeleteActorEvent : BaseDramaEvent
    {
        public string actorName;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            actorName = param [12];
            this.line = line;
        }
        public override IEnumerator Play ()
        {
            Actor [] actor = StoryManager.self.ssm.Actor.GetComponentsInChildren<Actor> ();
            for ( int i = 0; i < actor.Length; i++ )
            {
                if ( actor [i].actorName == actorName )
                {
                    actor [i].EmoticoncBG.gameObject.SetActive ( false );
                    actor [i].GetComponent<Image> ().DOFade ( 0 , 0.5f );
                    GameObject.Destroy ( actor[i].gameObject , 0.5f );
                    break;
                }
            }
            StoryManager.self.ClearTemp ();
            yield return new DeleteTextEvent ().Play ();
            yield return new WaitForSeconds ( 0.5f );
            yield return null;
        }
    }
}