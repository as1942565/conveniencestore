﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class DeleteTextEvent : BaseDramaEvent
    {
        public override IEnumerator Play ()
        {
            TalkBoxManager.self.StoryText.text = "";
            TalkBoxManager.self.ActorName.text = "";
            TalkBoxManager.self.ActorNameBox.SetActive ( false );
            yield return null;
        }
    }
}
