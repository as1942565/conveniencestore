﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

namespace DramaEvent
{
    public class WatchVideoEvent : BaseDramaEvent
    {
        private string videoName;
        private int line;

        private const string path = "Video/";

        public override void SetParam ( string [] param , int line )
        {
            videoName = param [12];
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            if ( GameLoopManager.instance.GetSteam () == true )
                yield break;
            if ( GameLoopManager.instance.AllAge == true )
                yield break;

            VideoPlayer videoPlayer = StoryManager.self.videoPlayer;
            float volume = GameLoopManager.instance.om.audioData.GetVideo () / 100f;
            Debug.Log ( volume );
            for ( int i = 0; i <= videoPlayer.audioTrackCount; i++ )
                videoPlayer.SetDirectAudioVolume ( (ushort)i , volume );
            VideoClip clip = Resources.Load<VideoClip> ( path + videoName );
            if ( clip == null )
            {
                Debug.LogError ( path + videoName + "路徑不正確" );
                yield break;
            }
            videoPlayer.gameObject.SetActive ( true );
            videoPlayer.clip = clip;
            videoPlayer.Prepare ();
            while ( videoPlayer.isPrepared == false )
            {
                yield return null;
            }
            videoPlayer.Play ();

            while ( videoPlayer.isPlaying )
            {
                yield return null;
            }
            videoPlayer.gameObject.SetActive ( false );
            yield return null;
        }
    }
}