﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace DramaEvent
{
    public class MoveActorEvent : BaseDramaEvent
    {
        public string actorName;
        public string endPos;
        public string walkTime;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            actorName = param [1];
            endPos = param [4];
            walkTime = param [5];
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            Transform actorTransform = StoryManager.self.ssm.Actor.Find ( actorName );
            if ( actorTransform == null )
            {
                Debug.LogError ( line + ": " + "場景中找不到" + actorName );
                yield break;
            }

            float end = 0f;
            float duration = 0f;
            if ( float.TryParse ( endPos , out end ) )
            {
                if ( float.TryParse ( walkTime , out duration ) )
                {
                    actorTransform.transform.DOLocalMoveX ( end , duration ).SetEase ( Ease.Linear );
                }
                else
                {
                    Debug.LogError ( line + ": " + walkTime + "不是浮點數" );
                }
            }
            yield return null;
        }
    }
}