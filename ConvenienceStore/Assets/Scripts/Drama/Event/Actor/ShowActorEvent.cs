﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class ShowActorEvent : BaseDramaEvent
    {
        public Transform actor;
        public float heigth;

        private Vector2 scale = new Vector2 ( 1.75f , 1.75f );

        public ShowActorEvent () : this ( null , 0 )
        {

        }
        public ShowActorEvent ( Transform actor , float heigth )
        {
            this.actor = actor;
            this.heigth = heigth;
        }

        public override IEnumerator Play ()
        {
            float newScaleX = 1;
            if ( actor.transform.localScale.x < 0 )
                newScaleX = -1;
            actor.transform.localScale = new Vector3 ( Mathf.Abs ( scale.x ) * newScaleX , scale.y , 0 );
            actor.transform.localPosition = new Vector3 ( actor.transform.localPosition.x , -heigth , 0 );

            yield return null;
        }
    }
}