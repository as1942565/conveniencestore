﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DramaEvent
{
    public class UpdateActorEvent : BaseDramaEvent
    {
        public string actorName;
        public string faceID;
        public string clothesName;
        public int line;

        private const string ActorPath = "Sprites/Common/Drama/Actor/";

        public override void SetParam ( string [] param , int line )
        {
            actorName = param [1];
            faceID = param [6];
            clothesName = param [8];
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            Image actor = null;
            Actor [] tempActor = StoryManager.self.ssm.Actor.GetComponentsInChildren<Actor> ();
            for ( int i = 0; i < tempActor.Length; i++ )
            {
                if ( tempActor [i].actorName == actorName )
                {
                    actor = tempActor [i].GetComponent<Image> ();
                    break;
                }
            }
            if ( actor == null )
            {
                Debug.LogError ( line + ": " + actorName + "找不到該角色" );
                yield break;
            }

            if ( faceID.Length == 1 )
                faceID = "0" + faceID;

            string path = ActorPath + EID () + CID () + faceID;
            Sprite spr = Resources.Load<Sprite> ( path );
            if ( GameLoopManager.instance.GetSteam () == true && actorName == "笭伊姍" )
                spr = SpritePath.None;
            if ( spr == null )
            {
                Debug.LogError ( line + ": " + path + "路徑不正確" );
                yield break;
            }
            actor.sprite = spr;
            yield return null;
        }
        string [] Employee = new string [8] { "顏無菁" , "夜蓮心" , "笭伊姍" , "端木壹" , "司馬小仙" , "顏紅玉" , "幽香" , "林原琳" };
        string [] EmployeeE = new string [8] { "Gin Yen" , "Lycianna Yeh" , "Elissa Lin" , "Manager" , "Xiaoxian Sima" , "Jade Yen" , "Yusra" , "Lauryn Lin" };
        string [] EmployeeC = new string [8] { "颜无菁" , "夜莲心" , "笭伊姗" , "端木壹" , "司马小仙" , "颜红玉" , "幽香" , "林原琳" };
        string [] EmployeeK = new string [8] { "진 옌" , "리시아나 예" , "엘리사 린" , "도미닉" , "샤오셴 시마" , "제이드 옌" , "유스라" , "라우린 린" };
        string [] EmployeeR = new string [8] { "Янь Уцзин" , "Е Ляньсинь" , "Элисса Линь" , "Доминик" , "Сима Сяосянь" , "Янь Хунъюй" , "Ю Сян" , "Линь Юаньлин" };

        string EID ()
        {
            for ( int i = 0; i < Employee.Length; i++ )
            {
                if ( Employee [i] == actorName )
                    return i.ToString () + "/";
            }
            for ( int i = 0; i < EmployeeE.Length; i++ )
            {
                if ( EmployeeE [i] == actorName )
                {
                    return i.ToString () + "/";
                }
            }
            for ( int i = 0; i < EmployeeC.Length; i++ )
            {
                if ( EmployeeC [i] == actorName )
                {
                    return i.ToString () + "/";
                }
            }
            for ( int i = 0; i < EmployeeK.Length; i++ )
            {
                if ( EmployeeK [i] == actorName )
                {
                    return i.ToString () + "/";
                }
            }
            for ( int i = 0; i < EmployeeR.Length; i++ )
            {
                if ( EmployeeR [i] == actorName )
                {
                    return i.ToString () + "/";
                }
            }
            return "EX/";
        }

        string [] Clothes = new string[10] { "制服" , "cos" , "內衣" , "半制服" , "私服" , "裸" , "麋鹿" , "住院" , "旗袍" , "半裸" };
        string CID ()
        {
            for ( int i = 0; i < Clothes.Length; i++ )
            {
                if ( Clothes [i] == clothesName )
                    return i.ToString () + "/Actor";
            }
            if ( clothesName == "西裝" )
                return "1/Actor";
            return "";
        }
    }
}