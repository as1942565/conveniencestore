﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace DramaEvent
{
    public class FadeActorEvent : BaseDramaEvent
    {
        public string actorName;
        public string fade;
        public string duration;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            actorName = param [12];
            fade = param [13];
            duration = param [14];
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            Image actor = null;
            Actor [] tempActor = StoryManager.self.ssm.Actor.GetComponentsInChildren<Actor> ();
            for ( int i = 0; i < tempActor.Length; i++ )
            {
                if ( tempActor [i].actorName == actorName )
                {
                    actor = tempActor [i].GetComponent<Image> ();
                    break;
                }
            }
            if ( actor == null )
            {
                Debug.LogError ( line + ": " + actorName + "找不到該角色" );
                yield break;
            }
            
            float f;
            float d;
            if ( float.TryParse ( fade , out f ) )
            {
                if ( StorySceneManager.skip == true )
                {
                    actor.DOFade ( f , 0 );
                    yield break;
                }
                if ( float.TryParse ( duration , out d ) )
                    actor.DOFade ( f , d );
            }
            yield return null;
        }
    }
}