﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class CancelHireWorkerEvent : BaseDramaEvent
    {
        public override IEnumerator Play ()
        {
            GameLoopManager.instance.pm.businessUnlock.SetUnlock ( 11 , 0 , false );
            yield return null;
        }
    }
}
