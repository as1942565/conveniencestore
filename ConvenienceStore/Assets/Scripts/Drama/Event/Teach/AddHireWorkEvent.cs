﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class AddHireWorkEvent : BaseDramaEvent
    {
        public override IEnumerator Play ()
        {
            GameLoopManager.instance.pm.businessUnlock.SetUnlock ( 11 , 0 , true );
            yield return null;
        }
    }
}