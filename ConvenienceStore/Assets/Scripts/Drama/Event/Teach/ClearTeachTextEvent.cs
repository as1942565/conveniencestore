﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class ClearTeachTextEvent : BaseDramaEvent
    {
        public override IEnumerator Play ()
        {
            StoryManager.self.tsm.TeachText.text = "";
            yield return null;
        }
    }
}