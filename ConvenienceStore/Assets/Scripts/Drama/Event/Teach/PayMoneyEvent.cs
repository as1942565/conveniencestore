﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class PayMoneyEvent : BaseDramaEvent
    {
        public override IEnumerator Play ()
        {
            PlayerDataManager pm = GameLoopManager.instance.pm;

            pm.playerData.AddMoney ( -10000 );
            int totalShift = pm.calender.TotalShift;
            pm.gameLog.AddBusiness ( totalShift , 10000 );
            
            GameLoopManager.instance.nextShift.Business ( 0 );

            yield return null;
        }
    }
}