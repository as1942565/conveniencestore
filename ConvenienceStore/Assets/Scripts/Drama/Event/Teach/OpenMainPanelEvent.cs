﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class OpenMainPanelEvent : BaseDramaEvent
    {
        public int id;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            this.line = line;

            if ( !int.TryParse ( param [12] , out id ) )
            {
                Debug.LogError ( "沒有介面編號" );
                id = -1;
            }
        }

        public override IEnumerator Play ()
        {
            if ( id == -1 )
                yield break;


            UI.MainPanel.self.SwitchPanel ( id );
            yield return null;
        }
    }
}