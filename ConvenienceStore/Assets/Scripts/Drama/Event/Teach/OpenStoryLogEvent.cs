﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class OpenStoryLogEvent : BaseDramaEvent
    {
        public override IEnumerator Play ()
        {
            UI.MainPanel.self.SwitchPanel ( 4 );
            
            yield return null;
        }
    }
}