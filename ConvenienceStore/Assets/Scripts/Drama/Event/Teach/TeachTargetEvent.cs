﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DramaEvent
{
    public class TeachTargetEvent : BaseDramaEvent
    {
        public string path;
        public string nextTeach;
        public int line;

        private Button button;

        [HideInInspector]
        public bool targetBool;

        private const string teachPath = "Prefabs/NextTeach";

        public override void SetParam ( string [] param , int line )
        {
            this.line = line;
            path = param [12];
            nextTeach = param [13];
        }

        public override IEnumerator Play ()
        {
            GameObject obj = GameObject.Find ( path );

            while ( obj == null )
            {
                obj = GameObject.Find ( path );
                yield return null;
            }

            button = obj.GetComponent<Button> ();
            if ( button == null )
            {
                Debug.LogError ( line + ": " + path + "物件上找不到按鈕" );
                yield break;
            }
            button.enabled = true;
            button.onClick.AddListener ( OnClick );
            yield return null;

            if ( nextTeach != null )
            {
                Prefab.NextTeach teach = GameObject.Instantiate ( Resources.Load<Prefab.NextTeach> ( teachPath ) , StoryManager.self.transform );
                teach.DoEvent ( this , nextTeach );
            }
        }

        void OnClick ()
        {
            targetBool = true;
            button.onClick.RemoveListener ( OnClick );
        }
    }
}