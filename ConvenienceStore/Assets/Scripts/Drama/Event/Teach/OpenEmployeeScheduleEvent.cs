﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class OpenEmployeeScheduleEvent : BaseDramaEvent
    {
        public override IEnumerator Play ()
        {
            UI.MainPanel.self.SwitchPanel ( 3 );
            
            yield return null;
        }
    }
}
