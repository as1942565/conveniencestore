﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class SwitchTeachEvent : BaseDramaEvent
    {
        public bool enabled = false;

        public override void SetParam ( string [] param , int line )
        {
            if ( !bool.TryParse ( param [12] , out enabled ) )
            {
                Debug.LogError ( param [12] + "不是布林值" );
                enabled = false;
            }
        }

        public override IEnumerator Play ()
        {
            StoryManager.self.EnableTeach = enabled;
            yield return null;
        }
    }
}