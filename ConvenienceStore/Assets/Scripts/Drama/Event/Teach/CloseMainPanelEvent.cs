﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class CloseMainPanelEvent : BaseDramaEvent
    {
        public override IEnumerator Play ()
        {
            UI.MainPanel.self.Close ();
            yield return null;
        }
    }
}