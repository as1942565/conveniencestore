﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class EnableRestockEvent : BaseDramaEvent
    {
        public override IEnumerator Play ()
        {
            StoryManager.self.tsm.LockRestockBtn = false;
            StoryManager.self.tsm.pass = true;
            yield return null;
        }
    }
}