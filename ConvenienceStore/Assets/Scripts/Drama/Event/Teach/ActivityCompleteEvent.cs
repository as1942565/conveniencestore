﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class ActivityCompleteEvent : BaseDramaEvent
    {
        private const string path = "GameUICanvas/MainAction/ActivityManager/SchedualeManager/BuildMission";

        public override IEnumerator Play ()
        {
            GameObject obj = GameObject.Find ( path );
            if ( obj == null )
            {
                Debug.LogError ( path + "路徑不正確" );
                yield break;
            }
            UI.BuildMission buildMission = obj.GetComponent<UI.BuildMission> ();
            PlayButtonAudio.self.PlayActivityClick ();
            buildMission.OK ();
            yield return null;
        }
    }
}