﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DramaEvent
{
    public class ResearchEvent : BaseDramaEvent
    {
        private int commodityID;
        private int line;

        private const string effectPath = "EffectCanvas";
        private const string ResearchPath = "Prefabs/GameUI/Commodity/Research/ResearchAnim";

        public override void SetParam ( string [] param , int line )
        {
            this.line = line;
            if ( !int.TryParse ( param [12] , out commodityID ) )
            {
                Debug.LogError ( param [12] + "不是商品編號" );
                commodityID = 0;
            }
        }

        public override IEnumerator Play ()
        {
            GameObject effectObj = GameObject.Find ( effectPath );
            if ( effectObj == null )
            {
                Debug.LogError ( line + ": " + effectPath + "路徑不正確" );
                yield break;
            }
            Prefab.ResearchPrefab research = GameObject.Instantiate ( Resources.Load<Prefab.ResearchPrefab> ( ResearchPath ) , effectObj.transform );
            research.commodityID = commodityID;
            int rank = GameLoopManager.instance.gm.commodityList.Data [commodityID].rank;
            int money = GameLoopManager.instance.gm.customData.Data.DevelopMoney [rank];
            GameLoopManager.instance.pm.playerData.AddMoney ( -money );
            yield return null;
        }
    }
}