﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class SetupCommodityEvent : BaseDramaEvent
    {
        public int commodityID;
        public int line;

        private bool auto;

        public override void SetParam ( string [] param , int line )
        {
            if ( param [14] == "?" )
            {
                auto = true;
                return;
            }
            auto = false;

            if ( !int.TryParse ( param [14] , out commodityID ) )
            {
                Debug.LogError ( line + ": " + param [14] + "不是商品編號" );
                commodityID = 0;
            }
        }

        private const string path = "GameUICanvas/MainAction/BuildCommodity/Panel/SetupCommodity/BuildCabinet";

        public override IEnumerator Play ()
        {
            UI.BuildCabinet buildCabinet = GameObject.Find ( path ).GetComponent<UI.BuildCabinet>();
            if ( buildCabinet == null )
            {
                Debug.LogError ( line + ": " + path + "路徑不正確" );
                yield break;
            }

            Prefab.CabinetPrefab [] [] cabinets = buildCabinet.GetCabinets ();
            int type = StoryManager.self.teachSceneManager.setupCommodityType;
            int id = 0;
            if ( auto == false )
                id = StoryManager.self.teachSceneManager.setupCommodityID;
            else
                id = TeachSceneManager.researchID;
            while ( cabinets[type][id].GetCommodityID () != commodityID )
            {
                yield return null;
            }
            StoryManager.self.tsm.pass = false;
            yield return null;
        }
    }
}