﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class ChangeStoreAbilityEvent : BaseDramaEvent
    {
        public int popular;
        public int police;
        public int clean;
        public int money;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            this.line = line;
            if ( !int.TryParse ( param [12] , out popular ) )
            {
                popular = -1;
            }
            
            if ( !int.TryParse ( param [13] , out police ) )
            {
                police = -1;
            }

            if ( !int.TryParse ( param [14] , out clean ) )
            {
                clean = -1;
            }
            if ( !int.TryParse ( param [11] , out money ) )
            {
                money = -1;
            }
        }

        public override IEnumerator Play ()
        {
            PlayerDataManager pm = GameLoopManager.instance.pm;

            bool popularBool = popular != -1;
            bool policeBool = police != -1;
            bool cleanBool = clean != -1;
            bool moneyBool = money != -1;
            while ( popularBool || policeBool || cleanBool || moneyBool )
            {
                if ( popularBool )
                {
                    popularBool = ChangePopular ( pm );
                }
                if ( policeBool )
                {
                    policeBool = ChangePolice ( pm );
                }
                if ( cleanBool )
                {
                    cleanBool = ChangeClean ( pm );
                }
                if ( moneyBool )
                {
                    moneyBool = ChangeMoney ( pm );
                }
                yield return new WaitForSeconds ( 0.01f );
            }
            yield return null;
        }

        bool ChangePopular ( PlayerDataManager pm )
        {
            int oldPopular = pm.playerData.GetPopular ();
            oldPopular--;
            pm.playerData.SetPopular ( oldPopular );
            if ( oldPopular < popular )
            {
                pm.playerData.SetPopular ( popular );
                return false;
            }
            return true;
        }
        bool ChangePolice ( PlayerDataManager pm )
        {
            int oldPolice = pm.playerData.GetPolice ();
            oldPolice--;
            pm.playerData.SetPolice ( oldPolice );
            if ( oldPolice < police )
            {
                pm.playerData.SetPolice ( police );
                return false;
            }
            return true;
        }
        bool ChangeClean ( PlayerDataManager pm )
        {
            int oldClean = pm.playerData.GetClean ();
            oldClean--;
            pm.playerData.SetClean ( oldClean );
            if ( oldClean < clean )
            {
                pm.playerData.SetClean ( clean );
                return false;
            }
            return true;
        }
        bool ChangeMoney ( PlayerDataManager pm )
        {
            int oldMoney = pm.playerData.GetMoney ();
            pm.playerData.AddMoney ( -109 );
            if ( pm.playerData.GetMoney () < money )
            {
                pm.playerData.SetMoney ( money );
                return false;
            }
            return true;
        }
    }
}
