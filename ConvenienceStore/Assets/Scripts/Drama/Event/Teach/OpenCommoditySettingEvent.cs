﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class OpenCommoditySettingEvent : BaseDramaEvent
    {
        public override IEnumerator Play ()
        {
            UI.MainPanel.self.SwitchPanel ( 0 );
            UI.BuildMenuItem.self.OpenRestock ();
            yield return null;
        }
    }
}