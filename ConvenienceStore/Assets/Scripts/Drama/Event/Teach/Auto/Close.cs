﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Auto
{
    public class Close : BaseAuto
    {
        public override IEnumerator Play ()
        {
            UI.MainPanel.self.Close ();
            yield return null;
        }
    }
}