﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Auto
{
    public class Manager
    {
        public IEnumerator Play ( int index , string word )
        {
            Type t = Type.GetType ( "Auto." + word );
            if ( t == null )
            {
                Debug.LogError ( index + ": " + word + "不是自動事件名稱" );
                yield break;
            }
            BaseAuto e = (BaseAuto)Activator.CreateInstance ( t );
            if ( index == -1 )
            {
                Debug.LogWarning ( "沒有填入行數" );
            }
            yield return e.Play ();
        }
    }
}