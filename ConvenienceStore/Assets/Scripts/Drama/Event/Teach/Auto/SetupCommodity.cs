﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Auto
{
    public class SetupCommodity : BaseAuto
    {
        public override IEnumerator Play ()
        {
            UI.MainPanel.self.SwitchPanel ( 0 );
            UI.BuildMenuItem.self.switchType ( 2 );
            yield return null; 
        }
    }
}