﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Auto
{
    public class BaseAuto
    {
        public virtual void SetParam ( string eventName )
        {

        }

        public virtual IEnumerator Play ()
        {
            yield return null;
        }
    }
}