﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Auto
{
    public class RestockReview : BaseAuto
    {
        private const string path = "GameUICanvas/MainAction/BuildCommodity/Panel/Restock/BuildReview";
        public override IEnumerator Play ()
        {
            UI.BuildReview buildReview = GameObject.Find ( path ).GetComponent<UI.BuildReview> ();
            if ( buildReview == null )
            {
                Debug.LogError ( path + "路徑不正確" );
                yield break;
            }
            /*buildReview.RemoveBtn ( 0 );
            yield return new WaitForSeconds ( 0.5f );
            buildReview.RemoveBtn ( 0 );
            yield return new WaitForSeconds ( 0.5f );
            buildReview.AddBtn ( 0 );
            yield return new WaitForSeconds ( 0.5f );
            buildReview.AddBtn ( 0 );*/
            yield return null;
        }
    }
}