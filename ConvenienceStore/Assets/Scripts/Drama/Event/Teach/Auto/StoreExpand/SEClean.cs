﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Auto
{
    public class SEClean : BaseAuto
    {
        public override IEnumerator Play ()
        { 
            UI.BuildStoreExpansion.self.SwitchType ( GameData.StoreExpansion.ExpansionType.clean );
            yield return new DramaEvent.DisableButtonInChildEvent ( "GameUICanvas/MainAction/BuildStoreExpansion/VerticalScroll/Content" ).Play ();
            yield return null;
        }
    }
}