﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Auto
{
    public class Purchase : BaseAuto
    {
        public override IEnumerator Play ()
        {
            UI.MainPanel.self.SwitchPanel ( 0 );
            UI.BuildMenuItem.self.switchType ( 3 );
            yield return null;
        }
    }
}