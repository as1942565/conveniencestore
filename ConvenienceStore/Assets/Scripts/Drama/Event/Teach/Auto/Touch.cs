﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Auto
{
    public class Touch : BaseAuto
    {
        public override IEnumerator Play ()
        {
            StoryManager.self.touchBool = true;
            while ( true )
            {
                if ( Input.GetMouseButtonUp ( 0 ) )
                {
                    break;
                }
                if ( Input.GetKeyDown ( KeyCode.Space ) )
                {
                    break;
                }
#if UNITY_EDITOR
                if ( Input.GetKey ( KeyCode.V ) )
                {
                    break;
                }
#endif
                yield return null;
            }
            yield return new WaitForSeconds ( 0.01f );
            StoryManager.self.touchBool = false;
            yield return null;
        }
    }
}