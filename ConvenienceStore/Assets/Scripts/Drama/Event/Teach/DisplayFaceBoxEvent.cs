﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class DisplayFaceBoxEvent : BaseDramaEvent
    {
        public bool show;
        public int line;

        private const string path = "TeachTalkBox/FaceBox";

        public override void SetParam ( string [] param , int line )
        {
            this.line = line;
            if ( !bool.TryParse ( param[12] , out show ) )
            {
                Debug.LogError ( line + ": " + param[12] + "不是布林值" );
                show = false;
            }
        }
        public override IEnumerator Play ()
        {
            Transform faceBox = StoryManager.self.tsm.transform.Find ( path );
            if ( faceBox == null )
            {
                Debug.LogError ( line + ": " + path + "路徑不正確" );
                yield break;
            }
            faceBox.gameObject.SetActive ( show );
            yield return null;
        }
    }
}
