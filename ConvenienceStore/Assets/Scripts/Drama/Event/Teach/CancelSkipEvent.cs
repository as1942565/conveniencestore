﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class CancelSkipEvent : BaseDramaEvent
    {
        public override IEnumerator Play ()
        {
            DramaManager.self.CancelSkip ();
            yield return null;
        }
    }
}