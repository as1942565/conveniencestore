﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class WaitNextTeachEvent : BaseDramaEvent
    {
        private string targetPath;
        private string targetName;
        private bool wait;

        public override void SetParam ( string [] param , int line )
        {
            targetPath = param [12];
            targetName = param [13];
            if ( !bool.TryParse ( param [14] , out wait ) )
            {
                wait = false;
            }
        }

        public override IEnumerator Play ()
        {
            EnableButtonEvent button = new EnableButtonEvent ();
            button.path = targetPath + targetName;
            button.wait = wait;
            button.targetBool = false;
            yield return button.Play ();

            while ( button.targetBool == false )
            {
                yield return null;
            }

            yield return null;
        }
    }
}