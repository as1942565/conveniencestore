﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class AddInformationEvent : BaseDramaEvent
    {
        public override IEnumerator Play ()
        {
            string log = Localizer.Translate ("今日早班無人值班。" ) + "\n" + Localizer.Translate ( "今日晚班無人值班。" ) + "\n" + Localizer.Translate ( "今日大夜班無人值班。" );
            GameLoopManager.instance.pm.gameLog.AddInformation ( 0 , log );

            yield return null;
        }
    }
}