﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class UpgradeCompleteEvent : BaseDramaEvent
    {
        public string path;
        public string name;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            path = param [12];
            name = param [13];
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            GameObject obj = GameObject.Find ( path + name);
            if ( obj == null )
            {
                Debug.LogError ( line + ": " + path + name + "路徑不正確" );
                yield break;
            }
            obj.GetComponent<Prefab.UpgradePrefab> ().Click ();
            yield return null;
        }
    }
}