﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class ActivitySuccessEvent : BaseDramaEvent
    {
        public override IEnumerator Play ()
        {
            ActivityScheduale.Scheduale [] scheduales = GameLoopManager.instance.pm.activityScheduale.GetSchedules ();
            for ( int i = 0; i < scheduales.Length; i++ )
            {
                scheduales [i].SetSuccessed ( true );
                scheduales [i].SetExcuteShift ( 2 );
            }
            yield return null;
        }
    }
}