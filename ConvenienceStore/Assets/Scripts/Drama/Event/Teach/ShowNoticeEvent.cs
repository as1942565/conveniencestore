﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class ShowNoticeEvent : BaseDramaEvent
    {
        private string text;
        private int line;

        public override void SetParam ( string [] param , int line )
        {
            text = param [12];
            this.line = line;
        }
        public override IEnumerator Play ()
        {
            UI.MessageBox.Notice ( text );
            yield return null;
        }
    }
}