﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class NextShiftEvent : BaseDramaEvent
    {
        public bool next;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            this.line = line;

            if ( !bool.TryParse ( param [12] , out next ) )
            {
                Debug.LogError ( "沒有填布林值" );
                next = false;
            }
        }

        public override IEnumerator Play ()
        {
            GameLoopManager.WaitStoryLog = next;

            yield return null;
        }
    }
}