﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class ShowTeachBoxEvent : BaseDramaEvent
    {
        public bool show;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            if ( !bool.TryParse ( param [12] , out show ) )
            {
                Debug.LogError ( param [12] + "不是布林值" );
                show = false;
            }
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            StoryManager.self.tsm.TalkBox.gameObject.SetActive ( show );
            yield return null;
        }
    }
}