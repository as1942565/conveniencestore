﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class UpgradeEvent : BaseDramaEvent
    {
        private int commodityID;
        private int star;
        private int line;

        private const string effectPath = "EffectCanvas";
        private const string upgradePath = "Prefabs/GameUI/Commodity/Upgrade/Improve/Upgrade";

        public override void SetParam ( string [] param , int line )
        {
            if ( !int.TryParse ( param [12] , out commodityID ) )
            {
                Debug.LogError ( line + ": " + param [12] + "不是商品編號" );
                commodityID = 0;
            }
            if ( !int.TryParse ( param [13] , out star ) )
            {
                Debug.LogError ( line + ": " + param [13] + "不是星級" );
                star = 0;
            }
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            GameObject effectObj = GameObject.Find ( effectPath );
            if ( effectObj == null )
            {
                Debug.LogError ( line + ": " + effectPath + "路徑不正確" );
                yield break;
            }
            Prefab.UpgradePrefab upgrade = GameObject.Instantiate ( Resources.Load<Prefab.UpgradePrefab> ( upgradePath ) , effectObj.transform );
            GameData.Commodity.PopularAddition [] popular = GameLoopManager.instance.pm.commodityItem.GetData ( commodityID ).popular;
            Vector3 BeforePopular = new Vector3 ( (int)popular[0] , (int)popular[1] , (int)popular[2] );
            Vector3 AfterPopular = new Vector3 ( (int)popular[0] + 1 , (int)popular[1] + 1 , (int)popular[2] + 1 );
            for ( int i = 0; i < 3; i++ )
                GameLoopManager.instance.pm.commodityItem.RanPopular ( commodityID , i , 100 , 0 );
            upgrade.UpdateData ( commodityID , star , BeforePopular , AfterPopular );
            yield return null;
        }
    }
}