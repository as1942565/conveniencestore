﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class HireWorkerEvent : BaseDramaEvent
    {
        public int [] workerID;

        public override void SetParam ( string [] param , int line )
        {
            string [] worker = param [12].Split ( '-' );
            workerID = new int [worker.Length];
            for ( int i = 0; i < worker.Length; i++ )
            {
                workerID [i] = int.Parse ( worker [i] );
            }
        }

        private const string path = "GameUICanvas/MainAction";

        public override IEnumerator Play ()
        {
            PlayButtonAudio.self.PlayCustom ( "Audios/Effect/" , "Coin" , false );
            int spend = GameLoopManager.instance.gm.businessList.Data [11].cost;
            GameLoopManager.instance.pm.playerData.AddMoney ( -spend );
            int totalShift = GameLoopManager.instance.pm.calender.TotalShift;
            GameLoopManager.instance.pm.gameLog.AddBusiness ( totalShift , spend );

            Transform parent = GameObject.Find ( path ).transform;
            UI.BuildHireWorker buildHireWorker = FindExtenstion.FindBuildHireWorkerInChild ( parent );
            GameLoopManager.instance.pm.hireWorker.SetQuantity ( 1 );
            buildHireWorker.Open ();
           // buildHireWorker.gameObject.SetActive ( true );
            buildHireWorker.Teach ( workerID );
            yield return null;
        }
    }
}