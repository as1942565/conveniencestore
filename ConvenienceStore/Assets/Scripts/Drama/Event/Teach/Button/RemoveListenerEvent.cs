﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace DramaEvent
{
    public class RemoveListenerEvent : BaseDramaEvent
    {
        public string path;
        public string name;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            path = param [12];
            name = param [13];
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            GameObject obj = GameObject.Find ( path + name );
            if ( obj == null )
            {
                Debug.LogError ( line + ": " + path + name + "路徑不正確" );
                yield break;
            }
            StoryManager.self.tsm.tempEvent = obj.GetComponent<Button> ().onClick;
            obj.GetComponent<Button> ().onClick = new Button.ButtonClickedEvent ();
            yield return null;
        }
    }
}