﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace DramaEvent
{
    public class EnableButtonEvent : BaseDramaEvent
    {
        public string path;
        public bool targetBool;
        public bool wait;
        public bool endable;
        public string nextTeach;
        public int line;

        private Button button;

        public override void SetParam ( string [] param , int line )
        {
            this.line = line;
            path = param [12];
            if ( !bool.TryParse ( param [13] , out targetBool ) )
            {
                targetBool = false;
                Debug.LogError ( line + ": " + param [13] + "不是布林值" );
            }
            if ( !bool.TryParse ( param [14] , out wait ) )
            {
                wait = false;
            }
            if ( !bool.TryParse ( param [15] , out endable ) )
            {
                endable = false;
            }
            nextTeach = param [16];
        }

        public override IEnumerator Play ()
        {
            GameObject obj = GameObject.Find ( path );
            
            if ( wait == true )
            {
                while ( obj == null )
                {
                    obj = GameObject.Find ( path );
                    yield return null;
                }
            }

            if ( obj == null )
            {
                Debug.LogError ( line + ": " + path + "路徑不正確" );
                yield break;
            }
            button = obj.GetComponent<Button> ();
            if ( button == null )
            {
                Debug.LogError ( line + ": " + path + "物件上找不到按鈕" );
                yield break;
            }
            button.enabled = true;
            button.onClick.AddListener ( OnClick );
            yield return null;
        }
        void OnClick ()
        {
            targetBool = true;
            button.onClick.RemoveListener ( OnClick );
            if ( endable == false )
                button.enabled = false;
        }
    }
}