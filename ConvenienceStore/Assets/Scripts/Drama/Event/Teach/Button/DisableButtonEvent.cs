﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DramaEvent
{
    public class DisableButtonEvent : BaseDramaEvent
    {
        public string path;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            path = param [12];
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            GameObject obj = GameObject.Find ( path );
            if ( obj == null )
            {
                Debug.LogError ( line + ": " + path + "路徑不正確" );
                yield break;
            }
            Button button = obj.GetComponent<Button> ();
            if ( button == null )
            {
                Debug.LogError ( line + ": " + path + "物件上沒按鈕" );
                yield break;
            }
            button.enabled = false;
            yield return null;
        }
    }
}