﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DramaEvent
{
    public class WaitForTouchTargetEvent : BaseDramaEvent
    {
        private string path;
        private string name;
        private int line;

        private bool targetBool = false;

        public override void SetParam ( string [] param , int line )
        {
            path = param [12];
            name = param [13];
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            GameObject obj = GameObject.Find ( path + name );
            if ( obj == null )
            {
                Debug.LogError ( line + ": " + path + name + "路徑不正確" );
                yield break;
            }
            obj.GetComponent<Button> ().onClick.AddListener ( OnClick );
            while ( targetBool == false )
            {
                yield return null;
            }
        }

        void OnClick ()
        {
            targetBool = true;
        }
    }
}