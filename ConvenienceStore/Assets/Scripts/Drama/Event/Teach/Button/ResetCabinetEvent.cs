﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DramaEvent
{
    public class ResetCabinetEvent : BaseDramaEvent
    {
        private int [] cabinetQuantity = new int [] { 2 , 2 , 2 , 0 , 1 };

        private const string path = "GameUICanvas/MainAction/BuildCommodity/Panel/SetupCommodity/BuildCabinet/cabinet";

        public override IEnumerator Play ()
        {
            for ( int t = 0; t < cabinetQuantity.Length; t++ )
            {
                for ( int i = 0; i < cabinetQuantity [t]; i++ )
                {
                    if ( GameObject.Find ( path + t + "-" + i ) == null )
                        yield break;
                    Image cabinet = GameObject.Find ( path + t + "-" + i ).GetComponent<Image> ();
                    if ( cabinet == null )
                    {
                        Debug.LogError ( path + t + "-" + i + "路徑不正確" );
                        yield break;
                    }
                    cabinet.raycastTarget = true;
                    yield return null;
                }
            }
        }
    }
}