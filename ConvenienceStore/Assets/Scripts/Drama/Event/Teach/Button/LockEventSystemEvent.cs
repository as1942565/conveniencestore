﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DramaEvent
{
    public class LockEventSystemEvent : BaseDramaEvent
    {
        public string path;
        public string name;
        public bool enabled;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            this.line = line;
            path = param [12];
            name = param [13];
            if ( bool.TryParse ( param [14] , out enabled ) )
            {

            }
            else
            {
                Debug.LogError ( line + ": " + param [14] + "不是布林值" );
                enabled = false;
            }
        }

        public override IEnumerator Play ()
        {
            
            IPointerDownHandler eventSystem = GameObject.Find ( path + name ).GetComponent<IPointerDownHandler> ();
            if ( eventSystem == null )
            {
                Debug.LogError ( line + ": " + path + name + "路徑不正確" );
                yield break;
            }
            yield return null;
        }
    }
}