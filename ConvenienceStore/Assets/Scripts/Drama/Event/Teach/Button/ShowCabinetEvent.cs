﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DramaEvent
{
    public class ShowCabinetEvent : BaseDramaEvent
    {
        private int type;
        private int commodityID;
        private bool show;
        private int line;

        private const string path = "GameUICanvas/MainAction/BuildCommodity/Panel/SetupCommodity/BuildCabinet/cabinet";

        public override void SetParam ( string [] param , int line )
        {
            if ( !int.TryParse ( param [12] , out type ) )
            {
                Debug.LogError ( type + "不是整數" );
                type = 0;
            }
            if ( !int.TryParse ( param [13] , out commodityID ) )
            {
                Debug.LogError ( commodityID + "不是整數" );
                commodityID = 0;
            }
            if ( !bool.TryParse ( param [14] , out show ) )
            {
                Debug.LogError ( show + "不是布林值" );
                show = false;
            }
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            Image cabinet = GameObject.Find ( path + type + "-" + commodityID ).GetComponent<Image> ();
            if ( cabinet == null )
            {
                Debug.LogError ( path + type + "-" + commodityID + "路徑不正確" );
                yield break;
            }
            cabinet.raycastTarget = show;
            yield return null;
        }
    }
}