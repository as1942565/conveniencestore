﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DramaEvent
{
    public class DisableButtonInChildEvent : BaseDramaEvent
    {
        public string path;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            path = param [12];
            this.line = line;
        }
        public DisableButtonInChildEvent () : this ( "" )
        {

        }

        public DisableButtonInChildEvent ( string path )
        {
            this.path = path;
            line = 0;
        }

        public override IEnumerator Play ()
        {
            GameObject parent = GameObject.Find ( path );
            if ( parent == null )
            {
                Debug.LogError ( line + ": " + path + "路徑不正確" );
            }
            else
            {
                Button [] buttons = parent.transform.GetComponentsInChildren<Button> ();
                for ( int i = 0; i < buttons.Length; i++ )
                {
                    buttons [i].enabled = false;
                }
            }
            yield return null;
        }
    }
}