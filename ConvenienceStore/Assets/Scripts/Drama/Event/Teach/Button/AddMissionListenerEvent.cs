﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DramaEvent
{
    public class AddMissionListenerEvent : BaseDramaEvent
    {
        private const string path = "GameUICanvas/MainAction/ActivityManager/SchedualeManager/BuildMission";

        public override IEnumerator Play ()
        {
            GameObject obj = GameObject.Find ( path );
            if ( obj == null )
            {
                Debug.LogError ( path + "路徑不正確" );
                yield break;
            }
            UI.BuildMission buildMission = obj.GetComponent<UI.BuildMission> ();

            GameObject btn = GameObject.Find ( path + "/OKBtn" );
            if ( btn == null )
            {
                Debug.LogError ( path + "/OKBtn路徑不正確" );
                yield break;
            }
            btn.GetComponent<Button> ().onClick.AddListener ( buildMission.OK );
            yield return null;
        }
    }
}