﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DramaEvent
{
    public class EnableAllButtonEvent : BaseDramaEvent
    {
        public override IEnumerator Play ()
        {
            Transform Background = GameObject.Find ( "BackgroundCanvas" ).transform;
            List<Button> backgroundButton = FindExtenstion.FindButtonInChild ( Background );
            foreach ( Button button in backgroundButton )
            {
                button.enabled = true;
            }
            yield return null;

            Transform GameUI = GameObject.Find ( "GameUICanvas" ).transform;
            List<Button> gameUIButton = FindExtenstion.FindButtonInChild ( GameUI );
            foreach ( Button button in gameUIButton )
            {
                button.enabled = true;
            }
            yield return null;
        }
    }
}