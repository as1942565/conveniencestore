﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class TeachEvent : BaseDramaEvent
    {
        public bool teach;
        public bool story;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            if ( !bool.TryParse ( param [12] , out teach ) )
            {
                Debug.LogError ( param [12] + "不是布林值" );
            }
            if ( !bool.TryParse ( param [13] , out story ) )
            {
                Debug.LogError ( param [13] + "不是布林值" );
            }
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            StoryManager.self.ssm.Manager.SetActive ( story );
            TalkBoxManager.self.TalkBox.gameObject.SetActive ( story );
            StoryManager.self.tsm.Manager.SetActive ( teach );
            StoryManager.self.TeachBool = teach;

            if ( teach == false )
                StoryManager.self.tsm.TeachText.text = "";
            yield return null;
        }
    }
}