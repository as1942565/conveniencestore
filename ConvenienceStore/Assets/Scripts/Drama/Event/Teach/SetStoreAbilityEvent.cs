﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class SetStoreAbilityEvent : BaseDramaEvent
    {
        public int popular;
        public int police;
        public int clean;
        public int money;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            this.line = line;
            if ( !int.TryParse ( param [12] , out popular ) )
            {
                popular = -1;
            }

            if ( !int.TryParse ( param [13] , out police ) )
            {
                police = -1;
            }

            if ( !int.TryParse ( param [14] , out clean ) )
            {
                clean = -1;
            }
            if ( !int.TryParse ( param [11] , out money ) )
            {
                money = -1;
            }
        }

        public override IEnumerator Play ()
        {
            PlayerDataManager pm = GameLoopManager.instance.pm;

            bool popularBool = popular != -1;
            bool policeBool = police != -1;
            bool cleanBool = clean != -1;
            bool moneyBool = money != -1;
            if ( popularBool )
            {
                pm.playerData.SetPopular ( popular );
            }
            if ( policeBool )
            {
                pm.playerData.SetPolice ( police );
            }
            if ( cleanBool )
            {
                pm.playerData.SetClean ( clean );
            }
            if ( moneyBool )
            {
                pm.playerData.SetMoney ( money );
            }
            yield return null;
        }
    }
}