﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class MoveTeachEvent : BaseDramaEvent
    {
        public Vector2 pos;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            this.line = line;
            float x;
            float y;
            if ( float.TryParse ( param [12] , out x ) )
            {
                if ( float.TryParse ( param [13] , out y ) )
                {
                    pos = new Vector2 ( x , y );
                }
                else
                {
                    Debug.LogError ( line + ": " + param [13] + "不是Y軸座標" );
                }
            }
            else
                {
                    Debug.LogError ( line + ": " + param [12] + "不是X軸座標" );
                }
        }

        public override IEnumerator Play ()
        {
            StoryManager.self.tsm.TeachText.text = "";
            StoryManager.self.tsm.TalkBox.localPosition = pos;
            yield return null;
        }
    }
}