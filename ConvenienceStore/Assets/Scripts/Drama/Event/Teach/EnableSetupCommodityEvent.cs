﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class EnableSetupCommodityEvent : BaseDramaEvent
    {
        public int type;
        public int id;
        public int line;

        public override void SetParam ( string [] param , int line )
        {
            this.line = line;
            if ( !int.TryParse ( param [12] , out type ) )
            {
                Debug.LogError ( line + ": " + param [12] + "不是商品種類" );
                type = 0;
            }
            if ( !int.TryParse ( param [13] , out id ) )
            {
                Debug.LogError ( line + ": " + param [13] + "不是商品櫃編號" );
                id = 0;
            }
        }

        public override IEnumerator Play ()
        {
            StoryManager.self.teachSceneManager.setupCommodityType = type;
            StoryManager.self.teachSceneManager.setupCommodityID = id;
            StoryManager.self.tsm.pass = true;

            yield return null;
        }
    }
}