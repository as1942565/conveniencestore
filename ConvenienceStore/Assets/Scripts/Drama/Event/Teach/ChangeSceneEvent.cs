﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DramaEvent
{
    public class ChangeSceneEvent : BaseDramaEvent
    {
        private string sceneName;

        public ChangeSceneEvent ( string sceneName )
        {
            this.sceneName = sceneName;
        }

        public override IEnumerator Play ()
        {
            if ( sceneName == "" )
            {
                Debug.LogError ( sceneName + "不是場景名稱" );
                yield break;
            }
            SceneManager.LoadScene ( sceneName );
            yield return null;
        }
    }
}