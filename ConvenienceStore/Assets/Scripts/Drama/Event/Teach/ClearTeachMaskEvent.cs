﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class ClearTeachMaskEvent : BaseDramaEvent
    {
        public override IEnumerator Play ()
        {
            StoryManager.self.tsm.Mask.sprite = SpritePath.None;
            yield return null;
        }
    }
}