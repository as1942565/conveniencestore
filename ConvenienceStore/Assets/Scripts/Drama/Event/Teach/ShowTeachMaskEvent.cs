﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class ShowTeachMaskEvent : BaseDramaEvent
    {
        public string name;
        public int line;

        private const string path = "Sprites/Common/Drama/Teach/Mask/";

        public override void SetParam ( string [] param , int line )
        {
            name = param [12];
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            Sprite mask = Resources.Load<Sprite> ( path + name );
            if ( mask == null )
            {
                Debug.LogError ( line + ": " + path + name + "路徑不正確" );
                StoryManager.self.tsm.Mask.sprite = SpritePath.None;
            }
            else
            {
                StoryManager.self.tsm.Mask.sprite = mask;
            }
            yield return null;
        }
    }
}