﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class UseSetupCommodityEvent : BaseDramaEvent
    {
        private bool use;
        private int line;

        public override void SetParam ( string [] param , int line )
        {
            if ( !bool.TryParse ( param [12] , out use ) )
            {
                Debug.LogError ( line + ": " + param [12] + "不是布林值" );
                use = false;
            }
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            StoryManager.self.tsm.UseSetupCommodity = use;
            yield return null;
        }
    }
}