﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class DebuffEvent : BaseDramaEvent
    {
        public override IEnumerator Play ()
        {
            NextShift.StealCommodity.Run ( GameLoopManager.instance.pm , GameLoopManager.instance.gm );
            for ( int i = 0; i < GameLoopManager.instance.gm.commodityList.Data.Length; i++ )
            {
                int quantity = GameLoopManager.instance.pm.storage.GetQuantity ( i );
                if ( quantity == 0 )
                    continue;
                quantity = Mathf.FloorToInt ( quantity * GameLoopManager.instance.pm.punishAddition.GetSaleQuantityAddition () );
                GameLoopManager.instance.pm.storage.SetQuantity ( i , quantity );
            }
            yield return null; 
        }
    }
}
