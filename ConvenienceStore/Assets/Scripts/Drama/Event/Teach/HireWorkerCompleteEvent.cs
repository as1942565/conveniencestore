﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class HireWorkerCompleteEvent : BaseDramaEvent
    {
        private const string path = "GameUICanvas/MainAction/BuildHireWork";

        public override IEnumerator Play ()
        {
            GameObject obj = GameObject.Find ( path );
            if ( obj == null )
            {
                Debug.LogError ( path + "路徑不正確" );
                yield break;
            }
            UI.BuildHireWorker buildHireWorker = obj.GetComponent<UI.BuildHireWorker> ();
            buildHireWorker.HireWorker ();
            yield return null;
        }
    }
}