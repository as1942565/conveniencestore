﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DramaEvent
{
    public class RestockEvent : BaseDramaEvent
    {
        private int commodityID;
        private int quantity;
        private int line;

        public override void SetParam ( string [] param , int line )
        {
            if ( !int.TryParse ( param [12] , out commodityID ) )
            {
                Debug.LogError ( param [12] + "不是商品編號" );
                commodityID = 0;
            }
            if ( !int.TryParse ( param [13] , out quantity ) )
            {
                Debug.LogError ( param [13] + "不是數量" );
                quantity = 0;
            }
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            UI.BuildRestock buildRestock = GameObject.Find ( "GameUICanvas/MainAction/BuildCommodity/Panel/Restock" ).GetComponent<UI.BuildRestock> ();
            
            int storage = GameLoopManager.instance.pm.storage.GetQuantity ( commodityID );
            int purchase = GameLoopManager.instance.pm.purchase.GetWaitPurchase ( commodityID );
            while ( storage + purchase < quantity )
            {
                purchase = GameLoopManager.instance.pm.purchase.GetWaitPurchase ( commodityID );
                yield return null;
            }
            buildRestock.TeachOnStockUpdated ( commodityID , quantity - storage );

            StoryManager.self.tsm.pass = false;
            StoryManager.self.tsm.LockRestockBtn = true;
        }
    }
}