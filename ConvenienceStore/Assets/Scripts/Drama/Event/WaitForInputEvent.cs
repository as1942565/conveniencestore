﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace DramaEvent
{
    public class WaitForInputEvent : BaseDramaEvent
    {
        private float autoTime = 1f;
        private bool pass;

        public override IEnumerator Play ()
        {
            float speed = autoTime / GameLoopManager.instance.om.optionData.GetAutoPlaySpeed () * 50;
            float waitTime = Time.time + speed;
            IsRunning = true;
            while ( true )
            {
                if ( TeachPlayerManager.skip == true )
                    break;

                if ( TouchButton ( ref pass ) )
                {
                    break;
                }
                if ( Input.GetKeyDown ( KeyCode.Space ) )
                {
                    break;
                }
#if UNITY_EDITOR
                if ( Input.GetKey ( KeyCode.V ) )
                {
                    break;
                }
#endif
                if ( StorySceneManager.fast )
                {
                    break;
                }
                if ( StorySceneManager.auto && Time.time > waitTime )
                {
                    break;
                }
                if ( StorySceneManager.skip == true )
                {
                    break;
                }
                yield return null;
            }

            if ( GameLoopManager.instance.om.optionData.GetContinueSound () == false )
            {
                while ( StorySceneManager.isPlayingSound == true )
                    yield return null;
            }

            if ( StorySceneManager.skip == false )
                yield return new WaitForSeconds ( 0.01f );
            IsRunning = false;
        }


        bool TouchButton ( ref bool pass )
        {
            if ( StorySceneManager.log )
                return false;
            if ( Input.GetMouseButtonDown ( 0 ) )
            {
                PointerEventData eventData = new PointerEventData ( StoryManager.self.eventSystem );
                eventData.position = Input.mousePosition;

                GraphicRaycaster raycaster = StoryManager.self.graphicsRaycaster;
                List<RaycastResult> result = new List<RaycastResult> ();
                raycaster.Raycast ( eventData , result );
                if ( result.Count == 0 )
                {
                    pass = true;
                    return pass;
                }
                Button button = null;
                button = result [0].gameObject.GetComponent<Button> ();
                pass = button == null;
            }
            return pass;
        }
    }
}