﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace DramaEvent
{
    public class CreateActorEvent : BaseDramaEvent
    {
        public string actorName;
        public string startPos;
        public string endPos;
        public string walkTime;
        public string emoticonsID;
        public string faceID;
        public string clothesName;
        public bool fade;
        public bool show;
        public int line;

        private float heigth = 200f;
        private float EXHeigth = 0f;

        public string tempPos;
        public string tempName;
        private const string PrefabPath = "Prefabs/Drama/Actor";
        private const string ActorPath = "Sprites/Common/Drama/Actor/";

        public override void SetParam ( string [] param , int line )
        {
            actorName = param [1];
            startPos = param [3];
            endPos = param [4];
            walkTime = param [5];
            faceID = param [6];
            emoticonsID = param [7];
            clothesName = param [8];
            if ( !bool.TryParse ( param [9] , out fade ) )
            {
                fade = false;
            }
            if ( !bool.TryParse ( param [10] , out show ) )
            {
                show = false;
            }
            this.line = line;
        }

        public CreateActorEvent () : this ( null , "" , "" , 0 )
        {

        }
        public CreateActorEvent ( string [] line , string tempPos , string tempName , int index )
        {
            if ( line == null )
                return;
            this.line = index;
            actorName = line [1];
            startPos = line [3];
            endPos = line [4];
            walkTime = line [5];
            faceID = line [6];
            emoticonsID = line [7];
            clothesName = line[8];
            if ( !bool.TryParse ( line [9] , out fade ) )
            {
                fade = false;
            }
            if ( !bool.TryParse ( line [10] , out show ) )
            {
                show = false;
            }
            this.tempName = tempName;
            this.tempPos = tempPos;
        }

        public override IEnumerator Play ()
        {
            Transform actorTransform = StoryManager.self.ssm.Actor.Find ( actorName );
            Actor [] tempActor = StoryManager.self.ssm.Actor.GetComponentsInChildren<Actor> ();
            if ( startPos == "" )
            {
                for ( int i = 0; i < tempActor.Length; i++ )
                {
                    bool talkBool = tempActor [i].name == actorName;
                    if ( tempPos != startPos || tempName != actorName )
                        tempActor [i].Talk ( talkBool );
                }
            }
            else
            {
                for ( int i = 0; i < tempActor.Length; i++ )
                {
                    if ( tempActor [i].posX == float.Parse ( startPos ) && tempActor [i].actorName != actorName )
                    {
                        GameObject.Destroy ( tempActor [i].gameObject );
                        break;
                    }
                }

                if ( actorTransform == null )
                {
                    actorTransform = GameObject.Instantiate ( Resources.Load<Transform> ( PrefabPath ) , StoryManager.self.ssm.Actor );
                }
                if ( fade )
                {
                    Image image = actorTransform.GetComponent<Image> ();
                    Color color = image.color;
                    color.a = 0;
                    image.color = color;
                    image.DOFade ( 1 , 0.5f );
                }
            }
            actorTransform.name = actorName;
            Actor actor = actorTransform.GetComponent<Actor> ();
            actor.actorName = actorName;
            
            yield return UpdateSprite ( actor , tempActor );

            if ( endPos != "" )
            {
                MoveActorEvent move = new MoveActorEvent ();
                move.SetParam ( new string [] { "" , actorName , "" , "" , endPos , walkTime } , line );
                yield return move.Play ();
            }
        }
        bool steam;
        string [] Employee = new string [8] { "顏無菁" , "夜蓮心" , "笭伊姍" , "端木壹" , "司馬小仙" , "顏紅玉" , "幽香" , "林原琳" };
        string [] EmployeeE = new string [8] { "Gin Yen" , "Lycianna Yeh" , "Elissa Lin" , "Manager" , "Xiaoxian Sima" , "Jade Yen" , "Yusra" , "Lauryn Lin" };
        string [] EmployeeC = new string [8] { "颜无菁" , "夜莲心" , "笭伊姗" , "端木壹" , "司马小仙" , "颜红玉" , "幽香" , "林原琳" };
        string [] EmployeeK = new string [8] { "진 옌" , "리시아나 예" , "엘리사 린" , "도미닉" , "샤오셴 시마" , "제이드 옌" , "유스라" , "라우린 린" };
        string [] EmployeeR = new string [8] { "Янь Уцзин" , "Е Ляньсинь" , "Элисса Линь" , "Доминик" , "Сима Сяосянь" , "Янь Хунъюй" , "Ю Сян" , "Линь Юаньлин" };
        string EID ()
        {
            for ( int i = 0; i < Employee.Length; i++ )
            {
                if ( Employee [i] == actorName )
                {
                    steam = i == 2 || i == 4;
                    return i.ToString () + "/";
                }
            }
            for ( int i = 0; i < EmployeeE.Length; i++ )
            {
                if ( EmployeeE [i] == actorName )
                {
                    steam = i == 2 || i == 4;
                    return i.ToString () + "/";
                }
            }
            for ( int i = 0; i < EmployeeC.Length; i++ )
            {
                if ( EmployeeC [i] == actorName )
                {
                    return i.ToString () + "/";
                }
            }
            for ( int i = 0; i < EmployeeK.Length; i++ )
            {
                if ( EmployeeK [i] == actorName )
                {
                    return i.ToString () + "/";
                }
            }
            for ( int i = 0; i < EmployeeR.Length; i++ )
            {
                if ( EmployeeR [i] == actorName )
                {
                    return i.ToString () + "/";
                }
            }
            return "EX/";
        }
        string [] Clothes = new string[10] { "制服" , "cos" , "內衣" , "半制服" , "私服" , "裸" , "麋鹿" , "住院" , "旗袍" , "半裸" };
        string CID ()
        {
            for ( int i = 0; i < Clothes.Length; i++ )
            {
                if ( Clothes [i] == clothesName )
                    return i.ToString () + "/Actor";
            }
            if ( clothesName == "西裝" )
                return "1/Actor";
            return "";
        }

        string [] emoticons = new string [] { "開心" , "玻璃心" , "無言" , "生氣" , "汗顏" , "得意" , "靈光一閃" , "驚嘆號" , "問號" , "反問號" };
        IEnumerator UpdateSprite ( Actor actor , Actor [] tempActor )
        {
            if ( faceID.Length == 1 )
                faceID = "0" + faceID;
            string path = ActorPath + EID () + CID () + faceID;
            Sprite actorSpr;
            if ( faceID == "EX10" || faceID == "EX15" || faceID == "EX16" )
                steam = true;

            if ( steam == true && GameLoopManager.instance.GetSteam () == true )
                actorSpr = SpritePath.None;
            else
                actorSpr = GetSprite ( path );
            if ( GameLoopManager.instance.GetSteam () == true && actorName == "笭伊姍" )
                actorSpr = SpritePath.None;
            actor.actorImage.sprite = actorSpr;

            if ( show )
            {
                if ( actorName == "笭伊姍" )
                    yield return new ShowActorEvent ( actor.transform , EXHeigth ).Play ();
                else
                    yield return new ShowActorEvent ( actor.transform , heigth ).Play ();
            }

            if ( tempPos != startPos || tempName != actorName )
            {
                if ( show == false )
                    actor.UpdateData ( float.Parse ( startPos ) );
                for ( int i = 0; i < tempActor.Length; i++ )
                {
                    if ( tempActor [i] == null )
                        continue;
                    bool talkBool = tempActor [i].gameObject.name == actorName;
                    tempActor [i].Talk ( talkBool );
                }
            }
            if ( GameLoopManager.instance.GetSteam () == true && actorName == "笭伊姍" )
            {
                actor.EmoticoncBG.gameObject.SetActive ( false );
                yield break;
            }
            bool useEmoticons = false;
            for ( int i = 0; i < emoticons.Length; i++ )
            {
                if ( emoticons[i] == emoticonsID )
                {
                    useEmoticons = true;
                    yield return new CreateEmoticonsEvent ( i , actor.gameObject.name , actor.EmoticoncBG ).Play ();
                    break;
                }
            }
            if ( useEmoticons == false )
                actor.EmoticoncBG.gameObject.SetActive ( false );
            yield return null;
        }
        Sprite GetSprite ( string path )
        {
            Sprite spr;
            if ( StorySceneManager.ActorSprite.TryGetValue ( path , out spr ) == false )
            {
                Sprite s = Resources.Load<Sprite> ( path );
                if ( s == null )
                {
                    Debug.LogError ( line + ": " + path + "路徑不正確" );
                    return SpritePath.None;
                }
                StorySceneManager.ActorSprite.Add ( path , s );
                spr = s;
            }
            return spr;
        }
    }
}