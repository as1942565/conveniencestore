﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DramaEvent
{
    public class CreateEmoticonsEvent : BaseDramaEvent
    {
        public int emoticonsID;
        public string name;
        public Transform emoticonsBG;

        private const string path = "Sprites/Common/Drama/Actor/Emoticons";

        public CreateEmoticonsEvent () : this ( 0 , "" , null )
        {

        }
        public CreateEmoticonsEvent ( int emoticonsID , string name , Transform emoticonsBG )
        {
            this.emoticonsID = emoticonsID;
            this.name = name;
            this.emoticonsBG = emoticonsBG;
        }

        public override IEnumerator Play ()
        {
            Sprite [] emoticonsSpr = Resources.LoadAll<Sprite> ( path );
            if ( emoticonsSpr == null )
            {
                Debug.LogError ( path + "路徑不正確" );
                yield break;
            }
            if ( emoticonsID >= emoticonsSpr.Length || emoticonsID < 0 )
            {
                Debug.LogError ( emoticonsID + "大於表情符號長度" );
                yield break;
            }
            Image emoticonsImg = emoticonsBG.GetChild ( 0 ).GetComponent<Image> ();
            if ( emoticonsImg == null )
            {
                Debug.LogError ( "表情符號圖是空的" );
                yield break;
            }
            yield return null;

            emoticonsBG.gameObject.SetActive ( true );
            emoticonsImg.sprite = emoticonsSpr [emoticonsID];
            if ( name == "笭伊姍" )
            {
                emoticonsBG.transform.localPosition = new Vector3 ( 250 , 235 , 0 );
            }
            else
            {
                emoticonsBG.transform.localPosition = new Vector3 ( 250 , 360 , 0 );
            }
            yield return null;
        }
    }
}