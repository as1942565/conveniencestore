﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace DramaEvent
{
    public class CreatePictureEvent : BaseDramaEvent
    {
        public string workerID;
        public string type;
        public string id;
        public int line;

        private const string path = "Drama/Picture";

        public override void SetParam ( string [] param , int line )
        {
            workerID = param [12];
            type = param [13];
            id = param [14];
            this.line = line;
        }

        public override IEnumerator Play ()
        {
            if ( GameLoopManager.instance.GetSteam () == true && workerID == "2" )
            {
                yield break;
            }
            Transform picture = StoryManager.self.ssm.Picture;
            if ( picture.childCount > 1 )
            {
                Transform parent = picture.GetChild ( 0 );
                GameObject.Destroy ( parent.gameObject , 1f );
            }
            Image image = GameObject.Instantiate ( Resources.Load<Image> ( "Prefabs/" + path ) , StoryManager.self.ssm.Picture );
            //Sprite [] spr = Resources.LoadAll<Sprite> ( "Sprites/" + path + + workerID + "/Picture" + type );
            Sprite spr = Resources.Load<Sprite> ( "Sprites/Common/" + path + "/" + workerID + "/" + type + "/" + id );
            if ( spr == null )
            {
                Debug.LogError ( line + ": " + "Sprites/Common/" + path + "/" + workerID + "/" + type + "/" + id + "路徑不正確" );
                yield break;
            }
            /*if ( id >= spr.Length )
            {
                //Debug.LogError ( id + "超過插圖數量" );
                yield break;
            }*/

            image.sprite = spr;
            if ( StorySceneManager.skip )
                image.DOFade ( 1 , 0.00001f );
            else
                image.DOFade ( 1 , 0.5f );
            yield return null;
        }
    }
}