﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct StoryLogData
{
    public string id;
    public string story;
    public string name;
    public bool soundBool;
}

public class StoryLogManager : MonoBehaviour
{
    public static StoryLogManager self;
    private void Awake ()
    {
        self = this;
        Close ();
    }
    public SG.InitOnStart initOnStart;
    public GameObject Background;
    public Scrollbar scrollbar;
    public Transform LogSpace;

    [SerializeField]
    List<StoryLogData> logs = new List<StoryLogData> ();

    private const string path = "Drama/";

    public void AddLog ( string story , string name , bool sound , string id )
    {
        StoryLogData log = CreateLog ( story , name , sound , id );
        logs.Add ( log );
    }

    StoryLogData CreateLog ( string story , string name , bool sound , string id )
    {
        StoryLogData log = new StoryLogData ();
        log.id = id;
        log.story = story;
        if ( name == "？？？" )
            log.name = "【 " + name + "】";
        else if ( name != "" )
            log.name = "【 " + name + " 】";
        log.soundBool = sound;
        return log;
    }
    public void ClearLog ()
    {
        logs.Clear ();
    }

    public void Open ()
    {
        Background.GetComponent<Image> ().sprite = Resources.Load<Sprite> ( $"Sprites/{GameLoopManager.instance.om.optionData.GetLanguageStr ()}Drama/Story/Log/Background" );
        Background.SetActive ( true );

        initOnStart.UpdateCount ( logs.Count );
        
    }
    
    public void Close ()
    {
        Background.SetActive ( false );
    }

    public StoryLogData GetStoryLogData ( int id )
    {
        if ( id >= logs.Count )
        {
            Debug.LogError ( id + "超過劇情長度" );
            StoryLogData temp = new StoryLogData ();
            temp.story = "";
            return temp;
        }
        return logs [id];
    }
}
