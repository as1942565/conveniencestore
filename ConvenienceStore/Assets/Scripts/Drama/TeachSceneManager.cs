﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TeachSceneManager : MonoBehaviour
{
    public GameObject Manager;
    public Transform TalkBox;
    public Image Mask;
    public Text TeachText;
    public Image Actor;
    public int setupCommodityType;
    public int setupCommodityID;
    public bool pass;
    public bool UseSetupCommodity = true;
    public bool LockRestockBtn;
    public Button.ButtonClickedEvent tempEvent;
    public GameObject SkipBtn;

    public static int researchID;

    private void Start ()
    {
        SkipBtn.transform.parent = StoryManager.self.transform;
    }

    public void Skip ()
    {
        BGMManager.self.Fade ( 0 , 0.5f );
        LoadingScene.self.Transitions ( 0.5f , 1f , true );

        Invoke ( "skip" , 0.5f );
    }
    void skip ()
    {
        int index = TeachPlayerManager.self.GetIndex ();
        if ( index < 3 )
        {
            TeachPlayerManager.skip = true;
            return;
        }


        for ( int i = 0; i < 5; i++ )
            GameLoopManager.instance.om.unlockActorData.SetUnlock ( i , true );
        GameLoopManager.instance.om.unlockActorData.SetUnlock ( 7 , true );
        GameLoopManager.instance.om.unlockActorData.SetUnlock ( 8 , true );
        SaveLoadManager.OnlyDataSave ( GameLoopManager.instance.om );

        SaveLoadManager.Load ( out GameLoopManager.instance.pm , "Teach" );
        GameLoopManager.instance.OnLoaded ( GameLoopManager.instance.pm );
        SkipBtn.SetActive ( false );
        this.gameObject.SetActive ( false );
        SceneManager.LoadScene ( "Demo" );
        GameLoopManager.instance.ReadFirstStory = false;
        SaveLoadManager.Save ( GameLoopManager.instance.pm , "Auto" );
        BGMManager.self.PlayShiftBGM ();
    }
}
