﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace DramaEvent
{
    public class Actor : MonoBehaviour
    {
        public Transform EmoticoncBG;
        public float moveDistance;

        [HideInInspector]
        public string actorName;
        [HideInInspector]
        public float posX;
        [HideInInspector]
        public Image actorImage;
        [HideInInspector]
        public bool talkBool;

        RectTransform rect;
        float defaultWidth;
        float defaultHeight;

        void Init ()
        {
            rect = GetComponent<RectTransform> ();
            defaultHeight = rect.rect.height;
            defaultWidth = rect.rect.width;
        }

        public void UpdateData ( float posX )
        {
            DOTween.Kill ( transform );
            this.posX = posX;
            transform.localPosition = new Vector3 ( posX , transform.localPosition.y , 0 );
            int scaleX = posX < 0 ? -1 : 1;
            transform.localScale = new Vector3 ( Mathf.Abs ( transform.localScale.x ) * scaleX , transform.localScale.y , 1 );

            if ( rect == null )
                Init ();
        }

        public void Talk ( bool talk )
        {
            talkBool = talk;
            if ( talkBool )
            {
                transform.DOLocalMoveY ( transform.localPosition.y + moveDistance , 0.1f ).OnComplete ( () => transform.DOLocalMoveY ( transform.localPosition.y - moveDistance , 0.1f ) );
            }
            Color32 mask = talkBool ? new Color32 ( 255 , 255 , 255 , 255 ) : new Color32 ( 200 , 200 , 200 , 255 );
            actorImage.DOColor ( mask , 0.3f );
        }
    }
}