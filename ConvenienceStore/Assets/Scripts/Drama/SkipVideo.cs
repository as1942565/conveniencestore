﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.EventSystems;

public class SkipVideo : MonoBehaviour , IPointerClickHandler 
{
    private bool doubleClick = false;

    public void OnPointerClick ( PointerEventData eventData )
    {
        if ( doubleClick == true )
        {
            GetComponent<VideoPlayer> ().Stop ();
        }

        doubleClick = true;

        Invoke ( "CancelDoubleClick" , 0.2f );
    }
    void CancelDoubleClick ()
    {
        doubleClick = false;
    }
}
