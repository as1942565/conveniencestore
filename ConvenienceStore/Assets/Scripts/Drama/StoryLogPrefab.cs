﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prefab
{
    public class StoryLogPrefab : MonoBehaviour
    {
        public string id;
        public Text StoryText;
        public Text NameText;
        public Button SoundButton;

        public float UpdateData ( StoryLogData data )
        {
            StoryText.text = data.story;
            NameText.text = data.name;
            id = data.id;
            bool soundBool = data.soundBool;
            SoundButton.gameObject.SetActive ( soundBool );

            Canvas.ForceUpdateCanvases ();

            float scale = StoryText.transform.lossyScale.y;
            float height = StoryText.rectTransform.rect.height * scale;

            if ( soundBool == false )
                return height;
            SoundButton.onClick.AddListener ( PlaySound );
            return height;
        }

        private const string path = "Audios/Sound/H/";

        void PlaySound ()
        {
            EffectManager.self.DeleteSound ();
            Debug.Log ("Play");
            //Todo : 播放指定聲優
            string soundType = StoryManager.self.storyPath;
            Debug.Log ($"{path}{soundType}/");
            EffectManager.self.PlaySound ( $"{path}{soundType}/" , id , false , EffectPlayer.AudioType.sound );
        }
    }
}