﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WatchStory : MonoBehaviour
{
    private void Start ()
    {
        if ( GameLoopManager.instance.WatchStory == true )
        {
            string language = GameLoopManager.instance.om.optionData.GetLanguageStr ();
            string type = $"Data/Story/{language}H/";
            int employeeID = GameLoopManager.instance.ids [0];
            int storyID = GameLoopManager.instance.ids [1];
            string Path = $"H_Employee{employeeID}/H_Employee{employeeID}_0{storyID + 1}";

            if ( employeeID == 3 )
            {
                type = $"Data/Story/{language}";
                if ( storyID == 0 )
                    Path = "H/H_Employee3/H_Employee3_EX01";
                else if ( storyID == 1 )
                    Path = "Worker/Work18";
            }

            if ( StoryManager.self != null )
            {
                StoryManager.self.StartStory ( type , Path );
                GameLoopManager.instance.om.unlockCGData.SetUnlock ( employeeID , storyID , true );
            }
        }
    }
}
