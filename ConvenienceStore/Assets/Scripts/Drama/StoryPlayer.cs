﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace DramaEvent
{
    public class StoryPlayer : BaseDramaEvent
    {
        public StoryPlayer( string storyType , string Path )
        {
            this.storyType = storyType;
            this.Path = Path;
        }
        private string storyType;
        private string Path;
        public TextAsset Drama;
        private float KeySpeed = 0.05f;                                          //字出現的速度
        private float MaxKeySpeed;                                              //下一次文字出現的時間
        private bool pass;   
        
        public IEnumerator WriterText ( int index , string tempText , string name )
        {
            Text text = TalkBoxManager.self.StoryText;
            text.text = "";
            TalkBoxManager.self.TalkBox.gameObject.SetActive ( true );
            if ( GameLoopManager.instance.om.optionData.GetKeyWordSpeed () == 0 )
                text.text = tempText;
            else
            {
                bool skipWord = false;
                foreach ( char word in tempText )
                {
                    while ( pass == false )
                    {
                        if ( StorySceneManager.skip )
                        {
                            break;
                        }
                        if ( TeachPlayerManager.skip )
                        {
                            break;
                        }

                        if ( TouchButton ( ref pass ) )
                        {
                            break;
                        }
                        if ( Input.GetKeyDown ( KeyCode.Space ) )
                        {
                            break;
                        }
#if UNITY_EDITOR
                        if ( Input.GetKey ( KeyCode.V ) )
                        {
                            break;
                        }

                        while ( StorySceneManager.log )
                        {
                            yield return null;
                        }
                        if ( Input.GetKey ( KeyCode.P ) )
                        {
                            DramaPlayerManager.pass = true;
                            break;
                        }
#endif
                        if ( Time.time > MaxKeySpeed )                      //該出現下一個字的時候	
                            break;
                        yield return null;
                    }
                    float speed = KeySpeed / GameLoopManager.instance.om.optionData.GetKeyWordSpeed () * 50;
                    if ( StorySceneManager.fast )
                        speed = 0.005f;
                    MaxKeySpeed = Time.time + speed;                     //更新冷卻時間
                    /*if ( word == 'W' )
                    {
                        yield return new WaitForSeconds ( 0.5f );
                        continue;
                    }*/
                    if ( word == '{' )
                    {
                        skipWord = true;
                        StoryManager.self.PlayAutoEvent ( index , ref tempText );
                        continue;
                    }
                    if ( word == '}' )
                    {
                        skipWord = false;
                        continue;
                    }
                    if ( skipWord == false )
                        text.text += word;
                }
            }
            if ( StorySceneManager.skip == false && TeachPlayerManager.skip == false )
            {
                yield return new WaitForSeconds ( 0.01f );
                yield return new WaitForInputEvent ().Play ();              //等待玩家點擊 
            }
            pass = false;                                               //取消掠過打字機效果
            EffectManager.self.DeleteSound ();
        }

        bool TouchButton ( ref bool pass )
        {
            if ( Input.GetMouseButtonUp ( 0 ) )
            {
                PointerEventData eventData = new PointerEventData ( StoryManager.self.eventSystem );
                eventData.position = Input.mousePosition;

                GraphicRaycaster raycaster = StoryManager.self.graphicsRaycaster;
                List<RaycastResult> result = new List<RaycastResult> ();
                raycaster.Raycast ( eventData , result );
                if ( result.Count == 0 )
                {
                    pass = true;
                    return pass;
                }
                Button button = null;
                button = result [0].gameObject.GetComponent<Button> ();
                pass = button == null;
            }
            return pass;
        }
    }
}
