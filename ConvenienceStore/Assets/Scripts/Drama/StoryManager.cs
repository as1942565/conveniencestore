﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using UnityEngine.Video;

public class StoryManager : MonoBehaviour
{
    public GameObject Mask;
    public GameObject TeachDay;

    private StoryManager ()
    {

    }

    public bool EnableTeach;
    public bool UseMoneyEffect;

    [HideInInspector]
    public bool TeachBool;
    [HideInInspector]
    public bool touchBool;
    [HideInInspector]
    public bool ReadWorkerDrama;
    [HideInInspector]
    public bool ReadExpantionDrama;
    [HideInInspector]
    public bool ReadEndDrama;
    [HideInInspector]
    public bool ReadActivityDrama;
    [HideInInspector]
    public bool WaitForDramaEnd;
    public Transform DramaSpace;
    public StorySceneManager storySceneManager;
    public TeachSceneManager teachSceneManager;
    public static StoryManager self;
    public EventSystem eventSystem;
    public GraphicRaycaster graphicsRaycaster;
    public Transform audioSpace;
    public VideoPlayer videoPlayer;
    public string storyPath;

    public string tempPos;
    public string tempName;

    private void Awake ()
    {
        self = this;
        tsm = Instantiate ( teachSceneManager , DramaSpace );
        ssm = Instantiate ( storySceneManager , DramaSpace );
    }

    [HideInInspector]
    public StorySceneManager ssm;
    [HideInInspector]
    public TeachSceneManager tsm;

    private void Start ()
    {
        if ( GameLoopManager.instance.WatchStory == true )
            return;

        if ( GameLoopManager.instance.Load == false )
        {
            gameObject.SetActive ( false );
            BGMManager.self.PlayShiftBGM ();
            return;
        }
        EnableTeach = GameLoopManager.instance.ReadFirstStory;
        GameLoopManager.instance.ReadFirstStory = true;
        if ( GetComponent<StoryDemo> () != null )
            return;
        if ( !EnableTeach )
        {
            gameObject.SetActive ( false );
            if ( ReadTeachData () == false )
                PlayTeach ();
            BGMManager.self.PlayShiftBGM ();
            return;
        }
        PlayTeach ();
    }

    bool ReadTeachData ()
    {
        bool res = SaveLoadManager.Load ( out GameLoopManager.instance.pm , "Teach" );
        if ( res )
        {
            GameLoopManager.instance.OnLoaded ( GameLoopManager.instance.pm );
            return true;
        }
        Debug.LogError ( "此記錄檔為空白" );
        return false;
    }

    public void PlayTeach ()
    {
        TeachDay.SetActive ( true );
        GameLoopManager.instance.Init ();
        gameObject.SetActive ( true );
        StartCoroutine ( play () );
    }
    IEnumerator play ()
    {
        TeachPlayerManager.self.SetStoryName ();
        while ( TeachPlayerManager.self.ReadFinish == false )
        {
            yield return null;
        }
        while ( true )
        {
            TeachPlayerManager.skip = false;
            string teachName = TeachPlayerManager.self.GetStoryName ();
            if ( teachName == "Finish" )
            {
                TeachDay.SetActive ( false );
                PlayerDataManager pm = GameLoopManager.instance.pm;
                GameDataManager gm = GameLoopManager.instance.gm;
                pm.calender = new Calender ();
                pm.activityScheduleLog = new ActivityScheduleLog ();
                pm.employeeScheduleLog = new EmployeeScheduleLog ( gm.workerList.Data.Length , gm.schedulingCabinetList.Data.Length );
                pm.gameLog = new GameLog ( gm.commodityList.Data.Length , pm.storage , pm );
                pm.storeLog = new StoreLog ( gm.commodityList.Data.Length , gm.storeExpansionList.Data.Length , gm.commodityList );

                pm.activityScheduale = new ActivityScheduale ();

                int [] employeeIDs = new int [] { 0 , 1 };
                ActivityScheduale.Scheduale temp = new ActivityScheduale.Scheduale ( gm.activityList.GenerateEvent ( 19 ) , employeeIDs , 19 );
                Action.Activity.ScheduleWithoutChecks ( temp , pm , gm );
                pm.activityScheduleLog.OnGetSchedule ( pm.calender , temp );

                int [] employeeIDs2 = new int [] { 2 };
                ActivityScheduale.Scheduale temp2 = new ActivityScheduale.Scheduale ( gm.activityList.GenerateEvent ( 20 ) , employeeIDs2 , 20 );
                Action.Activity.ScheduleWithoutChecks ( temp2 , pm , gm );
                pm.activityScheduleLog.OnGetSchedule ( pm.calender , temp2 );

                SaveLoadManager.Save ( GameLoopManager.instance.pm , "Teach" );
                AutoSave ( GameLoopManager.instance.pm );
                GameLoopManager.instance.ReadFirstStory = false;
                for ( int i = 0; i < 5; i++ )
                    GameLoopManager.instance.om.unlockActorData.SetUnlock ( i , true );
                GameLoopManager.instance.om.unlockActorData.SetUnlock ( 7 , true );
                GameLoopManager.instance.om.unlockActorData.SetUnlock ( 8 , true );
                SaveLoadManager.OnlyDataSave ( GameLoopManager.instance.om );
                yield return new DramaEvent.ChangeSceneEvent ( "Demo" ).Play ();
                yield break;
            }
            string language = GameLoopManager.instance.om.optionData.GetLanguageStr ();
            DramaPlayerManager manager = new DramaPlayerManager ( $"Data/Story/{language}Teach/" , teachName , true );
            yield return manager.Init ();
        }
    }
    void AutoSave ( PlayerDataManager pm )
    {
        SaveLoadManager.Save ( pm , "Auto" );
        SaveData saveData = new SaveData ();
        saveData.SetData ( pm.calender.GetDay () , pm.playerData.GetMoney () , (int)pm.calender.GetShift () );

        SaveLoadManager.SaveDataSave ( saveData , "Auto" );
        ScreenShot.self.SaveScreenShot ( null , "Auto" );
    }
    
    public void StartStory ( string storyType , string storyPath )
    {
        this.storyPath = storyPath;
        if ( tsm.SkipBtn.activeSelf == true )
            tsm.SkipBtn.SetActive ( false );
        gameObject.SetActive ( true );
        StartCoroutine ( startStory ( storyType , storyPath ) );
    }

    IEnumerator startStory ( string storyType , string storyPath )                                              
    {
        DramaPlayerManager manager = new DramaPlayerManager ( storyType , storyPath , false );
        yield return manager.Init ();
    }

    public void ClearTemp ()
    {
        tempPos = "";
        tempName = "";
    }

    public void PlayAutoEvent ( int index , ref string tempText )
    {
        int start = tempText.IndexOf ( '{' ) + 1;
        int end = tempText.IndexOf ( '}' );
        int lenght = end - start;
        string words = tempText.Substring ( start , lenght );
        StartCoroutine ( new Auto.Manager ().Play ( index , words ) );

        Regex r = new Regex ( "{" );
        tempText = r.Replace ( tempText , "" , 1 );
        r = new Regex ( "}" );
        tempText = r.Replace ( tempText , "" , 1 );
    }
}
