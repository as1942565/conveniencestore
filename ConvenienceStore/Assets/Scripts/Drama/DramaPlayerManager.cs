﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DramaEvent;
using UnityEngine.SceneManagement;

public class DramaPlayerManager
{
    public string storyType;
    public string storyPath;
    public bool teachBool;

    private float vol;


    #if UNITY_EDITOR
    public static bool pass;
    #endif

    public DramaPlayerManager ( string storyType , string storyPath , bool teachBool )
    {
        this.storyType = storyType;
        this.storyPath = storyPath;
        this.teachBool = teachBool;
    }

    public IEnumerator Init ()
    {
        vol = BGMManager.self.audioSource.volume;
        TextAsset textAsset = null;
        textAsset = Resources.Load<TextAsset> ( storyType + storyPath );
        if ( textAsset == null )
        {
            Debug.LogError ( "找不到檔案 : " + storyType + storyPath );
            yield break;
        }
        string drama = textAsset.text;
        string [] StoryText = drama.Split ( '\n' );
        yield return Play ( StoryText );
    }
    public IEnumerator Play ( string [] StoryText )
    {
        yield return null;
        DramaManager.self.CloseLog ();
        int alpha = GameLoopManager.instance.om.optionData.GetTalkBoxAlpha ();
        TalkBoxManager.self.SetTalkBoxAlpha ( alpha );
        StoryPlayer storyPlayer = new StoryPlayer ( storyType , storyPath );
        TeachPlayer teachPlayer = new TeachPlayer ( storyType , storyPath );
        CheckEvent check = new CheckEvent ();
        
        for ( int i = 1; i < StoryText.Length; i++ )
        {
            if ( TeachPlayerManager.skip == true )
            {
                TalkBoxManager.self.StoryText.text = "";
                break;
            }
            #if UNITY_EDITOR
            if ( pass == true )
            {
                pass = false;
                break;
            }
            #endif
            if ( StoryText [i] == "" )
                continue;

            string [] line = StoryText [i].Split ( ',' );
            if ( line [2] == "" )
            {
                continue;
            }

            if ( line [15] == "X" )
                continue;

            if ( line [2].Contains ( "{" ) && line [2].Contains ( "}" ) )
            {
                check.events = line;
                check.line = i + 1;
                yield return check.Play ();
                if ( check.clearActor )
                {
                    StoryManager.self.tempName = "";
                    StoryManager.self.tempPos = "";
                }
                continue;
            }

            line [2] = line [2].Replace ( '[' , '{' ).Replace ( ']' , '}' ).Replace ( "*" , "," ).Replace ( "，" , "," );
            line [2] = line [2].Replace ( "\"\"\"" , "\"" );
            if ( !StoryManager.self.TeachBool )
            {
                yield return Actor ( line , i );
                yield return storyPlayer.WriterText ( i , line [2] , line [1] );
            }
            else
            {
                yield return TeachActor ( line , i );
                yield return teachPlayer.TeachWriterText ( i , line [2] , line [12] , line [13] );
            }
        }
        yield return EndStory ();
    }
    IEnumerator EndStory ()
    {
        Transform background = StoryManager.self.ssm.Background;
        Transform [] parent = background.GetComponentsInChildren<Transform> ();
        for ( int i = 1; i < parent.Length; i++ )
        {
            GameObject.Destroy ( parent [i].gameObject , 1f );
        }

        if ( GameLoopManager.instance.WatchStory == true )
        {
            yield return new WaitForSeconds ( 0.5f );
            SceneManager.LoadScene ( "GameStart" );
            GameLoopManager.instance.om.audioData.SetStoryVol ( vol );
            yield break;
        }

        DramaManager.self.ClearAutoAndSkip ();
        if ( teachBool == false )
        {
            DramaManager.self.ClearAutoAndSkip ();
            yield return new FadeBGMEvent ( 0 , 0.5f ).Play ();
            LoadingScene.self.Transitions ( 0.5f , 1 , true );

            if ( StoryManager.self.ReadEndDrama == true )
            {
                StoryManager.self.ReadEndDrama = false;
                SceneManager.LoadScene ( "GameStart" );
            }
            yield return new WaitForSeconds ( 0.5f );

            yield return new DeletePictureEvent ().Play ();

            StoryManager.self.gameObject.SetActive ( false );
            TalkBoxManager.self.TalkBox.gameObject.SetActive ( false );

            StoryManager.self.ReadWorkerDrama = false;
            StoryManager.self.ReadExpantionDrama = false;
            StoryManager.self.ReadActivityDrama = false;
            GameLoopManager.ReadStory = false;
            GameLoopManager.Trouble = false;
            GameLoopManager.SpecialDay = false;
            
            if ( StoryManager.self.WaitForDramaEnd == true )
                yield break;
            if ( GameLoopManager.SpecialDay == true )
                yield break;

            GameLoopManager.instance.om.audioData.SetStoryVol ( vol );
            BGMManager.self.PlayShiftBGM ();
        }
        yield return null;
    }
    IEnumerator Actor ( string [] line , int index )
    {
        TalkBoxManager.self.ActorName.text = line [1];
        if ( line [3] != "" )
        {
            var e = new CreateActorEvent ( line , StoryManager.self.tempPos , StoryManager.self.tempName , index );
            yield return e.Play ();
        }
        StoryManager.self.tempName = line [1];
        StoryManager.self.tempPos = line [3];
        TalkBoxManager.self.ActorNameBox.SetActive ( line [0] != "旁白" );

        bool sound = false;
        if ( int.TryParse ( line [0] , out int temp ) )
        {
            sound = true;
            if ( line [0].Length == 1 )
                line [0] = "00" + line [0];
            else if ( line [0].Length == 2 )
                line [0] = "0" + line [0];
            Debug.Log ( storyPath + " " + line [0] );
            yield return new PlaySoundEvent ( storyPath , line [0] ).Play ();
        }
        StoryLogManager.self.AddLog ( line [2] , line [1] , sound , line [0] );
    }

    private const string path = "Sprites/Common/Drama/Actor/";
    IEnumerator TeachActor ( string [] line , int index )
    {
        string name = line [1];
        string faceID = line [6];
        if ( faceID == "" )
            faceID = "01";
        if ( name == "端木壹" )
            StoryManager.self.tsm.Actor.sprite = GetSprite ( $"{path}3/0/Actor{faceID}" , index );
        else
            StoryManager.self.tsm.Actor.sprite = GetSprite ( $"{path}0/0/Actor{faceID}" , index );
        yield return null;
    }
    Sprite GetSprite ( string path , int line )
    {
        Sprite spr;
        if ( StorySceneManager.ActorSprite.TryGetValue ( path , out spr ) == false )
        {
            Sprite s = Resources.Load<Sprite> ( path );
            if ( s == null )
            {
                Debug.LogError ( line + ": " + path + "路徑不正確" );
                return SpritePath.None;
            }
            StorySceneManager.ActorSprite.Add ( path , s );
            spr = s;
        }
        return spr;
    }
}
