﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeachPlayerManager : MonoBehaviour
{
    [HideInInspector]
    public bool ReadFinish = false;

    public static bool skip;
    public static TeachPlayerManager self;
    private void Awake ()
    {
        self = this;
    }

    private GameData.TeachData [] data;

    private int index = 0;

    public void SetStoryName ()
    {
        StartCoroutine ( ReadData () );
    }
    public string GetStoryName ()
    {
        if ( index == 0 && GameLoopManager.SkipPrologue == true )
            index++;

        if ( index >= data.Length )
            return "Finish";

        string name = data[index].teachName;
        index++;
        return name;
    }

    IEnumerator ReadData ()
    {
        ReadFinish = false;
        if ( data == null )
            data = GameLoopManager.instance.gm.teachDataList.Data;
        while ( data [index].skip )
        {
            SkipDrama ( index );
            index++;
            if ( index >= data.Length )
                yield break;
            yield return null;
        }
        yield return null;
        ReadFinish = true;
    }

    public void SkipDrama ( int index )
    {
        PlayerDataManager pm = GameLoopManager.instance.pm;
        GameDataManager gm = GameLoopManager.instance.gm;
        NextShiftProcessor nextShift = GameLoopManager.instance.nextShift;

        //0.序章
        if ( index == 0 )
        {

        }
        //1.開場故事
        else if ( index == 1 )
        {

        }
        //2.前導教學
        else if ( index == 2 )
        {

        }
        //3.研發食品
        else if ( index == 3 )
        {
            pm.playerData.AddMoney ( -500 );
            nextShift.Research ( 2 );
        }
        //4.食品上架
        else if ( index == 4 )
        {
            pm.cabinet.SetCommodityID ( 4 , 0 , 2 );
        }
        //5.研發飲品
        else if ( index == 5 )
        {
            pm.playerData.AddMoney ( -500 );
            nextShift.Research ( 19 );
        }
        //6.飲品上架
        else if ( index == 6 )
        {
            pm.cabinet.SetCommodityID ( 4 , 0 , 19 );
            pm.cabinet.SetCommodityID ( 0 , 0 , 2 );
            UI.MainPanel.self.SwitchPanel ( 0 );
        }
        //7.研發雜貨
        else if ( index == 7 )
        {
            UI.MainPanel.self.Close ();
            pm.playerData.AddMoney ( -500 );
            nextShift.Research ( 34 );
            int quantity = pm.storage.GetQuantity ( 2 );
            int money = quantity * gm.commodityList.Data [2].price [0];
            pm.playerData.AddMoney ( -money );
            pm.storage.SetQuantity ( 2 , 0 );
            pm.cabinet.SetCommodityID ( 0 , 0 , -1 );
            GameLoopManager.WaitStoryLog = true;
        }

        //8.今日銷售
        else if ( index == 8 )
        {
            UI.MainPanel.self.SwitchPanel ( 3 );
            GameLoopManager.WaitStoryLog = false;
        }
        //9.員工排班
        else if ( index == 9 )
        {
            int today = pm.calender.GetDay ();
            pm.employeeScheduleLog.SetEmployeeID ( today , 0 , 0 );
            pm.employeeScheduleLog.SetEmployeeID ( today , 1 , 1 );
            pm.employeeScheduleLog.SetEmployeeID ( today , 2 , 2 );
            UI.MainPanel.self.Close ();
        }
        //10.商品進貨
        else if ( index == 10 )
        {
            int totalShift = pm.calender.TotalShift;
            pm.storage.AddQuantity ( 2 , 60 , totalShift );
            pm.playerData.AddMoney ( -gm.commodityList.Data [2].price [0] * 60 );
            pm.storage.AddQuantity ( 19 , 35 , totalShift );
            pm.playerData.AddMoney ( -gm.commodityList.Data [19].price [0] * 35 );
            pm.storage.AddQuantity ( 34 , 20 , totalShift );
            pm.playerData.AddMoney ( -gm.commodityList.Data [34].price [0] * 20 );
            UI.MainPanel.self.SwitchPanel ( 0 );
        }
        //11.食品、雜貨上架
        else if ( index == 11 )
        {
            pm.cabinet.SetCommodityID ( 4 , 0 , 2 );
            pm.cabinet.SetCommodityID ( 1 , 0 , 19 );
            pm.cabinet.SetCommodityID ( 2 , 0 , 34 );
            UI.MainPanel.self.SwitchPanel ( 0 );
        }
        //12.研發食品
        else if ( index == 12 )
        {
            pm.playerData.AddMoney ( -1000 );
            nextShift.Research ( 4 );
            TeachSceneManager.researchID = 4;
            UI.MainPanel.self.SwitchPanel ( 0 );
        }
        //13.食品上架
        else if ( index == 13 )
        {
            pm.cabinet.SetCommodityID ( 0 , 0 , 4 );
            UI.MainPanel.self.SwitchPanel ( 0 );
        }
        //14.食品改良
        else if ( index == 14 )
        {
            for ( int i = 0; i < 3; i++ )
                pm.commodityItem.RanPopular ( 2 , i , 100 , 0 );
            Action.Upgrade.Run ( 2 , pm , gm , nextShift );
            UI.MainPanel.self.SwitchPanel ( 0 );
        }
        //15.研發飲品
        else if ( index == 15 )
        {
            pm.playerData.AddMoney ( -1000 );
            nextShift.Research ( 23 );
            TeachSceneManager.researchID = 23;
            UI.MainPanel.self.SwitchPanel ( 0 );
        }
        //16.飲品上架
        else if ( index == 16 )
        {
            pm.cabinet.SetCommodityID ( 1 , 1 , 23 );
            UI.MainPanel.self.SwitchPanel ( 0 );
        }
        //17.研發雜貨
        else if ( index == 17 )
        {
            pm.playerData.AddMoney ( -1000 );
            nextShift.Research ( 40 );
            TeachSceneManager.researchID = 40;
            UI.MainPanel.self.Close ();
            GameLoopManager.WaitStoryLog = true;
        }
        //18.今日銷售
        else if ( index == 18 )
        {
            UI.MainPanel.self.SwitchPanel ( 3 );
            GameLoopManager.WaitStoryLog = false;
        }
        //19.員工排班
        else if ( index == 19 )
        {
            int today = pm.calender.GetDay ();
            pm.employeeScheduleLog.SetEmployeeID ( today , 0 , 0 );
            pm.employeeScheduleLog.SetEmployeeID ( today , 1 , 1 );
            pm.employeeScheduleLog.SetEmployeeID ( today , 2 , 2 );
            UI.MainPanel.self.Close ();
        }
        //20.自由進貨
        else if ( index == 20 )
        {
            int totalShift = pm.calender.TotalShift;
            int quantityFood = 30 - pm.storage.GetQuantity ( 4 );
            if ( quantityFood > 0 )
            {
                pm.storage.AddQuantity ( 4 , quantityFood , totalShift );
                pm.playerData.AddMoney ( -gm.commodityList.Data [4].price [0] * quantityFood );
            }
            int quantityDrink = 30 - pm.storage.GetQuantity ( 23 );
            if ( quantityDrink > 0 )
            {
                pm.storage.AddQuantity ( 23 , quantityDrink , totalShift );
                pm.playerData.AddMoney ( -gm.commodityList.Data [23].price [0] * quantityDrink );
            }
            UI.MainPanel.self.SwitchPanel ( 0 );
        }
        //21.自由擺放
        else if ( index == 21 )
        {
            pm.cabinet.SetCommodityID ( 2 , 2 , 40 );
        }
        //22.檢查改良
        else if ( index == 22 )
        {
            UI.MainPanel.self.Close ();
        }
        //23.招募員工
        else if ( index == 23 )
        {
            Action.HireEmployee.Run ( 3 , pm , gm );
            pm.calender.UseEnergy ();
        }
        //24.等待
        else if ( index == 24 )
        {

        }
        //25.招募員工
        else if ( index == 25 )
        {
            Action.HireEmployee.Run ( 4 , pm , gm );
            pm.calender.UseEnergy ();
        }
        //26.員工活動
        
        else if ( index == 26 )
        {
            int totalShift = pm.calender.TotalShift;
            int [] employeeIDs = new int [] { 2 };
            ActivityScheduale.Scheduale temp = new ActivityScheduale.Scheduale ( gm.activityList.GenerateEvent ( 16 ) , employeeIDs , 16 );
            temp.SetMethod ( ActivityScheduale.Scheduale.SchedualeMethod.boss );
            temp.SetSuccessed ( true );
            temp.SetBonus ( 500 );
            Action.Activity.Run ( temp , pm , gm );
            pm.activityScheduleLog.OnGetSchedule ( pm.calender , temp );
            GameLoopManager.WaitStoryLog = true;
            pm.calender.UseEnergy ();
        }
        //27.今日銷售
        else if ( index == 27 )
        {
            GameLoopManager.WaitStoryLog = false;
        }
        //28.員工排班
        else if ( index == 28 )
        {
            int today = pm.calender.GetDay ();
            pm.employeeScheduleLog.SetEmployeeID ( today , 0 , 0 );
            pm.employeeScheduleLog.SetEmployeeID ( today , 3 , 1 );
            pm.employeeScheduleLog.SetEmployeeID ( today , 4 , 2 );
            UI.MainPanel.self.Close ();
        }
        //29.自由進貨
        else if ( index == 29 )
        {

        }
        //30.商品上架
        else if ( index == 30 )
        {
            pm.cabinet.SetCommodityID ( 4 , 0 , 2 );
            pm.cabinet.SetCommodityID ( 0 , 0 , 4 );
            pm.cabinet.SetCommodityID ( 1 , 0 , 19 );
            pm.cabinet.SetCommodityID ( 1 , 1 , 23 );
            pm.cabinet.SetCommodityID ( 2 , 0 , 34 );
            pm.cabinet.SetCommodityID ( 2 , 1 , 40 );
        }
        //31.檢查改良
        else if ( index == 31 )
        {

        }
        //32.員工活動
        else if ( index == 32 )
        {
            int totalShift = pm.calender.TotalShift;

            List<ActivityScheduale.Scheduale> scheduales = new List<ActivityScheduale.Scheduale> ();

            int [] employeeIDs = new int [] { 0 , 1 };
            ActivityScheduale.Scheduale temp = new ActivityScheduale.Scheduale ( gm.activityList.GenerateEvent ( 13 ) , employeeIDs , 13 );
            temp.SetMethod ( ActivityScheduale.Scheduale.SchedualeMethod.money );
            temp.SetExcuteShift ( 2 );
            temp.SetSuccessed ( true );
            temp.SetBonus ( 500 );
            scheduales.Add ( temp );
            for ( int i = 0; i < scheduales.Count; i++ )
            {
                Action.Activity.Run ( scheduales [i] , pm , gm );
                pm.activityScheduleLog.OnGetSchedule ( pm.calender , scheduales [i] );
            }
        }
        //33.還債1
        else if ( index == 33 )
        {
            pm.playerData.AddMoney ( -10000 );
            int totalShift = pm.calender.TotalShift;
            pm.gameLog.AddBusiness ( totalShift , 10000 );

            GameLoopManager.instance.nextShift.Business ( 0 );
        }
        //34.髒亂事件
        else if ( index == 34 )
        {

        }
        //35.店面擴建
        else if ( index == 35 )
        {
            Action.StoreExpansion.Run ( new int [] { 19 } , pm , gm , nextShift );
        }
        //36.休息
        else if ( index == 36 )
        {

        }
        //37.今日銷售
        else if ( index == 37 )
        {
            pm.calender = new Calender ();
            pm.activityScheduleLog = new ActivityScheduleLog ();
            pm.employeeScheduleLog = new EmployeeScheduleLog ( gm.workerList.Data.Length , gm.schedulingCabinetList.Data.Length );
            pm.gameLog = new GameLog ( gm.commodityList.Data.Length , pm.storage , pm );
            pm.storeLog = new StoreLog ( gm.commodityList.Data.Length , gm.storeExpansionList.Data.Length , gm.commodityList );
            GameLoopManager.instance.ReadFirstStory = false;
            SaveLoadManager.OnlyDataSave ( GameLoopManager.instance.om );

            pm.activityScheduale = new ActivityScheduale ();

            int [] employeeIDs = new int [] { 0 , 1 };
            ActivityScheduale.Scheduale temp = new ActivityScheduale.Scheduale ( gm.activityList.GenerateEvent ( 19 ) , employeeIDs , 19 );
            Action.Activity.ScheduleWithoutChecks ( temp , pm , gm );
            pm.activityScheduleLog.OnGetSchedule ( pm.calender ,temp );

            int [] employeeIDs2 = new int [] { 2 };
            ActivityScheduale.Scheduale temp2 = new ActivityScheduale.Scheduale ( gm.activityList.GenerateEvent ( 20 ) , employeeIDs2 , 20 );
            Action.Activity.ScheduleWithoutChecks ( temp2 , pm , gm );
            pm.activityScheduleLog.OnGetSchedule ( pm.calender ,temp2 );


            SaveLoadManager.Save ( GameLoopManager.instance.pm , "Teach" );
        }
    }
    private const string path = "GameUICanvas/MainAction/BuildCommodity";
    void OnOpenResearch ()
    {
        UI.BuildLog.self.OnClosed -= OnOpenResearch;
        UI.MainPanel.self.SwitchPanel ( 0 );
        UI.BuildMenuItem menuItem = GameObject.Find ( path ).GetComponent<UI.BuildMenuItem> ();
        menuItem.SwitchType ( 0 );
    }
    public void ClearIndex ()
    {
        index = 0;
    }

    public int GetIndex ()
    {
        return index;
    }

    public void SetIndex ( int index )
    {
        this.index = index;
    }
}
