﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Text.RegularExpressions;

namespace DramaEvent
{
    public class TeachPlayer
    {
        private string teachPath;
        private string teachName;
        public TeachPlayer( string teachPath , string teachName )
        {
            this.teachPath = teachPath;
            this.teachName = teachName;
        }
        
        private float KeySpeed = 0.05f;
        private float MaxKeySpeed;
        private bool pass;

        public IEnumerator TeachWriterText ( int index , string tempText , string targetPath , string targetName )
        {
            EnableButtonEvent button = null;
            if ( targetPath != "" )
            {
                button = new EnableButtonEvent ();
                button.path = targetPath + targetName;
                button.targetBool = false;
                yield return button.Play ();
            }
            Text text = StoryManager.self.tsm.TeachText;
            text.text = "";
            int SpecialNum = 0;
            bool colorBool = false;
            string [] colorText = new string [] { };
            bool skip = false;
            bool skipWord = false;
            foreach ( char word in tempText )
            {
                pass = false;
                while ( skip == false || StoryManager.self.touchBool )
                {
                    if ( TeachPlayerManager.skip )
                        break;

                    if ( !StoryManager.self.touchBool )
                    {
                        if ( TouchButton ( ref pass ) )
                        {
                            break;
                        }
                        if ( Input.GetMouseButtonUp ( 0 ) )
                        {
                            skip = true;
                            break;
                        }
#if UNITY_EDITOR
                        if ( Input.GetKey ( KeyCode.Space ) )
                        {
                            skip = true;
                            break;
                        }
#endif
                    }
                    while ( StoryManager.self.touchBool )
                    {
                        yield return null;
                    }
                    if ( Time.time > MaxKeySpeed )                      //該出現下一個字的時候	
                        break;
                    yield return null;
                }
                
                if ( !colorBool )
                {
                    if ( word == '<' )
                    {
                        colorBool = true;
                        colorText = ColorWord ( ref tempText );
                        continue;
                    }
                    else if ( word == '>' )
                    {
                        SpecialNum = 0;
                        continue;
                    }
                    else if ( word == 'X' && GameLoopManager.instance.om.optionData.GetLanguage () == 0 || word == 'Ｘ' && GameLoopManager.instance.om.optionData.GetLanguage () != 0 )
                    {
                        text.text += "    ";
                        string path = "Sprites/Common/Drama/Teach/";
                        string name = "Fork";
                        string space = "StoryCanvas/Drama/TeachSceneManager(Clone)/TeachTalkBox";
                        Vector2 pos = new Vector2 ( -248 , -17 );
                        Vector2 size = new Vector2 ( 35 , 35 );
                        yield return new CreateSpriteEvent ( path , name , space , pos , size ).Play ();
                        continue;
                    }
                    else if ( word == '#' )
                    {
                        text.text += "    ";
                        string path = "Sprites/Common/Drama/Teach/";
                        string name = "Curcle";
                        string space = "StoryCanvas/Drama/TeachSceneManager(Clone)/TeachTalkBox";
                        Vector2 pos = new Vector2 ( -248 , -17 );
                        Vector2 size = new Vector2 ( 35 , 35 );
                        yield return new CreateSpriteEvent ( path , name , space , pos , size ).Play ();
                        continue;
                    }
                    else if ( word == '^' )
                    {
                        text.text += '\n';
                        continue;
                    }
                    if ( word == '{' )
                    {
                        skip = false;
                        skipWord = true;
                        StoryManager.self.PlayAutoEvent ( index , ref tempText );
                        continue;
                    }
                    if ( word == '}' )
                    {
                        skipWord = false;
                        continue;
                    }
                    else
                    {
                        if ( skipWord == false )
                            text.text += word;                              //增加下一個字
                        else
                            continue; 
                    }
                }
                else
                {
                    if ( SpecialNum >= colorText.Length )
                    {
                        Debug.LogError ( SpecialNum + "超過陣列長度" );
                        SpecialNum = 0;
                    }
                    text.text += colorText [SpecialNum];
                    SpecialNum++;
                    colorBool = colorText.Length > SpecialNum;
                }

                float speed = KeySpeed / GameLoopManager.instance.om.optionData.GetKeyWordSpeed () * 50;        
                MaxKeySpeed = Time.time + speed;//更新冷卻時間
            }
            if ( TeachPlayerManager.skip == false )
                yield return new WaitForSeconds ( 0.01f );
            if ( button == null )
            {
                while ( true )
                {
                    if ( Input.GetMouseButtonUp ( 0 ) )
                    {
                        break;
                    }
#if UNITY_EDITOR
                    if ( Input.GetKey ( KeyCode.Space ) )
                    {
                        break;
                    }
#endif
                    if ( StoryManager.self.tsm.pass )
                    {
                        StoryManager.self.tsm.pass = false;
                        break;
                    }
                    yield return null;
                }
            }
            else
            {
                while ( button.targetBool == false )
                {
                    yield return null;
                }
            }
            
            if ( targetPath != "" )
            {
                DisableButtonEvent disableButton = new DisableButtonEvent ();
                disableButton.path = targetPath + targetName;
                yield return disableButton.Play ();
            }
            yield return null;
        }

        string [] ColorWord (ref string textTemp )
        {
            int start = textTemp.IndexOf ( '<' ) + 1;                  //找到<的數字後+1(不然會抓到[)
            int end = textTemp.IndexOf ( '>' );                        //找到>的數字
            int lenght = end - start;                               //<>內的文字數
            string words = textTemp.Substring ( start , lenght );   //取得<>內的文字
            string [] SWords = new string [words.Length];           //根據文字長度來開陣列

            string colorStr = "<color=#fed700>";

            if ( GameLoopManager.instance.om.optionData.GetLanguage () == 0 )
            {
                for ( int i = 0; i < words.Length; i++ )
                {
                    if ( words [i] == 'G' )
                    {
                        colorStr = "<color=#7fff17>";
                    }
                    else if ( words [i] == 'B' )
                    {
                        colorStr = "<color=#00fcf3>";
                    }
                    else if ( words [i] == 'O' )
                    {
                        colorStr = "<color=#ff8400>";
                    }
                    else if ( words [i] == 'P' )
                    {
                        colorStr = "<color=#ff77d1>";
                    }
                    else
                    {
                        SWords [i] = colorStr + textTemp.Substring ( start + i , 1 ) + "</color>";
                    }
                }
            }
            else
            {
                for ( int i = 0; i < words.Length; i++ )
                {
                    if ( words [i] == 'Ｇ' )
                    {
                        colorStr = "<color=#7fff17>";
                    }
                    else if ( words [i] == 'Ｂ' )
                    {
                        colorStr = "<color=#00fcf3>";
                    }
                    else if ( words [i] == 'Ｏ' )
                    {
                        colorStr = "<color=#ff8400>";
                    }
                    else if ( words [i] == 'Ｐ' )
                    {
                        colorStr = "<color=#ff77d1>";
                    }
                    else
                    {
                        SWords [i] = colorStr + textTemp.Substring ( start + i , 1 ) + "</color>";
                    }
                }

            }
            Regex r = new Regex ( "<" );
            textTemp = r.Replace ( textTemp , "" , 1 );
            r = new Regex ( ">" );
            textTemp = r.Replace ( textTemp , "" , 1 );
            return SWords;                                      //回傳
        }

        bool TouchButton ( ref bool pass )
        {
            if ( Input.GetMouseButtonUp ( 0 ) )
            {
                PointerEventData eventData = new PointerEventData ( StoryManager.self.eventSystem );
                eventData.position = Input.mousePosition;

                GraphicRaycaster raycaster = StoryManager.self.graphicsRaycaster;
                List<RaycastResult> result = new List<RaycastResult> ();
                raycaster.Raycast ( eventData , result );
                if ( result.Count == 0 )
                {
                    pass = true;
                    return pass;
                }
                Button button = null;
                button = result [0].gameObject.GetComponent<Button> ();
                pass = button == null;
            }
            return pass;
        }
    }
}
