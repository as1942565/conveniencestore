﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TalkBoxManager : MonoBehaviour
{
    public Image TalkBox;                       //對話框
    public Text StoryText;                      //故事用文字
    public Text ActorName;                      //角色名稱文字
    public GameObject ActorNameBox;             //角色名稱框

    [HideInInspector]
    public bool closeUI = false;

    public static TalkBoxManager self;
    private void Awake ()
    {
        self = this;
    }

    private void Update ()
    {
        if ( closeUI == true && Input.GetMouseButtonDown ( 0 ) )
        {
            closeUI = false;
            TalkBox.gameObject.SetActive ( true );
        }
    }

    public void SetTalkBoxAlpha ( int alpha )
    {
        Color32 color = TalkBox.color;
        color.a = (byte)( 255 * alpha / 100 );
        TalkBox.color = color;
    }


}
