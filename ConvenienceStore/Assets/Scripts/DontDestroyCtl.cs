﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//不要刪除的物件們
public class DontDestroyCtl : MonoBehaviour        
{          
    private static bool OnceBool = false;           

    void Awake ()
    {
        if ( !OnceBool )
        {
            DontDestroyOnLoad ( gameObject );
            OnceBool = true;
        }
        SceneManager.LoadScene ( 1 );
    }
}
