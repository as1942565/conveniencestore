﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;

namespace Simulation
{
    public class SimulationLog
    {
        public StringBuilder keyWord = new StringBuilder ();
        public StringBuilder sb;

        public void Init ()
        {
            sb = new StringBuilder ();
            sb.Append ( "人氣值\t清潔度\t治安度\t天數\t班次\t行為種類\t名稱\t玩家金錢\t商品賺錢\t收入加成(食品)\t收入加成(飲品)\t收入加成(雜貨)\t商品銷量\n" );
        }

        public void Log ( string Dostuff , PlayerDataManager pm , GameDataManager gm )
        {
            if ( pm.calender.GetDay () < Calender.MaximumDay )
            {
                int extraclean = 0;
                int extrapolice = 0;
                if ( Dostuff.Contains ( "活動:" ) )
                {
                    string l = Dostuff.Split ( '\t' ) [1].Replace("+","");
                    if ( l.Contains ( "清" ) )
                    {
                       extraclean = int.Parse( l.Replace ( "清" , "" ));
                    }else if ( l.Contains ( "治" ))
                    {
                       extrapolice = int.Parse( l.Replace ( "治" , "" ));
                    }            
                }

                string popular = pm.playerData.GetPopular ().ToString ( "N0" );
                int cleanLess = Formula.Clean.ReduceCleanAmount ( pm ); 
                string less = cleanLess.ToString ();
                string addClean = (Formula.Clean.AddClean ( pm )+extraclean).ToString ();
                string clean = $"{pm.playerData.GetClean ().ToString ( "N0" )}(-{less}+{addClean})";
                string addPolice = (Formula.Police.AddPolice ( pm )+extrapolice).ToString ();
                int policeLess = Formula.Police.ReducePoliceAmount ( pm );
                string police = $"{pm.playerData.GetPolice ().ToString ( "N0" )}(-{less}+{addPolice})";
                int totalShift = pm.calender.TotalShift;
                int shift = (int)pm.calender.GetShift ();
                string [] shiftName = new string [] { "早班" , "晚班" , "大夜" };
                string playerMoney = pm.playerData.GetMoney ().ToString ( "N0" );
                string commodityIncome = pm.gameLog.GetTotalIncome ( totalShift , gm ).ToString ( "N0" );
                string foodIncome = Formula.CommodityIncome.Calculate ( GameData.Commodity.CommodityType.food , pm ).ToString ();
                string drinkIncome = Formula.CommodityIncome.Calculate ( GameData.Commodity.CommodityType.drink , pm ).ToString ();
                string groceryIncome = Formula.CommodityIncome.Calculate ( GameData.Commodity.CommodityType.grocery , pm ).ToString ();
                string saleQuantity = CommoditySaleQuantity ( totalShift , gm , pm );
                sb.Append ( $"{popular}\t{clean}\t{police}\t第{( totalShift / 3 + 1 ).ToString ()}天\t{shiftName [shift]}\t{Dostuff}\t{playerMoney}\t{commodityIncome}\t{foodIncome}\t{drinkIncome}\t{groceryIncome}\t{saleQuantity}\n" );

                if ( Dostuff == "業務:\t還債3/3" )
                {
                    string [] employeeData = new string [3];
                    string [] commodityStar = new string [3];
                    string [] commoditySale = new string [3];
                    int [] commodityID = new int [] { 16 , 8 , 28 };
                    for ( int i = 0; i < employeeData.Length; i++ )
                    {
                        employeeData [i] = $"{gm.workerList.Data [i].name}的好感度等級 : {pm.employeeList.GetEmployee ( i ).GetRelationRank ().ToString ()}";
                        int id = commodityID [i];
                        //commodityStar [i] = $"{gm.commodityList.Data [id].name}的星級 : {pm.commodityItem.GetData ( id ).star.ToString ()}";
                        commodityStar [i] = $"{Localizer.Translate ( gm.commodityList.Data [id].name )}的星級 : {pm.commodityItem.GetData ( id ).star.ToString ()}";
                        commoditySale [i] = $"目前販售數 : {pm.commodityItem.GetData ( id ).saleQuantity}";
                    }
                    keyWord.Append ( $"第{pm.calender.GetDay () + 1}天{shiftName [shift]}還債3\t{employeeData [0]}\t{commodityStar [0]}\t{commoditySale [0]}\t{employeeData [1]}\t{commodityStar [1]}\t{commoditySale [1]}\t{employeeData [2]}\t{commodityStar [2]}\t{commoditySale [2]}\t" );
                }
                if ( Dostuff == "擴建\t特殊商品櫃" )
                {
                    keyWord.Append ( $"第{pm.calender.GetDay () + 1}天{shiftName [shift]}擴建特殊商品櫃" );
                }
                int endID = pm.businessUnlock.AnyEndingComplete ();
                if ( endID != -1  )
                {
                    keyWord.Append ( $"第{pm.calender.GetDay () + 1}天{shiftName [shift]}達成{gm.workerList.Data [endID].name}結局" );
                }
            }
        }
        public void SaveKey ( string Filename )
        {
            File.WriteAllText ( Filename , keyWord.ToString () , Encoding.Unicode );
        }
        public void SaveLog ( string Filename )
        {
            File.WriteAllText ( Filename , sb.ToString () , Encoding.Unicode );
        }

        string CommoditySaleQuantity ( int totalShift , GameDataManager gm , PlayerDataManager pm )
        {
            string saleQuantity = "";
            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )
            {
                int quantity = pm.gameLog.GetCommoditySaleQuantity ( totalShift , i );
                if ( quantity != 0 )
                {
                    string [] commodityName = new string [] { "(食)" , "(飲)" , "(雜)" , "(特)" };
                    int type = (int)gm.commodityList.Data [i].type;
                    //saleQuantity += commodityName[type] + gm.commodityList.Data [i].name + quantity.ToString () + "個,";
                    saleQuantity += commodityName[type] + Localizer.Translate ( gm.commodityList.Data [i].name ) + quantity.ToString () + "個,";
                }
            }
            if ( saleQuantity == "" )
                return "";
            return saleQuantity.Substring ( 0 , saleQuantity.Length - 1 );
        }
    }
}
