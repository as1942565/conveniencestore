﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Simulation;

public class SimulationManager
{
    public Restock restock;
    public SetupEmployee setupEmployee;
    public SetupCommodity setupCommodity;
    public Activity activity;

    public Upgrade upgrade;
    public Research research;
    public StoreExpansion storeExpansion;
    public Business business;

    public SimulationLog simulationLog= new SimulationLog ();
   
    public void Init ( PlayerDataManager pm , GameDataManager gm , NextShiftProcessor nextShift )
    {
        restock = new Restock ( pm , gm );
        setupEmployee = new SetupEmployee ( pm , gm );
        setupCommodity = new SetupCommodity ( pm , gm );
        activity = new Activity ( pm , gm );

        upgrade = new Upgrade ( pm , gm , nextShift );
        research = new Research ( pm , gm , nextShift );
        storeExpansion = new StoreExpansion ( pm , gm , nextShift );
        business = new Business ( pm , gm , nextShift );
        simulationLog.Init ();
    }
}
