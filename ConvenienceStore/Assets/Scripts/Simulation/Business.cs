﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tools;
namespace Simulation
{
    public class Business : BaseSimulation
    {
        public enum SimulationType
        {
            Smart =0,
            Random =1,
        }
        public Business ( PlayerDataManager pm , GameDataManager gm , NextShiftProcessor nextShift ) : base ( pm , gm , nextShift )
        {
            actionPriority = new List<actionEnum> ();
            for ( int i = 0; i < 5; i++ )
            {
                actionPriority.Add ( (actionEnum)i );
            }
        }

        private enum actionEnum
        {
            debt = 0,
            exploreItem = 1,
            hire = 2,
            thanks = 3,
            activity = 4,
        }

        private int activityOrder;
        private int CanUseMoney;
        private List<actionEnum> actionPriority;

        public string Run ( SimulationType type )
        {                                                                       //
            CanUseMoney = pm.playerData.GetMoney () / 2;                        //紀錄可使用的金額
            List<int> NoAPID = new List<int> ();
            for ( int i = 0; i < gm.businessList.Data.Length; i++ )
            {
                if ( gm.businessList.Data [i].eventType != 0 && pm.businessUnlock.GetMode ( i ) != BusinessUnlock.state.hide )
                    NoAPID.Add ( i );
            }
            for ( int i = 0; i < NoAPID.Count; i++ )
            {
                int id = NoAPID [i];
                if ( pm.businessUnlock.GetUnlock ( id ) && pm.businessUnlock.GetMode ( id ) != BusinessUnlock.state.watch )
                    Action.Business.Run ( id , pm , gm , nextShift );
            }
            switch ( type )
            {
                case SimulationType.Smart:
                    return SmartStrategy ();
                default:
                    return RandomStrategy ();
            }
        }

        private string SmartStrategy ()
        {
            string s = "";
            s = debt ();
            if ( s == "" ) { s = exploreItem (); }
            if ( s == "" ) { s = hire (); }
            if ( s == "" ) { s = thanks (); }
            if ( s == "" ) { s = activity (); }
            return s;
        }

        private string RandomStrategy ()
        {
            actionPriority.Shuffle ();
            foreach ( actionEnum ac in actionPriority )
            {
                string s;
                switch ( ac )
                {
                    case actionEnum.debt:
                        s = debt ();
                        if ( s != "" )
                        {
                            return s;
                        }
                        break;
                    case actionEnum.exploreItem:
                        s = exploreItem ();
                        if ( s != "" )
                        {
                            return s;
                        }
                        break;
                    case actionEnum.hire:
                        s = hire ();
                        if ( s != "" )
                        {
                            return s;
                        }
                        break;
                    case actionEnum.thanks:
                        s = thanks ();
                        if ( s != "" )
                        {
                            return s;
                        }
                        break;
                    case actionEnum.activity:

                        s = activity ();
                        if ( s != "" )
                        {
                            return s;
                        }
                        break;
                }
            }
            return "";
        }

        private string debt ()
        {
            for ( int i = 0; i < 3; i++ )                                       //偵測還債1 2 3
            {
                if ( pm.businessUnlock.GetUnlock ( i ) && pm.businessUnlock.GetMode ( i ) == BusinessUnlock.state.Available )//如果還債已解鎖但未完成
                {
                    int spend = gm.businessList.Data [i].cost;                  //紀錄還債金額
                    if ( CanUseMoney >= spend )                                 //如果可以還債
                    {
                        Action.Business.Run ( i , pm , gm , nextShift );        //還債
                        return "業務:\t" + gm.businessList.Data [i].name;
                    }
                    //( "(金錢不足) 玩家可使用的金額為 : " + CanUseMoney + "，無法執行" + gm.businessList.Data [i].name );
                }
            }
            return "";
        }
        //如果探索特殊商品已解鎖 而且 特殊商品的數量低於10個
        private string exploreItem ()
        {
            if ( pm.businessUnlock.GetUnlock ( 9 ) && pm.storage.GetSpecialQuantity ( gm ) < 10 )//如果探索特殊商品已解鎖 而且 特殊商品的數量低於10個
            {
                int spend = gm.businessList.Data [9].cost;                      //紀錄探索商品金額
                if ( CanUseMoney > spend )                                      //如果金錢足夠探索商品
                {
                    Action.Business.Run ( 9 , pm , gm , nextShift );            //探索特殊商品
                    return "業務:\t" + gm.businessList.Data [9].name;
                }
            }
            if ( !pm.businessUnlock.GetUnlock ( 9 ) )
                return "";
            if ( pm.storage.GetSpecialQuantity ( gm ) >= 10 )
                return "";
            return "";
        }
        //員工低於10個就招募工讀生
        private string hire ()
        {
            for ( int i = 11; i > 9; i-- )
            {
                if ( pm.businessUnlock.GetUnlock ( i ) && pm.employeeList.GetAllUnlockID ().Length < 10 )//如果招募工讀生已解鎖但未完成
                {
                    int spend = gm.businessList.Data [i].cost;                  //紀錄招募工讀生金額
                    if ( CanUseMoney < spend )                                  //如果可以招募工讀生
                        continue;
                    Action.Business.Run ( i , pm , gm , nextShift );                                  //招募工讀生
                    List<int> canHireID = pm.hireWorker.GetCanHireID ();
                    int quantity = pm.hireWorker.GetQuantity ();
                    canHireID.Shuffle ();
                    for ( int j = 0; j < quantity; j++ )
                        Action.HireEmployee.Run ( canHireID [j] , pm , gm );
                    return "業務:\t" + gm.businessList.Data [i].name;
                }
            }
            return "";
        }
        //感謝祭活動(有2就不要1)
        private string thanks ()
        {
            for ( int i = 5; i > 3; i-- )
            {
                //TODO Check is unlock
                if ( pm.businessUnlock.GetUnlock ( i ) && pm.businessUnlock.GetMode ( i ) == BusinessUnlock.state.Available )
                {
                    int spend = gm.businessList.Data [i].cost;                  //紀錄感謝祭費用
                    if ( CanUseMoney > spend )                                  //如果金錢足夠
                    {
                        Action.Business.Run ( i , pm , gm , nextShift );        //舉辦感謝祭
                        return "業務:\t" + gm.businessList.Data [i].name;
                    }
                }
            }
            return "";
        }
        //舉行活動 (看哪個商品種類較多)
        private string activity ()
        {
            int tempNum = 4;                                                    //暫存研發數(最低4種)
            int id = -1;                                                        //暫存編號
            int [] activityID = new int [] { 6 , 7 , 8 };
            int tempSpend = 0;
            for ( int i = 0; i < 3; i++ )
            {
                int aid = activityID [i];
                if ( pm.businessUnlock.GetUnlock ( aid ) && pm.businessUnlock.GetMode ( aid ) == BusinessUnlock.state.Available && pm.commodityItem.GetResearchNumByType ( gm , i ) >= tempNum )//如果該業務還沒完成 而且 研發數大於前者
                {
                    if ( CanUseMoney > gm.businessList.Data [aid].cost )        //如果玩家金錢足夠
                    {
                        tempNum = pm.commodityItem.GetResearchNumByType ( gm , i );//更新研發數
                        id = aid;                                               //更新編號
                        tempSpend = gm.businessList.Data [aid].cost;            //更新費用
                    }
                }
            }
            if ( id <= -1 )
            {
                //( "所有研發數都不足4個" );
                return "";
            }
            Action.Business.Run ( id , pm , gm , nextShift );
            return "業務:\t" + gm.businessList.Data [id].name;
        }
    }
}