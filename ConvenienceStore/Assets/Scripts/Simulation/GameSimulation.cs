﻿using GameData;
using System.Collections;
using System.Collections.Generic;
using System.Security.AccessControl;
using Tools;
using UnityEngine;

//遊戲模擬
public class GameSimulation : MonoBehaviour
{
    public static GameSimulation instance;//
    private void Awake ()
    {
        instance = this;
    }
    private GameSimulation ()
    {

    }
    public GameLoopManager manager;                                         //遊戲管理器
    public SimulationManager sm;
    public CheckFormula cf;
    [Header ( "消耗行動點" )]
    [SerializeField]
    private SimulationStrategy actionStrategy = SimulationStrategy.Random;        //模擬方式
    [Header ( "店長業務" )]
    [SerializeField]
    private Simulation.Business.SimulationType businessStrategy = Simulation.Business.SimulationType.Random;        //模擬方式
    [Header ( "商品研發" )]
    [SerializeField]
    private Simulation.Research.SimluationType researchStrategy = Simulation.Research.SimluationType.Random;
    [Header ( "商品改良" )]
    [SerializeField]
    private Simulation.Upgrade.SimulationType upgradeStrategy = Simulation.Upgrade.SimulationType.Random;
    [Header ( "店面擴建" )]
    [SerializeField]
    private Simulation.StoreExpansion.SimulationType storeStrategy = Simulation.StoreExpansion.SimulationType.Random;
    [Header ( "商品進貨" )]
    [SerializeField]
    private Simulation.Restock.SimulationType restockStrategy = Simulation.Restock.SimulationType.Smart;
    [Header ( "商品上架" )]
    [SerializeField]
    private Simulation.SetupCommodity.SimulationType setupCommodityStrategy = Simulation.SetupCommodity.SimulationType.Smart;
    public bool FastEnding;

    private SimulationStrategy tempActionStrategy;
    private Simulation.Research.SimluationType tempResearchStrategy;
    private Simulation.Upgrade.SimulationType tempUpgradeStrategy;

    public enum SimulationStrategy
    {
        Random,                                                             //亂數
        Fixed,                                                              //固定
    }

    void Start ()
    {
        tempResearchStrategy = researchStrategy;
        tempUpgradeStrategy = upgradeStrategy;
        tempActionStrategy = actionStrategy;
        sm = new SimulationManager ();
        Init ();                                                            //初始化
        simulate ();                                                        //模擬
    }

    void Init ()
    {
        employeeID = -1;
        manager.Init ();                                                    //遊戲整體資料初始化
        sm.Init ( manager.pm , manager.gm , manager.nextShift );             //模擬資料初始化
        manager.pm.activityScheduale.OnRunScheduales += AddActivityLog;
        //Teach ();                                                           //教學途中會做到的改變的數值
    }

    void AddActivityLog ( ActivityScheduale.Scheduale s )
    {
        int id = s.ActivityID;
        //string name = manager.gm.activityList.Data [id].name;
        string name = Localizer.Translate ( manager.gm.activityList.Data [id].name );
        List<ActivityEventData>classes = manager.gm.activityList.Data [id].eventData;
        for ( int i = 0; i < classes.Count; i++ )
        {
            if ( classes [i].Event == ActivityEventData.EventType.AddCleanA )
            {
                string dostuff = $"活動:\t+{classes[i].Param [0]}清";
                sm.simulationLog.Log ( dostuff , manager.pm , manager.gm );
            }
            else if (classes[i].Event == ActivityEventData.EventType.AddPoliceA)
            {
                string dostuff = $"活動:\t+{classes[i].Param[0]}治";
                sm.simulationLog.Log ( dostuff , manager.pm , manager.gm );
            }
        }
    }

    string logs;
    int simulationTime;
    Vector2 scrollPosition = Vector2.zero;
    private void OnGUI ()
    {
        GUILayout.Space ( 150 );
        GUILayout.BeginHorizontal ();
        {

            GUILayout.BeginVertical ( GUILayout.Width ( 1300 ) );
            {
                int p = 0;
                if( int.TryParse(GUILayout.TextField ( simulationTime.ToString ()),out p ) )
                {
                    simulationTime = p;
                }
                if ( GUILayout.Button ( "CheckKeyWord" ) )
                {
                    for ( int i = 0; i < simulationTime; i++ )
                    {
                        sm.simulationLog.keyWord.Append ( $"第{i}次模擬結果 : " );
                        Init ();
                        simulate ();
                        sm.simulationLog.keyWord.Append ( "\n" );
                        sm.simulationLog.SaveLog ( $"SimulationLog_{i}.csv" );
                    }
                    sm.simulationLog.SaveKey ( "KeyWord.csv" );
                }
                
                if ( GUILayout.Button ( "1 Loop" ) )
                {
                    Init ();
                    simulate ();
                    logs = sm.simulationLog.sb.ToString ();
                    sm.simulationLog.SaveLog ( "SimulationLog.csv" );

                }
                if ( GUILayout.Button ( "NextSim" ) )
                {
                    if ( manager.pm.calender.GetDay () < Calender.MaximumDay )
                    {
                        nextSim ();
                        logs = sm.simulationLog.sb.ToString ();
                        sm.simulationLog.SaveLog ( "SimulationLog.csv" );
                    }
                }
                if ( GUILayout.Button ( "Reset" ) )                                 //重新設定
                {
                    Init ();
                    //初始化
                    logs = sm.simulationLog.sb.ToString ();
                    cf.UpdateAll ( manager.pm , manager.gm );
                }
                scrollPosition = GUILayout.BeginScrollView ( scrollPosition );
                GUILayout.Label ( logs );
                GUILayout.EndScrollView ();
            }
            GUILayout.EndVertical ();

            GUILayout.BeginVertical ();
            {
                GUILayout.Label ( cf.shiftLog );
                GUILayout.Label ( cf.saleQuantityLog );
                GUILayout.Label ( cf.CommodityIncomeLog );
                GUILayout.Label ( cf.popularLog );
                GUILayout.Label ( cf.policeLog );
                GUILayout.Label ( cf.cleanLog );
            }
            GUILayout.EndVertical ();
        }
        GUILayout.EndHorizontal ();
    }
    
    void simulate ()                                                      //模擬
    {
        int test = 0;
        while ( manager.pm.calender.GetDay () < Calender.MaximumDay )       //30天內一直循環
        {
            test++;
            if ( test > 10000 )
            {
                Debug.LogError ( "模擬無限迴圈" );
                break;
            }
            nextSim ();
            if ( manager.pm.calender.GetDay () == Calender.MaximumDay - 1 && manager.pm.calender.GetShift () == Calender.Shift.night )
            {
                cf.UpdateAll ( manager.pm , manager.gm );
            }

            if ( manager.pm.businessUnlock.AnyEndingComplete () != -1 )
            {
                break;
            }
        }
        //when debt3 ->day
        //解鎖商品
        //員工等級好感度
        //calculate score : 遊戲結算 (是否通關 解鎖甚麼商品 員工等級好感度 玩家金錢 等等)
    }

    void nextSim ()                                                         //模擬一個時段
    {
        if ( FastEnding )
            EndingState ();
        Calender.Shift shift = manager.pm.calender.GetShift ();             //取得現在的班次
        if ( shift == Calender.Shift.morning )                              //如果現在是早班
        {
            restock ();                                                     //進貨
        }
        else if ( shift == Calender.Shift.noon )                            //如果現在是晚班
        {
        }
        else if ( shift == Calender.Shift.night )                           //如果現在是大夜班
        {
            setupEmployee ();                                               //員工排班
        }

        setupCommodity ();                                                  //商品擺放
        activity ();                                                        //盡力去做員工活動

        string s;
        switch ( actionStrategy )
        {
            case SimulationStrategy.Fixed:
                s = FixedStrategy ();
                break;
            case SimulationStrategy.Random:
                s = RandomStrategy ();
                break;
            default:
                s = RandomStrategy ();
                break;
        }
        sm.simulationLog.Log ( s , manager.pm , manager.gm );
        if ( manager.pm.calender.GetDay () < Calender.MaximumDay )
        {
            cf.UpdateAll ( manager.pm , manager.gm );
        }
    }

    private string FixedStrategy ()                                         //固定流程
    {
        string s;
        s = upgrade ();                                                     //商品改良
        if ( s == "" ) { s = research (); }                                 //如果沒辦法商品改良 就做商品研發
        if ( s == "" ) { s = storeExpansion (); }                           //如果沒辦法商品研發 就做店面擴建
        if ( s == "" ) { s = business (); }                                 //如果沒辦法店面擴建 就做店長業務
        if ( s == "" ) { s = skip (); }                                     //如果沒辦法店長業務 就休息
        //else if ( specialActivity () ) { }    
        return s;
    }
    private string RandomStrategy ()                                        //亂數流程
    {
        var actions = new List<System.Func<string>> { upgrade , research , storeExpansion , business };
        //else if ( specialActivity () ) { }    
        actions.Shuffle ();
        string s = "";
        for ( int i = 0; i < actions.Count; i++ )
        {
            s = actions [i] ();
            if ( s != "" )
            {
                return s;
            }
        }
        s = skip ();
        return s;
    }

    string upgrade ()                                                       //商品改良
    {
        return sm.upgrade.Run ( upgradeStrategy );
    }
    string research ()                                                      //商品研發
    {
        return sm.research.Run ( researchStrategy );
    }
    string storeExpansion ()                                                //店面擴建
    {
        return sm.storeExpansion.Run ( storeStrategy );
    }
    string business ()                                                      //店長業務                             
    {
        return sm.business.Run ( businessStrategy );
    }
    /*bool specialActivity ()                                               //員工活動(店長指導)                                                 
    {
        return false;
    }*/
    string skip ()                                                          //純消耗行動點      
    {
        manager.nextShift.Business ( 12 );
        //UI.ShiftMessage.self.ShowShiftMessage ( "「店長業務」休息" );
        return "休息\t";
    }

    void restock ()                                                         //進貨
    {                                                                       //
        sm.restock.Run ( restockStrategy );
    }
    void setupEmployee ()                                                   //員工排班
    {
        sm.setupEmployee.ProRun ();
    }
    void setupCommodity ()                                                  //商品擺放
    {
        sm.setupCommodity.Run ( setupCommodityStrategy );
    }
    void activity ()                                                        //員工活動
    {
        sm.activity.Run ();
    }

    [HideInInspector]
    public int [] commodityID = new int [] { 16 , 8 , 28 };
    public int employeeID = -1;
    void EndingState ()
    {
        if ( employeeID == -1 )
            return;
        int id = commodityID[employeeID];
        if ( !manager.pm.commodityItem.GetData ( id ).unlock )
        {
            researchStrategy = Simulation.Research.SimluationType.ending;
            actionStrategy = SimulationStrategy.Fixed;
        }
        else
            researchStrategy = tempResearchStrategy;

        if ( !manager.pm.commodityItem.GetImproveComplete ( id ) )
        {
            upgradeStrategy = Simulation.Upgrade.SimulationType.ending;
            actionStrategy = SimulationStrategy.Fixed;
        }
        else
        {
            upgradeStrategy = tempUpgradeStrategy;
            actionStrategy = tempActionStrategy;
        }

    }
    public int GetCommodityID ()
    {
        if ( employeeID == -1 )
            return -1;
        return commodityID [employeeID];
    }
}
