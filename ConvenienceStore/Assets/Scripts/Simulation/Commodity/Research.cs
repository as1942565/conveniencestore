﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tools;

namespace Simulation
{
    public class Research : BaseSimulation
    {
        public enum SimluationType
        {
            Smart = 0,
            Random = 1,
            ending = 2,
        }

        public Research ( PlayerDataManager pm , GameDataManager gm , NextShiftProcessor nextShift ) : base ( pm , gm , nextShift )
        {

        }

        public string Run ( SimluationType type )
        {
            List<int> researchQuantity = new List<int> ();
            List<int> researchType = new List<int> ();
            int CanUseMoney = pm.playerData.GetMoney () / 2;                    //紀錄可以使用的金錢
            int commodityTypeQuantity = 8;

            for ( int i = 0; i < 3; i++ )
            {
                int num = pm.commodityItem.GetResearchNumByType ( gm , i );
                if ( num < commodityTypeQuantity )                                             //如果該種類的研發數小於10 (代表還沒研發完畢)
                {
                    researchQuantity.Add ( num );
                    researchType.Add ( i );
                }
            }

            if ( researchQuantity.Count <= 0 )
            {
                //( "全部研發完畢" );
                return "";
            }

            switch ( type )
            {
                case SimluationType.Smart:
                    return SmartStrategy ( researchQuantity , CanUseMoney );
                case SimluationType.ending:
                    return EndingStrategy ();
                default:
                    return RandomStrategy ( researchType , CanUseMoney );
            }

        }
        private string SmartStrategy ( List<int> researchQuantity , int CanUseMoney )
        {
            researchQuantity.Sort ();
            int minQuantity = researchQuantity [0];
            //找出該種類最低能研發的階級
            int minRank = 3;                                                   //最大階級為3
            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )
            {
                GameData.Commodity data = gm.commodityList.Data [i];
                if ( !pm.commodityItem.GetData ( i ).unlock && (int)data.type == minQuantity && data.rank < minRank )
                {
                    minRank = data.rank;
                }
            }
            //檢查玩家金錢的一半是否大於開發費用
            int spend = gm.customData.Data.DevelopMoney [minRank];              //取得研發金額
            if ( CanUseMoney < spend )                                          //如果可使用的金錢 大於 研發金額
            {
                //( "金錢不足" );
                return "";
            }
            if ( minRank >= 3 )
            {
                //( "全部研發完畢" );
                return "";
            }
            return research ( minRank , (GameData.Commodity.CommodityType)minQuantity );
        }
        private string RandomStrategy ( List<int> researchType , int CanUseMoney )
        {
            researchType.Shuffle ();
            int type = researchType [0];
            List<int> commodityRank = new List<int> ();

            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )
            {
                int rank = gm.commodityList.Data [i].rank;
                rank = Mathf.Clamp ( rank , 0 , 2 );        //如果有rank等於3(S級) 把它變成2(高投資額)
                int spend = gm.customData.Data.DevelopMoney [rank];
                if ( (int)gm.commodityList.Data[i].type == type && !pm.commodityItem.GetData ( i ).unlock && CanUseMoney >= spend )
                {
                    commodityRank.Add ( rank );
                }
            }
            if ( commodityRank.Count <= 0 )
                return "";
            commodityRank.Shuffle ();
            commodityRank [0] = Mathf.Clamp ( commodityRank [0] , 0 , 2 );        //如果有rank等於3(S級) 把它變成2(高投資額)
            return research ( commodityRank [0] , (GameData.Commodity.CommodityType)type );
        }
        private string EndingStrategy ()
        {
            int id = GameSimulation.instance.GetCommodityID ();
            if ( pm.commodityItem.GetData ( id ).unlock )
                return "";
            int rank = gm.commodityList.Data [id].rank;
            GameData.Commodity.CommodityType type = gm.commodityList.Data [id].type;
            return research ( rank , type );
        }

        private string research ( int rank , GameData.Commodity.CommodityType commodityType )
        {
            int type = (int)commodityType;
            int id = Action.Research.ResearchID ( rank , (int)commodityType , gm , pm );
            if ( id == -1 )
                return "研發失敗";
            Action.Research.Run ( id , nextShift );          //研發種最少而且最低投資額的商品
            string [] typeName = new string [] { "食" , "飲" , "雜" };
            string [] typeName2 = new string [] { "(小)" , "(中)" , "(大)" };

            int [] money = gm.customData.Data.DevelopMoney;
            return "研發\t" + typeName [type] + typeName2 [rank];
        }
    }
}
