﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tools;

namespace Simulation
{
    public class Upgrade : BaseSimulation
    {
        public enum SimulationType
        {
            Smart = 0,
            Random = 1,
            ending = 2,
        }

        public Upgrade ( PlayerDataManager pm , GameDataManager gm , NextShiftProcessor nextShift ) : base ( pm , gm , nextShift )
        {

        }

        public string Run ( SimulationType type )
        {
            //檢查所有商品改良前置是否完成 ( 剃除改良完畢的商品 )
            List<int> ID = new List<int> ();
            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )
            {
                if ( pm.commodityItem.GetImproveComplete ( i ) )
                    continue;
                if ( !pm.commodityItem.GetImproveUnlock ( i , gm ) )
                    continue;
                if ( gm.commodityList.Data [i].type == GameData.Commodity.CommodityType.special )
                    continue;
                ID.Add ( i );                                               //取得可改良的商品陣列
            }
            if ( ID.Count <= 0 )
            {
                //( "沒有可以改良的商品" );
                return "";
            }
            switch ( type )
            {
                case SimulationType.Smart:
                    return SmartStrategy ( ID );
                default:
                    return RandomStrategy ( ID );
            }
        }

        private string SmartStrategy ( List<int> ID )
        {
            List<int []> upgrades = new List<int []> ();
            //再剩下的商品中選擇CP值最高的商品做改良
            for ( int i = 0; i < ID.Count; i++ )                                //搜尋所有可改良商品
            {
                int [] data = new int [2]; 
                data [0] = ID [i];                                              //取得商品編號
                int cost = gm.commodityList.Data [i].cost;                      //取得商品成本
                int sale = pm.commodityItem.GetData ( i ).sale;                 //取得商品售價
                int popular = 0;
                for ( int p = 0; p < pm.commodityItem.GetData ( i ).popular.Length; p++ )
                {
                    popular += (int)pm.commodityItem.GetData ( i ).popular [p]; //取得熱賣程度加總
                }
                int score = ( sale - cost ) * popular;                          //取得商品分數
                data [1] = score;                                               //取得商品分數
                upgrades.Add ( data );
            }
            upgrades.Sort ( delegate ( int [] c1 , int [] c2 ) { return c1 [1].CompareTo ( c2 [1] ) * -1; } );//針對商品評分由大到小排序
            Action.Upgrade.Run ( upgrades [0] [0] , pm , gm , nextShift );      //改良CP值最高的商品
            //return "改良\t"+gm.commodityList.Data[upgrades [0] [0]].name; 
            return "改良\t"+ Localizer.Translate ( gm.commodityList.Data[upgrades [0] [0]].name ); 
        }

        private string RandomStrategy ( List<int> ID )
        {
            ID.Shuffle ();
            Action.Upgrade.Run ( ID [0] , pm , gm , nextShift );                //隨機改良商品
            //return "改良\t"+gm.commodityList.Data[ID [0]].name; 
            return "改良\t"+  Localizer.Translate ( gm.commodityList.Data[ID [0]].name ); 
        }

        private string EndingStrategy ()
        {
            int id = GameSimulation.instance.GetCommodityID ();
            if ( pm.commodityItem.GetImproveUnlock ( id , gm ) == false )
                return "";
            Action.Upgrade.Run ( id , pm , gm , nextShift );
            //return "改良\t"+gm.commodityList.Data[id].name; 
            return "改良\t"+ Localizer.Translate ( gm.commodityList.Data[id].name ); 
        }
    }
}
