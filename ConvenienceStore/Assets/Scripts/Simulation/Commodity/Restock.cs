﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tools;

namespace Simulation
{
    public class Restock : BaseSimulation
    {
        public enum SimulationType
        {
            Smart = 0,
            Random = 1,
            ending = 2,
        }

        public Restock ( PlayerDataManager pm , GameDataManager gm ) : base ( pm , gm , null )
        {
        }

        public bool Run ( SimulationType type )
        {
            int storageRemain = pm.storage.GetStorageRemain ();                 //取得目前剩餘庫存量
            if ( storageRemain <= 0 )                                           //如果剩餘的倉庫位置小等於0 
            {
                return false;
            }
            if ( storageRemain - pm.purchase.GetAllWaittingNum () <= 0 )        //如果目前剩餘庫存量小於等於0
            {
                return false;
            }

            switch ( type )
            {
                case SimulationType.Smart:
                    return SmartStrategy ( storageRemain );
                default:
                    return RandomStrategy ( storageRemain );
            }
        }

        private bool SmartStrategy ( int storageRemain )
        {
            List<int []> unlockID = new List<int []> ();                        //已解鎖的商品陣列
            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )            //搜尋所有商品
            {
                if ( pm.commodityItem.GetData ( i ).unlock && (int)gm.commodityList.Data[i].type != 3 )                    //如果該編號商品已解鎖
                {
                    int [] item = new int [2];
                    item [0] = i;                                               //取得商品編號
                    int cost = gm.commodityList.Data [i].cost;                  //取得商品成本
                    int sale = pm.commodityItem.GetData ( i ).sale;             //取得商品售價
                    int popular = 0;
                    for ( int p = 0; p < pm.commodityItem.GetData ( i ).popular.Length; p++ )
                    {
                        popular += (int)pm.commodityItem.GetData ( i ).popular [p];//取得熱賣程度加總
                    }
                    int score = ( pm.commodityItem.GetData ( i ).sale - cost ) * popular;//取得商品分數
                    item [1] = score;                                           //取得商品分數
                    unlockID.Add ( item );                                      //加入陣列中
                }
            }

            int canUseMoney = pm.playerData.GetMoney () / 2;                    //取得可以使用的金錢

            unlockID.Sort ( delegate ( int [] c1 , int [] c2 ) { return c1 [1].CompareTo ( c2 [1] ) * -1; } );//根據商品評分由大排到小

            EndingStrategy ( storageRemain );

            foreach ( int [] commodity in unlockID )                            //搜尋所有已解鎖的商品
            {
                int id = commodity [0];                                         //取得商品編號
                int cost = gm.commodityList.Data [id].cost;                     //取得商品的成本
                int num = pm.playerData.GetMoney () / 2 / cost;                 //取得最大購買數
                int quantity = Mathf.Min ( Mathf.FloorToInt ( num ) , pm.purchase.GetMaxPurchaseNumByOne () - pm.purchase.GetDefinePurchase ( id ) );
                int space = storageRemain - pm.purchase.GetAllWaittingNum () - pm.purchase.GetAllDefiningNum ();
                int canBuy = Mathf.Min ( quantity , Mathf.Min ( pm.purchase.GetMaxPurchaseNumByOne () , space ) );//比較可購買的商品數量(可以使用的錢除以商品成本)與進貨上限(進貨上限跟剩餘庫存量先比較)哪個小 就記錄哪個
                if ( canBuy > 0 )
                {
                    Action.Restock.Run ( id , canBuy , gm , pm );               //盡可能進貨CP值最好的商品
                }
            }
            return true;
        }

        private bool RandomStrategy ( int storageRemain )
        {
            List<int> unlockID = new List<int> ();                              //已解鎖的商品陣列
            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )            //搜尋所有商品
            {
                if ( pm.commodityItem.GetData ( i ).unlock )                    //如果該編號商品已解鎖
                    unlockID.Add ( i );
            }
            int canUseMoney = pm.playerData.GetMoney () / 2;                    //取得可以使用的金錢

            unlockID.Shuffle ();

            EndingStrategy ( storageRemain );

            foreach ( int id in unlockID )                                      //搜尋所有已解鎖的商品
            {
                int cost = gm.commodityList.Data [id].cost;                     //取得商品的成本
                int num = pm.playerData.GetMoney () / 2  / cost;                 //取得最大購買數
                int space = storageRemain - pm.purchase.GetAllWaittingNum ();
                int RanNum = Random.Range ( 0 , pm.purchase.GetMaxPurchaseNumByOne () );
                int canBuy = Mathf.Min ( Mathf.FloorToInt ( num ) , Mathf.Min ( RanNum , space ) );//比較可購買的商品數量(可以使用的錢除以商品成本)與進貨上限(進貨上限跟剩餘庫存量先比較)哪個小 就記錄哪個
                if ( canBuy > 0 )
                {
                    Action.Restock.Run ( id , canBuy , gm , pm );               //盡可能進貨CP值最好的商品
                }
            }
            return true;
        }

        private void EndingStrategy ( int storageRemain )
        {
            int commodityID = GameSimulation.instance.GetCommodityID ();
            if ( commodityID != -1 )
            {
                int cost = gm.commodityList.Data [commodityID].cost;
                int num = pm.playerData.GetMoney () / 2 / cost; 
                int quantity = Mathf.Min ( Mathf.FloorToInt ( num ) , pm.purchase.GetMaxPurchaseNumByOne () - pm.purchase.GetDefinePurchase ( commodityID ) );
                int space = storageRemain - pm.purchase.GetAllWaittingNum () - pm.purchase.GetAllDefiningNum ();
                int canBuy = Mathf.Min ( quantity , Mathf.Min ( pm.purchase.GetMaxPurchaseNumByOne () , space ) );
                if ( canBuy > 0 )
                {
                    Action.Restock.Run ( commodityID , canBuy , gm , pm );               //盡可能進貨CP值最好的商品
                }
            }
        }
    }
}
