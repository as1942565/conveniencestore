﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tools;

namespace Simulation
{
    public class SetupCommodity : BaseSimulation
    {
        public enum SimulationType
        {
            Smart = 0,
            Random = 1,
        }

        public SetupCommodity ( PlayerDataManager pm , GameDataManager gm ) : base ( pm , gm , null )
        {

        }

        public bool Run ( SimulationType type )
        {
            //先全部下架
            pm.cabinet.ClearID ();

            switch ( type )
            {
                case SimulationType.Smart:
                    return SmartStrategy ();
                default:
                    return RandomStrategy ();
            }
        }

        private bool SmartStrategy ()
        {
            //找出所有已解鎖商品
            List<int []> unlockIDOver10 = new List<int []> ();                  //已解鎖而且超過10個的商品陣列
            List<int []> unlockIDLess10 = new List<int []> ();                  //已解鎖而且少於10個的商品陣列

            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )            //搜尋所有商品
            {
                if ( pm.storage.GetQuantity ( i ) > 0 )                         //如果該編號商品數量大於0
                {
                    int [] item = new int [4];
                    item [0] = i;                                               //取得商品編號
                    item [1] = (int)pm.commodityItem.GetData ( i ).popular [0]; //取得早班熱賣程度
                    item [2] = (int)pm.commodityItem.GetData ( i ).popular [1]; //取得晚班熱賣程度
                    item [3] = (int)pm.commodityItem.GetData ( i ).popular [2]; //取得大夜熱賣程度
                    if ( pm.storage.GetQuantity ( i ) >= 10 )                   //如果商品超過10個
                        unlockIDOver10.Add ( item );                            //加入陣列中
                    else if ( pm.storage.GetQuantity ( i ) > 0 )                //如果沒超過10個而且不等於0
                        unlockIDLess10.Add ( item );                            //加入陣列中
                }
            }
            //查看該時段誰的受歡迎度最高
            int s = (int)pm.calender.GetShift () + 1;
            unlockIDOver10.Sort ( delegate ( int [] c1 , int [] c2 ) { return c1 [s].CompareTo ( c2 [s] ) * -1; } );
            unlockIDLess10.Sort ( delegate ( int [] c1 , int [] c2 ) { return c1 [s].CompareTo ( c2 [s] ) * -1; } );
            //找出每種商品櫃可以擺放幾種商品
            int [] cabinetType = new int [gm.cabinetList.Data.Length];          //商品櫃剩餘的空位陣列
            for ( int i = 0; i < cabinetType.Length; i++ )                      //搜尋所有商品櫃種類
            {
                cabinetType [i] = pm.cabinet.GetTypeUnlockNum ( i );            //取得商品櫃剩餘的空位
            }

            bool ending = EndingStrategy ();

            //商品剩餘10個以上優先上架
            //最好的放在主打商品
            if ( ending == false && unlockIDOver10.Count > 0 )
            {
                Action.SetupCommodity.Run ( 4 , 0 , unlockIDOver10 [0] [0] , pm );   //放置主打商品櫃
                cabinetType [0]--;                                              //主打商品櫃空位減1
            }
            //這邊 comodity　0是食品 1是飲品 2是雜貨 3是限定, 商品櫃子是 1限定 2食品 3飲品 4雜貨
            //用這個矩陣來看什麼種類的東西在什麼位置
            for ( int i = 1; i < unlockIDOver10.Count; i++ )                    //搜尋所有已解鎖而且超過10個的商品
            {
                for ( int type = 0; type < gm.customData.Data.CommodityType; type++ )        //搜尋所有商品的種類
                {
                    if ( (int)gm.commodityList.Data [unlockIDOver10[i][0]].type == type && cabinetType [type] > 0 )//如果該編號商品是否符合該商品櫃種類 而且 該商品櫃種類還有空位
                    {
                        Action.SetupCommodity.Run ( type , cabinetType [type] - 1 , unlockIDOver10 [i] [0] , pm );//放置商品到該種類欄位 , 欄位需要-1 
                        cabinetType [type]--;                    //該商品櫃空位減1
                    }
                }
            }
            //如果有少於10個的物件並且主打櫃子是空
            if ( unlockIDLess10.Count > 0 && pm.cabinet.GetCommodityID ( 4 , 0 ) == -1 )
            {
                Action.SetupCommodity.Run ( 4 , 0 , unlockIDLess10 [0] [0] , pm );   //放置主打商品櫃
                cabinetType [0]--;                                              //主打商品櫃空位減1
            }
            for ( int i = 0; i < unlockIDLess10.Count; i++ )                    //搜尋所有已解鎖但沒超過10個的商品
            {
                for ( int type = 0; type < gm.customData.Data.CommodityType; type++ )        //搜尋所有商品櫃種類
                {
                    if ( (int)gm.commodityList.Data [unlockIDLess10[i][0]].type == type && cabinetType [type] > 0 )//如果該編號商品事符合該商品櫃種類 而且 該商品櫃種類還有空位
                    {
                        Action.SetupCommodity.Run ( type , cabinetType [type] - 1 , unlockIDLess10 [i] [0] , pm );//放置商品到該種類欄位, 欄位需要-1 因為矩陣是0開始
                        cabinetType [type]--;                    //該商品櫃空位減1
                    }
                }
            }

            return true;
        }

        private bool RandomStrategy ()
        {
            //找出每種商品櫃可以擺放幾種商品
            int [] cabinetType = new int [gm.cabinetList.Data.Length];          //商品櫃剩餘的空位陣列
            for ( int i = 0; i < cabinetType.Length; i++ )                      //搜尋所有商品櫃種類
            {
                cabinetType [i] = pm.cabinet.GetTypeUnlockNum ( i );            //取得商品櫃剩餘的空位
            }
            List<int> unlockID = new List<int> ();
            for ( int i = 0; i < gm.commodityList.Data.Length; i++ )            //搜尋所有商品
            {
                if ( pm.storage.GetQuantity ( i ) > 0 )                         //如果該編號商品數量超過0
                {
                    unlockID.Add ( i );
                }
            }
            if ( unlockID.Count <= 0 )
                return false;
            unlockID.Shuffle ();

            bool ending = EndingStrategy ();
            if ( !ending )
                Action.SetupCommodity.Run ( 4 , 0 , unlockID [0] , pm );            //放置主打商品櫃
            for ( int i = 1; i < unlockID.Count; i++ )                          //搜尋所有已解鎖的商品
            {
                int id = unlockID [i];
                for ( int type = 0; type < gm.customData.Data.CommodityType; type++ )        //搜尋所有商品櫃種類
                {
                    if ( (int)gm.commodityList.Data [i].type == type && cabinetType [type] > 0 )//如果該編號商品事符合該商品櫃種類 而且 該商品櫃種類還有空位
                    {
                        Action.SetupCommodity.Run ( type , cabinetType [type] - 1 , id , pm );//放置商品到該種類欄位, 欄位需要-1 因為矩陣是0開始
                        cabinetType [type]--;                    //該商品櫃空位減1
                    }
                }
            }
            return true;
        }

        private bool EndingStrategy ()
        {
            int id = GameSimulation.instance.GetCommodityID ();
            if ( id == -1 )
                return false;
            int quaitity = pm.storage.GetQuantity ( id );
            if ( quaitity == 0 )
                return false;
            Action.SetupCommodity.Run ( 4 , 0 , id , pm );
            return true;
        }
    }
}
