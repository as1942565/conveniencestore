﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Simulation
{
    public class Activity : BaseSimulation
    {
        public Activity ( PlayerDataManager pm , GameDataManager gm ) : base ( pm , gm , null )
        {

        }

        private const int MinExecuteChance = 90;

        public bool Run ()
        {                                                                       //
            //找出待命的員工
            List<Employee> employees = new List<Employee> ();
            int [] unlockID = pm.employeeList.GetAllUnlockID ();                //取得已解鎖的員工編號
            for ( int i = 0; i < unlockID.Length; i++ )
            {
                if ( pm.activityScheduale.IsEmployeeFree ( unlockID [i] ) )     //如果該編號員工有空
                {
                    employees.Add ( pm.employeeList.GetEmployee ( unlockID [i] ) );                                     //加入陣列中
                }
            }
            if ( employees.Count <= 0 )
            {
                //Debug.LogError ( "沒有員工可以使用" );
                return false;
            }
            //todofirst
            if ( pm.playerData.GetClean () > pm.playerData.GetPolice () )
            {
                PoliceActivity ( employees );
                CleanActivity ( employees );
            }
            else
            {
                CleanActivity ( employees );
                PoliceActivity ( employees );
            }

            //找出所有可執行的員工活動
            List<int []> activitys = new List<int []> ();
            List<int> id = pm.activityUnlock.GetAllCanRunID ();
            if ( id.Count <= 0 )
            {
                return false;
            }

            //最困難並且可90%的優先執行
            for ( int i = 0; i < id.Count; i++ )
            {
                activitys.Add ( new int [3] );
                activitys [i] [0] = id [i];
                activitys [i] [1] = (int)gm.activityList.Data [i].difficult;         //取得活動困難度
                activitys [i] [2] = (int)gm.activityList.Data [i].type;              //取得活動種類
            }
            activitys.Sort ( delegate ( int [] c1 , int [] c2 ) { return c1 [1].CompareTo ( c2 [1] ) * -1; } );//活動困難度由簡單到困難
            //針對活動種類分配員工
            for ( int i = 0; i < activitys.Count; i++ )                         //搜尋所有可執行的員工活動
            {
                int type = activitys [i] [2];                                   //取得活動動類
                if (type != 3)
                {
                    employees.Sort(delegate (Employee c1, Employee c2) { return c1.GetAbility(type).CompareTo(c2.GetAbility(type)) * -1; });//根據活動種類排員工屬性 由大到小
                }
                else
                {
                    //特殊事件的排序需要修改
                    employees.Sort(delegate (Employee c1, Employee c2) { return (c1.GetAbility(0)+ c1.GetAbility(1)).CompareTo((c2.GetAbility(0)+ c2.GetAbility(1))) * -1; });//根據活動種類排員工屬性 由大到小
                }
                int [] addition = gm.customData.Data.ActivityChance [activitys [i] [1]].ToArray ();//取得該困難度該有的成功率
                SetupEmployee ( employees , activitys [i] [0] );                //安排員工執行員工活動
            }
            return true;
        }

        void SetupEmployee ( List<Employee> employees , int activityID )        //安排員工執行員工活動
        {
            int chance = 0;
            List<int> employeeID = new List<int> ();                            //記錄受委託的員工編號
            for ( int j = 0; j < employees.Count; j++ )                         //搜尋所有員工
            {
                if ( !pm.activityScheduale.IsEmployeeFree ( employees [j].GetID () ) )//如果該編號員工沒空
                    break;
                if ( chance < MinExecuteChance && employeeID.Count < 2 )        //如果成功率低於90 而且受委託的員工不到2人
                {
                    employeeID.Add ( employees [j].GetID () );                  //派遣這位員工
                    GameData.Activity.ActivityDifficult difficult = gm.activityList.Data [activityID].difficult;//取得該活動的困難度
                    GameData.Activity.ActivityType type = gm.activityList.Data [activityID].type;
                    chance = Action.Activity.Chance ( employeeID.ToArray () , type , difficult , gm , pm );
                }
                if ( chance >= MinExecuteChance || employeeID.Count ==2 )                               //如果派遣後成功率大於90
                {
                    bool success = Action.Activity.Success ( chance );
                    Action.Activity.Run ( activityID , employeeID.ToArray () , success , ActivityScheduale.Scheduale.SchedualeMethod.normal , pm , gm );//發佈委託
                    break;
                }
            }
        }

        void CleanActivity ( List<Employee> employees )                         //執行清潔度活動
        {
            employees.Sort(delegate (Employee c1, Employee c2) { return c1.GetAbility(0).CompareTo(c2.GetAbility(0)) * -1; });//根據活動種類排員工屬性 由大到小

            for ( int i = 15; i > 12; i-- )
            {
                if ( pm.activityUnlock.GetUnlock ( i ) && pm.activityUnlock.GetMode ( i ) == 0 )
                {
                    SetupEmployee ( employees , i );
                    return;
                }
            }
        }
        void PoliceActivity ( List<Employee> employees )                        //執行治安度活動
        {
            employees.Sort(delegate (Employee c1, Employee c2) { return c1.GetAbility(0).CompareTo(c2.GetAbility(0)) * -1; });//根據活動種類排員工屬性 由大到小

            for ( int i = 18; i > 15; i-- )
            {
                if ( pm.activityUnlock.GetUnlock ( i ) && pm.activityUnlock.GetMode ( i ) == 0 )
                {
                    SetupEmployee ( employees , i );
                    return;
                }
            }
        }
    }
}
