﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tools;

namespace Simulation
{
    public class StoreExpansion : BaseSimulation
    {
        public enum SimulationType
        {
            Smart = 0,
            Random = 1,
        }

        public StoreExpansion ( PlayerDataManager pm , GameDataManager gm , NextShiftProcessor nextShift ) : base ( pm , gm , nextShift )
        {

        }

        public string Run ( SimulationType type )
        {
            int CanUseMoney = pm.playerData.GetMoney () / 2;                    //紀錄可使用的金錢
            
            //檢查有哪些已解鎖但未完成 而且價格低於玩家金錢一半
            List<int> storeID = new List<int>();
            for ( int i = 0; i < gm.storeExpansionList.Data.Length; i++ )
            {
                bool unlock = pm.storeExpansionUnlock.GetUnock ( i );           //紀錄已解鎖的擴建編號
                int level = gm.storeExpansionList.Data [i].storeLevel;
                int storeLevel = pm.businessUnlock.GetLevel ();
                bool complete = pm.storeExpansionUnlock.GetComplete ( i );      //紀錄已完成的擴建編號
                int cost = gm.storeExpansionList.Data [i].cost;                //紀錄擴建的費用
                if ( unlock && !complete && CanUseMoney > cost && storeLevel >= level )                //如果該擴建已解鎖但未完成
                {
                    storeID.Add ( i );
                }
            }
            if ( storeID.Count <= 0 )
            {
                return "";
            }
            switch ( type )
            {
                case SimulationType.Smart:
                    return SmartStrategy ( storeID );
                default:
                    return RandomStrategy ( storeID );
            }
        }

        private string SmartStrategy ( List<int> storeID )
        {
            //檢查哪個價格最低
            int minCost = int.MaxValue;                                         //暫存的擴建費用
            int ID = -1;                                                        //暫存的擴建編號
            for ( int i = 0; i < storeID.Count; i++ )
            {
                int id = storeID [i];
                int cost = gm.storeExpansionList.Data [id].cost;               //紀錄擴建的費用
                if ( cost < minCost )                                           //如果該擴建已解鎖但未完成 而且費用小於前者
                {
                    minCost = cost;                                             //更新費用
                    ID = id;                                                    //更新編號
                }
            }
            if ( ID < 0 )
            {
                return "";
            }

            List<int> IDs = new List<int> ();
            IDs.Add ( ID );
            Action.StoreExpansion.Run ( IDs.ToArray () , pm , gm , nextShift );             //擴建最低價格的設施
            return "擴建\t" + gm.storeExpansionList.Data [ID].name;
        }

        private string RandomStrategy ( List<int> storeID )
        {
            storeID.Shuffle ();
            int ID = storeID [0];
            List<int> IDs = new List<int> ();
            IDs.Add ( ID );
            Action.StoreExpansion.Run ( IDs.ToArray () , pm , gm , nextShift );             //擴建最低價格的設施
            return "擴建\t" + gm.storeExpansionList.Data [ID].name;
        }
    }
}
