﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Simulation
{
    public class BaseSimulation
    {
        protected PlayerDataManager pm;
        protected GameDataManager gm;
        protected NextShiftProcessor nextShift;

        public BaseSimulation ( PlayerDataManager pm , GameDataManager gm , NextShiftProcessor nextShift )
        {
            this.pm = pm;
            this.gm = gm;
            this.nextShift = nextShift;
        }

    }
}
