﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Simulation
{
    public class SetupEmployee : BaseSimulation
    {
        public SetupEmployee ( PlayerDataManager pm , GameDataManager gm ) : base ( pm , gm , null )
        {

        }

        private int [,] defaultShift = new int [,] { { 0 , 1 , 2 } , { 1 , 2 , 3 } , { 2 , 3 , 0 } , { 3 , 0 , 1 } };

        public bool Run ()
        {
            int day = pm.calender.GetDay ();
            //移除所有員工
            pm.employeeScheduleLog.ResetAllEmployeeID ( day );
            //找出所有已解所員工
            List<int> employeeID = new List<int>();
            int [] ID = pm.employeeList.GetAllUnlockID ();                 //取得已解鎖的員工編號
            int scheduleDay = day % 4;
            for ( int i = 0; i < 3; i++ )
            {
                employeeID.Add ( defaultShift[scheduleDay , i] ); 
            }
            for ( int i = 4; i < ID.Length; i++ )
            {
                int id = ID [i];
                if ( pm.employeeList.GetEmployee ( id ).GetPower () > 0 )   //如果員工的體力大於0
                {
                    employeeID.Add ( id );                                  //加入陣列中
                }
            }
            if ( employeeID.Count <= 0 )
            {
                return false;
            }
            Setup ( day , employeeID );
            
            return true;
        }
        public bool ProRun ()
        {
            int day = pm.calender.GetDay ();
            //移除所有員工
            pm.employeeScheduleLog.ResetAllEmployeeID ( day );

            List<int []> employees = new List<int []> ();                       //紀錄可排班的員工
            int [] unlockID = pm.employeeList.GetAllUnlockID ();                //取得已解鎖的員工編號
            for ( int i = 0; i < unlockID.Length; i++ )
            {
                int [] data = new int [2];
                data [0] = unlockID [i];
                int power = pm.employeeList.GetEmployee ( unlockID [i] ).GetPower ();
                if ( power != 0 )
                {
                    data [1] = power;
                    employees.Add ( data );
                }
            }
            employees.Sort ( delegate ( int [] c1 , int [] c2 ) { return c1 [1].CompareTo ( c2 [1] ); } );
            List<int> ID = new List<int> ();
            for ( int i = 0; i < employees.Count; i++ )
            {
                ID.Add ( employees[i][0] );
            }
            Setup ( day , ID );
            return true;
        }

        private void Setup ( int day , List<int> employeeID )
        {
            int shift = 0;                                                      //紀錄員工的班次
            int endingID = GameSimulation.instance.employeeID;
            if ( endingID != -1 )
            {
                if ( pm.employeeList.GetEmployee ( endingID ).GetPower () > 0 )
                {
                    Action.SetupEmployee.Run ( day , endingID , 0 , pm );         //安排員工到該欄位
                    shift++;
                }
            }

            //第一天0號休息 第二天1號休息 以此類推 其他依序排班(早-晚-夜-早-晚)
            EmployeeScheduing schedule = pm.employeeScheduing;
            for ( int i = 0; i < employeeID.Count; i++ )                        //搜尋所有員工
            {
                shift = shift % 3;
                if ( schedule.GetUnlock ( shift ) > pm.employeeScheduleLog.GetScheduleQuantity ( day , shift , pm.employeeList ) )//如果該時段還沒滿人
                {
                    int id = employeeID [i];
                    Action.SetupEmployee.Run ( day , id , shift , pm );         //安排員工到該欄位
                    shift++;                                                    //換下一個班次
                }
            }
        }
    }
}
