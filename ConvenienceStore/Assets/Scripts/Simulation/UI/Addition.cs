﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Observer
{
    public class Addition : BaseObserver
    {
        private Text AdditionName;
        private Text AdditionText;
        private Text AdditionTime;

        private string [] typeName = new string [] { "食品" , "飲品" , "雜貨" };
        private string [] additionStr = new string [4];
        private string [] textStr = new string [4];
        private string [] timeStr = new string [4];

        private void Awake ()
        {
            AdditionName = transform.Find ( "AdditionName" ).GetComponent<Text> ();
            AdditionText = transform.Find ( "AdditionText" ).GetComponent<Text> ();
            AdditionTime = transform.Find ( "AdditionTime" ).GetComponent<Text> ();
        }

        protected override void RefreshComponent ( PlayerDataManager pm )
        {
            ShowText ();
        }

        protected override void RegisterObserver ( PlayerDataManager pm )
        {
            pm.activityAddition.OnActivityIncomeChanged += AddActivityIncomeAddition;
            pm.activityAddition.OnActivitySaleQuantityChanged += AddActivitySaleQuantityAddition;
            pm.businessAddition.BusinessIncomeChanged += AddBusinessIncomeAddition;
            pm.businessAddition.BusinessSaleQuantityChanged += AddBusinessSaleQuantityAddition;
        }

        protected override void UnregisterObserver ( PlayerDataManager pm )
        {
            pm.activityAddition.OnActivityIncomeChanged -= AddActivityIncomeAddition;
            pm.activityAddition.OnActivitySaleQuantityChanged -= AddActivitySaleQuantityAddition;
            pm.businessAddition.BusinessIncomeChanged -= AddBusinessIncomeAddition;
            pm.businessAddition.BusinessSaleQuantityChanged -= AddBusinessSaleQuantityAddition;
        }

        private void AddActivityIncomeAddition ( List<ActivityIncomeAddition> addition )
        {
            int totalShift = manager.pm.calender.TotalShift;
            additionStr [0] = "";
            textStr [0] = "";
            timeStr [0] = "";
            for ( int i = 0; i < addition.Count; i++ )
            {
                int t = (int)addition [i].type;
                additionStr [0] += $"(活動){typeName [t]}收入增加 \n";
                textStr [0] += Mathf.RoundToInt ( addition [i].addition * 100 ) + "%\n";
                timeStr [0] += "剩餘" + ( addition [i].time - totalShift ).ToString () + "\n";
            }
            ShowText ();
        }
        private void AddActivitySaleQuantityAddition ( List<ActivitySaleQuantityAddition> addition )
        {
            int totalShift = manager.pm.calender.TotalShift;
            additionStr [1] = "";
            textStr [1] = "";
            timeStr [1] = "";
            for ( int i = 0; i < addition.Count; i++ )
            {
                int t = (int)addition [i].type;
                additionStr [1] += $"(活動){typeName [t]}銷量增加 \n";
                textStr [1] += addition [i].addition + "\n";
                timeStr [1] += "剩餘" + ( addition [i].time - totalShift ).ToString () + "\n";
            }
            ShowText ();
        }
        private void AddBusinessIncomeAddition ( List<BusinessIncomeAddition> addition )
        {
            int totalShift = manager.pm.calender.TotalShift;
            additionStr [2] = "";
            textStr [2] = "";
            timeStr [2] = "";
            for ( int i = 0; i < addition.Count; i++ )
            {
                if ( addition [i].RunShift > totalShift )
                    continue;
                int t = (int)addition [i].type;
                additionStr [2] += $"(業務){typeName [t]}收入增加 \n";
                textStr [2] += ( addition [i].addition * 100 ) + "%\n";
                timeStr [2] += "剩餘" + ( addition [i].time - totalShift ).ToString () + "\n";
            }
            ShowText ();
        }
        private void AddBusinessSaleQuantityAddition ( List<BusinessSaleQuantityAddition> addition )
        {
            int totalShift = manager.pm.calender.TotalShift;
            additionStr [3] = "";
            textStr [3] = "";
            timeStr [3] = "";
            for ( int i = 0; i < addition.Count; i++ )
            {
                if ( addition [i].RunShift > totalShift )
                    continue;
                int t = (int)addition [i].type;
                additionStr [3] += $"(業務){typeName [t]}銷量增加 \n";
                textStr [3] += addition [i].addition + "\n";
                timeStr [3] += "剩餘" + ( addition [i].time - totalShift ).ToString () + "\n";
            }
            ShowText ();
        }

        private void Init ()
        {
            AdditionName.text = "";
            AdditionText.text = "";
            AdditionTime.text = "";
        }

        private void ShowText ()
        {
            Init ();
            for ( int i = 0; i < 4; i++ )
            {
                AdditionName.text += additionStr [i];
                AdditionText.text += textStr [i];
                AdditionTime.text += timeStr [i];
            }
            if ( AdditionName.text == "" )
                AdditionName.text = Localizer.Translate ( "無" );
        }
    }
}
