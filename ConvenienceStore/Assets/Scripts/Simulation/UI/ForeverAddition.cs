﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Observer
{
    public class ForeverAddition : BaseObserver
    {
        private Text incomeText;
        private Text saleQuantityText;
        private Text popularText;
        private Text autoCleanText;
        private Text autoPoliceText;

        private void Awake ()
        {
            incomeText = transform.Find ( "incomeText" ).GetComponent<Text> ();
            saleQuantityText = transform.Find ( "saleQuantityText" ).GetComponent<Text> ();
            popularText = transform.Find ( "popularText" ).GetComponent<Text> ();
            autoCleanText = transform.Find ( "autoCleanText" ).GetComponent<Text> ();
            autoPoliceText = transform.Find ( "autoPoliceText" ).GetComponent<Text> ();
        }

        protected override void RefreshComponent ( PlayerDataManager pm )
        {
            float income = pm.storeExpansionAddition.GetIncome ();
            float saleQuantity = pm.storeExpansionAddition.GetSaleQuantity ();
            float popular = pm.storeExpansionAddition.GetPopular ();
            int autoClean = pm.storeExpansionAddition.GetAutoAddClean ();
            int autoPolice = pm.storeExpansionAddition.GetAutoAddPolice ();

            OnIncomeUpdated ( income );
            OnSaleQuantityUpdated ( saleQuantity );
            OnPopularUpdated ( popular );
            OnAutoCleanUpdated ( autoClean );
            OnAutoPoliceUpdated ( autoPolice );
        }

        protected override void RegisterObserver ( PlayerDataManager pm )
        {
            pm.storeExpansionAddition.OnIncomeChanged += OnIncomeUpdated;
            pm.storeExpansionAddition.OnSaleQuantityChanged += OnSaleQuantityUpdated;
            pm.storeExpansionAddition.OnPopularChanged += OnPopularUpdated;
            pm.storeExpansionAddition.OnAutoCleanChanged += OnAutoCleanUpdated;
            pm.storeExpansionAddition.OnAutoPoliceChanged += OnAutoPoliceUpdated;
        }

        protected override void UnregisterObserver ( PlayerDataManager pm )
        {
            pm.storeExpansionAddition.OnIncomeChanged -= OnIncomeUpdated;
            pm.storeExpansionAddition.OnSaleQuantityChanged -= OnSaleQuantityUpdated;
            pm.storeExpansionAddition.OnPopularChanged -= OnPopularUpdated;
            pm.storeExpansionAddition.OnAutoCleanChanged -= OnAutoCleanUpdated;
            pm.storeExpansionAddition.OnAutoPoliceChanged -= OnAutoPoliceUpdated;
        }

        private void OnIncomeUpdated ( float addition )
        {
            int income = Mathf.RoundToInt ( addition * 100 );
            incomeText.text = income.ToString () + "%";
        }
        private void OnSaleQuantityUpdated ( float addition )
        {
            int saleQuantity = Mathf.RoundToInt ( addition * 100 );
            saleQuantityText.text = saleQuantity.ToString () + "%";
        }
        private void OnPopularUpdated ( float addition )
        {
            int popular = Mathf.RoundToInt ( addition * 100 );
            popularText.text = popular.ToString () + "%";
        }
        private void OnAutoCleanUpdated ( int autoClean )
        {
            autoCleanText.text = autoClean.ToString ();
        }
        private void OnAutoPoliceUpdated ( int autoPolice )
        {
            autoPoliceText.text = autoPolice.ToString ();
        }
    }
}
