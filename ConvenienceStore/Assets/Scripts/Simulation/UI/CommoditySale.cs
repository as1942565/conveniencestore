﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Observer
{
    public class CommoditySale : BaseObserver
    {
        private Text nextShiftText;

        private void Awake ()
        {
            nextShiftText = GetComponent<Text> ();
        }

        protected override void RefreshComponent ( PlayerDataManager pm )
        {
            nextShiftText.text = "";
        }

        protected override void RegisterObserver ( PlayerDataManager pm )
        {
            NextShift.Commodity.OnNextShiftChanged += ShowMessage;
        }

        protected override void UnregisterObserver ( PlayerDataManager pm )
        {
            NextShift.Commodity.OnNextShiftChanged -= ShowMessage;
        }

        private void ShowMessage ( string message )
        {
            if ( message == "" )
                nextShiftText.text = "";
            else
                nextShiftText.text = message;
        }
    }
}
