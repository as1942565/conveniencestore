﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Observer
{
    public class Employee : BaseObserver
    {
        private Text employeeText;

        private void Awake ()
        {
            employeeText = GetComponent<Text> ();
        }

        protected override void RefreshComponent ( PlayerDataManager pm )
        {
            ShowText (pm.calender);
        }

        protected override void RegisterObserver ( PlayerDataManager pm )
        {
            pm.calender.AfterNextShift += ShowText;
        }

        protected override void UnregisterObserver ( PlayerDataManager pm )
        {
            pm.calender.AfterNextShift -= ShowText;
        }

        private void ShowText (Calender c)
        {
            employeeText.text = "";
            PlayerDataManager pm = GameLoopManager.instance.pm;
            GameDataManager gm = GameLoopManager.instance.gm;
            int [] unlockID = pm.employeeList.GetAllUnlockID ();
            for ( int i = 0; i < unlockID.Length; i++ )
            {
                int id = unlockID [i];
                string name = gm.workerList.Data [id].name;
                string level = pm.employeeList.GetEmployee ( id ).GetLevel ().ToString ();
                string relationRank = pm.employeeList.GetEmployee ( id ).GetRelationRank ().ToString ();
                int [] ability = new int [3];
                for ( int a = 0; a < ability.Length; a++ )
                {
                    ability [a] = pm.employeeList.GetEmployee ( id ).GetAbility ( a );
                }
                string [] abilityName = new string [] { "C" , "B" , "A" , "S" };
                string power = pm.employeeList.GetEmployee ( id ).GetPower ().ToString ();
                employeeText.text += $"{name} \t等級{level}級\t好感度{relationRank}級\t體力{abilityName[ability[0]]} 魅力{abilityName[ability[1]]} 運氣{abilityName[ability[2]]}\t體力{power}\n";
            }
        }
    }
}
