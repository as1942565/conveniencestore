﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Observer
{
    public class CommodityMessage : BaseObserver
    {
        private Text commodityText;

        private void Awake ()
        {
            commodityText = GetComponent<Text> ();
        }

        protected override void RefreshComponent ( PlayerDataManager pm )
        {
            ShowCommodityMessage ( pm.calender );
        }

        protected override void RegisterObserver ( PlayerDataManager pm )
        {
            pm.calender.AfterNextShift += ShowCommodityMessage;
        }

        protected override void UnregisterObserver ( PlayerDataManager pm )
        {
            pm.calender.AfterNextShift -= ShowCommodityMessage;
        }

        private void ShowCommodityMessage ( Calender c )
        {
            GameLoopManager manager = GameLoopManager.instance;
            List<int> commodityID = new List<int>();
            for ( int i = 0; i < manager.gm.commodityList.Data.Length; i++ )
            {
                if ( manager.pm.commodityItem.GetData ( i ).unlock )
                    commodityID.Add ( i );
            }
            string message = "";
            for ( int i = 0; i < commodityID.Count; i++ )
            {
                int id = commodityID [i];
                string star = manager.pm.commodityItem.GetData ( id ).star.ToString ();
                //string name = manager.gm.commodityList.Data [id].name;
                string name = Localizer.Translate ( manager.gm.commodityList.Data [id].name );
                string quantity = manager.pm.storage.GetQuantity ( id ).ToString ();
                int type = (int)manager.gm.commodityList.Data [id].type;
                string [] typeName = new string [] { "食" , "飲" , "雜" , "特" };
                if ( manager.pm.storage.GetQuantity ( id ) == 0 )
                    message += $"<color=#808080ff>{i + 1}. ({typeName [type]}){star}星 {name} : {quantity}個</color>\n";
                else
                    message += $"{i + 1}. ({typeName [type]}){star}星 {name} : {quantity}個\n";
            }
            if ( message.Length <= 0 )
                return;
            commodityText.text = message.Substring ( 0 , message.Length - 1 );
        }
    }
}
