﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Observer
{
    public class ShiftMessage : BaseObserver
    {
        private Text shiftText;

        private void Awake ()
        {
            shiftText = GetComponent<Text> ();
        }

        protected override void RefreshComponent ( PlayerDataManager pm )
        {
            ShowShiftMessage ( "" );
        }

        protected override void RegisterObserver ( PlayerDataManager pm )
        {
            //Todo : 如何增加監聽
            pm.gameLog.OnShiftMessage += ShowShiftMessage;
        }

        protected override void UnregisterObserver ( PlayerDataManager pm )
        {
            pm.gameLog.OnShiftMessage -= ShowShiftMessage;
        }

        public void ShowShiftMessage ( string log )
        {
            shiftText.text = log;
        }
    }
}
