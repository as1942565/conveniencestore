﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObserverButton : MonoBehaviour
{
    public Image [] Buttons;
    public GameObject [] Messages;
    public bool [] show;

    private void Start ()
    {
        for ( int i = 0; i < show.Length; i++ )
        {
            Messages [i].SetActive ( show [i] );
            Buttons [i].color = show [i] ? Color.white : Color.gray;
        }
    }

    public void Change ( int id )
    {
        show [id] = !show [id];
        Messages [id].SetActive ( show [id] );
        Buttons [id].color = show [id] ? Color.white : Color.gray;
    }
}
