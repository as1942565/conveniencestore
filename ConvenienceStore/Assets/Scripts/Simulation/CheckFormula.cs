﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckFormula : MonoBehaviour
{
    public GameLoopManager manager;
    private int saleQuantity;
    public string saleQuantityLog;
    private int CommodityIncome;
    public string CommodityIncomeLog;
    private int popular;
    public string popularLog;
    private int clean;
    public string cleanLog;
    private int police;
    public string policeLog;
    public string shiftLog;

    public void Init ()
    {
        shiftLog = "";
        saleQuantityLog = "";
        popularLog = "";
        policeLog = "";
        cleanLog = "";
        CommodityIncomeLog = "";
    }

    public void UpdateAll ( PlayerDataManager pm , GameDataManager gm )
    {
        shiftLog = $"取得時間 => 天數： {pm.calender.GetDay ()} , 班次： {pm.calender.GetShift ()}, 總班次： {pm.calender.TotalShift}";
        SaleQuantityCalculate ( pm , gm );
        CommodityIncomeCalculate ( pm , gm );
        PopularCalculate ( pm , gm );
        CleanCalculate ( pm , gm );
        PoliceCalculate ( pm , gm );
    }

    private void SaleQuantityCalculate ( PlayerDataManager pm , GameDataManager gm )
    {
        //[當前行動點人氣值 × 商品預計銷售量 × 時段加成 x 商品櫃加成 ]+推廣商品 (員工活動+店長業務) = 實際銷售量
        float popular = Formula.CommoditySaleQuantity.Popular ( pm );
        float baseSaleValue = Formula.CommoditySaleQuantity.BaseSaleValue ( gm , 0 );
        int shift = (int)pm.calender.GetShift ();
        string [] shiftName = new string [] { "早班" , "晚班" , "大夜" };
        float shiftAddition = Formula.CommoditySaleQuantity.ShiftAddition ( pm , gm , 0 );
        float cabinetAddition = Formula.CommoditySaleQuantity.CabinetAddition ( gm , 2 );
        float activityAddition = Formula.CommoditySaleQuantity.ActivityAddition ( pm , 0 );
        float businessAddition = Formula.CommoditySaleQuantity.BusinessAddition ( pm , 0 );
        float result = popular * baseSaleValue * shiftAddition * cabinetAddition + activityAddition + businessAddition;
        saleQuantity = Mathf.FloorToInt ( result );
        saleQuantityLog = $"( 人氣值 {popular} * 預計銷售量 {baseSaleValue} * {shiftName [shift]}加成 {shiftAddition} * 商品櫃加成 {cabinetAddition} ) + 員工推廣 {activityAddition} + 店長推廣 {businessAddition} = 實際銷售量 {saleQuantity}";
    }
    private void CommodityIncomeCalculate ( PlayerDataManager pm , GameDataManager gm )
    {
        //實際銷售量(saleQuantity) × 個別商品的價格 × 員工時段加成 × 設施加成 × 員工活動加成 × 店長業務加成 = 實際收入
        float sale = pm.commodityItem.GetData ( 0 ).sale;
        float employeeAddition = Formula.CommodityIncome.ScheduleAddition ( pm );
        float storeAddition = Formula.CommodityIncome.StoreAddition ( pm );
        float activityAddition = Formula.CommodityIncome.ActivityAddition ( 0 , pm );
        float businessAddition = Formula.CommodityIncome.BusinessAddition ( 0 , pm );
        float result = saleQuantity * sale * employeeAddition * storeAddition * activityAddition * businessAddition;
        CommodityIncome = Mathf.FloorToInt ( result );
        CommodityIncomeLog = $"實際銷售量 {saleQuantity} * 商品價格 {sale} * 員工時段加成 {employeeAddition} * 設施加成 {storeAddition} * 員工活動加成 {activityAddition} * 店長業務加成 {businessAddition} = 實際收入 { CommodityIncome}";
    }
    private void PopularCalculate ( PlayerDataManager pm , GameDataManager gm )
    {
        //( 當前行動點人氣值 + 每次行動點固定增加人氣值 + 銷售量轉人氣值數值 ) x 擴建設施人氣加成 + 員工活動增加的實際人氣值 + 店長業務增加的實際人氣值 = 增加人氣值
        float popular = Formula.Popular.popular ( pm );
        float addPopular = Formula.Popular.AddPopular ();
        float popularWithSaleQuantity = Formula.Popular.AddPopularWithSaleQuantity ( pm , gm );
        float storeAddition = Formula.Popular.StoreAddition ( pm );
        this.popular = Formula.Popular.Calculate ( pm , gm );
        int grow = this.popular - (int)popular;
        popularLog = $"( 人氣值 {popular} + 固定增加 {addPopular} + 銷售量轉人氣值 {popularWithSaleQuantity} ) * 擴建加成 {storeAddition} = 人氣值 {popular} + {grow}";
    }
    private void CleanCalculate ( PlayerDataManager pm , GameDataManager gm )
    {
        //當前行動點的清潔度-( 5 + 當前擴建設施數量*2 ) = 下回合清潔度 (數值最低為0，不會變成負數。)
        float clean = Formula.Clean.clean ( pm );
        float lessClean = Formula.Clean.LessClean ();
        float storeQuantity = Formula.Clean.StoreExpandNum ( pm );
        this.clean = Formula.Clean.Calculate ( pm );
        cleanLog = $"清潔度 {clean} - ( {lessClean} + 設施數量 {storeQuantity} * 2 ) = 清潔度 {this.clean}";
    }
    private void PoliceCalculate ( PlayerDataManager pm , GameDataManager gm )
    {
        //當前行動點的治安度-( 5 + 當前擴建設施數量*2 ) = 下回合治安度  (數值最低為0，不會變成負數。)
        float police = Formula.Police.police ( pm );
        float lessPolice = Formula.Police.LessPolice ();
        float storeQuantity = Formula.Police.StoreExpandNum ( pm );
        this.police = Formula.Police.Calculate ( pm );
        policeLog = $"治安度 {police} - ( {lessPolice} + 設施數量 {storeQuantity} * 2 ) = 治安度 {this.police}";
    }
}
