﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Prefab
{
    public class NextTeach : MonoBehaviour
    {
        public Image image;
        public GameObject obj;

        private DramaEvent.TeachTargetEvent events;

        public void DoEvent ( DramaEvent.TeachTargetEvent events , string nextTeach )
        {
            Sprite [] sprites = Resources.LoadAll<Sprite> ( "Sprites/Common/Drama/Teach/NextTeach" );

            if ( nextTeach == "商品進貨" )
                image.sprite = sprites [0];
            else if ( nextTeach == "商品擺放" )
                image.sprite = sprites [1];
            else if ( nextTeach == "商品改良" )
                image.sprite = sprites [2];
            else
            {
                Debug.LogError ( nextTeach + "  不是指定文字" );
                image.sprite = sprites [0];
            }
            this.events = events;
            StartCoroutine ( Play () );
        }

        IEnumerator Play ()
        {
            Color loadingColor = image.color;
            loadingColor.a = 0;
            image.color = loadingColor;
            image.DOFade ( 1 , 0.5f );
            yield return null;

            while ( events.targetBool == false )
            {
                yield return null;
            }

            Destroy ( obj );
        }
    }
}